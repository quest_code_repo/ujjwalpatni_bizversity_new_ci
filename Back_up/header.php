<!DOCTYPE html>
<html>
    <head>
        <title>Ujjwal Patni</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <base href="<?=base_url();?>">

        <link rel="icon" type="image/png" sizes="96x96" href="front_assets/images/icons/favicon.png">
        <!-- Bootstrap Core CSS -->
        <link href="front_assets/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="front_assets/css/bootstrap-social.css" rel="stylesheet"/>
        <link href="front_assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet"/>
        <link href="front_assets/css/animate.css" rel="stylesheet"/>
        <!-- owl CSS -->
        <link href="front_assets/css/owl.theme.css" rel="stylesheet"/>
        <link href="front_assets/css/owl.transitions.css" rel="stylesheet"/>
        <link href="front_assets/css/owl.carousel.css" rel="stylesheet"/>
        <!-- hover-min CSS -->
        <link href="front_assets/css/yamm.css" rel="stylesheet"/>
        <!-- Fonts icons -->
        <link href="front_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="front_assets/css/jquery-ui.css" rel="stylesheet"/>

        <!-- Custom CSS -->
        <link href="front_assets/css/custom.css" rel="stylesheet"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JQuery -->
        <script type="text/javascript" src="front_assets/js/jquery-1.11.3.min.js"></script>
    </head>



<body>



<!-- Header html -->
<header class="header-main">
    <div class = "navbar navbar-default main-menu" role = "navigation">
        <div class="container-fluid">
            <div class="row">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle = "collapse" data-target = "#navbar-collapse">
                        <span class = "sr-only">Toggle navigation</span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="<?php echo base_url();?>">
                        <img src="front_assets/images/other/logo.png">
                    </a>
                </div>
               
                <div class = "collapse navbar-collapse" id = "navbar-collapse">
                    <ul class = "nav navbar-nav">
                        <!-- <li><a href = "#">About</a></li> -->
                       
                        <!-- <li><a href = "#">programs</a></li> -->
                        <li class="dropdown new-biz-dropdown">
                          <a href="javascript:void(0)" class="dropbtn">programs &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-content">
                              <?php
                              if (!empty($events_details)) {
                                 foreach ($events_details as $key => $value) {
                                  ?>
                               <li><a href="<?php echo $value->url; ?>"><?php echo $value->name; ?></a></li>

                              <?php   }
                               } 
                              ?>
                               
                            </ul>
                         </li>
                            <li class="dropdown new-biz-dropdown">
                               <a href="javascript:void(0);" class="dropbtn">mentoring &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-content">
                                  <?php
                                      if(!empty($Mentoring) && count($Mentoring)>0)
                                      {
                                          foreach($Mentoring as $manResult)
                                          {
                                            ?>
                                             <li><a href="mentoring/mentoring_criteria/<?php echo $manResult->url;?>"><?php echo $manResult->title;?></a></li>
                                            <?php
                                          }
                                      }
                                  ?>  
                                
                                </ul>
                         </li>
                          <li class="dropdown new-biz-dropdown">
                               <a href="javascript:void(0)" class="dropbtn">e-Learning &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                              <ul class="dropdown-content">
                                 <li><a href="elearning/courses">Courses</a></li> 
                                 <!-- <li><a href="elearning/courses">Courses</a></li>  -->
                              </ul>
                         </li>
                        
                        <li><a href = "product">products</a></li>
                        <!--  <li><a href = "mentoring/coming_soon">products</a></li>  -->
                        <li><a href = "blog">blog</a></li>
                         <li class="dropdown new-biz-dropdown">
                               <a href="javascript:void(0)" class="dropbtn">About &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                              <ul class="dropdown-content">
                                 <li><a href="about-us">About Us</a></li>
                                 <li><a href="Ujjwal-Patni-International">Ujjwal Patni International</a></li>
                                 <li><a href="contact-us">Contact</a></li>
                                 <li><a href="privacy-policy">Privacy Policy</a></li>
                                 
                              </ul>
                         </li>
                        <!-- <li><a href = "login.php">login</a></li> -->
                        <?php

              if(empty($this->session->userdata('id')))
              {
                ?>
                  <li><a href="#" data-toggle="modal" data-target="#register-modal">Login/Sign up</a></li>
                <?php
              }
              else
              {
                ?>
            <li class="dropdown new-biz-dropdown">
                <a href="javascript:void(0)" class="dropbtn user-pic"><img src="assets/images/other/profile.jpg" class="img-responsive" style="height: 40px;"></a>
                <ul class="dropdown-content profil-dropdown">
                    <li>
                        <div class="media">
                          <div class="media-left">                                     
                            <img src="assets/images/other/profile.jpg" class="media-object" style="height: 45px;">
                          </div>

                         

                          <?php
                              if(!empty($this->session->userdata('id')))
                              {
                              
                                ?>
                          <div class="media-body">
                            <h5 class="media-heading"><?php echo ($this->session->userdata('username'));?></h5>
                            <p><?php echo ($this->session->userdata('full_name'));?></p>
                          </div>
                                <?php
                              }
                              else
                              {
                                ?>
                          <div class="media-body">
                            <h5 class="media-heading">Test</h5>
                            <p>Test@gmail.com</p>
                          </div>
                                <?php
                              }
                          ?>
                        
                        </div>
                    </li>
                    <li><a href="my-courses"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Dashboard</a></li>
                    <li><a href="mentoring/coming_soon"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Profile</a></li>
                    <li><a href="mentoring/coming_soon"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp; Change Password</a></li>
                    <li><a href="<?php echo base_url('home/logout')?>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; Log Out</a></li>
                 </ul>
            </li> 
                <?php  }  ?>
                      <li class="cart"><a href = "<?php echo base_url('my-cart')?>" title="cart">
                        <img src="front_assets/images/icons/cart.png">
                        <div class="badge" id="item_value"><?php if (!empty($item_count[0]->total_qty)) {
                          echo $item_count[0]->total_qty;
                        }else{ echo "0";} ?></div>
                        <input type="hidden" id="cart_count" value="<?php if (!empty($item_count[0]->total_qty)) {
                          echo $item_count[0]->total_qty;
                        }else{ echo "0";} ?>">
                      </a></li>
                    </ul>
                    <ul class="social-links pl-0">
                        <li>
                            <a href="javascript:void(0);">Call <span>88787 59999</span></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.facebook.com/ujjwalpatni/timeline"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://twitter.com/Ujjwal_Patni"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.youtube.com/user/personality2009"><i class="fa fa-youtube-play" aria-hidden="true"></i></a
                                ></li>
                        <li>
                            <a target="_blank" href="https://www.linkedin.com/company/patni-personality-plus-pvt-ltd/?originalSubdomain=in"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header html end -->