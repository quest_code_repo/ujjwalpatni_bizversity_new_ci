<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		//Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/xcrud/plugins/ckeditor/ckeditor.js';
		$xcrud = Xcrud::get_instance()->table('testimonials');
        /**********view pages***********/
		$xcrud->order_by('testimonials_id','desc');
		$xcrud->unset_add(); 
		$xcrud->relation('user_id','customer','id','full_name');

		$xcrud->subselect('User Email','select email from customer where id={user_id}','user_id');
		$xcrud->label('user_id','User');
		
        $data['contentdata'] = $xcrud->render();
		$data['title'] ='Testimonial List';
		admin_view('admin/testmonial_list', $data);
    }

    public function front_content()
    {
    	Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/xcrud/plugins/ckeditor/ckeditor.js';
    	$xcrud = Xcrud::get_instance()->table('front_content');
    	$data['contentdata'] = $xcrud->render('edit', 1);
    	// $xcrud->hide_button('save_new');
    	$xcrud->hide_button('Return');
    	// $xcrud->hide_button('save_return');
		$data['title'] ='Content List';
		admin_view('admin/content', $data);
    }
	
}
