<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        $this->load->helper('path');
        //$this->load->model('front_end/cat_frontend');
        
    }


	public function index()
	{

    		$data['Mentoring'] = $this->home_model->getwheres_data('mentoring_criteria',array('status'=>'active'));

    		$date = date('Y-m-d');
    		$data['upcomingEvent'] = $this->home_model->getwhere_data_order_asc(array('tbl_programs.status'=>'active','program_date >=' =>$date),'','program_date','');

        $data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);

        $data['EbookContent'] = $this->home_model->getwheres_data('ebook_content',array('ebook_content_status'=>'active'));
        $data['SocialMedia'] = $this->home_model->getwheres_data('social_media_subscribers',array());

    		$this->load_view('index',$data);
	}


	public function user_registration()
	{
		$postData = $this->input->post();

		if(!empty($postData))
		{
			$where = array('username'=> $postData['username']);
			$check_email = $this->home_model->getwheres_data('tbl_students',$where);
			

			if(!empty($check_email))
			{
				
				echo "CHECK_ERROR";
				die();
			}

			$newUser = array('username'  => $postData['username'],
							'password'   => md5($postData['password']),
							'full_name'  => $postData['full_name'],
							'created_at' => date('Y-m-d H:i:s')
							);
			$resultId = $this->home_model->insert_data('tbl_students',$newUser);

			// $to = $postData['username'];
			// $subject = 'Successfull Registraion On Bizversity';
			// $name = $postData['full_name'];

			// $content = "Dear ".$name.",<br> <br> your regstraion has been completed Successfully below is your login details:<br> <br> Usernmae :".$postData['username']."<br>password: ".$postData['password']."";

			// $this->sendmail($to,$subject,$content);


			if(!empty($resultId))
			{
				 $where = array('id'=>$resultId);
			    $UserData = $this->home_model->getwheres_data('tbl_students',$where);

			    if(!empty($UserData))
			    {
			    	$userDetails = array('username' => $UserData[0]->username,
			    						 'full_name'=> $UserData[0]->full_name,
			    						 'id'       => $UserData[0]->id
			    					);

			    	$this->session->set_userdata($userDetails);

			    	echo "SUCCESS";
			    }
			}

			
		}

	}


	public function user_login()
	{
		$postData = $this->input->post();

		if(!empty($postData))
		{
			$email = $postData['username'];
			$pass  = md5($postData['password']);


			if(!empty($email))
			{
				$where = array('username'=> $email);
			    $check_email = $this->home_model->getwheres_data('tbl_students',$where);

			    if(empty($check_email))
			    {
			    	echo "EMAIL_ERROR";
			    	die();
			    }

			    if(!empty($pass))
			    {
			    	$where = array('password'=> $pass);
			       $check_pass = $this->home_model->getwheres_data('tbl_students',$where);

				    if(empty($check_pass))
				    {
				    	echo "PASS_ERROR";
				    	die();
				    }
			    }

			    $where = array('username'=> $email,'password'=>$pass);
			    $UserData = $this->home_model->getwheres_data('tbl_students',$where);

			    if(!empty($UserData))
			    {
			    	$userDetails = array('username' => $UserData[0]->username,
			    						 'full_name'=> $UserData[0]->full_name,
			    						 'id'       => $UserData[0]->id
			    					);

			    	$this->session->set_userdata($userDetails);

			    	echo "SUCCESS";
			    }

			}
		}
	}


	 public function sendmail($to,$subject,$content){

       $this->load->library('email');
       $from_email = "training@ujjwalpatni.com"; 

       $this->email->from($from_email,'Ujjwap Patni Training Programs');
       $this->email->to($to); 
       $this->email->subject($subject);
       $this->email->set_mailtype('html');
       $this->email->message($content);  

       if($this->email->send()){
               return true;
       }else{
               return false;
       }
   }

 

	public function logout()
  {
        $session = array(
            'login'=>FALSE,
            'id'=>'',
            'sessionid'=>'',
            'username' =>'',
            'full_name' =>''
            
        );
      
        $this->session->unset_userdata($session);

         $this->session->sess_destroy();

      redirect(base_url());
      exit();
  }


  // *******************  Contact Us Imformation **********************//

  public function contact_us()
  {
  	   $this->load_view('home/contact_us');
  }

  public function add_new_contact_imformation()
  {
  	$postData = $this->input->post();

  	if(!empty($postData))
  	{
  		$newData = array('name'       => $postData['contact_name'],
  					     'email'      => $postData['contact_email'],
  					     'contact_no' => $postData['contact_mobile'],
  					     'message'    => $postData['comment'],
  					     'created_at' => date('Y-m-d H:i:s')
  		);	

  		$contactId = $this->home_model->insert_data('contact_us',$newData);

  		if(!empty($contactId))
  		{      

                /***Mail For The Server***/ 
                $config['mailtype'] = 'html';
                $config['charset'] = 'iso-8859-1';
                $config['priority'] = 1;
                $this->email->initialize($config);
                $this->email->clear();

                $message = "";

         $message.='<!DOCTYPE html>
<html>
<head>
    <title>Contact Details</title>
</head>
<body>

<div style="width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;">
    <div style="background-color: #D55220;">
        <img src="https://ujjwalpatni.com/front_assets/logo.png" style="height: 55px;">
    </div>
    <div style="font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px">
        <p>Dear Team Ujjwal Patni,</p>
       
    <table>
        <tr>
            <th style="width: 190px;height: 30px;">Contact Name:</th>
            <td>'.$postData['contact_name'].'</td>
        </tr>
        <tr>
            <th style="width: 190px;height: 30px;">Contact Email:</th>
            <td>'.$postData['contact_email'].'</td>
        </tr>
        <tr>
            <th style="width: 190px;height: 30px;">Contact Mobile:</th>
            <td>'.$postData['contact_mobile'].'</td>
        </tr>
        <tr>
            <th style="width: 190px;height: 30px;">Message:</th>
            <td>'.$postData['comment'].'</td>
        </tr>
        
    </table>
   
    

    <p style="margin:25px 0 5px; ">Thanks!</p>
    <p style="margin:5px 0;">Regards,</p>
    <p style="margin:5px 0;">Team Ujjwal Patni</p>
    </div>
</div>

</body>
</html>';


                $to = "ppppltd@gmail.com";
                $from_email = "info@questglt.com";
                $this->email->from($from_email,'Ujjwal Patni Training Programs');
                $this->email->to($to); 
                $this->email->subject('Contact Details');
                $this->email->set_mailtype('html');
                $this->email->message($message);  
                $send = $this->email->send();


  				echo "SUCCESS";
  		}
  	}
  }


  // *******************  Contact Us Imformation **********************//


  // ********************  Ujjwal Patni International ********************//

  public function ujjwal_patni_international()
  {
  		$data['UjjwalInternational'] = $this->home_model->getwheres_data('pages',array('id'=>8));
  		$this->load_view('home/ujjwal_patni_international',$data);
  }


  // ********************  Ujjwal Patni International ********************//

  public function about_us()
  {
  		$this->load_view('home/about_us');
  }


  public function privacy_policy()
  {
  		$this->load_view('home/privacy_policy');
  }


  	// **************************  Chat Box **************************************//

  	public function add_new_chat_imformation()
  	{
  		$postData = $this->input->post();

  		if(!empty($postData))
  		{
  			$newData = array('name'       => $postData['chat_name'],
  					     'email'      => $postData['chat_email'],
  					     'message'    => $postData['chat_message'],
  					     'created_at' => date('Y-m-d H:i:s')
  		);	

  		$contactId = $this->home_model->insert_data('contact_us',$newData);

		  		if(!empty($contactId))
		  		{
		  				echo "SUCCESS";
		  		}
  		}
  	}

  	// *****************************************************************************//


  	// ********************************************************** //


  	public function forgot_pass()
  	{
  		$email = $this->input->post('forgot_email');

  		if(!empty($email))
  		{
  			    $where = array('username'=> $email);
			    $check_email = $this->home_model->getwheres_data('tbl_students',$where);

			    if(empty($check_email))
			    {
			    	echo "EMAIL_ERROR";
			    	die();
			    }

			    $pass = "123456";
			    $new_pass = md5($pass);
			    $to = $email;



			    $config['mailtype'] = 'html';
                $config['charset'] = 'iso-8859-1';
                $config['priority'] = 1;
                $this->email->initialize($config);
                $this->email->clear();

			        $message = "<!DOCTYPE html>
                        <html>
                        <head>
                            <title>Forget Password</title>
                        </head>
                        <body>
                        <div style='width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;'>
                            <div style='background-color: #D55220;'>
                                <img src='https://ujjwalpatni.com/front_assets/logo.png' style='height: 55px;'>
                            </div>
                            <div style='font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px'>
                                <p>Dear " .$this->session->userdata('full_name'). ",</p>
                                <P>Your PassWord Is <b>" .$pass. "</b>.</p>
                                <p>Below are your registration details.</p>
                              
                                <p style='margin:25px 0 5px; '>Thanks!</p>
                                <p style='margin:5px 0;'>Regards,</p>
                                <p style='margin:5px 0;'>Team Ujjwal Patni</p>
                            </div>
                        </div>

                        </body>
                        </html>";
                // $content = $this->load->view('events/event-booking-mail-template', $email_data);
                $from_email = "training@ujjwalpatni.com";
                $this->email->from($from_email,'Ujjwal Patni Training Programs');
                $this->email->to($to); 
                $this->email->subject('Forget Password');
                $this->email->set_mailtype('html');
                $this->email->message($message);  
                $send = $this->email->send();

                $where = array('username'=> $email);
                $data5 = array('password' => $new_pass);
			    $check_email = $this->home_model->update_data('tbl_students',$data5,$where);


			    echo "SUCCESS";

  		}
  	}


    
    public function thank_you()
    {
      $this->load_view('home/thank_you');
    }



     public function add_new_corporate_details()
    {
        $postData = $this->input->post();

        if(!empty($postData))
        {
           $new_e_book_form = array('e_name'         => $postData['corporate_name'],
                        'e_email'        => $postData['corporate_email']
                        // 'e_organization' => $postData['corporate_organization'],
                        // 'e_position'     => $postData['corporate_position'],
                        // 'e_industry'     => $postData['corporate_industry'],
                        // 'e_staff_count'  => $postData['corporate_staff']
                      );

              $e_book_id = $this->home_model->insert_data('e_book_contact_form',$new_e_book_form);

              if(!empty($e_book_id))
              {

              	$ebookContent = $this->home_model->getwheres_data('ebook_content',array('ebook_content_id'=>$postData['ebook_content_id']));


               $to = $postData['corporate_email'];
               $config['mailtype'] = 'html';
                $config['charset'] = 'iso-8859-1';
                $config['priority'] = 1;
                $this->email->initialize($config);
                $this->email->clear();


                $message = '<!DOCTYPE html>
              <html>
              <head>
                <title>E Book</title>
              </head>
              <body>

              <div style="width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;">
                <div style="background-color: #D55220;">
                  <img src="https://ujjwalpatni.com/front_assets/logo.png" style="height: 55px;">
                </div>
                <div style="font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px">
                  <p>Dear <b>'.$postData['corporate_name'].'</b>,</p>
                  <p>Please find the E-book attached. </p>
                  <p><b style="color: #D55220;">Wish you a happy learning!!!</b>.</p>

                  <div>
                      <img src="https://ujjwalpatni.com/front_assets/images/other/55.png" style="height: 200px;">
                  </div>
                  
                <p style="margin:25px 0 5px; ">Thanks!</p>
                <p style="margin:5px 0;">Regards,</p>
                <p style="margin:5px 0;">Team Ujjwal Patni</p>
                </div>
              </div>

              </body>
              </html>';


                   $from_email = "training@ujjwalpatni.com";
                    $this->email->from($from_email,'Ujjwal Patni Training Programs');
                    $this->email->to($to); 
                    $this->email->subject('E-Book');
                    $this->email->set_mailtype('html');
                    $this->email->message($message);

                     $path = set_realpath('uploads/e_books/');
                    //$this->email->attach($path);
                    $this->email->attach($path . $ebookContent[0]->ebook_content_image.'');
                    
                    $send = $this->email->send();

                    echo "SUCCESS";
              }
        }
    }


  	// ***********************************************************//

}
