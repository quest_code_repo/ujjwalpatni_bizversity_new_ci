<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('Events_model','events');
        $this->load->model('home_model');
        
    }


	public function vip()
	{
		if ($_POST) {
			$postData = $this->input->post();
	        $userId = $this->session->userdata('id');
	    	if(!empty($postData))
	        {
	        	$where = array('user_id'=>$userId,'product_id'=>$postData['programes_id'], 'product_type' => 'programe');
	        	$exist = $this->events->get_data_orderby_where('cart_items','',$where,'');

	        	if (empty($exist)) {	        	
	        		$cartItems = array(
		            	'user_id'      => $userId,
		                'product_id'   => $postData['programes_id'],
		                'product_type' => 'programe',
		                'p_quantity'   => $postData['qty'],
		                'actual_price' => base64_decode($postData['programe_price']),
		                'created_at'   => date('Y-m-d H:i:s'),
		            );
	            	$this->events->insert_data('cart_items',$cartItems);

		            $updatestDetails = array(
		            	'f_name'   =>  $postData['f_name'],
		             	'l_name'   =>  $postData['l_name'],
		             	'address'  =>  $postData['address'],
		             	'pincode'  =>  $postData['pincode'],
		             	'mobile'   =>  $postData['phone'],
		             	'city'     =>  $postData['city_name'],
		             	'date_ob'  =>  $postData['date_ob'],
		         	);

		            $this->events->update_data('tbl_students',$updatestDetails,array('id'=>$userId));
	        	}
	        	redirect(base_url('my-cart'));
	        }  

		}

		if ($this->session->userdata('id')) {
			$where = array('id'=>$this->session->userdata('id'));
			$data['UserDetail'] = $this->events->get_data_orderby_where('tbl_students', "", $where, "");
		}else{
			$data['UserDetail'] = '';
		}


		$where = array('programs_category_id'=>1,'status'=>'active');
		$data['explored_data'] = $this->events->get_data_orderby_where('tbl_vip_explored_content', "exp_id", $where, "ASC");

			$date = date('Y-m-d');

		$where = array('tbl_programs.category_id'=>1,'tbl_programs.deleted'=>'0','tbl_programs.program_date >='=>$date);
		$column = array('*','tbl_programs.id as programe_id');
		$data['program_details'] = $this->events->get_program_details($column, $where, "tbl_programs.program_date", "ASC", "","");

		$data['all_cities'] = $this->events->get_data_orderby_where('cities','city_name',array(),'ASC');

		$data['BrochureDetails'] = $this->home_model->getdata_orderby_where_join2('brochure_content','tbl_programs_category','brochure_category_id','id',array('brochure_category_id'=>1),'brochure_name,brochure_image,brochure_id','','');

			$data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);

			$data['VideoUrl'] =  $this->events->get_data_orderby_where('tbl_programs_category', "",array('deleted'=>'0','name'=>'VIP'), "");

			
		$this->load_view('events/vip', $data);
	}

	public function excellence_gurukul()
	{
		if ($_POST) {
			$postData = $this->input->post();
	        $userId = $this->session->userdata('id');
	    	if(!empty($postData))
	        {
	        	$where = array('user_id'=>$userId,'product_id'=>$postData['programes_id'], 'product_type' => 'programe');
	        	$exist = $this->events->get_data_orderby_where('cart_items','',$where,'');

	        	if (empty($exist)) {	        	
	        		$cartItems = array(
		            	'user_id'      => $userId,
		                'product_id'   => $postData['programes_id'],
		                'product_type' => 'programe',
		                'p_quantity'   => $postData['qty'],
		                'actual_price' => base64_decode($postData['programe_price']),
		                'created_at'   => date('Y-m-d H:i:s'),
		            );
	            	$this->events->insert_data('cart_items',$cartItems);

		            $updatestDetails = array(
		            	'f_name'   =>  $postData['f_name'],
		             	'l_name'   =>  $postData['l_name'],
		             	'address'  =>  $postData['address'],
		             	'pincode'  =>  $postData['pincode'],
		             	'mobile'   =>  $postData['phone'],
		             	'city'     =>  $postData['city_name'],
		             	'date_ob'  =>  $postData['date_ob'],
		         	);

		            $this->events->update_data('tbl_students',$updatestDetails,array('id'=>$userId));
	        	}
	        	redirect(base_url('my-cart'));
	        }
		}

		if ($this->session->userdata('id')) {
			$where = array('id'=>$this->session->userdata('id'));
			$data['UserDetail'] = $this->events->get_data_orderby_where('tbl_students', "", $where, "");
		}else{
			$data['UserDetail'] = '';
		}

		$where = array('programs_category_id'=>2,'status'=>'active');
		$data['explored_data'] = $this->events->get_data_orderby_where('tbl_vip_explored_content', "exp_id", $where, "ASC");

		$date = date('Y-m-d');

		$where = array('tbl_programs.category_id'=>2,'tbl_programs.deleted'=>'0','tbl_programs.program_date >='=>$date);
		$column = array('*','tbl_programs.id as programe_id');
		$data['program_details'] = $this->events->get_program_details($column, $where, "tbl_programs.program_date", "ASC", "","");

			

		$data['all_cities'] = $this->events->get_data_orderby_where('cities','city_name',array(),'ASC');

		$data['BrochureDetails'] = $this->home_model->getdata_orderby_where_join2('brochure_content','tbl_programs_category','brochure_category_id','id',array('brochure_category_id'=>2),'brochure_name,brochure_image,brochure_id','','');
	
			$data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);

			
			   $data['VideoUrl'] =  $this->events->get_data_orderby_where('tbl_programs_category', "",array('deleted'=>'0','name'=>'Excellence Gurukul'), "");


		$this->load_view('events/excellence-gurukul', $data);
	}

	public function power_parenting()
	{
		if ($_POST) {
			$postData = $this->input->post();
	        $userId = $this->session->userdata('id');
	    	if(!empty($postData))
	        {
	        	$where = array('user_id'=>$userId,'product_id'=>$postData['programes_id'], 'product_type' => 'programe');
	        	$exist = $this->events->get_data_orderby_where('cart_items','',$where,'');

	        	if (empty($exist)) {	        	
	        		$cartItems = array(
		            	'user_id'      => $userId,
		                'product_id'   => $postData['programes_id'],
		                'product_type' => 'programe',
		                'p_quantity'   => $postData['qty'],
		                'actual_price' => base64_decode($postData['programe_price']),
		                'created_at'   => date('Y-m-d H:i:s'),
		            );
	            	$this->events->insert_data('cart_items',$cartItems);

		            $updatestDetails = array(
		            	'f_name'   =>  $postData['f_name'],
		             	'l_name'   =>  $postData['l_name'],
		             	'address'  =>  $postData['address'],
		             	'pincode'  =>  $postData['pincode'],
		             	'mobile'   =>  $postData['phone'],
		             	'city'     =>  $postData['city_name'],
		             	'date_ob'  =>  $postData['date_ob'],
		         	);

		            $this->events->update_data('tbl_students',$updatestDetails,array('id'=>$userId));
	        	}
	        	redirect(base_url('my-cart'));
	        }
		}

		if ($this->session->userdata('id')) {
			$where = array('id'=>$this->session->userdata('id'));
			$data['UserDetail'] = $this->events->get_data_orderby_where('tbl_students', "", $where, "");
		}else{
			$data['UserDetail'] = '';
		}

		$where = array('programs_category_id'=>3,'status'=>'active');
		$data['explored_data'] = $this->events->get_data_orderby_where('tbl_vip_explored_content', "exp_id", $where, "ASC");

		$date = date('Y-m-d');
		$where = array('tbl_programs.category_id'=>3,'tbl_programs.deleted'=>'0','tbl_programs.program_date >='=>$date);
		$column = array('*','tbl_programs.id as programe_id');
		$data['program_details'] = $this->events->get_program_details($column, $where, "tbl_programs.program_date", "ASC", "","");

		$data['all_cities'] = $this->events->get_data_orderby_where('cities','city_name',array(),'ASC');

		$data['BrochureDetails'] = $this->home_model->getdata_orderby_where_join2('brochure_content','tbl_programs_category','brochure_category_id','id',array('brochure_category_id'=>3),'brochure_name,brochure_image,brochure_id','','');

			$data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);


			$data['VideoUrl'] =  $this->events->get_data_orderby_where('tbl_programs_category', "",array('deleted'=>'0','name'=>'Power Parenting'), "");

		$this->load_view('events/power-parenting', $data);
	}

	public function key_note()
	{
		if ($_POST) {
			$postData = $this->input->post();
	        $userId = $this->session->userdata('id');
	    	if(!empty($postData))
	        {
	        	$where = array('user_id'=>$userId,'product_id'=>$postData['programes_id'], 'product_type' => 'programe');
	        	$exist = $this->events->get_data_orderby_where('cart_items','',$where,'');

	        	if (empty($exist)) {	        	
	        		$cartItems = array(
		            	'user_id'      => $userId,
		                'product_id'   => $postData['programes_id'],
		                'product_type' => 'programe',
		                'p_quantity'   => $postData['qty'],
		                'actual_price' => base64_decode($postData['programe_price']),
		                'created_at'   => date('Y-m-d H:i:s'),
		            );
	            	$this->events->insert_data('cart_items',$cartItems);

		            $updatestDetails = array(
		            	'f_name'   =>  $postData['f_name'],
		             	'l_name'   =>  $postData['l_name'],
		             	'address'  =>  $postData['address'],
		             	'pincode'  =>  $postData['pincode'],
		             	'mobile'   =>  $postData['phone'],
		             	'city'     =>  $postData['city_name'],
		             	'date_ob'  =>  $postData['date_ob'],
		         	);

		            $this->events->update_data('tbl_students',$updatestDetails,array('id'=>$userId));
	        	}
	        	redirect(base_url('my-cart'));
	        }
		}

		if ($this->session->userdata('id')) {
			$where = array('id'=>$this->session->userdata('id'));
			$data['UserDetail'] = $this->events->get_data_orderby_where('tbl_students', "", $where, "");
		}else{
			$data['UserDetail'] = '';
		}

		$where = array('programs_category_id'=>4,'status'=>'active');
		$data['explored_data'] = $this->events->get_data_orderby_where('tbl_vip_explored_content', "exp_id", $where, "ASC");

		$where = array('tbl_programs.category_id'=>4,'tbl_programs.deleted'=>'0');
		$column = array('*','tbl_programs.id as programe_id');
		$data['program_details'] = $this->events->get_program_details($column, $where, "tbl_programs.program_date", "ASC", "","");

		$data['all_cities'] = $this->events->get_data_orderby_where('cities','city_name',array(),'ASC');

		$data['BrochureDetails'] = $this->home_model->getdata_orderby_where_join2('brochure_content','tbl_programs_category','brochure_category_id','id',array('brochure_category_id'=>4),'brochure_name,brochure_image,brochure_id','','');

			$data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);


			$data['VideoUrl'] =  $this->events->get_data_orderby_where('product_categories', "",array('category_id'=>1), "");

		$this->load_view('events/key-note', $data);
	}

	public function get_events_date_by_city(){

		$category_id = $this->input->post('category_id');
		$city_id = $this->input->post('city_id');

        $userId = $this->session->userdata('id');
        if (!empty($userId)) {
            $where = array('category_id'=>$category_id,'city_ids'=>$city_id,'deleted'=>'0','status'=>'active');
            $data['events_date_details'] = $this->events->get_data_orderby_where('tbl_programs','',$where,'');
            // $this->load->view('events/ajax_events',$data);
            $this->load->view('events/ajax_event_date',$data);
        }
       
	}
	public function get_events_details_by_date(){

		$category_id = $this->input->post('category_id');
		$program_date    = $this->input->post('program_date');

        $userId = $this->session->userdata('id');
        if (!empty($userId)) {
            $where=array('category_id'=>$category_id,'program_date'=>$program_date,'deleted'=>'0','status'=>'active');
            $exist = $this->events->get_data_orderby_where('tbl_programs','',$where,'');
            $encode_price 	= base64_encode($exist[0]->price);
            $show_price 	= $exist[0]->price;
            $program_id 	= $exist[0]->id;
            $program_name 	= $exist[0]->program_name;
            $program_date 	= date('d-M-y H:i',strtotime($exist[0]->program_date));
            
            $msg = "SUCCESS";
            $data = array('msg'=>$msg,'program_id'=>$program_id,'program_name'=>$program_name,'actual_price'=>$show_price,'encode_price'=>$encode_price);
            $arr  = json_encode($data);
            print_r($arr);
            exit;
            // $this->load->view('events/ajax_event_date',$data);
            
        }
       
	}
	
	public function programe_add_to_card()
    {
    	$postData = $this->input->post();
        $userId = $this->session->userdata('id');
    	if(!empty($postData))
        {
        	$where = array('user_id'=>$userId,'product_id'=>base64_decode($postData['programes_id']), 'product_type' => 'programe');
        	$exist = $this->events->get_data_orderby_where('cart_items','',$where,'');

        	if ($exist) {
        		$delete = $this->events->delete_data('cart_items',$where);
        		$msg = "REMOVE";
        	}else{
        		$cartItems = array(
	            	'user_id'      => $userId,
	                'product_id'   => base64_decode($postData['programes_id']),
	                'product_type' => 'programe',
	                'p_quantity'   => $postData['qty'],
	                'actual_price' => base64_decode($postData['programe_price']),
	                'created_at'   => date('Y-m-d H:i:s'),
	            );

            	$this->events->insert_data('cart_items',$cartItems);

	            $updatestDetails = array(
	            	'f_name'   =>  $postData['f_name'],
	             	'l_name'   =>  $postData['l_name'],
	             	'address'  =>  $postData['address'],
	             	'pincode'  =>  $postData['pincode'],
	             	'mobile'   =>  $postData['phone'],
	             	'city'     =>  $postData['city_name'],
	             	'date_ob'  =>  $postData['date_ob'],
	         	);
	            $this->events->update_data('tbl_students',$updatestDetails,array('id'=>$userId));
	            $msg = "SUCCESS";
        	}

	        if (!empty($userId)) {
	          	$item_count = $this->front->cart_item_sum_sql($userId);
	          	if ($item_count[0]->total_qty != NULL) {
	          		$item_count = $item_count[0]->total_qty;
	          	} else {
		          $item_count = 0;
		        }
		        $data = array('msg'=>$msg,'item_count'=>$item_count);
		        $arr = json_encode($data);
		        print_r($arr);
		        exit;
		    }
	        // echo json_encode($data['item_count']);
            
        }   
    }

    public function delete_for_program_cart_item($cart_id="")
    {   
        if(!empty($cart_id))
        {
            $where = array('cart_id'=>$cart_id);
            $this->home_model->delete_data('cart_items',$where);
            redirect('Cart/cart','refresh');
        }
    }


    public function get_dates_acc_to_city()
    {
    	$postData = $this->input->post();

    	if(!empty($postData))
    	{
    		$data = $this->home_model->getwhere_data('tbl_programs',array('status'=>'active','category_id'=>$postData['event_type'],'city_ids'=>$postData['e_city']),'program_date','id','');

    		if(!empty($data))
    		{
    			$datesr = '';
    			foreach($data as $result)
    			{
    				$datesr .= "".date("m-d-Y", strtotime($result->program_date)).",";

    			}

    			$first = rtrim($datesr,",");
    			echo $first;
    			
    		}
    	}
    }


}
