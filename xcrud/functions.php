<?php
function publish_request($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE contact_form SET `status` = "pending" WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}
function unpublish_request($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE contact_form SET `status` = "processed" WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}

function publish_request_comment($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE comments SET `status` = "approved" WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}

function unpublish_request_comment($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE comments SET `status` = "pending" WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}

function insert_brand_cat($postdata, $primary, $xcrud)
{
    
    if ($postdata->get('category_id'))
    {
        $db = Xcrud_db::get_instance();
        $cats = explode(',', $postdata->get('category_id'));
        if(!empty($cats)){
            foreach ($cats as  $cat) {
                $query = 'INSERT INTO `brand_pro_cat_rel` (`brand_id`, `pro_category_id`) VALUES ('.$primary.','. $cat.')';
                $db->query($query);
            }
        }
    }
}
function update_brand_cat($postdata, $primary, $xcrud)
{
    if ($postdata->get('category_id'))
    {
        $db = Xcrud_db::get_instance();
        $cats = explode(',', $postdata->get('category_id'));
        // var_dump($cats);
        // var_dump($primary);
        // die;
        if(!empty($cats)){
            $db->query('DELETE FROM `brand_pro_cat_rel` WHERE `brand_pro_cat_rel`.`brand_id` = '.$primary);
            foreach ($cats as  $cat) {
                $query = 'INSERT INTO `brand_pro_cat_rel` (`brand_id`, `pro_category_id`) VALUES ('.$primary.','. $cat.')';
                $db->query($query);
            }
        }
    }
}

function update_order_status($xcrud)
{
    if($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();


        $query1 = 'SELECT * from  product_orders  WHERE product_order_id = "'.$xcrud->get('primary').'"';
        $db->query($query1);
        $result=$db->row();

        $total=$result['order_amount']+$result['remaining_amount'];


      
         $query = 'UPDATE product_orders SET order_amount="'.$total.'",remaining_amount=0,`delivery_status` = "delivered" WHERE product_order_id = "'.$xcrud->get('primary').'"';
         $db->query($query);
    }
}


function update_order_cancel($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE orders SET `status` = "cancelled" WHERE order_id = "'.$xcrud->get('primary').'"';
        $db->query($query);
        
        $query = 'UPDATE order_items SET `status` = "cancelled" WHERE order_id = "'.$xcrud->get('primary').'"';
        $db->query($query);
    }
}

function make_url($postdata,$primary, $xcrud)
{
    $title = $postdata->get('title');
    // echo "<br>";
    $alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $title));
    $blog_url = str_replace(' ', '-', $alpha);
    // $CI= &get_instance();
    $db = Xcrud_db::get_instance();
    $query_res = $db->query("SELECT count(*) as total FROM `mentoring_criteria` WHERE `url` LIKE '%$blog_url%'");
    $total =$db->row($query_res);
    $nomb = $total['total'];
    // print_r($primary);
    // exit;
    // $nomb = $query_res->num_rows();
    if ($nomb > 0) {
        $blog_url1 = $blog_url.'-'.$nomb;
    }else{
        $blog_url1 = $blog_url;
    }
    // $xcrud->pass_var('url', $blog_url1);
    // print_r($postdata);
    // exit;

    if ($primary)
    {
        // $db = Xcrud_db::get_instance();
        $query = 'UPDATE mentoring_criteria SET `url` = "'.$blog_url1.'" WHERE id = "'.$primary.'"';
        $db->query($query);
        
        // $query = 'UPDATE order_items SET `status` = "cancelled" WHERE order_id = "'.$xcrud->get('primary').'"';
        // $db->query($query);
    }
}
