
/* Show hide scrolltop button */
$(window).scroll(function () {
    /* Show hide scrolltop button */
    if ($(window).scrollTop() == 0) {
        $('.scroll_top').stop(false, true).fadeOut(600);
    } else {
        $('.scroll_top').stop(false, true).fadeIn(600);
    }
});

/* scroll top */
$(document).on('click', '.scroll_top', function () {
    $('body,html').animate({scrollTop: 0}, 400);
    return false;
})
/*---- scroll top end-----*/

/*---- scroll top end-----*/
jQuery(document).ready(function ($) {
    var navbar = $('.header-main'),
            distance = navbar.offset().top + 20,
            $window = $(window);

    $window.scroll(function () {
        if ($window.scrollTop() >= distance) {
            navbar.removeClass('navbar-fixed-top').addClass('navbar-fixed-top');
            $("body").css("padding-top", "0px");
        } else {
            navbar.removeClass('navbar-fixed-top');
            $("body").css("padding-top", "0px");
        }
    });
});
/*---- scroll top end-----*/

/* -- section-program-slider -- */
$(document).ready(function () {
    var owl1 = $("#owl-program");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 2],
            [1200, 2],
            [1400, 2],
            [1600, 2]
        ],
        autoPlay: true,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});


/* -- section-program-slider -- */
$(document).ready(function () {
    var owl1 = $("#owl-media");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        autoPlay: true,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});


/* -- section-testimonial-slider -- */
$(document).ready(function () {
    var owl1 = $("#owl-testimonial");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        autoPlay: true,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});

$(document).ready(function () {
    var owl1 = $("#owl-works");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        autoPlay: true,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});

$(document).ready(function () {
    var owl1 = $("#owl-courses-dash1");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        autoPlay: false,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});

$(document).ready(function () {
    var owl1 = $("#owl-courses-dash2");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        autoPlay: false,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});

$(document).ready(function () {
    var owl1 = $("#owl-courses-dash3");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        autoPlay: false,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});


$(document).ready(function () {
    var owl1 = $("#owl-courses-cart");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        autoPlay: false,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});


$(document).ready(function () {
    var owl1 = $("#owl-related-books");
    owl1.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        autoPlay: false,
        navigation: true,
        // transitionStyle: "fade",
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false
    });
});
/*---- icheckbox -----*/
/*$(document).ready(function () {
 $('.input-checkbox').iCheck({
 checkboxClass: 'icheckbox_square-orange',
 increaseArea: '20%'
 });
 $('.input-radio').iCheck({
 radioClass: 'iradio_flat-orange',
 increaseArea: '20%'
 });
 });*/
/*---- end-----*/

/*----nice select-----*/
/*$(document).ready(function () {
 $('select').niceSelect();
 });*/
/*----nice select end-----*/

/* slick sliders */
/*$('.banners-slick').slick({
 autoplay: true,
 arrows: true,
 focusOnSelect: true,
 dots: true,
 infinite: true,
 speed: 1000,
 slidesToShow: 1,
 centerMode: true,
 variableWidth: true
 });*/
/* slick end */



/*----wow initialization-----*/
wow = new WOW(
        {
            animateClass: 'animated',
            offset: 100,
            callback: function (box) {
                console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
        }
);
wow.init();
/*----wow end-----*/




















