<section class="section-banner">
    <img src="front_assets/images/banners/Vip Banner-01.jpg" class="img-responsive"/>
</section>

<section class="topics-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="topics-block">
					<h2 class="common-heading color-key-purple wow fadeInUp">Topics to be explored</h2>
					<ul class="topics-list key-note-topic">
                        <?php
                        if (!empty($explored_data)) {
                            $i=1;
                            foreach ($explored_data as $value) {
                                $slide = $i%2;
                        ?>

                        <li class="key-<?php echo $i; ?> wow <?php if ($slide==1) { echo "slideInLeft";
                        }else{ echo "slideInRight"; } ?>" ><?php echo $value->content; ?></li>
                        <?php $i++; } } ?>

						<!-- <li class="key-1 wow slideInLeft">Learn the habits of global leaders</li>
						<li class="key-2 wow slideInRight">Extreme prductivity habits</li>
						<li class="key-3 wow slideInLeft">Technology habits</li>
						<li class="key-4 wow slideInRight">Daily success habits</li>
						<li class="key-5 wow slideInLeft">Habits for 21st Century</li>
						<li class="key-6 wow slideInRight">Activation of three senses</li>
						<li>Subconscious habits</li> -->
					</ul>
				</div>
				<div class="topic-background">
					<img src="front_assets/images/other/topic-back.png">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-register">
    <div class="color-overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <div class="col-md-12 clearfix">
                            <ul>
             
                                <li class="event-name seat-report">
                                    Limited seats available for next event
                                </li>
                                <li class="in">
                                    in
                                </li>
                                <li class="place">
                                    <?php if (!empty($program_details)) {
                                            echo $program_details[0]->city_name;
                                        }else{
                                            echo "No Event Here";
                                        }  
                                    ?>
                                </li>
                                <li class="date">
                                    <?php if (!empty($program_details)) {
                                        echo $usable_time = date("M d", strtotime($program_details[0]->program_date));
                                    }
                                    ?>
                                </li>
                                <li class="text-right">
                                  <a href="#Register-sec" class="btn btn-register Registr-Now">REGISTER NOW</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="why-attend-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="why-attent-inner">
					<div class="row">
						<div class="col-sm-4">
							<div class="why-card">
								<div class="why-card-relative key-relative">
									<h1>why</h1>
								<h2>should i attend</h2>
								 <img src="front_assets/images/other/vip-logo.png" class="img-responsive">
								</div>

							</div>
						</div>
						<div class="col-sm-8">
							<div class="why-block-content key-block-content">
								<p>I desire to be <span>super productive</span> with time in my hand</p>

								<p>I want to know how ordinary people into <span>super achievers</span></p>

								<p>I need aq better <span>Solution</span> for managing my calls & social media engagement</p>

								<p>I would like to <span>Balance</span> my work life & family life</p>

								<p>I want to <span>Feel special</span> and be <span>acnowledged </span> everywhere</p>
								<p>I wish to <span>conquer</span> my self doubt & procrastination</p>

								<p>I need immediately implementable <span>Practical ideas</span></p>
								<div>
									<img src="front_assets/images/icons/key-ques.png" class="img-responsive">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="wht-to-expect">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="wht-expect-content">
					<div class="row">
						<div class="col-md-6 col-sm-12 pr-0">
							<div class="video-block">
								<div class="wht-expect-video">
								<?php if (!empty($program_details[0]->video_url)) { ?>
                                    <iframe width="100%" height="280" src="<?php if (!empty($program_details[0]->video_url)) { echo $program_details[0]->video_url; }?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                 <?php }else{ ?>
                                   <iframe width="100%" height="280" src="https://www.youtube.com/embed/u5DV5lLksUg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                               <?php } ?> 
								
							</div>
							<div class="text-center share-outer">
			                        <a href="" class="btn btn-common wow pulse">Share Video With Friends</a>
			                    </div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 pl-0">
							<h2 class="common-heading gold-heading wow fadeInUp">Topics to be explored</h2>
							<div class="row mr-0">
								<div class="col-md-6  col-sm-12 pl-0">
									<div class="expect-rule-list key-rule-list">
										<ul>
											<li>Residential Learning Days</li>
											<li>Training Kit, Stationery and Worksheets</li>
											<li>Meditation and Intense sessions</li>
										</ul>
									</div>
								</div>
								<div class="col-md-6  col-sm-12 pl-0">
									<div class="expect-rule-list key-rule-list">
										<ul>
											<li>Residential Learning Days</li>
											<li>Training Kit, Stationery and Worksheets</li>
											<li>Meditation and Intense sessions</li>
										</ul>
									</div>
								</div>
								<div class="text-center share-outer">
			                       <a href="" class="btn btn-common wow pulse">
			                       Share Brochure With Friends</a>
			                    </div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Simple Section html -->
<!-- Simple Section html -->
<section class="section-upcoming for-events-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-lg-3 col-lg-offset-3 ">
                <p class="wow slideInLeft">DISCOVER MORE UPCOMING EVENTS</p>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <ul id = "myTab" class = "nav nav-tabs">
                    <li >
                        <h4>Select Your City</h4>
                    </li>
                    <li class = "active">
                         <div class="form-group">
                              <select class="form-control" id="sel1">
                                <?php
                                if (!empty($program_details)) {
                                    foreach ($program_details as $key => $vip_cities) {
                                ?>
                                    <option><?php echo $vip_cities->city_name; ?></option>
                                        
                                <?php  } }else{ ?>
                                    <option>No Event Here</option>

                                <?php } ?>
                              </select>
                            </div> 
                       <!--  <a href = "#april" data-toggle = "tab">april</a> -->
                    </li>
                   <!--  <li>
                        <a href = "#may" data-toggle = "tab">may</a>
                    </li> -->
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-3">
                <div class="calendar">
                            <div class="calendar-box">
                                <div class="white-box">
                                    <div id="datepicker1"></div>
                                    <hr>
                                    <div class="event-details clearfix">
                                        <span class="event-date"><div>28</div></span>
                                        <span class="event-logo"><img src="front_assets/images/other/vip-logo.png"></span>
                                        <span class="event-place">kolkata</span>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-12">
                    <h2 class="common-heading wow fadeInUp">APPRECIATIONS</h2>
                </div>
                <div class="col-sm-12">
                    <div id="owl-testimonial">
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit amet, consequat nec ipsum. Maecenas consectetur, vel aliquam molestie, nunc diam blandit nisl, a placerat felis ante. ”
                                    </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/person1.jpg">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name">MR. RAHUL AGARWAL,<span>CEO Zen Enterprise</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit amet, consequat nec ipsum. Maecenas consectetur, vel aliquam molestie, nunc diam blandit nisl, a placerat felis ante. ”
                                </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/person2.jpg">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name">MR. RAHUL AGARWAL,<span>CEO Zen Enterprise</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit amet, consequat nec ipsum. Maecenas consectetur, vel aliquam molestie, nunc diam blandit nisl, a placerat felis ante. ”
                                </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/person3.jpg">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name">MR. RAHUL AGARWAL,<span>CEO Zen Enterprise</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->

<section class="section-form">
       <div id="Register-sec"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <?php
                if(empty($this->session->userdata('id')))
                {
                ?>   
                <div class="col-sm-12">
                    <h2 class="common-heading wow fadeInUp">registration</h2>
                    <h4 class="register-down-text text-center">To register, Please <a href="javacsript:void(0)" data-toggle="modal" data-target="#register-modal">Login</a></h4>
                </div>
                 <?php
                } elseif (empty($program_details)) { ?>
                <div class="col-sm-12">
                    <h2 class="common-heading gold-heading wow fadeInUp">registration</h2>
                    <h4 class="register-down-text text-center">No Event Here</h4>
                </div>
                <?php }else{  ?>
                <form id="key_note_register_form" action="vip" method="POST">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>First Name*</label>
                            <input type="text" name="f_name" data-rule-required="true" data-msg-required="Please Enter First Name" class="form-control" value="<?php echo isset($UserDetail[0]->f_name)?$UserDetail[0]->f_name:$this->session->userdata('full_name');?>">
                        </div>
                    </div>
                    <div class="col-sm-6 padd-left">
                        <div class="form-group">
                            <label>Last Name*</label>
                            <input type="text" name="l_name" data-rule-required="true" data-msg-required="Please Enter Last Name" class="form-control" value="<?php echo isset($UserDetail[0]->l_name)?$UserDetail[0]->l_name:'';?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Address*</label>
                            <input type="text" name="address" data-rule-required="true" data-msg-required="Please Enter Address" class="form-control" value="<?php echo isset($UserDetail[0]->address)?$UserDetail[0]->address:'';?>">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Pincode*</label>
                            <input type="text" name="pincode" data-rule-required="true" data-msg-required="Please Enter Pincode" class="form-control" value="<?php echo isset($UserDetail[0]->pincode)?$UserDetail[0]->pincode:'';?>">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Phone number*</label>
                            <input type="text" name="phone" data-rule-required="true" data-msg-required="Please Enter Phone Number" onkeypress="return isNumberKey(event);" data-rule-number="true" data-rule-minlength="10"  data-msg-minlength="Please Enter Minimum 10 Digits" data-rule-maxlength="13"  data-msg-maxlength="You Can't Enter More Than 13 Digits" class="form-control" value="<?php echo isset($UserDetail[0]->mobile)?$UserDetail[0]->mobile:'';?>">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>E-mail*</label>
                            <input type="email" name="username" data-rule-required="true" data-msg-required="Please Enter Email" class="form-control" value="<?php echo isset($UserDetail[0]->username)?$UserDetail[0]->username:$this->session->userdata('username');?>">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="sel1">City*</label>
                            <select class="form-control" name="city_name" id="sel1" data-rule-required="true" data-msg-required="Please Select City">
                                <option value="">Select your city</option>
                                <?php
                                if (!empty($all_cities)) {
                                    foreach ($all_cities as $value) {
                                        if (!empty($UserDetail[0]->city)) {
                                            if ($UserDetail[0]->city == $value->city_id) {
                                                $selected = "selected";
                                            }else{
                                                $selected = "";
                                            }
                                        }else{
                                            $selected = "";
                                        }
                                ?>
                                <option value="<?php echo $value->city_id; ?>" <?php echo $selected; ?> ><?php echo $value->city_name; ?></option>

                                <?php  }  } ?>
                            </select>
                        </div> 
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="sel1">Select Programe City*</label>
                            <select class="form-control" name="event_city" id="event_city" data-rule-required="true" data-msg-required="Please Select Programe City" onchange="get_events_date_by_city(this.value);">
                            <option value="">Select</option>
                            <?php
                            if (!empty($program_details)) {
                                foreach ($program_details as $key => $events_cities) {
                            ?>
                                <option value="<?php echo $events_cities->city_ids; ?>"><?php echo $events_cities->city_name; ?></option>
                            <?php  } } ?>
                            </select>
                            <input type="hidden" id="event_category_id" value="4">
                        </div> 
                    </div>    

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="sel1">Date Of Programe*</label>
                            <select class="form-control" name="program_date" id="program_date" data-rule-required="true" data-msg-required="Please Select Date Of Programe" onchange="get_events_details_by_date(this.value);">
                                <option value="">Select Programe Date</option> 
                            </select>                            
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="sel1">Programe Name*</label>
                            <input type="text" name="program_name" id="program_name" class="form-control" readonly="readonly" value="Programe Name">
                            <input type="hidden" name="programes_id" id="programes_id" value="">             
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="event-tickit-amt">
                            <p>Amount</p>
                            <h4 id="actual_price"> 
                            ₹
                            <?php 
                                if (!empty($program_details)) {
                                    echo $program_details[0]->price; }else{
                                        echo "0";
                                } ?>
                            </h4>
                            <input type="hidden" id="programe_price" name="programe_price" value="<?php if (!empty($program_details)) { echo $program_details[0]->price; } ?>">
                            <input type="hidden" name="qty" value="<?php echo '1'; ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox form-group">
                            <label class="p-0" >
                                <input value="0" name="terms_condition" id="terms_condition" data-rule-required="true" data-msg-required="Please Select Terms Condition" type="checkbox">
                                <span class="cr"><i class="cr-icon fa fa-check" onclick="terms_condition()" aria-hidden="true"></i></span>
                                I agree to the Terms & Conditions
                                <span class="error" id="error_tc"></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p>Details: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit. Amet, consequat nec ipsum. Maecenas consectetur, odio vel aliquam molestie, nunc diam.Blandit nisl, a placerat felis massa in ante. Donec quis metus magna. Fusce sed ullamcorpe.</p>
                    </div>
                    <div class="col-md-12">
                        <div class="form-btn-box add-cart">
                            <div class="col-md-6 text-center" id="msg" style="display: none;">
                            </div>
                            <!-- <input type="submit" class="btn btn-register" id="add_to_card" value="Add to Cart"> -->
                        </div>
                        <div class="form-btn-box add-cart add-cart-proceed">
                        
                            <button type="submit" class="btn btn-register" onclick="return check_term()">Add to Cart and proceed to checkout&nbsp; &nbsp; &nbsp; &nbsp;</button>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>
        </div>
    </div>
</section>   


<!-- Simple Section html -->
<section class="product-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 product-block">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <div>
                           <h3 class="wow slideInLeft">Products</h3>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                      <div class="wow zoomIn">
                        <img src="front_assets/images/icons/product1.png" class="img-responsive">
                        <h4>Training</h4>
                      </div>    
                    </div>
                    <div class="col-sm-2 col-xs-6">
                      <div class="wow zoomIn">
                        <img src="front_assets/images/icons/product2.png" class="img-responsive">
                        <h4>Books</h4>
                      </div>    
                    </div>
                    <div class="col-sm-2 col-xs-6">
                      <div class="wow zoomIn">
                        <img src="front_assets/images/icons/product3.png" class="img-responsive">
                        <h4>CDs</h4>
                      </div>    
                    </div>
                    <div class="col-sm-2 col-xs-6">
                      <div class="wow zoomIn">
                        <img src="front_assets/images/icons/product14.png" class="img-responsive">
                        <h4>Merchandise</h4>
                      </div>    
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <button class="btn btn-register">Buy Now</button>
                    </div>
                </div>
                <div class="single-line"></div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-program">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <h2 class="common-heading wow fadeInUp">EXPLORE MORE SIGNATURE PROGRAMMES</h2>
                </div>
                <div class="col-md-12">
                    <div id="owl-program">
                        <div class="item">
                            <div class="pro-box vip">
                                <div class="white-box">
                                    <img src="front_assets/images/other/vip-logo.png">
                                    <div class="txt">
                                    A TWO DAY AND ONE NIGHT RESIDENTIAL TRAINING PROGRAM
                                    </div>
                                    <div class="text-center mt-20"><a href="">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    PERSONAL PRODUCTIVITY AND BUSINESS SUCCESS HABITS
                                </div>
                                <a href="" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/excellence-logo.png">
                                    <div class="txt">
                                    A ONE YEAR COMPACT PERSONAL AND BUSINESS COURSE OF MULTIPLE SESSIONS
                                    </div>
                                    <div class="text-center mt-20"><a href="">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    PERSONAL, BUSINESS & PUBLIC EXCELLENCE
                                </div>
                                <a href="" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/keynote.png">
                                    <div class="txt">
                                    EQUALLY FLUENT IN BOTH HINDI AND ENGLISH
                                    </div>
                                    <div class="text-center mt-20"><a href="">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    INVITE DR.UJJWAL PATNI TO KICKSTART YOUR CONFERENCE  OR ANNUAL MEETING
                                </div>
                                <a href="" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/pioneer.png">
                                    <div class="txt">
                                    A PROGRAMME DESIGNED FOR COUPLES WITH KIDS
                                    </div>
                                    <div class="text-center mt-20"><a href="">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    SPECIAL SKILLS TO RAISE AND NURTURE EXTRAORINARY CHILDREN
                                </div>
                                <a href="" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<script src="<?php ?>front_assets/developer_validate/jquery.validate.min.js"></script>
<script src="<?php ?>front_assets/developer_validate/custom_validate.js"></script> 
<style type="text/css">
    .error{ color: red; }
</style>
<script type="text/javascript">
    $(document).ready(function (){
        $('#key_note_register_form').validate({
            onfocusout: function(element) {
                this.element(element);
            },
            errorClass: 'error_validate',
            errorElement:'span',
            highlight: function(element, errorClass) {
               $(element).removeClass(errorClass);
            }
        });
    });

    function terms_condition(){
        // alert();
        var terms_condition = $("#terms_condition").val();
        if (terms_condition=='0') {
            $("#terms_condition").val('1');
        }else{
            $("#terms_condition").val('0');
        }        
    }

    function check_term(){
        var terms_condition = $("#terms_condition").val();
        if (terms_condition=='0') {
            // alert('Please Select Terms And Conditions');
            $("#error_tc").show();
            $("#error_tc").text('Please Select Terms And Conditions');
            return false;
        }else{
            $("#error_tc").hide();
            return true;
        }
    }
</script>

<!-- End -->
<!-- ************************* CART *******************************-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#add_to_card').click(function(){
            $('#key_note_register_form').validate({
                onfocusout: function(element) {
                    this.element(element);
                },
                errorClass: 'error_validate',
                errorElement:'span',
                highlight: function(element, errorClass) {
                    $(element).removeClass(errorClass);
                },

                submitHandler:function(form)
                {
                    var terms_condition = $("#terms_condition").val();
                    if (terms_condition=='0') {
                        $("#error_tc").show();
                        $("#error_tc").text('Please Select Terms And Conditions');
                        return false;
                    }else{
                        $("#error_tc").hide();
                        // return true;
                    }

                    $.ajax({
                        type :'POST',
                        dataType: 'json',
                        url  :'<?php echo base_url("events/programe_add_to_card")?>',
                        data : $('#key_note_register_form').serialize(),
                        success:function(resp)
                        {
                            if(resp.msg == "SUCCESS")
                            {  
                                $('#msg').show();
                                $('#msg').css('color','green');
                                $('#item_value').text(resp.item_count);
                                $('#msg').text('Item Added To Cart Successfully');
                            } else {
                                $('#msg').show();
                                $('#msg').css('color','red');
                                $('#item_value').text(resp.item_count);
                                $('#msg').text('Item Added Remove Successfully');
                            }
                        } 
                    });
                }
            });
        });
    });
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function () {
    $(".Registr-Now").on('click', function (e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $('html, body').animate({
            scrollTop: ($(target).offset().top)
        }, 1500);
    });
});
</script>