<!DOCTYPE html>
<html>
    <head>
        <title>Ujjwal Patni</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <base href="<?=base_url();?>">
        
        <!-- Bootstrap Core CSS -->
        <link href="front_assets/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="front_assets/css/bootstrap-social.css" rel="stylesheet"/>
        <link href="front_assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet"/>
        <link href="front_assets/css/animate.css" rel="stylesheet"/>
        <!-- owl CSS -->
        <link href="front_assets/css/owl.theme.css" rel="stylesheet"/>
        <link href="front_assets/css/owl.transitions.css" rel="stylesheet"/>
        <link href="front_assets/css/owl.carousel.css" rel="stylesheet"/>
        <!-- hover-min CSS -->
        <link href="front_assets/css/yamm.css" rel="stylesheet"/>
        <!-- Fonts icons -->
        <link href="front_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="front_assets/css/jquery-ui.css" rel="stylesheet"/>

        <!-- Custom CSS -->
        <link href="front_assets/css/custom.css" rel="stylesheet"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<body>



<!-- Header html -->
<header class="header-main">
    <div class = "navbar navbar-default main-menu" role = "navigation">
        <div class="container-fluid">
            <div class="row">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle = "collapse" data-target = "#navbar-collapse">
                        <span class = "sr-only">Toggle navigation</span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="<?php echo base_url()?>">
                        <img src="front_assets/images/other/logo.png">
                    </a>
                </div>
               
                <div class = "collapse navbar-collapse" id = "navbar-collapse">
                    <ul class = "nav navbar-nav navbar-right">
                        <!-- <li><a href = "#">About</a></li> -->
                       
                        <!-- <li><a href = "#">programs</a></li> -->
                       <!-- <li>
                    <a href="javascript:void(0)" class="dropbtn" data-toggle="dropdown">Categories &nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                       <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                          <li class="dropdown-submenu">
                            <a tabindex="-1" href="vip">VIP</a>
                            <ul class="dropdown-menu">
                              <li><a tabindex="-1" href="#">Second level</a></li>
                              <li><a href="#">Second level</a></li>
                              <li><a href="#">Second level</a></li>
                            </ul>
                          </li>
                          <li class="dropdown-submenu">
                            <a tabindex="-1" href="excellence-gurukul">Excellence Gurukul</a>
                            <ul class="dropdown-menu">
                              <li><a tabindex="-1" href="#">Second level</a></li>
                              <li><a href="#">Second level</a></li>
                              <li><a href="#">Second level</a></li>
                            </ul>
                          </li>
                          <li class="dropdown-submenu">
                            <a tabindex="-1" href="">Profit Gurukul</a>
                            <ul class="dropdown-menu">
                              <li><a tabindex="-1" href="#">Second level</a></li>
                              <li><a href="#">Second level</a></li>
                              <li><a href="#">first level</a></li>
                            </ul>
                          </li>
                          <li class="dropdown-submenu">
                            <a tabindex="-1" href="power-parenting">Power Parenting</a>
                            <ul class="dropdown-menu">
                              <li><a tabindex="-1" href="#">Second level</a></li>
                              <li><a href="#">Second level</a></li>
                              <li><a href="#">Second level</a></li>
                            </ul>
                          </li>
                         
                        </ul>
                 </li> -->

                         
                         <!-- <li class="active">
                   <div class="blog-search header-dash-serch">
                          <form method="post" action="dashboard/course_details">
                              <div class="input-group">

                                  <input type="hidden" id="CourseUrl"  name="CourseUrl"  value=""  />

                                  <input class="form-control CourseName" id="CourseName" name="course_name" placeholder="Search" type="text" onkeyup="get_all_courses();">
                                  <span class="input-group-btn" style="display:  -webkit-box;">
                                     <button class="btn btn-default" type="submit">
                                         <i class="fa fa-search"></i>
                                     </button>
                                  </span>
                               </div>
                          </form>
                      </div>
                    </li> -->
                        
                         
                        
                        

                       <li class="cart">
                          <a href = "<?php echo base_url('my-cart')?>" title="cart">
                            <img src="front_assets/images/icons/cart.png">
                        <div class="badge" id="item_value"><?php if (!empty($item_count[0]->total_qty)) {
                          echo $item_count[0]->total_qty;
                        }else{ echo "0";} ?></div>
                        <input type="hidden" id="cart_count" value="<?php if (!empty($item_count[0]->total_qty)) {
                          echo $item_count[0]->total_qty;
                        }else{ echo "0";} ?>">
                      </a></li>


                         <li class="dropdown new-biz-dropdown">
                <a href="javascript:void(0)" class="dropbtn user-pic"><img src="assets/default_profile.png" class="img-responsive" style="height: 40px;"></a>
                <ul class="dropdown-content profil-dropdown new">
                    <li>
                        <div class="media">
                          <div class="media-left">                                     
                            <img src="assets/default_profile.png" class="media-object" style="height: 45px;">
                          </div>

                         

                          <?php
                              if(!empty($this->session->userdata('id')))
                              {                              
                          ?>
                          <div class="media-body">
                            <h5 class="media-heading"><?php echo ($this->session->userdata('username'));?></h5>
                            <p><?php echo ($this->session->userdata('full_name'));?></p>
                          </div>
                                <?php
                              }
                              else
                              {
                                ?>
                          <div class="media-body">
                            <h5 class="media-heading">Test</h5>
                            <p>Test@gmail.com</p>
                          </div>
                                <?php
                              }
                          ?>
                        
                        </div>
                    </li>
                    <li><a href="my-courses"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Dashboard</a></li>
                    <li><a href="dashboard/my_profile"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Profile</a></li>
                    <li><a href="mentoring/coming_soon"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp; Change Password</a></li>
                    <li><a href="<?php echo base_url('home/logout')?>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; Log Out</a></li>
                 </ul>
            </li> 

                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header html end -->

