<style type="text/css">
    .blog-addon{padding: 0;}
    .blog-addon .btn {
    padding: 5px 8px;
    border-radius: 0;
}
</style>

<section class="section-blogs section-blog-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12">
                      <div class="row">
                        <div class="col-sm-8">
                         <!--  <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                          <li role="presentation"><a href="#lifeStyle" aria-controls="lifeStyle" role="tab" data-toggle="tab">LifeStyle</a></li>
                          <li role="presentation"><a href="#health" aria-controls="health" role="tab" data-toggle="tab">Health</a></li>
                          <li role="presentation"><a href="#other" aria-controls="other" role="tab" data-toggle="tab">Other</a></li>
                      </ul> -->
                        </div>
                        <div class="col-sm-4">
                      
                          <div class = "input-group blog">
                            <input class="form-control blog_info" placeholder="Search" type="text" id="blogName" value="" onkeyup="blog_autocomplete();">

                            <input type="hidden" class="blog_url" name="blog_url" value=''  />

                            <span class = "input-group-addon blog-addon">
                                <button class="btn" onclick="get_blog_details();"><i class="fa fa-search"></i></button>
                            </span>
                          </div>
                        
                        </div>
                      </div>
                       
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="all">
                         <div class="row">

                            <?php
                                if(!empty($Blogs) && count($Blogs)>0)
                                {
                                    foreach($Blogs as $blogDetais)
                                    {
                                        ?>
                        <div class="col-sm-4">
                             <div class = "panel panel-default panel-blog">
                                <div class = "panel-heading">
                                    <a href="javascript:void(0);"><img src="<?php echo base_url()?>uploads/blog-image/<?php echo $blogDetais->image_url;?>"/></a>
                                    <div class="panel-heading-overlay">
                                        <a href="<?php echo base_url('blog/blog_detail/'.$blogDetais->blog_url.'')?>"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class = "panel-body">
                                    <h4 class="heading-blogg"><a href="<?php echo base_url('blog/blog_detail/'.$blogDetais->blog_url.'')?>"><?php echo $blogDetais->blog_title;?></a></h4>

                                    <?php
                                        $mydate = $blogDetais->created_date;
                                        $actualDate = date("d M y",strtotime($mydate));
                                    ?>

                                     <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $actualDate;?></p>
                                  
                                    <div class="clearfix"></div>
                                    <div class="content-blog">
                                        <p><?php echo $blogDetais->short_decription;?>
                                          <br><a href="<?php echo base_url('blog/blog_detail/'.$blogDetais->blog_url.'')?>">Read More</a>
                                        </p>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                    <h2>No Record Found</h2>
                                    <?php
                                }
                            ?>
                    
                       
                    </div>
                 <!--    <div class="row">
                        <div class="col-md-12 text-center">
                             <ul class = "pagination">
                               <li><a href = "#">&laquo;</a></li>
                               <li><a href = "#">1</a></li>
                               <li><a href = "#">2</a></li>
                               <li><a href = "#">3</a></li>
                               <li><a href = "#">4</a></li>
                               <li><a href = "#">5</a></li>
                               <li><a href = "#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div> -->
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lifeStyle">
                        
                    </div>
                    <div role="tabpanel" class="tab-pane" id="health">
                        
                    </div>
                    <div role="tabpanel" class="tab-pane" id="other">
                        
                    </div>
                </div>
            </div>
             </div>
               
                </div>
            </div>
        </section>


        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">


 <script type="text/javascript">

    function blog_autocomplete(){
       jQuery('.blog_info').autocomplete({
            source: '<?php echo base_url()?>blog/get_blog_autocomplete?blog_name='+$('#blog_name').val(),
            minLength: 1,
            select: function(event, ui) {

              jQuery('.course_info').val(ui.item.blog_title); 
              $('.blog_url').val(ui.item.url); 
              
               window.location.href = "<?php echo base_url()?>blog/blog_detail/"+ui.item.url;
            }
        });
    }
</script>

<script type="text/javascript">
    function get_blog_details()
    {
        var url = $('.blog_url').val(); 
        window.location.href = "<?php echo base_url()?>blog/blog_detail/"+url;

    }
</script>