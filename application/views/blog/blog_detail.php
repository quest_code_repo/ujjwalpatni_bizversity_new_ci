
<!-- Slider Section html -->
            <section class="section-blog-desc">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <h4 class="common-heading wow fadeInUp"><?php echo $BlogDetail[0]->blog_title;?></h4>


                            
                            <div class="row">
                                <div class="col-md-9 left-panel">
                                    <div class="blog-details">
                                        <div class="blog-img">
                                            <img src="<?php echo base_url('uploads/blog-image/'.$BlogDetail[0]->image_url.'')?>">
                                        </div>
                                        <div class="blog-desc">


                                        <?php
                                            $mydate = $BlogDetail[0]->created_date;
                                            $actualDate = date("d M y",strtotime($mydate));
                                        ?>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <span class="cal">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i><?php echo $actualDate;?>
                                                    </span>
                                                    <span class="blog-user">
                                                        <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $BlogDetail[0]->author;?>
                                                    </span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="socail-icon-blog">
                                                        <span>Share on </span>
                                                       
                                                        <?php
                                                      	$shareUrl = current_url();
                                                        ?> 

                                                     <a name="fb_share" type="button" href="https://www.facebook.com/sharer.php?u=<?php echo $shareUrl;?>" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>

                                                     <a id="ref_gp" href="https://plus.google.com/share?url=<?php echo urlencode($shareUrl);?>" target="_blank" class="gp">
                                                         <i class="fa fa-google-plus"></i>
                                                     </a>   

                                                     <a href="whatsapp://send?text=<?php echo base_url()?>blog/blog_detail/<?php echo $BlogDetail[0]->blog_url; ?>" data-action="share/whatsapp/share" class="wa hidden-md hidden-lg hidden-sm"><i class="fa fa-whatsapp"></i></a>

                                                        <a href="https://twitter.com/intent/tweet?url=<?php echo $BlogDetail[0]->blog_title;?>&text=<?php echo $BlogDetail[0]->blog_url;?>&via=Ujjwal_Patni" target="_blank" class="tw">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $BlogDetail[0]->blog_url;?>&title=<?php echo $BlogDetail[0]->blog_title;?>
&summary=&source=LinkedIn" class="linkd" target="_blank">
                                                            <i class="fa fa-linkedin"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="blog-text">
                                                <p><?php echo $BlogDetail[0]->blog_desc;?></p>
                                                
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <!-- Slider Section html end -->