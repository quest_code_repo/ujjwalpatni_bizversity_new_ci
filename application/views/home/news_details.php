

<section class="new-detail-section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h4 class="common-heading wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">2 Favourite Brands that Bounced Back to the Summit, After They had Lost Their Business</h4>
                <div class="blog-details">
                    <div class="blog-img">
                        <img src="front_assets/images/other/blog1.jpg" class="img-responsive">
                    </div>
                    <div class="blog-desc">
                           <span class="cal">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>06 Jun 18                                                    </span>
                               
                          <div class="blog-text">
                            <p>Friends, people repeatedly ask me if a brand can become positive after it has become negative. Or, can a product become successful in the market once again, after it has failed.
                              </p><ol>
                                <li>How to transform a negative brand into a positive brand.</li>
                                <li>How to establish a failure product in the market.</li>
                              </ol>
                              I would like to talk on this, giving you examples of 2 famous brands in India, so that it is easy for you to relate.<br>
                              The first brand is Cadbury, adjudged as the best gift for any happy occasion. A few years back, Cadbury experienced a setback. Some chocolates were found infested with worms and fungus. As the media spread&nbsp; this new throughout the country, Cadbury chocolates earned a bad name. So much so, parents dissuaded their children from eating their favourite chocolates.<br>
                              However, Cadbury was sure about the quality of its product. After a proper investigation, the fault was traced with the supply chain and its storage. Due to sub- standard storage facility at some places, the packing was affected and the chocolates got infested.<br>
                              Cadbury immediately swung into action and started a massive campaign to rectify the supply chain, the storage and packaging. Soon after this exercise, Cadbury deputed Shri Amitabh Bacchan as their brand ambassador, because he is regarded as a positive brand. This association of Amitabh Bacchan with Cadbury, again made Cadbury the leaders in the market.<br>
                              Generally, it becomes difficult for a product to revive after a negative branding. But, if the product has the potential and is of good quality, then even after a failure,&nbsp; it experiences only a temporary setback.<br>
                              Another example that I would like to share is that of Maggie. This brand is popular from Kashmir to Kanyakumari, and a favourite food item for children.&nbsp; Sometime back there was a report against this brand, and the media took severe action. Packets of Maggie were thrown off the shelves and burnt. The business for Maggie suddenly dropped to Zero, and for several months Maggie was out of market. Such an exclusive product, top ranking in instant noodles, seemed to have seen its end.<br>
                              &nbsp;Maggie did not lose hope. As a corrective measure, it filed a court case and won. Thereafter, they published positive advertisements and stood up again.<br>
                              People soon forgot all negative things circulated about Cadbury and Maggie, and began to use the products again.<br>
                              If a product, an enterprise or a person, has the potential, then no failure can deter them from standing up again. You must have read stories of hundreds of businessmen, who failed in their venture, were almost finished, they were almost written off; but they stood up again and wrote their success stories.<br>
                              Faith in oneself is the key to turn failure into success. Cadbury and Maggie are excellent examples to show how a product that has failed can again become successful.<br>
                              &nbsp;<p></p>
                            
                            </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>

 