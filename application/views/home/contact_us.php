
<section class="about-us-section contact-us-section" style="padding-bottom: 0;">
    <div class="container">
        <div class="row">
              <div class="col-sm-10 col-sm-offset-1">
                <h2 class="common-heading wow fadeInUp">Contact Us</h2>
              </div>
           </div>
        <div class="row">
          <div class="col-md-10 col-sm-offset-1 contact-info">
            <div class="row text-center">
                <address class="col-sm-4 col-md-4">
                <div class="big-icon bg"> <i class="fa fa-map-marker fa-2x"></i> </div>
                <div class="sub-heading">Address</div>
                <p><b>PATNI PERSONALITY PLUS PVT. LTD.</b> </p>
                </address>
                <address class="col-sm-4 col-md-4">
                <div class="big-icon bg"> <i class="fa fa-microphone fa-2x"></i> </div>
                <div class="sub-heading">Phone</div>
                <p>+918-8787-59999</p>
                </address>
                <address class="col-sm-4 col-md-4">
                <div class="big-icon bg"> <i class="fa fa-envelope fa-2x"></i> </div>
                <div class="sub-heading">Email Addresses</div>
                <div><a href=""> training@ujjwalpatni.com</a></div>
                </address>
            </div>
           
        </div>
        </div>
    </div>
</section>


		

<section class="section-form">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-12">
                    <h2 class="common-heading  wow fadeInUp">Get In Touch with Us
                </div>

                <form id="contact_form" name="contact_form" method="post">

                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Name*</label>
                        <input name="contact_name" id="contact_name" class="form-control" type="text" data-rule-required="true" data-msg-required="Please Enter Name ">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>E-mail*</label>
                        <input name="contact_email" id="contact_email" class="form-control" type="email" data-rule-required="true" data-msg-required="Please Enter Email ">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Name Of Company</label>
                        <input name="company_name" id="company_name" class="form-control" type="text" data-rule-required="true" data-msg-required="Please Enter Company Name">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Designation</label>
                        <input name="designation" id="designation" class="form-control" type="text" data-rule-required="true" data-msg-required="Please Enter Designation">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Whatsapp Number</label>
                        <input name="contact_mobile" id="contact_mobile" class="form-control descPriceNumVal" type="text" data-rule-required="true" data-msg-required="Please Enter Whatsapp Number" minlength="10" maxlength="10">
                    </div>
                </div>
             <!--    <div class="col-sm-12">
                    <div class="form-group">
                        <label>Phone number*</label>
                        <input name="contact_mobile" id="contact_mobile" class="form-control descPriceNumVal" type="text" data-rule-required="true" data-msg-required="Please Enter Mobile No " minlength="10" maxlength="13">
                    </div>
                </div> -->
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Name Of City</label>
                        <input name="city" id="city" class="form-control" type="text" data-rule-required="true" data-msg-required="Please Enter City">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Name Of State</label>
                        <input name="state" id="state" class="form-control" type="text" data-rule-required="true" data-msg-required="Please Enter State">
                    </div>
                </div>
                  <div class="col-md-12">
                        <div class="contact-text-area">
                          <label>Comment*</label>
                          <textarea  name="comment" id="comment" class="form-control bottom-15" rows="3" data-rule-required="true" data-msg-required="Please Enter Comment "></textarea>
                        </div>
                    </div>

                <div id="contactDiv"></div>

                <div class="col-md-12">
                    <div class="mt-20 text-center">
                        <input type="submit" class="btn btn-register" id="addContact" value="Send Now" >
                    </div>
                </div>

                </form>

            </div>
        </div>
    </div>
</section>





