<section class="about-us-section vision-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				
				<h2 class="common-heading wow fadeInUp">OUR VISION & CORE VALUES MAKE US THE FIRST CHOICE</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				 <div class="">
					<h4 class="sub-heading  wow slideInRight">OUR VISION</h4>
					<p class="wow slideInLeft">To be the first and the most preferred enterprise support system for small and medium enterprises in India.</p>
					<p class="wow slideInLeft">To inspire, train, mentor and enable, individuals and organizations to dream big, and achieve the next level of desired success. </p>
				</div>
			</div>
		</div>
        <div class="row">
        	<div class="col-sm-12">
        		<h4 class="sub-heading wow slideInRight">OUR CORE VALUES</h4>
        	</div>
        </div>
		<div class="row">
			<div class="col-sm-6">
				 <div class="vision-content wow slideInLeft">
					
					<h4>Deliver more than promised</h4>
					<p>We shall always exceed client’s expectation in whatever we do. </p>
					<p>We shall go out of the way to support the client in achieving their goals.</p>
					<p>Customer delight should always be our standard practice.</p>
				</div>
              </div>
              <div class="col-sm-6">
				 <div class="vision-content wow slideInRight">
					<h4>Do what is best for the client</h4>
					<p>Every suggestion we give, and every decision we take, will focus on client’s success.</p>
					<p>Client’s best interest will always supersede personal gains, but without compromising our ethics and genuine rights.</p>
				</div>
             </div>
         </div>
         <div class="row">
			<div class="col-sm-6">
				 <div class="vision-content wow slideInLeft">
					<h4>Driven by benchmarking</h4>
					<p>We should always remind ourselves that ‘If we are not upgrading, we are downgrading’. We shall always be one among top three motivational training companies in India and continuously strive to become number one. Every action shall drive us towards this benchmark without any excuse. We shall introspect and consistently act to retain our position and upgrade our rank, fan following and reputation. </p>
				</div>
            </div>
            <div class="col-sm-6">
				 <div class="vision-content wow slideInRight">
					<h4>Do what we preach</h4>
					<p>Our behaviour should reflect our teachings. </p>
					<p>Our character and actions shall be an inspiration to others. </p>
					<p>Our promises shall be non-negotiable.</p>
				</div>
			</div>
		</div>
	</div>
</section>