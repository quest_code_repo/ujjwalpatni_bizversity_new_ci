
<section class="about-us-section">
	<div class="container">
		<div class="row">
           	  <div class="col-sm-12">
           	  	<h2 class="common-heading wow fadeInUp">About Us</h2>
           	  </div>
           </div>
           <div class="row">
              <div class="col-sm-6">
              	 <div class="wow slideInLeft">
              	 	<img src="<?php echo base_url()?>front_assets/images/other/Patni4885.png" class="img-responsive">
              	 </div>
              </div>
			  <div class="col-sm-6">
				<div class="intro-block wow slideInRight">
					<h4 class="sub-heading">Dr Ujjwal Patni</h4>
					<p>Dr Ujjwal Patni is an international trainer, motivational author and business coach. He had inspired and motivatedmore than 1 million of people throughout the globe byhis free motivational videos on life and business ‘The Ujjwal Patni Show”. This show is been uploaded weekly twice in his youtube channel, facebook page, mobile app and also broadcasted in whatsapp groups.</p>
					<p>Dr Patni is the only motivational speaker of India who has led 3 Guinness World Records. His youtube channel is been awarded with Gold Creator Award by Youtube, US for crossing 1 million subscribers in 2018. He has been felicitated with more than 15 awards nationally and internationally like TOP 10 Thinkers Award Indian Ranking 2014 by MTC, Bright Author Award, Kamal Patra, PtSundarlal Sharma RajyaAlankaran by the government and many more. </p>
                    <p>Dr Patni conducts life and business transforming training programs, workshops and seminars for SMEs, MSMEs, entrepreneurs and business owners PAN India.</p>

				</div>
			</div>
             <div class="col-sm-12 intro-block">
                    <p>500+ entrepreneurs has changed their way of doing business and living life after attending  training programs namely Excellence Gurukul, VIP, etc. He has shared stage with topmost celebrities, politicians, industrialists and social figures from different walks of life. He has been featured in topmost Indian newspapers for his achievements and many TV channels in different countries.A true leader, risk taker and dreamer, Dr Ujjwal Patni left his lucrative career of Dentistry and committed his life to inspire people and organizations to achieve their maximum potential. </p>
                    <p>Dr Patni’s life and business transforming products includes bestselling DVDs like How owners kill their own business, Business kirakshahetusaatkadam, Business ke 8 Ujjwal Mantra, Safal Logon kikarantikaariaadatein, 10 aadateinchuniyeaam se khasbaniye, 30 minatshandaarzindagikeliye, shradhanjali do safalta lo, Parenting - 10 paise se banimerizindagi, Public Speaking Mastery, Kaise bane safalVakta, Jeet ha haar, etc. </p>
                </div>
		</div>
	   <div class="his-lagecy-block">	
		<div class="row">
			<div>
				<h2 class="common-heading wow fadeInUp">His Legacy</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-7"> 
                      <ul class="lagecy-list wow slideInLeft">
                      	<li>7 books in 12 Indian languages and 2 foreign languages available in more than 28 countries</li>

                      	<li>More than one million copies of his books have been sold in in various countries like USA, UK, Srilanka, South Africa, Fiji, Mauritius, Kenya, Thailand, Pakistan, Bangladesh, Nepal etc.</li>

                      	<li>2000+ Motivational training programs conducted in 100 plus cities attended by millions of entrepreneurs, business owners and big dreamers.</li>

                      	<li>More than 500 articles, interviews and thoughts published in top newspapers and magazines of India</li>

                      	<li>His books are a part of recommended reading in more than ten journalism, media, management and engineering institutions like NIT, CSVTU etc.</li>

                        <li>His books have been made available in more than 5000 schools to motivate teachers as well as students by the government.</li>
                      </ul>
           	  	  </div>
           	  	  <div class="col-sm-5">
           	  	  	<div class="wow slideInRight">
              	 	<img src="front_assets/images/other/index.png" class="img-responsive">
              	 </div>
           	  	  </div>
           	  </div>
		</div>
	</div>
</section>



<section class="section-awards">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <h2 class="common-heading wow fadeInUp">AWARDS & CREDENTIALS</h2>
                </div>
                <div class="col-md-12">
                    <div class="top">
                        <h5>LED 3 GUINNESS WORLD RECORDS</h5>
                        <ul>
                            <li>Led Guinness World Record 2005</li>
                            <li>Led Guinness world record 2011</li>
                            <li>Led Guinness world record 2015</li>
                        </ul>
                        <img src="front_assets/images/other/seal.png">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="bottom">
                         <li class="wow lightSpeedIn">
                            <div class="txt">
                            Gold Creator Award 2018 by Youtube, US</div>
                            <img src="front_assets/images/icons/a1.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Best corporate trainer of India award, MTC Global</div>
                            <img src="front_assets/images/icons/a1.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Pandit Sunderlal Sharma state Literary award, Government of Chhattisgarh</div>
                            <img src="front_assets/images/icons/a2.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Speech Guru Award, Rotary International</div>
                            <img src="front_assets/images/icons/a3.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Lifetime achievement award, Indian Jaycees</div>
                            <img src="front_assets/images/icons/a4.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Excellence award, Lions International</div>
                            <img src="front_assets/images/icons/a5.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Young Icon of India award</div>
                            <img src="front_assets/images/icons/a6.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Gururatna national award</div>
                            <img src="front_assets/images/icons/a7.png">
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="bottom">
                         <li class="wow lightSpeedIn">
                            <div class="txt">
                            Top 10 Thinkers Award Indian Ranking 2014 by MTC</div>
                            <img src="front_assets/images/icons/a1.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Bright Author Award
                            <img src="front_assets/images/icons/a8.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            'Kamal Patra', the biggest award by Indian Jaycee
                            <img src="front_assets/images/icons/a9.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Bachelor in Dental Surgery (B.D.S.)
                            <img src="front_assets/images/icons/a10.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Master of Arts (M.A.) in Political Science.
                            <img src="front_assets/images/icons/a11.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Specialization in Consumer Protection.
                            <img src="front_assets/images/icons/a12.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Specialization in Human Rights.
                            <img src="front_assets/images/icons/a13.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Worked as a faculty for teacher training and upgradation program in various universitie
                            <img src="front_assets/images/icons/a14.png">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="educational-qualy-section">
        <div class="wht-expect-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                         <h2 class="common-heading pink-heading wow fadeInUp">Educational Qualifications:</h2>
                            <div class="row">
                                <div class="col-md-6  col-sm-12">
                                    <div class="expect-rule-list excellenc-list wow slideInRight" >
                                        <ul>
                                            <li>Bachelor in Dental Surgery (B.D.S.)</li>
                                            <li>Master of Arts (M.A.) in Political Science.</li>
                                            <li>Specialization in Consumer Protection.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6  col-sm-12">
                                    <div class="expect-rule-list excellenc-list wow slideInRight">
                                        <ul>
                                            <li>Specialization in Human Rights.</li>
                                            <li>Worked as a faculty for teacher training.</li>
                                            <li>upgradation program in various universities.</li>
                                        </ul>
                                    </div>
                                </div>                               
                            </div>
                    </div>
                </div>   
            </div>
       </div>
</section>



<section class="section-testimonial section-wrk-place">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-12">
                    <div id="owl-works">
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                    “It is an ISO Certified 9001:2008 company engaged in corporate training, consulting, executive coaching and publishing in the segment of personal and organizational excellence. The programs are organized for government and semi government organizations, corporate houses, private companies, educational institutions, Ngo's and individuals.”
                                    </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/ppp.jpg">
                                </div>
                               <!--  <div class="figure1">
                                    <img src="assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="assets/images/other/figure2.png">
                                </div> -->
                                <div class="name">Patni Personality Plus Pvt Ltd</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                “Medident India Books is a fast growing Indian Book Publishing Company that publishes books on Personality Development, Organizational Excellence, Career Building, Network Marketing, Sales, Motivation, Positive Thinking and Leadership. ”
                                </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/medident.jpg">
                                </div>
                               <!--  <div class="figure1">
                                    <img src="assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="assets/images/other/figure2.png">
                                </div> -->
                                <div class="name">Medident India Books</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                “Medident India came in light after achieving one of the most prestigious government training contracts for capacity building and motivational training of six thousand officers' of government in 2010. The company was preferred amongst some of the topmost international companies due to extremely tailored training design suitable for Indian scenario and a comprehensive follow-up program.”
                                </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/ppp.jpg">
                                </div>
                                <!-- <div class="figure1">
                                    <img src="assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="assets/images/other/figure2.png">
                                </div> -->
                                <div class="name">Medident India Training</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


