 <section class="about-us-section section-gallery">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                         <div class="">
                           <h2 class="common-heading wow fadeInUp">GALLERY</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-inner-content">
            <div class="container">
                <div class="panel panel-default panel-product-block">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#img-modal">
                                    <div class = "panel panel-default product-block product-top-block">
                                    <div class="product-img-block">
                                        <img src="front_assets/images/products/2.jpg" class="img-responsive product-img"/>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <div class = "panel panel-default product-block product-top-block">
                                    <div class="product-img-block">
                                        <img src="front_assets/images/products/3.jpg" class="img-responsive product-img"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class = "panel panel-default product-block product-top-block">
                                    <div class="product-img-block">
                                        <img src="front_assets/images/products/4.jpg" class="img-responsive product-img"/>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class = "panel panel-default product-block product-top-block">
                                    <div class="product-img-block">
                                        <img src="front_assets/images/products/5.jpg" class="img-responsive product-img"/>
                                       
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class = "panel panel-default product-block product-top-block">
                                    <div class="product-img-block">
                                        <img src="front_assets/images/products/6.jpg" class="img-responsive product-img"/>
                                       
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class = "panel panel-default product-block product-top-block">
                                    <div class="product-img-block">
                                        <img src="front_assets/images/products/2.jpg" class="img-responsive product-img"/>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


  <!-- Modal -->
<div id="img-modal" class="modal fade" role="dialog">
  <div class="modal-dialog gallery-img-modal">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
           <div class="gallery-full-img">
                <img src="front_assets/images/products/2.jpg" class="img-responsive product-img"/>
            </div>
      </div>
    </div>

  </div>
</div>      