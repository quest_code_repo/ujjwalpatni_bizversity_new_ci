<section class="profile-section about-us-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-login">
	              <div id="profile-nav-tabs">
	                <ul id="myTab" class="nav nav-tabs navbar-right">
	                    <li class="active"><a href="#english" data-toggle="tab" aria-expanded="true">English</a></li>
	                    <li class=""><a href="#hindi" data-toggle="tab" aria-expanded="false">Hindi</a></li>   
	                </ul>
	              </div>
	              <div id="myTabContent" class="tab-content">
	                 <div class="tab-pane fade active in" id="english">
	                     <div>
	                     	<h2 class="common-heading">Ujjwal Patni</h2>
	                     	 <ul class="main-list">
	                     	 	<li>Dr Ujjwal Patni is an International Trainer, Bestselling Author and Motivational Speaker.</li>
	                     	 	<li>He is only motivational speaker of India who has <b>Led3 Guinness World Records</b> and 15 other prestigious awards including 
	                     	 		<ul class="sub-list">
	                     	 			<li>Top 10 Thinker 2014 by MTC</li>
	                     	 			<li>Best Corporate Trainer of India</li>
	                     	 			<li>Pundit Sunderlal Sharma RajyaAlankaranand </li>
	                     	 			<li>Kamal Patra award makes him a true achiever.</li>
	                     	 		</ul>
	                     	 	</li>

	                     	 	<li><b>‘The Ujjwal Patni Show’</b> based on life and business is watched by more than one million Indians every week across <b>30 countries</b> on Youtube&Whatsapp. More than 1 Million people have subscribed him on different digital platforms and ‘Ujjwal Patni’ mobile app.</li>

	                     	 	<li>He has authored<b> 7 books</b> that are published in <b>12 Indian languages</b> and <b>2 foreign languages</b> that have sold more than 1 million copies in 28 countries. He has conducted more than 2000 training programs in more than <b>100 cities</b> nationally and internationally. </li>

	                     	 	<li>More than 2000+ programs have been organised in 100 plus cities in India and abroad attended by more than <strong>1 million audiences</strong>. Excellence Gurukul and VIP are his signature programs for entrepreneurs.</li>
	                     	 	<li>His famous books are Power Thinking, JeetYaaHaar – RahoTaiyaar, SafalVaktaSafalVyakti, Judo Jodo Jeeto, Great Words Win Hearts, etc.</li>

	                     	 	<li>His bestselling DVDs are How owners kill their own business, Business kirakshahetusaatkadam, 30 minatshaandarzindagikeliye, Shradhanjali do safalta lo, Public speaking mastery course, 10 aadateinchuniyeaam se khasbaniye, 10 paise se banimerizindagi, etc.</li>

	                     	 	<li>DrUjjwalPatni’s achievements in different fields are featured on various TV channel, premier newspapers and magazines in India and other countries.</li>
	                     	 </ul>

	                     	<h4 class="cont-link-heading">Connect With Ujjwal Patni:</h4>
								<p>
								    Subscribe Youtube : <a href="https://www.youtube.com/user/personality2009">
								        https://www.youtube.com/user/personality2009
								    </a>
								</p>
								<p>
								    Like Facebook Page: <a href="https://www.facebook.com/ujjwalpatni/">
								        https://www.facebook.com/ujjwalpatni/
								    </a>
								</p>
								<p>
								    Follow Twitter Handle: <a href="https://twitter.com/Ujjwal_Patni/">
								        https://twitter.com/Ujjwal_Patni/
								    </a>
								</p>
								<p>
								    Instagram: <a href="https://www.instagram.com/dr.ujjwal_patni/">
								        https://www.instagram.com/dr.ujjwal_patni/
								    </a>
								</p>
								<p>
								    Linkedin: <a href="https://in.linkedin.com/in/dr-ujjwal-patni">
								        https://in.linkedin.com/in/dr-ujjwal-patni
								    </a>

								</p>
								<p>
								    Download Mobile App: <a href="App: https://play.google.com/store/apps/details?id=com.ujjwal.glt020&hl=en">
								       App: https://play.google.com/store/apps/details?id=com.ujjwal.glt020&hl=en
								    </a>
								</p>
								<p>
								    Browse Website: <a href="http://www.ujjwalpatni.com">www.ujjwalpatni.com</a>
								</p>

	                     </div>
	                 </div>
	                 <div class="tab-pane fade" id="hindi">
                        <div class="hindi-profile">
                        	<h2 class="common-heading">Ujjwal Patni</h2>
                        	<ul class="main-list">
                        		<li>डॉ उज्ज्वल पाटनी एक अंतर्राष्ट्रीय ट्रेनर, मोटिवेशनल स्पीकर एवं बेस्ट्सेलिंगलेखक है। </li>

                        		<li>डॉ पाटनी को 3 गिनीज़ वर्ल्ड रिकार्ड और 15 से ज़्यादा प्रतिष्ठित पुरस्कारों से सम्मानित किया जा चुका है जिनमे प्रमुख है ‘बेस्ट कॉर्पोरेट ट्रेनर ऑफ इंडिया’,‘पंडित सुंदरलाल शर्मा राज्य अलंकरण’ (सरकार के द्वारा) एवं ‘कमल पत्र’ उन्हे एक अचीवर की पहचान देता है। </li>

                        		<li>यूट्यूब और व्हाट्स एप में बिज़नेस और जीवन पर आधारित “द उज्ज्वल पाटनी शो” को 1 मिलियन से ज़्यादा भारतीय 30 से ज़्यादा देशों में देखते हैं। 7 लाख से ज़्यादा लोगों ने अलग-अलग सोशल मीडिया प्लाट्फ़ोर्म्स और “उज्ज्वल पाटनी” मोबाइल एप में उन्हें सबस्क्राइब किया है। </li>


                        		<li>डॉ पाटनी की 7 पुस्तकें, 12 भारतीय भाषाओं एवं 2 विदेशी भाषाओं में प्रकाशित हुई हैं,जिसकी 1 मिल्यन से ज़्यादा प्रतियाँ 28 देशों में उपलब्ध कराई गयी है। </li>


                        		<li>डॉ पाटनी की प्रसिद्ध पुस्तकों में पवार थिंकिंग, जीत या हार – रहो तैयार, सफल वक्ता सफल व्यक्ति, जुड़ों जोड़ो जीतो, ग्रेट वर्ड्स वीन हर्ट्स, एक्सपायर बिफोर यू एक्सपायर, आदि सम्मिलित है। </li>

                        		<li>डॉ पाटनी ने 2000 से ज़्यादा मोटिवेशनल सेमीनारों को, 100 से ज़्यादा भारतीय एवं विदेशी शहरों में आयोजित किया गया है जिसमें 1 मिलियन से ज़्यादा श्रोता सम्मिलित हुये हैं। एक्सिलेन्स गुरुकुल एवं वीआईपी उनके प्रसिद्ध मोटिवेशनल कार्यक्रम हैं। </li>

                        		<li>डॉ पाटनी के विभिन्न क्षेत्रों में अर्जित किए गए उपलब्धियों को अनेकों टीवी चैनलों, प्रतिष्ठित समाचार पत्रों एवं पत्रिकाओं ने देश और विदेशों में प्रकाशित किया हैं। </li>

                        		<li>आप हमारी वैबसाइट <a href="http://www.ujjwalpatni.com">www.ujjwalpatni.com</a> के द्वारा डॉ पाटनी से जुड़ सकते हैं। </li>
                        	</ul>

                        	<h4 class="cont-link-heading">Connect With Ujjwal Patni:</h4>
								<p>
								    Subscribe Youtube : <a href="https://www.youtube.com/user/personality2009">
								        https://www.youtube.com/user/personality2009
								    </a>
								</p>
								<p>
								    Like Facebook Page: <a href="https://www.facebook.com/ujjwalpatni/">
								        https://www.facebook.com/ujjwalpatni/
								    </a>
								</p>
								<p>
								    Download Mobile App: <a href="App: https://play.google.com/store/apps/details?id=com.ujjwal.glt020&hl=en">
								       App: https://play.google.com/store/apps/details?id=com.ujjwal.glt020&hl=en
								    </a>
								</p>
								

                        </div>
                   </div>
                 </div>
               </div>
			</div>
		</div>
	</div>
</section>