<link href='front_assets/css/simplelightbox.min.css' rel='stylesheet' type='text/css'>


<section class="about-us-section profile-section">
    <div class="container">
        <div class="row">
             <div class="col-sm-12">
                 <div id="profile-nav-tabs">
                    <ul id="myTab" class="nav nav-tabs">
                      <li class="active"><a href="#all" data-toggle="tab" aria-expanded="true">ALL</a></li>
                      <li class=""><a href="#article" data-toggle="tab" aria-expanded="false">Articles</a></li> 
                      <li class=""><a href="#book-review" data-toggle="tab" aria-expanded="false">Book Reviews</a></li>
                      <li class=""><a href="#newspapr" data-toggle="tab" aria-expanded="false">NewsPaper</a></li> 
                    </ul>
                  </div> 
             </div>
        </div>
         <div id="myTabContent" class="tab-content media-tab-content">
               <div class="tab-pane fade" id="all">
                 <div class="row">
                   <div class="for-check">
                        <a href="front_assets/images/products/girahlaxmi.jpg" class="big">
                            <div class="col-sm-4">
                                 <div class="media-outer">
                                     <div class="media-img-block">
                                        <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                    </div>
                                 </div>
                                              
                            </div>
                        </a>
                         <a href="front_assets/images/products/girahlaxmi.jpg" class="big">
                            <div class="col-sm-4">
                                 <div class="media-outer">
                                     <div class="media-img-block">
                                        <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                    </div>
                                 </div>
                                              
                            </div>
                        </a>
                         <a href="front_assets/images/products/girahlaxmi.jpg" class="big">
                            <div class="col-sm-4">
                                 <div class="media-outer">
                                     <div class="media-img-block">
                                        <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                    </div>
                                 </div>
                                              
                            </div>
                        </a>
                        <div class="clear"></div>

                    </div>
                </div>
               </div>

                <div class="tab-pane fade" id="article">
                </div>
                
                 <div class="tab-pane fade in active" id="book-review">
                    <div class="row">
                          <div class="for-check">
                            <a href="front_assets/images/products/1.jpg" class="big">
                                <div class="col-sm-4">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                                  
                                </div>
                            </a>
                             <a href="front_assets/images/products/1.jpg" class="big">
                                <div class="col-sm-4">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                                  
                                </div>
                            </a>
                        </div>
                    </div>
                </div> 

                 <div class="tab-pane fade" id="newspapr">
                </div>     
           </div>
       
    </div>
</section>

<script src=""></script>
<script type="text/javascript" src="<?php echo base_url()?>front_assets/js/simple-lightbox.js"></script>
<script>
  $( document ).ready(function() {
        var $gallery = $('.active .row .for-check a').simpleLightbox();

        $gallery.on('show.simplelightbox', function(){
            console.log('Requested for showing');
        })
        .on('shown.simplelightbox', function(){
            console.log('Shown');
        })
        .on('close.simplelightbox', function(){
            console.log('Requested for closing');
        })
        .on('closed.simplelightbox', function(){
            console.log('Closed');
        })
        .on('change.simplelightbox', function(){
            console.log('Requested for change');
        })
        .on('next.simplelightbox', function(){
            console.log('Requested for next');
        })
        .on('prev.simplelightbox', function(){
            console.log('Requested for prev');
        })
        .on('nextImageLoaded.simplelightbox', function(){
            console.log('Next image loaded');
        })
        .on('prevImageLoaded.simplelightbox', function(){
            console.log('Prev image loaded');
        })
        .on('changed.simplelightbox', function(){
            console.log('Image changed');
        })
        .on('nextDone.simplelightbox', function(){
            console.log('Image changed to next');
        })
        .on('prevDone.simplelightbox', function(){
            console.log('Image changed to prev');
        })
        .on('error.simplelightbox', function(e){
            console.log('No image found, go to the next/prev');
            console.log(e);
        });
    });
</script>

