
<section class="about-us-section profile-section">
    <div class="container">
         <div class="row">
                    <div class="col-sm-12">
                         <div class="">
                           <h2 class="common-heading wow fadeInUp">Media</h2>
                        </div>
                    </div>
                </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="section-login">
                  <div id="profile-nav-tabs">
                    <ul id="myTab" class="nav nav-tabs">
                      <li class="active"><a href="#all" data-toggle="tab" aria-expanded="true">ALL</a></li>
                      <li class=""><a href="#article" data-toggle="tab" aria-expanded="false">Articles</a></li> 
                      <li class=""><a href="#book-review" data-toggle="tab" aria-expanded="false">Book Reviews</a></li>
                      <li class=""><a href="#newspapr" data-toggle="tab" aria-expanded="false">NewsPaper</a></li> 
                    </ul>
                  </div>
                  <div id="myTabContent" class="tab-content media-tab-content">
                     <div class="tab-pane fade active in" id="all">
                      <div class="row">
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                     
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                             <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                           <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="article">
                           <div class="row">
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                     
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                             <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                           <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                        </div>
                     </div>
                    <div class="tab-pane fade" id="book-review">
                       <div class="row">
                             <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                           <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                             <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                        </div>
                   </div>
                    <div class="tab-pane fade" id="newspapr">
                         <div class="row">
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                     
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                             <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                           <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                     <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/1.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" data-toggle="modal" data-target="#media-modal">
                                    <div class="media-outer">
                                         <div class="media-img-block">
                                            <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                                        </div>
                                     </div>
                                </a>
                            </div>
                        </div>
                   </div>
                 </div>
               </div>
            </div>
        </div>
    </div>
</section>



 <!-- Modal -->
<div id="media-modal" class="modal fade" role="dialog">
  <div class="modal-dialog media-img-modal">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
           <div id="owl-media">
                        <div class="item">
                          <div class="media-img">
                               <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                          </div>
                        </div>
                        <div class="item">
                          <div class="media-img">
                               <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                          </div>
                        </div>
                        <div class="item">
                            <div class="media-img">
                               <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                          </div>
                        </div>
                        <div class="item">
                            <div class="media-img">
                               <img src="front_assets/images/products/girahlaxmi.jpg" class="img-responsive product-img"/>
                          </div>
                        </div>
                    </div>
      </div>
    </div>

  </div>
</div>      