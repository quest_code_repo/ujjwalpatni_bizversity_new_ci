


<section class="about-us-section">
	<div class="container">
		<div class="row">
           	  <div class="col-sm-12">
           	  	<h2 class="common-heading wow fadeInUp">Ujjwal Patni International</h2>
           	  </div>
           </div>
           <div class="row">
              <div class="col-sm-4">
              	 <div class="wow slideInLeft patni-international-img">
              	 	<img src="front_assets/images/other/ujjwal-patni.png" class="img-responsive">
              	 </div>
              </div>
			  <div class="col-sm-8">
				<div class="intro-block wow slideInRight">
					<?php
						if(!empty($UjjwalInternational) && count($UjjwalInternational))
						{
							?>
							<p><?php
								echo $UjjwalInternational[0]->content;?></p>

							<?php
						}
						else
						{
							?>
							<h3>No Record Found</h3>
							<?php
						}
					?>
				</div>
			
			</div>
		</div>
    </div>
</section>

