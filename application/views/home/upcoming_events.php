<section class="about-us-section vision-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				
				<h2 class="common-heading wow fadeInUp">OUR Future Programs</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="upcoming-event-block">
					<table class="table">
						<tr>
							<th>Event Name</th>
							<th>Date</th>
							<th>Location</th>
							<th>Contact No.1</th>
							<th>Contact No.2</th>
							<th>Action</th>
						</tr>
						<tr>
							<td>
								<h4 class="text-center">VIP</h4>
								
								<img src="front_assets/images/other/vip-logo.png" class="img-responsive">
							</td>
							<td>
								12 July 2018
							</td>
							<td>
								Delhi
							</td>
							<td>
								098 765 4321
							</td>
							<td>
								098 765 4321
							</td>
							<td>
								<a href="" class="btn btn-register">REGISTER NOW</a>
							</td>
						</tr>
						<tr>
							<td>
								<h4 class="text-center">Excellence Gurukul</h4>
								<img src="front_assets/images/other/excellence-logo.png" class="img-responsive">
							</td>
							<td>
								12 July 2018
							</td>
							<td>
								Delhi
							</td>
							<td>
								098 765 4321
							</td>
							<td>
								098 765 4321
							</td>
							<td>
								<a href="" class="btn btn-register">REGISTER NOW</a>
							</td>
						</tr>
					</table>
					
			</div>
		</div>
       
	</div>
</section>