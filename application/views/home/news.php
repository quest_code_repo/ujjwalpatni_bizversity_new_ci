

<section class="section-blogs section-blog-list">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
              <div class="row">
              	<div class="col-sm-12">
              		<div>
              			<h2 class="common-heading wow fadeInUp">News</h2>
              		</div>
              	</div>
              </div>	
              <div class="row">
              	<div class="col-sm-4">
                     <div class="panel panel-default panel-blog">
                        <div class="panel-heading">
                            <a href="javascript:void(0);"><img src="front_assets/images/other/blog1.jpg"></a>
                            <div class="panel-heading-overlay">
                                <a href=""><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <h4 class="heading-blogg news-heading"><a href="">News Heading</a></h4>

                            
                             <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 06 Jun 18</p>
                          
                            <div class="clearfix"></div>
                            <div class="content-blog">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,                                           
                                	<br><a href="">Read More</a>
                                </p>
                            </div>
                           
                        </div>
                    </div>
                </div>
               	<div class="col-sm-4">
                     <div class="panel panel-default panel-blog">
                        <div class="panel-heading">
                            <a href="javascript:void(0);"><img src="front_assets/images/other/blog1.jpg"></a>
                            <div class="panel-heading-overlay">
                                <a href=""><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <h4 class="heading-blogg news-heading"><a href="">News Heading</a></h4>

                            
                             <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 06 Jun 18</p>
                          
                            <div class="clearfix"></div>
                            <div class="content-blog">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,                                           
                                	<br><a href="">Read More</a>
                                </p>
                            </div>
                           
                        </div>
                    </div>
                </div>
                	<div class="col-sm-4">
                     <div class="panel panel-default panel-blog">
                        <div class="panel-heading">
                            <a href="javascript:void(0);"><img src="front_assets/images/other/blog1.jpg"></a>
                            <div class="panel-heading-overlay">
                                <a href=""><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <h4 class="heading-blogg news-heading"><a href="">News Heading</a></h4>

                            
                             <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 06 Jun 18</p>
                          
                            <div class="clearfix"></div>
                            <div class="content-blog">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,                                           
                                	<br><a href="">Read More</a>
                                </p>
                            </div>
                           
                        </div>
                    </div>
                </div>
              </div>
          </div>   
        </div>
    </div>
</section>

 