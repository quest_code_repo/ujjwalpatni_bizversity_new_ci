
<section class="course-main border-course about-us-section course-overlay">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div>
          <img src="<?php echo base_url();?>uploads/products/<?php echo $cource_details[0]->image_url; ?>" class="img-responsive">
        </div>
      </div>
      <div class="col-sm-8">
        <div class="common-heading">
          <h4><?php echo $cource_details[0]->pname?></h4>
           <p>
              <?php
                  if(!empty($StarRating) && count($StarRating)>0)
                  {
                      ?>
                        <div class="starRating1">
                          <div>
                            <div>
                                <div>
                                  <div>
                                      <input id="rating6" type="radio"  value="1" <?php echo ($StarRating[0]->rating_star == "1")?'checked':'';?>>
                                      <label for="rating6"><span></span></label>
                                  </div>
                                  <input id="rating7" type="radio"  value="2" <?php echo ($StarRating[0]->rating_star == "2")?'checked':'';?>>
                                  <label for="rating7"><span></span></label>
                                </div>
                                <input id="rating8" type="radio"  value="3" <?php echo ($StarRating[0]->rating_star == "3")?'checked':'';?>>
                                <label for="rating8"><span></span></label>
                            </div>
                            <input id="rating9" type="radio"  value="4" <?php echo ($StarRating[0]->rating_star == "4")?'checked':'';?>>
                            <label for="rating9"><span></span></label>
                          </div>
                          <input id="rating10" type="radio"  value="5" <?php echo ($StarRating[0]->rating_star == "5")?'checked':'';?>>
                          <label for="rating10"><span></span></label>
                      </div>  
                      <?php
                  }
                  else
                  {
                      ?>
                       <div class="starRating1">
                          <div>
                            <div>
                                <div>
                                  <div>
                                      <input id="rating6" type="radio"  value="1">
                                      <label for="rating6"><span></span></label>
                                  </div>
                                  <input id="rating7" type="radio"  value="2">
                                  <label for="rating7"><span></span></label>
                                </div>
                                <input id="rating8" type="radio" nam value="3">
                                <label for="rating8"><span></span></label>
                            </div>
                            <input id="rating9" type="radio"  value="4">
                            <label for="rating9"><span></span></label>
                          </div>
                          <input id="rating10" type="radio" value="5">
                          <label for="rating10"><span></span></label>
                      </div>   
                      <?php
                  }
              ?>
           


            <span class="edit-rating"><a href=""  data-toggle="modal" data-target="#myModalrating">Edit Your Rating</a></span>
          </p>

            <!-- Modal -->
            <div class="modal fade" id="myModalrating" role="dialog">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title heading-main">Rating</h4>
                  </div>
                  <div class="modal-body clearfix rating-modal">
                     
                <form id="rating_form" name="rating_form" action="<?php echo base_url('dashboard/new_star_rating')?>" method="post">
                  <input type="hidden" name="course_id" value="<?php echo $cource_details[0]->product_id; ?>"  />
                  <div class="modal-body clearfix rating-modal text-center">
                      <div class="starRating1">
                          <div>
                            <div>
                                <div>
                                  <div>
                                      <input id="rating6" type="radio" name="rating" value="1">
                                      <label for="rating6"><span></span></label>
                                  </div>
                                  <input id="rating7" type="radio" name="rating" value="2">
                                  <label for="rating7"><span></span></label>
                                </div>
                                <input id="rating8" type="radio" name="rating" value="3">
                                <label for="rating8"><span></span></label>
                            </div>
                            <input id="rating9" type="radio" name="rating" value="4">
                            <label for="rating9"><span></span></label>
                          </div>
                          <input id="rating10" type="radio" name="rating" value="5">
                          <label for="rating10"><span></span></label>
                      </div>   

                      <div class="text-center">
                        <button type="submit" class="btn btn-register" onclick="return check_rating();">Submit</button>
                      </div>

                  </div>
              </form>
                  </div>
                </div>
              </div>
            </div>
                  
          <div class="row">
            <div class="col-sm-10">
              <p class="progress-count"><?php echo isset($completed_lectures[0]->completed_lectures)?$completed_lectures[0]->completed_lectures:'0'?> of <?php echo isset($Total_lectures[0]->total_lectures)?$Total_lectures[0]->total_lectures:''?> items complete</p>
              <div class="progress">

                <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo isset($completed_lectures[0]->completed_lectures)?$completed_lectures[0]->completed_lectures:'0'?>"
                  aria-valuemin="<?php echo isset($completed_lectures[0]->completed_lectures)?$completed_lectures[0]->completed_lectures:'0'?>" aria-valuemax="<?php echo isset($Total_lectures[0]->total_lectures)?$Total_lectures[0]->total_lectures:''?>" style="width:<?php echo isset($completed_lectures[0]->completed_lectures)?$completed_lectures[0]->completed_lectures:'0'?>%">
                  <span class="sr-only">10% Complete</span>
                </div>

              </div> 
            </div>
            <div class="col-sm-2 pl-0">
              <div class="price-cup">
              </div>
            </div>
          </div>
        </div>
     </div>
    </div> 
  </div>
</section>

<section class="course-main">
  <div class="container">
     <div class="row">
      <div class="col-sm-12">
           <div class="course-detail-heading">
            <ul class = "nav nav-tabs">
               <li class = "active"><a href = "dashboard/cource_details?product_id=<?php echo $pro_id;?>">Overview</a></li>
               <li><a href = "dashboard/course_content?product_id=<?php echo $pro_id;?>">Course Content</a></li>
            </ul>
            <hr>
          </div>
       </div>
       <!-- <div class="col-sm-12">
           <div class="course-detail-heading">
            <ul id = "myTab" class = "nav nav-tabs">
               <li class = "active"><a href = "#course1" data-toggle = "tab">Overview</a></li>
               <li><a href = "#course2" data-toggle = "tab">Course Content</a></li>
            </ul>
          </div>
       </div> -->
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="course-detail-heading">

  
                  <div class="tab-content-1">
                    
                    <div class="tab-about">
                      <h3>About this course</h3>
                    <p><?php echo $cource_details[0]->short_disc?></p>
                    </div>

                    <div class="tab-about">
                      <div class="row">
                        <div class="col-sm-2">
                          <p>By the numbers</p>
                        </div>


                        <div class="col-sm-4">
                           <p>Lectures: <?php echo isset($Total_lectures[0]->total_lectures)?$Total_lectures[0]->total_lectures:''?></p>
                       </div>
                         <!-- <div class="col-sm-4">
                           <p>Video: 12 hours</p>
                        </div>
 -->

                    </div>


                    </div>

                    <div class="tab-about">
                      <div class="row">
                        <div class="col-sm-2">
                           <p>Description</p>
                        </div>
                        <div class="col-sm-9">
                           <p><?php echo isset($cource_details[0]->description)?$cource_details[0]->description:''?></p>
                        </div>
                    </div>
                    </div>

                    <?php
                        if(!empty($Instructor) && count($Instructor)>0)
                        {
                            ?>
                            <div class="tab-about">
                      <div class="row">
                         <div class="col-sm-12">
                           <h3>About the Instructor</h3>
                           <?php
                              foreach($Instructor as $instDetails)
                              {
                                  ?>
                              <div class="row">
                               <div class="col-sm-1">
                                  <img src="<?php echo base_url('uploads/instructor/'.$instDetails->instructor_image.'')?>" class="img-circle" style="height:64px;">
                               </div>
                               <div class="col-sm-11">
                                  <h4 class="Instructor-heading"><?php echo $instDetails->instructor_name;?></h4>
                                 
                               </div>
                             </div>
                               <div>
                                   <div class="text-left share-icon">
                                   <ul> 
                                    <?php
                                        if(!empty($instDetails->facebook_link))
                                        {
                                            ?>  
                                            <li class="fb"><a target="_blank" href="<?php echo $instDetails->facebook_link;?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <?php
                                        }
                                    ?>

                                    <?php
                                        if(!empty($instDetails->twitter_link))
                                        {
                                          ?>
                                            <li class="tw"><a target="_blank" href="<?php echo $instDetails->twitter_link;?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                          <?php
                                        }
                                    ?>
                                   
                                   <?php
                                        if(!empty($instDetails->google_link))
                                        {
                                            ?>
                                            <li class="gplus"><a target="_blank" href="<?php echo $instDetails->google_link;?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <?php
                                        }
                                   ?>
                                 
                                 <?php
                                    if(!empty($instDetails->youtube_link))
                                    {
                                        ?>
                                        <li class="yt"><a target="_blank" href="<?php echo $instDetails->youtube_link;?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                        <?php
                                    }
                                 ?>
                                   
                                   
                                   </ul>
                                </div>
                               </div>
                               <div>
                                 <p><?php echo $instDetails->instructor_description;?></p>
                               </div>

                            </div>
                                  <?php
                              }
                           ?>

                            


                         </div>
                      </div>
                            <?php
                        }
                    ?>
                     


                     </div>
                 
        </div>
     </div>
  </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">


<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
   acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
           panel.style.display = "none";
       } else {
           panel.style.display = "block";
       }
   });
}
</script>

<script type="text/javascript">

    function chapter_autocomplete(){
       jQuery('.chapter_info').autocomplete({
              source: '<?php echo base_url()?>dashboard/get_chapter_autocomplete/?chapter='+$('#chapter_name').val()+'&course_id='+$('#course_id').val(),
            minLength: 1,
            select: function(event, ui) {

              jQuery('.chapter_info').val(ui.item.pname); 
              jQuery('#chapter_id').val(ui.item.chapter_id); 
              
            }
        });
    }
</script>

<script type="text/javascript">
  function get_chapter()
  {
    $.ajax({
              type :'POST',
              url  :'<?php echo base_url("dashboard/ajax_chapter_list")?>',
              data : $('#chapter_form').serialize(),
              success:function(resp)
              {
                  resp = resp.trim();
                  $('#chapterResult').html(resp);
              } 

    })
  }
</script>


<script type="text/javascript">
  function check_rating()
  {
      var rate = $('input:radio[name=rating]:checked').val();

      if(rate == "" || rate==undefined)
      {
          alert("Please Select Stars To Rate This Course");
          return false;
      }
  }
</script>
