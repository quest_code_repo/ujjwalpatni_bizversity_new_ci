<style type="text/css">
  .tab-content-2.new{margin: 15px 0 15px 15px;}
  .sidenav .closebtn.new{top: 43px;right: 0px;}
</style>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn new" id="close-side" onclick="closeNav()">&times;</a>
  <div class="course-detail-heading">
    <div class="tab-content">
       <?php
                                  if(!empty($Chapter) && count($Chapter)>0)
                                  {
                                    $i=1;
                                      foreach($Chapter as $chapResult)
                                      {

                                          $chapterLecture = get_chapter_lecture_acc_to_course($chapResult->ch_id,$chapResult->sem_id);

                                          ?>
                            <div class="tab-pane active" id="tab1"><div class="tab-content-2 new">
                                 <button class="heading accordion">
                                    <span class="pull-left">
                                    <h5 class="text-left">Section: <?php echo $i;?></h5>  
                                    <h4><?php echo $chapResult->ch_name;?></h4>
                                    </span>
                                     <span class="pull-right">

                                      <?php

                                          $total_lectures = get_total_lecture_of_particular_chapter($chapResult->ch_id,$chapResult->sem_id);
                                      ?>

                                      <h5><b><?php echo isset($total_lectures[0]->total_lectures)?$total_lectures[0]->total_lectures .'/'. $total_lectures[0]->total_lectures:'0/0'?></b></h5>
                                    </span>
                                 
                                 </button>

                                 <div class="content panel" style="display: none;">

                                    <?php
                                        if(!empty($chapterLecture) && count($chapterLecture)>0)
                                        {

                                            $j = 1;
                                            foreach($chapterLecture as $lectDetails)
                                            {
                                                  if($lectDetails->lecture_type == 1)
                                                  {

                                                        if($lectDetails->lect_id == $ActiveLecture)
                                                        {
                                                            $active ="active";
                                                        }
                                                        else
                                                        {
                                                            $active = "";
                                                        }

                                                      ?>
                                                    <li class="<?php echo $active;?>">
                                                        <a href="<?php echo base_url('dashboard/video_course/'.$chapResult->ch_id.'/'.$lectDetails->lect_id.'/'.$chapResult->sem_id.'')?>">
                                                        <span><i class="fa fa-play" aria-hidden="true"></i></span>
                                                        <span><?php echo $j;?>. <?php echo $lectDetails->lect_name;?></span>
                                                        <span class="pull-right">
                                                           <span><?php echo $lectDetails->lect_duration;?></span>
                                                           <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                                        </span> 
                                                         </a>
                                                      </li>
                                                      <?php
                                                     
                                                  }

                                                  else if($lectDetails->lecture_type == 3)
                                                  {
                                                      ?>
                                                <li>
                                                   <a href="">
                                                      <span><i class="fa fa-play" aria-hidden="true"></i></span>
                                                      <span><?php echo $j;?>. <?php echo $lectDetails->lect_name;?></span>
                                                      </a>
                                                      <span class="pull-right">
                                                         <span><?php echo $lectDetails->lect_duration;?></span>
                                                         <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                                      </span>
                                                 </li>
                                                <li class="sub-list">
                                                   <a href="<?php echo base_url('uploads/semester/'.$lectDetails->lect_resource.'')?>">
                                                      <p><i class="fa fa-download" aria-hidden="true"></i>&nbsp;<?php echo $lectDetails->lect_resource;?></p>
                                                   </a>
                                                 </li>


                                                      <?php
                                                      $j++;
                                                  }
                                            }
                                        }

                                        else
                                        {
                                              ?>
                                                <h3>No Lecture Found</h3>
                                              <?php
                                        }
                                    ?>
                                  

                                </div>


                             </div>
                           </div>
                                          <?php
                                          $i++;
                                      }
                                  }
                              ?>
      </div>
  </div>
</div>
<div id="main">
 <div class="navbar-side-btn">
         <span class="opennav-video" onclick="openNav()" id="vidio-play-btn">&#9776; &nbsp;1. <?php echo $LectureVideo[0]->lect_name;?></span>
        
    </div>
<!-- <a href="<?=$_SERVER['HTTP_REFERER'];?>" class="go-to-back" ><span>&times;</span></a> -->
<a href="<?php echo base_url('dashboard/my_courses')?>" class="go-to-back" ><span>&times;</span></a>
<iframe width="100%" height="600" frameborder="0" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" src="<?php echo $LectureVideo[0]->lect_video_url;?>" id="ankitankit"></iframe>


<!-- <iframe width="100%" height="600" frameborder="0" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" src="https://player.vimeo.com/video/<?php echo $LectureVideo[0]->lect_video_url;?>" id="ankitankit"></iframe> -->
<!-- <video id="Video1" controls="" style="width: 100%; height: 100vh;">
<source src="assets/video/ujjwal-patni.mp4" type="video/mp4">
</video> -->
</div>
<script type="text/javascript" src="front_assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
function openNav() {
document.getElementById("mySidenav").style.width = "390px";
document.getElementById("main").style.marginLeft = "390px";
document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}
function closeNav() {
document.getElementById("mySidenav").style.width = "0";
document.getElementById("main").style.marginLeft= "0";
document.body.style.backgroundColor = "white";
}
</script>
<!-- <script>
$(document).ready(function(){
$("#header-course1").click(function(){
$("#hideshow-content1").slideToggle(1000);
});
});
</script> -->
<script>
$(".opennav-video").click(function(){
$(".opennav-video").hide(200);
});
</script>
<script>
$(".closebtn").click(function(){
$(".opennav-video").show(200);
});
</script>
<script>
$('#vidio-play-btn').click(function () {
if ($("#Video1").get(0).paused) {
$("#Video1").get(0).play();
} else {
$("#Video1").get(0).pause();
}
});
</script>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
   acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
           panel.style.display = "none";
       } else {
           panel.style.display = "block";
       }
   });
}
</script>