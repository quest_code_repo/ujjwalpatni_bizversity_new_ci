
<!-- Simple Section html -->
<section class="section-form change-pass">
    <div class="container">
        <div class="row">
            <form id="password_form" name="password_form" method="post">
            <div class="col-md-6 col-md-offset-3">
                <div class="col-sm-12">
                    <h2 class="common-heading wow fadeInUp">Change Password</h2>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Current Password</label>
                        <input type="password" name="password" id="dash_password" class="form-control" data-rule-required="true" data-msg-required="Please Enter Current Password">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" name="new_password" id="dash_password_new_password" class="form-control" data-rule-required="true" data-msg-required="Please Enter New Password">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="confirm_password" id="dash_password_confirm_password" class="form-control" data-rule-required="true" data-msg-required="Please Enter Confirm Password">
                    </div>
                </div>

                <div id="corpDiv"></div>

                <div class="col-sm-12 text-center">
                    <input type="submit" class="btn btn-register" id="chPass" value="Submit" >
                </div>
            </div>
            </form>
        </div>
    </div>
</section>

