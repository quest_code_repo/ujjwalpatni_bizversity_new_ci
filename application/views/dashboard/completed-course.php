<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="user-cours-list wow slideInLeft">
           <nav class="nav-sidebar">
              <ul class="nav tabs">
                <li ><a href="my-courses">My Courses</a></li>
                <li class="active"><a href="completed-course">Completed Courses</a></li>
                <li><a href="my-result">My Results</a></li>
                <li><a href="purchased-courses">Manage Group Users</a></li>
                <li><a href="group-result">My Group Results</a></li>
              </ul>
            </nav>
        </div>
      </div>
      <div class="col-sm-9">
        <div class="complete-course-section commn-head-adjust">
          <div class="row">
             <div class="col-sm-12">
                <h2 class="common-heading wow fadeInUp">Completed Courses</h2>
                <p class="text-center">You don't have any completed courses right now.</p>
             </div>
          </div>
          <div class="row">

            <?php
                if(!empty($CompletedCourses) && count($CompletedCourses)>0)
                {
                    foreach($CompletedCourses as $cource)
                    {
                        ?>
              <div class="col-sm-5 col-sm-offset-1">
                  <a href="cource-details?product_id=<?php echo base64_encode($cource->product_id);?>">
                    <div class = "panel panel-default">
                       <div class = "panel-heading">
                          <img src="<?php echo base_url();?>uploads/products/<?php echo $cource->image_url;?>" class="img-responsive">
                       </div>
                       <div class = "panel-body">
                        <h4 class="title"><?php echo $cource->pname; ?></h4>
                        <p><?php
                          $string = strip_tags($cource->short_disc);
                          if (strlen($string) > 80) {
                            // truncate string
                            $stringCut = substr($string, 0, 80);
                            $endPoint = strrpos($stringCut, ' ');
                            //if the string doesn't contain any space then it will cut without word basis.
                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                          }
                          echo $string;
                          ?></p>
                           
                            <div>                           
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                            </div>
                      </div>
                 </div>
                  </a>
                </div>
                        <?php
                    }
                }
                else
                {
                    ?>
                     <div class="col-sm-4 col-sm-offset-4">
                        <div>
                          <img src="front_assets/images/icons/enrollments_empty_state_complete.png" class="img-responsive">
                        </div>
                    </div>
                    <?php
                }
            ?>

           
          </div>
        </div>
        <hr>
      </div>
    </div>
  </div>
</section> 