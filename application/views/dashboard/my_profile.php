
<!-- Simple Section html -->
<section class="section-form change-pass">
    <div class="container">
        <div class="row">
         <form name="profile_form" id="profile_form" action="dashboard/update_profile" method="post">
            <div class="col-md-6 col-md-offset-3">
                <div class="col-sm-12">
                    <h2 class="common-heading wow fadeInUp">My Profile</h2>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="dashboard_first_name" id="dashboard_first_name" class="form-control" value="<?php echo isset($MyProfile[0]->f_name)?$MyProfile[0]->f_name:'';?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="dashboard_last_name" id="dashboard_last_name" class="form-control" value="<?php echo isset($MyProfile[0]->l_name)?$MyProfile[0]->l_name:'';?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="dashboard_username" id="dashboard_username" class="form-control" value="<?php echo isset($MyProfile[0]->username)?$MyProfile[0]->username:'';?>" readonly>
                    </div>
                </div>
                <div class="col-sm-12 text-center">
                    <input type="submit" class="btn btn-register" value="Submit">
                </div>
            </div>
        </form>
        </div>
    </div>
</section>


