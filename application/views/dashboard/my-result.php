<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="user-cours-list wow slideInLeft">
           <nav class="nav-sidebar">
              <ul class="nav tabs">
                <li><a href="my-courses">My Courses</a></li>
                <li><a href="completed-course">Completed Courses</a></li>
                <li class="active"><a href="my-result">My Results</a></li>
                <li><a href="purchased-courses">Manage Group Users</a></li>
                <li><a href="group-result">My Group Results</a></li>
              </ul>
            </nav>
        </div>
      </div>
      <div class="col-sm-9">
        <div class="group-result-section commn-head-adjust">
              <div class="row">
           <div class="col-sm-12">
              <h2 class="common-heading wow fadeInUp">My Results</h2>
           </div>
       </div> 
    <div class="row">
      <div class="col-sm-12">
        <!-- <div class="filter-selection">
          <div class="row">
            <div class="col-sm-2 col-sm-offset-10">
              <div class="form-group">
              <label for="sel1">Filter:</label>
              <select class="form-control" id="sel1">
                <option>Course Name</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </select>
            </div>
            </div>
          </div>
        </div> -->
        <div class="row">

          <?php
              if(!empty($MyResult) && count($MyResult)>0)
              {
                  foreach($MyResult as $myDetails)
                  {
                      ?>
         <div class="col-sm-6">
             <div class="group-result-inner">
                <div class="row">

                   <?php
                        $courseName = get_course_name_acc_to_chapter($myDetails->tr_chapter_id);
                    ?>

                  <div class="col-sm-3">
                    <img src="<?php echo base_url('uploads/products/'.$courseName[0]->image_url.'')?>" class="img-responsive">
                  </div>
                  <div class="col-sm-5">
                    <h4 class="sub-heading"></h4><?php echo isset($courseName[0]->pname)?$courseName[0]->pname:''?></h4>
                    <p><?php echo $myDetails->tr_test_name;?></p>
                    <h4 class="marks">marks: <?php echo $myDetails->tr_my_score;?> / <?php echo $myDetails->tr_total_score;?></h4>
                  </div>
                  <div class="col-sm-4">
                     <h3 class="text-center">Percentage</h3>
                    <div class="progress yellow">
                            <span class="progress-left">
                                <span class="progress-bar"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar"></span>
                            </span>

                          <?php
                            if($myDetails->tr_my_score !=0 && $myDetails->tr_total_score !=0)
                            {
                                $percentage = ($myDetails->tr_my_score / $myDetails->tr_total_score) * 100;
                            }
                            else
                            {
                              $percentage = 0;
                            }   
                            ?>

                            <div class="progress-value"><?php echo number_format($percentage,2);?>%</div>
                        </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                       <a href="<?php echo base_url()?>summary_analysis?test_id=<?php echo $myDetails->tr_test_id;?>" button="" class="btn btn-default btn-register add-user-btn">View Analysis</a>
                    </div>
                </div>
              </div>
          </div>
                      <?php
                  }
              }

              else
              {
                  ?>
                  <h2>No Record Found</h2>
                  <?php
              }
          ?>

          
       

        </div>
        
      </div>
     </div>
        </div>
      </div>
    </div>
  </div>
</section>
