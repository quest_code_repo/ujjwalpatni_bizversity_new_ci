<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="user-cours-list wow slideInLeft">
          <nav class="nav-sidebar">
            <ul class="nav tabs">
              <li class="active"><a href="my-courses">My Courses</a></li>
              <li><a href="completed-course">Completed Courses</a></li>
              <li><a href="my-result">My Results</a></li>
              <li><a href="purchased-courses">Manage Group Users</a></li>
              <li><a href="group-result">My Group Results</a></li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="col-sm-9">
          <div class="my-courses-section commn-head-adjust">
            <div class="row">
                 <div class="col-sm-12">
                     <h2 class="common-heading wow fadeInUp">My Courses</h2>
                 </div>
            </div>
      </div>
      <div class="all-Courses-section">
        <div class="row">
          <?php
          if (!empty($user_cources)) {
             foreach ($user_cources as $cource) {
          ?>
          <div class="col-sm-5 col-sm-offset-1">
            <a href="cource-details?product_id=<?php echo base64_encode($cource->product_id);?>">
              <div class = "panel panel-default">
                 <div class = "panel-heading">
                    <img src="<?php echo base_url();?>uploads/products/<?php echo $cource->image_url;?>" class="img-responsive">
                 </div>
                 <div class = "panel-body">
                  <h4 class="title"><?php echo $cource->pname; ?></h4>
                  <p><?php
                    $string = strip_tags($cource->short_disc);
                    if (strlen($string) > 80) {
                      // truncate string
                      $stringCut = substr($string, 0, 80);
                      $endPoint = strrpos($stringCut, ' ');
                      //if the string doesn't contain any space then it will cut without word basis.
                      $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                    }
                    echo $string;
                    ?></p>
                      <!-- <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70"
                           aria-valuemin="0" aria-valuemax="100" style="width:70%">
                        </div>
                      </div>
                      <div>
                        <span class="progress-report">65% complete</span>
                      </div> -->

                      <?php
                          $starRating = get_star_rating_for_my_courses($cource->product_id);

                          if(!empty($starRating))
                          {
                              $rating = round($starRating[0]->overall_avg);
                              ?>  
                     <div class="starRating1">
                          <div>
                            <div>
                                <div>
                                  <div>
                                      <input id="rating6" type="radio"  value="1" <?php echo ($rating == "1")?'checked':'';?>>
                                      <label for="rating6"><span></span></label>
                                  </div>
                                  <input id="rating7" type="radio"  value="2" <?php echo ($rating == "2")?'checked':'';?>>
                                  <label for="rating7"><span></span></label>
                                </div>
                                <input id="rating8" type="radio"  value="3" <?php echo ($rating == "3")?'checked':'';?>>
                                <label for="rating8"><span></span></label>
                            </div>
                            <input id="rating9" type="radio"  value="4" <?php echo ($rating == "4")?'checked':'';?>>
                            <label for="rating9"><span></span></label>
                          </div>
                          <input id="rating10" type="radio"  value="5" <?php echo ($rating == "5")?'checked':'';?>>
                          <label for="rating10"><span></span></label>
                      </div> 
                              <?php
                          }
                          else
                          {
                              ?>
                       <div class="starRating1">
                          <div>
                            <div>
                                <div>
                                  <div>
                                      <input id="rating6" type="radio"  value="1" >
                                      <label for="rating6"><span></span></label>
                                  </div>
                                  <input id="rating7" type="radio"  value="2" >
                                  <label for="rating7"><span></span></label>
                                </div>
                                <input id="rating8" type="radio"  value="3" >
                                <label for="rating8"><span></span></label>
                            </div>
                            <input id="rating9" type="radio"  value="4" >
                            <label for="rating9"><span></span></label>
                          </div>
                          <input id="rating10" type="radio"  value="5" >
                          <label for="rating10"><span></span></label>
                      </div> 
                              <?php
                          }
                      ?>
                      

                </div>
           </div>
            </a>
          </div>
          <?php }
        }
        else
        {
           ?>
              <h2>No Record Found</h2>
           <?php            
        }
        ?>
        </div>
      </div> 
      </div>
    </div>
  </div>
</section>

