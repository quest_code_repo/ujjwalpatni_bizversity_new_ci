<style type="text/css">
  .sec-coming{
    padding: 80px 0;
    background-color: #C74724;
    text-align: center;
}
.sec-coming h2{font-family: 'Heavitas';font-size: 28px;color: #fff;margin: 30px 0;}
.sec-coming .logo-img img{max-width: 100%;max-height: 120px;}
.sec-coming .countdown-wrapper{padding: 20px 0 0;}
.sec-coming #countdown-text {font-family: 'Heavitas';margin: 0;}        
.sec-coming .countdown {font-size: 40px;color: #EDD8A0;}
.sec-coming .counter-text {font-size: 20px;color: #fff;}
.sec-coming .timer{display: inline-block;margin: 0 20px;}
@media (max-width: 768px){
    .sec-coming .counter-text {font-size: 16px;}
    .sec-coming .countdown {font-size: 25px;}
    .sec-coming .timer{ margin: 0 10px;}
    .sec-coming .countdown-wrapper {padding: 30px 0 60px;}
    .sec-coming h2 {font-size: 20px;}
    .sec-coming .logo-img img {max-height: 100px;}
}
</style>

<!-- Header html end -->
<section class="sec-coming">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="logo-img"><img src="front_assets/images/other/logo-white.png"></div>
                <h2>We are coming soon</h2>
                <div class="countdown-wrapper">
                    <div id="countdown-text">
                      <div class="timer">
                        <div id="daysTicker" class="countdown"></div>
                        <div class="counter-text">DAYS</div>
                      </div>
                      <div class="timer">
                        <div id="hoursTicker" class="countdown"></div>
                        <div class="counter-text">HOURS</div>
                      </div>
                      <div class="timer">
                        <div id="minsTicker" class="countdown"></div>
                        <div class="counter-text">MINS</div>
                      </div>
                      <div class="timer">
                        <div id="secsTicker" class="countdown"></div>
                        <div class="counter-text">SECS</div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</section>



<script type="text/javascript">
    // coming soon timer

    // Set the date we're counting down to
    var countDownDate = new Date("June 10, 2018 12:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get todays date and time
      var now = new Date().getTime();

      // Find the distance between now an the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000); 
      
      //Zeros
    var hours = (hours.toLocaleString(undefined,{minimumIntegerDigits: 2}));  
      
    var minutes = (minutes.toLocaleString(undefined,{minimumIntegerDigits: 2}));
      
    var seconds = (seconds.toLocaleString(undefined,{minimumIntegerDigits: 2}));
      
     
      
      // Display the result in the element with id="demo"
      document.getElementById("daysTicker").innerHTML = days;
      document.getElementById("hoursTicker").innerHTML = hours;
      document.getElementById("minsTicker").innerHTML = minutes;
      document.getElementById("secsTicker").innerHTML = seconds;
      
      // If the count down is finished, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "THE CAMPAIGN BEGINS";
      }
    }, 1000);
</script>