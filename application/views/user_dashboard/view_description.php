<section class="section-online-dcs">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
      <?php
      
      if (!empty($question_details)) { 
        foreach ($question_details as $key => $value) {              
      ?>
        <div class="tab-quetion tab-answer">
          <p><strong>Question <?php echo $key+1;?> :-  </strong> <?php echo strip_tags($value['question_desc'],'<img><p><sub></sub><sup></sup><br>');?></p><br>
          <?php
          if (!empty($value['question_image'])) {
            ?>
            <p><img width="150px;" src="<?php echo base_url();?>uploads/test_question_images/<?php echo $value['question_image']; ?>"></p>
          <?php } ?>
          <?php
          if ($value['question_type']==2) {

            $user_id = $this->session->userdata('id');
            $user_option= get_users_ans_multiple_option($user_id,$value['ques_id']);
            if(!empty($user_option)){
              $usr_sub_mul = '';
              $submited_val = '';
              $submited_img_val = '';
              $explode_val = explode(',',$user_option[0]['answer']);
              foreach($explode_val as $key => $answer){

                $submited_option = get_submit_user_ques_ans($value['ques_id']);
               
                $submited_val .= $submited_option[0]['ans_'.$answer];

                if (!empty($submited_option[0]['ans_'.$answer.'_image'])) {

                  $submited_val .= "<img width='100px;' src=".base_url()."uploads/test_questions_ans_images/".$submited_option[0]['ans_'.$answer.'_image'].">";
                }
                
                if ($answer == 1) {
                  $usr_ans = 'A';
                }
                if ($answer == 2) {
                  $usr_ans = 'B';
                }
                if ($answer == 3) {
                  $usr_ans = 'C';
                }
                if ($answer == 4) {
                  $usr_ans = 'D';
                }

                $usr_sub_mul .= $usr_ans.',';
              }
              $user_option = rtrim($usr_sub_mul,','); 
            }
            ?>
            
            <?php
              $correct_ans = get_correct_answers_multiple($value['ques_id']);
              $user_id = $this->session->userdata('id');
              $user_ans = get_correct_answers_single_option($user_id,$value['ques_id']);
              if ($correct_ans == $user_ans) {
                $mark_class = 'QBYQ_questioncorrect';
                $ans = 'Correct';
                $right_ans_is = '';
              }else{
                $mark_class = 'QBYQ_questionIncorrect';
                $ans = 'Wrong';
                $right_ans_is = $correct_ans;
              }
            ?>

            <?php 
            if (!empty($user_option) || !empty($submited_val)) { ?>
              <p><strong>Your Submitted Option :- <?php echo $user_option;?></strong> </p>
              <p><strong>That is :- <?php echo $submited_val;?></strong> 
              <p><span class="<?php echo $mark_class;?>"><font class="answerMsg" color="#20d16a"> <?php echo $ans;?> Answer.</font></span>
            </p>
            <?php }else{ ?>
            
              <p><strong>Not Attempted</strong></p>

            <?php } ?>

            <?php
              if(!empty($right_ans_is)){
                $quesright_ans = '';
                $explode_val = explode(',',$right_ans_is);
                foreach($explode_val as $answer){
                  if ($answer == 1) {
                    $right_ans = 'A';
                  }
                  if ($answer == 2) {
                    $right_ans = 'B';
                  }
                  if ($answer == 3) {
                    $right_ans = 'C';
                  }
                  if ($answer == 4) {
                    $right_ans = 'D';
                  }

                  $quesright_ans .= $right_ans.',';
                }
                $corr_ans = rtrim($quesright_ans,','); 
                
            ?>    
              <p><strong>Correct Answer Will Be Option :- <?php echo $corr_ans; ?></strong></p>
              <p><strong>That Will Be :- <?php echo multiple_view_descriptive($value['ques_id']);?></strong></p>

            <?php } ?>


          <?php }else{ 
            // echo "<pre>";print_r($question_details);die;
            $user_id = $this->session->userdata('id');
            $answer=get_correct_answers_single_option($user_id,$value['ques_id']);
            // echo $answer;die;
            if (!empty($answer)) {
              if ($answer == 1) {
                $single_option = 'A';
              }elseif ($answer == 2) {
                $single_option = 'B';
              }elseif ($answer == 3) {
                $single_option = 'C';
              }elseif ($answer == 4) {
                $single_option = 'D';
              }
            }

            if ($answer == $value['ques_right_ans']) {
              $mark_class = 'QBYQ_questioncorrect';
              $ans = 'Correct';
              $right_ans_is = '';
            }else{
              $mark_class = 'QBYQ_questionIncorrect';
              $ans = 'Wrong';
              $right_ans_is = $value['ques_right_ans'];
            }
          ?>

          <?php
            if (!empty($answer)) {
          ?>    
            <p><strong>Your Submitted Option :- <?php echo $single_option; ?></strong></p>
            <p><strong>That is :- 
            <?php
              $submited_ans = strip_tags($value['ans_'.$answer],'<img><p><sub></sub><sup></sup><br>');
              echo $submited_ans;
            ?></strong> 
            <?php
              if (!empty($value['ans_'.$answer.'_image'])) {
                ?>
                <p><img width="150px;" src="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $value['ans_'.$answer.'_image']; ?>"></p>
              <?php } ?>
            </p>

            <p><span class="<?php echo $mark_class;?>"><font class="answerMsg" color="#20d16a"> <?php echo $ans;?> Answer.</font></span></p>

          <?php }else{ ?>
            <p><strong>Not Attempted</strong></p>

          <?php } ?>
                    
            <?php
              if (!empty($right_ans_is)) {
                if ($right_ans_is == 1) {
                  $right_ans = 'A';
                }elseif ($right_ans_is == 2) {
                  $right_ans = 'B';
                }elseif ($right_ans_is == 3) {
                  $right_ans = 'C';
                }elseif ($right_ans_is == 4) {
                  $right_ans = 'D';
                }
            ?>    
             <p><strong>Correct Answer Will Be Option :- <?php echo $right_ans; ?></strong></p>
             <p><strong>That Will Be :- 
                <?php 
                  $right_will = strip_tags($value['ans_'.$right_ans_is],'<img><p><sub></sub><sup></sup><br>');
                  echo $right_will;
                ?>
            </strong>
            <?php
              if (!empty($value['ans_'.$right_ans_is.'_image'])) {
                ?>
                <p><img width="150px;" src="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $value['ans_'.$right_ans_is.'_image']; ?>"></p>
              <?php } ?>
            </p>

            <?php } ?>

          <?php } ?>

          
            <p><strong>Explaination :- </strong><?php echo strip_tags($value['explain_right_answer'],'<img><p><sub></sub><sup></sup><br>');?></p>
        </div>
      <?php }} ?>

      </div>
    </div>
  </div>
</section>

