
<!DOCTYPE html>
<html>
    <head>
        <title>Ujjwal Patni</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


      
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>exam_assets/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>exam_assets/css/bootstrap-social.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>exam_assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>exam_assets/css/animate.css" rel="stylesheet"/>
        <!-- owl CSS -->
        <link href="<?php echo base_url(); ?>exam_assets/css/owl.theme.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>exam_assets/css/owl.transitions.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>exam_assets/css/owl.carousel.css" rel="stylesheet"/>
        <!-- hover-min CSS -->
        <link href="<?php echo base_url(); ?>exam_assets/css/hover-min.css" rel="stylesheet"/>
        <!-- yamm css -->
        <link href="<?php echo base_url(); ?>exam_assets/css/yamm.css" rel="stylesheet"/>
        <!-- Fonts icons -->
        <link href="<?php echo base_url(); ?>exam_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- loader css -->
        <link href="<?php echo base_url(); ?>exam_assets/css/loader.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>exam_assets/css/nice-select.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>exam_assets/css/jQuery-plugin-progressbar.css" rel="stylesheet" type="text/css">
        <!-- video popup CSS -->
        <link href="<?php echo base_url(); ?>exam_assets/css/jquery.popup.css" rel="stylesheet" type="text/css">

        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>exam_assets/css/custom.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>exam_assets/custom_css/custom.css" rel="stylesheet"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <base href="<?=base_url();?>">
        <script src="assets/js/jquery-1.11.3.min.js"></script>

          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
             <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

             <script type="text/javascript" async
src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML&locale=en">
</script>
             <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <style>
            .dropbtn {
              border: medium none;
              color: #ffffff;
              cursor: pointer;
              font-size: 16px;
              padding: 0;
            }

            .dropdown2 {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown2:hover .dropdown-content {
                display: block;
                text-decoration: none;
            }
        </style>

    </head>
    <body>

        <!-- loader html -->
       <!--  <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two" style="left:20px;"></div>
                    <div class="object" id="object_three" style="left:40px;"></div>
                    <div class="object" id="object_four" style="left:60px;"></div>
                    <div class="object" id="object_five" style="left:80px;"></div>
                </div>
            </div>
        </div> -->
        <!-- loader html end -->


        <!-- Header html -->
        <header class="header-main header-analysis">
            <div class = "navbar navbar-default yamm main-menu  header-inner" role = "navigation">
                <div class="container-fluid">
                    <!-- <div class = "navbar-header">
                        <button type = "button" class = "navbar-toggle" data-toggle = "collapse" data-target = "#navbar-collapse">
                            <span class = "sr-only">Toggle navigation</span>
                            <span class = "icon-bar"></span>
                            <span class = "icon-bar"></span>
                            <span class = "icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="<?php echo base_url(); ?>">
                            <img src="<?php echo base_url(); ?>assets/images/logo.png"/>
                        </a>
                    </div> -->
                   <div class="row">
                        <div class = "navbar-header">
                        <a class="navbar-brand logo" href="<?php echo base_url(); ?>">
                            <img src="<?php echo base_url()?>front_assets/images/other/logo.png"/>
                        </a>
                    </div>
                    <div class = "collapse navbar-collapse" id = "navbar-collapse">
                        <ul class = "nav navbar-nav navbar-right menu2">
                             <li class="dropdown new-biz-dropdown">
                                    <a href="javascript:void(0)" class="dropbtn user-pic"><img src="assets/images/other/profile.jpg" class="img-responsive" style="height: 40px;"></a>
                                    <ul class="dropdown-content profil-dropdown">
                                        <li>
                                            <div class="media">
                                              <div class="media-left">                                     
                                                <img src="assets/images/other/profile.jpg" class="media-object" style="height: 45px;">
                                              </div>

                                             

                                              <?php
                                                  if(!empty($this->session->userdata('id')))
                                                  {                              
                                              ?>
                                              <div class="media-body">
                                                <h5 class="media-heading"><?php echo ($this->session->userdata('username'));?></h5>
                                                <p><?php echo ($this->session->userdata('full_name'));?></p>
                                              </div>
                                                    <?php
                                                  }
                                                  else
                                                  {
                                                    ?>
                                              <div class="media-body">
                                                <h5 class="media-heading">Test</h5>
                                                <p>Test@gmail.com</p>
                                              </div>
                                                    <?php
                                                  }
                                              ?>
                                            
                                            </div>
                                        </li>
                                        <li><a href="my-courses"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Dashboard</a></li>
                                        <li><a href="dashboard/my_profile"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Profile</a></li>
                                        <li><a href="mentoring/coming_soon"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp; Change Password</a></li>
                                        <li><a href="<?php echo base_url('home/logout')?>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; Log Out</a></li>
                                     </ul>
                                </li> 
                        </ul>
                    </div>
                   </div>
                </div>
            </div>

        </header>
        

