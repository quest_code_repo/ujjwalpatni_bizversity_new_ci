
<section class="section-analysis section-test-info">
<div class="container main-container">
<div class="row">

<div class="total-test">
	<div class="col-xs-4 p-0">
		<div class="total-info">
			Total Tests
		</div>
	</div>
	<div class="col-xs-2 p-0">
		<div class="total-no">
			<?=count($test_count);?>
		</div>
	</div>
	<div class="col-xs-4 p-0">
		<div class="total-solved">
			Solved Tests
		</div>
	</div>
	<div class="col-xs-2 p-0">
		<div class="total-solve-no">
			<?=count($given_test_list);?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="analysis-box" style="display: none;">
<div class="col-sm-3">
  <div class="student-info">
    <?php if (!empty($student_record[0]['image_name'])) { ?>
    <img class = "media-object"  src="<?php echo base_url();?>uploads/user-image/<?php echo $student_record[0]['image_name']; ?>" alt = "Media Object" style="margin: 0 auto;">
    <?php }else{ ?>
    <img class = "media-object"  src="<?php echo base_url();?>assets/default_gravtar.png" alt = "Media Object" style="margin: 0 auto;">
    <?php }?>
    <div class="response"><?=$this->session->userdata('full_name')?></div>
    <div class="response"><b>Class :</b> <?=$course_name?></div>
    
  </div>
</div>
<div class="col-sm-9">
  <div class="analysis-point">
    <div class="col-sm-6 col-xs-6">
      <div class="student-details">
        <h2 class="heading-black">Total Test</h2>
        
        <div class="energy-number"><?=count($test_count);?></div>
        <!-- <div class="progress">
          <div class="progress-bar bg-info bg-red2" role="progressbar" style="width: <?=@$tot4?>%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
          </div> -->
        </div>
      </div>
      <div class="col-sm-6 col-xs-6">
        <div class="student-details">
          <h2 class="heading-black">Solved Test</h2>
          
          <div class="energy-number color-green"><?=count($given_test_list);?></div>
          <!-- <div class="progress">
            <div class="progress-bar bg-info bg-green2" role="progressbar" style="width: <?=@$tot_watch?>%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div> -->
          </div>
        </div>
       
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>


<div class="row mt-20">
	<div class="col-xs-12  p-0">
		<div class="pending-info">
			<h4>Pending Tests</h4>
			

        <?php 
           
            if(!empty($tests)){
             foreach ($tests as $key => $test) { ?>

          <div class="pending-test">
            
				  <div class="student-test-name">
          
          <a href="javascript:void(0);" style="text-decoration: none;pointer-events: none;">
            <?=$test['test_name'];?></a>

          </div>

        <div class="total-marks">
        <span class="total-marks">Total Marks: <b><?=$test['total_marks'];?></b></span>
        <span class="total-duration">Time Duration: <b><?=$test['ideal_time_duration'];?> Min</b></span>
        </div>
        <div class="start-time">
        Test will Start on <b>
        <?php 
        $time_get = @$test['appear_time'];
        $dis_time_get = @$test['disappear_time'];

        $fin_appear_time = date('d.m.Y H:i',strtotime($time_get));
        echo $fin_appear_time; ?> Hrs
  
        </b> & End On 
        
        <b>

        <?php $fin_disappear_time = date('d.m.Y H:i',strtotime($dis_time_get));
        echo $fin_disappear_time;?> Hrs
          
        </b>
        </div>


        </div>

        <div class="start-btn">
        <a href="front_end/exam_control/exam_instruction/?eid=<?=base64_encode($test['test_id']);?>" class="btn btn-start">Start Test</a>
        </div>
        <div class="clearfix"></div>
        <hr>

        <?php }}else{ ?>
        Sorry! There is no test scheduled today.
        <?php } ?>
			
			<div class="clipboard">
				<img src="assets/images/icons/clipboards.png">
			</div>
	</div>

</div>
</div>
<div class="row mt-20">
	<div class="col-xs-12  p-0">
		<div class="pending-info">
			<h4>Solved Tests</h4>

      <?php if(!empty($given_test_list)){ 

        foreach ($given_test_list as $key => $value) {
              if ($value['tr_total_que']==0) {
                $value['tr_total_que']=1;
              }
              if ($value['tr_attempt_que']==0) {
                $value['tr_attempt_que']=1;
              }
              $final_precent=($value['tr_right_ans']/$value['tr_total_que'])*100;
              $accuracy_precent=($value['tr_right_ans']/$value['tr_attempt_que'])*100;

        ?>
			<div class="pending-test">

				<div class="student-test-name"><?=ucfirst($value['tr_test_name']);?></div>
				<div class="total-marks">
					<span class="total-marks">Total Marks: <b><?php echo mult_marks_tots($value['tr_test_id']);?></b></span>
					<span class="total-duration">Total Duration: <b><?php echo mult_times_tots($value['tr_test_id']);?> Minutes</b></span>
				</div>
				<div class="start-time">
				Test was on <b><?php echo mult_dates_tots($value['tr_test_id']);?></b>
				</div>
			</div>
			<div class="start-btn">
				<a href="summary_analysis/?test_id=<?=$value['tr_test_id']; ?>" class="btn btn-analysis">View Analysis</a>
			</div>
			<div class="clearfix"></div>
			<hr>

      <?php }}else{ ?>
        Test Not Available.
        <?php } ?>
			<div class="clipboard">
				<img src="assets/images/icons/clipboards.png">
			</div>
	</div>
</div>
</div>


<!-- Expired Test -->

<div class="row mt-20">
  <div class="col-xs-12  p-0">
    <div class="pending-info">
      <h4>Expired Tests</h4>
      
        <?php 
           
            if(!empty($tests_expires)){
             foreach ($tests_expires as $key => $test) { ?>

          <div class="pending-test">
            
          <div class="student-test-name">
          
          <a href="javascript:void(0);" style="text-decoration: none;pointer-events: none;">
            <?=$test['test_name'];?></a>

          </div>

        <div class="total-marks">
        <span class="total-marks">Total Marks: <b><?=$test['total_marks'];?></b></span>
        <span class="total-duration">Time Duration: <b><?=$test['ideal_time_duration'];?> Min</b></span>
        </div>
        <div class="start-time">
        Test was started on <b>
        <?php 
        $time_get = @$test['appear_time'];
        $dis_time_get = @$test['disappear_time'];

        $fin_appear_time = date('d.m.Y H:i',strtotime($time_get));
        echo $fin_appear_time; ?> Hrs
  
        </b> & Ended On 
        
        <b>

        <?php $fin_disappear_time = date('d.m.Y H:i',strtotime($dis_time_get));
        echo $fin_disappear_time;?> Hrs
          
        </b>
        </div>


        </div>

        
        <div class="clearfix"></div>
        <hr>

        <?php }}else{ ?>
        Sorry! There is no any expired test.
        <?php } ?>
      
      <div class="clipboard">
        <img src="assets/images/icons/clipboards.png">
      </div>
  </div>

</div>
</div>






<!-- END -->

  <div class="body-txt" style="width: 100%; display: none">                      
    <div class="top">
        <img src="assets/images/instruction_ico.png">
        <b>Full Length Test.</b>
    </div>
    <div class="bottom">
        <ol>
            <?php 
            // echo "<pre>";
            // print_r($tests);
            // print_r($demo_tests);
            // echo "</pre>";
            if(!empty($tests)){
             foreach ($tests as $key => $test) { ?>
            
            <li><a href="front_end/exam_control/exam_instruction/?eid=<?=base64_encode($test['test_id']);?>">
            <?=$test['test_name'];?></a></li>
            <?php }}else{ ?>
            Sorry! There is no test scheduled today.
            <?php } ?>
        </ol>   
    </div>
  </div>


  <?php if(!empty($given_test_list)){ ?>
  <div class="row mt-20" style="display: none">
    <div class="col-sm-12 p-0 col-analysis">
      <h4 class="heading-black">Recently Solved Test</h4>
      <!-- <span class="view"><a href="">View All</a></span> -->
      <div class="table-responsive table-analysis">
        <table class="table table-bordered table-board table-practice">
          <tbody>
            <?php
            foreach ($given_test_list as $key => $value) {
              if ($value['tr_total_que']==0) {
                $value['tr_total_que']=1;
              }
              if ($value['tr_attempt_que']==0) {
                $value['tr_attempt_que']=1;
              }
              $final_precent=($value['tr_right_ans']/$value['tr_total_que'])*100;
              $accuracy_precent=($value['tr_right_ans']/$value['tr_attempt_que'])*100;
            
            ?>
              <tr>
                <td width="20%"><div class="sheetno color-blue text-right"><h4><?=ucfirst($value['tr_test_name']);?></h4></div></td>
                <td width="16%"><div class="chapterno">Overall <?=number_format($final_precent, 2, '.', '');?>%</div></td>
                <td width="35%" class="accuracy-bar">
                  <div class="accuracy">Accuracy</div>
                  <div class="progress">
                    <div class="progress-bar bg-info pro-green" role="progressbar" style="width: <?=$accuracy_precent;?>%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <span class="number1">0</span>
                  <span class="number2"><?=number_format($accuracy_precent, 2, '.', '');?>%</span>
                </td>
                <!-- <td width="8%"><a href="exam?eid=NDY=&amp;sem_id=MA==&amp;re=1" class="btn btn-retake"><img src="assets/images/icons/retake.png"> Retake</a></td> -->
                <td width="8%">
                  <a href="summary_analysis/?test_id=<?=$value['tr_test_id']; ?>" class="btn btn-view">View Analysis <img src="assets/images/icons/view.png"></a></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php 
          // echo "<pre>"; print_r($given_test_list); ?>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
</section>
