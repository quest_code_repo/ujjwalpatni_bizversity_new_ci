<section class="section-summray">
 		<div class="container">
      	<div class="row">
          	<div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default panel-summary">
                    <div  class="panel-heading">
                      Here is how you have performed in <b><?=ucfirst(@$testdata[0]['test_name']);?></b> test taken on <b><?=@$testtime[0]['date']?></b>
                    </div>
                      <div class="panel-body">
                        <div class="circle-score score1 wow fadeIn" >
                          <img src="<?php echo base_url(); ?>exam_assets/images/icons/trophy2.png">
                          <div class="score-no"><?php echo ($marks_gained).'/'; 
                            if(!empty($total_ques_marks))
                            {
                              echo $total_ques_marks;
                            }else{
                              echo "0";
                            }
                          ?></div>
                          <div class="score-title">SCORE</div>
                        </div>
                        <div class="circle-score score2 wow fadeIn">
                          <img src="<?php echo base_url(); ?>exam_assets/images/icons/favorites-button.png">
                          <div class="score-no">
                            <?php 
                            if($question_attemp==0)
                            {
                              echo 0;
                            }else{
                              $accurracy = ($correct_ans / $question_attemp) * 100;
                              echo number_format($accurracy, 2, '.', '').'%'; 
                            }
                            ?>
                          </div>
                          <div class="score-title">ACCURACY</div>
                        </div>
                        <div class="circle-score score3 wow fadeIn" >
                          <img src="<?php echo base_url(); ?>exam_assets/images/icons/like.png">
                          <div class="score-no">
                          <?php 
                          // $total_ques = count($question_count); 
                          $attempted_ques = $question_attemp;
                          $attempt_percent =  $attempted_ques/$total_question;
                          $attmpt_ratio = $attempt_percent*100;
                          echo number_format($attmpt_ratio, 2, '.', '').'%';
                          ?>
                        </div>
                        <div class="score-title">ATTEMPT<br>PERCENTAGE</div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
              </div>
          </div>

          <div class="row mt-20">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-3 question-box box-1">
                    <div class="question-info">
                    <span class="pull-left q-no"><?php echo $total_question; ?></span>
                      <span class="pull-right q-no">Total Questions</span>
                      <div class="clearfix"></div>
                      </div>
                  </div>
                  <div class="col-md-3 question-box box-2">
                  	<div class="question-info">
                  	<span class="pull-left q-no"><?php echo $question_attemp;?></span>
                      <span class="pull-right q-no">Attempted</span>
                      <div class="clearfix"></div>
                      </div>
                  </div>
                  <div class="col-md-3 question-box box-3">
                  	<div class="question-info">
                      <span class="pull-left q-no"><?php echo $correct_ans; ?></span>
                      <span class="pull-right q-no">Correct</span>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3 question-box box-4">
                  	<div class="question-info">
                  	<span class="pull-left q-no"><?php echo $question_attemp-$correct_ans; ?></span>
                      <span class="pull-right q-no">Incorrect</span>
                      <div class="clearfix"></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row mt-20">
          	<div class="col-md-10 col-md-offset-1">
              	<div class="col-md-6 pl-0">
                  	<div class = "media media-avg">
                         <a class = "pull-left" href = "javascript:void(0);">
                            <img class = "media-object"  src="<?php echo base_url(); ?>exam_assets/images/icons/timer.png" alt = "Media Object">
                         </a>
                         
                         <div class = "media-body">
                            <h4 class = "media-heading">Average Time / Questions</h4>
                            <div class="timer">
                              <?php 
                              if($question_attemp==0)
                              {
                                echo '00:00:00';
                              }
                              else{
                                $total_time = $total_time_taken;
                                $get_time =  $total_time/$question_attemp;
                                
                                echo gmdate("H:i:s", $get_time*60);
                              }
                              ?>
                            </div>
                         </div>
                      </div>
                  </div>
                  <div class="col-md-6 pr-0">
                  	<div class = "media media-avg">
                         <a class = "pull-left" href = "javascript:void(0);">
                            <img class = "media-object"  src="<?php echo base_url(); ?>exam_assets/images/icons/discount.png" alt = "Media Object">
                         </a>
                         
                         <div class = "media-body">
                            <h4 class = "media-heading">Percentage Score</h4>
                            <div class="timer">
                            <?php 
                            if(!empty($total_question))
                            {
                              if (!empty($marks_gained)) {
                                $percent = (($marks_gained) / $total_ques_marks) * 100;
                                echo number_format($percent, 2, '.', '').'%';
                              }else{
                                echo "0";
                              }
                              
                            } else{
                              echo "0";
                            }
                            ?>
                              
                            </div>
                         </div>
                      </div>
                  </div>
              </div>
          </div>
           
          <div class="row mt-20">
            <div class="col-md-10 col-md-offset-1">
                <div  class="box-analysis twoAnalysisBox noShadow">
                  	<div class="col-md-6 wow bounceInLeft">
                      	<div class="panel panel-primary dept-analysis text-center">
                        	<h4 class="panel-heading">In Depth Analysis</h4>
                            
                            <a href="<?php echo base_url(); ?>depth_analysis/?test_id=<?php echo $test_id; ?>"class="btn btn-blue">Click to view</a>
                        </div>
                      </div>
                      
                      <div class="col-md-6  wow bounceInRight"> 
                      	<div class="panel panel-primary dept-analysis text-center">
                        	<h4 class="panel-heading">Solution Description</h4>
                            
                            <a href="<?php echo base_url();?>view_description/?test_id=<?php echo $test_id; ?>"><button class="btn btn-blue">Click to view</button></a>
                        </div>
                      </div>
                      <!-- <div class="col-md-4">
                      	<div class="dept-analysis text-center last-dept">
                        	<h4>Video Solution</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a href="<?php echo base_url();?>test_video_description/?test_id=<?php echo $test_id; ?>"><button class="btn btn-blue">Click to view</button></a>
                        </div>
                      </div> -->
                      
                      <div class="clearfix"></div>
  
                      <!-- <div class="buttonPanelBot wow slideInDown"><a href="summary_analysis/?test_id=<?php echo $test_id; ?>" class="btn.large btn-gradient">Click on Summery Page</a></div> -->
                  </div>
              </div>
          </div>
          
      </div>
      <div class="row mt-20">      </div>
      <div class="row mt-20">      </div>
 </section>
 <section class="purchase">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="purchase-txt text-center">
                    <p></p>
                      <!-- <a href="detail_analysis/?test_id=<?=$this->input->get('test_id');?>" class="btn btn-blue dt-analysis">Detailed Analysis</a> -->
                    <!-- <button class="btn btn-blue">BUY NOW</button> -->
                </div>
            </div>
        </div>
    </div>        
</section>
