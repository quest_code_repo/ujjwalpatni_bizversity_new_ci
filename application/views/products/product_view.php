
<!-- Slider Section html -->
<section class="section-banner product-banner">
    <img src="front_assets/images/banners/banner.png" class="img-responsive"/>
    <div class="banner-txt wow slideInLeft">
    	<h1 class="text-center bnner-head">Products</h1>
        <h3>To conqure life</h3>
    </div>
</section>

<section class="products-section">
	<div class=" container-wide">
		<div class="row">
			<div class="col-sm-2 p-0">
				<div class="left-category">
					<ul>
                        

						<li><h4 class="products-heading">Category</h4></li>

						<?php
								if(!empty($Product_category) && count($Product_category)>0)
								{
									if(!empty($this->input->get()))
									{
										$pro_cat = $this->input->get('product_category');
									}
									else
									{
										$pro_cat = "";
									}
										

										foreach($Product_category as $catDetails)
								     {
								     	$total = get_category_total_count($catDetails->id);

								     		if($catDetails->id == $pro_cat)
								     		{
								     			$checked ="checked";
								     		}
								     		else
								     		{
								     			$checked = "";
								     		}

								     	?>
								     		<li>
													<div class="radio">
												        <label><input type="radio" name="product_category" value="<?php echo $catDetails->id;?>" onclick="get_filter_value();" <?php echo $checked;?>><?php echo $catDetails->name;?> </label><span><?php echo isset($total[0]->total_count)?$total[0]->total_count:'0'?></span>
												   </div>
					                        </li>
								     	<?php
								}
							}
						?>
						
					
					</ul>

					<!-- <ul>
						<li><h4 class="products-heading">Language</h4></li>
						<?php
								if(!empty($Product_languages) && count($Product_languages)>0)
								{

									if(!empty($this->input->get()))
									{
										$pro_languages = $this->input->get('product_languages');
									}
									else
									{
										$pro_languages = "";
									}

									$final_pro = explode(',',$pro_languages);


									foreach($Product_languages as $proLang)
									{
											if(in_array($proLang->product_language_id,$final_pro))
											{
												$checked1 = "checked";
											}
											else
											{
												$checked1 = "";
											}

										?>
										<li><div class="checkbox"><label><input type="checkbox" name="product_languages[]"  value="<?php echo $proLang->product_language_id;?>" onclick="get_filter_value();" <?php echo $checked1;?>><?php echo $proLang->product_language_name;?></label></div></li>
										<?php
									}

								}
						?>
                         
                         
					</ul> -->
				</div>
			</div>
			<div class="col-sm-8">
				<div>
			     <!--  <h4 class="products-heading">Products<span>Sort By A-Z</span></h4> -->

                   <div class="row">

                   		<?php
                   			if(!empty($Products) && count($Products)>0)
                   			{
                   					foreach($Products as $pDetails)
                   					{
                   							?>
				                   <div class="col-sm-4">
				                   	  	 <div class="products-block">
				                   	  	 	<img src="<?php echo base_url('uploads/products/'.$pDetails->image_url)?>" class="img-responsive">
				                   	  	 	<h4><?php echo $pDetails->pname;?></h4>
				                   	  	 	<h1 class='product_detail'>
				                   	  	 		
				                   	  	 	 <?php 
				                   	  	 			if($pDetails->disc_price !=0)
				                   	  	 			{
				                   	  	 				?>
				                   	  	 				<span><i class="fa fa-inr" area-hidden="true"></i> <span class="price-cut"><?php echo $pDetails->product_price;?></span> <span class="actual_price"><?php echo $pDetails->disc_price;?></span></span>
				                   	  	 				<?php
				                   	  	 			}
				                   	  	 			else
				                   	  	 			{
				                   	  	 				?>
				                   	  	 				<span class="actual_price"><i class="fa fa-inr" area-hidden="true"></i> <?php echo $pDetails->product_price;?></span>
				                   	  	 				<?php
				                   	  	 			}
				                   	  	 		?>
				                   	  	 		 

				                   	  	 		 <?php

				                   	  	 		 $UserId = $this->session->userdata('id');

				                   	  	 		 $finalSessionid = $this->session->userdata('session_id');

				                   	  	 		 if(!empty($UserId))
				                   	  	 		 {
				                   	  	 		 	$finalUserId = $UserId;
				                   	  	 		 }
				                   	  	 		 else
				                   	  	 		 {
				                   	  	 		 	$finalUserId = $finalSessionid;
				                   	  	 		 }

				                   	  	 		 	
				                   	  	 		 	$ProCheck = check_product_in_cart($pDetails->product_id,$finalUserId);

				                   	  	 		 		if(!empty($ProCheck) && count($ProCheck)>0)
				                   	  	 		 		{
				                   	  	 		 			?>
				                   	  	 		 			<span class="cart-img"><img src="front_assets/images/icons/checked.png" class="img-responsive ActualResp1" title="Item Added To Cart"></span>
				                   	  	 		 			<?php
				                   	  	 		 		}
				                   	  	 		 		else
				                   	  	 		 		{
				                   	  	 		 			?>
				                   	  	 		 			<span class="cart-img"><img src="front_assets/images/icons/cart-add.png" class="img-responsive ActualResp" onclick="add_item_to_cart(this,<?php echo $pDetails->product_id;?>,'product');">
								                   	  	 	<img src="front_assets/images/icons/checked.png" class="img-responsive ActualResp1" title="Item Added To Cart" style="display:none;">	
								                   	  	 	   </span>
				                   	  	 		 			<?php
				                   	  	 		 		}
				                   	  	 		 ?>
				                   	  	 		
				                   	  	 </h1>

				                   	  	 </div>
				                   	</div>
                   							<?php
                   					}
                   			}

                   			else
                   			{
                   				?>
                   				<h3><div style="text-align: center">No Record Found</div></h3>
                   				<?php
                   			}
                   		?>
                   	  


                   </div>
                   
                  
				</div>
			</div>
			
		</div>
	</div>
</section>



<script type="text/javascript">
	function add_item_to_cart(obj,product_id,product_type)
	{

		var actual_price = $(obj).closest('.products-block').find('.product_detail').find('.actual_price').text();

		$.ajax({
				 type : 'POST',
				 url  : '<?php echo base_url("Cart/cart/add_new_product_items_to_cart")?>',
				 data : {'product_id':product_id,'product_type':product_type,'product_price':actual_price},
				 success:function(resp)
				 {
				 	resp = resp.trim();

				 	if(resp == "SUCCESS")
				 	{
				 		$(obj).closest('.products-block').find('.product_detail').find('.ActualResp').css('display','none');

				 		$(obj).closest('.products-block').find('.product_detail').find('.ActualResp1').css('display','block');
				 		
				 		get_cart_count();

				 	}

				 	else if(resp == "Not_Login")
				 	{
				 		$('#register-modal').modal('show');
				 	}
				 	else
				 	{
				 		alert("Something Went Wrong, Please Try Again Later");
				 	}
				 }


		});
	}
</script>




<script type="text/javascript">
	function get_filter_value()
	{
		var product_category  = $("input[name='product_category']:checked").val();


		if(product_category == "" || product_category==undefined)
		{
			pro_cat = "";
		}
		else
		{
			pro_cat = product_category;
		}


		 var favorite = [];
            $.each($("input[name='product_languages[]']:checked"), function(){            
                favorite.push($(this).val());
            });

            var a = favorite.join(",");

		 var redirect_url = "<?php echo base_url();?>product/?product_category="+pro_cat+"&product_languages="+a;

		window.location.href = redirect_url;

		
	}
</script>




