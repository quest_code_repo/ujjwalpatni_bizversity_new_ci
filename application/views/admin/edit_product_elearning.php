<div class="centercontent tables">
  <form class="stdform" action="" method="post" enctype="multipart/form-data">
    <div class="pageheader notab">
      <h1 class="pagetitle">Edit e-Leaarning Product</h1>
    </div><!--pageheader-->

    <div id="contentwrapper" class="contentwrapper">
      <!-- <div class="one_half"> -->
      <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
        <p>
          <label>Product Name<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="pname" class="smallinput" id="pname" required="required" value="<?=$product[0]['pname'];?>" /></span>
            <?php echo form_error('pname', '<div class="error_validate">', '</div>'); ?>
        </p>

        <p>
          <label>Categories<span style="color:red;">*</span></label>
          <select name="product_category" id="product_category" required="required" class="count_cat"> 
            <option value="">--select category--</option>
            <?php 
             if(!empty($product_category))
             {
              foreach($product_category as $each_category)
              {
               ?>
               <option value="<?= $each_category['id'];?>" <?= ($each_category['id']==$product[0]['product_category_id'])? "selected='selected'" : '' ?> ><?php echo $each_category['name']; ?></option>
               <?php
              }
             }
            ?>
          </select>
        </p>
      <!--   <p>
          <label>Course for this product<span style="color:red;">*</span></label>
          <select name="product_course" id="product_course" required="required" class="count_cat"> 
            <option value="">--select course--</option>
            <?php 
             if(!empty($all_course))
             {
              foreach($all_course as $course)
              {
               ?>
               <option value="<?= $course['sem_id'];?>" <?= ($course['sem_id']==$product[0]['course_id'])? "selected='selected'" : '' ?> ><?php echo $course['sem_name']; ?></option>
               <?php
              }
             }
            ?>
          </select>
        </p>
        <p><?php echo form_error('product_category', '<div class="error_validate">', '</div>'); ?></p>
      -->


        <p>
          <label>Video Url<span style="color:red;">*</span></label>
         <span class="field"><input type="text" class="smallinput" name="video_url" id="video_url" value="<?= $product[0]['video_url'];?>"   required="required" /></span>
        </p>


         <p>
          <label>Course Type<span style="color:red;"  ></span></label>
            <span class="field" required="required" >
            <?php
              if($product[0]['course_type'] == "Business")
              {
                ?>
                <label class="radio-inline"><input type="radio" name="course_type" value="Business" checked >Business</label>
              <label class="radio-inline" style="float: none;"><input type="radio" name="course_type" value="Individual">Individual</label>
            </span>
                <?php
              }
              else
              {
                ?>
                 <label class="radio-inline"><input type="radio" name="course_type" value="Business"  >Business</label>
              <label class="radio-inline" style="float: none;"><input type="radio" name="course_type" value="Individual" checked>Individual</label>
                <?php
              }
            ?>
            

        </p>

        <p>
          <label>Product Image<span style="color:red;">*</span></label>
          <input type="hidden" name="image_url1" value="<?=$product[0]['image_url']?>" />
          <span  class="float-left">
            <input type="file" name="image_url" id="image_url" accept="image/*"  onchange="displayPreview(this.files);"/>
          </span>
          <span  class="float-right">
            <img src="uploads/products/<?=@$product[0]['image_url']?>" height='100' >
          </span>
          <?php echo form_error('image_url', '<div class="error_validate">', '</div>'); ?>
        </p>

        <p>
          <label>Product price<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="product_price" class="smallinput" id="product_price" required="required" onkeyup="myFunction();" onblur="myFunction();" value="<?= $product[0]['product_price'];?>" /></span>
           <?php echo form_error('product_price', '<div class="error_validate">', '</div>'); ?>

        </p>

        <p>
          <label>Discounted Price<span style="color:red;"></span></label>
          <span class="field"><input type="text" name="disc_price" class="smallinput" id="disc_price" required="required" onkeyup="myFunction();" onblur="myFunction();" value="<?= $product[0]['disc_price'];?>" /></span>

        </p>

        <p>
          <label>Course Duration (in months)<span style="color:red;">*</span></label>
         <span class="field"><input type="text" name="course_duration" id="course_duration" value="<?= $product[0]['course_duration'];?>"   required="required" /></span>
        </p>
      
        <p>
          <label>If Featured</label>
          <select name="is_featured" id="is_featured">
            <option value="no" <?= ('no'==$product[0]['is_featured'])? "selected='selected'" : '' ?> >No</option>
            <option value="yes" <?= ('yes'==$product[0]['is_featured'])? "selected='selected'" : '' ?> >Yes</option>
          </select>
        </p>

        <p>
          <label>Featured Image<span style="color:red;"></span></label>
          <input type="hidden" name="featured_image1" value="<?=$product[0]['image_featured']?>" />
          <input type="file" name="featured_image" id="featured_image" accept="image/*" onchange=""/>
          <img src="uploads/featured-image/<?=@$product[0]['image_featured']?>" height='100' >
          <?php //echo form_error('featured_image', '<div class="error_validate">', '</div>'); ?>
        </p>

        <p>
          <label>Product Status</label>
          <select name="active_inactive" id="active_inactive">
            <option value="active" <?= ('active'==$product[0]['active_inactive'])? "selected='selected'" : '' ?> >Active</option>
            <option value="inactive" <?= ('inactive'==$product[0]['active_inactive'])? "selected='selected'" : '' ?> >Inactive</option>
          </select>
        </p>
      

        <p>
          <label>Video Duration (Hours)<span style="color:red;">*</span></label>
          <input name="hrs" type='text' class="smallinput" id="hrs" value="<?php echo isset($product_meta[0]['duration_hours'])?$product_meta[0]['duration_hours']:'';?>" required="required"/></input>
        </p>
        <p >
          <label>Articles<span style="color:red;">*</span></label>
          <input name="articles" type='text' class="smallinput" id="articles" value="<?php echo isset($product_meta[0]['articles'])?$product_meta[0]['articles']:'';?>" required="required"/></input>
        </p>
        <p >
          <label>Supplemental Resources<span style="color:red;">*</span></label>
          <input name="supplemental" type='text' class="smallinput" id="supplemental" value="<?php echo isset($product_meta[0]['supplemental_resources'])?$product_meta[0]['supplemental_resources']:'';?>" required="required"/></input>
        </p>
        <p >
          <label>Full lifetime access<span style="color:red;">*</span></label>
          <select name="access">

            <?php
                if($product_meta[0]['full_time_access'] == "Yes")
                {
                    $yes = "selected";
                    $no  = "";
                }
                elseif($product_meta[0]['full_time_access'] == "No")
                {
                    $yes = "";
                    $no  = "selected";
                }
                else
                {
                    $yes = "";
                    $no  = "";
                }
            ?> 

            <option value="Yes" <?php echo $yes;?>>Yes</option>
            <option value="No" <?php echo $no;?>>No</option>
          </select>
        </p>
        <p >
          <label>Access on mobile and TV </label>
         
          <?php


                if($product_meta[0]['access_on_mobile'] == "Yes")
                {
                    $yes = "selected";
                    $no  = "";
                }
                elseif($product_meta[0]['access_on_mobile'] == "No")
                {
                    $yes = "";
                    $no  = "selected";
                }
                else
                {
                    $yes = "";
                    $no  = "";
                }
            ?> 

          <select name="mobile" class="smallinput" id="mobile" required="required">
            <option value="Yes" <?php echo $yes;?>>Yes</option>
            <option value="No" <?php echo $no;?>>No</option>
          </select>
        </p>
        <p >
          <label>Certificate of Completion<span style="color:red;">*</span></label>
       
           <?php
                if($product_meta[0]['completion_certificate'] == "Yes")
                {
                    $yes = "selected";
                    $no  = "";
                }
                elseif($product_meta[0]['completion_certificate'] == "No")
                {
                    $yes = "";
                    $no  = "selected";
                }
                else
                {
                    $yes = "";
                    $no  = "";
                }
            ?> 

           <select name="Certificate" class="smallinput" id="Certificate" required="required">
            <option value="Yes" <?php echo $yes;?>>Yes</option>
            <option value="No" <?php echo $no;?>>No</option>
          </select>
        </p>




        <p>
          <label>Add more Tags: <span style="color:red;">*</span></label>
          <p><?php foreach ($product_tags as $product_tagskey => $product_tagsvalue) {
            echo $product_tagsvalue['name'].', ';
            // print_r($product_tagsvalue);
          } ?></p>
          <input type="text" name="tags"  id="tags" data-role="tagsinput" placeholder="Tags" class="smallinput" />
          <!-- <input name="articles" type='text' class="smallinput" id="articles" required="required"/></input> -->
        </p>
        
        <div class="col-md-2">
          <label>What I learn<span style="color:red;">*</span></label>
        </div>
        <div class="col-md-10">
          <textarea name="short_disc" class="smallinput" id="short_disc" required="required"/><?= $product[0]['short_disc'];?></textarea>
        </div>
        
      
        <div class="col-md-2">
          <label>Description<span style="color:red;">*</span></label>
        </div>
        <div class="col-md-10">
          <textarea name="description" class="smallinput" id="description" required="required"/><?= $product[0]['description'];?></textarea>
        </div>
        <!--  </div> -->
        <!-- </div>contentwrapper -->
        <p class="stdformbutton">
          <button class="submit radius2" id="addbtn">Update</button>
        </p>
    </div><!-- centercontent -->
  </form>
</div>

</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('description');
  CKEDITOR.replace('short_disc');
  CKEDITOR.replace('pfeatures');
</script>
<script>
 // var catid;
 // function get_sub_list(catid)
 // {
 //   $.ajax({
 //            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
 //            data:{'category_id':catid},
 //            type: 'POST',
 //            success: function(result)
 //            {
 //              //alert("catid"+result);
 //              var result_data=JSON.parse(result);
 //              var result_length=result_data.length;
 //              var cathtml="";
 //              cathtml = "<option value=''>--select variant--</option>";
 //            if(result_length>0)
 //            {
 //              for(var i=0;i<result_length;i++)
 //              {
 //                cathtml+="<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
 //              }
 //            }
 //          else
 //          {
 //            cathtml+="<option value=''>--select variant--</option>";
 //          }
 //          $('#sub_category').html(cathtml);
 //        }
 //     });
 // }

 //  function get_sub_sub_list(catid)
 //  {
 //    $.ajax({
 //      url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
 //      data:{'category_id':catid},
 //      type: 'POST',
 //      success: function(result)
 //      {
 //        //alert("catid"+result);
 //        var result_data=JSON.parse(result);
 //        var result_length=result_data.length;
 //        var cathtml="";
 //        cathtml = "<option value=''>--select variant--</option>";
 //        if(result_length>0)
 //        {
 //          for(var i=0;i<result_length;i++)
 //          {
 //            cathtml += "<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
 //          }
 //        }
 //        else
 //        {
 //          cathtml+="<option value=''>--select variant--</option>";
 //        }
 //        $('#sub_sub_category').html(cathtml);
 //      }
 //    });
 //  }
   function myFunction(){

    let product_actual_price = parseFloat($('#product_price').val());
    let product_discounted_price = parseFloat($('#disc_price').val());

    if( product_discounted_price > product_actual_price){
      alert('Alert..Discounted Price Can not more than actual price');
      // alert(product_actual_price+'-'+product_discounted_price);
       $('#disc_price').val('');
    }

   }
</script>
</body>
</html>
