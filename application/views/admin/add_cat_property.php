<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/furnish_dashboard/category_property" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Category Properties</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
            
            <?php 
              if($this->session->flashdata('success'))
              {
               ?>
                 <div class="alert alert-success"><?php echo $this->session->flashdata('success');?></div>
               <?php
              }
              else if($this->session->flashdata('error'))
              {
               ?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
               <?php 
              }

            ?>
                      <div>
                       

                          <p>
                          <label>Category List<span style="color:red;">*</span></label>
                           <select name="cat_id" id="cat_id" required="required" onchange="get_cat_prop(this.value);">
                            <option value="">--select Category--</option>
                            <?php 
                              if(!empty($category_list))
                              {
                                foreach($category_list as $each_cat)
                                {
                                ?>
                                <option value="<?php echo $each_cat['id'];?>" ><?php echo $each_cat['name'];?></option>
                                <?php
                                } 
                              }
                            ?>
                           </select>
                          </p>


                       

                          <p>
                           <label>Property Name</label>
                           <input type="text" name="property" id="property" class="smallinput" value="" required="required"/>
                         </p>
                          <p>
                            <label>Properties</label>
                            <div id="cat_prop" >

                            </div>

                          </p>
                        
                      </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
    <div style="clear:both;"></div>
                 <p class="stdformbutton">
                          <button class="submit radius2">Submit</button>
                 </p>
                     
  </form>
     
      

    </div>
  




</div><!--bodywrapper-->

</body>

</html>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script type="text/javascript">

 var cat_id;
 function get_cat_prop(cat_id)
 {
  //alert('hello');
    $.ajax({

            url: '<?php echo base_url();?>admin/furnish_dashboard/get_cat_property',
            data:{'cat_id':cat_id},
            type: 'POST',
            success: function(result)
            {
              //alert(result);
              $('#cat_prop').html(result);
            }
     });
 }

var prop_id;
var cat_id1;
 function remove_cat_prop(prop_id,cat_id1)
 {
    $.ajax({

            url: '<?php echo base_url();?>admin/furnish_dashboard/remove_cat_property',
            data:{'prop_id':prop_id},
            type: 'POST',
            success: function(result)
            {
              //alert(result);
              if(result==1)
              {
                get_cat_prop(cat_id1);
              }
              else
              {
                alert("Property not removed");
              }

            }
     });
 }
</script>