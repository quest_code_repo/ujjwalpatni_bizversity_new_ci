 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}
.edit-list {
  list-style: outside none none;
  padding: 0;
}
.edit-list.icons-list.icon-width {
  width: 100px;
}
.edit-list > li {
  display: inline-block;
}
.icons-list > li {
  padding: 0 2px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">User Details</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	} else if($this->session->flashdata('update'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('update'); ?>
           </div>
           <?php
          }
        ?>
        <div id="contentwrapper" class="contentwrapper">
            
             <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
              <thead>
                  <tr>
                     
                      <th>Full Name</th>
                      <th>Email</th>
                      <th>Address</th>
                      <th>Mobile</th>
                      <th>Join Date</th>
                  </tr>
              </thead>
              <tbody>
                
                     
                  <tr>
                      <td><?php echo isset($UserDetail[0]['full_name'])?$UserDetail[0]['full_name']:''?></td>
                      <td><?php echo isset($UserDetail[0]['username'])?$UserDetail[0]['username']:'' ?></td>
                      <td><?php echo isset($UserDetail[0]['address'])?$UserDetail[0]['address']:'' ?></td>
                      <td><?php echo isset($UserDetail[0]['mobile'])?$UserDetail[0]['mobile']:'' ?></td>
                      <td><?php echo isset($UserDetail[0]['created_at'])?$UserDetail[0]['created_at']:'' ?></td>
                     
                  </tr>
                  
                  
              </tbody>
          </table> 
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->


  
  


</body>

</html>



