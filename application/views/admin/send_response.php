<style type="text/css">
  .text_class {
    float: none !important;
    text-align: left !important;
}
</style>
<div class="centercontent tables">  
    <div class="pageheader notab">
      <h1 class="pagetitle">Student Doubt</h1> 

        <?php 
        if($this->session->flashdata('message'))
        {
         ?>
         <div class="alert alert-success">
          <?php echo $this->session->flashdata('message'); ?>
         </div>
         <?php
        }
        else if($this->session->flashdata('error'))
        {
         ?>
         <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error'); ?>
         </div>
         <?php
        }
          ?> 
        
    </div><!--pageheader-->
        
    <div id="contentwrapper" class="contentwrapper">
                    
      <p>
        <label class="text_class">Question<span style="color:red;">*</span></label>
          <textarea name="title" class="smallinput" id="title" rows="5" cols="30" dir="ltr" required/><?php echo $doubt[0]['title']?></textarea>
      </p>
      <p>
        <label class="text_class">Description<span style="color:red;">*</span></label>
          <span class="field"><textarea type="text" name="description" class="smallinput" id="description" /><?php echo $doubt[0]['description']?></textarea></span>
      </p>
      <p>
        <label class="text_class">Doubt Image<span style="color:red;"></span></label>
          <?php 
          if(!empty($doubt[0]['image'])) { ?>
            <span style="margin-left: 20px;"><img src="<?php echo base_url();?>uploads/doubt-image/maths/<?php echo $doubt[0]['image'];?>" width="100" height="100" /></span>
          <?php }else{ echo 'No Image';} ?>
      </p>

      
    </div>
  <form id="send_resp" class="stdform" action="<?php echo base_url(); ?>admin/add_semester/send_response/<?php echo $doubt[0]['id'];?>" method="post" enctype="multipart/form-data">
    <div id="contentwrapper" class="contentwrapper">  
      <div class="pageheader notab">
        <h1 class="pagetitle">Send Response</h1>
      </div>                   
      <p>
        <label class="text_class">Answer<span style="color:red;">*</span></label>
        <span class="field"><textarea type="text" name="answer" class="smallinput" id="answer" /></textarea></span>
      </p>

      <p>
        <label>Video URL</label>
        <span class="field"><input type="text" name="link" class="smallinput" id="link"/></span>
      </p>

      <p>
        <label>Select Answer Image<span style="color:red;"></span></label>
        <input type="file" name="image_ans" id="image_ans" value=""  />
      </p>

      
    </div>

      <div class="text-center" style="padding-bottom: 20px;"> 
        <!-- <a href="<?php //echo base_url();?>admin/teachers/teacher_details"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a> -->     
        <button type="submit" class="btn btn-orange" id="">Save</button>
      </div>
      <div class="clearfix"></div>
  
  </form>  
</div>
  



</div><!--bodywrapper-->
<script src="<?php echo base_url() ?>assets/tinymce/js/tinymce/tinymce.min.js?apiKey=x7mmfrsinal4f5v31e7q5iiwvluk8b2nof1eslzgr05i98bi"></script>
<script>

tinymce.init({
    selector: "textarea",
    themes: "modern",
    height : 50,
    plugins: "code",
  
});


</script>

<!-- <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('description');
  CKEDITOR.replace('pfeatures');
</script> -->
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#send_resp1").validate({
    rules:{
      
      username: {
        required: true,
      },
       password: {
        required: true,
        minlength: 6,
        maxlength: 6
      },
      fullname: {
        required: true,
      },
      mobile: {
        required: true,
        number:true,
        minlength: 10,
        maxlength: 12
      }

    },
    
    messages:{
      username: "Please Enter Username",
      password: "Please Enter Password only 6 digit",
      fullname: "Please Enter Full Name",
      mobile: "Please Enter Valid Mobile No."
      

    },
       
  });
</script>

</body>

</html>
