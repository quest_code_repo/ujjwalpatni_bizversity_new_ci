<div class="centercontent tables">
  <form id="video_form" class="stdform" action="" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Edit Free Ride to RSM</h1>
         
          
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper"> 
        <!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
          
            echo $this->session->flashdata('error'); 
          
          }
         ?>
        <div class="form-group">
          <label>Video Name<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="name" class="smallinput" id="name" required="required" value="<?php echo $video_detail[0]['name'];?>"  /></span>
               <?php echo form_error('name', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group">
          <label>Video URL<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="video_url" class="smallinput" id="video_url" value="<?php echo $video_detail[0]['video_url'];?>"  /></span>
               <?php echo form_error('video_url', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group">
          <label>Time Duration<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="duration" class="smallinput" id="duration" required="required" value="<?php echo $video_detail[0]['duration'];?>"  /></span>
               <?php echo form_error('duration', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group">
          <label> Type Status</label>
          <select name="status" id="status">
            <option value="active" <?php if($video_detail[0]['status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($video_detail[0]['status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
        </div>

        <div class="text-center" style="padding-bottom: 20px;"> 
        <a href="<?php echo base_url();?>admin/add_semester/free_video_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
     
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
      </div>
      <div class="clearfix"></div>
          
          <!-- <div class="form-group text-center">
              <button class="submit radius2" id="editbtn">Save</button>
          </div> -->
                 
      </div><!--contentwrapper-->
  <!-- centercontent -->
  </form>    
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('type_description');
  CKEDITOR.replace('description');
</script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#video_form").validate({
    rules:{
      
      name: {
        required: true,
      },
       video_url: {
        required: true,
        url: true
      },
      duration: {
        required: true,     
      }

    },
    
    messages:{
      name: "Please Enter Video Name",      
      video_url: "Please Enter Valid URL",
      duration: "Please Enter Video Time Duration"

    },
       
  });
</script>

</body>

</html>
