<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/Addservices/add_team_member" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Blogs</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="team_name" class="mediuminput" id="team_name" required="required" /></span>
            </p>
 <div class="service_field_error"><?php echo form_error('team_name'); ?></div>

 <p>
              <label>Designation<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="team_Designation" class="mediuminput" id="team_Designation" required="required" /></span>
            </p>
 <div class="service_field_error"><?php echo form_error('team_Designation'); ?></div>

            

            <p>
              <label>Member Image<span style="color:red;">*</span></label>
              <span class="field"><input type="file" name="member_image" class="mediuminput" id="member_image" accept="image/*" required="required" /></span>
            </p>
                  

              <p>
              <label>Short Description<span style="color:red;">*</span></label>
                <span class="field"><textarea name="member_desc" class="mediuminput" id="member_desc" maxlength="200" required></textarea></span>
            </p>
            <div class="service_field_error"><?php echo form_error('member_desc'); ?></div>

 <label>Facebook URL<span style="color:red;"></span></label>
                <span class="field"><input type="text" name="fac_url" class="mediuminput" id="fac_url" r/></span>
            </p>
 <div class="service_field_error"><?php echo form_error('team_name'); ?></div>

  <label>Google Plus URL<span style="color:red;"></span></label>
                <span class="field"><input type="text" name="google_url" class="mediuminput" id="google_url" /></span>
            </p>
 <div class="service_field_error"><?php echo form_error('team_name'); ?></div>

  <label>Twitter URL<span style="color:red;"></span></label>
                <span class="field"><input type="text" name="twitter_url" class="mediuminput" id="twitter_url" /></span>
            </p>
 <div class="service_field_error"><?php echo form_error('team_name'); ?></div>
           

             <!--  <p>
              <label>Brief Intro<span style="color:red;">*</span></label>
                <span class="field"><textarea name="brief_intro" class="mediuminput" id="brief_intro" maxlength="160" required></textarea></span>
            </p> -->
            <p>
              <label>Status<span style="color:red;">*</span></label>
               <select name="active_status" id="active_status">
                 <option value="Active">Active</option>
                 <option value="Inactive">Inactive</option>
              </select>
              </p>

            
          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Blog</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('blog_content');

  </script>

</div><!--bodywrapper-->

</body>

</html>