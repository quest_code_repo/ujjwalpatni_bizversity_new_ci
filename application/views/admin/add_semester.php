<div class="centercontent tables">
<form id="sem-form" class="stdform" action="<?php echo base_url(); ?>admin/add_semester/add_new_semester" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Course</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
                    	<!-- <div class="one_half"> -->
                      <?php 
                        if($this->session->flashdata('error'))
                        {
                          echo $this->session->flashdata('error'); 
                        }
                       ?>
                        <p>
                          <label>Course Name<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="sem_name" class="smallinput" id="sem_name" required="required" /></span>
                            <?php echo form_error('sem_name', '<div class="error_validate">', '</div>'); ?>
                        </p>

                        <p>
                       	  <label>Course Image<span style="color:red;">*</span></label>
                       	  <input type="file" name="sem_image" id="sem_image" accept="image/*" value="" required="required" onchange="displayPreview(this.files);"/>
                          <?php echo form_error('sem_image', '<div class="error_validate">', '</div>'); ?>
                        </p>

                         <p>
                          <label>Course Duration(in hours)<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="sem_duration" class="smallinput" id="sem_duration" required="required">
                            </span>
                            <?php echo form_error('sem_duration', '<div class="error_validate">', '</div>'); ?>
                        </p>

                        <p>
                          <label>No of Chapter<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="sem_no_of_chapter" class="smallinput" id="sem_no_of_chapter" required="required">
                            </span>
                            <?php echo form_error('sem_no_of_chapter', '<div class="error_validate">', '</div>'); ?>
                        </p>

                        <p>
                          <label>No of Topic<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="sem_no_of_lecture" class="smallinput" id="sem_no_of_lecture" required="required">
                            </span>
                            <?php echo form_error('sem_no_of_lecture', '<div class="error_validate">', '</div>'); ?>
                        </p>
                         
                        <!-- <p>
                          <label>Course Price<span style="color:red;">*</span></label>
                          <span class="field"><input type="text" name="sem_price" class="smallinput" id="sem_price" required="required"/></span>
                           <?php echo form_error('sem_price', '<div class="error_validate">', '</div>'); ?>

                        </p> -->

                        <p>
                          <label>Course Status</label>
                          <select name="sem_status" id="sem_status">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                          </select>
                        </p>


                        <p>
                          <label>Description<span style="color:red;">*</span></label>
                          <textarea name="sem_description" class="smallinput" id="sem_description" required="required"/></textarea>
                        </p>

                   
        </div><!--contentwrapper-->
		
  	  <div class="text-center" style="padding-bottom: 20px;"> 
        <a href="<?php echo base_url();?>admin/add_semester/semester_list">
          <input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" ></a>
          <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
      </div>
      <div class="clearfix"></div>

	<!-- </div>centercontent -->
				
  </form>
			<!------- Including PHP Script here ------>
</div>
</div><!--bodywrapper-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('sem_description');
  CKEDITOR.replace('pfeatures');
</script>
<script>
  var catid;
  function get_sub_list(catid)
  {
    $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
  				    var result_data=JSON.parse(result);
  				    var result_length=result_data.length;
  				    var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
				    if(result_length>0)
				    {
					   for(var i=0;i<result_length;i++)
					   {
						    cathtml+="<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
						  }
				    }
				  else
				  {
					cathtml+="<option value=''>--select variant--</option>";
				  }
				  $('#sub_category').html(cathtml);
        }
     });
 }
 function get_sub_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
              
             for(var i=0;i<result_length;i++)
             {
                
                cathtml += "<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_sub_category').html(cathtml);
        }
     });
 }
</script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#sem-form").validate({
    rules:{
      
      sem_name: {
        required: true,
      },
       sem_image: {
        required: true,
      },
      sem_duration: {
        required: true,
      },
      sem_no_of_chapter: {
        required: true,
        number: true
      },
      sem_no_of_lecture: {
        required: true,
        number:true
      },
      sem_price: {
        required: true,
        number:true
      }

    },
    
    messages:{
      sem_name: "Please Enter Course Name",
      sem_image: "Please Select Course Image",
      sem_duration: "Please Enter Course Duration",
      sem_no_of_chapter: "Please Enter No of Chapter In Number",
      sem_no_of_lecture: "Please Enter No of Topics in Number",
      sem_price: "Please Enter Price In number"
      

    },
       
  });
</script>

</body>

</html>
