<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">


<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
 .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}
</style>

<div class="centercontent tables">
<form id="cop_form" class="stdform" action="<?php echo base_url(); ?>admin/add_product/add_product_coupons" method="post" enctype="multipart/form-data">

 <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
        
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Coupon</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Coupon Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="coupons_names" class="mediuminput" id="coupons_names" required="required" placeholder="Coupon Name" /></span>
            </p>
            <p id="title_exist_error" style="color: red;text-align: center;"></p>
            <div class="service_field_error"><?php echo form_error('coupons_names'); ?></div>

            <p>
              <label>Coupon Type<span style="color:red;">*</span></label>
               <select name="coupon_types" id="coupon_types" required="required" onchange="myFunction()">
                 <option value="">Select Type</option>
                 <option value="flat">Flat</option>
                 <option value="percent">Percent</option>
              </select>
              </p>

              <p style="display: none;" id="flat_amt_area">
              <label>Flat Ammount<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="flat_amount" class="mediuminput" id="flat_ammount" required="required" placeholder="Enter Amount"  onkeypress="return isNumberKey(event);"/></span>
            </p>


            <p style="display: none;" id="discouts_amt_area">
              <label>Discount Percentage(%)<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="discount_percentage" class="mediuminput" id="coupos_percentage" required="required" placeholder="Enter Percentage value" onkeypress="return isNumberKey(event);" onblur="return check_percentage_value(this.value);"/></span>
            </p>

            <p>
              <label>Coupon Valid From<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="coupon_from" class="mediuminput datetimepicker" id="coupon_from"  required="required" placeholder="YYY-MM-DD" /></span>
            </p>
            <div class="service_field_error"><?php echo form_error('coupon_from'); ?></div>

            <p>
              <label>Coupon Valid Till<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="coupon_to" class="mediuminput datetimepicker" id="coupon_to" required="required" placeholder="YYY-MM-DD" /></span>
            </p>
            <div class="service_field_error"><?php echo form_error('coupon_to'); ?></div>
          <!--   <p>
              <label>Brief Description<span style="color:red;">*</span></label>
                <span class="field"><textarea type="text" name="brief_desc" class="mediuminput" id="brief_desc" required="required" placeholder="Coupon Description" /></textarea></span>
            </p>
            <div class="service_field_error"><?php echo form_error('brief_desc'); ?></div> -->



            

           <p>
              <label>Coupon Status<span style="color:red;">*</span></label>
               <select name="coupon_status" id="coupon_status">
                 <option value="active">Active</option>
                 <option value="deactive">Inactive</option>
              </select>
              </p>

          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button type="submit" class="submit" id="addProduct" onclick="return check_programs_have();">Add Coupon</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  


</div><!--bodywrapper-->




</body>

</html>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<script type="text/javascript">
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>

<script type="text/javascript">

 function check_title(){

//   var cat_name = $('#program_name').val();
//   var type_val = 2;

//   $.ajax({
//          type:'POST',
//          url:'<?php echo site_url();?>admin/Add_programs_category/check_category_title',
//          data:{"name":cat_name,'type_calls':type_val},
//          success: function(data)
//          {  
//             if(data==1){

//               $('#program_name').val('');
//               $('#title_exist_error').html('Program Name Already Exist');

//             } else{
//               $('#title_exist_error').html('');

//             }
//          }
//      });
 }

function isNumberKey(evt)
    {
                 var charCode = (evt.which) ? evt.which : evt.keyCode
                 if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                 return true;
    }



</script>
 <script type="text/javascript" src="<?php echo base_url("assets/admincss/js/jquery.datetimepicker.js"); ?>"></script> 
 <!-- <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery.validate.min.js"></script> -->

  <script>
  $( function() {
    $.noConflict();
   
     jQuery('.datetimepicker').datetimepicker({
                timepicker: false,
                format: 'Y-m-d',
                formatDate: 'Y-m-d',
            })
  });

  function myFunction(){

    var vals = jQuery('#coupon_types').val();

    if(vals=='flat'){

      jQuery('#flat_amt_area').css('display','block');
      jQuery('#discouts_amt_area').css('display','none');

    } else if(vals=='percent'){

      jQuery('#discouts_amt_area').css('display','block');
      jQuery('#flat_amt_area').css('display','none');

    } else{
      jQuery('#discouts_amt_area').css('display','none');
      jQuery('#flat_amt_area').css('display','none');
    }

  }

  jQuery(document).ready(function (){

   jQuery('#cop_form').validate({
       onfocusout: function(element) {
             this.element(element);
       },
       errorClass: 'error_validate',
       errorElement:'span',
       highlight: function(element, errorClass) {
        jQuery(element).removeClass(errorClass);
       }
     });
 });

  function check_programs_have(){

    var val_pro = jQuery('#coupos_programs').val();
    
    if(val_pro==''){

    jQuery('#no_program_selected').html('Select program required field');
    jQuery('#no_program_selected').css('color','red');
    return false;

  } else{

    jQuery('#no_program_selected').html('');

  }
    
  }

  </script>
  

  <script type="text/javascript">
    function check_percentage_value(obj)
    {
        if(obj !="")
        {
            if(obj >=100)
            {
                alert('Discount Percentage Cannot Be Greater Than 100');
                $('#coupos_percentage').val('');
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            alert('Please Enter Discount Percentage');
            return false;
        }
    }
  </script>

