<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

 

<div class="centercontent tables">
    <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>

<form class="stdform" action="<?php echo base_url(); ?>admin/Course_category" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Course Category</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="course_category_name" class="mediuminput" id="course_category_name" required="required" placeholder="Course Category Name" /></span>
            </p>
 <div class="service_field_error"><?php echo form_error('course_category_name'); ?></div>

        
              <label>Category Status<span style="color:red;">*</span></label>
               <select name="active_status" id="active_status">
                 <option value="Active">Active</option>
                 <option value="Inactive">Inactive</option>
              </select>
              </p>

            
          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Course Category</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('blog_content');

  </script>

</div><!--bodywrapper-->

</body>

</html>