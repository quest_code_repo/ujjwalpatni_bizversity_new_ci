<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/Addservices/add_aboutus_content" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Blogs</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
          <!--   <p>
              <label>Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="blog_title" class="mediuminput" id="blog_title" required="required" /></span>
            </p>
 <div class="service_field_error"><?php echo form_error('blog_title'); ?></div> -->

            

          <!--   <p>
              <label>Blog Image<span style="color:red;">*</span></label>
              <span class="field"><input type="file" name="blog_image" class="mediuminput" id="blog_image" accept="image/*" required="required" /></span>
            </p> -->
                  <!-- <p>
                          <label>Choose Category<span style="color:red;"></span></label>
                           <select name="choose_cat" id="choose_cat"/>
                             <option value="">--select category--</option>

                            <?php 
                              if(!empty($blog_category_name))
                              {
                                foreach($blog_category_name as $cat_name)
                                {
                              ?>
                              <option value="<?php echo $cat_name['id'];?>"><?php echo $cat_name['blog_cat_name'];?></option>
                              <?php
                                } 
                              }
                            ?>
                           </select>
                          </p> -->

             <!--  <p>
              <label>Short Description<span style="color:red;">*</span></label>
                <span class="field"><textarea name="short_desc" class="mediuminput" id="short_desc" maxlength="165" required></textarea></span>
            </p> -->


            <div class="form-group">
                         <h5 style="color: black;font-weight: bold;">First Paragraph<span style="color:red;">*</span></h5>
                         <textarea name="blog_content1" class="smallinput" id="first_para" /></textarea>
                           <?php echo form_error('blog_content1', '<div class="error_validate">', '</div>'); ?>
                     </div>

            <div class="form-group">
                         <h5 style="color: black;font-weight: bold;">Second Paragraph<span style="color:red;">*</span></h5>
                         <textarea name="blog_content2" class="smallinput" id="second_para" /></textarea>
                           <?php echo form_error('blog_content2', '<div class="error_validate">', '</div>'); ?>
                     </div>

            <div class="form-group">
                         <h5 style="color: black;font-weight: bold;">Third Paragraph<span style="color:red;">*</span></h5>
                         <textarea name="blog_content3" class="smallinput" id="third_para" /></textarea>
                           <?php echo form_error('blog_content3', '<div class="error_validate">', '</div>'); ?>
                     </div>



           <!-- <p>
                          <label>Author<span style="color:red;">*</span></label>
                           <select name="author_name" id="author_name" required="required">
                             <option value="">--select author--</option>

                            <?php 
                              if(!empty($employees))
                              {
                                foreach($employees as $employee)
                                {
                              ?>
                              <option value="<?php echo $employee['name'];?>"><?php echo $employee['name'];?></option>
                              <?php
                                } 
                              }
                            ?>
                           </select>
                          </p> -->
             <!--  <p>
              <label>Brief Intro<span style="color:red;">*</span></label>
                <span class="field"><textarea name="brief_intro" class="mediuminput" id="brief_intro" maxlength="160" required></textarea></span>
            </p> -->
            <p>
              <label>Status<span style="color:red;">*</span></label>
               <select name="active_status" id="active_status">
                 <option value="Active">Active</option>
                 <option value="Inactive">Inactive</option>
              </select>
              </p>

            
          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Blog</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('first_para');
  CKEDITOR.replace('second_para');
  CKEDITOR.replace('third_para');

  </script>

</div><!--bodywrapper-->

</body>

</html>