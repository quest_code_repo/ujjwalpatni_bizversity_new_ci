 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}

 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Special Registraion List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	}
        ?>
        <div id="contentwrapper" class="contentwrapper">
          

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

            <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone No</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                            if(!empty($special_data))
                                            {

                                              $get_page = $this->input->get('per_page');

                                              if(!empty($get_page)){

                                                $j = $get_page+1;

                                              } else {

                                                $j=1;

                                              }
                                                foreach($special_data as $result)
                                            {?>
                                            <tr>

                                                <td><?php echo $j; ?></td>
                                                <td><?php  echo $result->full_name; ?></td>
                                                <td><?php  echo $result->email; ?></td>
                                                <td><?php  echo $result->mobile_no; ?></td>
                                               
                                
                                            </tr>
                                            <?php 
                                                $j++;
                                                  }
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
        <div class="pagination"><p><?php echo $links;?></p></div>
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>

<script type="text/javascript">

    function delete_category_data(id)
    {

      if(confirm("Are you Sure! You Want to delete it")){   

        $.ajax({
         type:'POST',
         url:'<?php echo site_url();?>admin/Add_programs_category/delete_orders_placed',
         data:{"id":id},
         success: function(data)
         {  
            if(data==1){

                 location.reload();
            }
         }
     });

        return true;
      } else{
        
        return false;
      }

    }
  </script>