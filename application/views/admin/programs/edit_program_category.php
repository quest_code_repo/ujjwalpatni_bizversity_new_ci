<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Edit Category</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="category_name" class="mediuminput" id="category_name" required="required" placeholder="Category Name" onblur="check_title();" value="<?php if(!empty($all_data)){ echo $all_data[0]->name; } ?>"/></span>
            <div class="service_field_error"><?php echo form_error('category_name'); ?></div>
            </p>

            

           
              <label>Category Status<span style="color:red;">*</span></label>
               <select name="active_status" id="active_status">
                 <option value="active">Active</option>
                 <option value="inactive">Inactive</option>
              </select>
              </p>

            
          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Category</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('blog_content');

  </script>


</div><!--bodywrapper-->

</body>

</html>


<script type="text/javascript">

// function check_title(){

//   var cat_name = $('#category_name').val();
//   var type_val = 1;

//   $.ajax({
//          type:'POST',
//          url:'<?php echo site_url();?>admin/Add_programs_category/check_category_title',
//          data:{"name":cat_name,'type_calls':type_val},
//          success: function(data)
//          {  
//             if(data==1){

//               $('#category_name').val('');
//               $('.service_field_error').html('Category Already Exist');

//             } else{
//               $('.service_field_error').html('');

//             }
//          }
//      });
// }
  

</script>
