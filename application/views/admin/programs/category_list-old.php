 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Category List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	}
        else if($this->session->flashdata('deleted'))
            { ?> <div class="alert alert-danger">
                <?php echo $this->session->flashdata('deleted'); ?>
             </div>
             <?php
            } else if($this->session->flashdata('update'))
            { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('update'); ?>
             </div>
             <?php
            } ?>

        <div id="contentwrapper" class="contentwrapper">
            <!-- <a href="<?php //echo base_url();?>admin/add_programs_category"><button class="btn-primary">ADD CATEGORY</button></a> -->
              <!-- <a href="<?php //echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->
            <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>CATEGORY NAME</th>
                                                <th>URL</th>
                                                <th>Status</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                            if(!empty($all_data))
                                            {
                                                $j=1;
                                                foreach($all_data as $result)
                                            {?>
                                            <tr>

                                                <td><?php echo $j; ?></td>
                                                <td><?php  echo $result->name; ?></td>
                                                <td><?php  echo $result->url; ?></td>
                                                <td><?php  echo $result->status; ?></td>
                                                <td>
                                                <a href="<?php echo base_url();?>admin/add_programs_category/edit_category/?cat_id=<?php echo base64_encode($result->id); ?>
                                                ">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="javascript:void(0);" onclick="return delete_category_data(<?php echo $result->id ?>)"  title="Delete"><i class="fa fa-times" aria-hidden="true" style='color:red;'"></i></a></td>
                                
                                            </tr>
                                            <?php 
                                                $j++;
                                                  }
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>

<script type="text/javascript">

    function delete_category_data(id)
    {
      if(confirm("Are you Sure! You Want to delete it")){   
      alert(id);

        $.ajax({
          type:'POST',
          url:'<?php echo base_url();?>admin/add_programs_category/delete_data',
          data:{"id":id},
          success: function(data)
          {  
            if(data==1){
              location.reload();
            }
          }
        });

        return true;
      } else {
        
        return false;
      }

    }
  </script>