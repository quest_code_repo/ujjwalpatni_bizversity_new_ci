 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}
.edit-list {
  list-style: outside none none;
  padding: 0;
}
.edit-list.icons-list.icon-width {
  width: 100px;
}
.edit-list > li {
  display: inline-block;
}
.icons-list > li {
  padding: 0 2px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Brochure List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	} else if($this->session->flashdata('update'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('update'); ?>
           </div>
           <?php
          }
        ?>
        <div id="contentwrapper" class="contentwrapper">
            <a href="<?php echo base_url();?>admin/add_programs_category/add_brochure"><button class="btn-primary">ADD Brochure</button></a>

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

             <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
              <thead>
                  <tr>
                      <th>S.No.</th>
                      <th>Brochure Name</th>
                      <th>Category</th>
                      <th>Brochure Url</th>
                  </tr>
              </thead>
              <tbody>
                 <?php 
                  if(!empty($all_data))
                  {
                      $j=1;
                      foreach($all_data as $result)
                  {?>
                  <tr>

                      <td><?php echo $j; ?></td>
                      <td><?php  echo $result->brochure_name; ?></td>
                      <td><?php  echo $result->name; ?></td>
                      <td><a href="<?php echo base_url('uploads/program_brochure/'.$result->brochure_image.'')?>" target="_blank">View</a></td>
                  </tr>
                  <?php 
                      $j++;
                        }
                      }
                  ?>
                  
              </tbody>
          </table> 
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>


