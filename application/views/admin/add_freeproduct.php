<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/bundlepack_control/add_freeproduct" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Free Product</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
      
               
                    	<div class="">
						<p>
                       	  <label>Offer Name<span style="color:red;">*</span></label>
                            <span class="field">
                            <select name="offer_id" id="offer_id" required="required">
                            	<option value="">--Select--</option>
                                <?php foreach($offerid as $offername)
								{ ?>
                                <option value="<?php echo $offername['offer_id']; ?>"><?php echo $offername['offer_name']; ?></option>
                                <?php } ?>
                            </select>
                          </span> 
                        </p>
						
						<p>
                       	  <label>Product Name<span style="color:red;">*</span></label>
                            <span class="field">
                            <select name="pname" id="pname" required="required" onchange="load_weight(this.value,1);">
                            	<option value="">--Select--</option>
                                <?php foreach($pname as $prdname)
								{ ?>
                                <option value="<?php echo $prdname['product_id']; ?>"><?php echo $prdname['pname']; ?></option>
                                <?php } ?>
                            </select>
                          </span> 
                        </p>
						
						 
						<p> 
                        <label>Select Quantity<span style="color:red;">*</span></label>
                        <span class="field" id="weg1">&nbsp; </span>
                        </p>
						
					    <p>
                       	  <label>Product Quantity</label>
                          <span class="field"><input type="text" name="pquantity" class="mediuminput" id="pquantity" /></span> 
                        </p>
						<p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Product</button>
             </p>
						
                        </div>
						
                        
                        
                   
        </div><!--contentwrapper-->
		

  </form>
     
			<!------- Including PHP Script here ------>

		</div>
	
<script>
 function load_weight(pid,i)
	{   
		
		if(pid=='') { return false; }
		jQuery.ajax({url: '<?php echo base_url();?>admin/quickshop_control/load_weight/'+pid+'/'+i, success: function(result){
		//alert(result);
		if(result) { 
			$('#weg'+i).html(result); 
		}}});

	}
</script>
</div><!--bodywrapper-->

</body>

</html>