<div class="centercontent tables">
  <form id="edit-sem-form" class="stdform" action="" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Edit Course</h1>
         
          
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper"> 
        <!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
          
            echo $this->session->flashdata('error'); 
          
          }
         ?>
        <div class="form-group">
          <label>Course Name<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="sem_name" class="smallinput" id="sem_name" required="required" value="<?php echo $sem_detail[0]['sem_name'];?>"  /></span>
               <?php echo form_error('sem_name', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group">
          <label>Course Image</label>
            <span>Max upload size:width-1024,Height-768</span>
            <input type="file" name="sem_image" id="sem_image" accept="image/*" value="" <?php if(empty($sem_detail[0]['sem_image'])){ ?>required="required" <?php } ?> />

            <?php 
            if(!empty($sem_detail[0]['sem_image'])) 
             {
            ?>
              <img src="<?php echo base_url();?>uploads/semester/<?php echo $sem_detail[0]['sem_image'];?>" width="100" height="100" />
              <input type="hidden" name="sem_image1" id="sem_image1" value="<?php echo $sem_detail[0]['sem_image'];?>" />
            <?php 
              }
            ?>
        </div>

        <div class="form-group">
          <label>Course Duration(In Hours)<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="sem_duration" class="smallinput" id="sem_duration" required="required" value="<?php echo $sem_detail[0]['sem_duration'];?>"  /></span>
               <?php echo form_error('sem_duration', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group">
          <label>No of Chapter<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="sem_no_of_chapter" class="smallinput" id="sem_no_of_chapter" required="required" value="<?php echo $sem_detail[0]['sem_no_of_chapter'];?>"  /></span>
               <?php echo form_error('sem_no_of_chapter', '<div class="error_validate">', '</div>'); ?>
        </div>
<!-- 
        <div class="form-group">
          <label>No of Topics<span style="color:red;">*</span></label>
              <span class="field">
               <?php echo form_error('sem_no_of_lecture', '<div class="error_validate">', '</div>'); ?>
        </div> -->
        <input type="hidden" name="sem_no_of_lecture" class="smallinput" id="sem_no_of_lecture" required="required" value="<?php echo $sem_detail[0]['sem_no_of_lecture'];?>"  /></span>
        
        <div class="form-group">
          <label>Course Price<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="sem_price" class="smallinput" id="sem_price" required="required" value="<?php echo $sem_detail[0]['sem_price']; ?>" /></span>
             <?php echo form_error('sem_price', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group">
            <h4>Description<span style="color:red;"></span></h4>
            <textarea class="form-control" name="sem_description"  id="sem_description" required="required"><?php echo $sem_detail[0]['sem_description']; ?></textarea>
        </div>

        <div class="form-group" style="padding-bottom: 20px;">
          <label>Course Status</label>
          <select name="sem_status" id="sem_status">
            <option value="active" <?php if($sem_detail[0]['sem_status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($sem_detail[0]['sem_status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
        </div>

        <div class="text-center" style="padding-bottom: 20px;"> 
          <button type="submit" class="btn btn-orange" id="addbtn">Save</button>

          <a href="<?php echo base_url();?>admin/add_semester/semester_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > 
      </div>
      <div class="clearfix"></div>
          
          <!-- <div class="form-group text-center">
              <button class="submit radius2" id="editbtn">Save</button>
          </div> -->
                 
      </div><!--contentwrapper-->
  <!-- centercontent -->
  </form>    
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('sem_description');
  CKEDITOR.replace('description');
</script>
<script>
 var catid;
 function get_variant_list(catid)
 {
  
   $.ajax({

            url: '<?php echo base_url();?>admin/furnish_dashboard/get_category_variant',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
            var result_data=JSON.parse(result);
            var result_length=result_data.length;
            var cathtml="";
            if(result_length>0)
            {
          
             for(var i=0;i<result_length;i++)
             {
            cathtml+="<option value='"+result_data[i]['variant_id']+"'>"+result_data[i]['variant_name']+"</option>";
            
             }

          
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }

        

      
        $('#category_variant').html(cathtml);
              
                  
            }
     });

   $.ajax({
            url: '<?php echo base_url();?>admin/furnish_dashboard/get_cat_property_detail',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert(result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml1 ="";
              if(result_length>0)
              {
                cathtml1 +="<table class='table table-bordered table-responsive'><thead><tr><th>Property</th><th>Value</th></tr></thead>";
                for(var i=0;i<result_length;i++)
                {
                  cathtml1+="<tr><td>"+result_data[i]['property_name']+"</td><td><input name='prop["+result_data[i]['property_id']+"]' type='text'></td></tr>";
                }
                cathtml1 += "</table>";
                //alert(cathtml1);
              }
              else
              {
                cathtml1 = "";
              }
              $('.cat_pro').html(cathtml1);
        }
     });
 }
</script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#edit-sem-form").validate({
    rules:{
      
      sem_name: {
        required: true,
      },       
      sem_duration: {
        required: true,
      },
      sem_no_of_chapter: {
        required: true,
        number: true
      },
      sem_no_of_lecture: {
        required: true,
        number:true
      },
      sem_price: {
        required: true,
        number:true
      }

    },
    
    messages:{
      sem_name: "Please Enter Course Name",
      sem_duration: "Please Enter Course Duration",
      sem_no_of_chapter: "Please Enter No of Chapter In Number",
      sem_no_of_lecture: "Please Enter No of Topics in Number",
      sem_price: "Please Enter Price In number"
      

    },
       
  });
</script>

</body>

</html>
