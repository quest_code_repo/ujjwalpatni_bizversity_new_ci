 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}

 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Order List</h1>
           
            
        </div><!--pageheader-->
        <?php 
            if($this->session->flashdata('success'))
            {
             ?>
             <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
             </div>
             <?php
            }
            else if($this->session->flashdata('error'))
            {
             ?>
             <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error'); ?>
             </div>
             <?php
            }
        ?>
        <div id="contentwrapper" class="contentwrapper">
          

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

            <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
                                        <thead>
                                            <tr>
                                              <th>S No.</th>
                                              <th>Product Name</th>
                                              <th>Quantity</th>
                                              <th>Price</th>
                                              <th>Product Type</th>
                                              <th>Category</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                            if(!empty($OrderDetails))
                                            {

                                              $get_page = $this->input->get('per_page');

                                              if(!empty($get_page)){

                                                $j = $get_page+1;

                                              } else {

                                                $j=1;

                                              }
                                                foreach($OrderDetails as $result)
                                            {
                                               $pname = get_pname_for_placed_order($result->product_id,$result->product_type);
                                              ?>
                                            <tr>

                                                <td><?php echo $j; ?></td>
                                                <td><?php echo $pname[0]->item_name;?></td>
                                                <td><?php  echo $result->product_quantity; ?></td>
                                                <td><?php  echo $result->product_price; ?></td>
                                                 <?php
                                                        if($result->product_type == "e_learning")
                                                        {
                                                          ?>
                                                        <td>E-Learning</td>
                                                          <?php 
                                                        }

                                                        else if($result->product_type == "programe")
                                                        {
                                                          ?>
                                                          <td>Program</td>
                                                          <?php
                                                        }

                                                        else if($result->product_type == "product")
                                                        {
                                                          ?>
                                                           <td>Product</td>
                                                          <?php
                                                        }
                                                    ?>
                                                    <td><?php echo $pname[0]->category;?></td>
                                            </tr>
                                            <?php 
                                                $j++;
                                                  }
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
      
          

               <a href="<?php echo base_url('admin/Orders')?>" button type="button" class="btn btn-danger">Back</a>

        </div><!--contentwrapper-->
    
            
        
    </div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>

