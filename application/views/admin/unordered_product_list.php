 <style>
    .xcrud-th{

        background: #E9E9E9;
    white-space: nowrap;
    height: 35px;
    text-align: left;
    font-weight: bold;
    font-size: 12px;
    border-bottom: 1px solid #DADADA;
    color: #777;
    border-right: 1px solid #F1F1F1;
    vertical-align: middle;
    padding: 2px 8px;

    } 
         
 </style>
 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Unordered Products</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
           <table class="xcrud-list table table-striped table-hover table-bordered">
           <thead>
               <tr class="xcrud-th">
                 <th>#</th>
                 <th class="xcrud-column xcrud-action"><b>Product Name</b></th>
                 <th class="xcrud-column xcrud-action"><b>Product Images</b></th>
                 <th class="xcrud-column xcrud-action"><b>Action</b></th>
               </tr>
              
            </thead>
            <tbody>
                <?php 
                    if(!empty($unordered_list))
                    {
                        $i=1;
                        foreach($unordered_list as $each_pro)
                        {
                     ?>

                      <tr>
                         <td><?php echo $i; ?></td>
                         <td class="xcrud-column xcrud-action"><?php echo $each_pro['product_name']; ?></td>
                         <td class="xcrud-column xcrud-action">
                         <?php 
                            if(!empty($each_pro['images']))
                            {
                         ?>
                         <img src="<?php echo base_url();?>uploads/products/<?php echo $each_pro['images'];?>" width="40" height="40" />
                            <?php 
                            }
                            ?>
                         </td>

                         <td class="xcrud-column xcrud-action"><a href="javascript:void(0);" onclick="delete_product('<?php echo $each_pro['product_id'] ?>');">Delete</a></td>  
                        </tr>
                     <?php 
                         $i++; 
                      }
                    }
                ?>
                

            </tbody>
           </table>
               
        </div><!--contentwrapper-->
            <?php 
            if(!empty($pagination))
            {
             echo $pagination;   
            }
            ?>
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->
<script>
  var productid;
  function delete_product(productid)
  {

    var r = confirm("Are you sure to delete this product!");
    
    if(r == true) 
    {
         $.ajax({

          url:'<?php echo base_url();?>admin/furnish_dashboard/product_deletion',
          data:{'productid':productid},
          type: 'POST',
          success: function(result)
          {
           
                if(result==1)
                {
                    alert("Product deleted successfully");

                    location.reload();         
                }
                else
                {
                    alert("Something went wrong!!");
                }
          }
        }); 
    } 
    else 
    {
       
    }

  }
</script>

</body>

</html>