
    <div class="centercontent">
    
        <div class="pageheader">
        <?php 
        if($this->session->flashdata('success'))
        {
         ?>
         <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
         </div>
        <?php
        }
        else if($this->session->flashdata('error'))
        {
         ?>
         <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
         </div>
        <?php
        }
        ?>  
            <h1 class="pagetitle">Dashboard</h1>
            <span class="pagedesc">Welcome to the admin dashboard</span>
            
            <!--<ul class="hornav">
                <li class="current"><a href="#updates">Updates</a></li>
                <li><a href="#activities">Activities</a></li>
            </ul>-->
        </div><!--pageheader-->
        
        <br clear="all" />
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>
