<style type="text/css">
    
   .table-question thead th {
  color: #000 !important;
  font-size: 12px;
  font-weight: bold !important;
}
.contentwrapper {
  padding: 20px 0;
}
</style>

<div class="centercontent">
    
    <div class="pageheader notab">
        <h1 class="pagetitle"><?php echo $title; ?></h1>
    </div><!--pageheader-->
    <?php 
    if($this->session->flashdata('success'))
    {
     ?>
     <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
     </div>
     <?php
    }
    else if($this->session->flashdata('error'))
    {
     ?>
     <div class="alert alert-danger">
        <?php echo $this->session->flashdata('error'); ?>
     </div>
     <?php
    }
    ?>
    <div id="contentwrapper" class="contentwrapper">
        <a href="<?php echo base_url();?>admin/add_semester/add_test"><button class="btn-primary">Add Test</button></a>

        <table class="table table-bordered table-question">
            <thead>
                <tr>
               
                 <th>Test Name</th>
                 <th>Ideal Time Duration</th>
                 <th>No of Question</th>
                 <!-- <th>Passing Marks</th> -->
                 <th>Status</th>
                 <th>Action</th>
                 <th>Question</th>
                </tr>
            </thead>
                       
            <tbody>
                <?php 
                if(!empty($test_detail)){
                foreach ($test_detail as $test) {
                ?>
                <tr>
              
                    <td><?php echo $test['test_name']; ?></td>
                    <td><?php echo $test['ideal_time_duration']; ?></td>
                     <td><?php echo get_my_questions($test['test_id']); ?></td>
                    <!-- <td><?php //echo $test['test_passing_marks']; ?></td> -->
                 
                    <td><?php echo $test['test_status']; ?></td>
                    <td>
                    <a href="<?php echo base_url(); ?>admin/add_semester/edit_test/<?php echo $test['test_id']; ?>" onClick="javascript:return confirm('Are you sure to Update it?')"><i class="fa fa-pencil" aria-hidden="true"> Edit</i></a>
                    </td>
                    
                    <td><a href="<?php echo base_url(); ?>admin/add_semester/manage_question/<?php echo $test['test_id']?>/?lect_id=<?php echo $test['lecture_id']; ?>&ch_id=<?php echo $test['chapter_id']; ?>&sem_id=<?php echo $test['sem_id']; ?>&choice_id=<?php echo $test['choice']; ?>"><button class="btn-default" style="background-color: #3A4C69; color: white;">Manage</button></a></td>
                 
                </tr>
              
                <?php }} ?>
            </tbody>
            
        </table>

    </div><!--contentwrapper-->
            
        
</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>