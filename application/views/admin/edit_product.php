<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">
<style type="text/css">
    .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url('admin/add_product/edit_product')?>" method="post" enctype="multipart/form-data">

  <input type="hidden" name="product_id" value="<?php echo $product_detail[0]['product_id'];?>"  />

    <input type="hidden" name="product_type" value="product"  />

        <div class="pageheader notab">
            <h1 class="pagetitle">Edit Product</h1>
           
            
        </div><!--pageheader-->


        
        <div id="contentwrapper" class="contentwrapper">
      
               
                      <!-- <div class="one_half"> -->
                      <?php 
                        if($this->session->flashdata('error'))
                        {
                        
                          echo $this->session->flashdata('error'); 
                        
                        }
                       ?>
                      <div class="form-group row">
                        <label>Product Name<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="pname" class="smallinput" id="pname" required="required" value="<?php echo $product_detail[0]['pname'];?>" onblur="return check_product_name(this.value);" /></span>
                             <?php echo form_error('pname', '<div class="error_validate">', '</div>'); ?>
                      </div>

                      <p id="pnameDiv"></p>

                  <div class="row">
                        <div class="col-md-2">
                          <label>Product Type<span style="color:red;"  ></span></label>
                        </div>


                        <?php
                            if(!empty($product_category) && count($product_category)>0)
                            {
                                foreach($product_category as $subData)
                                {
                                    if($subData['id']  == $product_detail[0]['product_category_id'])
                                    {
                                       ?>
                                      <div class="col-md-2">
                                          <label class="radio-inline"><input type="radio" name="product_category_id" value="<?php echo $subData['id'];?>" onclick="get_related_products();" checked><?php echo $subData['name'];?></label>
                                       </div>
                                    <?php
                                   }
                                   else
                                   {
                                        ?>
                                      <div class="col-md-2">
                                          <label class="radio-inline"><input type="radio" name="product_category_id" value="<?php echo $subData['id'];?>" onclick="get_related_products();" ><?php echo $subData['name'];?></label>
                                       </div>

                                        <?php
                                   }
                                }
                              }
                            else
                            {
                                echo "No Product Category Found , Please Create The New One";
                            }
                        ?>
                      </div>



                            <div id="relatedDiv">   
                        <p>
                          <label>Related Product</label>
                          <select class="test form-control" multiple="multiple" name="related_product[]">

                            <option value="">Select Related Product</option>
                          <?php if(!empty($product_detail_inner)){

                            foreach ($product_detail_inner as $details){ 

                              if(isset($p_ids)){

                                if(in_array($details['product_id'],$p_ids)){

                                  $relted_selected = 'selected';
                                
                                } else{

                                  $relted_selected = '';
                                }


                              } else{

                                $relted_selected = '';
                              }
                              ?>

                            <option value="<?php echo $details['product_id']?>" <?php echo $relted_selected; ?>><?php echo $details['pname'];?></option>
                            <?php } }?>
                          
                          </select>
                        </p>
                      </div> 


                    <!--   <div class="form-group">
                        <label>Product Categories<span style="color:red;">*</span></label>
                          <select name="product_category" id="product_category" required="required">
                            <option value="">--select category--</option>
                            <?php 
                            if(!empty($product_category))
                            {
                              foreach($product_category as $each_category)
                              {
                                if($product_detail[0]['product_category_id']==$each_category['id'])
                                {
                                  $selected_cat="selected='selected'";
                                }
                                else
                                {
                                  $selected_cat="";
                                }
                               ?>
                               <option value="<?php echo $each_category['id'];?>" <?php echo $selected_cat;?> ><?php echo $each_category['name']; ?></option>
                               <?php
                              }

                            }
                            ?>

                          </select>
                           <?php echo form_error('product_category', '<div class="error_validate">', '</div>'); ?>
                      </div> -->

                      <div class="form-group">
                        <label>Image</label>
                         <!--  <span>Max upload size:width-1024,Height-768</span> -->
                          <input type="file" name="product_image" id="product_image" accept="image/*" value="" <?php if(empty($product_detail[0]['image_url'])){ ?>required="required" <?php } ?> />

                          <?php 
                          if(!empty($product_detail[0]['image_url'])) 
                           {
                          ?>
                            <img src="<?php echo base_url();?>uploads/products/<?php echo $product_detail[0]['image_url'];?>" width="100" height="100" />
                            <input type="hidden" name="product_image1" id="product_image1" value="<?php echo $product_detail[0]['image_url'];?>" />
                          <?php 
                            }
                          ?>
                      </div>


                       <div class="form-group">
                            <label>Product Language<span style="color:red;" >*</span></label>
                            <span class="field">
                                <?php
                                    if(!empty($product_language) && count($product_language)>0)
                                    {
                                      ?>
                                         <select class="test1 form-control" name="pro_languages[]" multiple="multiple" data-rule-required="true" data-msg-required="Please Select Product language">
                                          <?php
                                        foreach($product_language as $reData)
                                        {
                                             if(isset($l_ids)){

                                              if(in_array($reData['product_language_id'],$l_ids)){

                                                $lang_selected = 'selected';
                                              
                                              } else{

                                                $lang_selected = '';
                                              }


                                            } else{

                                              $lang_selected = '';
                                            }
                                      ?>

                                             
                                                  <option value="<?php echo $reData['product_language_id'];?>" <?php echo $lang_selected;?>><?php echo $reData['product_language_name'];?></option>
                                                
                                                <?php
                                            }
                                        ?>
                                       </select>
                                       <?php 
                                    }
                                    else
                                    {
                                        echo "No Product Language Found , Please Create The New One";
                                    }
                                ?>
                            </span>

                        </div>
                        

                       <!-- <div class="form-group">
                        <label>Course Duratio(In days)<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="course_duraion" class="smallinput" id="course_duraion" required="required" value="<?php echo $product_detail[0]['subscription_duration'];?>"  /></span>
                             <?php echo form_error('course_duraion', '<div class="error_validate">', '</div>'); ?>
                      </div> -->

                      <div class="form-group">
                        <label>Product quantity<span style="color:red;">*</span></label>
                          <span class="field"><input type="text" name="quantity" class="smallinput" id="quantity" required="required" value="<?php echo $product_detail[0]['quantity'];?>"/></span>
                           <?php echo form_error('quantity', '<div class="error_validate">', '</div>'); ?>
                      </div>

                      <div class="form-group">
                        <label>Product price<span style="color:red;">*</span></label>
                          <span class="field"><input type="text" name="product_price" class="smallinput" id="product_price" required="required" value="<?php echo $product_detail[0]['product_price']; ?>" /></span>
                           <?php echo form_error('product_price', '<div class="error_validate">', '</div>'); ?>
                      </div>

                      <div class="form-group">
                        <label>Discounted price<span style="color:red;"></span></label>
                          <span class="field"><input type="text" name="disc_price" class="smallinput" id="disc_price" value="<?php echo $product_detail[0]['disc_price']; ?>" onblur="myFunction()" /></span>
                      </div>

                      <div class="form-group">
                        <label>If Featured</label>
                        <select name="is_featured" id="is_featured">
                          <option value="yes" <?php if($product_detail[0]['is_featured']=='yes'){ echo $selected_featured="selected='selected'";}?>>Yes</option>
                          <option value="no" <?php if($product_detail[0]['is_featured']=='no'){ echo $selected_featured="selected='selected'";}?>>No</option>
                        </select>
                      </div>


                      <!-- <div class="form-group">
                        <label>Image</label>
                          <span>Max upload size:width-1024,Height-768</span>
                          <input type="file" name="featured_image" id="featured_image" accept="image/*" value="" <?php if(empty($product_detail[0]['image_featured'])){ ?>" <?php } ?> />

                          <?php 
                          if(!empty($product_detail[0]['image_featured'])) 
                           {
                          ?>
                            <img src="<?php echo base_url();?>uploads/featured-image/<?php echo $product_detail[0]['image_featured'];?>" width="100" height="100" />
                            <input type="hidden" name="featured_image1" id="featured_image1" value="<?php echo $product_detail[0]['image_featured'];?>" />
                          <?php 
                            }
                          ?>
                      </div>
 -->



                      <div class="form-group">
                        <label>Product Status</label>
                        <select name="active_inactive" id="active_inactive">
                          <option value="active" <?php if($product_detail[0]['active_inactive']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
                          <option value="inactive" <?php if($product_detail[0]['active_inactive']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
                        </select>
                      </div>




                      <div class="form-group">
                        <label>Stock Status</label>
                        <select name="stock_status" id="stock_status">
                          <option value="In Stock" <?php if($product_detail[0]['stock_status']=='In Stock'){ echo $selected_stock="selected='selected'";}?> >In Stock</option>
                          <option value="Out Of Stock" <?php if($product_detail[0]['stock_status']=='Out Of Stock'){ echo $selected_stock="selected='selected'";}?>>Out Of Stock</option>
                        </select>
                      </div>

                      <div class="form-group">
                          <h4>Short Description<span style="color:red;">*</span></h4>
                          <textarea class="form-control" name="short_disc"  id="short_disc" required="required"><?php echo $product_detail[0]['short_disc']; ?></textarea>
                      </div>

                      <div class="form-group">
                          <h4>Description<span style="color:red;"></span></h4>
                          <textarea class="form-control" name="description"  id="description" required="required"><?php echo $product_detail[0]['description']; ?></textarea>
                      </div>

                       <input type="hidden" id="check_id" name="check_id" value="<?php echo $product_detail[0]['product_id']; ?>"  />

                       <?php 

                       if(!empty($p_ids)){

                        $data_r = implode(',',$p_ids);
                        
                       } else{

                        $data_r ='';

                       }

                       $product_idr = $this->uri->segment(4);

                       ?>

                     <input type="hidden" value="<?php  echo $data_r ?>" id="for_see_selected">
                     <input type="hidden" value="<?php  echo $product_idr ?>" id="pros_idr">


                        <div class="row">
                            <div class="col-sm-12">
                              <button type="submit"  id="editbtn" >Update</button>
                              <a href="<?php echo base_url('admin/add_product/product_list')?>" button type="button" class="btn btn-danger" >Cancel</button></a>
                            </div>
                       </div>


                       
                   
        </div><!--contentwrapper-->
<!-- centercontent -->
        
  </form>
     

</div><!--bodywrapper-->
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  //CKEDITOR.replace('pfeatures');
  CKEDITOR.replace('description');
</script>
<script>
 var catid;
 function get_variant_list(catid)
 {
  
   $.ajax({

            url: '<?php echo base_url();?>admin/furnish_dashboard/get_category_variant',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
            var result_data=JSON.parse(result);
            var result_length=result_data.length;
            var cathtml="";
            if(result_length>0)
            {
          
             for(var i=0;i<result_length;i++)
             {
            cathtml+="<option value='"+result_data[i]['variant_id']+"'>"+result_data[i]['variant_name']+"</option>";
            
             }

          
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }

        

      
        $('#category_variant').html(cathtml);
              
                  
            }
     });

   $.ajax({
            url: '<?php echo base_url();?>admin/furnish_dashboard/get_cat_property_detail',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert(result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml1 ="";
              if(result_length>0)
              {
                cathtml1 +="<table class='table table-bordered table-responsive'><thead><tr><th>Property</th><th>Value</th></tr></thead>";
                for(var i=0;i<result_length;i++)
                {
                  cathtml1+="<tr><td>"+result_data[i]['property_name']+"</td><td><input name='prop["+result_data[i]['property_id']+"]' type='text'></td></tr>";
                }
                cathtml1 += "</table>";
                //alert(cathtml1);
              }
              else
              {
                cathtml1 = "";
              }
              $('.cat_pro').html(cathtml1);
        }
     });
 }
</script>

   <script type="text/javascript">
    function get_related_products()
    {
      
        var type =  jQuery("input[name='product_category_id']:checked").val();
        var pree_val = jQuery('#for_see_selected').val();
        var productr_id = jQuery('#pros_idr').val();

        jQuery.ajax({
               type : 'POST',
               url  : '<?php echo base_url('admin/add_product/get_related_products_edit')?>',
               data : {type:type,pree_data:pree_val,product_id:productr_id},
               success:function(resp)
               {
                 jQuery('#relatedDiv').html(resp);
                 jQuery('.test').fSelect();
               }
        });
    }
  </script>

<script>
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>

<script type="text/javascript">
    function check_product_name(obj)
    {
       var checkId = $('#check_id').val();
        $.ajax({
                type : 'POST',
                url  : '<?php echo site_url('admin/add_product/check_product_name')?>',
                data : {pname:obj,check_id:checkId},
                success:function(resp)
                {
                  resp = resp.trim();

                  if(resp =="SUCCESS")
                  {
                    var pname = $('#pname').val();

                    $('#pnameDiv').css('color','red');
                    $('#pname').val('');
                    $('#pnameDiv').html('Product ' +pname+ ' Already Exists');
                    return false;
                  }
                  else
                  {
                    return true;
                  }
                }

        });
    }



</script>
<script>
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>


<script>
 (function($) {
        $(function() {
            $('.test1').fSelect();
        });
    })(jQuery);
</script>

<script type="text/javascript">
    function myFunction(){

  var product_actual_price     = $('#product_price').val();
  var product_discounted_price = $('#disc_price').val();

  
  if( parseInt(product_discounted_price) > parseInt(product_actual_price)){

    alert('Alert..Discounted Price Can not more than actual price');
     $('#disc_price').val('');

  }

 }
</script>

</body>

</html>
