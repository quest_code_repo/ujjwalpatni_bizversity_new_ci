<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_product/update_language" method="post" >
        <div class="pageheader notab">
            <h1 class="pagetitle">Update Product Langugae</h1>
           
            
        </div><!--pageheader-->


         <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
        
        <input type="hidden" name="product_language_id" value="<?php echo isset($Language[0]->product_language_id)?$Language[0]->product_language_id:''?>"   />

        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Language Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="language_name" class="mediuminput" id="language_name" value="<?php echo isset($Language[0]->product_language_name)?$Language[0]->product_language_name:''?>" required="required" /></span>
            </p>

            <p>
              <label>Language Status<span style="color:red;">*</span></label>
               <select name="active_status" id="active_status">
                <?php
                    if($Language[0]->language_status == "Active")
                    {
                        $active   = "selected";
                        $inactive = "";
                    }
                    else
                    {
                        $active   = "";
                        $inactive = "selected";
                    }
                ?>
                 <option value="Active" <?php echo $active;?>>Active</option>
                 <option value="Inactive" <?php echo $inactive;?>>Inactive</option>
              </select>
              </p>

            
          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Update Language</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>