 <div class="centercontent">
    
      <div class="pageheader notab">
        <h1 class="pagetitle"><?php echo $title; ?></h1>
        <?php 
        if($this->session->flashdata('message'))
        {
         ?>
         <div class="alert alert-success">
          <?php echo $this->session->flashdata('message'); ?>
         </div>
         <?php
        }
        else if($this->session->flashdata('error'))
        {
         ?>
         <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error'); ?>
         </div>
         <?php
        }
          ?> 
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
      
               <?php echo $content; ?>
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>