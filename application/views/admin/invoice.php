<script>
function printData()
{
   var divToPrint=document.getElementById("contentwrapper");
   newWin= window.open('', 'Order Invoice', 'height=auto,width=800');
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
}
</script>
 <div class="centercontent">
     <form method="post">
      <div class="pageheader notab">
           <table width="90%"><tr><td>
    <h1 class="pagetitle">Order Details</h1></td><td align="right">
    <a class="btn btn_orange btn_print" href="javascript:void(0);" onclick="printData();">
<span>Print</span>
</a>
  </td></tr></table>
       </div><!--pageheader-->
   </form>
        <div id="contentwrapper" class="contentwrapper">
      <?php $dis=0; ?>
              <table style="width:600px;margin:0px;padding:0px;border:1px solid #CCC;border-collapse:collapse;font-size:12px;">
  <tr style="border:1px solid #CCC;border-collapse:collapse;">
    <td width="50%" style="border:1px solid #CCC;border-collapse:collapse;padding:5px;vertical-align:top;"><img src="<?php echo base_url(); ?>assets/front/images/logo.jpg" height="60" /></td>
    <td align="right" style="border:1px solid #CCC;border-collapse:collapse;padding:5px;vertical-align:top;"><strong>Gioteak,</strong><br />
      5 SR Compound,Lasudia Mori,Dewas Naka,Indore 452010<br />
      Tel : 9109119440 <br />
      e-mail : care@gioteak.com   </td>
  </tr>
  <tr style="border:1px solid #CCC;border-collapse:collapse;">
    <td colspan="2" style="padding:5px;" align="center"><b>Invoice</b><span style="float:right;"><?php// echo $order[0]['order_code']; ?></span></td>
  </tr>
  <tr>
    
    <td colspan="2" style="border:1px solid #CCC;border-collapse:collapse;padding:5px;"><strong>Billing Address :</strong> <br />

    <b><?php echo $order[0]['billing_first_name']; ?> <?php echo $order[0]['billing_last_name']; ?></b><br>Email-Id:-<?php echo $order[0]['billing_emailid']; ?><br>Address:-<?php echo $order[0]['billing_address']; ?><br> Pincode- <?php echo $order[0]['billing_pincode']; ?>
   <br /> Contact : <?php echo $order[0]['billing_mobile']; ?>    </td>
  </tr>
   <tr>
    <td style="border:1px solid #CCC;border-collapse:collapse;padding:5px;"><strong>Order Number :</strong> <?php echo $order[0]['order_code']; ?>    </td>
    <td style="border:1px solid #CCC;border-collapse:collapse;padding:5px;"><strong>Order Date :</strong>  <?php echo date('Y-m-d',strtotime($order[0]['created_on'])); ?>    </td>

  </tr>
  <tr style="border:1px solid #CCC;border-collapse:collapse;">
    <!-- <td colspan="2" style="padding:5px;" align="center">Tin Number : 23249057443</td> -->
  </tr>
  <tr>
    <td colspan="2"><table style="width:600px;margin:0px;padding:0px;border:1px solid #CCC;border-collapse:collapse;font-size:12px;">
      <tr align="">
        <td width="163" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Product Name</strong></td>
        <td width="163" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Product At</strong></td>

        <td width="163" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Product Image</strong></td>

        <td width="" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Quantity</strong></td>
        <td width="110" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Amount</strong></td>
		    
      </tr><?php
      // echo '<pre>'; 
      // print_r($order);
      // echo '</pre>';
      // print_r($info);
      // die()
      ?>

      <?php $total=0; if(!empty($order)) {
		  foreach($order as $item) { 
		
		  ?>
      <tr>
        <?php $this->method_call =& get_instance();
          $product = $this->method_call->fetch_product_byid($item['product_id']); ?>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;text-align:center;">
    		<?php echo $product->pname; ?> <br>
    		</td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;text-align:center;">
        <?php echo $item['attr_value']; ?> <br>
        </td>
        <td>
          <?php if(!empty($item['colored_image'])){ ?>
           <img class="media-object img-responsive" src="<?php echo base_url();?>uploads/gallery/<?php echo $item['colored_image'] ?>" alt="cat img">
          <?php 
            }
            else 
            {
              ?>
               <img class="media-object img-responsive" src="<?php echo base_url();?>uploads/products/<?php echo $item['image_url'];?>" alt="cat img">
              <?php
            }
          ?>
        </td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;text-align:center;">
        <?php echo $item['product_quantity']; ?> <br>
        </td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;text-align:center;">
        <?php echo $item['product_price']; ?> <br>
        </td>
		<?php
		
			$price=$item['product_price']*$item['product_quantity']; 
		
		?>
      </tr>
      <?php 
	 // $total=$total+$ss; 
	  $total+=$price; 
	  
	  }} ?>
    </table></td>
    </tr>
  <tr>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Sub Total</td>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Rs. <?php echo number_format($total,2, '.', ''); ?></td>
  </tr>
  <?php $cou = 0;
  if($order[0]['coupon_id'] != 0) {?>
   <tr>
    <?php $this->method_call =& get_instance();
          $coupon = $this->method_call->fetch_coupon_byid($order[0]['coupon_id']); ?>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Coupon Discount</td>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><?php if($coupon->discount_type == 'flat'){ echo 'Rs'; } else { echo '%'; } ?>. <?php echo $coupon->discount; ?></td>
  </tr>
  <?php if($coupon->discount_type == 'flat'){ $cou = $coupon->discount; } else { $cou = ($total*$coupon->discount)/100;  } ?>
  <?php } ?>
  <?php 
  $order_total = $total-$cou; 
   if($single_order->delivery_status=='delivered')
    {
      $total_paid=$order_total;
      $remaining=0;
    }
    else
    {

         if($single_order->order_payment_mod==25) 
         {
           
            $total_paid=$order_total*25/100;
            $remaining=$order_total- $total_paid;

         }
         else
         {
             $total_paid=$order_total;
             $remaining=0;
         }


    }
   ?>
  <tr>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Total (incl. all taxes)</td>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Rs. <?php echo number_format(($order_total),2, '.', '') ?></td>
  </tr>
  <tr>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Paid Amount</td>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Rs. <?php echo number_format(($total_paid),2, '.', '') ?></td>
  </tr>
  <tr>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Remaining Amount</td>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Rs. <?php echo number_format(($remaining),2, '.', '') ?></td>
  </tr>
  
 
  <tr>
    <td colspan="2" align="left" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><br />
      Thank you for your business! <br />
	  <strong>FOR ORDER LOGIN TO<a href="#"> www.gioteak.com</a> OR CALL ON 9109119440</strong> <br />
	  * T &amp; C apply </td>
    </tr>
</table>

        </div><!--contentwrapper-->
            
        
</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>