
<script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo admin_assets; ?>js/custom/tables.js"></script>

<script>
function checkchk()
{
	if(document.getElementById("chk").checked == true)
	{
		document.getElementById("sname").value=document.getElementById("name").value;
		document.getElementById("sphone").value=document.getElementById("phone1").value;
		document.getElementById("saddress").value=document.getElementById("address").value;
		document.getElementById("szipcode").value=document.getElementById("zipcode").value;
    document.getElementById("city1").value=document.getElementById("city").value;
		
		var cur_state=document.getElementById("state").value;
		jQuery('#state1').val(cur_state);
		var cur_city=document.getElementById("city").value;
		jQuery('#city1').val(cur_city);
			
	}
	else {
	    document.getElementById("sname").value='';
		document.getElementById("sphone").value='';
		document.getElementById("saddress").value='';
		document.getElementById("szipcode").value='';
       document.getElementById("city1").value='';
		
		var cur_state=0;
		jQuery('#state1').val(cur_state);
	}
}
$("input").val()
/*function setSelectedIndex(s, i)
{
s.options[i].selected = true;
return;
}*/
</script>
        
    <div class="centercontent tables">
    
     <div class="pageheader">
    <h1 class="pagetitle">Add Order</h1>
   
    <ul class="hornav">
      <li class="current"><a href="#updates">ORDER ITEMS</a></li>
      <li><a href="#activities">CUSTOMER DETAILS</a></li>
    </ul>
  </div>
        
    
        <div id="contentwrapper" class="contentwrapper"> 
        
        <div id="updates" class="subcontent">
        <form action="<?php echo base_url(); ?>admin/create_order/update_cart" class="stdform" method="post">
      <div class="mini-cart">
      <?php  $this->method_call =& get_instance();
	  $this->method_call->cart_items();
	  
	  ?>
            
           
      </div>
        </form>
    </div>
    
    <div id="activities" class="subcontent" style="display: none;">
      <form name="frm" method="post" class="stdform" action="<?php echo base_url(); ?>admin/create_order/order">
          <div style="width:46.1%;float:left;">
          <div class="widgetbox">
            <div class="title">
              <h3>Billing Address</h3>
            </div>
            <input type="hidden" name="customer_id" value="" />
            <table width="100%" height="300">
              <tr>
                <td width="25%">Name</td>
                <td><input id="name" name="name" value="" title="First Name" maxlength="255" type="text" required></td>
              </tr>
              <tr>
                <td>Mobile Number</td>
                <td><input name="phone" title="Telephone" autocomplete="off" onkeyup="loaddetails(this.value);" value="" id="phone1" type="text" required></td>
              </tr>
              
              <tr>
                <td> State/Province</td>
                <td><select name="state_id" id="state" class="form-control" onchange="get_city()">
                    <option>--Please select region, state or province--</option>
                    <?php foreach($states as $state_name) { ?>
                    <option value="<?php echo $state_name['id']; ?>" ><?php echo $state_name['name']; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
              <tr>
                <td>City</td>
                <td><select class="" required id="city" name="city" style="width:80%">
                        <option>-- Please select city --</option>
                        <?php foreach($cities as $city) {?>
                        <option value="<?php echo $city['id']; ?>" ><?php echo $city['name']; ?></option>
                        <?php } ?>
                    </select></td>
              </tr>
              <tr>
                <td>Street Address</td>
                <td><textarea name="address"  id="address" placeholder="Street Address" required></textarea></td>
              </tr>
              <tr>
                <td>Zip/Postal Code </td>
                <td><input name="zipcode" value="" title="Zip/Postal Code" id="zipcode" type="text" required></td>
              </tr>
              <tr>
                <td>email </td>
                <td><input name="email" value="" title="email" id="email"  type="text" required></td>
              </tr>
            </table>
          </div>
        </div>
        <div style="width:46.1%;float:left;">
          <div class="widgetbox">
            <div class="title">
              <h3>Shipping Address</h3>
            </div>
            <table width="100%" height="300">
              <tr>
                <td width="25%"><input type="checkbox" name="chk" onclick="checkchk();" id="chk" value="1"/></td>
                <td> Same as Billing Address </td>
              </tr>
              <tr>
                <td> Name</td>
                <td><input id="sname" name="sname" value="" title="First Name" type="text" required></td>
              </tr>
              <tr>
                <td> Telephone </td>
                <td><input name="sphone"  title="Telephone" value="" id="sphone" type="text" required></td>
              </tr>
              <tr>
                <td> State/Province </td>
                <td><select name="sstate_id" id="state1" onchange="get_city()">
                    <option>--Select State--</option>
                    <?php foreach($states as $state_name) { ?>
                    <option value="<?php echo $state_name['id']; ?>"><?php echo $state_name['name']; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
              <tr>
                <td> City </td>
                <td><select class="" required id="city1" name="ship_city" style="width:80%">
                        <?php foreach($cities as $city) {?>
                        <option value="<?php echo $city['id']; ?>"><?php echo $city['name']; ?></option>
                        <?php } ?>
                    </select></td>
              </tr>
              <tr>
                <td> Street Address </td>
                <td><textarea name="saddress" id="saddress" placeholder="Street Address" required></textarea></td>
              </tr>
              <tr>
                <td> Zip/Postal Code </td>
                <td><input name="szipcode" value="" title="Zip/Postal Code" id="szipcode"  type="text" required></td>
              </tr>
               <tr>
               <td><button type="submit" class="submit radius2">Create Order</button></td>
               </tr>
            </table>
            
          </div>
        </div>
      </form>
    </div>
        
          <div class="contenttitle2">
                	<h3>Product List</h3>
                </div><!--contenttitle-->
                <table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable2">
                    <colgroup>
                        <col class="con0" style="width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th class="head0">Product Name</th>
                            <th class="head1">Price</th>
                            <th class="head0">Unit</th>
                            <th class="head1">Quantity</th>
                            <th class="head0">Add to Cart</th>
                        </tr>
                    </thead>
                  
                    <tbody>
                    <?php $i=0; if(!empty($products)) {
						foreach($products as $details) { $i++; ?>
                        <tr class="gradeX">
                          <td align="center"><?php echo $i; ?></td>
                            <td><?php echo $details['pname']; ?> </td>
                            <td><span id="price<?php echo $details['product_id']; ?>"><?php 
			$this->method_call =& get_instance();
			$quarate = $this->method_call->get_one_quantity($details['product_id']);
			if(!empty($quarate)) {
			if($quarate[0]['disc_price']=="0.00"){ $price12=$quarate[0]['org_price']; } else{  $price12=$quarate[0]['org_price'] - $quarate[0]['disc_price']; }
			echo $price12; }
			?></span></td>
                            <td> <?php 
		  $this->method_call =& get_instance();
			$quantity = $this->method_call->products_quantity($details['product_id']); 
			?>
           <select id="qua<?php echo $details['product_id']; ?>" class="form-control my-select" onchange="loadprice(<?php echo $details['product_id']; ?>,this.value);">
              <option value="">Select Quantity</option>
          <?php
		   if(!empty($quantity)) {
		    foreach($quantity as $qua_info) {
		   ?>
            <option <?php if($quarate[0]['qua_id']==$qua_info['qua_id']) { echo 'selected="selected"'; } ?> value="<?php echo $qua_info['qua_id']; ?>"><?php echo $qua_info['weight']; ?> <?php echo $qua_info['name']; ?></option>
           <?php }} ?>
            </select></td>
                            <td class="center"><input type="text" name="qua[]" value="1" style="width:50px;" /></td>
                            <td class="center"> <button onclick="addtocart(<?php echo $details['product_id']; ?>,1);" class="button btn-cart" type="button"><span>Add to Cart</span></button></td>
                        </tr>
                        <?php }} ?>
                        
                    </tbody>
                </table>
        
        </div><!--contentwrapper-->
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->
<script>
        function get_city()
        {
            //alert('inside');
            var state_id = $('#state').val();
            
            jQuery.ajax({
                url: '<?php echo base_url(); ?>home/getcity1/' + state_id, 
                success: function (result) {
                //alert(result);
                $('#city').html(result);
            }
        });
    }

</script>
</body>

</html>
