<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">
<style type="text/css">
    .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}
</style>

<div class="centercontent tables">
  <form id="edit-chapter-form" class="stdform"  method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Edit Chapter</h1>
         
          
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper"> 
        <!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
          
            echo $this->session->flashdata('error'); 
          
          }
         ?>

         <input type="hidden" name="ch_id" value="<?php echo $chapter_detail[0]['ch_id'];?>"   />

         <div class="form-group">
          <label>Course Name</label>
          <select name="product_id" id="product_id" class="test">
            <option value="">--select course name--</option>
            <?php 
              if(!empty($course_list))
              {
              foreach($course_list as $each_sem_id)
              {
              if($chapter_detail[0]['sem_id']==$each_sem_id['product_id'])
              { 
              $select_id="selected='selected'";
              }
              else
              {
              $select_id="";
              } 
              ?>
            <option value="<?php echo $each_sem_id['product_id'];?>" 
                    <?php echo $select_id;?> >
            <?php echo $each_sem_id['pname'];?>
            </option>
            <?php
            } 
            }
            ?>
        </select>
         </div>

        <div class="form-group">
          <label>Chapter Name<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="ch_name" class="smallinput" id="ch_name" required="required" value="<?php echo $chapter_detail[0]['ch_name'];?>"  /></span>
               <?php echo form_error('ch_name', '<div class="error_validate">', '</div>'); ?>
        </div>


        <div class="form-group">
          <label>No of Topics<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="ch_no_of_lecture" class="smallinput descPriceNumVal" id="ch_no_of_lecture" required="required" value="<?php echo $chapter_detail[0]['ch_no_of_lecture'];?>"  /></span>
               <?php echo form_error('ch_no_of_lecture', '<div class="error_validate">', '</div>'); ?>
        </div>
        
        <div class="form-group">
          <label>Chapter Number<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="chapter_number" class="smallinput descPriceNumVal" id="chapter_number" required="required" value="<?php echo $chapter_detail[0]['chapter_number']; ?>" /></span>
             <?php echo form_error('chapter_number', '<div class="error_validate">', '</div>'); ?>
        </div>

      
     <!--    <div class="form-group">
            <h4>Description<span style="color:red;"></span></h4>
            <textarea class="form-control" name="ch_description"  id="ch_description" required="required"><?php echo $chapter_detail[0]['ch_description']; ?></textarea>
        </div> -->

        <div class="form-group">
          <label>Chapter Status</label>
          <select name="ch_status" id="ch_status">
            <option value="active" <?php if($chapter_detail[0]['ch_status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($chapter_detail[0]['ch_status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
        </div>

        <div class="text-center" style="padding-bottom: 20px;"> 
     
        <button type="button" class="btn btn-orange" id="addbtn" onclick="return add_chapter();">Update</button>
        <a href="<?php echo base_url();?>admin/add_semester/chapter_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" ></a> 
      </div>
      <div class="clearfix"></div>
          
          <!-- <div class="form-group text-center">
              <button class="submit radius2" id="editbtn">Save</button>
          </div> -->
                 
      </div><!--contentwrapper-->
  <!-- centercontent -->
  </form>    
</div><!--bodywrapper-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('ch_description');
  CKEDITOR.replace('description');
</script>


<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script>
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>


<script type="text/javascript">
        $(document).ready(function () {
        //called when key is pressed in textbox
        $(".orgPriceNumVal").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $(".orgPriceNumVal").attr("placeholder","Digits Only");
        return false;
        }
        });

        $(".descPriceNumVal").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57)) {
        //display error message
        $(".descPriceNumVal").attr("placeholder","");
        return false;
        }
        });

    });

/******END: Check Price Number  Input**********/

</script>


<script type="text/javascript">

   function add_chapter()
   {
      var chap_name = $('#ch_name').val();
      var course_id = $('#product_id').val();
      var ch_no_of_lecture = $('#ch_no_of_lecture').val();
      var chap_no   = $('#chapter_number').val();
   //   var desc        = $('#ch_description').val();

     
      if(course_id == "")
      {
           $('#chapDiv').html('Please Select Course ');
            return false;
      }

       else if(chap_name == "")
      {
        $('#chapDiv').html('Please Enter Chapter Name');
        return false;
      }

      else if(ch_no_of_lecture == "")
      {
           $('#chapDiv').html('Please Enter No Of Topic');
             return false;
      }

      else if(chap_no == "")
      {
           $('#chapDiv').html('Please Enter Chapter No');
             return false;
      }

      else
      {
        

            $.ajax({
                  type : 'POST',
                  url  : '<?php echo base_url("admin/add_semester/edit_chapter")?>',
                  data : $('#edit-chapter-form').serialize(),
                  success:function(resp)
                  {
                      resp = resp.trim();

                     

                      if(resp == "SUCCESS")
                      {
                            
                            $('#chapDiv').css('color','green');
                            $('#chapDiv').html('Chapter Updated Successfully');
                            window.location.href="<?php echo base_url('admin/add_semester/chapter_list')?>";
                            return true;
                      }
                      else
                      {
                          $('#chapDiv').html('Something Went Wrong');
                      }
                  }


        });
      }

        
   }
 </script>

</body>

</html>
