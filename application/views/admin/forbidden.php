 <div class="centercontent">
    
      <!--pageheader-->
        <div class="contentwrapper padding10">
    	<div class="errorwrapper error403">
        	<div class="errorcontent">
                <h1>403 Forbidden Access</h1>
                <h3>Your dont' have permission to access this page.</h3>
                
                <p>This is likely to be caused by one of the following</p>
                <ul>
                    <li>The author of the page has intentionally limited access to it.</li>
                    <li>The computer on which the page is stored is unreachable.</li>
                    <li>You like this page.</li>
                </ul>
                <br />
                <button class="stdbtn btn_black" onclick="history.back()">Go Back to Previous Page</button> &nbsp; 
                
            </div><!--errorcontent-->
        </div><!--errorwrapper-->
    </div>
        <!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>