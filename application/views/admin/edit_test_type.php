<div class="centercontent tables">
  <form id="type-form" class="stdform" action="" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Edit Test_Type</h1>
         
          
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper"> 
        <!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
          
            echo $this->session->flashdata('error'); 
          
          }
         ?>
        <div class="form-group">
          <label>Test Type<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="test_type" class="smallinput" id="test_type" required="required" value="<?php echo $type_detail[0]['test_type'];?>"  /></span>
               <?php echo form_error('test_type', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group">
          <label> Type Status</label>
          <select name="status" id="status">
            <option value="active" <?php if($type_detail[0]['status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($type_detail[0]['status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
        </div>

        <div class="text-center" style="padding-bottom: 20px;"> 
        <a href="<?php echo base_url();?>admin/add_semester/test_type_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
     
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
      </div>
      <div class="clearfix"></div>
          
          <!-- <div class="form-group text-center">
              <button class="submit radius2" id="editbtn">Save</button>
          </div> -->
                 
      </div><!--contentwrapper-->
  <!-- centercontent -->
  </form>    
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('type_description');
  CKEDITOR.replace('description');
</script>
<script>
 var catid;
 function get_variant_list(catid)
 {
  
   $.ajax({

            url: '<?php echo base_url();?>admin/furnish_dashboard/get_category_variant',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
            var result_data=JSON.parse(result);
            var result_length=result_data.length;
            var cathtml="";
            if(result_length>0)
            {
          
             for(var i=0;i<result_length;i++)
             {
            cathtml+="<option value='"+result_data[i]['variant_id']+"'>"+result_data[i]['variant_name']+"</option>";
            
             }

          
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }

        

      
        $('#category_variant').html(cathtml);
              
                  
            }
     });

   $.ajax({
            url: '<?php echo base_url();?>admin/furnish_dashboard/get_cat_property_detail',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert(result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml1 ="";
              if(result_length>0)
              {
                cathtml1 +="<table class='table table-bordered table-responsive'><thead><tr><th>Property</th><th>Value</th></tr></thead>";
                for(var i=0;i<result_length;i++)
                {
                  cathtml1+="<tr><td>"+result_data[i]['property_name']+"</td><td><input name='prop["+result_data[i]['property_id']+"]' type='text'></td></tr>";
                }
                cathtml1 += "</table>";
                //alert(cathtml1);
              }
              else
              {
                cathtml1 = "";
              }
              $('.cat_pro').html(cathtml1);
        }
     });
 }
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#type-form").validate({
    rules:{
      
      test_type: {
        required: true,
      }

    },
    
    messages:{
      test_type: "Please Enter Lecture Type"

    },
       
  });
</script>

</body>

</html>
