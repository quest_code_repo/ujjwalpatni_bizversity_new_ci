<div class="centercontent tables">
  <form class="stdform" action="" method="post" enctype="multipart/form-data">
    <div class="pageheader notab">
      <h1 class="pagetitle">Add e-Leaarning Product</h1>
    </div><!--pageheader-->

    <div id="contentwrapper" class="contentwrapper">
      <!-- <div class="one_half"> -->
      <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
        <p>
          <label>Product Name<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="pname" class="smallinput" id="pname" required="required" /></span>
            <?php echo form_error('pname', '<div class="error_validate">', '</div>'); ?>
        </p>

        <p>
          <label>Categories<span style="color:red;">*</span></label>
          <select name="product_category" id="product_category" required="required" class="count_cat"> 
            <option value="">--select category--</option>
            <?php 
             if(!empty($product_category))
             {
              foreach($product_category as $each_category)
              {
               ?>
               <option value="<?= $each_category['id'];?>"  ><?php echo $each_category['name']; ?></option>
               <?php
              }
             }
            ?>
          </select>
        </p>
       <!--  <p>
          <label>Course for this product<span style="color:red;">*</span></label>
          <select name="product_course" id="product_course" required="required" class="count_cat"> 
            <option value="">--select course--</option>
            <?php 
             if(!empty($all_course))
             {
              foreach($all_course as $course)
              {
               ?>
               <option value="<?= $course['sem_id'];?>"  ><?php echo $course['sem_name']; ?></option>
               <?php
              }
             }
            ?>
          </select>
        </p>
        <p><?php echo form_error('product_category', '<div class="error_validate">', '</div>'); ?></p>
       -->


        <p>
          <label>Video Url<span style="color:red;">*</span></label>
          <input type="text" name="video_url" id="video_url"  class="smallinput"  required="required" />
        </p>


              <p>
                <label>Course Type<span style="color:red;"  ></span></label>
                  <span class="field" required="required" >
                  <label class="radio-inline"><input type="radio" name="course_type" value="Business" checked>Business</label>
                    <label class="radio-inline" style="float: none;"><input type="radio" name="course_type" value="Individual">Individual</label>
                  </span>

              </p>

        <p>
          <label>Product Image<span style="color:red;">*</span></label>
          <input type="file" name="image_url" id="image_url" accept="image/*" value="" required="required" onchange="displayPreview(this.files);"/>
          <?php echo form_error('image_url', '<div class="error_validate">', '</div>'); ?>
        </p>

        <p>
          <label>Product price<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="product_price" class="smallinput" id="product_price" onkeyup="myFunction();" onblur="myFunction();" required="required"/></span>
           <?php echo form_error('product_price', '<div class="error_validate">', '</div>'); ?>

        </p>

        <p>
          <label>Discounted Price<span style="color:red;"></span></label>
          <span class="field"><input type="text" name="disc_price" class="smallinput" id="disc_price" onkeyup="myFunction();" onblur="myFunction();" required="required"/></span>

        </p>

          <p>
          <label>Course Duration(in months)<span style="color:red;">*</span></label>
          <input type="text" name="course_duration" id="course_duration" class="smallinput"   required="required" />
        </p>


      
        <p>
          <label>If Featured</label>
          <select name="is_featured" id="is_featured">
            <option value="no">No</option>
            <option value="yes">Yes</option>
          </select>
        </p>

        <p>
          <label>Featured Image<span style="color:red;"></span></label>
          <input type="file" name="featured_image" id="featured_image" accept="image/*" value="" onchange=""/>
          <?php //echo form_error('featured_image', '<div class="error_validate">', '</div>'); ?>
        </p>

        <p>
          <label>Product Status</label>
          <select name="active_inactive" id="active_inactive">
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
          </select>
        </p>


        
        <p >
          <label>Video Duration (Hours)<span style="color:red;">*</span></label>
          <input name="hrs" type='text' class="smallinput" id="hrs" required="required"/></input>
        </p>
        <p >
          <label>Articles<span style="color:red;">*</span></label>
          <input name="articles" type='text' class="smallinput" id="articles" required="required"/></input>
        </p>
        <p >
          <label>Supplemental Resources<span style="color:red;">*</span></label>
          <input name="supplemental" type='text' class="smallinput" id="supplemental" required="required"/></input>
        </p>
        <p >
          <label>Full lifetime access<span style="color:red;">*</span></label>
          <!-- <input name="access" type='text' class="smallinput" id="access" required="required"/></input> -->
          <select name="access">
            <option value="Yes">Yes</option>
            <option value="No">No</option>
          </select>
        </p>
        <p >
          <label>Access on mobile and TV </label>
          <!-- <input name="mobile" type='text' class="smallinput" id="mobile" required="required"/></input> -->
          <select name="mobile" class="smallinput" id="mobile" required="required">
            <option value="Yes">Yes</option>
            <option value="No">No</option>
          </select>
        </p>
        <p >
          <label>Certificate of Completion<span style="color:red;">*</span></label>
        <!--   <input name="Certificate" type='text' class="smallinput" id="Certificate" required="required"/></input> -->

           <select name="Certificate" class="smallinput" id="Certificate" required="required">
            <option value="Yes">Yes</option>
            <option value="No">No</option>
          </select>
        </p>
        <p >
          <label>Tags: <span style="color:red;">*</span></label>
          <input type="text" name="tags"  id="tags" data-role="tagsinput" placeholder="Tags" class="smallinput" />
          <!-- <input name="articles" type='text' class="smallinput" id="articles" required="required"/></input> -->
        </p>
        
        <div class="col-md-2">
          <label>What I learn<span style="color:red;">*</span></label>
        </div>
        <div class="col-md-10">
          <textarea name="short_disc" class="smallinput" id="short_disc" required="required"/></textarea>
        </div>
        
      
        <div class="col-md-2">
          <label>Description<span style="color:red;">*</span></label>
        </div>
        <div class="col-md-10">
          <textarea name="description" class="smallinput" id="description" required="required"/></textarea>
        </div>
        <!--  </div> -->
        <!-- </div>contentwrapper -->
        <p class="stdformbutton">
          <button class="submit radius2" id="addbtn">Add Product</button>
        </p>
    </div><!-- centercontent -->
  </form>
</div>

</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('description');
  CKEDITOR.replace('short_disc');
  CKEDITOR.replace('pfeatures');
</script>
<script>

  function myFunction(){

  let product_actual_price = parseFloat($('#product_price').val());
  let product_discounted_price = parseFloat($('#disc_price').val());

  if( product_discounted_price > product_actual_price){
    alert('Alert..Discounted Price Can not more than actual price');
    // alert(product_actual_price+'-'+product_discounted_price);
     $('#disc_price').val('');
  }

 }
</script>
</body>
</html>
