<link rel="stylesheet" href="<?php echo base_url(); ?>assets/timepicker/jquery.simple-dtpicker.css">
<style type="text/css">
  .sm-input {
  width: 20% !important;
}
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<div class="centercontent tables">
  <form class="stdform" id="test-form" action="<?php echo base_url(); ?>admin/add_semester/add_test" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Add Test</h1>
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper">
        <!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
            echo $this->session->flashdata('error'); 
          }
         ?>

        <p>
          <label>Course Name<span style="color:red;">*</span></label>
           <select name="sem_id" id="sem_id" onchange="get_chapter_course_wise(this.value);" required="required" >
            <option value="">Select Course</option>
            <?php foreach ($course_list as $key => $value) {
             ?>
            <option value="<?=$value['product_id']?>"><?=$value['pname']?></option>
           <?php }  ?>
            
           </select>
           <?php echo form_error('sem_id','<div class="error_validate">','</div>'); ?>
        </p>


          <p>
          <label>Chapter Name<span style="color:red;">*</span></label>
          
           <select name="chapter_id" id="chapter_id" required="required" >
          
           </select>
           
        </p>

      


        <p>
          <label>Test Name<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="test_name" class="smallinput" id="test_name" required="required" /></span>
            <?php echo form_error('test_name', '<div class="error_validate">', '</div>'); ?>
        </p>

         <p>
          <label>Test Type<span style="color:red;">*</span></label>
           <select name="choice_data" id="choice_data" required="required" >
            <option value="">Select Course</option>
            
            <option value="1">Choice Based</option>
            <option value="2">Descreptive</option>
          
            
           </select>
           <?php echo form_error('choice_data','<div class="error_validate">','</div>'); ?>
        </p>


         <p>
          <label>Ideal Time Duration(in min)<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="ideal_time_duration" class="smallinput" id="ideal_time_duration" required="required">
            </span>
            <?php echo form_error('ideal_time_duration', '<div class="error_validate">', '</div>'); ?>
        </p>

     
        <p>
          <label>Total Marks.<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="total_marks" class="smallinput" id="total_marks" required="required"></span>
        </p>

       
        <p>
          <label>Start Time<span style="color:red;">*</span></label>
          <span class="field">
            <input type="text" class="smallinput " name="date10" value=""> 
          </span>
        </p>
        <p>
          <label>End Time<span style="color:red;">*</span></label>
          <span class="field">
            <input type="text" class="smallinput " name="date11" value="">
          </span>
        </p>

<!-- 
  <div class="container">
    <div class="row">
        <div class='col-sm-6'>
            <label>Start Time<span style="color:red;">*</span></label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="date10" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
    </div>
</div>


  <div class="container">
    <div class="row">
        <div class='col-sm-6'>
            <label>End Time<span style="color:red;">*</span></label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="date11" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker();
            });
        </script>
    </div>
</div> -->
                
           
        <p>
          <label>Status</label>
          <select name="test_status" id="test_status">
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
          </select>
        </p>

                 
      </div><!--contentwrapper-->

      <div class="text-center" style="padding-bottom: 20px;">      
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
        
        <a href="<?php echo base_url();?>admin/add_semester/test_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
      </div>
      <div class="clearfix"></div>

    
      <!-- <p class="stdformbutton">
        <button class="submit radius2" id="addbtn">Save</button>
      </p> -->

  </form>
     
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('test_description');
  CKEDITOR.replace('pfeatures');
</script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/timepicker/jquery.simple-dtpicker.js"></script>
   <script type="text/javascript">
      (function($) {
        $(function(){
          $('*[name=date10]').appendDtpicker({
            "closeOnSelected": true
          });
        });
        $(function(){
          $('*[name=date11]').appendDtpicker({
            "closeOnSelected": true
          });
        });
//         $('#test_start_time').appendDtpicker({
//   "autodateOnStart": false
// });
        })(jQuery);
      </script>


<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#test-form").validate({
    rules:{
      
      lecture_id: {
        required: true,
      },
       test_type: {
        required: true,
      },
      test_name: {
        required: true,
      },
      
      ideal_time_duration: {
        required: true,
      },
      test_no_of_question: {
        required: true,
        number:true
      },
      test_passing_marks: {
        required: true,
        number:true
      },
      

    },
    
    messages:{
      lecture_id: "Please Select Topic Name",
      test_type: "Please Select Test Type",
      test_name: "Please Enter Name",
      description_url: "Please Enter Valid Test Description URL",
      desc_video_pass: "Please Enter Test Description Video Password",
      ideal_time_duration: "Please Enter Time Duration",
      test_no_of_question: "Please Enter Question in Number",
      test_passing_marks: "Please Enter number",
      test_energy_point: "Please Enter number"
      

    },
       
  });
</script>

<script type="text/javascript">
  function get_test_type(value) {

    if (value == 1) {
      $('#lecture').hide();
      $('#chapter').hide();
      $('#semester').show();
      $('#radio').hide();
      $('#radio-practice').hide();
    }
    if (value == 2) {
      $('#lecture').show();
      $('#chapter').hide();
      $('#semester').hide();
      $('#radio').hide();
      $('#radio-practice').hide();
    }
    if (value == 3) {
      $('#radio-practice').show();
      $('#lecture').hide();
      $('#chapter').hide();
      $('#semester').hide();
      $('#radio').hide();
    }    
    if (value == 4) {
      $('#radio').show();
      $('#lecture').hide();
      $('#chapter').hide();
      $('#semester').hide();
      $('#radio-practice').hide();
    }

    
    $("#radio-topic, #radio-chap").change(function () {
        if ($("#radio-topic").is(":checked")) {
            $('#lecture').show();
            $('#chapter').hide();
        }
        else if ($("#radio-chap").is(":checked")) {
            $('#lecture').hide();
            $('#chapter').show();
        }

      });

    $("#radio-chapter, #radio-sem").change(function () {
        if ($("#radio-chapter").is(":checked")) {
            $('#chapter').show();
            $('#semester').hide();
        }
        else if ($("#radio-sem").is(":checked")) {
            $('#semester').show();
            $('#chapter').hide();
        }

      });

  }
  
</script>
  


      <script type="text/javascript">
    function get_chapter_course_wise(courseId)
    {
      $.ajax({
                type : 'POST',
                url  : '<?php echo base_url("admin/add_semester/get_chapter_course_wise")?>',
                data : {'course_id':courseId},
                success:function(resp)
                {
                    resp = resp.trim();
                    $('#chapter_id').html(resp);
                }

      })
    }
</script>

</body>

</html>
