 <?php $message=$this->session->flashdata('user_activities'); ?>

<script>

error = '<?php echo $message?>'
if(error !='') alert(error);
	
//**********Start : Function for Preview Patient Profile Photo**********

   function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#pre_photo').attr('src', e.target.result);
       }
        reader.readAsDataURL(input.files[0]);
       }
    }

 //**********End : Function for Preview Patient Profile Photo**********

</script>
 
 
<div class="centercontent">
    
    <div class="pageheader notab">
        <h1 class="pagetitle">Update Profile</h1>        
    </div><!--pageheader-->
    
    <div id="contentwrapper" class="contentwrapper">        	
        <div class="subcontent">         
        	<?php
				include("session_msg.php"); 									
			?>
            <form id="form1" class="stdform" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>admin/authentication/change_profile">
             	<p>
                	<label>Username</label>
                    <span class="field"><input type="text" name="name" id="name" style="width:35%" disabled="true" value="<?php echo $row[0]['emp_code']; ?>"  class="mediuminput" /></span>
                </p>
				<p>
                	<label>Email</label>
                    <span class="field"><input type="text" name="email" id="email"  style="width:35%" disabled="true" value="<?php echo $row[0]['personal_email']; ?>" class="mediuminput" /></span>
                </p>
				<p>
                	<label>Full Name</label>
                    <span class="field"><input type="text" name="name" id="name"  style="width:35%"  value="<?php echo $row[0]['name']; ?>" class="mediuminput" /></span>
                </p>
				<p>
                	<label>Mobile</label>
                    <span class="field"><input type="text" name="personal_contact_no" id="personal_contact_no" maxlength="10"style="width:35%"  value="<?php echo $row[0]['personal_contact_no']; ?>" class="mediuminput" /></span>
                    <?php echo form_error('personal_contact_no', '<p class="text-danger">', '</p>'); ?>
					 
                </p>
				<p>
                    <label>City</label> 
                    <span class="field"><input type="text" name="city" id="city"  style="width:35%" value="<?php echo $row[0]['city']; ?>" class="mediuminput" /></span>
                    <?php echo form_error('city', '<p class="text-danger">', '</p>'); ?>
                    
                </p>


                <p>
                    <label>Upload Profile Picture</label> 
                    
					<a href="javascript;"><img id="pre_photo" class="img-polaroid" src="<?php echo base_url();?>uploads/admin_picture/<?php echo $row[0]['photo'];?>" height="100" width="150"></a>
					<div class="uploader" style="margin-left: 31%;margin-top: -6%;">
					<input type="file" name="userfile" />
					<input type="hidden" name="img_name"  value="<?php echo $row[0]['photo']; ?>" />
					
					<span class="filename" style="-moz-user-select: none;">No file selected</span>
                    <span class="action" style="-moz-user-select: none;">Choose File</span>
					</div> <span id="img_error" style="color:red"> </span>
					     
                </p>
                
                <p class="stdformbutton">
                	<button type="submit" class="submit radius2" name="news_sub">Save Changes</button>
                </p>
            </form>

        </div><!--subcontent-->
      
      
           
   </div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>