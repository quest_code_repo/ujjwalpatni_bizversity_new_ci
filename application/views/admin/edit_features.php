<div class="centercontent tables">
  <form id="video_form" class="stdform" action="" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Edit Features</h1>
         
          
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper"> 
        <!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
          
            echo $this->session->flashdata('error'); 
          
          }
         ?>
        <div class="form-group">
          <label>Name<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="name" class="smallinput" id="name" required="required" value="<?php echo $features_detail[0]['name'];?>"  /></span>
               <?php echo form_error('name', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group">        
          <label>Icon<span style="color:red;">*</span></label>
          <input type="file" name="icon" id="icon" accept="image/*" value="" <?php if(empty($features_detail[0]['icon'])){ ?>required="required" <?php } ?> />

            <?php 
            if(!empty($features_detail[0]['icon'])) 
             {
            ?>
              <img src="<?php echo base_url();?>uploads/features-icon/<?php echo $features_detail[0]['icon'];?>" width="100" height="100" />
              <input type="hidden" name="old_icon" id="old_icon" value="<?php echo $features_detail[0]['icon'];?>" />
            <?php 
              }
            ?>
        </div>

        <div class="form-group">        
          <label>Slider Image<span style="color:red;">*</span></label>
          <input type="file" name="image" id="image" accept="image/*" value="" <?php if(empty($features_detail[0]['image'])){ ?>required="required" <?php } ?> />

            <?php 
            if(!empty($features_detail[0]['image'])) 
             {
            ?>
              <img src="<?php echo base_url();?>uploads/features-image/<?php echo $features_detail[0]['image'];?>" width="100" height="100" />
              <input type="hidden" name="old_image" id="old_image" value="<?php echo $features_detail[0]['image'];?>" />
            <?php 
              }
            ?>
        </div>

        <div class="form-group">
          <label>Short Desc<span style="color:red;">*</span></label>
          <textarea type="text" name="content" class="smallinput" id="content" value="<?php echo $features_detail[0]['content'];?>"><?php echo $features_detail[0]['content'];?></textarea>
        </div>

        <div class="form-group">
          <label>Full Desc<span style="color:red;">*</span></label>
          <textarea type="text" name="full_desc" class="smallinput" id="full_desc" value="<?php echo $features_detail[0]['full_desc'];?>"><?php echo $features_detail[0]['full_desc'];?></textarea>
        </div>

        <div class="form-group">
          <label> Type Status</label>
          <select name="status" id="status">
            <option value="active" <?php if($features_detail[0]['status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($features_detail[0]['status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
        </div>

        <div class="text-center" style="padding-bottom: 20px;"> 
        <a href="<?php echo base_url();?>admin/add_semester/features_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
     
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
      </div>
      <div class="clearfix"></div>
          
          <!-- <div class="form-group text-center">
              <button class="submit radius2" id="editbtn">Save</button>
          </div> -->
                 
      </div><!--contentwrapper-->
  <!-- centercontent -->
  </form>    
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('full_desc');
  CKEDITOR.replace('description');
</script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#video_form").validate({
    rules:{
      
      name: {
        required: true,
      },
       video_url: {
        required: true,
        url: true
      },
      duration: {
        required: true,     
      }

    },
    
    messages:{
      name: "Please Enter Video Name",      
      video_url: "Please Enter Valid URL",
      duration: "Please Enter Video Time Duration"

    },
       
  });
</script>

</body>

</html>
