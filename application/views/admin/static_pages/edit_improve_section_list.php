<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/static_page/edit_improve_section" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Edit</h1>

          <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
          <!--   <p>
              <label>Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="title" class="mediuminput" id="title" required="required" placeholder="Speaker Name" onblur="check_title();" value="<?php if(isset($all_data)) { echo $all_data[0]->heading;} ?>"/></span>
            </p>
            <p id="title_exist_error" style="color: red;text-align: center;"></p>

            <div class="service_field_error"><?php echo form_error('title'); ?></div> -->


             <p>
              <label> Sub Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="sub_title" class="mediuminput" id="sub_title" required="required" placeholder="Speaker Name" onblur="check_title();" value="<?php if(isset($all_data)) { echo $all_data[0]->sub_title;} ?>"/></span>
            </p>

            <div class="service_field_error"><?php echo form_error('sub_title'); ?></div>

        <!--     <p>
              <label>Category<span style="color:red;">*</span></label>
               <select name="program_category" id="program_category" required="required">
                 <option value="">Select Category</option>

                 <?php if(!empty($cats)){ 

                  foreach($cats as $catgory){?>

                  <option value="<?php echo $catgory->id;?>"><?php echo $catgory->name;?></option>

                  <?php } } ?>
                
              </select>
              </p> -->


        <!--     <p>
              <label>Program Date<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_date" class="mediuminput" id="program_date" required="required" placeholder="YYY-MM-DD" /></span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_date'); ?></div> -->

          <!--   <p>
              <label>Program From Time<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_from_time" class="mediuminput" id="program_from_time" required="required" placeholder="HH:MM:SSS" /></span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_from_time'); ?></div> -->


        <!--     <p>
              <label>Program To Time<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_to_time" class="mediuminput" id="program_to_time" required="required" placeholder="HH:MM:SSS" /></span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_to_time'); ?></div> -->


            <p>
              <label>Description<span style="color:red;">*</span></label>
                <span class="field"><textarea type="text" name="description" class="mediuminput" id="description" required="required" placeholder="Brief Description" /><?php if(isset($all_data)) { echo $all_data[0]->description;} ?></textarea></span>
            </p>
            <div class="service_field_error"><?php echo form_error('description'); ?></div>


             <p>
            <label>image<span style="color:red;">*</span></label>
            <input type="file" name="image" id="image" accept="image/*" value="" />
            <?php echo form_error('image', '<div class="error_validate">', '</div>'); ?> 
     <img src="<?php  echo base_url().'uploads/improve_section_images/'.$all_data[0]->image_url; ?>" style="width: 100px;height: 100px;">
          </p>
          <div class="service_field_error"><?php echo form_error('image'); ?></div>


             <input type="hidden" name="updated_id" value="<?php echo $this->input->get('item_id');?>">
             <input type="hidden" name="video_if_not_change" value="<?php if(isset($all_data)) { echo $all_data[0]->image_url;} ?>">

           <p>
              <label>Speaker Status<span style="color:red;">*</span></label>
               <select name="status" id="status">
                  <?php if(isset($all_data)) { 
                $sts =  $all_data[0]->status;

                if($sts=='active'){ ?>
                 
                 <option value="active" selected="selected">Active</option>
                 <option value="inactive">Inactive</option>
             
             <?php } else if($sts=='inactive') { ?>
                  <option value="active">Active</option>
                 <option value="inactive" selected="selected">Inactive</option>

                 <?php } } ?>
              </select>
              </p>

          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Update</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('sem_description');

  </script>

</div><!--bodywrapper-->

</body>

</html>


<script type="text/javascript">

// function check_title(){

//   var cat_name = $('#speaker_name').val();
//   var type_val = 3;

//   $.ajax({
//          type:'POST',
//          url:'<?php echo site_url();?>admin/Add_programs_category/check_category_title',
//          data:{"name":cat_name,'type_calls':type_val},
//          success: function(data)
//          {  
//             if(data==1){

//               $('#speaker_name').val('');
//               $('#title_exist_error').html('Speaker Name Already Exist');

//             } else{
//               $('#title_exist_error').html('');

//             }
//          }
//      });
// }
</script>