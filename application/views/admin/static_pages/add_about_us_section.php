<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/static_page/edit_about_us_data" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Edit About Us</h1>

          <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Inner Page Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="title" class="mediuminput" id="title" required="required" placeholder="Title" value="<?php echo @$all_data[0]->inner_title;?>" onblur="check_title();"/></span>
            </p>
            <p id="title_exist_error" style="color: red;text-align: center;"></p>

            <div class="service_field_error"><?php echo form_error('title'); ?></div>


             <p>
              <label>Sub Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="sub_title" class="mediuminput" id="sub_title" required="required" placeholder="Sub Title" onblur="check_title();" value="<?php echo @$all_data[0]->sub_heading;?>"/></span>
            </p>

            <div class="service_field_error"><?php echo form_error('sub_title'); ?></div>

             <p>
              <label>Speaker Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="speaker_name" class="mediuminput" id="speaker_name" required="required" placeholder="Speaker Name" onblur="check_title();" value="<?php echo @$all_data[0]->speaker_name;?>"/></span>
            </p>

            <div class="service_field_error"><?php echo form_error('speaker_name'); ?></div>

    


             <p>
              <label>Front Description<span style="color:red;"></span></label>
              <textarea name="description_front_data" class="description_front" id="description_front" /><?php echo @$all_data[0]->front_description;?></textarea>
            </p>


             <p>
              <label>Inner Page Description<span style="color:red;"></span></label>
              <textarea name="description_back_data" class="description_back" id="description_back" /><?php echo @$all_data[0]->inner_description;?></textarea>
            </p>


            <p>
            <label>Front Image<span style="color:red;">*</span></label>
            <input type="file" name="front_image" id="front_image" accept="image/*" value=""/>
            <?php echo form_error('front_image', '<div class="error_validate">', '</div>'); ?>
            <img src="<?php  echo base_url().'uploads/speaker_image/'.$all_data[0]->front_image; ?>" style="width: 100px;height: 100px;">
          </p>

          <input type="hidden" name="front_image_hidden" value="<?php echo @$all_data[0]->front_image;?>">


          <p>
            <label>Back Image<span style="color:red;">*</span></label>
            <input type="file" name="back_image" id="back_image" accept="image/*" value=""/>
            <?php echo form_error('back_image', '<div class="error_validate">', '</div>'); ?>
              <img src="<?php  echo base_url().'uploads/speaker_image/'.$all_data[0]->inner_image; ?>" style="width: 100px;height: 100px;">
          </p>

          <input type="hidden" name="inner_image_hidden" value="<?php echo @$all_data[0]->inner_image;?>">

          <input type="hidden" name="updated_id" value="<?php echo $this->input->get('item_id');?>">


          

<!-- 
              <p>
              <label>Description<span style="color:red;">*</span></label>
              <textarea name="progrm_description" class="smallinput" id="sem_description" required="required"/></textarea>
            </p> -->

             <p>
              <label>Status<span style="color:red;">*</span></label>
               <select name="status" id="status">
                  <?php if(isset($all_data)) { 
                $sts =  $all_data[0]->status;

                if($sts=='active'){ ?>
                 
                 <option value="active" selected="selected">Active</option>
                 <option value="inactive">Inactive</option>
             
             <?php } else if($sts=='inactive') { ?>
                  <option value="active">Active</option>
                 <option value="inactive" selected="selected">Inactive</option>

                 <?php } } ?>
              </select>
              </p>

          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Update</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('description_front');
  CKEDITOR.replace('description_back');

  </script>

</div><!--bodywrapper-->

</body>

</html>


<script type="text/javascript">

// function check_title(){

//   var cat_name = $('#speaker_name').val();
//   var type_val = 3;

//   $.ajax({
//          type:'POST',
//          url:'<?php echo site_url();?>admin/Add_programs_category/check_category_title',
//          data:{"name":cat_name,'type_calls':type_val},
//          success: function(data)
//          {  
//             if(data==1){

//               $('#speaker_name').val('');
//               $('#title_exist_error').html('Speaker Name Already Exist');

//             } else{
//               $('#title_exist_error').html('');

//             }
//          }
//      });
// }
</script>