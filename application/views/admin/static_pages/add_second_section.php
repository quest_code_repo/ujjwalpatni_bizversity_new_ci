<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/static_page/add_second_section" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Content</h1>

          <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="title" class="mediuminput" id="title" required="required" placeholder="Title" onblur="check_title();"/></span>
            </p>
            <p id="title_exist_error" style="color: red;text-align: center;"></p>

            <div class="service_field_error"><?php echo form_error('title'); ?></div>


             <p>
              <label>Sub Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="sub_title" class="mediuminput" id="sub_title" required="required" placeholder="Title" onblur="check_title();"/></span>
            </p>

            <div class="service_field_error"><?php echo form_error('sub_title'); ?></div>

        <!--     <p>
              <label>Category<span style="color:red;">*</span></label>
               <select name="program_category" id="program_category" required="required">
                 <option value="">Select Category</option>

                 <?php if(!empty($cats)){ 

                  foreach($cats as $catgory){?>

                  <option value="<?php echo $catgory->id;?>"><?php echo $catgory->name;?></option>

                  <?php } } ?>
                
              </select>
              </p> -->


        <!--     <p>
              <label>Program Date<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_date" class="mediuminput" id="program_date" required="required" placeholder="YYY-MM-DD" /></span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_date'); ?></div> -->

          <!--   <p>
              <label>Program From Time<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_from_time" class="mediuminput" id="program_from_time" required="required" placeholder="HH:MM:SSS" /></span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_from_time'); ?></div> -->


        <!--     <p>
              <label>Program To Time<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_to_time" class="mediuminput" id="program_to_time" required="required" placeholder="HH:MM:SSS" /></span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_to_time'); ?></div> -->


            <p>
              <label>Description<span style="color:red;">*</span></label>
                <span class="field"><textarea type="text" name="description" class="mediuminput" id="description" required="required" placeholder="Description" /></textarea></span>
            </p>
            <div class="service_field_error"><?php echo form_error('description'); ?></div>


            <p>
            <label>Speaker Video<span style="color:red;">*</span></label>
            <input type="file" name="speaker_video" id="speaker_video" accept="video/*" value="" required="required"/>
            <?php echo form_error('speaker_video', '<div class="error_validate">', '</div>'); ?>
          </p>
          <div class="service_field_error"><?php echo form_error('description'); ?></div>


          

<!-- 
              <p>
              <label>Description<span style="color:red;">*</span></label>
              <textarea name="progrm_description" class="smallinput" id="sem_description" required="required"/></textarea>
            </p> -->

           <p>
              <label>Status<span style="color:red;">*</span></label>
               <select name="status" id="status">
                 <option value="Active">Active</option>
                 <option value="Inactive">Inactive</option>
              </select>
              </p>

          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('sem_description');

  </script>

</div><!--bodywrapper-->

</body>

</html>


<script type="text/javascript">

// function check_title(){

//   var cat_name = $('#speaker_name').val();
//   var type_val = 3;

//   $.ajax({
//          type:'POST',
//          url:'<?php echo site_url();?>admin/Add_programs_category/check_category_title',
//          data:{"name":cat_name,'type_calls':type_val},
//          success: function(data)
//          {  
//             if(data==1){

//               $('#speaker_name').val('');
//               $('#title_exist_error').html('Speaker Name Already Exist');

//             } else{
//               $('#title_exist_error').html('');

//             }
//          }
//      });
// }
</script>