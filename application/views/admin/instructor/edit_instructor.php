<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_product/update_instructor" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Update Elearning Instructor</h1>
           
            
        </div><!--pageheader-->

        <input type="hidden" name="instructor_id" value="<?php echo $Instructor[0]->instructor_id;?>"    />
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Instructor Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="instructor_name" class="mediuminput" id="instructor_name" value="<?php echo  $Instructor[0]->instructor_name;?>" required="required" /></span>
            </p>

           
            <p>
             <label>Instructor  For<span style="color:red;">*</span></label>
              <select name="above_category" id="above_category" required="required">
               <option value="">--select elarning product--</option>

              <?php 
                if(!empty($Elearning))
                {
                  foreach($Elearning as $each_parent_cat)
                  {
                      if($each_parent_cat->product_id == $Instructor[0]->instructor_course_id)
                      {
                        ?>
                         <option value="<?php echo $each_parent_cat->product_id;?>" selected><?php echo $each_parent_cat->pname;?></option>
                        <?php
                      }
                      else
                      {
                        ?>
                          <option value="<?php echo $each_parent_cat->product_id;?>" ><?php echo $each_parent_cat->pname;?></option>
                        <?php
                      }
                    }
                  }
              ?>
             </select>
            </p>

            <p>
              <label>Instructor Image<span style="color:red;"></span></label>
              <span class="field"><input type="file" name="mat_image" class="mediuminput" id="mat_image" accept="image/*"  value="" <?php if(empty($Instructor[0]->instructor_image)){ ?>required="required" <?php } ?>/>

                  <?php 
                          if(!empty($Instructor[0]->instructor_image)) 
                           {
                          ?>
                            <img src="<?php echo base_url();?>uploads/instructor/<?php echo $Instructor[0]->instructor_image;?>" width="100" height="100" />
                            <input type="hidden" name="mat_image1" id="mat_image1" value="<?php echo $Instructor[0]->instructor_image;?>" />
                          <?php 
                            }
                          ?>
              </span>
            </p>

             <p>
              <label>Facebook Link<span style="color:red;"></span></label>
              <span class="field"><input type="text" name="facebook_link" class="mediuminput" id="facebook_link" value="<?php echo  $Instructor[0]->facebook_link;?>" /></span>
            </p>
             <p>
              <label>Twitter Link<span style="color:red;"></span></label>
                <span class="field"><input type="text" name="twitter_link" class="mediuminput" id="twitter_link" value="<?php echo  $Instructor[0]->twitter_link;?>" /></span>
            </p>
             <p>
              <label>Google+ Link<span style="color:red;"></span></label>
                <span class="field"><input type="text" name="google_link" class="mediuminput" id="google_link" value="<?php echo  $Instructor[0]->google_link;?>" /></span>
            </p>

            <p>
              <label>Youtube Link<span style="color:red;"></span></label>
             <span class="field"><input type="text" name="youtube_link" class="mediuminput" id="youtube_link" value="<?php echo  $Instructor[0]->youtube_link;?>" /></span>
            </p>

            <p>
              <label>Description<span style="color:red;"></span></label>
             <span class="field"><textarea id="instructor_description" name="instructor_description" required><?php echo  $Instructor[0]->instructor_description;?></textarea></span>
            </p>


          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Update Instructor</button>
                <a href="admin/add_product/elearning_instructor_list" class="btn btn-danger">Cancel</a>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>