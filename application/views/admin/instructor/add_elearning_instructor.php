<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_product/add_new_instructor" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Elearning Instructor</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Instructor Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="instructor_name" class="mediuminput" id="instructor_name" required="required" /></span>
            </p>

           
            <p>
             <label>Instructor  For<span style="color:red;">*</span></label>
              <select name="above_category" id="above_category" required="required">
               <option value="">--select elarning product--</option>

              <?php 
                if(!empty($Elearning))
                {
                  foreach($Elearning as $each_parent_cat)
                  {
                ?>
                <option value="<?php echo $each_parent_cat->product_id;?>"><?php echo $each_parent_cat->pname;?></option>
                <?php
                  } 
                }
              ?>
             </select>
            </p>

            <p>
              <label>Instructor Image<span style="color:red;"></span></label>
              <span class="field"><input type="file" name="mat_image" class="mediuminput" id="mat_image" accept="image/*"  required="required"/></span>
            </p>

             <p>
              <label>Facebook Link<span style="color:red;"></span></label>
              <span class="field"><input type="text" name="facebook_link" class="mediuminput" id="facebook_link" /></span>
            </p>
             <p>
              <label>Twitter Link<span style="color:red;"></span></label>
                <span class="field"><input type="text" name="twitter_link" class="mediuminput" id="twitter_link" /></span>
            </p>
             <p>
              <label>Google+ Link<span style="color:red;"></span></label>
                <span class="field"><input type="text" name="google_link" class="mediuminput" id="google_link" /></span>
            </p>

            <p>
              <label>Youtube Link<span style="color:red;"></span></label>
             <span class="field"><input type="text" name="youtube_link" class="mediuminput" id="youtube_link" /></span>
            </p>

            <p>
              <label>Description<span style="color:red;"></span></label>
             <span class="field"><textarea id="instructor_description" name="instructor_description" required></textarea></span>
            </p>


          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Instructor</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>