<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<?php if (isset($message)) { ?>
<div class="notibar msgsuccess" style="color:green;"><p><strong>plan added successfully</strong></p></div>
<?php } ?>
<form class="stdform" action="<?php echo base_url(); ?>admin/addservices/plan_levels" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">

            <h1 class="pagetitle">Add Plans Levels</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
      
               
                      <div>
                        <p>
                          <label>Level Name<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="plan_name" class="mediuminput" id="plan_name"/></span>
                        </p>
                        <div class="service_field_error"><?php echo form_error('plan_name'); ?></div>

                        <p>
                          <label>Required Points<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="plan_points" class="mediuminput" id="plan_points" />
                          </span>
                        </p>
                        <div class="service_field_error"><?php echo form_error('plan_points'); ?></div>

                        <p>
                          <label>Level Description<span style="color:red;"></span></label>
                            <span class="field"><textarea name="plan_des" class="mediuminput" id="plan_des"></textarea></span>
                        </p>
                         				            	                                            
                                                                                                          
                        <p>
                          <label>Service Status<span style="color:red;"></span></label>
                           <select name="active_status" id="active_status" >
                             <option value="Active">Active</option>
                             <option value="Inactive">Inactive</option>
                          </select>
                          </p>
            
                        
                        </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
                 <p class="stdformbutton">
                          <button class="submit radius2" id="addbtn">Add Plan</button>
                 </p>
                    
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>