<!DOCTYPE html>
  <head>
    <base href="<?=base_url();?>">
  <?php define("admin_assets", base_url()."assets/admincss/"); ?>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Bizversity | Admin Panel</title>
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url("assets/frontend/images/favicon.png"); ?>"/>
  <link rel="stylesheet" href="<?php echo admin_assets; ?>css/style.default.css" type="text/css" />
  <link href="<?php echo admin_assets; ?>css/font-awesome.min.css" rel="stylesheet">
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery-1.7.min.js"></script>
 
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/js/plugins/select2/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/plugins/jquery.ui.css">
  <!-- for tag -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/bootstrap-tagsinput.css">
  <link type="text/css" rel="stylesheet" href="<?php echo admin_assets; ?>css/bootstrap.min.css"/>
  <link type="text/css" rel="stylesheet" href="<?php echo admin_assets; ?>css/jquery.datetimepicker.css"/>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/bootstrap-tagsinput.min.js"></script>
   <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <!-- for tag end -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>

  <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery.cookie.js"></script>
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery.uniform.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery.tagsinput.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/charCount.js"></script>
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/ui.spinner.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/custom/general.js"></script>

  <script type="text/javascript" src="<?php echo base_url("assets/admincss/js/adminmain.js"); ?>"></script>  
  <script type="text/javascript" src="<?php echo base_url("assets/admincss/js/bootstrap.min.js"); ?>"></script>  

  
  

  <!-- <script src="<?php echo admin_assets; ?>js/jscolor.js" type="text/javascript"></script> -->
  <script>
    
  jQuery(function() {
    jQuery( "#date1,#date2" ).datepicker({dateFormat: "yy-mm-dd"});
  });
  </script>
  <script type="text/javascript" src="<?php echo admin_assets; ?>js/nicEdit-latest.js"></script>
  <script type="text/javascript">
    // bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
      
    function load_subcat(cid)
    {   //alert(stateid);
      if(cid=='') { return false; }
      jQuery.ajax({url: '<?php echo base_url();?>admin/product_control/load_subcat/'+cid, success: function(result){
      //alert(result);
      if(result) { 
        $('#subcat').html(result); 
      }}});
    }   

    function load_sub_subcat(cid)
    {   //alert(stateid);
      if(cid=='') { return false; }
      jQuery.ajax({url: '<?php echo base_url();?>admin/product_control/load_sub_subcat/'+cid, success: function(result){
      //alert(result);
      if(result) { 
        if(result == false){
          //alert("fg");
        }
        else{
          //alert("yes");
        $('#sub_subcat').html(result); 
        }
      }}});
    }
     function loadprice(pid, qid) {
            if (qid == '') {
                alert('Please Select Quantity');
                return false;
            }
            jQuery.ajax({url: '<?php echo base_url(); ?>admin/create_order/loadprice/' + qid, success: function (result) {
                    jQuery("#price" + pid).html(result);
                }});
    }

    function addtocart(pid, qua) {
        var qid = document.getElementById("qua" + pid).value;
        if (qid == '') {
            alert('Please Select Quantity');
            return false;
        }
        jQuery.ajax({url: '<?php echo base_url(); ?>admin/create_order/addtocart/' + qid + '/' + qua, success: function (result) {
                jQuery(".mini-cart").html(result);
        alert('Item added to your cart');
            }});
    }

    function loaddetails(val) {
      if(val=='') { return false; }
        jQuery.ajax({url: '<?php echo base_url(); ?>admin/create_order/loaddetails/' + val, success: function (result) {
                    
          var arr = JSON.parse(result);
          var shipping_address=arr[0].shipping_address;
          document.getElementById("saddress").value=shipping_address;
          
          var billing_address=arr[0].billing_address;
          document.getElementById("address").value=billing_address;
          
          var shipping_city=arr[0].shipping_city;
          document.getElementById("city1").value=shipping_city;
          
          var billing_city=arr[0].billing_city;
          document.getElementById("city").value=billing_city;
          
          var shipping_pincode=arr[0].shipping_pincode;
          document.getElementById("szipcode").value=shipping_pincode;
        
              var shipping_contact=arr[0].shipping_contact;
          document.getElementById("sphone").value=shipping_contact;
        
            var billing_pin=arr[0].billing_pin;
          document.getElementById("zipcode").value=billing_pin;
          
          var name=arr[0].name;
          document.getElementById("sname").value=name;
          
          var bname=arr[0].bname;
          document.getElementById("name").value=bname;
          
          var sstate=arr[0].sstate;
          jQuery('#state1').val(sstate);
          
          var bstate=arr[0].bstate;
          jQuery('#state').val(bstate); 
          
          var billing_email=arr[0].billing_email;
          document.getElementById("email").value=billing_email;
          
        }
      });
    }
  </script>
  
  
  <script>
    function createurl()
      {    
        var name=document.getElementById("catname").value;
        var s = name.replace(/[^a-zA-Z0-9]/g,'-');
        document.getElementById("curl").value=s;
      
    }
  </script>
 
  <script>
  $(function() {
    $("#date1").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#date2").datepicker({ dateFormat: 'yy-mm-dd' });
  });
  </script>
  
  </head>

  <body class="withvernav">
<div class="bodywrapper">
<div class="topheader">
    <div class="left">
    <a href="<?php echo base_url(); ?>admin/dashboard"><h1 class="logo">ADMIN<span> PANEL</span></h1></a>
    <div class="search" style="padding-top:10px;color:#FFF;"> </div>
    <!--search--> 
    
    <br clear="all" />
  </div>
    <!--left-->
    
    <div class="right">
      <div class="userinfo"> 

      <?php if (!empty($profile_img)) {?>
        <img src="<?php echo base_url();?>uploads/admin_picture/<?php echo $profile_img;?>" width="40" height="30" alt="<?php echo $this->session->userdata('name'); ?>" />
         <span><?php echo $this->session->userdata('name'); ?></span>
    <?php }else{ ?>
        <img src="<?php echo base_url();?>assets/admincss/images/adminslogo.png" width="40" height="30" alt="<?php echo $this->session->userdata('name'); ?>" />
         <span><?php echo $this->session->userdata('name'); ?></span> 
        <?php }?>
      </div>
    <!--userinfo-->
    
    <div class="userinfodrop">
        <div class="avatar">
    
        <a href="#"><img src="<?php echo base_url();?>assets/admincss/images/adminslogo.png" alt="" style="width:100px;"/></a> </div>
        <!--avatar-->
        <div class="userdata">
        <h4><?php echo $this->session->userdata('name'); ?></h4>
        <span class="email">(<?php echo $this->session->userdata('personal_email'); ?>)</span>
        <ul>
            <li><a href="<?php echo base_url(); ?>admin/authentication/my_profile">Edit Profile</a></li>
           <!-- <li><a href="accountsettings.html">Account Settings</a></li>-->
            <li><a href="<?php echo base_url(); ?>admin/authentication/change_password">Change Password</a></li>
            <li><a href="<?php echo base_url(); ?>admin/authentication/logout">Logout</a></li>
          </ul>
      </div>
        <!--userdata--> 
      </div>
    <!--userinfodrop--> 
  </div>
    <!--right--> 
  </div>
<!--topheader-->

  <!-- <div class="header">
    <ul class="headermenu">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><span class="icon icon-flatscreen"></span>Dashboard</a></li>
    </ul>
    <div class="headerwidget">
    </div>

  </div> -->
<!--header-->
<div class="vernav2 iconmenu">
    <ul>
    <?php 
      if($this->session->userdata('designation')==1)
      {
       foreach($perent_menulist as $menuname) { ?>
        <li><a href="<?php if($menuname['url']=='#') { echo '#'.$menuname['id']; } else { echo base_url(); ?>admin/<?php echo $menuname['url']; } ?>" class="elements"><?php echo $menuname['label']; ?></a>
            <?php $sub_menulist=load_submenu($menuname['id']); 
          //print_r($sub_menulist);
          if($sub_menulist){
          ?>
            <span class="arrow"></span>
            <ul id="<?php echo $menuname['id']; ?>">
            <?php foreach($sub_menulist as $submenuname)
            { 
            $check=check_submenu_per($submenuname['id']);
              // print_r($submenuname);
            //if($check==1) {
            ?>
            <li><a href="<?php echo base_url(); ?>admin/<?php echo $submenuname['url']; ?>"><?php echo $submenuname['label']; ?></a></li>
            <?php //} 
            } ?>
          </ul>
            <?php } ?>
          </li>
        <?php }} else {  
                foreach($perent_menulist as $menuname) { ?>
        <li><a href="<?php if($menuname['url']=='#') { echo '#'.$menuname['id']; } else { echo base_url(); ?>admin/<?php echo $menuname['url']; } ?>" class="elements"> <?php echo $menuname['label']; ?></a>
        <?php } }?>
    </ul>
    <a class="togglemenu"></a> <br />
    <br />
  </div>
<!--leftmenu-->
  