<div style='width:610px;border:5px solid #DFDFDF;padding:10px;'>
        <div>We would like to inform you that your order is/are out for delivery. You will receive soon.</div>
        
        <p>Your Order Summary,</p>
        <?php $dis = 0;
        if(!empty($info)) { ?>
              <table style='width:600px;margin:0px;padding:0px;border:1px solid #CCC;border-collapse:collapse;font-size:12px;'>
  <tr style='border:1px solid #CCC;border-collapse:collapse;'>
    <td width='50%' style='border:1px solid #CCC;border-collapse:collapse;padding:5px;vertical-align:top;'><img src='<?php echo base_url(); ?>images/wow-indore-logo.png' height='60' /></td>
    <td align='right' style='border:1px solid #CCC;border-collapse:collapse;padding:5px;vertical-align:top;'><strong>Agrawals Delight,</strong><br />
      167-A, Adarsh Mechanc Nagar,<br />
      Scheme No. 54, Bhamori, Indore 452008<br />
      Tel : (0731)2900130/9977916622 <br />
      e-mail : info@wowindore.in.    </td>
  </tr>
  <tr>
    <td style='border:1px solid #CCC;border-collapse:collapse;padding:5px;'><strong>Billing Address :</strong> <br />
    <?php echo $info->billing_address; ?>, <?php echo $bill_city->name; ?> (<?php echo $bill_state->name; ?>) - <?php echo $info->billing_pin; ?>
   <br /> Contact : <?php echo $info->billing_contact; ?>    </td>
    <td style='border:1px solid #CCC;border-collapse:collapse;padding:5px;'><strong>Shipping Address :</strong> <br />
    <?php echo $info->shipping_address; ?>, <?php echo $ship_city->name; ?> (<?php echo $ship_state->name; ?>) - <?php echo $info->shipping_pincode; ?>
   <br /> Contact : <?php echo $info->shipping_contact; ?>    </td>
  </tr>
   <tr>
    <td style='border:1px solid #CCC;border-collapse:collapse;padding:5px;'><strong>Order Number :</strong> <?php echo $info->order_id; ?>    </td>
    <td style='border:1px solid #CCC;border-collapse:collapse;padding:5px;'><strong>Order Time :</strong>  <?php echo $info->order_time; ?>    </td>

  </tr>
  <tr style="border:1px solid #CCC;border-collapse:collapse;">
    <td colspan="2" style="padding:5px;" align="center">Tin Number : 23249057443</td>
  </tr>
  <tr>
    <td colspan='2'><table style='width:600px;margin:0px;padding:0px;border:1px solid #CCC;border-collapse:collapse;font-size:12px;'>
      <tr align=''>
        <td width="163" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Product Name</strong></td>
        <td width="60" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Unit</strong></td>
        <td width="" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Quantity</strong></td>
        <td width="110" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Amount</strong></td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Discount</strong></td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Additional Discount</strong></td>
        <td width="80" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Vat %</strong></td>
        <td width="110" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Vat Amount</strong></td>
        
      </tr>

      <?php $total=0; if(!empty($order)) {
      foreach($order as $item) { 
    
      
      ?>
      <tr>
        <td style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>
    <?php echo $item['pname']; ?> 
    </td>
        <td style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'><?php echo $item['weight']; ?> <?php echo $item['name']; ?></td>
    <?php
    
      $price=$item['price']; 
    
    ?>
    
        <td style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'><?php echo $item['qua']; ?></td>
    
    
    
      <td style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Rs. <?php echo $ss=number_format($item['qua']*$item['org_price'],2); ?></td>

      <?php 
      $vat = ($item['org_price']*$item['vat'])/100;
      ?>

      <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;text-align:center;">Rs. <?php echo $vat ?></td>

      <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;text-align:center;">Rs. <?php if($item['disc_price'] != 0.00) { echo $item['org_price']-$item['disc_price']; } else { echo '0.00'; } ?></td>

      <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;text-align:center;"><?php echo $item['vat']; ?> %</td>

      
      <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;text-align:center;">Rs. <?php echo $vat ?></td>

      
      
      </tr>
      <?php 
   // $total=$total+$ss; 
    $total=$info->order_total; 
    
    }} ?>
    </table></td>
    </tr>
  <tr>
    <td align='right' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Sub Total</td>
    <td align='right' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Rs. <?php echo number_format($info->sub_total,2, '.', ''); ?></td>
  </tr>
   <tr>
    <td align='right' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Coupon Discount</td>
    <td align='right' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Rs. <?php echo $dis=number_format($info->coupon_discount,2, '.', ''); ?></td>
  </tr>
   <tr>
    <td align='right' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Shipping Charges</td>
    <td align='right' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Rs. <?php echo $info->shipping_price; ?></td>
  </tr>
  
  <tr>
    <td align='right' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Total (incl. all taxes)</td>
    <td align='right' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'>Rs. <?php echo number_format(($total),2, '.', '') ?></td>
  </tr>
  
  <tr>
    <td colspan='2' align='left' style='padding:5px;border:1px solid #CCC;border-collapse:collapse;'><br />
      Thank you for your business! <br />
    <strong>FOR ORDER LOGIN TO<a href='#'> WWW.WOWINDORE.IN</a> OR CALL ON 9977916622</strong> <br />
    * T &amp; C apply </td>
    </tr>
</table>
<?php } ?>
</div>