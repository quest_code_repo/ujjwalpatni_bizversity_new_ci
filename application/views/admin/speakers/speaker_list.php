 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Speakers List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php 
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	} else if($this->session->flashdata('update'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('update'); ?>
           </div>
           <?php
          }
        ?>
        <div id="contentwrapper" class="contentwrapper">
            <a href="<?php echo base_url();?>admin/Speaker_action/add_speakers"><button class="btn-primary">ADD SPEAKERS</button></a>

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

            <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Speaker Name</th>
                                                <th>Description</th>
                                                <!-- <th>Date</th> -->
                                                <!-- <th>From Time</th> -->
                                                <!-- <th>To Time</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                            if(!empty($all_data))
                                            {
                                                $j=1;
                                                foreach($all_data as $result)
                                            {?>
                                            <tr>

                                                <td><?php echo $j; ?></td>
                                                <td><?php  echo $result->speaker_name; ?></td>
                                                <!-- <td><?php  echo get_cat_name($result->category_id); ?></td> -->
                                               <td><?php  echo $result->description; ?></td>
                                                <!-- <td><?php  echo $result->program_from_time; ?></td> -->
                                                <!-- <td><?php  echo $result->program_to_time; ?></td> -->
                                                <td><a href="<?php echo base_url();?>admin/Speaker_action/edit_speaker_data/?speaker_id=<?php echo base64_encode($result->id);?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="javascript:void(0);" onclick="return delete_category_data(<?php echo $result->id; ?>)"  title="Delete"><i class="fa fa-times" aria-hidden="true" style='color:red;'"></i></a></td>
                                
                                            </tr>
                                            <?php 
                                                $j++;
                                                  }
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>

<script type="text/javascript">

    function delete_category_data(id)
    {

      if(confirm("Are you Sure! You Want to delete it")){   

        $.ajax({
         type:'POST',
         url:'<?php echo site_url();?>admin/Speaker_action/delete_data_coupons',
         data:{"id":id},
         success: function(data)
         {  
            if(data==1){

                 location.reload();
            }
         }
     });

        return true;
      } else{
        
        return false;
      }

    }
  </script>