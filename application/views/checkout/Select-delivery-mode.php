<section class="Select-add-section about-us-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				 <div class="row">
                  <div class="col-sm-12">
                       <h2 class="common-heading wow fadeInUp">Select a delivery address</h2>
                  </div>
                </div>
                <div class="row">
                	<div class="col-sm-12">
                		<div class="multiple-add-block1">
					      <p>Is the address you'd like to use displayed below? If so, click the corresponding "Deliver to this address" button. Or you can enter a new delivery address. </p>
				          <hr>
				     </div>
                	</div>
                </div>

		<div class="row">
                <div class="col-sm-4">
                	<?php
                		if(!empty($DeliveryAddress) && count($DeliveryAddress)>0)
                		{
                			foreach($DeliveryAddress as $del)
                			{
                				?>
                					<div class="multiple-add-block1">
										<h4><?php echo $del->user_full_name;?></h4>
										<p><?php echo $del->flat_no;?> <?php echo $del->street;?> <?php echo isset($del->landmark)?$del->landmark:'';?></p>
										<p class="add-state"><?php echo $del->city;?>, <?php echo $del->state;?></p>
										<div class="Deliver-btn">
										<a href="checkout/proceed_to_check/<?php echo base64_encode($del->user_address_id);?>"><button class="btn btn-default btn-register">Deliver to this address</button></a>
										</div>
										<div class="edit-delete">
										<a href="checkout/edit_address/<?php echo base64_encode($del->user_address_id);?>"><button class="btn btn-default btn-common">Edit</button></a> 
					                    <a href="checkout/delete_address/<?php echo base64_encode($del->user_address_id);?>"><button class="btn btn-default btn-common" onclick="return delete_address();">Delete</button></a>	
										</div>
					              </div>
                				<?php
                			}
                		}
					?>
                  
                  </div>	


                 <form id="delivery_form" name="delivery_form" action="checkout/add_new_address" method="post">
                  <div class="col-sm-8">
                	<div class="new-add-block">
                	<h4 class="sub-heading">Add a new address</h4>
                	<p>Be sure to click "Deliver to this address" when you've finished.</p>

                	<div class="form-group">
				     <label for="email">Full name:</label>
				     <input type="text" class="form-control" name="del_full_name" id="del_full_name" data-rule-required="true" data-msg-required="Please Enter Full Name">
				    </div>

				    <div class="form-group">
				      <label for="pwd"> Mobile number:</label> 
				       <input type="text" class="form-control descPriceNumVal" name="del_mobile_no" id="del_mobile_no" maxlength="10" minlength="10" data-rule-required="true" data-msg-required="Please Enter Mobile No">
				     </div>

				      <div class="form-group">
				      <label for="pwd">Pincode:</label>
				       <input type="text" class="form-control" name="del_pincode" id="del_pincode" data-rule-required="true" data-msg-required="Please Enter Pincode">
				     </div>

				      <div class="form-group">
				      <label for="pwd">Flat / House No. / Floor / Building: </label>
				       <input type="text" class="form-control" name="del_flat_no" id="del_flat_no" data-rule-required="true" data-msg-required="Please Enter Flat / House No. / Floor / Building">
				     </div>

				      <div class="form-group">
				      <label for="pwd">Colony / Street / Locality: </label>
				       <input type="text" class="form-control" name="del_street" id="del_street" data-rule-required="true" data-msg-required="Please Enter Colony / Street / Locality">
				     </div>

				     <h5>Landmark: </h5>
				     <p>(optional)</p>
				      <div class="form-group">
				       <input type="text" class="form-control" name="del_location" id="del_location">
				     </div>

				     <div class="form-group">
					      <label for="pwd">State:</label>
					      <select class="form-control" name="del_state" id="del_state" onchange="get_state_cities(this.value)" data-rule-required="true" data-msg-required="Please Select State">
					      		<option value="">Select State</option>
					      		<?php
					      			if(!empty($States) && count($States)>0)
					      			{
					      				foreach($States as $stateDetails)
					      				{
					      					?>
					      					<option value="<?php echo $stateDetails->city_state;?>"><?php echo $stateDetails->city_state;?></option>
					      					<?php
					      				}
					      			}
					      		?>
					      </select>

					   
				     </div>

				      <div class="form-group">
					      <label for="pwd">Town/City:</label>
					     <select class="form-control" name="del_city" id="del_city" data-rule-required="true" data-msg-required="Please Select City">

					     </select>
				     </div>

				    

				      
						<hr>
				      <div class="text-right Deliver-btn">
				      	<button type="submit" class="btn btn-default btn-register" id="AddDelivery">Deliver to this address</button>
				      </div>
                      </div>
                	</div>  
	
	          </form>  

                </div>
			</div>
		</div>
	</div>
</section>



<script type="text/javascript">
        $(document).ready(function () {
        //called when key is pressed in textbox
        $(".orgPriceNumVal").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $(".orgPriceNumVal").attr("placeholder","Digits Only");
        return false;
        }
        });

        $(".descPriceNumVal").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57)) {
        //display error message
        $(".descPriceNumVal").attr("placeholder","");
        return false;
        }
        });

    });

/******END: Check Price Number  Input**********/

</script>

<script type="text/javascript">
        $(document).ready(function(){
        $('#AddDelivery').click(function(){
        $('#delivery_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        }
        });
      });

      });   

</script>

<script type="text/javascript">
	function delete_address()
	{
		var r = confirm("Are You Sure Want To Delete!");
			if (r == true) {
			    return true;
			} else {
			    return false;
			}
	}
</script>


<script type="text/javascript">
	function get_state_cities(state_name)
	{	
			$.ajax({
					type : 'POST',
					url  : '<?php echo base_url('checkout/get_cities_data')?>',
					data : {'state_name' : state_name},
					success : function(resp)
					{
						resp = resp.trim();

						$('#del_city').html(resp);
					}


			})
	}
</script>