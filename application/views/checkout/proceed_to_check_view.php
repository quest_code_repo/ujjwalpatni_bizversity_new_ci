
<section class="section-breadcrumb new" style="background-image: url('assets/images/other/1.png');">
             <div class="parallax-overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                          <h1 class="heading-black">Proceed to check</h1>
                          <div class="navigate-breadcrumb">
                              <ol class="breadcrumb">
                                   <li><a href="">Home</a></li>
                                   <li><a href="javascript:void(0);" class="active">Proceed to check</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>
<section class="checkout-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
					<h2 class="common-heading wow fadeInUp">Confirm your Order</h2>
				</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="proceed-check-block">
					<div class="row">
						<div class="col-md-12">
							<div>
								<h3>Deliver to:</h3>
								<h4><?php echo $DeliveryAddress[0]['user_full_name']?></h4>
								<p><?php echo $DeliveryAddress[0]['flat_no']?></p>
                <p><?php echo $DeliveryAddress[0]['street']?></p>
								<p class="add-state"><?php echo $DeliveryAddress[0]['city']?>, <?php echo $DeliveryAddress[0]['state']?></p>


                <?php
                    if(!empty($CartDetails) && count($CartDetails)>0)
                    {
                      foreach($CartDetails as $cartInfo)
                      {
                          $details = get_cart_item_details($cartInfo->product_id, $cartInfo->product_type);
                          ?>
                          <h4><?php echo $details[0]->item_name; ?></h4>
                          <p><span class="quantity">Quantity: <?php echo $cartInfo->p_quantity;?></span> <span class="price"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $cartInfo->actual_price;?></span></p>
                          <?php
                      }
                    }

                ?>  
								
								
							</div>
							<hr>							
						</div>
					</div>
			</div>	
	</div>
		   <div class="col-md-3">
                <div class="proceed-pay-block">
                   <!--  <div class="proceed-pay-inner text-center">
                       <p>Your order is eligible for FREE Delivery. <span>Select this option at checkout.</span> 
                       <a href="">Details</a></p> 
                       <button class="btn btn-default btn-orange">Add Coupon</button>
                    </div> -->
                <form action="checkout/place_checkout_order"  method="post"  >


                    <div class="Subtotal-block">

                         <?php
                          if(!empty($CartDetails) && count($CartDetails)>0)
                          {
                            $sub_total = 0;
                            $qty       = 0;
                              foreach($CartDetails as $cartDetails)
                              {
                                    $sub_total+= $cartDetails->p_quantity * $cartDetails->actual_price;
                                    $qty+= $cartDetails->p_quantity;
                              }

                               $appliedCouponData = $this->session->userdata('couponData');



                              if(!empty($appliedCouponData))
                              {
                                  if($appliedCouponData['discount_type'] == "percent")
                                  {
                                      $percentData = ($sub_total * $appliedCouponData['discount_amount']) / 100;

                                      $final = $sub_total - $percentData;
                                  }
                                  else
                                  {
                                       $final = $sub_total - $appliedCouponData['discount_amount'];
                                  }
                              }
                              else
                              {
                                $final = $sub_total;
                              }
                          }
                        ?>

                      <h4>Ordertotal (<?php echo isset($qty)?$qty:'0'?> items):<span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo isset($final)?$final:'0'?></span></h4>  

                   
                        <input type="hidden" name="delivery_id" value="<?php echo $DeliveryAddress[0]['user_address_id']?>"   />
                       
                        

                        <div class="text-center">
                            <button  type ="submit"  class="btn btn-default btn-register">Place Order</button>
                        </div>

                    </div>
                  </form>


                    <div>                        
                     
                    </div>
                </div>
            </div>
	     </div>
	    </div>				  
	</div>
</section>

