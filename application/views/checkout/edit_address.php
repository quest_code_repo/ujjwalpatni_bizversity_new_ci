<section class="Select-add-section about-us-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				 <div class="row">
                  <div class="col-sm-12">
                       <h2 class="common-heading wow fadeInUp">Update a delivery address</h2>
                  </div>
                </div>
                

		<div class="row">
                	


                 <form id="delivery_form" name="delivery_form" action="checkout/update_address" method="post">

                 	<input type="hidden" name="user_address_id" value="<?php echo $UserAddress[0]->user_address_id;?>"   />

                  <div class="col-sm-8">
                	<div class="new-add-block">
                	
                	<div class="form-group">
				     <label for="email">Full name:</label>
				     <input type="text" class="form-control" name="del_full_name" id="del_full_name" data-rule-required="true" data-msg-required="Please Enter Full Name" value="<?php echo $UserAddress[0]->user_full_name;?>">
				    </div>

				    <div class="form-group">
				      <label for="pwd"> Mobile number:</label> 
				       <input type="text" class="form-control descPriceNumVal" name="del_mobile_no" id="del_mobile_no" maxlength="10" minlength="10" data-rule-required="true" data-msg-required="Please Enter Mobile No" value="<?php echo $UserAddress[0]->mobile_no;?>">
				     </div>

				      <div class="form-group">
				      <label for="pwd">Pincode:</label>
				       <input type="text" class="form-control" name="del_pincode" id="del_pincode" data-rule-required="true" data-msg-required="Please Enter Pincode" value="<?php echo $UserAddress[0]->pincode;?>">
				     </div>

				      <div class="form-group">
				      <label for="pwd">Flat / House No. / Floor / Building: </label>
				       <input type="text" class="form-control" name="del_flat_no" id="del_flat_no" data-rule-required="true" data-msg-required="Please Enter Flat / House No. / Floor / Building" value="<?php echo $UserAddress[0]->flat_no;?>">
				     </div>

				      <div class="form-group">
				      <label for="pwd">Colony / Street / Locality: </label>
				       <input type="text" class="form-control" name="del_street" id="del_street" data-rule-required="true" data-msg-required="Please Enter Colony / Street / Locality" value="<?php echo $UserAddress[0]->street;?>">
				     </div>

				     <h5>Landmark: </h5>
				     <p>(optional)</p>
				      <div class="form-group">
				       <input type="text" class="form-control" name="del_location" id="del_location" value="<?php echo isset($UserAddress[0]->landmark)?$UserAddress[0]->landmark:'';?>">
				     </div>

				     <div class="form-group">
					      <label for="pwd">State:</label>
					      <select class="form-control" name="del_state" id="del_state" onchange="get_state_cities(this.value)" data-rule-required="true" data-msg-required="Please Select State">
					      		<option value="">Select State</option>
					      		<?php
					      			if(!empty($States) && count($States)>0)
					      			{
					      				foreach($States as $stateDetails)
					      				{
					      					if($stateDetails->city_state == $UserAddress[0]->state)
					      					{
					      						?>
					      						<option value="<?php echo $stateDetails->city_state;?>" selected><?php echo $stateDetails->city_state;?></option>
					      						<?php
					      					}
					      					else
					      					{
					      						?>	
					      						<option value="<?php echo $stateDetails->city_state;?>"><?php echo $stateDetails->city_state;?></option>
					      						<?php
					      					}
					      				}
					      			}
					      		?>
					      </select>

					   
				     </div>

				      <div class="form-group">
					      <label for="pwd">Town/City:</label>
					     <select class="form-control" name="del_city" id="del_city" data-rule-required="true" data-msg-required="Please Select City">
					     		<?php
					      			if(!empty($Cities) && count($Cities)>0)
					      			{
					      				foreach($Cities as $cityDetails)
					      				{
					      					if($cityDetails->city_name == $UserAddress[0]->city)
					      					{
					      						?>
					      						<option value="<?php echo $cityDetails->city_name;?>" selected><?php echo $cityDetails->city_name;?></option>
					      						<?php
					      					}
					      					else
					      					{
					      						?>	
					      						<option value="<?php echo $cityDetails->city_name;?>"><?php echo $cityDetails->city_name;?></option>
					      						<?php
					      					}
					      				}
					      			}
					      		?>
					     </select>
				     </div>

				    

				      
						<hr>
				      <div class="text-right Deliver-btn">
				      	<button type="submit" class="btn btn-default btn-register" id="AddDelivery">Deliver to this address</button>
				      </div>
                      </div>
                	</div>  
	
	          </form>  

                </div>
			</div>
		</div>
	</div>
</section>



<script type="text/javascript">
        $(document).ready(function () {
        //called when key is pressed in textbox
        $(".orgPriceNumVal").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $(".orgPriceNumVal").attr("placeholder","Digits Only");
        return false;
        }
        });

        $(".descPriceNumVal").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57)) {
        //display error message
        $(".descPriceNumVal").attr("placeholder","");
        return false;
        }
        });

    });

/******END: Check Price Number  Input**********/

</script>

<script type="text/javascript">
        $(document).ready(function(){
        $('#AddDelivery').click(function(){
        $('#delivery_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        }
        });
      });

      });   

</script>

<script type="text/javascript">
	function delete_address()
	{
		var r = confirm("Are You Sure Want To Delete!");
			if (r == true) {
			    return true;
			} else {
			    return false;
			}
	}
</script>


<script type="text/javascript">
	function get_state_cities(state_name)
	{	
			$.ajax({
					type : 'POST',
					url  : '<?php echo base_url('checkout/get_cities_data')?>',
					data : {'state_name' : state_name},
					success : function(resp)
					{
						resp = resp.trim();

						$('#del_city').html(resp);
					}


			})
	}
</script>