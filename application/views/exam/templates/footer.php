 <!-- <a href="javascript:;" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a> -->

        <!-- JQuery -->
        <script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <!-- dropdownhover effects JavaScript -->
        <script type="text/javascript" src="assets/js/bootstrap-dropdownhover.min.js"></script>
        <!-- Owl JavaScript -->
        <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
        <!-- wow JavaScript -->
        <script type="text/javascript" src="assets/js/wow.min.js"></script>
        <!-- video player JavaScript -->
        <script src="assets/js/jquery.parallax-1.1.3.js"></script>
        <!-- jquery.nice-select.min.js -->
        <script type="text/javascript" src="assets/js/jquery.nice-select.min.js"></script>
        <!-- Custom JavaScript -->
        <script type="text/javascript" src="assets/js/custom.js"></script>
        
        <!-- <script src="assets/js/highcharts.js"></script> -->
        <!-- <script src="assets/js/highcharts-more.js"></script> -->
        <!-- <script src="assets/js/exporting.js"></script> -->
        <!-- <script>
        $(function () {
    
    $('#container').highcharts({
    
        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        
        title: {
            text: 'Speedometer'
        },
        
        pane: {
            startAngle: -90,
            endAngle: 90,
            background: null
        },
        
        plotOptions: {
            gauge: {
                dataLabels: {
                    enabled: false
             },
                dial: {
                    baseLength: '0%',
                    baseWidth: 10,
                    radius: '100%',
                    rearLength: '0%',
                    topWidth: 1
                }
            }
        },
           
        // the value axis
        yAxis: {
            labels: {
                enabled: true,
                x: 35, y: -10
            },
            tickPositions: [80, 90],
            minorTickLength: 0,
            min: 0,
            max: 100,
            plotBands: [{
                from: 0,
                to: 25,
                color: 'rgb(192, 0, 0)', // red
                thickness: '50%'
            }, {
                from: 25,
                to: 75,
                color: 'rgb(255, 192, 0)', // yellow
                thickness: '50%'
            }, {
                from: 75,
                to: 100,
                color: 'rgb(155, 187, 89)', // green
                thickness: '50%'
            }]        
        },
    
        series: [{
            name: 'Speed',
            data: [80]
        }]
    
    });
});
        </script> -->
        
<!--  <script src="assets/js/canvasjs.min.js"></script>
  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: ""
      }, 
      animationEnabled: true,              
      data: [
      {        
        type: "splineArea",
        color: "rgba(54,158,173,.4)",

        dataPoints: [
        {y: 1},     
        {y: 8},     
        {y: 18},     
        {y: 28},     
        {y: 31},     
        {y: 14},     
        {y: 26},     
        {y: 9},     
        {y: 18},     
        {y: 11},     
        {y: 7},     
        {y: 1}             
        ]
      },
      {        
        type: "splineArea",
        color: "rgba(194,70,66,.4)",
        dataPoints: [
        {y: 14},     
        {y: 23},     
        {y: 21},     
        {y: 16},     
        {y: 12},     
        {y: 14},     
        {y: 18},     
        {y: 14},     
        {y: null},     
        {y: 12},     
        {y: 17},     
        {y: 14}             
        ]
      }             
      
      ]
    });

    chart.render();
  }
  </script>
-->
<!-- <script>
Highcharts.chart('container2', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: 'Average fruit consumption during one week'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    xAxis: {
        categories: [
            'Teat 01',
            'Teat 02',
            'Teat 03',
            'Teat 04',
            'Teat 05',
            'Teat 06',
            'Teat 07'
        ],
        plotBands: [{ // visualize the weekend
            from: 4.5,
            to: 6.5,
            /*color: 'rgba(0, 0, 0, .2)'*/
        }]
    },
    yAxis: {
        
        
        title: {
            text: ''
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' units'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.4
        }
    },
    series: [{
        name: 'Total Score',
        data: [3, 4, 3, 5, 4, 10, 12]
    }, {
        name: 'Accuracy Percent',
        data: [1, 3, 4, 3, 3, 5, 4]
    }]
});
</script> -->
 <!-- Video popup JavaScript -->
   <script type="text/javascript" async
 src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML">
</script>
        <script src="assets/js/jquery.popup.js"></script>
        <script src="assets/js/jquery.popup.dialog.min.js"></script>
        <script>
            (function ($) {
                $(function () {
                    $('[data-dialog]').on('click', function (e) {
                        var $this = $(e.target);
                        $($this.data('dialog')).attr('class', 'popup ' + $this.data('effect'));
                    });
                });
            })(jQuery);
            $(document).ready(function () {
                $('.popup').popup({
                    close: function () {
                        $(this).find('.embed-container').empty();
                    }
                });
                $(document).on('click', '[data-action="watch-video"]', function (e) {
                    e.preventDefault();
                    var plugin = $('#popup-video.popup').data('popup');
                    /* $('#popup-video.popup .embed-container').html(
                     '<iframe src="'
                     + e.currentTarget.href
                     + '?autoplay=1" frameborder="0" allowfullscreen />'
                     );*/
                    $('#popup-video.popup .embed-container').html(
                            '<video id="play" controls="controls" autoplay="autoplay"><source src="'
                            + e.currentTarget.href
                            + '" type="video/mp4"></video>'
                            );
                    plugin.open();
                });

            });
        </script>
        <script src="assets/js/jQuery-plugin-progressbar.js"></script>
        
        <script>
        $(".progress-bar2").loading();
        $('input').on('click', function (){
            $(".progress-bar2").loading();
        });
    </script>
        <!-- video popup -->
        <div class="popup effect-fade-scale" id="popup-video">
            <div class="embed-container"></div>
            <a href="#" class="popup-close">
                <i class="glyphicon glyphicon-remove"></i>
            </a>
        </div>
        <div class="popup effect-fade-scale" id="popup-dialog">
            <div class="popup-content">
                <h3>Video Modal</h3>
                <button class="popup-close btn btn-danger">x</button>
            </div>
        </div>		<script>		/*function openNav() {			document.getElementById("mySidenav").style.width = "360px";		}		function closeNav() {			document.getElementById("mySidenav").style.width = "0";		} */		$(document).ready(function () {  var trigger = $('.hamburger'),      overlay = $('.overlay'),     isClosed = false;    trigger.click(function () {      hamburger_cross();          });    function hamburger_cross() {      if (isClosed == true) {                  overlay.hide();        trigger.removeClass('is-open');        trigger.addClass('is-closed');        isClosed = false;      } else {           overlay.show();        trigger.removeClass('is-closed');        trigger.addClass('is-open');        isClosed = true;      }  }    $('[data-toggle="offcanvas"]').click(function () {        $('#mySidenav').toggleClass('toggled');  });  });		</script>


        

    </body>
</html>
