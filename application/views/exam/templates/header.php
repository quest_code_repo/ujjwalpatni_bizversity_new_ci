<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <title> Ujjwal Patni </title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <base href="<?=base_url();?>">
        

        <!-- Bootstrap Core CSS -->
        <link href="exam_assets/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="exam_assets/css/bootstrap-social.css" rel="stylesheet"/>
        <link href="exam_assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet"/>
        <link href="exam_assets/css/animate.css" rel="stylesheet"/>
        <!-- owl CSS -->
        <link href="exam_assets/css/owl.theme.css" rel="stylesheet"/>

        <link href="exam_assets/css/owl.transitions.css" rel="stylesheet"/>

        <link href="exam_assets/css/owl.carousel.css" rel="stylesheet"/>

        <!-- hover-min CSS -->

        <link href="exam_assets/css/hover-min.css" rel="stylesheet"/>

        <!-- yamm css -->

        <link href="exam_assets/css/yamm.css" rel="stylesheet"/>

        <!-- Fonts icons -->

        <link href="exam_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <!-- loader css -->

        <link href="exam_assets/css/loader.css" rel="stylesheet"/>

		<link href="exam_assets/css/nice-select.css" rel="stylesheet"/>

        <link href="exam_assets/css/jQuery-plugin-progressbar.css" rel="stylesheet" type="text/css">

        <!-- video popup CSS -->

		<link href="exam_assets/css/jquery.popup.css" rel="stylesheet" type="text/css">



        <!-- Custom CSS -->

        <link href="exam_assets/css/custom.css" rel="stylesheet"/>

        <!-- JQuery -->

        <script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>


    


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>

            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

        <![endif]-->

       

    </head>

    <body oncontextmenu="return false">
        <!-- loader html -->

        <div id="loading">

            <div id="loading-center">

                <div id="loading-center-absolute">

                    <div class="object" id="object_one"></div>

                    <div class="object" id="object_two" style="left:20px;"></div>

                    <div class="object" id="object_three" style="left:40px;"></div>

                    <div class="object" id="object_four" style="left:60px;"></div>

                    <div class="object" id="object_five" style="left:80px;"></div>

                </div>

            </div>

        </div>

        <!-- loader html end -->





        <!-- Header html -->

        <header class="header-main test-header" style="position: fixed; width: 100%;z-index:10;">

            <div class = "navbar navbar-default yamm main-menu " role = "navigation">

                <div class="container-fluid">

                  <div class="row">
                          <div class = "navbar-header">

                        <!--<button type = "button" class = "navbar-toggle" data-toggle = "collapse" data-target = "#navbar-collapse">

                            <span class = "sr-only">Toggle navigation</span>

                            <span class = "icon-bar"></span>

                            <span class = "icon-bar"></span>

                            <span class = "icon-bar"></span>

                        </button>-->

                        <a class="navbar-brand logo wow fadeInUp" href="javascript:;">

                            <img src="<?php echo base_url()?>front_assets/images/other/logo.png"/>

                        </a>

                    </div>

                    <div class = "collapse2 navbar-collapse2" id = "navbar-collapse">

                        <ul class="nav navbar navbar-right navbar-timer" style="display:<?php if($this->uri->segment(1)=='exam' || $this->uri->segment(1)=='entrance'){echo 'none';}?>">
                                 <li class="dropdown new-biz-dropdown">
                                    <a href="javascript:void(0)" class="dropbtn user-pic"><img src="assets/images/other/profile.jpg" class="img-responsive" style="height: 40px;"></a>
                                    <ul class="dropdown-content profil-dropdown">
                                        <li>
                                            <div class="media">
                                              <div class="media-left">                                     
                                                <img src="assets/images/other/profile.jpg" class="media-object" style="height: 45px;">
                                              </div>

                                             

                                              <?php
                                                  if(!empty($this->session->userdata('id')))
                                                  {                              
                                              ?>
                                              <div class="media-body">
                                                <h5 class="media-heading"><?php echo ($this->session->userdata('username'));?></h5>
                                                <p><?php echo ($this->session->userdata('full_name'));?></p>
                                              </div>
                                                    <?php
                                                  }
                                                  else
                                                  {
                                                    ?>
                                              <div class="media-body">
                                                <h5 class="media-heading">Test</h5>
                                                <p>Test@gmail.com</p>
                                              </div>
                                                    <?php
                                                  }
                                              ?>
                                            
                                            </div>
                                        </li>
                                        <li><a href="my-courses"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Dashboard</a></li>
                                        <li><a href="dashboard/my_profile"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Profile</a></li>
                                        <li><a href="mentoring/coming_soon"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp; Change Password</a></li>
                                        <li><a href="<?php echo base_url('home/logout')?>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; Log Out</a></li>
                                     </ul>
                                </li> 

                            <li><span id="checks_choices"> <?php if($head_text == 1){ ?>TEST START <?php } else { ?>TIME LEFT <?php } ?></span>  <span id="exam-time-left"> </span></li>

                            

                        </ul>

                      

                            <?php 

                        //print_r($exam);

                        ?>

                    </div>
                    <div class="pull-right toggle-line">
                      <!--<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>-->
                        <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                            <span class="hamb-top"></span>
                            <span class="hamb-middle"></span>
                            <span class="hamb-bottom"></span>
                        </button>
                    </div>

                  </div>
                </div>

            </div>



        </header>