<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	 if ( ! function_exists('checkAuth'))
     { 
		function checkAuth()
		{
			$CI =& get_instance();	
			$CI->load->library('Session');
			if($CI->session->userdata('emp_code')!='')
			{
				return true;   
			}
			else 
			{
				redirect("admin/authentication");
			}
		}
	 }

if ( ! function_exists('admin_view'))
{       
	    function admin_view($view = '',$data = array()){
		$CI =& get_instance();	
		$CI->load->library('session');
		if($CI->session->userdata('emp_code')!='')
		{ 
			$chk = array(
				'id' => $CI->session->userdata('id'),
				'status' =>'active'
			 );
            $query = $CI->db->get_where('employees',$chk);
			$image_data = $query->result_array();
			 // echo "<pre>";print_r($image_data);die;
			if (!empty($image_data)) {
				$data['profile_img'] = $image_data[0]['photo'];
			}else{
				$data['profile_img'] = '';
			}
			if($CI->session->userdata('designation')==1)
			{
			$chk = array(
				'designation_id' => $CI->session->userdata('designation'),
				'parent_menu' =>0
			 );
            $query = $CI->db->get_where('admin_menu',$chk);
			$data['perent_menulist'] = $query->result_array();
			}
			else {
			$chk = array('emp_id' => $CI->session->userdata('id'));
           // $query = $CI->db->get_where('emp_menu_rel',$chk);
				$CI->db->select('*');
				$CI->db->from('admin_menu');
				$CI->db->join('emp_menu_rel', 'emp_menu_rel.menu_id = admin_menu.id'); 
				$CI->db->where('emp_menu_rel.emp_id', $CI->session->userdata('id'));
				$query = $CI->db->get();
				$data['perent_menulist'] = $query->result_array();
			}
		}
		else
		{
			$CI->session->sess_destroy();
			redirect('admin/authentication');
		}
		
		/* ------------------------------------Total Earning -------------------------------------*/
				$start_date=date('Y-m-d')." 00:00:00";
				$end_date=date('Y-m-d')." 23:59:59";
				// $CI->db->select('sum((order_items.`price`)*order_items.qua) as total_amount');
				//$CI->db->select('sum(orders.order_total) as total_amount');
					 //  $CI->db->from("orders");
					   // $CI->db->join('orders','order_items.order_id=orders.order_id');
					//   $CI->db->where('order_time >=',$start_date);
					  // $CI->db->where('order_time <=',$end_date);
					//	$query = $CI->db->get();
						//$total_amt=$query->result_array();
					//$data['total_amount']=$total_amt[0]['total_amount'];

				$data["list_count"]='';
					
			/*--------------------------------------- Total Orders ----------------------------------------*/
				//$CI->db->select('count(`order_id`) as total_order');
				//$CI->db->from('orders');
				//$CI->db->where('order_time >=',$start_date);
				//$CI->db->where('order_time <=',$end_date);
				//$query = $CI->db->get();
				//$total_order=$query->result_array();
				//$data['total_order']=$total_order[0]['total_order'];
				$data['admin_show']="Admin";
				
				
		$CI->load->view('admin/template/header',$data);
		if($CI->session->userdata('designation')!=1)
		{
		$url=$CI->uri->segment(2).'/'.$CI->uri->segment(3); 
		$CI->db->select('*');
		$CI->db->from('admin_menu');
		$CI->db->join('emp_menu_rel', 'emp_menu_rel.menu_id = admin_menu.id'); 
		$CI->db->where('emp_menu_rel.emp_id', $CI->session->userdata('id'));
		$CI->db->where('admin_menu.url', $url);
		$query11 = $CI->db->get();
		$permit=$query11->result_array();
		if(!empty($permit[0]))
		{  //print_r($permit);
			$CI->load->view($view, $data);
		} else { 
		$CI->load->view('admin/forbidden', $data); 
		}
		}
		else {
		$CI->load->view($view, $data);
		}
	}  
}
if(!function_exists('load_submenu'))
{
	function load_submenu($menuid)
	{  

	    $CI =& get_instance();
	   // echo 'designation'.$CI->session->userdata('designation'); 
		$chk = array(
				'designation_id' => $CI->session->userdata('designation'),
				'parent_menu' =>$menuid
			 );
            $query = $CI->db->get_where('admin_menu',$chk);

			$sub_menulist = $query->result_array();
			// print_r($sub_menulist);
			if(!empty($sub_menulist)) {
			return $sub_menulist; }else { return false; }
	}
}

if ( ! function_exists('check_submenu_per'))
{
function check_submenu_per($menuid)
	{   
	    $CI =& get_instance();
		$chk = array(
				'emp_id' => $CI->session->userdata('id'),
				'menu_id' =>$menuid
			 );
            $query = $CI->db->get_where('emp_menu_rel',$chk);
			$check_per = $query->result_array();
			if(!empty($check_per)) {
			return 1; }else { return 0; }
	}
}



if ( ! function_exists('get_pname_for_placed_order'))
{
function get_pname_for_placed_order($product_id,$product_type)
	{   
	    $CI =& get_instance();
		$CI->load->model('front_end/cat_frontend');
			if ($product_type == 'programe') {
				$where = array('tbl_programs.id'=>$product_id,'tbl_programs.status'=>'active','tbl_programs.deleted'=>'0','tbl_programs_category.status'=>'active','tbl_programs_category.deleted'=>'0');
				$column = array('*', 'program_name as item_name', 'name as category');
            	$items_details = $CI->cat_frontend->getdata_orderby_where_join2('tbl_programs','tbl_programs_category','category_id','id',$where,$column,'','');
			} else {
				$column = array('*', 'pname as item_name', 'name as category');
				$where = array('product_id'=>$product_id,'products.active_inactive'=>'active','product_categories.active_status'=>'active');
            	$items_details = $CI->cat_frontend->getdata_orderby_where_join2('products','product_categories','product_category_id','id', $where, $column, '','');
			}
            // echo "<pre>";print_r($items_details);die;
		
			if(!empty($items_details))
			{
				return $items_details;
			}	
	}
}

function get_my_questions($id)
	{   
	    $CI =& get_instance();
		
		$chk = array('test_id' => $id);

            $query = $CI->db->get_where('tbl_add_question',$chk);
			
			$check_per = $query->result_array();

			if(!empty($check_per)) {
			return COUNT($check_per); }else { return 0; }
	}