<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	if ( ! function_exists('get_cart_item_details')){
		function get_cart_item_details($product_id,$product_type){
			$CI =& get_instance();
			$CI->load->model('front_end/cat_frontend');
			if ($product_type == 'programe') {
				$where = array('tbl_programs.id'=>$product_id,'tbl_programs.status'=>'active','tbl_programs.deleted'=>'0','tbl_programs_category.status'=>'active','tbl_programs_category.deleted'=>'0');
				$column = array('*', 'program_name as item_name', 'name as category');
            	$items_details = $CI->cat_frontend->getdata_orderby_where_join2('tbl_programs','tbl_programs_category','category_id','id',$where,$column,'','');
			} else {
				$column = array('*', 'pname as item_name', 'name as category');
				$where = array('product_id'=>$product_id,'products.active_inactive'=>'active','product_categories.active_status'=>'active');
            	$items_details = $CI->cat_frontend->getdata_orderby_where_join2('products','product_categories','product_category_id','id', $where, $column, '','');
			}
            // echo "<pre>";print_r($items_details);die;
		
			if(!empty($items_details))
			{
				return $items_details;
			}	
		}  
	}


	// *******************  Checking product exists or not in user ******************//


	if ( ! function_exists('check_item_in_cart_user')){
		function check_item_in_cart_user($product_id="",$user_id="",$product_type=""){
			$CI =& get_instance();
			$CI->load->model('front_end/cat_frontend');
				
            	$items_details = $CI->cat_frontend->getwheres('cart_items',array('product_id'=>$product_id,'user_id'=>$user_id,'product_type'=>$product_type),'');
            	
			if(!empty($items_details))
			{
				return true;
			}
			else{
					return false;
			}	
		}  
	}



	// ******************  Getting values for Email Template This is only for product or elearing section *****************************//

	if ( ! function_exists('get_cart_item_details_of_program')){
		function get_cart_item_details_of_program($product_id="",$user_id="",$product_type=""){
			$CI =& get_instance();
			$CI->load->model('front_end/cat_frontend');
				
            	$where = array('tbl_programs.id'=>$product_id,'tbl_programs.status'=>'active','tbl_programs.deleted'=>'0','user_id'=>$user_id,'product_type'=>'programe');
				$column = 'program_name,program_date,program_end_date,location,p_quantity,actual_price,cart_id';
            	$items_details = $CI->cat_frontend->getdata_orderby_where_join2('tbl_programs','cart_items','id','product_id',$where,$column,'','');
            	
			if(!empty($items_details))
			{
				return $items_details;
			}
			else{
					return false;
			}	
		}  
	}

	//************************************************************************************//


	// *****************Getting values for elearning and product not program ********************//


	if ( ! function_exists('get_cart_item_details_for_elearning_product')){
		function get_cart_item_details_for_elearning_product($product_id="",$user_id="",$product_type=""){
			$CI =& get_instance();
			$CI->load->model('front_end/cat_frontend');

				if($product_type == "programe")
				{
					return false;
				}
				else
				{	
							$where = array('products.product_id'=>$product_id,'products.active_inactive'=>'active','products.deleted'=>'0','user_id'=>$user_id);
								$column = 'pname,p_quantity,actual_price,cart_id';
				            	$items_details = $CI->cat_frontend->getdata_orderby_where_join2('products','cart_items','product_id','product_id',$where,$column,'','');
				            	
							if(!empty($items_details))
							{
								return $items_details;
							}
							else{
									return false;
							}	
				}
				
            	
		}  
	}


	//*****************************************************************************************//



	// ************** Getting Delivery Address Details ************************/

	if ( ! function_exists('get_delivery_address')){
		function get_delivery_address($delivery_id=""){
			$CI =& get_instance();
			$CI->load->model('front_end/cat_frontend');
				
            	$delivery_details = $CI->cat_frontend->getwheres('user_address',array('user_address_id'=>$delivery_id),'');
            	
			if(!empty($delivery_details))
			{
				return $delivery_details;
			}
			else{
					return false;
			}	
		}  
	}

	//***************************************************************************//


	// ****************get total no of lectures ********************//

		if ( ! function_exists('get_delivery_address')){
		function get_delivery_address($delivery_id=""){
			$CI =& get_instance();
			$CI->load->model('front_end/cat_frontend');
				
            	$delivery_details = $CI->cat_frontend->getwheres('user_address',array('user_address_id'=>$delivery_id),'');
            	
			if(!empty($delivery_details))
			{
				return $delivery_details;
			}
			else{
					return false;
			}	
		}  
	}


	//************************************************************//


	// ******************************  get chapter lecture acc to course **********************//

	if ( ! function_exists('get_chapter_lecture_acc_to_course')){
		function get_chapter_lecture_acc_to_course($chapter_id="",$course_id=""){
			$CI =& get_instance();
			$CI->load->model('home_model');
				
            	$lecture_details = $CI->home_model->getwhere_data('tbl_lecture',array('ch_id'=>$chapter_id,'course_id'=>$course_id,'lect_status'=>'active'),'','','');
            	
			if(!empty($lecture_details))
			{
				return $lecture_details;
			}
			else{
					return false;
			}	
		}  
	}


	//***************************************************************************************//


	// *******************************  Get Chapter Resource  ac to course **************************//

		if ( ! function_exists('get_chapter_resource_acc_to_course')){
		function get_chapter_resource_acc_to_course($chapter_id="",$course_id=""){
			$CI =& get_instance();
			$CI->load->model('home_model');
				
            	$lecture_details = $CI->home_model->getwhere_data('tbl_lecture',array('ch_id'=>$chapter_id,'course_id'=>$course_id,'lect_status'=>'active','lecture_type !='=>1),'','','');
            	
			if(!empty($lecture_details))
			{
				return $lecture_details;
			}
			else{
					return false;
			}	
		}  
	}


	//**********************************************************************************//



	//*****************  get total 3/3 lectures ************************//

	if ( ! function_exists('get_total_lecture_of_particular_chapter')){
		function get_total_lecture_of_particular_chapter($chapter_id="",$course_id=""){
			$CI =& get_instance();
             $sql = "SELECT count(lect_id) as total_lectures
                FROM tbl_lecture
                WHERE course_id = $course_id AND ch_id = $chapter_id AND lect_status = 'active' ";
        $query = $CI->db->query($sql);
        $reultsss = $query->result();
            	
			if(!empty($reultsss))
			{
				return $reultsss;
			}
			else{
					return false;
			}	
		}  
	}


	//*******************************************************************//


	//*******************************************************************//

	if ( ! function_exists('get_course_name_acc_to_chapter')){
		function get_course_name_acc_to_chapter($chapter_id=""){
			$CI =& get_instance();
			$CI->load->model('front_end/cat_frontend');
				
							$where = array('product_type'=>'e_learning','active_inactive'=>'active','deleted'=>'0','ch_id'=>$chapter_id);
								$column = 'pname,image_url';
				            	$items_details = $CI->cat_frontend->getdata_orderby_where_join2('products','tbl_chapter','product_id','sem_id',$where,$column,'','');
				            	
							if(!empty($items_details))
							{
								return $items_details;
							}
							else{
									return false;
							}	
				
				
            	
		}  
	}



	//*************************************************************************//


	//*****************************************************************//

		if ( ! function_exists('get_exam_acc_to_chapter_course')){
		function get_exam_acc_to_chapter_course($chapter_id="",$course_id=""){
			$CI =& get_instance();
             $sql = "SELECT test_id,test_name
                FROM tbl_test
                WHERE sem_id = $course_id AND chapter_id = $chapter_id AND test_status = 'active' ORDER BY test_id DESC ";
        $query = $CI->db->query($sql);
        $reultsss = $query->result();
            	
			if(!empty($reultsss))
			{
				return $reultsss;
			}
			else{
					return false;
			}	
		}  
	}


	//*************************************************************************//


	//*****************************************************************//

		if ( ! function_exists('get_star_rating_for_my_courses')){
		function get_star_rating_for_my_courses($course_id=""){
			$CI =& get_instance();
             $sql = "SELECT AVG(rating_star) as overall_avg 
                FROM star_rating
                WHERE course_id = $course_id  ORDER BY rating_id DESC ";
        $query = $CI->db->query($sql);
        $star_rating = $query->result();
            	
			if(!empty($star_rating))
			{
				return $star_rating;
			}
			else{
					return false;
			}	
		}  
	}


	//*************************************************************************//

