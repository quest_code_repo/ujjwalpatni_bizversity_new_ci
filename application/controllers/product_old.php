<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        //$this->load->helper('front_end/cat_frontend');
        
    }

    public function index($url="")
    {

        if(!empty($url))
        {       
                $categoryId = $this->home_model->getwheres_data('product_categories',array('category_url'=>$url));
                 $data['Products'] = $this->home_model->getwheres_data('products',array('product_type'=>'product','active_inactive'=>'active','product_category_id'=>$categoryId[0]->id));
                 $data['Product_category'] =  $this->home_model->getwheres_data('product_categories',array('parent_category'=>14));
        }
        else
        {
                $data['Products'] = $this->home_model->getwheres_data('products',array('product_type'=>'product','active_inactive'=>'active'));
                $data['Product_category'] =  $this->home_model->getwheres_data('product_categories',array('parent_category'=>14));
        }

    	
    	$this->load_view('products/product_view',$data);
    }

}