<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        //$this->load->model('front_end/cat_frontend');
        
    }

    public function index()
    {
        $data['Blogs'] = $this->home_model->getwheres_data('blog',array('status'=>'active'));
      	$this->load_view('blog/blog',$data);
    }


    public function blog_detail($url="")
    {

        if(!empty($url))
        {
            $data['BlogDetail'] = $this->home_model->getwheres_data('blog',array('blog_url'=>$url));
            $this->blog_view('blog/blog_detail',$data);
        }
        else
        {
            redirect('blog','refresh');
        }
    	
    }

    public function get_blog_autocomplete()
    {
        $getData = $this->input->get();
        $return_arr = array();

        if(!empty($getData))
        {
             $where = array('status'=>'active');
             $res= $this->home_model->get_autocomplete_blog('id,blog_title,blog_url',$getData["term"],'blog',$where);

             if(!empty($res) && count($res)>0)
        {
            $i=0;

          foreach ($res as $r_val) 
          {
              $i++;
              if($i==10)
              {
                break;
              }

           $row_array['value']                = $r_val->blog_title;
           $row_array['url']                  = $r_val->blog_url;
           array_push($return_arr, $row_array);
          }
       }

       }

       echo json_encode($return_arr);
    }


    public function search_blog_detail()
    {
        $postData = $this->input->post('blog_url');

        if(!empty($postData))
        {
            $data['BlogDetail'] = $this->home_model->getwheres_data('blog',array('blog_url'=>$postData));
            $this->load_view('blog/blog_detail',$data);
        }
        else
        {
            redirect('blog','refresh');
        }

    }

}