<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Speaker_action extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model','front');
		$this->load->helper('xcrud');
		$this->load->helper('main_template_helper');
		$this->load->library(array('form_validation','session')); 
	}


	public function index(){

		$data['all_data'] = $this->front->get_data_orderby_where('tbl_speakers','',array('deleted'=>'0','status'=>'active'),'');

		admin_view('admin/speakers/speaker_list', $data);
	}


	public function add_speakers(){

		if($_POST){


			$this->form_validation->set_rules('speaker_name', 'Speaker Name', 'required');
			$this->form_validation->set_rules('speaker_description', 'Description', 'required');
			$this->form_validation->set_rules('speaker_status', 'Status', 'required');
			//$this->form_validation->set_rules('speaker_image', 'Profile Picture', 'required');
			

			if ($this->form_validation->run() == TRUE)
			{

				$name_pro = $this->input->post('speaker_name');
				$description = $this->input->post('speaker_description');
				$status = $this->input->post('speaker_status');
				$image_speaker = $this->input->post('speaker_image');

				$config = array(
					'upload_path' => "./uploads/speaker_image",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('speaker_image'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/Speaker_action/add_speakers');
					exit();
				}


				$check_urls = str_replace(' ', '-', $name_pro);

				$query_for_urls = $this->front->get_sql_record_obj("SELECT * FROM `tbl_speakers` WHERE deleted = '0' AND url LIKE '%$check_urls%' ORDER BY id DESC");


				if(!empty($query_for_urls)){


					$id_take = $query_for_urls[0]->id;

					$add_plus_one = $id_take+1;

					$url = $check_urls.'-'.$add_plus_one;

				} else {

					$url = str_replace(' ', '-', $name_pro);
				}




				$data_insert = array(
					'speaker_name'=>$name_pro,
					'description'=>$description,
					'speaker_image'=>$uimg,
					'url'=>$url,
					'status'=>$status,
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('id')
					);


				$speaker_insert = $this->front->insert_data('tbl_speakers', $data_insert);
				if ($speaker_insert){
					$this->session->set_flashdata("success", "Speakers added successfully");

					redirect('admin/Speaker_action/');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}


			}


		}

		//$data['cats'] = $this->front->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active'),'');

		admin_view('admin/speakers/add_speaker',$data);
	}


	public function speaker_list(){

		$data['all_data'] = $this->front->get_data_orderby_where('tbl_programs','',array('deleted'=>'0','status'=>'active'),'');

		admin_view('admin/programs/program_list',$data);
	}

	public function delete_data_coupons()
   	{

   		$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('user_id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('id' => $id);
    	$delet_master = $this->front->update_data('tbl_speakers',$master_delete_arr,$where);

    	
    	if($delet_master){

    		echo '1';
    	}

    }


    public function edit_speaker_data(){


    	$speaker_id = base64_decode($this->input->get('speaker_id'));

    	if($_POST){

    			$name_pro = $this->input->post('speaker_name');
				$description = $this->input->post('speaker_description');
				$status = $this->input->post('speaker_status');
				$image_speaker = $this->input->post('speaker_image');
				$update_id = base64_decode($this->input->post('updated_id'));

				$config = array(
					'upload_path' => "./uploads/speaker_image",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('speaker_image'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}

				if($uimg == ''){

					$uimg = $this->input->post('if_no_image');

				}

				$update_data = array(
					'speaker_name'=>$name_pro,
					'description'=>$description,
					'speaker_image'=>$uimg,
					//'url'=>str_replace(' ', '-', $name_pro),
					'status'=>$status,
					'updated_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
					);

				$ups_where = array('id'=>$update_id);


				$speaker_updates = $this->front->update_data('tbl_speakers', $update_data,$ups_where);

				if($speaker_updates){

					$this->session->set_flashdata("update", "Record updated successfully");

					redirect('admin/Speaker_action/');


				}




    	}

    	$data['all_data'] = $this->front->get_data_orderby_where('tbl_speakers','',array('deleted'=>'0','status'=>'active','id'=>$speaker_id),'');


    	admin_view('admin/speakers/edit_speaker', $data);

    }


} 

?>