<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bulk_import_quiz extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/Admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session','image_lib','')); 
		 $this->load->library('excel');

		
	}
	
	public function bulk_upload_quiz()
	{
	

	$data['test_list']=$this->Admin_common_model->fetch_record('tbl_test');
	$data['chapter_list']=$this->Admin_common_model->fetch_record('tbl_chapter');
	$data['topic_list']=$this->Admin_common_model->fetch_record('tbl_lecture');
		admin_view('admin/bulk_quiz_upload',$data);
    }
    public function quiz_bulk_submit()
	{

	 if(isset($_POST['btn-upload']))
	 {     
	 	$test_id = $this->input->post('test_name');
        $chapter_list = $this->input->post('chapter_name');
        $topic_list = $this->input->post('topic_name');

         $configUpload['upload_path'] = './uploads/';
         $configUpload['allowed_types'] = 'xls|xlsx|csv';
         $configUpload['max_size'] = '5000';
         $this->load->library('upload', $configUpload);
         $this->upload->do_upload('userfile');	
         $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
         $file_name = $upload_data['file_name']; //uploded file name
		 $extension=$upload_data['file_ext'];    // uploded file extension
		
//$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
 $objReader= PHPExcel_IOFactory::createReader('Excel2007');	// For excel 2007 	  
          //Set to read only
          $objReader->setReadDataOnly(true); 		  
        //Load excel file
		 $objPHPExcel=$objReader->load(FCPATH.'uploads/'.$file_name);		 
         $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel      	 
         $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
          //loop from first data untill last data
          for($i=2;$i<=$totalrows;$i++)
          {
              $Question_Name= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();			
              $Question_Image= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(); //Excel Column 1
			  $Question_Marks= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); //Excel Column 2
			  $Negative_Marks=$objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 3
			  $Answer_1=$objWorksheet->getCellByColumnAndRow(4,$i)->getValue(); 

			  $Answer_2=$objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); 

			  $Answer_3=$objWorksheet->getCellByColumnAndRow(6,$i)->getValue(); 
			  $Answer_4=$objWorksheet->getCellByColumnAndRow(7,$i)->getValue(); 
			  $Right_Answer=$objWorksheet->getCellByColumnAndRow(8,$i)->getValue(); 
			  $Explaination=$objWorksheet->getCellByColumnAndRow(9,$i)->getValue(); 
			  $Concept=$objWorksheet->getCellByColumnAndRow(10,$i)->getValue(); 
			  $Difficulty_Level=$objWorksheet->getCellByColumnAndRow(11,$i)->getValue(); 

			  $data_user= array(
                'test_id' => $test_id,
                'chapter_id' => $chapter_list,
                'lecture_id' => $topic_list,
                'question_desc' => $Question_Name,
                'ques_name_have_image' => $Question_Image,
                'que_marks' =>  $Question_Marks,
                'ques_negative_marks' => $Negative_Marks,
                'ans_1' => $Answer_1,
                'ans_2' => $Answer_2,
                'ans_3' => $Answer_3,
                'ans_4' => $Answer_4,
                'ques_right_ans' => $Right_Answer,
                'explain_right_answer' => $Explaination,
                'ques_concept_wise' => $Concept,
                'ques_difficulty_level' => $Difficulty_Level,
                'ques_status' => 'active');
			  $this->Admin_common_model->insert_data('tbl_add_question', $data_user);
						  
          }
             unlink('././uploads/'.$file_name); //File Deleted After uploading in database .			 
             redirect(base_url() . "put link were you want to redirect");
	 }
	else
    {
        echo "Moveing failed";

    }
}


	








	public function import_question_images()
	 {

	 	$data = array();
        if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name']))
        {
            $filesCount = count($_FILES['userFiles']['name']);
            for($i = 0; $i < $filesCount; $i++)
            {
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

                $uploadPath = 'uploads/question_images/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

  //               if (!$this->upload->do_upload('userFile'))
		// {
		// 	$error = $this->upload->display_errors();	 
	 //        $this->session->set_flashdata('error',$error."Insert Only JPG,PNG Format");
 			
		// }

            if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i]['modified'] = date("Y-m-d H:i:s");

            }

        
         
        }
      $this->session->set_flashdata('success',"Images Uploaded Successfully");
      redirect('admin/bulk_import_quiz/bulk_upload_quiz');           
           
        }
	  admin_view('admin/select_uploading_category');
      
	 }    
	
}
	
