<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
     
	public function view($msg = NULL){
		checkAuth();
		$xcrud = Xcrud::get_instance()->table('news');
        /**********view pages***********/
        $xcrud->columns('title,content,start_time,end_time,created_at');
		$xcrud->hide_button('save_new');
		$xcrud->change_type('content','textarea');
		$xcrud->order_by('id','desc');
		$xcrud->table_name('');
		$xcrud->label('content','Description');
		$xcrud->relation('created_by','employees','id','name');
        $data['content'] = $xcrud->render();
		$data['title'] ='News Control';
		admin_view('admin/allotmenu_list', $data);
    }
}