<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->library(array('form_validation','session')); 
	}

	// public function checkAuth()
	// {
	// 	// print_r($user_details);
	// 	// die();
 //       	if($this->session->userdata('emp_code')!= '')
	// 	{
	// 		return true;
	// 	}
	// 	else
	// 	{
	// 		$this->session->sess_destroy();
 //            redirect('admin/authentication');
	// 	}
	// }
	 
	public function index($msg = NULL){
		checkAuth();
		$data['msg']='';
		// echo "test";
		// die();
       admin_view('admin/dashboard', $data);
    } 
	
}