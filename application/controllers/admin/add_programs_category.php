<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_programs_category extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model','admin');
		$this->load->helper('xcrud');
		$this->load->helper('main_template_helper');
		$this->load->library(array('form_validation','session')); 
	}

	public function index(){

		if($_POST)
		{


			$this->form_validation->set_rules('category_name', 'Category Name', 'required');
			$this->form_validation->set_rules('active_status', 'Product Price', 'required');

			if ($this->form_validation->run() == TRUE)
			{

				$name      = $this->input->post('category_name');
				$status    = $this->input->post('active_status');
				$video_url = $this->input->post('video_url');


				$check_urls = str_replace(' ', '-', $name);

				$query_for_urls = $this->admin->get_sql_record_obj("SELECT * FROM `tbl_programs_category` WHERE deleted = '0' AND url LIKE '%$check_urls%' ORDER BY id DESC");

				$duplicateCat = $this->admin->get_sql_record_obj("SELECT * FROM `tbl_programs_category` WHERE deleted = '0' AND name LIKE '%$name%' ORDER BY id DESC");

				if(!empty($duplicateCat) && count($duplicateCat)>0)
				{
					$this->session->set_flashdata("success", "Category Already Exists, Please Try Another Name");

					redirect('admin/add_programs_category/category_list');
					die();
				}



				if(!empty($query_for_urls)){


					$id_take = $query_for_urls[0]->id;

					$add_plus_one = $id_take+1;

					$url = $check_urls.'-'.$add_plus_one;

				} else {

					$url = str_replace(' ', '-', $name);
				}

				$data_insert = array('name'=>$name,
					'status'=>$status,'url'=>$url,'created_at'=>date('Y-m-d H:i:s'),'created_by'=>$this->session->userdata('id'),'video_url'=>$video_url);

				$cat_insert = $this->admin->insert_data('tbl_programs_category', $data_insert);
				if ($cat_insert)
				{
					$this->session->set_flashdata("success", "Category added successfully");

					redirect('admin/add_programs_category/category_list');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}

			}
			
			
		}


		admin_view('admin/programs/add_program_category');
	}

	/*_____ worked by kamlesh _____*/
	public function category_list(){
		checkAuth();

		$xcrud = Xcrud::get_instance()->table('tbl_programs_category');
		$xcrud->where('deleted =','0');
		$xcrud->unset_print();
		$xcrud->unset_csv();
		$xcrud->unset_add();
		$xcrud->order_by('id','ASC');
		$xcrud->label('name','Program Category');
		$xcrud->fields(array('name','video_url','status'));
		$xcrud->columns(array('name','url','video_url','status'));

		$xcrud->validation_required(array('name','url','video_url'));

		$data['content'] = $xcrud->render();
		$data['title'] ='Category List';
		admin_view('admin/programs/category_list', $data);
	} 

	public function vip_explored_content(){
		checkAuth();

		$xcrud = Xcrud::get_instance()->table('tbl_vip_explored_content');
		
		$xcrud->unset_print();
		$xcrud->unset_csv();
		$xcrud->order_by('exp_id','ASC');

		// $cond = array('status' => 'active');
		// $xcrud->where($cond);

		$xcrud->change_type('icon', 'image' , '', array('width' => 300, 'height' => 200));

		$xcrud->label('programs_category_id','Program Category');

		// $xcrud->fields(array('content','programs_category_id','icon','status'));
		$xcrud->columns(array('content','programs_category_id','icon','status'));

		$xcrud->validation_required(array('content','programs_category_id','icon'));

		$xcrud->relation('programs_category_id','tbl_programs_category','id','name',array('tbl_programs_category.status' => 'active'));

		$data['content'] = $xcrud->render();
		$data['title'] ='Explored Content';
		admin_view('admin/programs/vip-explored-content', $data);

	} 

	public function add_programs(){
		if($_POST){

			$this->form_validation->set_rules('program_name', 'Program Name', 'required');
			$this->form_validation->set_rules('program_date', 'Program Date', 'required');
			$this->form_validation->set_rules('program_end_date', 'Program End Date', 'required');
			$this->form_validation->set_rules('program_category', 'Program Category', 'required');
			$this->form_validation->set_rules('program_price', 'Program Price', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$name_pro = $this->input->post('program_name');
				$date = $this->input->post('program_date');

				$from_hour = $this->input->post('start_hours');
				$from_minutes = $this->input->post('start_minutes');
				$from_seconds = $this->input->post('start_seconds');
				$from_ams_pms = $this->input->post('start_ams_pms');

				
				if($from_ams_pms=='PM'){

					$fins_time = $from_hour+12;

					$combine_time_data_from = $fins_time.':'.$from_minutes.':'.$from_seconds;

					$usable_time =  date("H:i:s", strtotime($combine_time_data_from));
					 
				} else {

					$combine_time_data_from = $from_hour.':'.$from_minutes.':'.$from_seconds;

					$usable_time =  date("H:i:s", strtotime($combine_time_data_from));

				}

				$date_final_from = $date.' '.$usable_time;

				//$from_time = $this->input->post('program_from_time');
				$to_time = $this->input->post('program_end_date');

				$till_hour 	  = $this->input->post('end_hours');
				$till_minutes = $this->input->post('end_minutes');
				$till_seconds = $this->input->post('end_seconds');
				$till_ams_pms = $this->input->post('end_ams_pms');


				if($till_ams_pms=='PM'){

				$fins_time_seconds = $till_hour+12;

				$combine_time_data_till = $fins_time_seconds.':'.$till_minutes.':'.$till_seconds;

				$usable_time_tills =  date("H:i:s", strtotime($combine_time_data_till));
				 

				} else {

					$combine_time_data_till = $till_hour.':'.$till_minutes.':'.$till_seconds;

					$usable_time_tills =  date("H:i:s", strtotime($combine_time_data_till));

				}

				$date_final_till = $to_time.' '.$usable_time_tills;

				// $description = $this->input->post('progrm_description');
				$pro_status = $this->input->post('program_status');
				$pro_category = $this->input->post('program_category');
				$speakers_name = $this->input->post('event_speakers');
				$location = $this->input->post('location');
				$price = $this->input->post('program_price');
				$city_ids = $this->input->post('program_city');
				// $insert_courses = $this->input->post('add_courses');
								
				$data_insert = array(
					'program_name'	=>$name_pro,
					'category_id'	=>$pro_category,
					'city_ids'	    =>$city_ids,
					'location'		=>$location,
					'program_date'	=>$date_final_from,
					'program_end_date'=>$date_final_till,
					'price'			=>$price,
					'status'		=>$pro_status,
					'created_at'	=>date('Y-m-d H:i:s'),
					'created_by'	=>$this->session->userdata('id')
				);		

				$program_insert = $this->admin->insert_data('tbl_programs', $data_insert);
				if ($program_insert)
				{
					if(!empty($speakers_name)){
		        		$data_speaker=array();
				    	foreach($speakers_name as $spek_id) 
				   	 	{
				       		$data_speaker = array('speaker_id' => $spek_id,'program_id'=>$program_insert,'created_by'=>$this->session->userdata('id'),'created_at'=>date('Y-m-d H:i:s'),'status'=>'active');
				       		$insert_exam = $this->admin->insert_data('tbl_program_speaker_rel',$data_speaker);
				  	  	}
					}


					/****************Insert Courses***********/


					if(!empty($insert_courses)){
		        		
		        		$data_courses=array();

				    	foreach($insert_courses as $courseid) {

				       $data_courses = array('course_id' => $courseid,'program_id'=>$program_insert,'created_by'=>$this->session->userdata('id'),'created_at'=>date('Y-m-d H:i:s'),'status'=>'active');
				       	$this->admin->insert_data('tbl_program_course_rel',$data_courses);
				  	  }
					}


					$this->session->set_flashdata("success", "Program added successfully");

					redirect('admin/add_programs_category/program_list');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}


			}


		}

		$data['cats'] = $this->admin->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active'),'');

		$data['speakers'] = $this->admin->get_data_orderby_where('tbl_speakers','',array('deleted'=>'0','status'=>'active'),'');

		$data['all_cities'] = $this->admin->get_data_orderby_where('cities','',array(),'');

		admin_view('admin/programs/add_program',$data);
	}

	/*_____ worked by kamlesh _____*/ 

	public function add_programs_old(){

		if($_POST){

			$this->form_validation->set_rules('program_name', 'Program Name', 'required');
			$this->form_validation->set_rules('program_date', 'Program Date', 'required');
			$this->form_validation->set_rules('program_end_date', 'Program End Date', 'required');
			//$this->form_validation->set_rules('program_from_time', 'Program From Time', 'required');
			//$this->form_validation->set_rules('program_to_time', 'Program To Time', 'required');
			$this->form_validation->set_rules('progrm_description', 'Program Description', 'required');
			$this->form_validation->set_rules('program_status', 'Program Status', 'required');
			$this->form_validation->set_rules('program_category', 'Program Category', 'required');
			$this->form_validation->set_rules('brief_desc', 'Brief Description', 'required');
			$this->form_validation->set_rules('program_price', 'Program Price', 'required');

			//print_r($_FILES['program_images']['name']);die;

			if ($this->form_validation->run() == TRUE)
			{

				$name_pro = $this->input->post('program_name');
				$date = $this->input->post('program_date');

				$from_hour = $this->input->post('start_hours');
				$from_minutes = $this->input->post('start_minutes');
				$from_seconds = $this->input->post('start_seconds');
				$from_ams_pms = $this->input->post('start_ams_pms');

				
				if($from_ams_pms=='PM'){

				$fins_time = $from_hour+12;

				$combine_time_data_from = $fins_time.':'.$from_minutes.':'.$from_seconds;

				$usable_time =  date("H:i:s", strtotime($combine_time_data_from));
				 
				} else {

					$combine_time_data_from = $from_hour.':'.$from_minutes.':'.$from_seconds;

					$usable_time =  date("H:i:s", strtotime($combine_time_data_from));

				}

				$date_final_from = $date.' '.$usable_time;


				//$from_time = $this->input->post('program_from_time');
				$to_time = $this->input->post('program_end_date');

				$till_hour = $this->input->post('end_hours');
				$till_minutes = $this->input->post('end_minutes');
				$till_seconds = $this->input->post('end_seconds');
				$till_ams_pms = $this->input->post('end_ams_pms');


				if($till_ams_pms=='PM'){

				$fins_time_seconds = $till_hour+12;

				$combine_time_data_till = $fins_time_seconds.':'.$till_minutes.':'.$till_seconds;

				$usable_time_tills =  date("H:i:s", strtotime($combine_time_data_till));
				 

				} else {

					$combine_time_data_till = $till_hour.':'.$till_minutes.':'.$till_seconds;

					$usable_time_tills =  date("H:i:s", strtotime($combine_time_data_till));

				}

				$date_final_till = $to_time.' '.$usable_time_tills;






				$description = $this->input->post('progrm_description');
				$pro_status = $this->input->post('program_status');
				$pro_category = $this->input->post('program_category');
				$brief = $this->input->post('brief_desc');
				$speakers_name = $this->input->post('event_speakers');
				$location = $this->input->post('location');
				$price = $this->input->post('program_price');
				$insert_courses = $this->input->post('add_courses');

				$datar = $this->getLatLong($location);

				$lat = $datar['latitude'];
				$long = $datar['longitude'];

				if(empty($lat) && empty($long)){

					$ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
					$url = "http://freegeoip.net/json/$ip";
					$ch  = curl_init();

					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
					$data = curl_exec($ch);
					curl_close($ch);

					if ($data) {
					    $location_second = json_decode($data);

					    $lat = $location_second->latitude;
					    $long = $location_second->longitude;
					}
				}

				$image_speaker = $this->input->post('program_images');

				$config = array(
					'upload_path' => "./uploads/program_image",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('program_images'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/add_programs_category/add_programs');
					exit();
				}


				$check_urls = str_replace(' ', '-', $name_pro);

				$query_for_urls = $this->admin->get_sql_record_obj("SELECT * FROM `tbl_programs` WHERE deleted = '0' AND url LIKE '%$check_urls%' ORDER BY id DESC");


				if(!empty($query_for_urls)){


					$id_take = $query_for_urls[0]->id;

					$add_plus_one = $id_take+1;

					$url = $check_urls.'-'.$add_plus_one;

				} else {

					$url = str_replace(' ', '-', $name_pro);
				}


				


				$data_insert = array(
					'program_name'=>$name_pro,
					'category_id'=>$pro_category,
					'breif_desc'=>$brief,
					'program_image'=>$uimg,
					'url'=>$url,
					'description'=>$description,
					'location'=>$location,
					'program_date'=>$date_final_from,
					//'program_from_time'=>$from_time,
					'program_end_date'=>$date_final_till,
					'latitude'=>$lat,
					'longitude'=>$long,
					'price'=>$price,
					'status'=>$pro_status,
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('id')
					);
			


				$program_insert = $this->admin->insert_data('tbl_programs', $data_insert);
				if ($program_insert)
				{

					if(!empty($speakers_name)){
		        		$data_speaker=array();
				    	foreach($speakers_name as $spek_id) 
				   	 {
				       $data_speaker = array('speaker_id' => $spek_id,'program_id'=>$program_insert,'created_by'=>$this->session->userdata('id'),'created_at'=>date('Y-m-d H:i:s'),'status'=>'active');
				       $insert_exam = $this->admin->insert_data('tbl_program_speaker_rel',$data_speaker);
				  	  }
					}


					/****************Insert Courses***********/


					if(!empty($insert_courses)){
		        		
		        		$data_courses=array();

				    	foreach($insert_courses as $courseid) {

				       $data_courses = array('course_id' => $courseid,'program_id'=>$program_insert,'created_by'=>$this->session->userdata('id'),'created_at'=>date('Y-m-d H:i:s'),'status'=>'active');
				       	$this->admin->insert_data('tbl_program_course_rel',$data_courses);
				  	  }
					}


					$this->session->set_flashdata("success", "Program added successfully");

					redirect('admin/add_programs_category/program_list');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}


			}


		}

		$data['cats'] = $this->admin->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active'),'');

		$data['courses_data'] = $this->admin->get_data_orderby_where('tbl_semester','',array('sem_status'=>'active'),'');

		$data['speakers'] = $this->admin->get_data_orderby_where('tbl_speakers','',array('deleted'=>'0','status'=>'active'),'');

		admin_view('admin/programs/add_program',$data);
	}


	public function getLatLong($address){
	    if(!empty($address)){
	        //Formatted address
	        $formattedAddr = str_replace(' ','+',$address);
	        //Send request and receive json data by address
	        $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false'); 
	        $output = json_decode($geocodeFromAddr);
	        //Get latitude and longitute from json data
	        $data['latitude']  = $output->results[0]->geometry->location->lat; 
	        $data['longitude'] = $output->results[0]->geometry->location->lng;
	        //Return latitude and longitude of the given address
	        if(!empty($data)){
	            return $data;
	        }else{
	            return false;
	        }
	    }else{
	        return false;   
	    }
	}


	/***********Edit Programs***********/

	public function edit_programs(){

		$get_id = base64_decode($this->input->get('program_id'));


		if($_POST){

				$name_pro = $this->input->post('program_name');
				$date = $this->input->post('program_date');

				$from_hour = $this->input->post('start_hours');
				$from_minutes = $this->input->post('start_minutes');
				$from_seconds = $this->input->post('start_seconds');
				$from_ams_pms = $this->input->post('start_ams_pms');


				if($from_ams_pms=='PM'){

					$fins_time = $from_hour+12;

					$combine_time_data_from = $fins_time.':'.$from_minutes.':'.$from_seconds;

					$usable_time =  date("H:i:s", strtotime($combine_time_data_from));
				 
				} else {

					$combine_time_data_from = $from_hour.':'.$from_minutes.':'.$from_seconds;

					$usable_time =  date("H:i:s", strtotime($combine_time_data_from));

				}

				$date_final_from = $date.' '.$usable_time;

				//$from_time = $this->input->post('program_from_time');
				$to_time = $this->input->post('program_end_date');

				$till_hour = $this->input->post('end_hours');
				$till_minutes = $this->input->post('end_minutes');
				$till_seconds = $this->input->post('end_seconds');
				$till_ams_pms = $this->input->post('end_ams_pms');


				if($till_ams_pms=='PM'){

				$fins_time_seconds = $till_hour+12;

				$combine_time_data_till = $fins_time_seconds.':'.$till_minutes.':'.$till_seconds;

				$usable_time_tills =  date("H:i:s", strtotime($combine_time_data_till));
				 

				} else {

					$combine_time_data_till = $till_hour.':'.$till_minutes.':'.$till_seconds;

					$usable_time_tills =  date("H:i:s", strtotime($combine_time_data_till));

				}

				$date_final_till = $to_time.' '.$usable_time_tills;

				
				$pro_status = $this->input->post('program_status');
				$pro_category = $this->input->post('program_category');
				$speakers_name = $this->input->post('event_speakers');
				$location = $this->input->post('location');
				$city_id = $this->input->post('program_city');
				$price = $this->input->post('program_price');
				$using_id = base64_decode($this->input->post('updated_id'));

				$up_data = array(
					'program_name'=>$name_pro,
					'category_id'=>$pro_category,
					'city_ids'=>$city_id,
					'location'=>$location,
					'program_date'=>$date_final_from,
					'program_end_date'=>$date_final_till,
					'price'=>$price,
					'status'=>$pro_status,
					'updated_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
				);

				$ups_wheres = array('id'=>$using_id);

				$update_program = $this->admin->update_data('tbl_programs', $up_data,$ups_wheres);

				if(!empty($speakers_name)){


					$up_spk_where = array('program_id'=>$using_id);
					$up_spk_data = array('deleted'=>'1');

					$this->admin->update_data('tbl_program_speaker_rel', $up_spk_data,$up_spk_where);


					$data_read_papers=array();

					foreach($speakers_name as $data_speks) {

                   $data_read_papers = array('speaker_id' => $data_speks,'program_id'=>$using_id,'created_at'=>date('Y-m-d H:i:s'),'created_by'=>$this->session->userdata('id'),'updated_at'=>date('Y-m-d H:i:s'),'updated_by'=>$this->session->userdata('id'));

                   $insert_papers = $this->admin->insert_data('tbl_program_speaker_rel',$data_read_papers);
                   
                }

              
				}

				/*************Edit Courses Data**********/

				if(!empty($courses_name)){

				$up_courses_where = array('program_id'=>$using_id);
				$up_courses_data = array('deleted'=>'1');

				$this->admin->update_data('tbl_program_course_rel', $up_courses_data,$up_courses_where);

				$data_our_rel_courses=array();

					foreach($courses_name as $data_courses) {

	          	 	$data_our_rel_courses = array('course_id' => $data_courses,'program_id'=>$using_id,'created_at'=>date('Y-m-d H:i:s'),'created_by'=>$this->session->userdata('id'),'updated_at'=>date('Y-m-d H:i:s'),'updated_by'=>$this->session->userdata('id'));

	           		$this->admin->insert_data('tbl_program_course_rel',$data_our_rel_courses);
	           
	       			 }

				}

                $this->session->set_flashdata("update", "Record updated successfully");
                redirect('admin/add_programs_category/program_list');
		}

		$data['program_data'] = $this->admin->get_data_orderby_where('tbl_programs','',array('deleted'=>'0','status'=>'active','id'=>$get_id),'');

		$rel_speakers = $this->admin->get_data_orderby_where('tbl_program_speaker_rel','',array('deleted'=>'0','status'=>'active','program_id'=>$get_id),'');

		if(!empty($rel_speakers)){

	         $p_id = array();
	         foreach ($rel_speakers as $spksid)
	         {
	            $p_id[]= $spksid->speaker_id;
	         }
	         $data['p_ids'] = $p_id;  

	       } 

	    /***********Related Courses*********/


	    $rel_courses = $this->admin->get_data_orderby_where('tbl_program_course_rel','',array('deleted'=>'0','status'=>'active','program_id'=>$get_id),'');

		if(!empty($rel_courses)){

	         $c_id = array();
	         foreach ($rel_courses as $csid)
	         {
	            $c_id[]= $csid->course_id;
	         }
	         $data['c_ids'] = $c_id;  

	       } 


		$data['cats'] = $this->admin->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active'),'');

		$data['speakers'] = $this->admin->get_data_orderby_where('tbl_speakers','',array('deleted'=>'0','status'=>'active'),'');

		$data['courses_data'] = $this->admin->get_data_orderby_where('tbl_semester','',array('sem_status'=>'active'),'');

		$data['all_cities'] = $this->admin->get_data_orderby_where('cities','',array(),'');

		/*************Date Process*********/

		$date_finals = $data['program_data'][0]->program_date;

		$data['newdatetime_from'] = date('A', strtotime($date_finals));


		$pieces = explode(" ", $date_finals);

		$data['date_from'] = $pieces[0];

		$pieces_sub = explode(":", $pieces[1]);

		$hours_from = $pieces_sub[0];

		if($hours_from > 12){

			$main_from_time = $pieces_sub[0]-12;

			$checks =  strlen($main_from_time);

          if( $checks == 1){

          	$data['hours_from'] = '0'.$main_from_time;

          } else {

          	$data['hours_from'] = $main_from_time;

          }

		} else {

			$data['hours_from'] = $hours_from;
		}

		$data['minutes_from'] = $pieces_sub[1];
		$data['seconds_from'] = $pieces_sub[2];


		/*************End Date Process*********/

		$date_finals_end = $data['program_data'][0]->program_end_date;

		$data['newdatetime_till'] = date('A', strtotime($date_finals_end));


		$pieces_end = explode(" ", $date_finals_end);

		$data['date_till'] = $pieces_end[0];

		$pieces_sub_end = explode(":", $pieces_end[1]);

		$hours_till_end = $pieces_sub_end[0];

		if($hours_till_end > 12){

			$main_from_time_end = $pieces_sub_end[0]-12;

			$checks =  strlen($main_from_time_end);

          if( $checks == 1){

          	$data['hours_till_end'] = '0'.$main_from_time_end;

          } else {

          	$data['hours_till_end'] = $main_from_time_end;

          }

		} else {

			$data['hours_till_end'] = $hours_till_end;
		}

		$data['minutes_end'] = $pieces_sub_end[1];
		$data['seconds_end'] = $pieces_sub_end[2];

		// echo "<pre>";print_r($data['program_data']);die;



		admin_view('admin/programs/edit_program',$data);


	}

	public function edit_programs_old(){

		$get_id = base64_decode($this->input->get('program_id'));


		if($_POST){

				$name_pro = $this->input->post('program_name');
				$date = $this->input->post('program_date');

				$from_hour = $this->input->post('start_hours');
				$from_minutes = $this->input->post('start_minutes');
				$from_seconds = $this->input->post('start_seconds');
				$from_ams_pms = $this->input->post('start_ams_pms');


				if($from_ams_pms=='PM'){

				$fins_time = $from_hour+12;

				$combine_time_data_from = $fins_time.':'.$from_minutes.':'.$from_seconds;

				$usable_time =  date("H:i:s", strtotime($combine_time_data_from));
				 
				} else {

					$combine_time_data_from = $from_hour.':'.$from_minutes.':'.$from_seconds;

					$usable_time =  date("H:i:s", strtotime($combine_time_data_from));

				}

				$date_final_from = $date.' '.$usable_time;

				//$from_time = $this->input->post('program_from_time');
				$to_time = $this->input->post('program_end_date');

				$till_hour = $this->input->post('end_hours');
				$till_minutes = $this->input->post('end_minutes');
				$till_seconds = $this->input->post('end_seconds');
				$till_ams_pms = $this->input->post('end_ams_pms');


				if($till_ams_pms=='PM'){

				$fins_time_seconds = $till_hour+12;

				$combine_time_data_till = $fins_time_seconds.':'.$till_minutes.':'.$till_seconds;

				$usable_time_tills =  date("H:i:s", strtotime($combine_time_data_till));
				 

				} else {

					$combine_time_data_till = $till_hour.':'.$till_minutes.':'.$till_seconds;

					$usable_time_tills =  date("H:i:s", strtotime($combine_time_data_till));

				}

				$date_final_till = $to_time.' '.$usable_time_tills;

				
				$description = $this->input->post('progrm_description');
				$pro_status = $this->input->post('program_status');
				$pro_category = $this->input->post('program_category');
				$brief = $this->input->post('brief_desc');
				$speakers_name = $this->input->post('event_speakers');
				$location = $this->input->post('location');
				$price = $this->input->post('program_price');
				$image_speaker = $this->input->post('program_images');
				$using_id = base64_decode($this->input->post('updated_id'));
				$courses_name = $this->input->post('add_courses');


				$datar = $this->getLatLong($location);

				$lat = $datar['latitude'];
				$long = $datar['longitude'];

				

				if(empty($lat) && empty($long)){

					$ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
					$url = "http://freegeoip.net/json/$ip";
					$ch  = curl_init();

					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
					$data = curl_exec($ch);
					curl_close($ch);

					if ($data) {
					    $location_second = json_decode($data);

					    $lat = $location_second->latitude;
					    $long = $location_second->longitude;

					}

				}


				$config = array(
					'upload_path' => "./uploads/program_image",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('program_images'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
			
				if($uimg==''){

					$uimg = $this->input->post('if_no_image');
				}


				$up_data = array(
					'program_name'=>$name_pro,
					'category_id'=>$pro_category,
					'breif_desc'=>$brief,
					'program_image'=>$uimg,
					'description'=>$description,
					'location'=>$location,
					'latitude'=>$lat,
					'longitude'=>$long,
					'program_date'=>$date_final_from,
					//'program_from_time'=>$from_time,
					'program_end_date'=>$date_final_till,
					'price'=>$price,
					'status'=>$pro_status,
					'updated_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
					);


				$ups_wheres = array('id'=>$using_id);

				$update_program = $this->admin->update_data('tbl_programs', $up_data,$ups_wheres);

				if(!empty($speakers_name)){


					$up_spk_where = array('program_id'=>$using_id);
					$up_spk_data = array('deleted'=>'1');

					$this->admin->update_data('tbl_program_speaker_rel', $up_spk_data,$up_spk_where);


					$data_read_papers=array();

					foreach($speakers_name as $data_speks) {

                   $data_read_papers = array('speaker_id' => $data_speks,'program_id'=>$using_id,'created_at'=>date('Y-m-d H:i:s'),'created_by'=>$this->session->userdata('id'),'updated_at'=>date('Y-m-d H:i:s'),'updated_by'=>$this->session->userdata('id'));

                   $insert_papers = $this->admin->insert_data('tbl_program_speaker_rel',$data_read_papers);
                   
                }

              
				}

				/*************Edit Courses Data**********/

				if(!empty($courses_name)){

				$up_courses_where = array('program_id'=>$using_id);
				$up_courses_data = array('deleted'=>'1');

				$this->admin->update_data('tbl_program_course_rel', $up_courses_data,$up_courses_where);

				$data_our_rel_courses=array();

					foreach($courses_name as $data_courses) {

	          	 	$data_our_rel_courses = array('course_id' => $data_courses,'program_id'=>$using_id,'created_at'=>date('Y-m-d H:i:s'),'created_by'=>$this->session->userdata('id'),'updated_at'=>date('Y-m-d H:i:s'),'updated_by'=>$this->session->userdata('id'));

	           		$this->admin->insert_data('tbl_program_course_rel',$data_our_rel_courses);
	           
	       			 }

				}

                $this->session->set_flashdata("update", "Record updated successfully");
                redirect('admin/add_programs_category/program_list');
		}

		$data['program_data'] = $this->admin->get_data_orderby_where('tbl_programs','',array('deleted'=>'0','status'=>'active','id'=>$get_id),'');

		$rel_speakers = $this->admin->get_data_orderby_where('tbl_program_speaker_rel','',array('deleted'=>'0','status'=>'active','program_id'=>$get_id),'');

		if(!empty($rel_speakers)){

	         $p_id = array();
	         foreach ($rel_speakers as $spksid)
	         {
	            $p_id[]= $spksid->speaker_id;
	         }
	         $data['p_ids'] = $p_id;  

	       } 

	    /***********Related Courses*********/


	    $rel_courses = $this->admin->get_data_orderby_where('tbl_program_course_rel','',array('deleted'=>'0','status'=>'active','program_id'=>$get_id),'');

		if(!empty($rel_courses)){

	         $c_id = array();
	         foreach ($rel_courses as $csid)
	         {
	            $c_id[]= $csid->course_id;
	         }
	         $data['c_ids'] = $c_id;  

	       } 


		$data['cats'] = $this->admin->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active'),'');

		$data['speakers'] = $this->admin->get_data_orderby_where('tbl_speakers','',array('deleted'=>'0','status'=>'active'),'');

		$data['courses_data'] = $this->admin->get_data_orderby_where('tbl_semester','',array('sem_status'=>'active'),'');

		/*************Date Process*********/

		$date_finals = $data['program_data'][0]->program_date;

		$data['newdatetime_from'] = date('A', strtotime($date_finals));


		$pieces = explode(" ", $date_finals);

		$data['date_from'] = $pieces[0];

		$pieces_sub = explode(":", $pieces[1]);

		$hours_from = $pieces_sub[0];

		if($hours_from > 12){

			$main_from_time = $pieces_sub[0]-12;

			$checks =  strlen($main_from_time);

          if( $checks == 1){

          	$data['hours_from'] = '0'.$main_from_time;

          } else {

          	$data['hours_from'] = $main_from_time;

          }

		} else {

			$data['hours_from'] = $hours_from;
		}

		$data['minutes_from'] = $pieces_sub[1];
		$data['seconds_from'] = $pieces_sub[2];


		/*************End Date Process*********/

		$date_finals_end = $data['program_data'][0]->program_end_date;

		$data['newdatetime_till'] = date('A', strtotime($date_finals_end));


		$pieces_end = explode(" ", $date_finals_end);

		$data['date_till'] = $pieces_end[0];

		$pieces_sub_end = explode(":", $pieces_end[1]);

		$hours_till_end = $pieces_sub_end[0];

		if($hours_till_end > 12){

			$main_from_time_end = $pieces_sub_end[0]-12;

			$checks =  strlen($main_from_time_end);

          if( $checks == 1){

          	$data['hours_till_end'] = '0'.$main_from_time_end;

          } else {

          	$data['hours_till_end'] = $main_from_time_end;

          }

		} else {

			$data['hours_till_end'] = $hours_till_end;
		}

		$data['minutes_end'] = $pieces_sub_end[1];
		$data['seconds_end'] = $pieces_sub_end[2];



		admin_view('admin/programs/edit_program',$data);


	}

	/**********Edit Coupons****************/

	public function edit_coupons_data(){


		$cid = base64_decode($this->input->get('coupon_id'));


		if($_POST){

			$coupon_name =  $this->input->post('coupons_names');
			$coupon_type =  $this->input->post('coupon_types');

			
			$coupon_flat_amount =  $this->input->post('flat_ammount');
			$coupon_percentage =  $this->input->post('coupos_percentage');
			$coupon_from =  $this->input->post('coupon_from');
			$coupon_to =  $this->input->post('coupon_to');
			$coupon_description =  $this->input->post('brief_desc');
			$status =  $this->input->post('coupon_status');
			$programs =  $this->input->post('program_names');
			$updated_id =  base64_decode($this->input->post('updated_id'));


			if($coupon_type=='flat'){

				$coupon_percentage = '';

			} else if($coupon_type=='percent'){

				$coupon_flat_amount = '';
			}


			$up_coupon_data =  array(
				'coupon_name' => $coupon_name, 
				'coupon_type' => $coupon_type, 
				'coupon_value' =>$coupon_flat_amount, 
				'coupon_percentage' => $coupon_percentage, 
				'valid_from' => $coupon_from, 
				'valid_till' => $coupon_to, 
				'description' => $coupon_description, 
				'update_at' => date('Y-m-d H:i:s'), 
				'updated_by' => $this->session->userdata('id'), 
				'status' => $status
				);

			$up_coupon_wheres = array ('coupon_id'=>$updated_id);

			$insert_data =  $this->admin->update_data('tbl_program_coupons',$up_coupon_data,$up_coupon_wheres);

			if(!empty($insert_data)){

				$up_spk_where = array('coupon_id'=>$updated_id);
				$up_spk_data = array('deleted'=>'1');

				$this->admin->update_data('tbl_program_coupon_rel', $up_spk_data,$up_spk_where);


				$data_coupons_useds=array();

				foreach($programs as $data_speks) {

               $data_coupons_useds = array('coupon_id' => $updated_id,'program_id'=>$data_speks,'created_at'=>date('Y-m-d H:i:s'),'created_by'=>$this->session->userdata('id'),'updated_at'=>date('Y-m-d H:i:s'),'updated_by'=>$this->session->userdata('id'));

               $insert_papers = $this->admin->insert_data('tbl_program_coupon_rel',$data_coupons_useds);
                   
                }

                $this->session->set_flashdata("update", "Record updated successfully");
                redirect('admin/add_programs_category/program_coupons_list');
              
				}

			}


		$rel_coupons = $this->admin->get_data_orderby_where('tbl_program_coupon_rel','',array('deleted'=>'0','status'=>'active','coupon_id'=>$cid),'');


		if(!empty($rel_coupons)){

	         $p_id = array();
	         foreach ($rel_coupons as $couporsid)
	         {
	            $p_id[]= $couporsid->program_id;
	         }
	         $data['p_ids'] = $p_id;  

	       }

		$data['all_programs'] = $this->admin->get_data_orderby_where('tbl_programs','',array('deleted'=>'0','status'=>'active'),'');

		$data['cdata'] = $this->admin->get_data_orderby_where('tbl_program_coupons','',array('deleted'=>'0','status'=>'active','coupon_id'=>$cid),'');

		admin_view('admin/programs/edit_program_coupon',$data);



	}


	/**************All Program List**********/


	public function program_list(){

		// $data['all_data'] = $this->admin->get_data_orderby_where('tbl_programs','',array('deleted'=>'0'),'');
		$where = array('tbl_programs.deleted'=>'0');
		$data['all_data'] = $this->admin->get_three_tbl_data_program_details("", $where, "tbl_programs.id", "DESC", "","");


		admin_view('admin/programs/program_list',$data);
	}

	/*************Check Category Title**********/


	public function check_category_title(){


		$name_cat = $this->input->post('name');
		$number = $this->input->post('type_calls');

		if($number==1){

		$data_query = $this->admin->get_sql_record_obj("SELECT * FROM `tbl_programs_category` WHERE deleted='0' AND status='active' AND name LIKE '%$name_cat%'");

				if($data_query){
				echo 1;
			}


		} else if($number==2) {

		$data_query = $this->admin->get_sql_record_obj("SELECT * FROM `tbl_programs` WHERE deleted='0' AND status='active' AND program_name LIKE '%$name_cat%'");

				if($data_query){
				echo 1;
			}


		} else if($number==3) {

		$data_query = $this->admin->get_sql_record_obj("SELECT * FROM `tbl_speakers` WHERE deleted='0' AND status='active' AND speaker_name LIKE '%$name_cat%'");

				if($data_query){
				echo 1;
			}


		} 

	}


	/*************Add Programs Coupons*********/


	public function program_coupons_list(){

		$data['coupon_list'] = $this->admin->get_data_orderby_where('tbl_program_coupons','',array('deleted'=>'0','status'=>'active'),'');

		


		admin_view('admin/programs/program_coupons_list',$data);

	}


	public function add_coupons(){

		if($_POST){


			$coupon_name =  $this->input->post('coupons_names');
			$coupon_type =  $this->input->post('coupon_types');
			$coupon_flat_amount =  $this->input->post('flat_ammount');
			$coupon_percentage =  $this->input->post('coupos_percentage');
			$coupon_from =  $this->input->post('coupon_from');
			$coupon_to =  $this->input->post('coupon_to');
			$coupon_description =  $this->input->post('brief_desc');
			$status =  $this->input->post('coupon_status');
			$programs =  $this->input->post('program_names');


			$insert_array =  array(
				'coupon_name' => $coupon_name, 
				'coupon_type' => $coupon_type, 
				'coupon_value' =>$coupon_flat_amount, 
				'coupon_percentage' => $coupon_percentage, 
				'valid_from' => $coupon_from, 
				'valid_till' => $coupon_to, 
				'description' => $coupon_description, 
				'created_at' => date('Y-m-d H:i:s'), 
				'created_by' => $this->session->userdata('id'), 
				'status' => $status
				);

			$insert_data =  $this->admin->insert_data('tbl_program_coupons',$insert_array);

			if($insert_data){

			if(!empty($programs)){
		        		$data_programs=array();
				    	foreach($programs as $pro_id) 
				   	 {
				       $data_programs = array(

				       	'coupon_id'  => $insert_data,
				       	'program_id' =>$pro_id,
				       	'created_by' =>$this->session->userdata('id'),
				       	'created_at' =>date('Y-m-d H:i:s'),
				       	'status'     =>'active'

				       	);
				       $insert_exam = $this->admin->insert_data('tbl_program_coupon_rel',$data_programs);
				  	  }
					}


					$this->session->set_flashdata("success", "Coupon added successfully");

					redirect('admin/add_programs_category/program_coupons_list');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}
			}



		$data['all_programs'] = $this->admin->get_data_orderby_where('tbl_programs','',array('deleted'=>'0','status'=>'active'),'');


		admin_view('admin/programs/add_program_coupon',$data);
	}


	/***********Delete Category*******/


	public function delete_data()
   	{

   		$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('user_id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('id' => $id);
    	$delet_master = $this->admin->update_data('tbl_programs_category',$master_delete_arr,$where);

    	
    	if($delet_master){

    		echo '1';
    	}

    }

    public function delete_data_programs()
   	{

   		$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('user_id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('id' => $id);
    	$delet_master = $this->admin->update_data('tbl_programs',$master_delete_arr,$where);

    	
    	if($delet_master){

    		echo '1';
    	}

    }

    public function delete_data_coupons(){

   		$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('user_id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('coupon_id' => $id);
    	$delet_master = $this->admin->update_data('tbl_program_coupons',$master_delete_arr,$where);

    	
    	if($delet_master){

    		echo '1';
    	}

    }

    public function edit_category(){

    	$cat_id = base64_decode($this->input->get('cat_id'));

    	if($_POST){

    		$this->form_validation->set_rules('category_name', 'Category Name', 'required');
			$this->form_validation->set_rules('active_status', 'Product Price', 'required');

			if ($this->form_validation->run() == TRUE)
			{

				$name = $this->input->post('category_name');
				$status = $this->input->post('active_status');


				$update_data = array('name'=>$name,
					'status'=>$status,
					'update_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
					);

				$update_where = array('id'=> $cat_id);

				$cat_insert = $this->admin->update_data('tbl_programs_category',$update_data,$update_where);

				if ($cat_insert)
				{
					$this->session->set_flashdata("update", "Record has been updated");

					redirect('admin/add_programs_category/category_list');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}


    	}
    }
    	
    	$data['all_data'] = $this->admin->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active','id'=>$cat_id),'');



		admin_view('admin/programs/edit_program_category', $data);

    }


    public function all_subscriber_list(){

    	$program_datar = $this->admin->get_data_orderby_where('tbl_program_order_place','id',array('deleted'=>'0'),'DESC');

    	$count_records = COUNT($program_datar);
       

        /****************Pagination Start**************/

        if($this->input->get('per_page'))
       {
          $page = $this->input->get('per_page');
       }
       else
       {
          $page=0;
       }
       $count_page=total_per_page;

       $data["links"]=$this->all_page_pagination(base_url()."admin/add_programs_category/all_subscriber_list/?",$count_records,$this->input->get('per_page'),$count_page);

       $data['all_orders'] = $this->admin->get_sql_record_obj("SELECT * FROM tbl_program_order_place WHERE `deleted`='0' ORDER BY `id` DESC LIMIT $page, $count_page");

    	admin_view('admin/programs/subscriber_list', $data);
    }

    public function delete_orders_placed(){

    	$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('user_id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('id' => $id);
    	$delet_master = $this->admin->update_data('tbl_program_order_place',$master_delete_arr,$where);

    	
    	if($delet_master){

    		echo '1';
    	}

    }

    /************Special Registration Record********/


    public function special_registraion_data(){


    	$program_datar = $this->admin->get_data_orderby_where('tbl_special_registration_program','id',array('deleted'=>'0'),'DESC');

    	$count_records = COUNT($program_datar);
       

        /****************Pagination Start**************/

        if($this->input->get('per_page'))
       {
          $page = $this->input->get('per_page');
       }
       else
       {
          $page=0;
       }
       $count_page=total_per_page;

       $data["links"]=$this->all_page_pagination(base_url()."admin/add_programs_category/special_registraion_data/?",$count_records,$this->input->get('per_page'),$count_page);

       $data['special_data'] = $this->admin->get_sql_record_obj("SELECT * FROM tbl_special_registration_program WHERE `deleted`='0' ORDER BY `id` DESC LIMIT $page, $count_page");

    	admin_view('admin/programs/special_registration_list', $data);

    }


    public function activate_program(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'active');
    	$update_where = array('id'=>$program_id);

    	$run = $this->admin->update_data('tbl_programs', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Program activated successfully");

			redirect('admin/add_programs_category/program_list');
		}

    }


    public function deactivate_program(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'inactive');
    	$update_where = array('id'=>$program_id);

    	$run = $this->admin->update_data('tbl_programs', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Program deactivated successfully");

			redirect('admin/add_programs_category/program_list');
		}

    }

    // *********************************************************************************///

    public function brochure_content()
    {
    	$data['all_data'] = $this->admin->get_data_twotable_column_where('brochure_content','tbl_programs_category','brochure_category_id','id',$column='',array(),$ordercol='brochure_id',$orderby='',$groupby='');

    	admin_view('admin/programs/brochure_content',$data);
    }

    public function add_brochure()
    {

    	$data['program_category'] = $this->admin->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active'),'');

    	admin_view('admin/programs/add_brochure',$data);
    }

    public function new_brochure()
    {
    	$postData = $this->input->post();


    	if(!empty($postData))
    	{
    		$brochure_name = $postData['brochure_name'];
			$above_category = $postData['above_category'];
			
			$config = array(
				'upload_path' => "./uploads/program_brochure",
				'allowed_types' => "*",
				'overwrite' => false,
				'file_name' => time()
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('cat_image'))
			{
				$uploadimg = $this->upload->data();
				$uimg = $uploadimg['file_name'];
			}
			else
			{
				$uimg = '';
			}

			$add_cat = array(
				'brochure_name' => $brochure_name,
				'brochure_category_id' => $above_category,
				'brochure_image' => $uimg,
				'created_at' => date('Y-m-d'),
				'created_by' => $this->session->userdata('id')
			);
			$category_id = $this->admin->insert_data('brochure_content', $add_cat);
			
				redirect('admin/add_programs_category/brochure_content');
    	}
    }


    //*******************************************************************************//

} 

?>