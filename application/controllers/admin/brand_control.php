<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('brand');
        /**********view pages***********/
		$xcrud->order_by('brand_id','desc');
		//$xcrud->unset_remove(); 
		$xcrud->relation('created_by','employees','id','name');
		$xcrud->relation('updated_by','employees','id','name');
        $data['content'] = $xcrud->render();
		$data['title'] ='Brand List';
		admin_view('admin/brand_list', $data);
    }
	
}