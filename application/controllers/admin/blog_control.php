<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
    
	 public function view($msg = NULL)
    {
         $xcrud = Xcrud::get_instance()->table('comments');
         $xcrud->order_by('id','desc');

         $data['content'] = $xcrud->render();
         $data['title'] ='Blog List';
         admin_view('admin/allotmenu_list', $data);
    }
	 
	public function blog_list($msg = NULL)
    {
    	Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/xcrud/plugins/ckeditor/ckeditor.js';
    	
        $xcrud = Xcrud::get_instance()->table('posts');
        $xcrud->pass_var('created_at',time());
       
    	$xcrud->relation('blog_cat','blog_categories','id','category');
       // $xcrud->relation('tag_id','tags_tbl','id','tags');
      	//$xcrud->fields('blog_url','title,content,blog_cat,tag_id,img_url,status,created_at');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		$xcrud->change_type('img_url', 'image', '', array(
        'width' => 450,
        'path' => '../uploads/blogs',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
		$data['content'] = $xcrud->render();
		$data['title'] ='Blog List';
		admin_view('admin/allotmenu_list', $data);
    }

    public function blog_category()
    {
        $xcrud = Xcrud::get_instance()->table('blog_categories');
        $xcrud->order_by('id','desc');
        $data['content'] = $xcrud->render();
        $data['title'] ='Blog Categories List';
        admin_view('admin/allotmenu_list', $data);
    }
      public function tags($msg = NULL){
        $xcrud = Xcrud::get_instance()->table('tags_tbl');
        /**********view pages***********/
        $xcrud->order_by('id','desc');
        $xcrud->table_name('');
        $data['content'] = $xcrud->render();
        $data['title'] ='Portfolio Tags';
        admin_view('admin/allotmenu_list', $data);
    }
	
}