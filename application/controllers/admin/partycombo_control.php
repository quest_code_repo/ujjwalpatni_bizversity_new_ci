<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Partycombo_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('partycombo');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		$xcrud->unset_remove(); 
		$xcrud->relation('created_by','employees','id','name');
		$data['content']=$xcrud->render();
		$data['title'] ='Party Combo List';
		admin_view('admin/offer_list', $data);
    }
	
}