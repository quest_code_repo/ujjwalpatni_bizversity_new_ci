<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('tbl_students');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		//$xcrud->relation('state','states','code','name');
		$xcrud->unset_add();
		// $xcrud->readonly('password'); 
		$xcrud->fields('image_name,password',false); 
		$xcrud->columns('username,full_name,mobile,created_at');
		$xcrud->label('username','Username/Email');
		$xcrud->label('user_point','Points');
		//$xcrud->subselect('Address','SELECT shipping_address FROM orders WHERE customer_id = {id} ORDER BY id DESC LIMIT 1'); 
		//$xcrud->subselect('Total Order','SELECT COUNT(id) FROM orders WHERE customer_id = {id}');
		//$xcrud->subselect('Total Spent','SELECT SUM(order_total) FROM orders WHERE customer_id = {id}');
		$xcrud->fields('token_id',true);
        $data['content'] = $xcrud->render();
		$data['title'] ='User List';
		admin_view('admin/allotmenu_list', $data);
    }
	
}