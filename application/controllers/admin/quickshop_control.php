<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quickshop_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL)
	{
		$xcrud = Xcrud::get_instance()->table('quickshop');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		//$xcrud->unset_remove(); 
		//$xcrud->unset_edit(); 
		$xcrud->relation('created_by','employees','id','name');
		
		$quickshopdetails = $xcrud->nested_table('quickshop','id','quickshop_product','quickshop_id'); // 2nd level
		
		$quickshopdetails->subselect('product','SELECT pname FROM products WHERE products.product_id = {product_id}','product_id');
		
		$quickshopdetails->columns('product,product_quantity');		
		//$quickshopdetails->unset_edit();
		//$quickshopdetails->unset_remove();
		$quickshopdetails->unset_add(); 
		$quickshopdetails->unset_csv();
		$quickshopdetails->unset_numbers();
		$quickshopdetails->unset_pagination();
		$quickshopdetails->unset_limitlist(); 
		$quickshopdetails->unset_search(); 
		$quickshopdetails->unset_view();
		
		$data['content']=$xcrud->render();
		$data['title'] ='Quick Shop List';
		admin_view('admin/offer_list', $data);
    }
	
	
	 
	public function add($msg = NULL)
	{
		$data['title'] ='Add Quick Shop List';
		$allproduct = array('active_inactive' => 'active'); 
		$data['products']=$this->admin_common_model->fetch_condrecord('products',$allproduct);
		admin_view('admin/add_quickshop', $data);
    }
	public function products_quantity($pid){
		$data['msg']='';
		$quantity=$this->admin_common_model->get_products_quantity($pid,'');
		return $quantity;
	}
	
	
	public function load_nop($nop)
	{
		?>
		<table class="stdtable" cellspacing="0" cellpadding="0" border="0">
		  <tr style="border-top:1px solid #999999;">
			<td>Product name </td>
			<td>weight</td>
			<td>Quantity</td>
		  </tr>
		  <?php for($i=0;$i<$nop;$i++) { ?>
		  <tr>
			<td><?php $chk = array('active_inactive' => 'active');
		$products=$this->admin_common_model->fetch_condrecord('products',$chk);
			?>
           <select id="product<?php echo $i; ?>" name="product<?php echo $i; ?>" class="form-control my-select" onchange="load_weight(this.value,<?php echo $i; ?>);">
              <option value="">Select Product</option>
          <?php
		   if(!empty($products)) {
		    foreach($products as $pro_info) {
		   ?>
            <option value="<?php echo $pro_info['product_id']; ?>"><?php echo $pro_info['pname']; ?></option>
           <?php }} ?>
            </select>
			</td>
			<td><span id="weg<?php echo $i; ?>" </td>
			<td><input type="text" name="qua<?php echo $i; ?>" required /></td>
		  </tr>
		  <?php } ?>
		</table>
		<?php
    }
	
	public function load_weight($cid,$i){
		$weight=$this->admin_common_model->fetch_condrecordweight($cid);
		if(!empty($weight))
		{?>
						
		
		   <select name="weight<?php echo $i; ?>" id="weight<?php echo $i; ?>">
			   <option value="">--Select Weight--</option>
				<?php foreach($weight as $w)
				{ ?>
				<option value="<?php echo $w['qua_id']; ?>"><?php echo $w['weight']; ?>&nbsp;<?php echo $w['name']; ?></option>
				<?php } ?>
			</select>
		
										
		
          <?php
		  
		 }
		
    }
	
	
	
	 
	public function edit($msg = NULL)
	{
		$data['title'] ='Edit Quick Shop List';
		$allproduct = array('active_inactive' => 'active'); 
		$data['products']=$this->admin_common_model->fetch_condrecord('products',$allproduct);
		admin_view('admin/edit_quickshop', $data);
    }
	
	public function add_quick_shop_list($msg = NULL)
	{	
		/*print_r($this->input->post());echo "<br>";
		print_r($this->input->post('title'));echo "<br>";
		print_r($this->input->post('price'));echo "<br>";
		print_r($this->input->post('nop'));echo "<br>";*/
		if($this->input->post('offer') == '1')
		{
			$quickshop_pro = array(
				'title' => $this->input->post('title'),
				'price' => $this->input->post('price'),
				'created_by' => $this->session->userdata('id'),
				'created_at' => date('Y-m-d'));
			$last_insert_id = $this->admin_common_model->insert_data('quickshop',$quickshop_pro);
			if($last_insert_id)
			{
				$no_of_prd = $this->input->post('nop');
				for($i=0;$i<$no_of_prd;$i++) 
				{
					$prd = "product".$i;
					$wt = "weight".$i;
					$qt = "qua".$i;
					$productId = $this->input->post($prd);
					$productWeight = $this->input->post($wt);
					$productQty = $this->input->post($qt);
					
					$quickprd_pro = array(
					'quickshop_id' => $last_insert_id,
					'product_id' => $productId,
					'product_qua_id' => $productWeight,
					'product_quantity' => $productQty);
					$this->admin_common_model->insert_data('quickshop_product',$quickprd_pro);
				}
			}
		}
		else
		{
			$party_pro = array(
				'title' => $this->input->post('title'),
				'price' => $this->input->post('price'),
				'created_by' => $this->session->userdata('id'),
				'created_at' => date('Y-m-d'));
			$last_insert_id = $this->admin_common_model->insert_data('party_combo',$party_pro);
			if($last_insert_id)
			{
				$no_of_prd = $this->input->post('nop');
				for($i=0;$i<$no_of_prd;$i++) 
				{
					$prd = "product".$i;
					$wt = "weight".$i;
					$qt = "qua".$i;
					$productId = $this->input->post($prd);
					$productWeight = $this->input->post($wt);
					$productQty = $this->input->post($qt);
					
					$partyprd_pro = array(
					'party_combo_id' => $last_insert_id,
					'product_id' => $productId,
					'product_qua_id' => $productWeight,
					'product_quantity' => $productQty);
					$this->admin_common_model->insert_data('party_combo_product',$partyprd_pro);
				}
			}
		}
		//die();
		if($this->input->post('offer') == '1')
		{
			redirect('admin/quickshop_control/view');
		}
		else{
			redirect('admin/quickshop_control/party_view');
		}
    }
	
	
	
	public function party_view($msg = NULL)
	{
		$xcrud = Xcrud::get_instance()->table('party_combo');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		$xcrud->unset_remove(); 
		//$xcrud->unset_edit(); 
		$xcrud->relation('created_by','employees','id','name');
		
		$quickshopdetails = $xcrud->nested_table('party_combo','id','party_combo_product','party_combo_id'); // 2nd level
		
		$quickshopdetails->subselect('product','SELECT pname FROM products WHERE products.product_id = {product_id}','product_id');
		
		$quickshopdetails->columns('product,product_quantity');		
		$quickshopdetails->unset_edit();
		//$quickshopdetails->unset_remove();
		$quickshopdetails->unset_add(); 
		$quickshopdetails->unset_csv();
		$quickshopdetails->unset_numbers();
		$quickshopdetails->unset_pagination();
		$quickshopdetails->unset_limitlist(); 
		$quickshopdetails->unset_search(); 
		$quickshopdetails->unset_view();
		
		$data['content']=$xcrud->render();
		$data['title'] ='Party Combo List';
		admin_view('admin/offer_list', $data);
    }
	
	
}