<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('newsletter_subscribers');
        /**********view pages***********/
        $xcrud->unset_add();
        $xcrud->readonly('email');
		$xcrud->order_by('id','desc');
        $data['content'] = $xcrud->render();
		$data['title'] ='Newsletter List';
		admin_view('admin/brand_list', $data);
    }
	
}