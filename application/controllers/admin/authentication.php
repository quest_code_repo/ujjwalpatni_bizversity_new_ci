<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/authentication_model');
		$this->load->library(array('Form_validation','session')); 
	}
	 
	public function index($msg = NULL){
        $data['msg'] = $msg;
        $this->load->view('admin/index', $data);
    } 
    public function adminlogin() {

    	$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$where = array('emp_code' => $username,'password' => $password );
        $result = $this->authentication_model->getSingle('employees',$where);
        
        if($result){

        	$user_details = array(
				'name' => $result[0]['name'],
				'emp_code' => $result[0]['emp_code'],
				'personal_email' => $result[0]['personal_email'],
				'designation' => $result[0]['designation'],			
				'id' => $result[0]['id']		
				);
			$this->session->set_userdata($user_details); 

            // print_r($user_details) ;
            // die;         
            redirect('admin/dashboard');
        }
		else{
			$msg = array(
				'msg' =>'<strong>Error!</strong> Invalid Username and Password. Log in failed.',
				'res' => 0
			);
			$this->session->set_userdata($msg);
			redirect('admin/authentication');
        }        
    }
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin/authentication'); 
	}
	
	
	public function forgot($msg = NULL){
        $data['msg'] = $msg;
        $this->load->view('admin/forgotpassword', $data);
    } 
	
	public function change_password()
	{
		if ($_POST) 
		{
            $this->form_validation->set_rules('old_password', 'old password', 'required|xss_clean');
            $this->form_validation->set_rules('new_password', 'new password', 'required|xss_clean|min_length[8]');
            $this->form_validation->set_rules('new_confirm_password', 'confirm password', 'required|xss_clean|matches[new_password]');
			
			$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		
            if ($this->form_validation->run() == TRUE) {

                $where = array(
                    'id' => $this->session->userdata('id'),
                    'password' => md5($this->input->post('old_password'))
                );
                $data = $this->authentication_model->fetch_recordbyid('employees', $where);
                if (!empty($data)) {

                    $arr = array
                        (
                        'password' =>  md5($this->input->post('new_password'))
                    );
                    $where = array(
                        'id' => $this->session->userdata('id'),
                        'password' =>  md5($this->input->post('old_password'))
                    );
                    $login = $this->authentication_model->update_data('employees', $arr, $where);
                    if ($login == 1) {
                        $this->session->set_flashdata('message', 'Your password is successfully changed');
                    } else {
                       
                    }
                } else {
                    $this->session->set_flashdata('error', 'Please fill in correct old password');
                }

                redirect('admin/authentication/change_password');
            } else {
               
            }
        }
        admin_view('admin/change_password');
	}
	
	public function forgot_sub($msg = NULL){
        $data['msg'] = $msg;
		$where = array('emp_code' => $this->input->post('username'),'official_email' => $this->input->post('email'));
		$check = $this->authentication_model->fetch_recordbyid('employees', $where);
		if (!empty($check)) {
			$length = 8;
			$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
			$ids=$check->id;
			$cid = array('id' => $ids);
			$newpass = array('password' => md5($randomString));
			$rs=$this->authentication_model->update_data('employees',$newpass,$cid);      
			$msg = array('msg' =>'<strong>Success!</strong> Your New password has been sent to your inbox or spam','res' => 1);
			$this->session->set_userdata($msg);
			$this->email->clear();
			$this->email->to($this->input->post('email'));
			$this->email->from('info@studentbazaar.com');
			$this->email->subject('Your New Account Details <br>');
			$this->email->message('User Name :'
			.$this->input->post('username').' <br> Your Password :'.$randomString);
			$this->email->send();
		}else {
			$msg = array('msg' =>'<strong>Error!</strong> Invalid Username or Email','res' => 0);
			$this->session->set_userdata($msg);
		}
        $this->load->view('admin/forgotpassword', $data);
    } 
	
	
	
//Change profile 
function  my_profile()
{	
	$user_id = $this->session->userdata('id');
	$where = array('id' => $user_id );
	$data['row'] = $this->authentication_model->getSingle('employees',$where); 
	admin_view('admin/change_profile_v',$data);
}


function change_profile_c($user_id)
{
	$user_id=$user_id;
	$data['row'] = $this->authentication_model->get_profile($user_id);	
	
	admin_view('admin/change_profile_v',$data);
}

function change_profile()
{
	$user_id = $this->session->userdata('id');
	// Check for validation
	// $this->form_validation->set_rules('personal_contact_no', 'personal_contact_no', 'trim|required|xss_clean|regex_match[/^[0-9]{10}$/]');
	$this->form_validation->set_rules('personal_contact_no','Personal Contact No','trim|required');
	$this->form_validation->set_rules('city', 'City', 'trim|required');
	
	if($_POST)
    {
        if($this->form_validation->run() == TRUE)
        {
        	$name = $this->input->post('name');
            $personal_contact_no = $this->input->post('personal_contact_no');
            $city = $this->input->post('city');
            
            if(!empty($_FILES['userfile']['name']))
            {
               $config = array(
                'upload_path' => "./uploads/admin_picture",
                'allowed_types' => "*",
                'overwrite' => false,
                'file_name' => time()
                );

                $this->load->library('Upload', $config);
                $this->upload->initialize($config);
        
                if($this->upload->do_upload('userfile')) 
                {
                    $uploadimg = $this->upload->data();
                    $uimg = $uploadimg['file_name'];                    
                } 
                else
                {
                    $uimg = '';
                }
            }
            else
            {
                $uimg = $this->input->post('img_name');
            }

            $user_data = array(
                'name'=>$name,
                'personal_contact_no'=>$personal_contact_no,
                'city'=>$city,
                'photo'=>$uimg                
            );

            $where = array('id' => $user_id );
            $this->authentication_model->update_data('employees',$user_data,$where);
        
            $this->session->set_flashdata("success","Profile updated successfully");
            redirect("admin/dashboard");
        }
        
	}	
}  


}