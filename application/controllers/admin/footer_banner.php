<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Footer_banner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session'));
	}

	public function manage(){
		checkAuth();
		$xcrud = Xcrud::get_instance()->table('footer_banner');
		
		/**********view pages***********/
		$xcrud->order_by('id','desc');
		// simple image upload
		$xcrud->change_type('image', 'image');
		//$xcrud->label('subject','Tickets Name');
		//$xcrud->validation_required(array('name','code'));

		$data['content'] = $xcrud->render();
		$data['title']='Footer banner';
		admin_view('admin/allotmenu_list', $data);
    }
}
?>