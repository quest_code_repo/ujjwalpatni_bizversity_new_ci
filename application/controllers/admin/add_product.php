<?php
class Add_product extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array(
			'form',
			'url',
			'xcrud'
		));
		$this->load->model('admin/admin_common_model');
		$this->load->model('front_end/cat_frontend');
		$this->load->library(array(
			'form_validation',
			'session',
			'image_lib'
		));
	}

	// //function for managing variant category
	// function variant_category()
	// {
	//     $xcrud->validation_required('variant_category_name');
	//     $xcrud->validation_required('variant_category_status');
	//     $xcrud->disabled('create_date');
	//     $xcrud->unset_remove();
	//     $xcrud->unset_print();
	//     $xcrud->unset_csv();
	//     $data['content'] = $xcrud->render();
	//     $data['title']   = 'Variant Category List';
	//     admin_view('admin/allotmenu_list', $data);
	// }
	// //function for managing variant category
	// //function for managing variant subcategory
	// function variant_subcategories()
	// {
	//     $xcrud = Xcrud::get_instance()->table('variant_subcategories');
	//     $xcrud->validation_required('variant_subcategory_name');
	//     $xcrud->validation_required('variant_category_id');
	//     $xcrud->validation_required('variant_subcategory_status');
	//     $xcrud->disabled('create_date');
	//     $xcrud->label('variant_category_id', 'Variant Category');
	//     $xcrud->unset_remove();
	//     $xcrud->unset_print();
	//     $xcrud->unset_csv();
	//     $data['content'] = $xcrud->render();
	//     $data['title']   = 'Variant Sub-category List';
	//     admin_view('admin/allotmenu_list', $data);
	// }
	// //function for managing variant subcategory
	// //function for managing variant list
	// function variant_list()
	// {
	//     $xcrud = Xcrud::get_instance()->table('variant_list');
	//     $xcrud->validation_required('variant_name');
	//     $xcrud->validation_required('variant_category_id');
	//     $xcrud->validation_required('variant_subcategory_id');
	//     $xcrud->disabled('create_date');
	//     $xcrud->label('variant_category_id', 'Variant Category');
	//     $xcrud->label('variant_subcategory_id', 'Variant Subcategory');
	//     //variant category list
	//     //variant category list
	//     //variant subcategory list
	//     $xcrud->relation('variant_subcategory_id', 'variant_subcategories', 'variant_subcategory_id', 'variant_subcategory_name', '', '', '', '', '', 'variant_category_id', 'variant_category_id');
	//     //variant subcategory list
	//     $xcrud->unset_remove();
	//     $xcrud->unset_print();
	//     $xcrud->unset_csv();
	//     $data['content'] = $xcrud->render();
	//     $data['title']   = 'Variant List';
	//     admin_view('admin/allotmenu_list', $data);
	// }
	// //function for managing variant list
	// //function for adding product category

	function add_category()
	{
		if ($_POST)
		{
			$category_name = $this->input->post('category_name');
			// $varient_list = $this->input->post('varient_ids');
			$above_category = $this->input->post('above_category');
			$category_status = $this->input->post('active_status');
			$category_des = $this->input->post('category_des');
			$config = array(
				'upload_path' => "./uploads/category",
				'allowed_types' => "*",
				'overwrite' => false,
				'file_name' => time()
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('cat_image'))
			{
				$uploadimg = $this->upload->data();
				$uimg = $uploadimg['file_name'];
			}
			else
			{
				$uimg = '';
			}


			$alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $category_name));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT 'id' FROM `product_categories` WHERE `category_url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }

			$add_cat = array(
				'name' => $category_name,
				'parent_category' => $above_category,
				'category_description' => $category_des,
				'cat_img' => $uimg,
				'created_at' => date('Y-m-d') ,
				'active_status' => $category_status,
				'category_url'  => $blog_url1
			);
			$category_id = $this->admin_common_model->insert_data('product_categories', $add_cat);
			
				redirect('admin/Add_product/category_list');
		}

		

		$get_parent_category = $this->admin_common_model->fetch_condrecord('product_categories', array(
			'active_status' => 'active', 'parent_category'=>0 ));
		// $data['variant_cat_data'] = $variant_data;
		$data['parent_category'] = $get_parent_category;
		admin_view('admin/add_category', $data);
	}

	// //function for adding product category
	// //function for category list

	function category_list()
	{
		$xcrud = Xcrud::get_instance()->table('product_categories');
		/**********view pages***********/
		$xcrud->where('active_status','Active');
		$xcrud->unset_remove();
		$xcrud->unset_add();
		$xcrud->unset_edit();
		$xcrud->unset_print();
		$xcrud->unset_csv();

		// $xcrud->unset_search();
		// $xcrud->table_name('Category List');

		$xcrud->button('admin/add_product/edit_category/{id}', 'Edit Category', 'glyphicon glyphicon-edit');
		$xcrud->change_type('cat_img', 'image', false, array(
			'width' => 450,
			'path' => '../uploads/category',
			'thumbs' => array(
				array(
					'height' => 55,
					'width' => 120,
					'crop' => true,
					'marker' => '_th'
				)
			)
		));
		$xcrud->relation('parent_category', 'product_categories', 'id', 'name');
		$variant_list = $xcrud->nested_table('variant', 'id', 'product_category_variants', 'product_category_id');
		$variant_list->join('variant_id', 'variant_list', 'variant_id');
		$variant_list->relation('variant_list.variant_subcategory_id', 'variant_subcategories', 'variant_subcategory_id', 'variant_subcategory_name');
		$variant_list->relation('product_category_id', 'product_categories', 'id', 'name');
		$data['content'] = $xcrud->render();
		$data['title'] = 'Category List';
		admin_view('admin/allotmenu_list', $data);
	}

	// //function for category list
	// function to update category

	function edit_category($cat_id)
	{
		$category_detail = $this->admin_common_model->fetch_condrecord('product_categories', array(
			'id' => $cat_id,'active_status'=>'Active'));
		if (!empty($category_detail))
		{
			if ($_POST)
			{
				$category_name = $this->input->post('category_name');
				$parent_category = $this->input->post('above_category');
				$variants_names = $this->input->post('varient_ids');
				$category_status = $this->input->post('active_status');
				$category_des = $this->input->post('category_des');

				if (!empty($_FILES['cat_image']['name']))
				{
					$config = array(
						'upload_path' => "./uploads/category",
						'allowed_types' => "*",
						'overwrite' => false,
						'file_name' => time()
					);
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('cat_image'))
					{
						$uploadimg = $this->upload->data();
						$uimg = $uploadimg['file_name'];
					}
					else
					{
						$uimg = '';
						$this->session->set_flashdata("error", $this->upload->display_errors());
						redirect('admin/add_product/edit_category/'.$cat_id.'');
						exit();
					}
				}
				else
				{
					$uimg = $this->input->post('cat_image1');
				}


			   $alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $category_name));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT 'id' FROM `product_categories` WHERE `category_url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }

				$update_cat = array(
					'name' => $category_name,
					'category_description' => $category_des,
					'parent_category' => $parent_category,
					'cat_img' => $uimg,
					'active_status' => $category_status
				);
				$where_cat = array(
					'id' => $cat_id
				);
				$updated_row = $this->admin_common_model->update_data('product_categories', $update_cat, $where_cat);


				$this->session->set_flashdata("success", "Product category updated successfully");
				redirect('admin/add_product/category_list');
			}

		

			$get_parent_category = $this->admin_common_model->fetch_condrecord('product_categories', array('active_status' => 'active' ));

			

			$data['category_detail'] = $category_detail;

			// $data['category_variant_list'] = $category_variant;
			// $data['variant_cat_data']      = $variant_data;

			$data['parent_category'] = $get_parent_category;
			admin_view('admin/edit_category', $data);
		}
		else
		{
			redirect('admin/Add_product/category_list');
		}
	}

	// //function to update category
	// function for adding product

	


	function add_product_ebooks()
	{

		// $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
		// print_r($this->input->post());
		// echo $img_name = $_FILES["image_url"]["name"];
		// exit;


		if ($_POST)
		{

			$this->form_validation->set_rules('pname', 'Product Name', 'required');
			$this->form_validation->set_rules('product_price', 'Product Price', 'required');
			$this->form_validation->set_rules('short_disc', 'short Description', 'required');
			$this->form_validation->set_rules('product_category', 'Product Category', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$product_category     = $this->input->post('product_category');
				$pname                = $this->input->post('pname');
				$product_price        = $this->input->post('product_price');
				$disc_price           = $this->input->post('disc_price');
				$is_featured          = $this->input->post('is_featured');
				$short_disc           = $this->input->post('short_disc');
				$description          = $this->input->post('description');
				$active_inactive      = $this->input->post('active_inactive');
				$configbook_file      = array(
					'upload_path'     => "./uploads/e_books",
					'allowed_types'   => "pdf|doc|docx|word",
					'overwrite'       => false,
					'file_name'       => $_FILES["book_file"]["name"]
				);

				// $config['allowed_types'] = 'gif|jpg|png';

				$this->load->library('upload', $configbook_file);
				$this->upload->initialize($configbook_file);
				if ($this->upload->do_upload('book_file'))
				{
					$uploadimg = $this->upload->data();
					$book_file = $uploadimg['file_name'];
				}
				else
				{
					$book_file = '';
					$this->session->set_flashdata("book_file_error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/add_product/add_product_ebooks');
					// echo $this->upload->display_errors('<p class="alert alert-danger" style="color:red">', '</p>');
					exit('book_file');
				}

				$config = array(
					'upload_path' => "./uploads/products",

					// 'allowed_types' => "*",
					'allowed_types' => "gif|jpg|png",

					'overwrite' => false,
					'file_name' => $_FILES["image_url"]["name"]
				);
				// $config['allowed_types'] = 'gif|jpg|png';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_url'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = '';
					$this->session->set_flashdata("image_url_error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					// redirect('admin/Add_product/add_product_ebooks');
					echo $this->upload->display_errors();
					// exit('file_name');
				}

				// $config_fea = array(
				// 	'upload_path' => "./uploads/featured-image",
				// 	'overwrite' => false,
				// 	'allowed_types' => "gif|jpg|png",
				// 	'file_name' => $_FILES["featured_image"]["name"]
				// );
				// $this->load->library('upload', $config_fea);
				// $this->upload->initialize($config_fea);
				// if ($this->upload->do_upload('featured_image'))
				// {
				// 	$uploadimg = $this->upload->data();
				// 	$uimg_featured = $uploadimg['file_name'];
				// 	$this->session->set_flashdata("featured_image_error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
				// 	redirect('admin/add_product/add_product_ebooks');
				// }
				// else
				// {
				// 	$uimg_featured = '';
				// }


				$alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $pname));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT 'product_id' FROM `products` WHERE `url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }

                /**********Sorting Price***********/


                if($disc_price=='' || $disc_price=='0'){

                	$sorting_price_val = $product_price;
                
                } else {

                	$sorting_price_val = $disc_price;

                }

				$product_data = array(
					'product_category_id' => $product_category,
					'pname'             => $pname,
					'url'               => $blog_url1,
					'product_price'     => $product_price,
					'disc_price'        => $disc_price,
					'image_url'         => $uimg,
					'sorting_price'		=>$sorting_price_val,
					'product_type'      => 'ebook',
					'short_disc'        => $short_disc,
					'description'       => $description,
					'is_featured'       => $is_featured,
					'stock_status'      => 'In Stock',
					'active_inactive'   => $active_inactive,
					'product_alt_tag'   => $book_file
				);
				$product_id = $this->admin_common_model->insert_data('products', $product_data);

				if ($product_id > 0)
				{
					$this->session->set_flashdata("success", "E-Book added successfully");
					redirect('admin/add_product/product_ebooks_list');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
					redirect('admin/add_product/product_ebooks_list');
				}

				 redirect('admin/add_product/add_product_ebooks');
			}
			else{
				echo validation_errors();
				exit('ok');
			}
		}

		$data['product_category'] = $this->admin_common_model->getwheres('product_categories', array(
			'parent_category' => 1
		));
		admin_view('admin/add_product_ebooks', $data);
	}


	public function product_ebooks_list()
	{
	    
	    $xcrud = Xcrud::get_instance()->table('products');
	    $xcrud->where('product_type','ebook');
	    /**********view pages***********/
	    //$xcrud->unset_remove();
	    $xcrud->unset_add();
	    $xcrud->unset_edit();
	    // $xcrud->unset_search();
	    $xcrud->unset_print();
	    $xcrud->unset_csv();

	    $xcrud->columns("pname,product_price,short_disc", true);
	    $xcrud->button('admin/add_product/edit_product_ebook/{product_id}','Edit Ebook Details','glyphicon glyphicon-edit');
        
        $xcrud->label('pname', 'Product Name');
        $xcrud->label('product_price', 'Price');
        $xcrud->label('short_disc', 'Description');
	    $data['content'] = $xcrud->render();
	    $data['title']   = 'Product Ebook List';
	    admin_view('admin/Ebook_List', $data);
	}


	function edit_product_ebook($product_id)
	{
	    //code for updating data
	    if ($_POST) {


	        $this->form_validation->set_rules('pname', 'Product Name', 'required');
	        // $this->form_validation->set_rules('product_category', 'Product Category', 'required');
	        // $this->form_validation->set_rules('quantity', 'Product Quantity', 'required');
	        $this->form_validation->set_rules('product_price', 'Product Price', 'required');
	       // $this->form_validation->set_rules('course_duraion', 'Course Duration', 'required');
	        if ($this->form_validation->run() == FALSE) {
	        } else {
	            $pname           = $this->input->post('pname');
	            $product_id      = $this->input->post('product_id');
	            // $product_category = $this->input->post('product_category');
	            //$related_product = $this->input->post('related_product');
	            // $quantity = $this->input->post('quantity');
	            $product_price   = $this->input->post('product_price');
	            $disc_price      = $this->input->post('disc_price');
	            $is_featured     = $this->input->post('is_featured');
	            $active_inactive = $this->input->post('active_inactive');
	            $stock_status    = $this->input->post('stock_status');
	            $short_disc      = $this->input->post('short_disc');
	            $description     = $this->input->post('description');
	           // $pfeatures       = $this->input->post('pfeatures');
	           // $duration        = $this->input->post('course_duraion');
	            if (!empty($_FILES['image_url']['name'])) {
	                $config = array(
	                    'upload_path' => "./uploads/products",
	                    'allowed_types' => "*",
	                    'overwrite' => false,
	                    'file_name' => time()
	                );
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                if ($this->upload->do_upload('image_url')) {
	                    $uploadimg = $this->upload->data();
	                    $uimg      = $uploadimg['file_name'];
	                } else {
	                    $uimg = '';
	                    $this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
	                    redirect('admin/add_product/edit_product_ebook/' . $product_id);
	                    exit();
	                }
	            } else {
	                $uimg = $this->input->post('image_url1');
	            }


	             if (!empty($_FILES['book_file']['name'])) {
	                $config = array(
	                    'upload_path' => "./uploads/products",
	                    'allowed_types' => "*",
	                    'overwrite' => false,
	                    'file_name' => time()
	                );
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                if ($this->upload->do_upload('book_file')) {
	                    $uploadimg = $this->upload->data();
	                    $auimg      = $uploadimg['file_name'];
	                } else {
	                    $auimg = '';
	                    $this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
	                    redirect('admin/add_product/edit_product_ebook/' . $product_id);
	                    exit();
	                }
	            } else {
	                $auimg = $this->input->post('book_file1');
	            }

	            $alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $pname));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT 'product_id' FROM `products` WHERE `url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }

                /**********Sorting Price***********/


                if($disc_price=='' || $disc_price=='0'){

                	$sorting_price_val = $product_price;
                
                } else {

                	$sorting_price_val = $disc_price;

                }
	          
	            $product_data   = array(
	                
	                'pname' => $pname,
	                'url'   => $blog_url1,
	                'product_price' => $product_price,
	                'disc_price' => $disc_price,
	                'image_url' => $uimg,
	                'sorting_price' =>$sorting_price_val,
	                //'image_featured' => $uimg_featured,
	                'short_disc' => $short_disc,
	                'description' => $description,
	                'is_featured' => $is_featured,
	                'stock_status' => $stock_status,
	                'active_inactive' => $active_inactive,
	               // 'subscription_duration' => $duration,
	                'product_alt_tag' => $auimg
	            );
	            $where          = array(
	                'product_id' => $product_id
	            );
	            $product_update = $this->admin_common_model->update_data('products', $product_data, $where);
	            $this->session->set_flashdata("success", "Product updated successfully");
	            redirect('admin/add_product/product_ebooks_list');
	        }
	    }
	   // $data['product_category'] = $this->admin_common_model->fetch_record('product_categories');
	   // $data['all_products']     = $this->admin_common_model->fetch_record('products');
	    $where                  = array(
	        'product_id' => $product_id
	    );
	    //$data['related_product'] = $this->admin_common_model->fetch_condrecord('products', $where);
	    $data['product_detail'] = $this->admin_common_model->fetch_condrecord('products', $where);
	    $data['product_category'] = $this->admin_common_model->getwheres('product_categories', array(
			'parent_category' => 1
		));
	    admin_view('admin/edit_product_ebook', $data);
	}

//*****************************************************************************************//


	function add_new_product()
	{
		if ($_POST)
		{

			$this->form_validation->set_rules('pname', 'Product Name', 'required');
			$this->form_validation->set_rules('product_price', 'Product Price', 'required');
			//$this->form_validation->set_rules('course_duration', 'Courese Duration', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				// $product_category = $this->input->post('product_category');
				

				$pname                = $this->input->post('pname');
				$product_type         = $this->input->post('product_type');
				$product_price 	      = $this->input->post('product_price');
				$disc_price           = $this->input->post('disc_price');
				$is_featured          = $this->input->post('is_featured');
				$active_inactive      = $this->input->post('active_inactive');
				$stock_status         = $this->input->post('stock_status');
				$short_disc           = $this->input->post('short_disc');
				$description          = $this->input->post('description');
				$product_category_id  = $this->input->post('product_category_id');
				 $quantity            = $this->input->post('quantity');
			
				$config = array(
					'upload_path' => "./uploads/products",

					// 'max_size'=>'2048',
					// 'max_width'=>'1030',
					// 'max_height'=>'780',

					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_url'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/Add_product/add_new_product');
					exit();
				}

				// $config_fea = array(
				// 	'upload_path' => "./uploads/featured-image",

				// 	// 'max_size'=>'2048',
				// 	// 'max_width'=>'1030',
				// 	// 'max_height'=>'780',

				// 	'allowed_types' => "*",
				// 	'overwrite' => false,
				// 	'file_name' => time()
				// );
				// $this->load->library('upload', $config_fea);
				// $this->upload->initialize($config_fea);
				// if ($this->upload->do_upload('featured_image'))
				// {
				// 	$uploadimg = $this->upload->data();
				// 	$uimg_featured = $uploadimg['file_name'];
				// }
				// else
				// {
				// 	$uimg_featured = '';
				// }

				$alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $pname));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT 'product_id' FROM `products` WHERE `url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }

                /**********Sorting Price***********/


                if($disc_price=='' || $disc_price=='0'){

                	$sorting_price_val = $product_price;
                
                } else {

                	$sorting_price_val = $disc_price;

                }

				$product_data = array(
					
					'pname'               => $pname,
					'url'                 => $blog_url1,
					'product_price'       => $product_price,
					'disc_price'          => $disc_price,
					'image_url'           => $uimg,
					'short_disc'          => $short_disc,
					'description'         => $description,
					'product_type'        => $product_type,
					'sorting_price'       => $sorting_price_val,
					//'sub_product_type'    => $sub_product_type,
					'is_featured'         => $is_featured,
					'stock_status'        => $stock_status,
					'active_inactive'     => $active_inactive,
					'product_category_id' => $product_category_id,
					'quantity'            => $quantity,
					'product_alt_tag'     => $uimg
				);
				$product_id = $this->admin_common_model->insert_data('products', $product_data);
				if ($product_id > 0)
				{
					if(!empty($this->input->post('related_product')))
					{
						$relatedId = $this->input->post('related_product');
						$total     = count($relatedId);

						for($i=0;$i<$total;$i++)
						{
							$newRelated  = array('related_product_id'=> $relatedId[$i],'product_id'=>$product_id);
							$this->admin_common_model->insert_data('product_relation', $newRelated);
							$newRelated  = '';
						}
					}


					if(!empty($this->input->post('pro_languages')))
					{
							$pro_languages   =  $this->input->post('pro_languages');
							$total_languages = count($pro_languages);

							for($r=0;$r<$total_languages;$r++)
							{
								$newLanguage  = array('product_language_id'=> $pro_languages[$i],'product_id'=>$product_id);
								$this->admin_common_model->insert_data('product_language_relation', $newLanguage);
								$newLanguage  = '';
							}
					}


					$this->session->set_flashdata("success", "Product added successfully");
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}

				redirect('admin/add_product/product_list');
			}
		}

		
		$data['product_category'] = $this->admin_common_model->getwheres('product_categories', array('parent_category'=>14));

		$data['product_language'] = $this->admin_common_model->getwheres('product_language', array('deleted'=>'0','language_status'=>'Active'));


		admin_view('admin/add_product', $data);
	}
	

	// ****************************** Related Product For Books and Productivity Tools ***************************//

	public function get_related_products()
	{	
		$type = $this->input->post('type');

		if(!empty($type))
		{
			$related_product = $this->admin_common_model->getwheres('products', array('product_category_id' => $type));

			if(!empty($related_product) && count($related_product)>0)
			{
				
                        echo '<p>
                            <label>Related Product<span style="color:red;">*</span></label>
                            <span class="field">
                              <select class="test form-control" name="related_product[]" multiple="multiple">
                                 <option value="">Select Related Product</option>';
                                
                                  if(!empty($related_product) && count($related_product)>0)
                                  {
                                  	echo 'Please Select Related Product';
                                    foreach($related_product as $catDetails)
                                    {
                                      
                                     echo '<option value="'.$catDetails['product_id'].'">'.$catDetails['pname'].'</option>';
                                     
                                    }
                                  }
                                
                            echo '</select>
                            </span>
                          </p>';
			}
			else
			{
				echo   '<p>
                          <label>Related Product</label>
                          <select class="test form-control" multiple="multiple" >
                            <option value="">No Related Product</option>
                          </select>
                        </p>';
			}
		}
	}

	public function get_related_products_edit()
	{	
		$type = $this->input->post('type');
		$val_prees = $this->input->post('pree_data');
		$pro_ids = $this->input->post('product_id');

		$final_array = explode(',',$val_prees);

		if(!empty($final_array)){
         $p_id = array();
         foreach ($final_array as $pro_id)
         {
            $p_id[]= $pro_id;
         }
         $p_id_finale = $p_id;  

       } else{
        $p_id_finale = '';  
       }

		if(!empty($type))
		{

			$where_inner = "product_category_id = '$type' AND product_id NOT IN ($pro_ids)";
			$related_product = $this->admin_common_model->getwheres('products', $where_inner);

			if(!empty($related_product) && count($related_product)>0)
			{
				
                        echo '<p>
                            <label>Related Product<span style="color:red;">*</span></label>
                            <span class="field">
                              <select class="test form-control" name="related_product[]" multiple="multiple">
                                 <option value="">Select Related Product</option>';
                                
                                  if(!empty($related_product) && count($related_product)>0)
                                  {
                                  	echo 'Please Select Related Product';
                                    foreach($related_product as $catDetails)
                                    {

                                    if(!empty($p_id_finale)){

	                                if(in_array($catDetails['product_id'],$p_id_finale)){

	                                  $relted_selected = 'selected';
	                                
	                                } else{

	                                  $relted_selected = '';
	                                }


	                              } else{

	                                $relted_selected = '';
	                              }
                                      
                                     echo '<option value="'.$catDetails['product_id'].'" '.$relted_selected.'>'.$catDetails['pname'].'</option>';
                                     
                                    }
                                  }
                                
                            echo '</select>
                            </span>
                          </p>';
			}
			else
			{
				echo   '<p>
                          <label>Related Product</label>
                          <select class="test form-control" multiple="multiple" >
                            <option value="">No Related Product</option>
                          </select>
                        </p>';
			}
		}
	}

// ****************************** Related Product For Books and Productivity Tools ***************************//


	public function product_list()
	{
	    // $cond = array('product_type !=' => 'ebook', 'product_type !=' => 'e_learning');
	    $xcrud = Xcrud::get_instance()->table('products');
	    $xcrud->join('product_category_id','product_categories','id');
         $xcrud->where('product_type !=' , 'e_learning');
	    /**********view pages***********/
	    //$xcrud->unset_remove();
	    $xcrud->unset_add();
	    $xcrud->unset_edit();
	    $xcrud->unset_search();
	    $xcrud->unset_print();
	    $xcrud->unset_csv();

	    $xcrud->button('admin/add_product/edit_product/{product_id}','Edit Product Details','glyphicon glyphicon-edit');
	    $xcrud->columns("pname,product_price,short_disc,product_categories.name", true);
        
        $xcrud->label('pname', 'Product Name');
        $xcrud->label('product_price', 'Price');
        $xcrud->label('short_disc', 'Description');
        $xcrud->label('name', 'Product Category');
	    $data['content'] = $xcrud->render();

	    $data['title']   = 'Product List';
	    admin_view('admin/Product_List', $data);
	}



	function edit_product($product_id)
	{
	    //code for updating data
	    if ($_POST) {



	        $this->form_validation->set_rules('pname', 'Product Name', 'required');
	       
	        $this->form_validation->set_rules('product_price', 'Product Price', 'required');
	       
	        if ($this->form_validation->run() == FALSE) {
	        } else {
	            $pname           = $this->input->post('pname');
	            $product_id      = $this->input->post('product_id');
	            $product_price   = $this->input->post('product_price');
	            $disc_price      = $this->input->post('disc_price');
	            $is_featured     = $this->input->post('is_featured');
	            $active_inactive = $this->input->post('active_inactive');
	            $stock_status    = $this->input->post('stock_status');
	            $short_disc      = $this->input->post('short_disc');
	            $description     = $this->input->post('description');
	            $product_type    = $this->input->post('product_type');
	            $product_category_id = $this->input->post('product_category_id');
	            $quantity            = $this->input->post('quantity');
	            if (!empty($_FILES['product_image']['name'])) {
	                $config = array(
	                    'upload_path' => "./uploads/products",
	                    'allowed_types' => "*",
	                    'overwrite' => false,
	                    'file_name' => time()
	                );
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                if ($this->upload->do_upload('product_image')) {
	                    $uploadimg = $this->upload->data();
	                    $uimg      = $uploadimg['file_name'];
	                } else {
	                    $uimg = '';
	                    $this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
	                    redirect('admin/add_product/edit_product/' . $product_id);
	                    exit();
	                }
	            } else {
	                $uimg = $this->input->post('product_image1');
	            }

	            $alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $pname));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT 'product_id' FROM `products` WHERE `url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }

                /**********Sorting Price***********/


                if($disc_price=='' || $disc_price=='0'){

                	$sorting_price_val = $product_price;
                
                } else {

                	$sorting_price_val = $disc_price;

                }
	          
	            $product_data   = array(
	                
	                'pname' => $pname,
	                'url'   => $blog_url1,
	                'product_price' => $product_price,
	                'disc_price' => $disc_price,
	                'image_url' => $uimg,
	                'sorting_price'=>$sorting_price_val,
	                'short_disc' => $short_disc,
	                'description' => $description,
	                'is_featured' => $is_featured,
	                'stock_status' => $stock_status,
	                'active_inactive' => $active_inactive,
	                'product_type'  => $product_type,
	                'product_category_id' => $product_category_id,
	                'product_alt_tag' => $uimg,
	                'quantity'        => $quantity
	            );
	            $where          = array(
	                'product_id' => $product_id
	            );
	            $product_update = $this->admin_common_model->update_data('products', $product_data, $where);

	            /************update related products********/

	            $relation_data = array('related_product_id' => $product_id,'product_id'=>$product_id);
	            $this->admin_common_model->delete_data('product_relation', $relation_data);

	            /***********Insert Data again***********/

	            if(!empty($this->input->post('related_product')))
					{
						$relatedId = $this->input->post('related_product');
						$total     = count($relatedId);

						for($i=0;$i<$total;$i++)
						{
							$newRelated  = array('related_product_id'=> $relatedId[$i],'product_id'=>$product_id);
							$this->admin_common_model->insert_data('product_relation', $newRelated);
							$newRelated  = '';
						}
					}

						// ****************  Language Relation *******************//

					   $lang_data = array('product_id' => $product_id);
	                   $this->admin_common_model->delete_data('product_language_relation', $lang_data);

	                   if(!empty($this->input->post('pro_languages')))
					{
							$pro_languages   =  $this->input->post('pro_languages');
							$total_languages = count($pro_languages);

							for($r=0;$r<$total_languages;$r++)
							{
								$newLanguage  = array('product_language_id'=> $pro_languages[$r],'product_id'=>$product_id);
								$this->admin_common_model->insert_data('product_language_relation', $newLanguage);
								$newLanguage  = '';
							}
					}



	                   //*********************************************************//


	            $this->session->set_flashdata("success", "Product updated successfully");
	            redirect('admin/add_product/product_list');
	        }
	    }
	 
	    $where                  = array('product_id' => $product_id);
	    $data['product_detail'] = $this->admin_common_model->fetch_condrecord('products', $where);

	 

		$get_types = $data['product_detail'][0]['product_category_id'];
		$where_inner = "product_category_id = '$get_types' AND product_id NOT IN ($product_id)";

		$data['product_detail_inner'] = $this->admin_common_model->fetch_condrecord('products', $where_inner);

        $where_paper_cond = array('product_id'=>$product_id);
        $get_all_datas = $this->admin_common_model->get_data_orderby_where('product_relation','',$where_paper_cond,'');



        if(!empty($get_all_datas)){
         $p_id = array();
         foreach ($get_all_datas as $paper_id)
         {
            $p_id[]= $paper_id->related_product_id;
         }
         $data['p_ids'] = $p_id; 

       } else{
        $data['p_ids'] = '';  
       }

         $where_lang_cond = array('product_id'=>$product_id);
        $get_lang_datas = $this->admin_common_model->get_data_orderby_where('product_language_relation','',$where_lang_cond,'');


        if(!empty($get_lang_datas)){
         $l_id = array();
         foreach ($get_lang_datas as $lang_id)
         {
            $l_id[]= $lang_id->product_language_id;
         }
         $data['l_ids'] = $l_id; 

       } else{
        $data['l_ids'] = '';  
       }

       $data['product_category'] = $this->admin_common_model->getwheres('product_categories', array('parent_category'=>14));

       $data['product_language'] = $this->admin_common_model->getwheres('product_language', array('deleted'=>'0','language_status'=>'Active'));

	    admin_view('admin/edit_product', $data);
	}



	//*****************************************************************************************************///


	function add_product_elearning()
	{
		if ($_POST)
		{
			$this->form_validation->set_rules('pname', 'Product Name', 'required');
			$this->form_validation->set_rules('product_price', 'Product Price', 'required');
			$this->form_validation->set_rules('short_disc', 'short Description', 'required');
			$this->form_validation->set_rules('product_category', 'Product Category', 'required');
			//$this->form_validation->set_rules('product_course', 'Product Course', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$product_category 	= $this->input->post('product_category');
				$pname 				= $this->input->post('pname');
				$category_child 	= $this->input->post('category_child');
				$category_child_child = $this->input->post('category_child_child');
				$product_price 		= $this->input->post('product_price');
				$disc_price 		= $this->input->post('disc_price');
				$is_featured 		= $this->input->post('is_featured');
				$short_disc 		= $this->input->post('short_disc');
				$description 		= $this->input->post('description');
				$hrs 				= $this->input->post('hrs');
				$articles 			= $this->input->post('articles');
				$supplemental 		= $this->input->post('supplemental');
				$access 			= $this->input->post('access');
				//$course_id 			= $this->input->post('product_course');
				$Certificate 		= $this->input->post('Certificate');
				$mobile 			= $this->input->post('mobile');
				$active_inactive    = $this->input->post('active_inactive');
				$courseType         = $this->input->post('course_type');
				$video_url          = $this->input->post('video_url');
				$course_duration    = $this->input->post('course_duration');
				// $configbook_file = array(
				// 	'upload_path' => "./uploads/videos",
				// 	'allowed_types' => "*",
				// 	'overwrite' => false,
				// 	'file_name' => $_FILES["book_file"]["name"]
				// );

				// // $config['allowed_types'] = 'gif|jpg|png';

				// $this->load->library('upload', $configbook_file);
				// $this->upload->initialize($configbook_file);
				// if ($this->upload->do_upload('book_file'))
				// {
				// 	$uploadimg = $this->upload->data();
				// 	$book_file = $uploadimg['file_name'];
				// }
				// else
				// {
				// 	$book_file = '';
				// 	$this->session->set_flashdata("book_file_error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
				// 	// redirect('admin/Add_product/add_product_elearning');
				// 	// echo $this->upload->display_errors('<p class="alert alert-danger" style="color:red">', '</p>');
				// 	echo $this->upload->display_errors();
				// 	exit('book_file');
				// }

				$config = array(
					'upload_path' => "./uploads/products",

					// 'allowed_types' => "*",
					'allowed_types' => "gif|jpg|png",

					'overwrite' => false,
					'file_name' => $_FILES["image_url"]["name"]
				);
				// $config['allowed_types'] = 'gif|jpg|png';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_url'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = '';
					$this->session->set_flashdata("image_url_error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					// redirect('admin/Add_product/add_product_ebooks');
					echo $this->upload->display_errors();
					// exit('file_name');
				}

				$config_fea = array(
					'upload_path' => "./uploads/featured-image",
					'overwrite' => false,
					'allowed_types' => "gif|jpg|png",
					'file_name' => $_FILES["featured_image"]["name"]
				);
				$this->load->library('upload', $config_fea);
				$this->upload->initialize($config_fea);
				if ($this->upload->do_upload('featured_image'))
				{
					$uploadimg = $this->upload->data();
					$uimg_featured = $uploadimg['file_name'];
					$this->session->set_flashdata("featured_image_error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					echo $this->upload->display_errors();
					// redirect('admin/add_product/add_product_elearning');
				}
				else
				{
					$uimg_featured = '';
				}

				$alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $pname));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT 'product_id' FROM `products` WHERE `url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }

				$product_data = array(
					'product_category_id' => $product_category,
					'pname' => $pname,
					'url' => $blog_url1,
					'product_price' => $product_price,
					'disc_price' => $disc_price,
					'image_url' => $uimg,
					'image_featured' => $uimg_featured,
					'short_disc' => $short_disc,
					'description' => $description,
					'is_featured' => $is_featured,
					'stock_status' => 'In Stock',
					'active_inactive' => $active_inactive,
					//'product_alt_tag' => $book_file,
					'product_type' => 'e_learning',
					'course_type'  => $courseType,
					//'course_id' => $course_id,
					'video_url' => $video_url,
					'course_duration'=>$course_duration

				);
				$product_id = $this->admin_common_model->insert_data('products', $product_data);
				if ($product_id > 0)
				{
				


						// ***********  Inserting data into product data relation **************//

					$pdRelationData = array('product_id'     => $product_id,
											'duration_hours' => $hrs,
											'articles'       => $articles,
											'supplemental_resources' => $supplemental,
											'full_time_access'  => $access,
											'access_on_mobile' => $mobile,
											'completion_certificate' => $Certificate
										);


					

					$pd_relation_id = $this->admin_common_model->insert_data('product_data_relation',$pdRelationData);



					//*****************************************************************//

					$tags=$this->input->post('tags');
					$xtag=explode(",",$tags);
					foreach($xtag as $tag)
					{
		                $chk = array(
						'name' => $tag
						 );
					    $tagschk=$this->admin_common_model->fetch_recordbyid('tags',$chk);
						if(!$tagschk)
						{
							$tagdata = array(
							'name' => $tag,
							'created_by' => $this->session->userdata('sellerid'),
							'created_at' => date('Y-m-d H:i:s')
							);
							$tagsrs = $this->admin_common_model->insert_data('tags',$tagdata);
							if($tagsrs)
							{
								$ptagsval=array(
								'product_id' => $product_id,
								'tag_id' => $tagsrs
								);
								$ptagsrs = $this->admin_common_model->insert_data('product_tags',$ptagsval);
							}
						}
						else{
							$ptagsval=array(
								'product_id' => $product_id,
								'tag_id' => $tagschk->id
								);
							$ptagsrs = $this->admin_common_model->insert_data('product_tags',$ptagsval);
						}
					}

					$this->session->set_flashdata("success", "Course added successfully");
					// exit('ok');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}
				redirect('admin/add_product/add_product_elearning');
			}
			else{
				redirect('admin/add_product/add_product_elearning');
				echo validation_errors();
				exit('ok');
			}
		}

		$data['product_category'] = $this->admin_common_model->getwheres('product_categories', array('parent_category' => 4));
		$data['all_course'] = $this->admin_common_model->getwheres('tbl_semester', array('sem_status' => 'active'));
		admin_view('admin/add_product_elearning', $data);
	}


	/* edit e-learning product */ 
	function edit_elearning($product_id)
	{
		if ($_POST)
		{
			$this->form_validation->set_rules('pname', 'Product Name', 'required');
			$this->form_validation->set_rules('product_price', 'Product Price', 'required');
			$this->form_validation->set_rules('short_disc', 'short Description', 'required');
			$this->form_validation->set_rules('product_category', 'Product Category', 'required');
			//$this->form_validation->set_rules('product_course', 'Product Course', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$product_category 	= $this->input->post('product_category');
				$pname 				= $this->input->post('pname');
				$category_child 	= $this->input->post('category_child');
				$category_child_child = $this->input->post('category_child_child');
				$product_price 		= $this->input->post('product_price');
				$disc_price 		= $this->input->post('disc_price');
				$is_featured 		= $this->input->post('is_featured');
				$short_disc 		= $this->input->post('short_disc');
				$description 		= $this->input->post('description');
				$hrs 				= $this->input->post('hrs');
				$articles 			= $this->input->post('articles');
				$supplemental 		= $this->input->post('supplemental');
				$access 			= $this->input->post('access');
			//	$course_id 			= $this->input->post('product_course');
				$Certificate 		= $this->input->post('Certificate');
				$mobile 		= $this->input->post('mobile');
				$active_inactive = $this->input->post('active_inactive');
				$video_url          = $this->input->post('video_url');
				$courseType         = $this->input->post('course_type');
				$course_duration    = $this->input->post('course_duration');
				// $configbook_file = array(
				// 	'upload_path' => "./uploads/videos",
				// 	'allowed_types' => "*",
				// 	'overwrite' => false,
				// 	'file_name' => $_FILES["book_file"]["name"]
				// );

				// // $config['allowed_types'] = 'gif|jpg|png';

				// $this->load->library('upload', $configbook_file);
				// $this->upload->initialize($configbook_file);
				// if ($this->upload->do_upload('book_file'))
				// {
				// 	$uploadimg = $this->upload->data();
				// 	$book_file = $uploadimg['file_name'];
				// }
				// else
				// {
				// 	$book_file = $this->input->post('book_file1');
				// }




				$config = array(
					'upload_path' => "./uploads/products",

					// 'allowed_types' => "*",
					'allowed_types' => "gif|jpg|png",

					'overwrite' => false,
					'file_name' => $_FILES["image_url"]["name"]
				);
				// $config['allowed_types'] = 'gif|jpg|png';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_url'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = $this->input->post('image_url1');
				}

				$config_fea = array(
					'upload_path' => "./uploads/featured-image",
					'overwrite' => false,
					'allowed_types' => "gif|jpg|png",
					'file_name' => $_FILES["featured_image"]["name"]
				);
				$this->load->library('upload', $config_fea);
				$this->upload->initialize($config_fea);
				if ($this->upload->do_upload('featured_image'))
				{
					$uploadimg = $this->upload->data();
					$uimg_featured = $uploadimg['file_name'];
					$this->session->set_flashdata("featured_image_error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					echo $this->upload->display_errors();
					// redirect('admin/add_product/add_product_elearning');
				}
				else
				{
					$uimg_featured = $this->input->post('featured_image1');;
				}

				$alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $pname));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT 'product_id' FROM `products` WHERE `url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }

				$product_data = array(
					'product_category_id' => $product_category,
					'pname' => $pname,
					'url' => $blog_url1,
					'product_price' => $product_price,
					'disc_price' => $disc_price,
					'image_url' => $uimg,
					'image_featured' => $uimg_featured,
					'short_disc' => $short_disc,
					'description' => $description,
					'is_featured' => $is_featured,
					'stock_status' => 'In Stock',
					'active_inactive' => $active_inactive,
					//'product_alt_tag' => $book_file,
					'course_type'   => $courseType,
					//'course_id' => $course_id,
					'video_url'  => $video_url,
					'course_duration'=>$course_duration
				);
				$where          = array(
	                'product_id' => $product_id
	            );
	            $product_update = $this->admin_common_model->update_data('products', $product_data, $where);
				
				if ($product_id > 0)
				{
					
						$updateProData = array('product_id'     => $product_id,
											'duration_hours'    => $hrs,
											'articles'           => $articles,
											'supplemental_resources' => $supplemental,
											'full_time_access'  => $access,
											'access_on_mobile' => $mobile,
											'completion_certificate' => $Certificate
										);

						$this->admin_common_model->update_data('product_data_relation',$updateProData,$where);



					$tags=$this->input->post('tags');
					$xtag=explode(",",$tags);
					foreach($xtag as $tag)
					{
		                $chk = array(
						'name' => $tag
						 );
					    $tagschk=$this->admin_common_model->fetch_recordbyid('tags',$chk);
						if(!$tagschk)
						{
							$tagdata = array(
							'name' => $tag,
							'created_by' => $this->session->userdata('sellerid'),
							'created_at' => date('Y-m-d H:i:s')
							);
							$tagsrs = $this->admin_common_model->insert_data('tags',$tagdata);
							if($tagsrs)
							{
								$ptagsval=array(
								'product_id' => $product_id,
								'tag_id' => $tagsrs
								);
								$ptagsrs = $this->admin_common_model->insert_data('product_tags',$ptagsval);
							}
						}
						else{
							$ptagsval=array(
								'product_id' => $product_id,
								'tag_id' => $tagschk->id
								);
							$ptagsrs = $this->admin_common_model->insert_data('product_tags',$ptagsval);
						}
					}

					$this->session->set_flashdata("success", "Course Updated successfully");
					// exit('ok');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}
				redirect('admin/add_product/elearning_list');
			}
			else{
				redirect("admin/add_product/edit_elearning/$product_id");
				echo validation_errors();
				exit('ok');
			}
		}

		$data['product'] = $this->admin_common_model->getwheres('products', array('product_id' => $product_id));
		
		$data['product_meta'] = $this->admin_common_model->getwheres('product_data_relation',array('product_id' => $product_id));



		$product_tags = $this->admin_common_model->get_sql_record("SELECT `tags`.* FROM `tags`, `product_tags` WHERE `product_tags`.`tag_id`=`tags`.`id` and `product_tags`.`product_id`=$product_id ");
		$data['product_tags'] = $product_tags; 

		$data['product_category'] = $this->admin_common_model->getwheres('product_categories', array('parent_category' => 4));
		$data['all_course'] = $this->admin_common_model->getwheres('tbl_semester', array('sem_status' => 'active'));
		admin_view('admin/edit_product_elearning', $data);
	}

	    //function for E-Learning list
    function elearning_list()
    {
        $xcrud = Xcrud::get_instance()->table('products');
        /**********view pages***********/
        $xcrud->where('product_type','e_learning');
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        
        $xcrud->change_type('image_url', 'image', false, array(
            'width' => 450,
            'path' => '../uploads/products',
            'thumbs' => array(
                array(
                    'height' => 55,
                    'width' => 120,
                    'crop' => true,
                    'marker' => '_th'
                )
            )
        ));
        
        
        $xcrud->columns("pname,url,product_price,disc_price", true);
        
        $xcrud->label('pname', 'Name');
        $xcrud->label('disc_price', 'Discounted Price');
        
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        $xcrud->create_action('publish', 'activeElearning'); // action callback, function publish_action() in functions.php
	    $xcrud->create_action('unpublish', 'deactiveElearning');
	    $xcrud->button('#', 'unpublished', 'icon-close glyphicon glyphicon-remove', 'xcrud-action',
	        array(  // set action vars to the button
	            'data-task' => 'action',
	            'data-action' => 'publish',
	            'data-primary' => '{product_id}'),
	        array(  // set condition ( when button must be shown)
	            'active_inactive',
	            '=',
	            'inactive')
	    );
	    $xcrud->button('#', 'published', 'icon-checkmark glyphicon glyphicon-ok', 'xcrud-action', array(
	        'data-task' => 'action',
	        'data-action' => 'unpublish',
	        'data-primary' => '{product_id}'), array(
	        'active_inactive',
	        '=',
	        'active'));
        $xcrud->button('admin/add_product/edit_elearning/{product_id}', 'Edit', 'glyphicon glyphicon-edit');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'E-Learning List';
        admin_view('admin/elearning_list', $data);
        
    }


     //************************ Elearning Instructor ***********************//

    public function elearning_instructor_list()
    {
    	$data['Instructor'] = $this->admin_common_model->get_data_twotable_column_where('instructor','products','instructor_course_id','product_id',$column='',array(),$ordercol='instructor_id',$orderby='',$groupby='');



    	admin_view('admin/instructor/instructor_list',$data);
    }

    public function add_elearning_instructor()
    {
    	$data['Elearning'] =  $this->admin_common_model->get_data_column_where('products','product_id,pname',array('product_type' =>'e_learning','deleted'=>'0'),'product_id','');

    	admin_view('admin/instructor/add_elearning_instructor',$data);
    }


    public function add_new_instructor()
    {
    	$postData = $this->input->post();

    	if(!empty($postData))
    	{
    		$config = array(
					'upload_path' => "./uploads/instructor",

					// 'allowed_types' => "*",
					'allowed_types' => "*",

					'overwrite' => false,
					'file_name' => $_FILES["mat_image"]["name"]
				);
				// $config['allowed_types'] = 'gif|jpg|png';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('mat_image'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = "";
				}

				$newData = array('instructor_name'    => $postData['instructor_name'],
								 'instructor_course_id'=> $postData['above_category'],
								 'instructor_image'   => $uimg,
								 'facebook_link'      => $postData['facebook_link'],
								 'twitter_link'       => $postData['twitter_link'],
								 'google_link'        => $postData['google_link'],
								 'youtube_link'       => $postData['youtube_link'],
								 'instructor_description'=>$postData['instructor_description']
								);

					$this->admin_common_model->insert_data('instructor',$newData);

					redirect('admin/add_product/elearning_instructor_list','refresh');
    	}
    }

    public function edit_instructor($encrypted_instructor_id = "")
    {
    	$instructorId = base64_decode($encrypted_instructor_id);

    	if(!empty($instructorId))
    	{

    		$data['Instructor'] =  $this->admin_common_model->get_data_column_where('instructor','',array('instructor_id' =>$instructorId),'instructor_id','');

    		$data['Elearning'] =  $this->admin_common_model->get_data_column_where('products','product_id,pname',array('product_type' =>'e_learning','deleted'=>'0'),'product_id','');

    		admin_view('admin/instructor/edit_instructor',$data);
    	}
    }


    public function update_instructor()
    {
    	$postData = $this->input->post();

    	if(!empty($postData))
    	{
    		$config = array(
					'upload_path' => "./uploads/instructor",

					// 'allowed_types' => "*",
					'allowed_types' => "*",

					'overwrite' => false,
					'file_name' => $_FILES["mat_image"]["name"]
				);
				// $config['allowed_types'] = 'gif|jpg|png';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('mat_image'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = $postData['mat_image1'];
				}

				$updateData = array('instructor_name'    => $postData['instructor_name'],
								 'instructor_course_id'=> $postData['above_category'],
								 'instructor_image'   => $uimg,
								 'facebook_link'      => $postData['facebook_link'],
								 'twitter_link'       => $postData['twitter_link'],
								 'google_link'        => $postData['google_link'],
								 'youtube_link'       => $postData['youtube_link'],
								 'instructor_description'=>$postData['instructor_description']
								);

					$this->admin_common_model->update_data('instructor',$updateData,array('instructor_id'=>$postData['instructor_id']));

					redirect('admin/add_product/elearning_instructor_list','refresh');
    	}
    }




    //***********************************************************************//


    // ********************************** Product Coupon ****************************//

    public function product_coupon_list()
    {
    	$data['coupon_list'] = $this->admin_common_model->get_data_orderby_where('coupons','','','');
		admin_view('admin/product_coupon/product_coupon_list',$data);
    }

    public function add_product_coupons()
    {
    	if($_POST){

    		
			$coupon_name =  $this->input->post('coupons_names');
			$coupon_type =  $this->input->post('coupon_types');

			if(!empty($this->input->post('flat_amount')))
			{
				$coupon_flat_amount =  $this->input->post('flat_amount');
			}
			else
			{
				$coupon_flat_amount =  $this->input->post('discount_percentage');
			}
			
			$coupon_from =  $this->input->post('coupon_from');
			$coupon_to =  $this->input->post('coupon_to');
			$status =  $this->input->post('coupon_status');
		


			$dupliCoupon = $this->admin_common_model->get_data_orderby_where('coupons','id',array('status'=>'active','code'=>$coupon_name),'DESC');

			if(!empty($dupliCoupon))
			{
				    $this->session->set_flashdata("error", "Coupon Already Exists");
					redirect('admin/add_product/add_product_coupons');
					die();
			}


			$insert_array =  array(
				'code'             => $coupon_name, 
				'discount_type'    => $coupon_type, 
				'discount'         => $coupon_flat_amount, 
				'valid_start'      => $coupon_from, 
				'valid_end'        => $coupon_to, 
				'created_at'       => date('Y-m-d H:i:s'), 
				'created_by'       => $this->session->userdata('id'), 
				'status'           => $status
				);

			$insert_data =  $this->admin_common_model->insert_data('coupons',$insert_array);

			if($insert_data){
			
					$this->session->set_flashdata("success", "Coupon added successfully");

					redirect('admin/add_product/add_product_coupons');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}
			}

		admin_view('admin/product_coupon/add_product_coupon',$data);
    }



    public function edit_product_coupon()
    {
    	$coupon_id = $this->input->get('coupon_id');

    	if(!empty($coupon_id))
    	{
    		$id = base64_decode($coupon_id);

	    	$data['cdata'] = $this->admin_common_model->get_data_orderby_where('coupons','id',array('id'=>$id),'DESC');

	    	admin_view('admin/product_coupon/edit_product_coupon',$data);
    	}
    	else
    	{
    		redirect('admin/add_product/add_product_coupons');
    	}

    	
    }

    public function update_coupon()
    {
    	$postData = $this->input->post();

    	if(!empty($postData))
    	{

			$coupon_name =  $this->input->post('coupons_names');
			$coupon_type =  $this->input->post('coupon_types');

			if(!empty($this->input->post('flat_amount')))
			{
				$coupon_flat_amount =  $this->input->post('flat_amount');
			}
			else
			{
				$coupon_flat_amount =  $this->input->post('discount_percentage');
			}
			
			$coupon_from =  $this->input->post('coupon_from');
			$coupon_to =  $this->input->post('coupon_to');
			$status =  $this->input->post('coupon_status');
			$couponId = base64_decode($this->input->post('id'));


				$dupliCoupon = $this->admin_common_model->get_data_orderby_where('coupons','id',array('status'=>'active','code'=>$coupon_name,'id !='=>$couponId),'DESC');

			if(!empty($dupliCoupon))
			{
				    $this->session->set_flashdata("error", "Coupon Already Exists");
					redirect('admin/add_product/edit_product_coupon/?coupon_id='.$this->input->post("id").'');
					die();
			}

			$couponData = array('code'          => $coupon_name,
								'discount_type' => $coupon_type,
								'discount'      => $coupon_flat_amount,
								'valid_start'   => $coupon_from,
								'valid_end'     => $coupon_to,
								'status'        => $status
								);

			$this->admin_common_model->update_data('coupons', $couponData,array('id' =>$couponId ));
			redirect('admin/add_product/product_coupon_list');
    	}
    }

    public function delete_product_coupon()
    {
    	$id = $this->input->post('id');

    	if(!empty($id))
    	{
    		$this->db->where('id',$id);
    		$this->db->delete('coupons');
    		redirect('admin/add_product/product_coupon_list');
    	}
    }


    //********************************** Product coupon ************************************//


    //************************ Check product name***********************//
    public function check_product_name()
    {
    	$postData = $this->input->post();

    	if(!empty($postData))
    	{
    		$pname    = $postData['pname'];
    		$check_id = $postData['check_id']; 

    		if(empty($check_id))
    		{
    			$WHERE = "product_type in('book','productivity_tools') AND pname = '$pname' AND active_inactive = 'active' ";
    		}
    		else
    		{
    			$WHERE = "product_type in('book','productivity_tools') AND pname = '$pname' AND active_inactive = 'active' AND product_id != '$check_id' ";
    		}
    		

    		$checkProduct = $this->cat_frontend->getwhere('products',$WHERE,'');
    		

    		if(!empty($checkProduct))
    		{
    			echo "SUCCESS";
    		}
    	}
    }


     //************************ Check product name***********************//


    //************************  Check Ebook ****************//

    public function check_ebook()
    {
    	$postData = $this->input->post();

    	if(!empty($postData))
    	{

    		$ebook_name = $postData['pname'];
    		$check_id   = $postData['check_id'];

    		if(empty($check_id))
    		{
    			$checkProduct = $this->cat_frontend->getwhere('products',array('product_type'=>'ebook','pname'=>$ebook_name),'');
    		}
    		else
    		{
    			$checkProduct = $this->cat_frontend->getwhere('products',array('product_type'=>'ebook','pname'=>$ebook_name,'product_id !='=>$check_id),'');
    		}
    		

    		if(!empty($checkProduct))
    		{
    			echo "SUCCESS";
    		}
    	}
    }

    // *************************  Check Ebook **********************//



     // *****************  Language List *************************//


    public function language_list()
    {
    	$data['LanguageData'] = $this->cat_frontend->getwhere('product_language','','');

    	admin_view('admin/product/language_list',$data);
    }


    public function add_product_language()
    {
    	admin_view('admin/product/add_product_language');
    }


    public function add_new_language()
    {
    	$postData = $this->input->post();

    	if(!empty($postData))
    	{
    		$checkLanguage = $this->cat_frontend->getwhere('product_language',array('product_language_name'=>$postData['language_name']),'');

    		if(!empty($checkLanguage))
    		{
    			 $this->session->set_flashdata('error', "Language Already Exits , Please Try Another One.");
    			 redirect('admin/add_product/add_product_language','refresh');
    			 die();
    		}

    		$newLanguage = array('product_language_name' => $postData['language_name'],
    							'language_status'        => $postData['active_status']
    						);

    			$languageId =  $this->admin_common_model->insert_data('product_language',$newLanguage);


    			if(!empty($languageId))
    			{
    				 $this->session->set_flashdata('success', "Language Added Succesfully");
    				 redirect('admin/add_product/add_product_language','refresh');
    			}
    			else
    			{
    				$this->session->set_flashdata('error', "Something Went Wrong, Please Try Again Later");
    				 redirect('admin/add_product/add_product_language','refresh');
    			}

    	}
    }

    public function edit_product_language()
    {
    	$decryptedId = $this->input->get('product_language_id');

    	if(!empty($decryptedId))
    	{
    		$languageId = base64_decode($decryptedId);

    		$data['Language'] = $this->cat_frontend->getwhere('product_language',array('product_language_id'=>$languageId),'');

    		admin_view('admin/product/edit_product_language',$data);
    	}
    }


    public function update_language()
    {
    	$postData = $this->input->post();

    	if(!empty($postData))
    	{
    		$checkLanguage = $this->cat_frontend->getwhere('product_language',array('product_language_name'=>$postData['language_name'],'product_language_id !='=>$postData['product_language_id']),'');

    		if(!empty($checkLanguage))
    		{
    			 $this->session->set_flashdata('error', "Language Already Exits , Please Try Another One.");
    			 redirect('admin/add_product/edit_product_language/?product_language_id= '.base64_encode($postData['product_language_id']).' ','refresh');
    			 die();
    		}

    		$updateLanguage = array('product_language_name' => $postData['language_name'],
    								'language_status'       => $postData['active_status']);


    		 $this->admin_common_model->update_data('product_language',$updateLanguage,array('product_language_id'=>$postData['product_language_id']));

    		 redirect('admin/add_product/language_list','refresh');
    	}
    }


    public function delete_product_language()
    {
    	$languageId = $this->input->post('product_language_id');

    	if(!empty($languageId))
    	{
    			$this->db->where('product_language_id',$languageId);
    			$this->db->delete('product_language');
    			echo "SUCCESS";
    	}
    }


     // ***********************************************************//





	// //function for adding product
	// //function for editing product
	
	// //function for editing product
	// //function for deleting product
	// function delete_product($productid)
	// {
	//     $product_detail = $this->admin_common_model->fetch_condrecord('products', array(
	//         'product_id' => $productid
	//     ));
	//     if (!empty($product_detail)) {
	//         //delete product
	//         $deleted_product = $this->admin_common_model->delete_data('products', array(
	//             'product_id' => $productid
	//         ));
	//         if ($deleted_product == TRUE) {
	//             $this->session->set_flashdata('success', "Product deleted successfully.");
	//         } else {
	//             $this->session->set_flashdata('success', "Something went wrong!!Please try again");
	//         }
	//         redirect('admin/furnish_dashboard/product_list');
	//         //delete product
	//     } else {
	//         redirect('admin/furnish_dashboard/product_list');
	//     }
	// }
	// //function for deleting product
	// //function for category variant list
	// function get_category_variant()
	// {
	//     $category_id      = $this->input->post('category_id');
	//     //$variant_category=$this->admin_common_model->fetch_condrecord('product_category_variants',array('product_category_id'=>$category_id));
	//     $variant_category = $this->admin_common_model->fetch_join_condi_records('variant_list', 'product_category_variants', 'variant_id', 'variant_id', 'product_category_id', $category_id);
	//     echo json_encode($variant_category);
	// }
	// //function for category variant list
	// //function for product list
	// function product_list()
	// {
	//     $xcrud = Xcrud::get_instance()->table('products');
	//     /**********view pages***********/
	//     //$xcrud->unset_remove();
	//     $xcrud->unset_add();
	//     $xcrud->unset_edit();
	//     // $xcrud->unset_search();
	//     $xcrud->unset_print();
	//     $xcrud->unset_csv();
	//     $xcrud->change_type('image_url', 'image', false, array(
	//         'width' => 450,
	//         'path' => '../uploads/products',
	//         'thumbs' => array(
	//             array(
	//                 'height' => 55,
	//                 'width' => 120,
	//                 'crop' => true,
	//                 'marker' => '_th'
	//             )
	//         )
	//     ));
	//     $xcrud->columns("pname,quantity,subscription_duration,product_price,disc_price,is_featured,image_url,stock_status,short_disc,active_inactive", true);
	//     $xcrud->label('pname', 'Course Name');
	//     $xcrud->label('stock_status', 'Stock Status');
	//     $xcrud->label('subscription_duration', 'Duration(In Days)');
	//     $xcrud->label('pfeatures', 'Features');
	//     $xcrud->label('pproperties', 'Properties');
	//     $xcrud->label('description', 'Description');
	//     $xcrud->label('image_url', 'Course Image');
	//     $xcrud->label('active_inactive', 'Course Status');
	//     $xcrud->button('edit_product/{product_id}', 'Edit Course', 'glyphicon glyphicon-edit');
	//     // $xcrud->button('delete_product/{product_id}','Delete Product','glyphicon glyphicon-remove');
	//     $data['content'] = $xcrud->render();
	//     $data['title']   = 'Course List';
	//     admin_view('admin/allotmenu_list', $data);
	// }
	// //function for product list
	// //function for getting product dimension images
	// function get_dim_images()
	// {
	//     $product_id = $this->input->post('product_id');
	//     $image_list = $this->admin_common_model->fetch_condrecord('product_dimension_images', array(
	//         'product_id' => $product_id
	//     ), 'dimension_img_id');
	//     $data['dimension_image'] = $image_list;
	//     $this->load->view('admin/dimension_image', $data);
	// }
	// //function for getting product dimension images
	// //function for getting product gallery images
	// function get_gallery_images()
	// {
	//     $product_id = $this->input->post('product_id');
	//     $var_id     = $this->input->post('var_id');
	//     $color      = $this->input->post('color');
	//     $image_list = $this->admin_common_model->fetch_condrecord('products_images', array(
	//         'product_id' => $product_id,
	//         'attr_id' => $var_id,
	//         'colorcode' => $color
	//     ), 'id');
	//     // echo $this->db->last_query();
	//     // die();
	//     $data['gallery_image'] = $image_list;
	//     $this->load->view('admin/product_gallery_image', $data);
	// }
	// //function for getting product gallery images
	// //function for removing dimension
	// function remove_dimension_image()
	// {
	//     $dimension = $this->input->post('dimension_id');
	//     $delete_img = $this->admin_common_model->delete_data('product_dimension_images', array(
	//         'dimension_img_id' => $dimension
	//     ));
	//     if ($delete_img == TRUE) {
	//         echo "1";
	//     } else {
	//         echo "0";
	//     }
	// }
	// //function for removing dimension
	// //function for removing gallery
	// function remove_gallery_image()
	// {
	//     $gallery_id = $this->input->post('gallery_id');
	//     $delete_img1 = $this->admin_common_model->delete_data('products_images', array(
	//         'id' => $gallery_id
	//     ));
	//     if ($delete_img1 == TRUE) {
	//         echo "1";
	//     } else {
	//         echo "0";
	//     }
	// }
	// //function for removing gallery
	// //function for managing slider
	// function product_dimension_slider()
	// {
	//     if ($_POST) {
	//         $product_id = $this->input->post('product_id');
	//         $uploade_file = $this->upload_files("./uploads/dimension_images", $_FILES['product_dimension_image'], 'product_dimension_image[]');
	//         $uploaddata = array();
	//         if (!empty($uploade_file)) {
	//             foreach ($uploade_file as $each_file) {
	//                 $dimension_data = array(
	//                     'dimension_image' => $each_file,
	//                     'product_id' => $product_id,
	//                     'created_date' => date('Y-m-d H:i:s')
	//                 );
	//                 $produt_img = $this->admin_common_model->insert_data('product_dimension_images', $dimension_data);
	//                 $uploaddata[] = $produt_img;
	//             }
	//             if (!empty($uploaddata)) {
	//                 $this->session->set_flashdata('success', "Product dimension images added successfully.");
	//             } else {
	//             }
	//         }
	//         redirect('admin/furnish_dashboard/product_dimension_slider');
	//     }
	//     $product_list = $this->admin_common_model->fetch_condrecord('products', array(
	//         'active_inactive' => 'active'
	//     ));
	//     $data['product_list'] = $product_list;
	//     admin_view('admin/add_product_slider', $data);
	// }
	// //function for managing slider
	// //function for managing slider
	// function product_gallery_slider()
	// {
	//     if ($_POST) {
	//         $product_id = $this->input->post('product_id');
	//         $attr_id    = $this->input->post('attr_id');
	//         $attr_val   = $this->input->post('attr_val');
	//         $colorcode  = $this->input->post('colorhash');
	//         // $uploade_file=$this->upload_files("./uploads/gallery",$_FILES['product_gallery_image'],'product_gallery_image[]');
	//         /* if(!empty($uploade_file))
	//         {
	//         foreach($uploade_file as $each_file)
	//         {
	//         $gallery_data=array(
	//         'image_url'=>$each_file,
	//         'product_id'=>$product_id,
	//         'att_type'=>$attr_id,
	//         'attr_id'=>$attr_val,
	//         'colorcode'=>$colorcode
	//         );
	//         $produt_img=$this->admin_common_model->insert_data('products_images',$gallery_data);
	//         }
	//         }*/
	//         if (!empty($_FILES['product_gallery_image']['name'])) {
	//             $files = $_FILES['product_gallery_image'];
	//             $name  = 'product_gallery_image[]';
	//             $images = array();
	//             $this->load->library('upload', $config);
	//             // $alphabet=array('a','b','c','d','e','f','g');
	//             $i            = 0;
	//             $file_gallery = array();
	//             foreach ($files['name'] as $key => $image) {
	//                 $_FILES[$name]['name']     = $files['name'][$key];
	//                 $_FILES[$name]['type']     = $files['type'][$key];
	//                 $_FILES[$name]['tmp_name'] = $files['tmp_name'][$key];
	//                 $_FILES[$name]['error']    = $files['error'][$key];
	//                 $_FILES[$name]['size']     = $files['size'][$key];
	//                 $config = array(
	//                     'upload_path' => "./uploads/gallery",
	//                     'allowed_types' => '*',
	//                     // 'max_width'=>'1030',
	//                     // 'max_height'=>'780',
	//                     'overwrite' => false,
	//                     'file_name' => time()
	//                 );
	//                 $this->upload->initialize($config);
	//                 if ($this->upload->do_upload($name)) {
	//                     $uploaded_image = $this->upload->data();
	//                     $uimg           = $uploaded_image['file_name'];
	//                     $gallery_data = array(
	//                         'image_url' => $uimg,
	//                         'product_id' => $product_id,
	//                         'att_type' => $attr_id,
	//                         'attr_id' => $attr_val,
	//                         'colorcode' => $colorcode
	//                     );
	//                     $produt_img     = $this->admin_common_model->insert_data('products_images', $gallery_data);
	//                     $file_gallery[] = $produt_img;
	//                     //$images[]=$uimg;
	//                 } else {
	//                     return false;
	//                 }
	//                 if (!empty($file_gallery)) {
	//                     $this->session->set_flashdata('success', "Product gallery images added successfully.");
	//                 }
	//                 $i++;
	//             }
	//         }
	//         redirect('admin/furnish_dashboard/product_gallery_slider');
	//     }
	//     $data['product_list']   = $this->admin_common_model->fetch_condrecord('products', array(
	//         'active_inactive' => 'active'
	//     ));
	//     $data['attribute_list'] = $this->admin_common_model->fetch_record('attribute_type');
	//     $data['color_list'] = $this->admin_common_model->fetch_condrecord('product_attrs', array(
	//         'attr_type_id' => '1'
	//     ));
	//     admin_view('admin/add_gallery_slider', $data);
	// }
	// //function for managing slider
	// //function for adding  product story products
	// function add_product_story()
	// {
	//     if ($_POST) {
	//         $product_ids = $this->input->post('product_id');
	//         if (!empty($product_ids)) {
	//             foreach ($product_ids as $each_id) {
	//                 $product = array(
	//                     'product_id' => $each_id,
	//                     'create_date' => date('Y-m-d H:i:s')
	//                 );
	//                 $ins_product = $this->admin_common_model->insert_data('product_story', $product);
	//             }
	//             if ($ins_product > 0) {
	//                 $this->session->set_flashdata('success', "Products added succesfully");
	//                 redirect('admin/furnish_dashboard/add_product_story');
	//             }
	//         }
	//     }
	//     $data['product_list'] = $this->admin_common_model->fetch_condrecord('products', array(
	//         'active_inactive' => 'active'
	//     ));
	//     admin_view('admin/add_product_story', $data);
	// }
	// //function for adding product story products
	// //function for displaying product story products
	// function product_story_listing()
	// {
	//     $xcrud = Xcrud::get_instance()->table('product_story');
	//     //$xcrud->unset_remove();
	//     $xcrud->unset_add();
	//     $xcrud->unset_edit();
	//     //$xcrud->unset_search();
	//     $xcrud->unset_print();
	//     $xcrud->unset_csv();
	//     $xcrud->table_name('Product Story List');
	//     $xcrud->relation('product_id', 'products', 'product_id', 'pname');
	//     $data['content'] = $xcrud->render();
	//     $data['title']   = 'Product Story List';
	//     admin_view('admin/allotmenu_list', $data);
	// }
	// //function for displaying product story products
	// //function for uploading images
	// function image_upload($imgname, $path_folder)
	// {
	//     $config = array(
	//         'upload_path' => "./" . $path_folder,
	//         'allowed_types' => "*",
	//         'overwrite' => false,
	//         'file_name' => time()
	//     );
	//     $this->load->library('upload', $config);
	//     if ($this->upload->do_upload($imgname)) {
	//         $uploadimg = $this->upload->data();
	//         $uimg      = $uploadimg['file_name'];
	//         return $uimg;
	//     } else {
	//         $uimg  = '';
	//         $error = array(
	//             'error' => $this->upload->display_errors()
	//         );
	//         return false;
	//     }
	// }
	// //function for uploading images
	// //function for uploading multiple images
	// private function upload_files($path, $files, $name)
	// {
	//     $config = array(
	//         'upload_path' => $path,
	//         // 'max_width'=>'1030',
	//         // 'max_height'=>'780',
	//         'allowed_types' => '*',
	//         'overwrite' => false,
	//         'file_name' => time()
	//     );
	//     $this->load->library('upload', $config);
	//     $images = array();
	//     foreach ($files['name'] as $key => $image) {
	//         $_FILES[$name]['name']     = $files['name'][$key];
	//         $_FILES[$name]['type']     = $files['type'][$key];
	//         $_FILES[$name]['tmp_name'] = $files['tmp_name'][$key];
	//         $_FILES[$name]['error']    = $files['error'][$key];
	//         $_FILES[$name]['size']     = $files['size'][$key];
	//         $this->upload->initialize($config);
	//         if ($this->upload->do_upload($name)) {
	//             $uploaded_image = $this->upload->data();
	//             $uimg           = $uploaded_image['file_name'];
	//             $images[] = $uimg;
	//         } else {
	//             return false;
	//         }
	//     }
	//     return $images;
	// }
	// //function for uploading multiple images
	// // Function For Add Category Properties
	// function category_property()
	// {
	//     if ($_POST) {
	//         $cat_id     = $this->input->post('cat_id');
	//         $prop       = $this->input->post('property');
	//         $data       = array(
	//             'category_id' => $cat_id,
	//             'property_name' => $prop
	//         );
	//         $produt_img = $this->admin_common_model->insert_data('category_property', $data);
	//         $this->session->set_flashdata('success', "Category Property Added Successfully.");
	//         redirect('admin/furnish_dashboard/category_property');
	//     }
	//     $data['category_list'] = $this->admin_common_model->fetch_record('product_categories');
	//     admin_view('admin/add_cat_property', $data);
	// }
	// //
	// function get_cat_property()
	// {
	//     $cat_id    = $this->input->post('cat_id');
	//     $prop_list = $this->admin_common_model->fetch_condrecord('category_property', array(
	//         'category_id' => $cat_id
	//     ));
	//     $data['prop_list'] = $prop_list;
	//     echo $this->load->view('admin/property_list', $data, true);
	// }
	// function remove_cat_property()
	// {
	//     $prop_id     = $this->input->post('prop_id');
	//     //echo $prop_id;
	//     $delete_prop = $this->admin_common_model->delete_data('category_property', array(
	//         'property_id' => $prop_id
	//     ));
	//     if ($delete_prop == TRUE) {
	//         echo "1";
	//     } else {
	//         echo "0";
	//     }
	// }
	// function get_cat_property_detail()
	// {
	//     $category_id = $this->input->post('category_id');
	//     $prop_list   = $this->admin_common_model->fetch_condrecord('category_property', array(
	//         'category_id' => $category_id
	//     ));
	//     if ($prop_list) {
	//         echo json_encode($prop_list);
	//     } else {
	//         echo 0;
	//     }
	// }
	// function fetch_property($cid)
	// {
	//     $prop_list = $this->admin_common_model->fetch_condrecord('category_property', array(
	//         'category_id' => $cid
	//     ));
	//     return $prop_list;
	// }
	// function fetch_property_value($propid, $proid)
	// {
	//     $where   = array(
	//         'product_id' => $proid,
	//         'property_id' => $propid
	//     );
	//     $pro_val = $this->admin_common_model->fetch_condrecord('product_property_rel', $where);
	//     return $pro_val;
	// }
	// function get_attribute_value()
	// {
	//     $attr_id = $this->input->post('attr_id');
	//     $color   = $this->input->post('color');
	//     $res     = $this->admin_common_model->fetch_condrecord('product_attrs', array(
	//         'attr_type_id' => $attr_id
	//     ), 'attr_name');
	//     if ($res) {
	//         $data['status'] = 1;
	//         $data['result'] = $res;
	//     } else {
	//         $data['status'] = 0;
	//         $data['result'] = '';
	//     }
	//     echo json_encode($data);
	// }
	// /* function for discount functionality */
	// function product_discount()
	// {
	//     if ($_POST) {
	//         $cat_id       = $this->input->post('cat_id');
	//         $product_id   = $this->input->post('product_name');
	//         $discount_per = $this->input->post('discount_amt');
	//         if (!empty($cat_id) && !empty($product_id)) {
	//             foreach ($product_id as $each_product) {
	//                 $upd_result = $this->admin_common_model->update_data('products', array(
	//                     'discount_amt' => $discount_per
	//                 ), array(
	//                     'product_id' => $each_product
	//                 ));
	//             }
	//         } else if (!empty($cat_id) && empty($product_id)) {
	//             //update category discount
	//             $upd_result = $this->admin_common_model->update_data('product_categories', array(
	//                 'category_discount' => $discount_per
	//             ), array(
	//                 'id' => $cat_id
	//             ));
	//         }
	//         if ($upd_result == true) {
	//             $this->session->set_flashdata('success', "Discount updated successfully");
	//         }
	//         redirect('admin/furnish_dashboard/product_discount');
	//     }
	//     $data['category_list'] = $this->admin_common_model->fetch_condrecord('product_categories', array(
	//         'active_status' => 'Active'
	//     ));
	//     admin_view('admin/add_product_discount', $data);
	// }
	// function get_cat_product()
	// {
	//     $cat_id = $this->input->post('cat_id');
	//     $product_list = $this->admin_common_model->fetch_condrecord('products', array(
	//         'product_category_id' => $cat_id,
	//         'active_inactive' => 'active'
	//     ));
	//     $data['product_list'] = $product_list;
	//     echo $this->load->view('admin/productlist', $data, true);
	// }
	// function get_cat_discount()
	// {
	//     $discount_cat = $this->input->post('cat_id');
	//     $product_id   = $this->input->post('product_id');
	//     if (!empty($discount_cat) && !empty($product_id)) {
	//         $discountamt = $this->admin_common_model->get_sql_record("select discount_amt from products inner join product_categories ON products.product_category_id=product_categories.id where product_id='" . $product_id . "' and (product_categories.id='" . $discount_cat . "' or product_categories.parent_category='" . $discount_cat . "' ) ");
	//         $total_discount = $discountamt[0]['discount_amt'];
	//         echo $total_discount;
	//     } else if (!empty($discount_cat) && empty($product_id)) {
	//         $discountamt = $this->admin_common_model->fetch_condrecord('product_categories', array(
	//             'id' => $discount_cat
	//         ));
	//         $total_discount = $discountamt[0]['category_discount'];
	//         echo $total_discount;
	//     }
	// }
	// /* function for discount functionality */
	// /* product deletion functionality*/
	// //list of unordered products
	// function unordered_products()
	// {
	//     $sql          = "select product_id,pname,image_url from products";
	//     $all_products = $this->admin_common_model->get_sql_record($sql);
	//     $unordered_pro = array();
	//     if (!empty($all_products)) {
	//         foreach ($all_products as $each_product) {
	//             $where_product = array(
	//                 'product_id' => $each_product['product_id']
	//             );
	//             //cart row
	//             $cart_row = $this->admin_common_model->fetch_condrecord('cart_items', $where_product);
	//             //cart row
	//             //order product
	//             $order_row = $this->admin_common_model->fetch_condrecord('product_order_details', $where_product);
	//             //order product
	//             if (empty($cart_row) && empty($order_row)) {
	//                 //$image_row=$this->admin_common_model->fetch_condrecord('products_images',$where_product);
	//                 $unordered_pro[] = array(
	//                     'product_id' => $each_product['product_id'],
	//                     'product_name' => $each_product['pname'],
	//                     'images' => $each_product['image_url']
	//                 );
	//             }
	//         }
	//     }
	//     $page       = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
	//     $total      = count($unordered_pro); //total items in array
	//     $limit      = 10; //per page
	//     $totalPages = ceil($total / $limit); //calculate total pages
	//     $page       = max($page, 1); //get 1 page when $_GET['page'] <= 0
	//     $page       = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
	//     $offset     = ($page - 1) * $limit;
	//     if ($offset < 0)
	//         $offset = 0;
	//     $unordered_pro = array_slice($unordered_pro, $offset, $limit);
	//     $data['unordered_list'] = $unordered_pro;
	//     $link           = base_url() . 'admin/furnish_dashboard/unordered_products?page=%d';
	//     $pagerContainer = '<div style="width: 300px;">';
	//     if ($totalPages != 0) {
	//         if ($page == 1) {
	//             $pagerContainer .= '';
	//         } else {
	//             $pagerContainer .= sprintf('<a href="' . $link . '" style="color: #c00"> &#171; prev page</a>', $page - 1);
	//         }
	//         $pagerContainer .= ' <span> page <strong>' . $page . '</strong> from ' . $totalPages . '</span>';
	//         if ($page == $totalPages) {
	//             $pagerContainer .= '';
	//         } else {
	//             $pagerContainer .= sprintf('<a href="' . $link . '" style="color: #c00"> next page &#187; </a>', $page + 1);
	//         }
	//     }
	//     $pagerContainer .= '</div>';
	//     $data['pagination'] = $pagerContainer;
	//     admin_view('admin/unordered_product_list', $data);
	// }
	// //list of unorddered products
	// //function for deleting product
	// function product_deletion()
	// {
	//     $product = $this->input->post('productid');
	//     //echo $product;die();
	//     $where_product = array(
	//         'product_id' => $product
	//     );
	//     $this->admin_common_model->delete_data('wishlist', $where_product);
	//     $this->admin_common_model->delete_data('product_story', $where_product);
	//     $this->admin_common_model->delete_data('product_faq', $where_product);
	//     $this->admin_common_model->delete_data('product_dimension_images', $where_product);
	//     $this->admin_common_model->delete_data('products_images', $where_product);
	//     $this->admin_common_model->delete_data('product_attr_price', $where_product);
	//     $this->admin_common_model->delete_data('product_property_rel', $where_product);
	//     $this->admin_common_model->delete_data('compare_product', $where_product);
	//     //$this->admin_common_model->delete_data('product_category_rel',$where_product);
	//     $delete_status = $this->admin_common_model->delete_data('products', $where_product);
	//     if ($delete_status == TRUE) {
	//         echo "1";
	//     } else {
	//         echo "0";
	//     }
	// }
	// function for deleting product

	/**/



}

?>