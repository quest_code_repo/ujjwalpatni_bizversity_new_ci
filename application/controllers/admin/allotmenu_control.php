<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Allotmenu_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper(array('form','url','xcrud'));
		$this->load->model('admin/admin_common_model');
		$this->load->library(array('form_validation','session')); 
	}
	 
	public function view($msg = NULL){
		checkAuth();
		$xcrud = Xcrud::get_instance()->table('admin_menu');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		// $xcrud->where('status','active');

		$xcrud->relation('designation_id','designations','id','name');
		$xcrud->relation('parent_menu','admin_menu','id','label');
        $data['content'] = $xcrud->render();
		$data['title'] ='Menu Control';
		admin_view('admin/allotmenu_list', $data);
    }
	
	
	public function designation($msg = NULL){
		checkAuth();
		$xcrud = Xcrud::get_instance()->table('designations');
		
		/**********view pages***********/
		$xcrud->columns('name');
		$xcrud->order_by('id','desc');
		$xcrud->label('name','Designation Name');
		$xcrud->validation_required(array('name','code'));
		$data['content'] = $xcrud->render();
		$data['title'] ='Designation List';
		admin_view('admin/allotmenu_list', $data);
    }
	public function department($msg = NULL){
		checkAuth();
		$xcrud = Xcrud::get_instance()->table('departments');
		/**********view pages***********/
		$xcrud->columns('name,manager');
		$xcrud->order_by('id','desc');
		$xcrud->label('name','Department Name');
		$xcrud->unset_remove();
		$xcrud->relation('manager','employees','id','name');
		$xcrud->validation_required(array('name','manager'));
		$data['content'] = $xcrud->render();
		$data['title'] ='Department List';
		admin_view('admin/allotmenu_list', $data);
    }
	
	public function statelist($msg = NULL){
		checkAuth();
		$xcrud = Xcrud::get_instance()->table('states');
		//$xcrud->unset_remove();
        /**********view pages***********/
		$xcrud->order_by('id','desc');
        $data['content'] = $xcrud->render();
		$data['title'] ='State List';
		admin_view('admin/allotmenu_list', $data);
    }
	
	public function citylist($msg = NULL){
		checkAuth();
		$xcrud = Xcrud::get_instance()->table('cities');
		//$xcrud->unset_remove();
        /**********view pages***********/
		$xcrud->relation('state_id','states','id','name');
		$xcrud->label('state_id','State Name');
		$xcrud->order_by('id','desc');
        $data['content'] = $xcrud->render();
		$data['title'] ='City List';
		admin_view('admin/allotmenu_list', $data);
    }
	
	public function allot_menu_permission($msg = NULL){
		checkAuth();
		 $data['emp']=$this->admin_common_model->fetch_record('employees');
		 $chk = array('parent_menu' => 0);
		 $data['admin_menu']=$this->admin_common_model->fetch_condrecord('admin_menu',$chk);
		admin_view('admin/allot_menu_permission', $data);
    }
	
	public function menu_sub(){
		 checkAuth();
		 $emp_id=$this->input->post('emp_id');
		 $deletechk = array('emp_id' => $emp_id);
		 $delrs=$this->admin_common_model->delete_data('emp_menu_rel',$deletechk); 
		 $menu=$this->input->post('menu');
		 if(!empty($menu))
		 {
			 foreach($menu as $menu_id)
			 {  
				$chk = array('emp_id' => $emp_id,'menu_id' => $menu_id);
				$check = $this->admin_common_model->fetch_recordbyid('emp_menu_rel',$chk);
				if($check)
				{
					$cid = array('menu_id' => $menu_id);
					$chk = array('id' => $check->id);
					$rs=$this->admin_common_model->update_data('emp_menu_rel',$cid,$chk); 
				}
				else{
					$data = array('emp_id' => $emp_id,'menu_id' => $menu_id);
					$result = $this->admin_common_model->insert_data('emp_menu_rel',$data);
				}
		 	}
				$msg = array(
				'msg' =>'<strong>Succsess!</strong> Menu Alloted!',
				'res' => 1);
				$this->session->set_userdata($msg);
				redirect('admin/allotmenu_control/allot_menu_permission');
    	}
		else{ 
				$msg = array(
				'msg' =>'<strong>Error!</strong> Menu Alloted!',
				'res' => 0);
				$this->session->set_userdata($msg);
				redirect('admin/allotmenu_control/allot_menu_permission');
		}
				
	}
	
	public function loademp_byspoke($sid){
		checkAuth();
		$chk = array('spoke_id' => $sid);
		$check = $this->admin_common_model->fetch_condrecord('employees',$chk);
		if(!empty($check))
		{   ?><option value="">-Select-</option> <?php 
		    foreach ($check as $empinfo)
			{  
		 ?>
            <option value="<?php echo $empinfo['id']; ?>"><?php echo $empinfo['name']; ?></option>
       <?php } 
			
		}
		else {
			?>?><option value="">-No Record Found-</option> <?php 
			}
    }
	
	public function load_menu($eid){
		checkAuth();
		$chk = array('emp_id' => $eid);
		$chk1 = array('parent_menu' => 0,'status'=>'active');
		 $admin_menu=$this->admin_common_model->fetch_condrecord('admin_menu',$chk1);
		$catinfo = $this->admin_common_model->fetch_condrecord('emp_menu_rel',$chk);
		if(!empty($catinfo)) { foreach($catinfo as $catval) { $pro_catrel[]=$catval['menu_id']; } }
		
		?>
        
        <div id="container">
        <?php  foreach ($admin_menu as $menu)
			{  
		 ?>
        <div class="innerBoxes">
          <strong><?php echo $menu['label']; ?></strong><br />
          <?php
           $admin_menu1 = $this->load_menu_child($menu['id']);  ?>
           <?php 
		   if(!empty($admin_menu1)) {
            foreach ($admin_menu1 as $menu)
			{  
		 ?>
         &nbsp; &nbsp;  <input <?php if(!empty($pro_catrel)) { if(in_array($menu['id'], $pro_catrel)) { echo "checked='checked'"; } } ?> type="checkbox"  name="menu[]" id="menu[]" value="<?php echo $menu['id']; ?>" /> <?php echo $menu['label']; ?>  <br /> 
        <?php  } }  ?>
          
          </div>
        <?php  } ?>
</div>

        <?php   
    }
	
	function load_menu_child($id)
	{ 	
		$chk = array('parent_menu' => $id,'status'=>'active');
		$admin_menu=$this->admin_common_model->fetch_condrecord('admin_menu',$chk);
		return $admin_menu;
	}

	public function front_menu($msg = NULL){
		checkAuth();
		// Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/../wow/xcrud/plugins/ckeditor/ckeditor.js';
		$xcrud = Xcrud::get_instance()->table('main_menu');
        /**********view pages***********/
        $data['content'] = $xcrud->render();
		$data['title'] ='Menu List';
		admin_view('admin/allotmenu_list', $data);
    }

    public function footer($msg = NULL){
		checkAuth();
		Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/../wow/xcrud/plugins/ckeditor/ckeditor.js';
		$xcrud = Xcrud::get_instance()->table('footer');
        /**********view pages***********/
        $data['content'] = $xcrud->render();
		$data['title'] ='Footer';
		admin_view('admin/allotmenu_list', $data);
    }

}