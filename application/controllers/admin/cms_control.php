<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('advertisement');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		$xcrud->unset_remove(); 
		$xcrud->table_name('');
		$xcrud->relation('category_name','product_categories','id','name');
		$xcrud->relation('created_by','employees','id','name');
		$xcrud->change_type('image_url', 'image', false, array(
        'width' => 450,
        'path' => '../uploads/advertisement',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
        $data['content'] = $xcrud->render();
		$data['title'] ='Advertisement List';
		admin_view('admin/allotmenu_list', $data);
    }

    public function blogs($msg = NULL)
    {
        // Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/../xcrud/plugins/ckeditor/ckeditor.js';
        $xcrud = Xcrud::get_instance()->table('blog');
        // $comments = $xcrud->nested_table('comments','id','comments','post_id');
        // $comments->relation('post_id','posts','id','title');
        // $comments->label('post_id','Post');
        $xcrud->pass_var('created_date',time());
        // $xcrud->pass_var('created_by', 1);
        $xcrud->fields('blog_title,blog_desc,image_url,status');
        /**********view pages***********/
        $xcrud->order_by('id','desc');
        $xcrud->change_type('image_url', 'image', '', array(
        'width' => 450,
        'path' => '../uploads/blogs',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
        $data['content'] = $xcrud->render();
        $data['title'] ='Blog List';
        admin_view('admin/allotmenu_list', $data);
    }

    public function mentoring_criteria()
    {
        $xcrud = Xcrud::get_instance()->table('mentoring_criteria');
        $xcrud->pass_var('created_date',time());
        // $xcrud->pass_var('author', 1);
        $xcrud->fields('title,full_desc,status');
        $xcrud->after_insert('make_url');
       // $xcrud->validation_required(array('title' , 'full_desc','image_url'));
        /**********view pages***********/
        $xcrud->order_by('id','desc');
        // $xcrud->change_type('image_url', 'image', '', array(
        // 'width' => 450,
        // 'path' => '../uploads/mentoring_page',
        // 'thumbs' => array(array(
        //         'height' => 55,
        //         'width' => 120,
        //         'crop' => true,
        //         'marker' => '_th'))));
        $data['content'] = $xcrud->render();
        $data['title'] ='Mentoring Topic`s List';
        admin_view('admin/allotmenu_list', $data);
    }



    public function mentoring_quote()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_mentoring_quote');
        /**********view pages***********/
        $xcrud->order_by('quote_id','desc');
        $xcrud->unset_add();

        $data['content'] = $xcrud->render();
        $data['title'] ='Mentoring Quote';
        admin_view('admin/allotmenu_list', $data);
    }

   public function mentoring_explored_content(){
       
        $xcrud = Xcrud::get_instance()->table('tbl_mentoring_explored_content');
        
        $xcrud->unset_print();
        $xcrud->unset_csv();
        //$xcrud->unset_add();
        $xcrud->order_by('mentoring_content_id','ASC');

        // $cond = array('status' => 'active');
        // $xcrud->where($cond);

        $xcrud->change_type('icon', 'image' , '', array('width' => 300, 'height' => 200));
        $xcrud->label('mentoring_id','Mentoring Category');
        $xcrud->columns(array('content','mentoring_id','icon','status'));
        $xcrud->validation_required(array('content','mentoring_id','icon'));
        $xcrud->relation('mentoring_id','mentoring_criteria','id','title','');

        $data['content'] = $xcrud->render();
        $data['title'] ='Mentoring Explored Content';
        admin_view('admin/allotmenu_list', $data);

    } 
    

    public function social_link()
    {
        $xcrud = Xcrud::get_instance()->table('social_links');
        $data['content'] = $xcrud->render();
        $data['title'] ='Social Links';
        admin_view('admin/allotmenu_list', $data);
    }

    public function coupons()
    {
        $xcrud = Xcrud::get_instance()->table('coupons');
        $data['content'] = $xcrud->render();
        $data['title'] ='Coupon List';
        admin_view('admin/allotmenu_list', $data);
    }

    public function product_faq()
    {
        $xcrud = Xcrud::get_instance()->table('product_faq');
        $xcrud->relation('product_id','products','product_id','pname');
        $xcrud->label('product_id', 'Product Name');
        $xcrud->unset_add();
        $xcrud->unset_remove();

        $data['content'] = $xcrud->render();
        $data['title'] ='Product FAQ';
        admin_view('admin/allotmenu_list', $data);
    }


    	public function social_media_subscriber()
    	{
    		    $xcrud = Xcrud::get_instance()->table('social_media_subscribers');
        /**********view pages***********/
		        $xcrud->order_by('social_sub_id','desc');
		        $xcrud->unset_add();
		        $xcrud->unset_remove();

		        $data['content'] = $xcrud->render();
		        $data['title'] ='Social Media Subscriber';
		        admin_view('admin/allotmenu_list', $data);
    	}


         public function mentoring_proposal()
        {
             $data['all_data'] =$this->admin_common_model->getwheres('mentoring_proposal',array());
           admin_view('admin/mentoring_proposal/mentoring_proposal_content',$data);

        }

        public function add_mentoring_proposal()
        {
            $data['MentoringCriteria'] = $this->admin_common_model->getwheres('mentoring_criteria',array('status'=>'active'));

            admin_view('admin/mentoring_proposal/add_mentoring_proposal',$data);
        }

         public function new_proposal()
    {
        $postData = $this->input->post();


        if(!empty($postData))
        {
            $mentoring_name = $postData['proposal_name'];
            $above_category = $postData['above_category'];
            
            $config = array(
                'upload_path' => "./uploads/mentoring_proposal",
                'allowed_types' => "*",
                'overwrite' => false,
                'file_name' => time()
            );
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('mat_image'))
            {
                $uploadimg = $this->upload->data();
                $uimg = $uploadimg['file_name'];
            }
            else
            {
                $uimg = '';
            }

            $add_cat = array(
                'mentoring_proposal_name' => $mentoring_name,
                'mentoring_proposal_for' => $above_category,
                'mentoring_proposal_url' => $uimg
                
            );
            $category_id = $this->admin_common_model->insert_data('mentoring_proposal', $add_cat);
            
                redirect('admin/cms_control/mentoring_proposal');
        }
    }

    public function delete_mentoring_proposal($id="")
    {
            if(!empty($id))
            {
                $this->db->where('mentoring_proposal_id',$id);
                $this->db->delete('mentoring_proposal');
                redirect('admin/cms_control/mentoring_proposal','refresh');
            }
    }

       


	
}