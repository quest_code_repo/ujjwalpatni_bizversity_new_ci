<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('banner');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		$xcrud->columns('name,image_url,status,name,created_by');
		$xcrud->relation('created_by','employees','id','name');
		$xcrud->change_type('image_url', 'image', false, array(
        'width' => 700,
        'path' => '../uploads/banner',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
        $data['content'] = $xcrud->render();
		$data['title'] ='Banner List';
		admin_view('admin/brand_list', $data);
    }
	
	public function front_banner($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('banner');
        /**********view pages***********/
		$xcrud->order_by('id','asc');
		$xcrud->columns('name,image_url,link_url,alt_tag,status');
		$xcrud->fields('name,image_url,link_url,alt_tag,status');
		$xcrud->unset_print();
		$xcrud->unset_csv();
		$xcrud->label('alt_tag','Button Name');
		$xcrud->change_type('image_url', 'image', false, array(
        'width' => 1200,
        'path' => '../uploads/banner',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
        $data['content'] = $xcrud->render();
		$data['title'] ='Banner List';
		admin_view('admin/brand_list', $data);
    }
	
}