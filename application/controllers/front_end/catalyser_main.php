<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalyser_main extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper(array('url','html','form')); 
		$this->load->helper('cookie');
		$this->load->model('front_end/cat_frontend');
	}

	// public function index()
	// {
	// 	$this->load->view('welcome_message');
	// }
	// public function courses_and_pricing_description()
	// {
	// 	load_front_view('courses_and_pricing_description');
	// }

	// public function signup()
	// {		
	// 	//$this->load->view('front_end/signup_header_footer/header');
	// 	load_front_view('front_end/signup');
	// 	//$this->load->view('front_end/signup_header_footer/footer');
	// }

	public function login()
	{

		if($_POST){

				$roll_no = $this->input->post('username');
				$password = $this->input->post('login_password');
				$user_id = $this->session->userdata('user_id');

				$data = array(
					'roll_number LIKE'=>$roll_no,
					'password' => $password,
					'status' => 'active'
					);

				$row = $this->cat_frontend->getSingle('tbl_students', $data);
				// echo "<pre>";print_r($row);die;
				//echo $this->db->last_query();die;
				if($row)
				{
					if ($row[0]['role'] == 'Student')
					{	
						$user_details = array(
							'id' => $row[0]['id'] ,
							'role' => $row[0]['role'] ,
							'username' => $row[0]['username'] ,
							'full_name' => $row[0]['full_name'],
							'mobile' => $row[0]['mobile'],
							'user_class' => @$row[0]['class_name'],
							'validated' => true
							);
						$this->session->set_userdata($user_details);
						// print_r($this->session->userdata('user_class'));die;

						redirect('test_dashboard');
						
					}elseif($row[0]['role'] == 'Teacher') {

						echo 'Success Teacher';

						$user_details = array(
						'id' => $row[0]['id'] ,
						'role' => $row[0]['role'] ,
						'username' => $row[0]['username'] ,
						'full_name' => $row[0]['full_name'],
						'mobile' => $row[0]['mobile'],				
						'validated' => true
						);			

						$this->session->set_userdata($user_details);
						session_write_close();
					}

					$memberid = $this->session->userdata('id');

					$cartdata = array('user_id' => $memberid);

		            $cartid = array('user_id' => $this->session->userdata('session_id'));            
		            $this->cat_frontend->update_data('cart_items',$cartdata,$cartid);

				}   
				else 
				{
					
					$this->session->set_flashdata('login_fail', 'Invalid roll no. or password.Try Again');
					redirect('login');
				}

		}
	
		load_front_view('front_end/login');
	}

	// /**********Forgot Password**********/


 //  	public function forgot_pass()
 //  	{

	// 	$chk = array(
	// 		'username' => $this->input->post('username'),
	// 		'status' => 'active'
	// 	);
	// 	$check = $this->cat_frontend->fetch_recordbyid('tbl_students',$chk);

			
	// 	if(!empty($check))
	// 	{
			
	// 		$length = 8;
	// 		$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	// 		$ids=$check->id;
	// 		$cid = array(
	// 		'id' => $ids
	// 		);
	// 		$newpass = array(
	// 		'password' => md5($randomString)
	// 		);
	// 		$rs=$this->cat_frontend->update_data('tbl_students',$newpass,$cid);

	// 		$msg = array(
	// 		'msg' =>'<strong>Success!</strong> Your New password has been sent to your inbox or spam',
	// 		'res' => 1);
	// 		$this->session->set_userdata($msg);


	// 		$to = $this->input->post('username');

	// 		$headers="From:worthfcfc321@gmail.com";
	// 		$subject = 'New Updated Password Of The Catalyser';
	// 		$message = ('User Name :'
	// 		.$this->input->post('username').' <br> Your Password :'.$randomString);

	// 		$result = $this->cat_frontend->sendmail('worthfcfc321@gmail.com','worthfcfc123','Builder Admin',$to,$subject,$message,$headers);

	// 				//echo "SUCCESS";	
	// 		$this->session->set_flashdata('message', 'Your updated password has been sent to your email');
	// 		redirect("forgot_password");	

	// 	}else{		
	// 		$this->session->set_flashdata('error', 'Invalid Username');
	// 		redirect("forgot_password");	 
	// 	}

			

	// }

	// public function forgot_pwd()
	// {
	// 	load_front_view('front_end/forgot_pass');
	// }


	// public function registration()
	// {
	// 	$email = $this->input->post('email');
	// 	$pass=md5($this->input->post('password'));
	//     $full_name=$this->input->post('fullname');
	//     $mobile_no=$this->input->post('mobile_no');
	//     $class_name=$this->input->post('class_name');

	// 	$where = array('username'=> $email);
	// 	$data['check_email_exist']= $this->cat_frontend->fetch_recordbyid('tbl_students',$where);

	// 	if($data['check_email_exist']){
	// 		echo "email_exist";
	// 	}
	// 	else{

	// 		// $string = '0123456789';
 //       		// $string_shuffled = str_shuffle($string);
 //       		// $otp = substr($string_shuffled, 1, 4);
	// 		$otp=rand(1000,9999);

	// 		$data = array (
	// 				'role' => 'Student',
	// 				'username' => $email,
	// 			 	'password' => $pass,
	// 			 	'full_name' => $full_name,
	// 			 	'image_name' => '',
	// 			 	'mobile' => $mobile_no,
	// 			 	'otp' => $otp,
	// 			 	'class_name' => $class_name,
	// 				'created_at' => date('Y-m-d H:i:s'),
	// 			 	'status' => 'inactive'
	// 			 );


	// 	$result = $this->cat_frontend->insert_data('tbl_students',$data);

	// 		if($result)
	// 		{
				
	// 			$message = '<p>Dear Customer Your OTP ,</p>'. $otp ;
	// 			$message .= '<p>Thank you for register in Catalyser. Please varify your account with the above OTP.Thanks</p>';
	// 			$message .='Regards <br/>Team Catalyser';
	// 			$to = $email;
	// 			$headers="From:worthfcfc321@gmail.com";
	// 			$headers .= "MIME-Version: 1.0\r\n";
	// 			$headers .= "Content-Type: text/html; charset=iso-8859-1\n";

	// 			$subject = 'Catalyser OTP Varification';

	// 			$result = $this->cat_frontend->sendmail('worthfcfc321@gmail.com','worthfcfc123','Builder Admin',$to,$subject,$message, $headers);

	// 		}
			
	// 	}
	// 	die;

	// }

	// public function varify_registration()
	// {

	// 	if (!empty($_POST))
	// 	{

	// 		$otp =$this->input->post('OTP');
	// 		$email =$this->input->post('user_name');
	// 		$password =md5($this->input->post('password'));

	// 		$where = array('username'=> $email,'otp'=>$otp);
	// 		$check_otp= $this->cat_frontend->fetch_recordbyid('tbl_students',$where);

	// 		if(empty($check_otp))
	// 		{
				
	// 			echo "invalid";
	// 		}
	// 		else
	// 		{
	// 		$cartdata = array('status' => 'active','otp' => '');

 //            $cartid = array('status' => 'inactive','username'=> $email);            
 //            $this->cat_frontend->update_data('tbl_students',$cartdata,$cartid);

			 
	// 				$pass = $this->input->post('password');
	// 				$message = '<p>Thank you for joining Catalyser.Your account details are  <br/><br/>  <b>Email:-' . $email . '</b><br/><b>Password:-</b>'.$pass .'</p>';
	// 				$message .='Regards <br/>Team Catalyser';
	// 				$to = $email;
	// 				$headers="From:worthfcfc321@gmail.com";
	// 				$headers .= "MIME-Version: 1.0\r\n";
	// 				$headers .= "Content-Type: text/html; charset=iso-8859-1\n";

	// 				$subject = 'Catalyser Registraion Success';

	// 				$result = $this->cat_frontend->sendmail('worthfcfc321@gmail.com','worthfcfc123','Team Catalyser',$to,$subject,$message, $headers);

	        
	// 				$data = array(
	// 					'username' => $email,
	// 					'password' => $password,
	// 					'status' => 'active'
	// 					);

	// 				$row = $this->cat_frontend->getSingle('tbl_students', $data);
					
	// 				if($row)
	// 				{
	// 					$user_details = array(
	// 						'id' => $row[0]['id'] ,
	// 						'role' => $row[0]['role'] ,
	// 						'username' => $row[0]['username'] ,
	// 						'full_name' => $row[0]['full_name'],
	// 						'mobile' => $row[0]['mobile'],				
	// 						'validated' => true
	// 						);			

	// 					$this->session->set_userdata($user_details);
	// 					session_write_close();
					
	// 				}

	// 			// }
	// 		}
	// 		die;

	// 	}
	// }

	//    /*****************Login By Students***********/
	public function user_login()
	{
		$roll_no = $this->input->post('username');
		$password = $this->input->post('login_password');
		$user_id = $this->session->userdata('user_id');
		$data = array(
			'roll_number' => $roll_no,
			'password' => $password,
			'status' => 'active'
			);
		$row = $this->cat_frontend->getSingle('tbl_students', $data);
		if($row)
		{
			if ($row[0]['role'] == 'Student')
			{	
			// print_r($row);die;			
				$user_details = array(
					'id' => $row[0]['id'] ,
					'role' => $row[0]['role'] ,
					'username' => $row[0]['username'] ,
					'full_name' => $row[0]['full_name'],
					'mobile' => $row[0]['mobile'],
					'user_class' => @$row[0]['class_name'],
					'validated' => true
					);
				$this->session->set_userdata($user_details);
				// echo json_encode($user_details);
				echo 'Success Student';
				// $sql="select `tbl_semester`.* from `tbl_semester` join `product_orders` ON `product_orders`.`order_products`= `tbl_semester`.`sem_id` WHERE `product_orders`.`order_user_id`='".$row[0]['id']."' and  `product_orders`.`order_status`='completed' ";
				// $sql="select `tbl_semester`.* from `tbl_semester` ";
				// // die;and `product_orders`.`end_date` >= CURDATE() 
				// $data['student_sem'] = $this->cat_frontend->get_sql_record($sql);

				// if(!empty($data['student_sem']))
				// {
				// 	if($this->input->get('sem')){
				// 		$mysem=base64_decode($this->input->get('sem'));
				// 		foreach($data['student_sem'] as $semm){
				// 			$sem_id[]=$semm['sem_id'];
				// 		}
				// 		// print_r($sem_id);
				// 		$get_refferer=$this->session->userdata('refferer');
				// 		if (strpos($get_refferer, '/friendship_index') !== false) {
				// 		    echo 'true';
				// 			redirect('entrance/?sem_id=0', 'refresh');
				// 		}
				// 		if(!in_array($mysem, $sem_id)){
				// 			redirect('dashboard', 'refresh');
				// 		}
						
				// 	}
				// 	// echo $data['student_sem'][0]['sem_id'];
				// 	$this->session->set_userdata(array('first_sem' => $data['student_sem'][0]['sem_id'] ));
				// }
				// session_write_close();
			}elseif($row[0]['role'] == 'Teacher') {

				echo 'Success Teacher';

				$user_details = array(
				'id' => $row[0]['id'] ,
				'role' => $row[0]['role'] ,
				'username' => $row[0]['username'] ,
				'full_name' => $row[0]['full_name'],
				'mobile' => $row[0]['mobile'],				
				'validated' => true
				);			

				$this->session->set_userdata($user_details);
				session_write_close();
			}

			$memberid = $this->session->userdata('id');

			$cartdata = array('user_id' => $memberid);

            $cartid = array('user_id' => $this->session->userdata('session_id'));            
            $this->cat_frontend->update_data('cart_items',$cartdata,$cartid);


		} 
		else 
		{
			echo 'fail';
		}
    }


	// public function add_to_cart()
	// {
	// 	if ($_POST) 
	// 	{
	// 		$sem_id = $this->input->post('sem_id');
	// 		if($this->session->userdata('id'))
	// 		{
	// 			$user_id = $this->session->userdata('id');
	// 		} else {
	// 			$user_id = $this->session->userdata('session_id');
	// 		}
	// 		$already_have = array('order_user_id' => $user_id,'order_products'=>$sem_id,'order_status'=>'Completed');
	// 		$registerd_already = $this->cat_frontend->getwheres('product_orders',$already_have);
	// 		$reapply_data = 	date("Y:m:d g:i a");
	// 		$check_validate = $registerd_already[0]['end_date'];
	// 		if(!($reapply_data > $check_validate))
	// 		{ 
	// 			echo "already purchased";
	// 			exit;
	// 		}else{
	// 			$where = array('sem_id' => $sem_id);
	// 			$sem_details = $this->cat_frontend->getwheres('tbl_semester',$where);			
	// 			$actual_price = $sem_details[0]['sem_price'];
	// 			$sem_duration = $sem_details[0]['sem_duration'];
	// 			$add_cart = array(
	// 				'user_id'=>$user_id,
	// 				'sem_id'=>$sem_id,
	// 				'p_quantity'=>'1',
	// 				'sem_duration'=>$sem_duration,
	// 				'actual_price'=>$actual_price,
	// 				'created_at'=>date("Y-m-d H:i:s"),
	// 				'status'=>"active"	    
	// 				);
	// 		}

	// 		$cart_where = array('user_id' => $user_id);
	// 		$data['data_cart_items']=$this->cat_frontend->getwheres('cart_items',$cart_where);

	// 		if($data['data_cart_items']){
	// 			$cart_delete = array('user_id' => $user_id,'sem_id'=>$sem_id);
	// 			$remove_semester = $this->cat_frontend->delete('cart_items',$cart_delete);
	// 		}
	// 		$add_semester = $this->cat_frontend->insert_data('cart_items',$add_cart);

	// 		if($add_semester)
	// 		{
	// 			echo "added";
	// 		}
	// 	}
	// }

	// public function remove_sem()
	// {
	//     if ($_POST) {
	//     	$sem_id = $this->input->post('sem_id');
	//     	if($this->session->userdata('id'))
	//     	{
	// 			$user_id = $this->session->userdata('id');
	// 		}else{
	// 			$user_id = $this->session->userdata('session_id');
	// 		}
		   
	// 		$cart_delete = array('user_id' => $user_id,'sem_id'=>$sem_id);
	// 	    $remove_semester = $this->cat_frontend->delete('cart_items',$cart_delete);

	// 	    if($remove_semester)
	// 	    {echo "remove";}
		
	// 	}
	// }


	// public function get_cart_details()
	// {
	// 	if($this->session->userdata('id')) {

	// 		$user_id = $this->session->userdata('id');

	// 		} else {

	// 			$user_id = $this->session->userdata('session_id');
	// 		}

	// 	$cart_wheres = array('user_id' => $user_id);
	//     $data_cart_items_total=$this->cat_frontend->get_data_twotable_column_where('tbl_semester','cart_items','sem_id','sem_id','',$cart_wheres,'');
	//     $data['data_cart_items_total'] = $data_cart_items_total;
	//     $data['num_rows'] = count( $data_cart_items_total);

 //        $sum_total=0;
	// 	foreach ($data_cart_items_total as $subtotal){

	// 		$sem_price = $subtotal['sem_price'];
	// 		$total = (float)$sem_price;
	// 		$sum_total+= $total;
	// 	}

	// 	$data['subtotal'] = $sum_total;

	//     $this->load->view('template/payment_info',$data);


	// }

	// function coupon_code()
	// {
	// 	$coupon = $this->input->post('applied_code');
	// 	$amt = $this->input->post('code_value');
	// 	$total_amt = base64_decode(base64_decode(base64_decode($amt)));
		

	// 	if($this->session->userdata('id')){

	// 		$user_id = $this->session->userdata('username');
	// 	}
	// 	else{

	// 		$user_id = $this->session->userdata('session_id');
	// 	}

	// 	//$email=$this->input->post('user_email');

	// 	$date = date('Y-m-d h:i:s');
	// 	$where = array(
	// 		'code' => $coupon
	// 		);
	// 	$res = $this->cat_frontend->getSingle('coupons', $where);

	// 	//$coupon_id = $res[0]['id'];

	// 	//$this->home->insert_data('coupon_users',$coupon_user);
		

	// 	if($res)
	// 	{
	// 		if(($date > $res[0]['valid_start']) && ($date < $res[0]['valid_end']))
	// 		{

				
	// 			$coupon_user=array('user_email'=>$user_id,'coupon_id'=>$res[0]['id']);

	// 			$coupon_row=$this->cat_frontend->getSingle('coupon_users',array('user_email'=>$user_id,'coupon_id'=>$res[0]['id']));
		
						

	// 			if(empty($coupon_row))
	// 			{
									
	// 				$this->cat_frontend->insert_data('coupon_users',$coupon_user);

	// 				$discount_ammout = $res[0]['discount'];
	// 				$discount_type = $res[0]['discount_type'];
											
	// 				$data['status'] = 1;
	// 				$data['msg'] = 'Coupon Applied Succesfully';
	// 				$data['code_id']=base64_encode($res[0]['id']);
	// 				$data['discount'] = $res[0]['discount'];		
	// 				$data['discount_type'] = $res[0]['discount_type'];	

	// 				$this->session->set_userdata($coupon_user);
	// 				session_write_close();
	// 			}

	// 			else
	// 			{

					
	// 				$data['status'] = 0;
	// 			   	$data['msg'] = 'You have already used it.';
	// 				$data['discount'] = 0;
	// 				$data['discount_type'] = '';

	// 			}
						
	// 		}
	// 		else
	// 		{
	// 			$data['status'] = 0;
	// 			$data['msg'] = 'Coupon Code Expired';
	// 			$data['discount'] = 0;
	// 			$data['discount_type'] = '';
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$data['status'] = 0;
	// 		$data['msg'] = 'Invalid Coupon Code';
	// 		$data['discount'] = 0;
	// 		$data['discount_type'] = '';
	// 	}
	// 	echo json_encode($data);
	// }

	// public function remove_coupons()
	// {

	// 	$coupon_id = $this->input->post('applied_code');
	// 	$cid = base64_decode($coupon_id);


	// 	if($this->session->userdata('id')) {

	// 		$user_id = $this->session->userdata('id');

	// 	} else {

	// 		$user_id = $this->session->userdata('session_id');
	// 	}
	// 	$where = array('id'=>$user_id);
	// 	$coupons_useremail= $this->cat_frontend->getwheres('tbl_students',$where);
	// 	$user_name = $coupons_useremail[0]['username'];


	// 	$coupons_get= $this->cat_frontend->getwheres('coupon_users',array("user_email"=>$user_name,'coupon_id'=>$cid));
	// 	if(!empty($coupons_get))
	// 	{

	// 		$coupn_id = $coupons_get[0]['coupon_id'];
	// 		$coupons_get_amt= $this->cat_frontend->getwheres('coupons',array('id'=>$coupn_id));
	// 		if(!empty($coupons_get_amt))
	// 		{
	// 			$remove_coupon = array("user_email"=>$user_name,'coupon_id'=>$cid);
	// 			$removed_coupon = $this->cat_frontend->delete('coupon_users',$remove_coupon);
	// 			if($removed_coupon)
	// 			{

	// 				$discount_ammout = $coupons_get_amt[0]['discount'];
	// 				$discount_type = $coupons_get_amt[0]['discount_type'];
								
	// 				$data['status'] = 1;
	// 				$data['msg'] = 'Coupon Removed Succesfully';
	// 				//$data['code_id']=base64_encode($coupons_get_amt[0]['id']);
	// 				$data['discount'] = $coupons_get_amt[0]['discount'];		
	// 				$data['discount_type'] = $coupons_get_amt[0]['discount_type'];
	// 			}

	// 		}

	// 		echo json_encode($data);
	// 	}

	// }


	// public function checkout()
	// {
	// 	$user_id = $this->session->userdata('id');
	// 	$cart_data= $this->cat_frontend->getwheres('cart_items',array("user_id"=>$user_id));
	// 	if(!empty($cart_data))
	// 	{

	// 		$data['country_list'] = $this->cat_frontend->getdata('tbl_country');
	// 		$data['state_list'] = $this->cat_frontend->getwhere('tbl_state',array('cid'=>101));

	// 		$where = array('id'=>$this->session->userdata('id'));
	// 		$data['user_point'] = $this->cat_frontend->getwheres('tbl_students',$where);

	// 		if($this->session->userdata('id')) {

	// 			$user_id = $this->session->userdata('id');

	// 		} else {

	// 			$user_id = $this->session->userdata('session_id');
	// 		}

	// 		$order_ammout = $this->input->post('coupen_encrypted');
	// 		$order_ammount_decrypt = base64_decode(base64_decode(base64_decode(base64_decode($order_ammout))));

	// 		$coupon_id = $this->input->post('ccode_id');			
	// 		$order_cid_decrypt = base64_decode(($coupon_id));
	// 		$data['coupon_apply_id'] = $coupon_id;

	// 		$where_ccid = array('id'=>$order_cid_decrypt);
	// 		$coupon_details = $this->cat_frontend->getwheres('coupons',$where_ccid);
	// 		if($coupon_details)
	// 		{

	// 			$data['dis_type'] = $coupon_details[0]['discount_type'];
	// 			$data['dis_amt'] = $coupon_details[0]['discount'];
	// 			$latest_amount = $order_ammount_decrypt;

	// 			if($data['dis_type']=='flat')
	// 			{			      		

	// 				$data['sum']= $latest_amount-$data['dis_amt'];

	// 				$data['coupon_discounted'] = $data['dis_amt'];
	// 			}
	// 			else
	// 			{	
	// 				$sum_amt = $latest_amount*$data['dis_amt'];
	// 				$data['sum_latest'] = $sum_amt/100;
	// 				$data['sum'] = $latest_amount - $data['sum_latest'];

	// 				$data['coupon_discounted'] = $data['sum_latest'];
	// 			}

	// 		}
	// 		else{

	// 			$data['sum']=  $order_ammount_decrypt;
	// 			$data['coupon_discounted'] = 0;

	// 		}
	// 		$cart_data=$this->cat_frontend->get_data_twotable_column_where('cart_items','tbl_semester','sem_id','sem_id','',array('user_id' => $user_id),'cart_items.id');
	// 		$car_img_data = array();
	// 		//  if(!empty($cart_data))
	// 		//  {


	// 		// 	$product_imgs=$this->cat_frontend->get_where_order_group('tbl_semester',array('sem_id'=>$each_cart['sem_id']), $limit='', $orderby='','');

	// 		//  $each_cart['images']=$product_imgs[0]['image_url'];


	// 		// }

	// 		$data['checkout'] =$cart_data;            
	// 		$data['status'] = 1;
	// 		$billing_address = array('user_id'=>$user_id);
	// 		$data['billing_address']= $this->cat_frontend->getwheres_orderby('order_payment_details',$billing_address,'payment_detail_id');

	// 		load_front_view('template/checkout',$data);
	// 	}
	// 	else{

	// 		echo "Sorry No Item In Your Cart For Checkout";
	// 	}
	// }


	// public function place_order(){
	// 	if($this->session->userdata('id')){
	// 		if($_POST){
	// 			$user_id = $this->session->userdata('id');
	// 			$order_ammout = $this->input->post('coupen_encrypted');
	// 			$order_ammount_decrypt = base64_decode(base64_decode(base64_decode(base64_decode($order_ammout))));

	// 			$coupon_id = $this->input->post('ccode_id');
	// 			$coupon_id_decyrpt = base64_decode($coupon_id);		
	// 			$cart_data= $this->cat_frontend->getwheres('cart_items',array("user_id"=>$user_id));
	// 			if(!empty($cart_data)){
	// 				foreach ($cart_data as $sem_name) {
	// 					$sem_id = $sem_name['sem_id'];
	// 					$sem_duration = $sem_name['sem_duration'];
	// 					$ord_code='ORDER-'.time();
	// 					$ord_status='Pending';
	// 					$ord_date=date('Y-m-d g:i a'); 
	// 					$end_data=Date('Y:m:d g:i a', strtotime("+".$sem_duration." days"));
	// 					$ord_data=array(
	// 						'order_code'=>$ord_code,
	// 						'order_status'=>$ord_status,
	// 						'order_user_id'=>$user_id,
	// 						'create_date'=>$ord_date,
	// 						'end_date'=>$end_data,
	// 						'order_amount'=>$order_ammount_decrypt,
	// 						'order_products'=>$sem_id,
	// 						'coupon_id'=>$coupon_id_decyrpt
	// 						);
	// 					$order_id=$this->cat_frontend->insert_data('product_orders',$ord_data);
	// 				}
	// 			}
	// 			$first_name = $this->input->post('first_name');
	// 			$last_name = $this->input->post('last_name');
	// 			$email_id = $this->input->post('email_id');
	// 			$mobile = $this->input->post('mobile');
	// 			$school = $this->input->post('school_name');
	// 			$country = $this->input->post('country');
	// 			$state = $this->input->post('state');
	// 			$city_name = $this->input->post('city_name');
	// 			$address = $this->input->post('address');
	// 			$address1 = $this->input->post('address1');
	// 			$pincode = $this->input->post('pincode');

	// 			$billing_data = array(
	// 				'order_id' => $ord_code, 
	// 				'user_id'=>$user_id,
	// 				'billing_first_name' => $first_name, 
	// 				'billing_last_name' => $last_name, 
	// 				'billing_emailid' => $email_id, 
	// 				'billing_mobile' => $mobile, 
	// 				'billing_amount' => $order_ammount_decrypt, 
	// 				'created_on' =>date('Y-m-d g:i a')
	// 				);

	// 			$res = $this->cat_frontend->insert_data('order_payment_details',$billing_data);		
	// 		}

	// 		$where_amount = array('order_user_id'=>$this->session->userdata('id'),'product_order_id'=>$order_id );
	// 		$gateway_amount = $this->cat_frontend->getwheres('product_orders',$where_amount);

	// 		if(!empty($gateway_amount))
	// 		{
	// 			$coupon_id = $gateway_amount[0]['coupon_id'];
	// 			if($coupon_id!=0)
	// 			{
	// 				$where_ccid = array('id'=>$coupon_id);
	// 				$coupon_details = $this->cat_frontend->getwheres('coupons',$where_ccid);
	// 				if($coupon_details)
	// 				{
	// 					$dis_type = $coupon_details[0]['discount_type'];
	// 					$dis_amt = $coupon_details[0]['discount'];
	// 					$latest_amount = $gateway_amount[0]['order_amount'];

	// 					if($dis_type=='flat')
	// 					{
	// 						$sum = $latest_amount-$dis_amt;
	// 					}
	// 					else
	// 					{

	// 						$sum_amt = $latest_amount*$dis_amt;
	// 						$sum_latest = $sum_amt/100;
	// 						$sum = $latest_amount - $sum_latest;
	// 					}
	// 				}

	// 			}
	// 			else{
	// 				$sum = $gateway_amount[0]['order_amount'];
	// 			}
	// 		}

	// 		$data['amount']=$sum;
	// 		$data['user_code']=$this->session->userdata('id');
	// 		//$data['productinfo']=$order_products;
	// 		$data['order_id']=$ord_code;
	// 		$data['billing_info']=$_POST; 

	// 		load_front_view('template/atomtech_request',$data);
	// 	}else
	// 	{
	// 		$viewcart_url = base_url();  
	// 		header("Location:" .$viewcart_url.'view-cart');
	// 	}
	// }


	// // public function atomtech_request(){

	// // 	load_view('atomtech_request');
	// // }

	// public function atomtech_response()
	// {
	// 	$user_pay = $_POST['clientcode'];
	// 	$user_ordercode = $_POST['udf9'];
	// 	$user_id = base64_decode($user_pay);
	// 	if($_POST['f_code']=="Ok") 
	// 	{
	// 		$this->session->set_flashdata("success","Congratulations you have enrolled Successfully below are the your order details");
	// 		redirect('front_end/catalyser_main/success_order/'.$user_ordercode);
	// 	}
	// 	else{
	// 		$this->session->set_flashdata('error', 'Your Order Has Been Declined');
	// 		redirect('front_end/catalyser_main/cancel_order/'.$user_ordercode);
	// 	}
	// }


	// public function success_order($user_ordercode)
	// {
	// 	$user_id = $this->session->userdata('id');
	// 	$empty_cart = array('user_id' => $user_id);
	// 	$remove_cart_service = $this->cat_frontend->delete('cart_items',$empty_cart);
	// 	$data = array(
	// 	'order_status' => 'completed', 
	// 	);
	// 	$where = array('order_user_id' => $user_id,'order_code'=> $user_ordercode);
	// 	$con = $this->cat_frontend->update_data('product_orders',$data,$where);
	// 	$where_myorder = array('order_user_id' => $user_id,'order_code'=> $user_ordercode,'order_status'=>'completed');
	// 	$purchase_pid = $this->cat_frontend->getwheres('product_orders',$where_myorder);
	// 	$data['gateway_return_orders'] = $purchase_pid;
	// 	$data['ordered_coupon'] =  $data['gateway_return_orders'][0]['coupon_id'];
	// 	if($data['ordered_coupon']!=0)
	// 	{
	// 		$where_ccid = array('id'=>$data['ordered_coupon']);
	// 		$coupon_details = $this->cat_frontend->getwheres('coupons',$where_ccid);
	// 		if($coupon_details)
	// 		{

	// 			$data['dis_type'] = $coupon_details[0]['discount_type'];
	// 			$data['dis_amt'] = $coupon_details[0]['discount'];
	// 			$latest_amount = $data['gateway_return_orders'][0]['order_amount'];
	// 			$data['subtotal_success'] = $latest_amount;
	// 			if($data['dis_type']=='flat')
	// 			{
	// 				$data['sum']= $latest_amount-$data['dis_amt'];
	// 				$item['coupon_discounted'] = $data['dis_amt'];
	// 			}else{	
	// 				$sum_amt = $latest_amount*$data['dis_amt'];
	// 				$data['sum_latest'] = $sum_amt/100;
	// 				$data['sum'] = $latest_amount - $data['sum_latest'];
	// 				$item['coupon_discounted'] = $data['sum_latest'];
	// 			}
	// 		}
	// 	}else{
	// 		$data['sum']=  $data['gateway_return_orders'][0]['order_amount'];
	// 		$latest_amount = $data['gateway_return_orders'][0]['order_amount'];
	// 		$data['subtotal_success'] = $latest_amount;
	// 		$data['coupon_discounted'] ='0';
	// 	}

	// 	foreach ($purchase_pid as $all_semesters) 
	// 	{
	// 		$my_ordered_semesters = $all_semesters['order_products'];
	// 		$purchase_pidss = $this->cat_frontend->getwheres('tbl_semester',array('sem_id'=>$my_ordered_semesters));
	// 		$all_semesters['sem_id']=$purchase_pidss;
	// 		$opt_data[]=$all_semesters;
	// 	}
	// 	$data['gateway_return_orders'] = $opt_data;
	// 	$user_details = array('id'=> $user_id);
	// 	$user_data = $this->cat_frontend->getwheres('tbl_students',$user_details);
	// 	$full_name=$user_data[0]['full_name'];
	// 	$email_id = $user_data[0]['username'];
	// 	$billling_details = array('user_id'=> $user_id,'order_id'=>$user_ordercode);
	// 	$billing_data = $this->cat_frontend->getwheres('order_payment_details',$billling_details);
	// 	$html = '<!DOCTYPE html>
	// 		<html lang="en">
	// 		<head>

	// 		<meta charset="utf-8">
	// 		<title>Order Invoce</title>
	// 		<style>
	// 		@font-face{font-family:SourceSansPro;src:url(http://182.70.242.26/catalyser_test/assets/front/invoice/SourceSansPro-Regular.ttf)}.clearfix:after{content:"";display:table;clear:both}a{color:#0087C3;text-decoration:none}body{position:relative;width:21cm;height:29.7cm;margin:0 auto;color:#555555;background:#FFFFFF;font-family:Arial, sans-serif;font-size:14px;font-family:SourceSansPro}header{padding:10px 0;margin-bottom:20px;border-bottom:1px solid #AAAAAA}#logo{float:left;margin-top:8px}#logo img{height:70px}#company{text-align:right}.clearfix{clear:both}#details{margin-bottom:50px}#client{padding-left:6px;border-left:6px solid #0087C3;float:left}#client .to{color:#777777}h2.name{font-size:1.4em;font-weight:normal;margin:0}#invoice{float:right;text-align:right}#invoice h1{color:#0087C3;font-size:2.4em;line-height:1em;font-weight:normal;margin:0 0 10px 0}#invoice .date{font-size:1.1em;color:#777777}table{width:100%;border-collapse:collapse;border-spacing:0;margin-bottom:20px}table th,table td{padding:20px;background:#EEEEEE;text-align:center;border-bottom:1px solid #FFFFFF}table th{white-space:nowrap;font-weight:normal}table td{text-align:right}table td h3{color:#57B223;font-size:1.2em;font-weight:normal;margin:0 0 0.2em 0}table .no{color:#FFFFFF;font-size:1.6em;background:#57B223}table .desc{text-align:left}table .unit{background:#DDDDDD}table .total{background:#57B223;color:#FFFFFF}table td.unit,table td.qty,table td.total{font-size:1.2em}table tbody tr:last-child td{border:none}table tfoot td{padding:10px 20px;background:#FFFFFF;border-bottom:none;font-size:1.2em;white-space:nowrap;border-top:1px solid #AAAAAA}table tfoot tr:first-child td{border-top:none}table tfoot tr:last-child td{color:#57B223;font-size:1.4em;border-top:1px solid #57B223}table tfoot tr td:first-child{border:none}#thanks{font-size:2em;margin-bottom:50px}#notices{padding-left:6px;border-left:6px solid #0087C3}#notices .notice{font-size:1.2em}footer{color:#777777;width:100%;height:30px;position:absolute;bottom:0;border-top:1px solid #AAAAAA;padding:8px 0;text-align:center}
	// 		</style>
	// 		</head>
	// 		<body>
	// 		<header class="clearfix">
	// 		<div id="logo">
	// 		<img src="http://182.70.242.26/ramanujam/assets/images/logo2.png" width="auto" style="height: 35px;">
	// 		</div>
	// 		<div id="company">
	// 		<h2 class="name">Ramanujam | School of Mathematics</h2>
	// 		<div>455 Foggy Heights, AZ 85004, US</div>
	// 		<div>(602) 519-0450</div>
	// 		<div><a href="mailto:company@example.com">company@example.com</a></div>
	// 		</div>
	// 		<div style="clear:both;"></div>
	// 		</div>
	// 		</header>
	// 		<main>
	// 		<div id="details" class="clearfix">
	// 		<div id="client">
	// 		<div class="to">INVOICE TO:</div>';
	// 	$html .= "<h2 class='name'>".$full_name."</h2>";
	// 	$html.= '<div class="email">';
	// 	$html.= '<a href="mailto:'.$email_id.'">'.$email_id.'</a></div></div>';
	// 	$html.='<div id="invoice">';
	// 	$html.='<h1>'.$user_ordercode.'</h1>';							
	// 	$html.='<div class="date">DATE:'.$billing_data[0]['created_on'].'</div>';
	// 	$html.='</div>';
	// 	$html.='</div>';
	// 	$html.='<table border="0" cellspacing="0" cellpadding="0">
	// 		<thead>
	// 		<tr>
	// 		<th class="desc">SEMESTER NAME</th>
	// 		<th class="unit">PRICE</th>
	// 		<th class="qty">DURATION</th>
	// 		<th class="total">TOTAL</th>
	// 		</tr>
	// 		</thead>
	// 		<tbody>';

	// 	foreach ($data['gateway_return_orders'] as $products) {
	// 		$html.='<tr><td class="desc"><h3 style="color:hsl(0, 0%, 13%);">'.$products['sem_id'][0]['sem_name'].'</h3></td>';
	// 		$html.='<td class="desc"><h3 style="color:hsl(0, 0%, 13%);">'.$products['sem_id'][0]['sem_price'].'</h3></td>';
	// 		$html.='<td class="desc"><h3 style="color:hsl(0, 0%, 13%);">'.$products['sem_id'][0]['sem_duration'].'(Days)</h3></td>';
	// 		$html.='<td class="desc"><h3 style="color:hsl(0, 0%, 13%);">'.$products['sem_id'][0]['sem_price'].'</h3></td></tr></tbody>';

	// 	}
	// 	$html.='</table>';
	// 	$html.='<table>';
	// 	$html.='<tr>';
	// 	$html.='<td colspan="2"></td>';
	// 	$html.='<td colspan="2">SUBTOTAL</td>';
	// 	$html.='<td>'.$data["subtotal_success"].'</td></tr>';

	// 	if($data['ordered_coupon']!=0)
	// 	{
	// 		if($data['dis_type']=='flat')
	// 		{
	// 			$html.='<tr><td colspan="2"></td><td colspan="2">DISCOUNT</td><td>'.$data['dis_amt'].'</td></tr>';
	// 		}else if($data['dis_type']=='percent'){
	// 			$html.='<tr><td colspan="2"></td><td colspan="2">DISCOUNT</td><td>'.$data['sum_latest'].'</td></tr>';
	// 		}
	// 	}
	// 	else{
	// 		$html.='<tr><td colspan="2"></td><td colspan="2">DISCOUNT</td><td>'.$data['coupon_discounted'].'</td></tr>';
	// 	}							

	// 	$html.='<tr>';
	// 	$html.='<td colspan="2"></td><td colspan="2">GRAND TOTAL</td><td>'.$data['sum'].'</td></tr></table>';

	// 	$html.='<div id="thanks">Thank you!</div>';
	// 	$html.='<div id="notices">';
	// 	$html.='<div>NOTICE:</div>';
	// 	$html.='<div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>';
	// 	$html.='</div>';
	// 	$html.='</main>';
	// 	$html.='<footer>';
	// 	$html.='Invoice was created on a computer and is valid without the signature and seal.';
	// 	$html.='</footer>';
	// 	$html.='</body>';
	// 	$html.='</html>';
	// 	$message = $html;
	// 	$to = $email_id;
	// 	$headers="From:worthfcfc321@gmail.com";
	// 	$headers .= "MIME-Version: 1.0\r\n";
	// 	//$headers .= 'MIME-Version: 1.0' . "\r\n";
	// 	//$headers .= 'Content-type: text/html;charset=iso-8859-1' . "\r\n";
	// 	$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
	// 	//$headers .= "Content-Type: text/html; charset=iso-8859-1\n";

	// 	$subject = 'Ramanujam Enrollment Invoice';

	// 	$result = $this->cat_frontend->sendmail('worthfcfc321@gmail.com','worthfcfc123','Builder Admin',$to,$subject,$message, $headers);

	// 	$data['my_oid'] = $user_ordercode;

	// 	load_front_view('template/success_order',$data);
	// }

	// public function cancel_order()
	// {
	// 	  load_front_view('template/cancel_order');

	// }

	

}
