<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ashwamegha_exam_control extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct(); 
        $this->load->helper(array('url','html','form')); 
        // $this->load->model('front/commonmodel');
        $this->load->model('front_end/exam_model');
        //$this->checkAuth();
    }
 
    public function checkAuth()
    {
        if($this->session->userdata('id')!='' )
        {
            return true;
        }
        else
        {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function index() 
    { 
        redirect('view_analysis');die;
        // $test_list=$this->db->query("SELECT * FROM `tbl_test` WHERE `test_status` = 'active' ");
        // $data['tests']=$test_list->result_array();
        // load_exam_view('exam/exam_list',$data);
    }

    public function entrance() 
    {
        $this->checkAuth();
        $user_class=$this->session->userdata('user_class');
        $data['sem_id']='0';
        if ($user_class<=6)
        {
            $data['sem_id']='1';
        }
        if ($user_class==7)
        {
            $data['sem_id']='2';
        }
        if ($user_class==8)
        {
            $data['sem_id']='3';
        }
        if ($user_class>=9)
        {
            $data['sem_id']='4';
        }
        $data['exam']=$this->exam_model->getEntranceDetails($data['sem_id']);
        // print_r($data['sem_id']);
        // print_r($this->session->all_userdata());
        $this->session->set_userdata(array('exam'=>$data['exam']));
        load_exam_view('exam/startexam', $data);
    }

    public function exam_instruction() 
    {
        $user_id=$this->session->userdata('id');
        // $data['sem_id'] = $this->input->get('sem_id');
        // if(is_numeric($data['sem_id'])){
        //     $sem_id=$data['sem_id'];
        // }else{
        //     $sem_id=base64_decode($this->input->get('sem_id'));
        // }

        // if($sem_id!=''){
        //     $sql21="SELECT `end_date` FROM `product_orders` WHERE `order_user_id` LIKE $user_id AND `order_products` LIKE $sem_id AND `order_status` = 'Completed' AND `end_date` > CURRENT_TIMESTAMP() ";
        //     // exit('pl');
        //     $student_sems = $this->db->query($sql21);
        //     if (!$student_sems){
        //         redirect('dashboard');
        //     }
            
        // }
        if($this->input->get('eid')!=''){
            $eid= base64_decode($this->input->get('eid'));
            if(is_numeric($eid)){
                $no_que=$this->db->query("SELECT COUNT(`ques_id`) as 'noq' FROM `tbl_add_question` WHERE `test_id` =  $eid AND `ques_status` = 'active' ");
                if($no_que->num_rows()>0){
                    //print_r($no_que->result_array());
                    $tque=$no_que->result_array();
                    $total_que=$tque[0]['noq'];
                }else{$total_que=0;}
                $data['total_que']=$total_que;
                // exit($total_que);
                if($this->input->get('re')!='1'){
                    $isexam=$this->db->query("SELECT * FROM `tbl_user_test` WHERE `user_id` = $user_id AND `test_id` = $eid");
                    if($isexam->num_rows()>0){
                        redirect("summary_analysis/?test_id=$eid");
                    }
                }
                $data['exam']=$this->exam_model->fetch_recordbyid('tbl_test',array('test_id'=>$eid,'test_status'=>'active'));
                // echo "<pre>";
                // print_r($data['exam']);
                // die;
                $this->session->set_userdata(array('exam'=>$data['exam']));
                // $this->load->view('user_dashboard/header_footer/header', $data);
                load_exam_view('exam/startexam', $data);
                // $this->load->view('user_dashboard/header_footer/footer', $data);
            }else{
                redirect('');
            }
        }else{
            redirect('');
        }
    }

    public function doexam($eid='')
    {
        $eid = $this->input->get('eid');
        if($eid!=''){
            if(is_numeric($eid)){
                $id=$eid;
            }else{
                $id=base64_decode($eid);
            }
            $sem_id = $this->input->get('sem_id');
            $user_id=$this->session->userdata('id');
            $testid = array('test_id' => $id,'test_start'=>$id );
            $this->session->set_userdata($testid);
            $exam = $this->session->userdata('exam');
            // var_dump($exam);
            if (!$exam) {
                redirect('front_end/ashwamegha_exam_control');
            }
            // if($exam->test_type=='2' || $exam->test_type=='3'){
            //     // exit('die');
            //     $exam->ideal_time_duration = 0;
            // }
                // print_r($exam);die;
            $data['exam']=$exam;
            $test_name = array('test_name' => $exam->test_name);
            $this->session->set_userdata($test_name);
            $where = array('user_id' => $user_id,'test_id' => $id);
            $examdata = $this->exam_model->fetch_recordbyid('tbl_user_test',$where);
            if($examdata){
                // echo $id;
                // echo "summary_analysis/?test_id=$id";
                // exit('on');
                redirect("summary_analysis/?test_id=$id");
                // $update_data=array('date'=>date('Y-m-d'),'sem_id'=>$sem_id,'score'=>'', 'status' => 'pending');
                // $this->exam_model->updateRecords('tbl_user_test',$update_data,$where);
                // echo $this->db->last_query();//exit;
                // $where1 = array('user_id' => $user_id,'test_id' => $id);
                // $this->exam_model->deleteRecords('tbl_user_ques_answer',$where1);
                // echo $this->db->last_query();exit;
            }else{
                // $this->exam_model->insert_data('tbl_user_test',array('user_id'=>$user_id,'test_id'=>$id,'sem_id'=>$sem_id,'date'=>date('Y-m-d'), 'status' => 'pending'));
                // $this->exam_model->insert_data('tbl_test_result',array('tr_user_id'=>$user_id,'tr_test_id'=>$id,'tr_sem_id'=>$sem_id));
            }
            $que_data=$this->get_user_exam_data($id);
            // echo $this->db->last_query();echo "<pre>";
            //  print_r($que_data['questions']);die;
            $data['sat_q_count']=0; $data['mat_q_count']=0; $data['lang_q_count']=0;
            foreach ($que_data['questions'] as $key => $value) {
                if ($key=='SAT'){
                    $data['sat_q_count']=count($value);
                }if ($key=='MAT'){
                    $data['mat_q_count']=count($value);
                }if ($key=='lang'){
                    $data['lang_q_count']=count($value);
                }
                # code...
            }
            // $data['mat_q_count']=$b;
            // $data['lang_q_count']=$c;
            // print_r($data);
            // die;
            $questions = array('questions' => $que_data['questions']);
            // echo "<pre>";print_r($this->session->all_userdata());echo "</pre><br>========================================";
            $this->session->set_userdata($questions);
            $data['no_questions']=count($que_data['questions']);
            // echo "<pre>";print_r($this->session->all_userdata());echo "</pre>";//die;
            // echo "<pre>";print_r($que_data['questions']['SAT'][0]);
            $fdata=$this->get_quest_data($que_data['questions']['MAT'][0]['question_id']);
            $data['thisquestion']=$fdata['thisquestion'];
            // echo "</pre>";
            // die;
            $data['questions']=@$que_data['questions']['MAT'][0];
            /* for adaptive learning */
            // if($exam->test_type==){
            //     load_exam_view('exam/do_adaptive', $data);                
            // }else{
            //     load_exam_view('exam/do_exam', $data);
            // }
            /* for adaptive learning end */
            load_exam_view('exam/do_exam', $data);
        }
        // $data=array();
        //     load_exam_view('exam/do_exam', $data);
    }
    public function get_quest_data($que_id){
        $que_data=$this->exam_model->grt_data('tbl_add_question',array('ques_id'=>$que_id));
        $exam['thisquestion'] = array();
        if (!empty($que_data))
        {
            $exam['thisquestion']['qid']=$que_id;
            $exam['thisquestion']['question_desc']=$que_data[0]['question_desc'];
            // print_r($que_data[0]['question_desc']);
            $answers = array();
            for ($ai=1;$ai<=4; $ai++) {
                $answer_data = array('id' =>$ai, 'text'=>trim($que_data[0]['ans_'.$ai]),'s'=>0);
                // $answer_data = array('id' =>$ai, 's'=>0);
                array_push($answers, $answer_data);
            }
            $exam['thisquestion']['answers'] = $answers;
        }
        // print_r($exam);
        return $exam;
    }

    public function get_user_exam_data($examid){
        $user = $this->session->userdata('id');
        $examdata = $this->exam_model->getExamQue($examid);
        $exam = array();
        $exam['questions'] = array();
        if (!empty($examdata))
        {
            // $exam['questions'] = $examdata;
            $exam['id']=$this->session->userdata('exam')->test_id;
            $exam['name']=$this->session->userdata('exam')->test_name;
            $a=0;$b=0;$c=0;$d=0;
            foreach ($examdata as $count=>$question)
            {
                if ($question['section']=='SAT') {$d=$a; $a++;}
                if ($question['section']=='MAT') {$d=$b; $b++;}
                if ($question['section']=='lang') {$d=$c; $c++;}
                $exam['questions'][$question['section']][$d]['test_id'] = $question['test_id'];
                $exam['questions'][$question['section']][$d]['question_id'] = $question['ques_id'];
                $exam['questions'][$question['section']][$d]['section'] = $question['section'];
                // $exam['questions'][$question['section']][$d]['text'] = $question['question_desc'];
                // $exam['questions'][$question['section']][$d]['question_desc'] = $question['question_desc'];
                $exam['questions'][$question['section']][$d]['image'] = ($question['attachment_img']  != '') ? '<img src="'.base_url().$question['attachment_img'].'" />' : '' ;
                $answers = array();
                for ($ai=1;$ai<=4; $ai++) {
                    // $answer_data = array('id' =>$ai, 'text'=>trim($question['ans_'.$ai]),'s'=>0);
                    $answer_data = array('id' =>$ai, 's'=>0);
                    array_push($answers, $answer_data);
                }
                $exam['questions'][$question['section']][$d]['answers'] = $answers;
            }
            // echo "<pre>"; print_r($exam);echo "</pre>";die;
            return $exam;
        }
        return $exam;
    }

    public function save_answer(){
        $user_id = $this->session->userdata('id');
        $question_id = $_POST["queno"];
        $section = $_POST["section"];
        if ($_POST['section']=='s') {$section='SAT'; }
        if ($_POST['section']=='m') {$section='MAT'; }
        if ($_POST['section']=='l') {$section='lang'; }
        $ans = $_POST["ans"];
        $time = $_POST["time"];
        $questions=$this->session->userdata("questions");
        $test_id=$this->session->userdata("test_id");
        $test_type=$this->session->userdata("test_type");
        $ses=$this->session->userdata('questions');
        $an=$ans-1;
        for ($i=0; $i <4 ; $i++) { 
            $ses[$section][$question_id]['answers'][$i]['s']=0;
            # code...
        }
        $ses[$section][$question_id]['answers'][$an]['s']=1;
        // print_r($ses[$question_id]['answers'][$an]['s']);
        $this->session->set_userdata(array('questions'=>$ses));
        // echo "<pre>";print_r($this->session->userdata('questions'));echo "</pre>";//die;
        /* For Adaptive */
        // if($test_type==2 ){
        //    $this->next_que($question_id, true);
        //    return;
        // }
        /* For Adaptive End */
        $que_id=$ses[$section][$question_id]['question_id'];
        $where = array('user_id' => $user_id,'question_id' => $que_id);
        $examdata = $this->exam_model->fetch_recordbyid('tbl_user_ques_answer',$where);

        if($examdata){
            $this->exam_model->deleteRecords('tbl_user_ques_answer',$where);
        }

        $data= array('user_id'=> $user_id, 'test_id'=>$test_id,'date'=>date('Y-m-d'),'question_id'=>$que_id,'ques_time'=>$time, 'answer'=>$ans,'attempted'=>'yes','status'=>'active');
        $this->exam_model->insert_data('tbl_user_ques_answer',$data);
        $this->next_que($question_id+1,false,$_POST['section']);
    }
    
    public function next_que($ques_index='' ,$adaptive=false,$ques_sec=''){
        if($ques_index==''){
            $ques_index=$this->input->get('id');
        }
        if($ques_sec==''){
            $ques_sec=$this->input->get('section');
        }
        if($ques_sec=='s'){$ques_sec="SAT";}
        if($ques_sec=='m'){$ques_sec="MAT";}
        if($ques_sec=='l'){$ques_sec="lang";}
        // print_r($ques_sec);die;
        $questions=$this->session->userdata('questions');
        // print_r($questions);
        // print_r($this->session->all_userdata());
        if(array_key_exists($ques_index,$questions[$ques_sec])){
            // print_r($questions[$ques_sec][$ques_index]['question_id']);

            $fdata=$this->get_quest_data($questions[$ques_sec][$ques_index]['question_id']);
            $data['thisquestion']=$fdata['thisquestion'];

            $data['questions']=$questions[$ques_sec][$ques_index];
            $data['ques_index']=$ques_index+1;
            if($adaptive){
                $this->load->view('exam/single_que_adaptive',$data);
            }
            $this->load->view('exam/single_que',$data);
        }else{
            exit("changeSec");
            // if($ques_sec=='SAT'){
            //     $data['questions']=$questions['MAT'][0];
            //     $data['ques_index']=0;
            //     $this->load->view('exam/single_que',$data);
            // }
            // if($ques_sec=='MAT'){
            //     $data['questions']=$questions['lang'][0];
            //     $data['ques_index']=0;
            //     $this->load->view('exam/single_que',$data);
            // }
            // if($ques_sec=='lang'){
            //     $data['questions']=$questions['SAT'][0];
            //     $data['ques_index']=0;
            //     $this->load->view('exam/single_que',$data);
            // }

            // echo "ij";
            // echo $ques_index;
            // $this->finish_user_exam();
        }
    }

    public function finish_user_exam($fin=''){
    
        $user_id = $this->session->userdata('id');
        $examid = $this->session->userdata('test_id');
        $fin = @$this->input->get['fin'];
        $where = array('user_id' => $user_id,'test_id' => $examid);
        $examdata = $this->exam_model->fetch_recordbyid('tbl_user_test',$where);
        if($examdata){
            $data=array('date'=>date('Y-m-d'), 'status' => 'completed');
            $this->exam_model->updateRecords('tbl_user_test',$data,$where);
        }else{
            // return true;
        }
        if($fin){
            $this->session->unset_userdata('exam');
            return true;                
        }
        $response = 'Click On Finish Button if you wish to Submit the Examination.';
        // redirect("catalyser_main/view_test_analysis/$examid");
        // redirect("summary_analysis/?test_id=examid");
        $this->session->unset_userdata('exam');
        $this->load->view('exam/finishExam');
        // echo $response;
    }

    public function online_test_for_chapter(){
        $ch_id= $this->input->get('id');
        $data['sem_id']= $this->input->get('sem_id');
        $online_test_data=$this->exam_model->grt_data('tbl_test',array('test_type'=>'4','chapter_id'=>$ch_id,'test_status'=>'active'));
        // print_r($online_test_data);
        $data['test_list']=$online_test_data;
        $this->load->view('exam/test_list_popup',$data);
    }   

    public function practiceTest_test_for_chapter(){
        $ch_id= $this->input->get('id');
        $data['sem_id']= $this->input->get('sem_id');
        $online_test_data=$this->exam_model->grt_data('tbl_test',array('test_type'=>'3','chapter_id'=>$ch_id,'test_status'=>'active'));
        // print_r($online_test_data);
        $data['test_list']=$online_test_data;
        $this->load->view('exam/test_list_popup',$data);

    }

    public function adaptiveTestForLacture(){
        $ch_id= $this->input->get('id');
        $data['sem_id']= $this->input->get('sem_id');
        $online_test_data=$this->exam_model->grt_data('tbl_test',array('test_type'=>'2','lecture_id'=>$ch_id,'test_status'=>'active'));
        // print_r($online_test_data);
        $data['test_list']=$online_test_data;
        $this->load->view('exam/test_list_popup',$data);

    }

    public function practiceTestForLacture(){
        $ch_id= $this->input->get('id');
        $data['sem_id']= $this->input->get('sem_id');
        $online_test_data=$this->exam_model->grt_data('tbl_test',array('test_type'=>'3','lecture_id'=>$ch_id,'test_status'=>'active'));
        // print_r($online_test_data);
        $data['test_list']=$online_test_data;
        $this->load->view('exam/test_list_popup',$data);

    }

} 
?>