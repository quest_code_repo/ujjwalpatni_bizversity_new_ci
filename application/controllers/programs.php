<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Programs extends MY_Controller{
	function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('Events_model','events');
        $this->load->model('home_model');
        
    }

    	public function index($url)
    	{

    		if(!empty($url))
    		{
    					if ($this->session->userdata('id')) {
			$where = array('id'=>$this->session->userdata('id'));
			$data['UserDetail'] = $this->events->get_data_orderby_where('tbl_students', "", $where, "");
		}else{
			$data['UserDetail'] = '';
		}

		$categoryId = $this->events->get_data_orderby_where('tbl_programs_category','name,id',array('url'=>$url),'ASC');

		$where = array('programs_category_id'=>$categoryId[0]->id, 'status'=>'active');
		$data['explored_data'] = $this->events->get_data_orderby_where('tbl_vip_explored_content', "exp_id", $where, "ASC");

			$date = date('Y-m-d');

		$where = array('tbl_programs.category_id'=>$categoryId[0]->id,'tbl_programs.deleted'=>'0','tbl_programs.program_date >='=>$date);
		$column = array('*','tbl_programs.id as programe_id');
		$data['program_details'] = $this->events->get_program_details($column, $where, "tbl_programs.program_date", "ASC", "","");

		$data['all_cities'] = $this->events->get_data_orderby_where('cities','city_name',array(),'ASC');

		$data['BrochureDetails'] = $this->home_model->getdata_orderby_where_join2('brochure_content','tbl_programs_category','brochure_category_id','id',array('brochure_category_id'=>$categoryId[0]->id),'brochure_name,brochure_image,brochure_id','','');

			
		$data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);

		$data['VideoUrl'] =  $this->events->get_data_orderby_where('tbl_programs_category', "",array('deleted'=>'0','url'=>$url), "");

		$where = array('status' => 'active', 'category_id' => '4');
    	$data['testimonials'] = $this->home_model->getdata_orderby_where('testimonials',$where,'','testimonials_id','ASC','');

    	$data['programCategoryId'] = $categoryId[0]->id;

    	
    	      $this->load_view('events/new_event', $data);

    		}

    		

    		
    	   
    	}
}