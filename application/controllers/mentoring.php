<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mentoring extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        //$this->load->helper('front_end/cat_frontend');
        
    }

    public function coming_soon()
    {
    	$this->load_view('mentoring/coming_soon');
    }

    public function mentoring_criteria($url="")
    {
        if(!empty($url))
        {
             $data['CurrentMentoring'] = $this->home_model->getwhere_data('mentoring_criteria',array('url'=>$url),'id,title','','');

            $data['MentoringExp'] = $this->home_model->getwhere_limit_data('tbl_mentoring_explored_content',array('mentoring_id'=>$data['CurrentMentoring'][0]->id),'','','',7);

            $data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);

            $data['Cities'] = $this->home_model->get_data_orderby_where('cities','city_name','','ASC');

             $data['MentoringProposal'] = $this->home_model->getwhere_data('mentoring_proposal',array('mentoring_proposal_for'=>$url),'','mentoring_proposal_id','');

            $where = array('status' => 'active', 'category_id' => '6');
            $data['testimonials'] = $this->home_model->getdata_orderby_where('testimonials',$where,'','testimonials_id','ASC','');


            $this->load_view('mentoring/mentoring_criteria',$data);
        }
       
    }

    public function new_mentoring_data()
    {
        $postData = $this->input->post();

        if(!empty($postData))
        {
            $newData = array('first_name'   => $postData['mentoring_contact_person'],
                            'last_name'     => $postData['mentoring_contact_sur_person'],
                            'mentoring_city'=> $postData['mentoring_city'],
                            'mentoring_pincode'=>$postData['mentoring_pincode'],
                            'mentoring_address'=>$postData['mentoring_address'],
                            'mentoring_email' => $postData['mentoring_email'],
                            'mobile_no'       => $postData['mentoring_phone_no'],
                            'organization_name'=>$postData['name_of_organization'],
                            'no_of_people'     =>$postData['no_of_people'],
                            'class_name'       =>$postData['mentoring_class_id']
                        );

           $quoteId =  $this->home_model->insert_data('tbl_mentoring_quote',$newData);

           if(!empty($quoteId))
           {
                $config['mailtype'] = 'html';
                $config['charset'] = 'iso-8859-1';
                $config['priority'] = 1;
                $this->email->initialize($config);
                $this->email->clear();

                $message = "";

                $message.=  '<!DOCTYPE html>
                            <html>
                            <head>
                                <title>Mentoring Session</title>
                            </head>
                            <body>

                            <div style="width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;">
                                <div style="background-color: #D55220;">
                                    <img src="https://ujjwalpatni.com/front_assets/logo.png" style="height: 55px;">
                                </div>
                                <div style="font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px">
                                    <p>Dear '.$postData["mentoring_contact_person"].'  '.$postData["mentoring_contact_sur_person"].'</p>
                                    <P>Thanks for submitting the mentoring form.</P>
                                     <p>Our team will contact you soon. </p>
                                
                                
                            <div style="font-size: 16px;font-family: sans-serif;text-align: left;margin-bottom: 40px; margin-right: 25px;">
                                 
                                <p style="margin:25px 0 5px; ">Thanks!</p>
                                <p style="margin:5px 0;">Regards,</p>
                                <p style="margin:5px 0;">Team Ujjwal Patni</p>
                                </div>
                            </div>

                            </body>
                            </html>';


                            $to = "ppppltd@gmail.com";
                $from_email = "info@questglt.com";
                $this->email->from($from_email,'Ujjwal Patni Training Programs');
                $this->email->to($to); 
                $this->email->subject('Contact Details');
                $this->email->set_mailtype('html');
                $this->email->message($message);  
                $send = $this->email->send();


                echo "SUCCESS";
           }

          
        }
    }


  }
