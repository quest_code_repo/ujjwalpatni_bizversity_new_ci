<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $this->load->helper('form');
		$this->load->helper(array('url','html','form')); 
		$this->load->helper('main_template_helper'); 
		$this->load->helper('cookie');
		$this->load->model('front_end/cat_frontend');
		$this->checkAuth();
	
	}

	// public function index()
	// {
	// 	$this->load->view('welcome_message');
	// }

	public function checkAuth()
	{
		// print_r($this->session->all_userdata());die;
		if($this->session->userdata('id')!='' )
		{
			return true;
		}
		else
		{
			$this->session->sess_destroy();
			$url = base_url();
			redirect( $url.'login');
		}
	}

	// public function profile()
	// {
	// 	$user_id=$this->session->userdata('id');
	// 	$user_where = array('id'=>$user_id);
	// 	$data['user_data']=$this->cat_frontend->getwheres('tbl_students', $user_where);
	// 	// print_r($data['user_data']);
	// 	load_user_dashboard_view('user_dashboard/user_profile',$data);

	// }
	// public function edit_profile()
	// {
	// 	$user_id=$this->session->userdata('id');
	// 	$user_where = array('id'=>$user_id);
	// 	$data['user_data']=$this->cat_frontend->getwheres('tbl_students', $user_where);
	// 	// print_r($data['user_data']);
	// 	load_user_dashboard_view('user_dashboard/edit_user_profile',$data);

	// }
	// public function SaveEditProfile()
	// {
	// 	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	// 		// echo $this->input->post('');
	// 		$user_id=$this->session->userdata('id');
	// 		$query="UPDATE `tbl_students` SET `full_name`=value,`date_ob`=value,`class_name`=value,`mobile`=value,`alt_mobile`=value,`gender`=value,`address`=value,`city`=value,`state`=value,`pincode`=value WHERE `id`=$user_id";
	// 		// $this->db->query($query);
	// 	}else{
	// 		return false;
	// 	}
	// }
	
	// public function test_progress_tab($userid, $get_semid)
	// {
	// 	$whre_test_user = array("tbl_user_test.user_id"=>$userid,'tbl_user_test.status'=>'completed','tbl_user_test.sem_id'=>$get_semid,'tbl_test.test_type'=>'3');

	//     $my_tests = $this->cat_frontend->get_data_twotable_column_where('tbl_user_test','tbl_test','test_id','test_id','',$whre_test_user,'');

	// 	if(!empty($my_tests))
	// 	{
	// 	    $count=0;
	// 	    $test_energy_point=0;
	// 		foreach($my_tests as $done_test)
	// 		{
	// 		  	$test_energy_point+= $done_test['test_energy_point'];
	// 		}
	// 		$ddata['solved_practiceshits'] = COUNT($my_tests);
	// 		$ddata['practice_shit_points'] = $test_energy_point;
	// 	}
	// 	else
	// 	{
	// 		$ddata['solved_practiceshits'] = 0;
	// 		$ddata['practice_shit_points'] = 0;
	// 	}	

	// 	$whre_test_user_online = array("tbl_user_test.user_id"=>$userid,'tbl_user_test.status'=>'completed','tbl_user_test.sem_id'=>$get_semid,'tbl_test.test_type'=>'4');

	//     $my_tests_online = $this->cat_frontend->get_data_twotable_column_where('tbl_user_test','tbl_test','test_id','test_id','',$whre_test_user_online,'');
	//     // echo $this->db->last_query();

	// 	if(!empty($my_tests_online))
	// 	{
	// 	    $count=0;
	// 	    $test_energy_point_online=0;
	// 		foreach($my_tests_online as $done_test_online)
	// 		{
	// 		  	$test_energy_point_online+= $done_test_online['test_energy_point'];
	// 		}
			
	// 		$ddata['solved_online_test'] = COUNT($my_tests_online);
	// 		$ddata['online_shit_points'] = $test_energy_point_online;
	// 	}else{
	// 		$ddata['solved_online_test'] = 0;
	// 		$ddata['online_shit_points'] = 0;
	// 	}

	// 		    $watch_where = array('sem_id'=>$get_semid,'user_id'=>$userid);
	//     $watching_points = $this->cat_frontend->getwheres('tbl_read_lecture', $watch_where);

	//     $total_frienship_points = 0;
	//     if(!empty($watching_points))
	//     {
	// 	    foreach($watching_points as $count_total)
	// 	    {
	// 			$total_frienship_points += $count_total['friendship_point'];
	// 	    }
	//      	$ddata['total_frienship_points'] = $total_frienship_points;
	//     }
	//     else
	//     {
	//     	$ddata['total_frienship_points'] = 0;
	//     }

	//     $whre_test_user_adaptive = array("tbl_user_test.user_id"=>$userid,'tbl_user_test.status'=>'completed','tbl_user_test.sem_id'=>$get_semid,'tbl_test.test_type'=>'2');

	//     $my_tests_adaptive = $this->cat_frontend->get_data_twotable_column_where('tbl_user_test','tbl_test','test_id','test_id','',$whre_test_user_adaptive,'');

	// 	if(!empty($my_tests_adaptive))
	// 	{
	// 	    $count=0;
	// 	    $test_energy_point_adaptive=0;
	// 		foreach($my_tests_adaptive as $done_test_adaptive)
	// 		{
	// 		  	$test_energy_point_adaptive+= $done_test_adaptive['test_energy_point'];
	// 		}
	// 		$ddata['solved_adaptive_test'] = COUNT($my_tests_adaptive);
	// 		$ddata['adaptive_shit_points'] = $test_energy_point_adaptive;
	// 	}
	// 	else
	// 	{
	// 		$ddata['solved_adaptive_test'] = 0;
	// 		$ddata['adaptive_shit_points'] = 0;
	// 	}

	// 	$get_test_type="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`sem_id`= '".$get_semid."' and `test_type` =3 ";
	// 	$test_data=$this->cat_frontend->get_sql_record($get_test_type);
	// 	if(!empty($test_data)){
	// 		$mycount=$test_data[0]['test_count'];
	// 	}else{
	// 		$mycount=0;
	// 	}

	// 	$get_test_type1="SELECT `ch_id`  FROM `tbl_chapter` WHERE `sem_id` = '".$get_semid."' ";
	// 	$test_data=$this->cat_frontend->get_sql_record($get_test_type1);
	// 	foreach ($test_data as $key => $value) {
	// 		$get_test_type2="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`chapter_id`= '".$value['ch_id']."' AND `test_type` =3 ";
	// 		$test_data2=$this->cat_frontend->get_sql_record($get_test_type2);
	// 		$mycount +=$test_data2[0]['test_count'];

	// 		$get_test_type3="SELECT *  FROM `tbl_lecture` WHERE `ch_id` = '".$value['ch_id']."' ";
	// 		$test_data3=$this->cat_frontend->get_sql_record($get_test_type3);
	// 		foreach ($test_data3 as $key => $value1) {
	// 			$get_test_type4="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`lecture_id`= '".$value1['lect_id']."' AND  `test_type` =3 ";
	// 			$test_data4=$this->cat_frontend->get_sql_record($get_test_type4);
	// 			$mycount +=$test_data4[0]['test_count'];
	// 		}
	// 	}	
	// 	$ddata['total_practiceshit'] = $mycount;

	// 	$get_test_type_test="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`sem_id`= '".$get_semid."' and `test_type` =4 ";
	// 	$test_data_online=$this->cat_frontend->get_sql_record($get_test_type_test);
	// 	// echo $this->db->last_query();

	// 	if(!empty($test_data_online)){
	// 		$mycount_online=$test_data_online[0]['test_count'];
	// 	}else{
	// 		$mycount_online=0;
	// 	}
	// 	$get_test_type1_online="SELECT `ch_id` FROM `tbl_chapter` WHERE `sem_id` = '".$get_semid."' ";
	// 	$test_data_type1=$this->cat_frontend->get_sql_record($get_test_type1_online);
	// 	foreach ($test_data_type1 as $key => $value)
	// 	{
	// 		$get_test_type2_online="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`chapter_id`= '".$value['ch_id']."' AND `test_type` =4 ";
	// 		$test_data2=$this->cat_frontend->get_sql_record($get_test_type2_online);
	// 		$mycount_online +=$test_data2[0]['test_count'];

	// 		$get_test_type3_online="SELECT *  FROM `tbl_lecture` WHERE `ch_id` = '".$value['ch_id']."' ";
	// 		$test_data3_online=$this->cat_frontend->get_sql_record($get_test_type3_online);
	// 		foreach ($test_data3_online as $key => $value1)
	// 		{
	// 			$get_test_type4_online="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`lecture_id`= '".$value1['lect_id']."' AND  `test_type` =4 ";
	// 			$test_data4=$this->cat_frontend->get_sql_record($get_test_type4_online);
	// 			$mycount_online +=$test_data4[0]['test_count'];
	// 		}
	// 	}	
	// 	$ddata['total_onlineshit'] = $mycount_online;
	// 	// echo "string";
	// 	$get_test_type_test1="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`sem_id`= '".$get_semid."' and `test_type` =2 ";
	// 	$test_data_adapt1=$this->cat_frontend->get_sql_record($get_test_type_test1);
	// 	// echo $this->db->last_query();

	// 	if(!empty($test_data_adapt1)){
	// 		$total_adaptive_count=$test_data_adapt1[0]['test_count'];
	// 	}else{
	// 		$total_adaptive_count=0;
	// 	}
	// 	$get_test_type10_online="SELECT `ch_id` FROM `tbl_chapter` WHERE `sem_id` = '".$get_semid."' ";
	// 	$test_data_type10=$this->cat_frontend->get_sql_record($get_test_type10_online);
	// 	foreach ($test_data_type10 as $key => $value2112)
	// 	{
	// 		$get_test_type3_online="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`chapter_id`= '".$value2112['ch_id']."' AND `test_type` =2 ";
	// 		$test_data20=$this->cat_frontend->get_sql_record($get_test_type3_online);
	// 		$total_adaptive_count +=$test_data20[0]['test_count'];

	// 		$get_test_type4_online="SELECT *  FROM `tbl_lecture` WHERE `ch_id` = '".$value2112['ch_id']."' ";
	// 		$test_data4_online=$this->cat_frontend->get_sql_record($get_test_type4_online);
	// 		foreach ($test_data4_online as $key => $value2)
	// 		{
	// 			$get_test_type4_online="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`lecture_id`= '".$value2['lect_id']."' AND  `test_type` =2 ";
	// 			$test_data5=$this->cat_frontend->get_sql_record($get_test_type4_online);
	// 			$total_adaptive_count +=$test_data5[0]['test_count'];
	// 		}
	// 	}	
	// 	$ddata['total_adaptive_test'] = $total_adaptive_count;
	//     return $ddata;
	// }
	
	// public function my_dashboard()
	// {
	// 	$this->checkAuth();	
	// 	$userid = $this->session->userdata('id');
	// 	$get_semid = base64_decode($this->input->get('sem'));
	// 	if(empty($get_semid))
	// 	{
	// 		$get_semid = $this->session->userdata('first_sem');
	// 	}

	// 	$where_for_get_sem_name = array('sem_id'=>$get_semid);
	// 	$got_name_of_sem = $this->cat_frontend->getwheres('tbl_semester',$where_for_get_sem_name);
	// 	if(!empty($got_name_of_sem))
	// 	{
	// 		$data['semester_name_enrolled'] = $got_name_of_sem[0]['sem_name'];
	// 	}
	// 	else
	// 	{
	// 		$data['semester_name_enrolled']='';
	// 	}
		
	// 	if($get_semid!=""){
	// 		$sql21="SELECT `end_date` FROM `product_orders` WHERE `order_user_id` LIKE $userid AND `order_products` LIKE $get_semid AND `order_status` = 'Completed' AND `end_date` < CURRENT_TIMESTAMP() ORDER BY `product_order_id`";
	// 		$student_sems = $this->cat_frontend->get_sql_record($sql21);
	// 		if ($student_sems){
	// 			$data['subscription']=1;
	// 			$data['subscription_end_date']=$student_sems[0]['end_date'];
	// 		}else{
	// 			$data['subscription']=0;
	// 		}
	// 	}else{
	// 		$data['subscription']=1;
	// 	}

	//     $whr = array('sem_id'=>$get_semid,'tbl_lecture.lect_video_url !='=>'');
	//     $second = $this->cat_frontend->get_data_twotable_column_where('tbl_chapter','tbl_lecture','ch_id','ch_id','',$whr,'');
	//     $data['total_videos'] = count($second);
	//     $watch_where = array('sem_id'=>$get_semid,'user_id'=>$userid);
	//     $watching_videos = $this->cat_frontend->getwheres_count('tbl_read_lecture', $watch_where);
	//     $data['total_watched']= $watching_videos;

	//     $watching_videos_get = $this->cat_frontend->getwheres_orderby('tbl_read_lecture', $watch_where,'id');
	//     $data['last_video'] = $watching_videos_get[0]['url'];
	   
	//     /***********By Ankit Sir********/

	//     $ddata=$this->test_progress_tab($userid, $get_semid);
	//     $data['total_frienship_points'] = $ddata['total_frienship_points'];
	// 	$data['solved_practiceshits'] = $ddata['solved_practiceshits'];
	// 	$data['practice_shit_points'] = $ddata['practice_shit_points'];
	// 	$data['solved_online_test'] = $ddata['solved_online_test'];
	// 	$data['online_shit_points'] = $ddata['online_shit_points'];
	// 	$data['solved_adaptive_test'] = $ddata['solved_adaptive_test'];
	// 	$data['adaptive_shit_points'] = $ddata['adaptive_shit_points'];
	// 	$data['total_practiceshit'] = $ddata['total_practiceshit'];
	// 	$data['total_onlineshit'] = $ddata['total_onlineshit'];

	// 	/********Adaptive Learning Statistics**********/
	
	//   	$get_test_type_test_adaptive="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`sem_id`= '".$get_semid."' and `test_type` =2 ";
	// 	$test_data_adaptive=$this->cat_frontend->get_sql_record($get_test_type_test_adaptive);

	// 	if(!empty($test_data_adaptive)){
	// 		$mycount_adaptive=$test_data_adaptive[0]['test_count'];
	// 	}else{
	// 		$mycount_adaptive=0;
	// 	}
	// 	$get_test_type1_adaptive="SELECT `ch_id`  FROM `tbl_chapter` WHERE `sem_id` = '".$get_semid."' ";
	// 	$test_data_adaptive=$this->cat_frontend->get_sql_record($get_test_type1_adaptive);
	// 	foreach ($test_data_adaptive as $key => $value) {
	// 			# code...
	// 		// echo ($value['ch_id']).'<br>';
	// 		$get_test_type2_adaptive="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`chapter_id`= '".$value['ch_id']."' AND `test_type` =2 ";
	// 		$test_data3=$this->cat_frontend->get_sql_record($get_test_type2_adaptive);
	// 		$mycount_adaptive +=$test_data3[0]['test_count'];
	// 		// echo $test_data2[0]['test_count'].'<br>';//DIE;

	// 		$get_test_type3_adaptive="SELECT *  FROM `tbl_lecture` WHERE `ch_id` = '".$value['ch_id']."' ";
	// 		$test_data3_adapt=$this->cat_frontend->get_sql_record($get_test_type3_adaptive);
	// 		foreach ($test_data3_adapt as $key => $value1) {
	// 			# code...
	// 			$get_test_type4_adapt="SELECT count(`test_id`) as 'test_count' FROM `tbl_test` WHERE `tbl_test`.`lecture_id`= '".$value1['lect_id']."' AND  `test_type` =2 ";
	// 			$test_data5=$this->cat_frontend->get_sql_record($get_test_type4_adapt);
	// 			$mycount_adaptive +=$test_data5[0]['test_count'];
	// 			// echo $test_data4[0]['test_count'].'<br>';
	// 		}
	// 	}	
	// 	$data['total_adaptive'] = $mycount_adaptive;


	// 	$whre_topic_analysis = array("tbl_read_lecture.user_id"=>$userid,'tbl_read_lecture.sem_id'=>$get_semid);
	//     $data['my_tests_topic_analysis'] = $this->cat_frontend->get_data_twotable_column_where('tbl_lecture','tbl_read_lecture','lect_id','topic_id','',$whre_topic_analysis,'');
	//     if(!empty($get_semid))
	//     {
	// 	    $where_activity = "SELECT * from `tbl_lecture` INNER JOIN `tbl_read_lecture` ON `tbl_lecture`.`lect_id`= `tbl_read_lecture`.`topic_id` WHERE  `tbl_read_lecture`.`user_id`= $userid AND `tbl_read_lecture`.`sem_id`= $get_semid ORDER BY  `tbl_read_lecture`.`id` DESC limit 5";
	// 	    $data['fetch_first_activity']=$this->cat_frontend->get_sql_record($where_activity);
	// 		$where_test_activity = "SELECT * from `tbl_test` INNER JOIN `tbl_user_test` ON `tbl_test`.`test_id`= `tbl_user_test`.`test_id` WHERE  `tbl_user_test`.`user_id`= $userid AND `tbl_user_test`.`sem_id`= $get_semid ORDER BY  `tbl_user_test`.`id` DESC limit 1";
	// 	    $data['fetch_second_activity']=$this->cat_frontend->get_sql_record($where_test_activity);
	// 	}		
	// 	$userid = $this->session->userdata('id');
	// 	$where = array('id'=>$userid);
	// 	$data['student_record'] = $this->cat_frontend->getwheres('tbl_students', $where);
		
	// 	/* For Accuracy Graph By Ankit Chouksey*/
	// 	$data['solved_online_test_data'] = $this->accu_graph();
	// 	/* For Accuracy Graph End*/
		
	// 	load_user_dashboard_view('user_dashboard/my_dashboard',$data);
	// }

	// public function lacture()
	// {
	//     $this->checkAuth();
	// 	$get_semid = base64_decode($this->input->get('sem'));
 //       	if(empty($get_semid))
 //        {
 //        	$get_semid = $this->session->userdata('first_sem');
 //        }
	//     $userid = $this->session->userdata('id');
		
	// 	$sql21="SELECT `product_order_id` FROM `product_orders` WHERE `order_user_id` LIKE $userid AND `order_products` LIKE $get_semid AND `order_status` = 'Completed' AND `end_date` > CURRENT_TIMESTAMP() ORDER BY `product_order_id`";
	// 	$student_sems = $this->cat_frontend->get_sql_record($sql21);
	// 	if ($student_sems){
	// 		$andwhere="";
	// 	}else{
	// 		$andwhere= " AND `lect_is_locked` = 'Unlocked' ";
	// 	}
	//     $lacture = base64_decode($this->input->get('l'));
	//     if(empty($lacture)){
	//     	redirect('dashboard',refresh);
	//     }
	//     $sql = "SELECT * FROM `tbl_lecture` WHERE  `lect_id` = $lacture AND `lect_status` = 'active' $andwhere";
	//     $lactures = $this->cat_frontend->get_sql_record($sql);
	//     $data['lactures']=$lactures;
	//     /* for next lacture */
	//     if($lactures){
	// 	    $sql2 = " SELECT * FROM `tbl_lecture` WHERE `lecture_type` = 1 AND `ch_id` ='".$lactures['0']['ch_id']."' ";
	// 	    $nextlactures = $this->cat_frontend->get_sql_record($sql2);
	// 	    // echo "<pre>";print_r($nextlactures);echo "</pre>";
	// 	    $prevlect="";
	// 	    $nextlect="";
	// 	    foreach ($nextlactures as $nextkey => $nextvalue) {
	// 	    	if ($nextvalue['lect_id']==$lactures['0']['lect_id'])
	// 	    	{	
	// 	    		if (array_key_exists($nextkey-1, $nextlactures)) {
	// 	    			$prevlect=$nextlactures[$nextkey-1]['lect_id'];
	// 	    		}
	// 	    		if (array_key_exists($nextkey+1, $nextlactures)) {
	// 	    			$nextlect=$nextlactures[$nextkey+1]['lect_id'];
	// 	    		}
	// 	    	}
	// 	    }
	// 	    $data['prevlect']=$prevlect;
	// 	    $data['nextlect']=$nextlect;

	// 		$main_url = $this->input->get('l');
	// 		$sql1 = "SELECT * FROM `tbl_read_lecture` WHERE `topic_id` = $lacture AND `user_id` = $userid";
	// 		$lactures_check = $this->cat_frontend->get_sql_record($sql1);

	// 		if(!empty($lactures_check))
	// 		{
	// 			$whr_delete = array('topic_id' => $lacture,'user_id'=> $userid);
	// 			$remove_semester = $this->cat_frontend->delete('tbl_read_lecture',$whr_delete);
	// 		}
	//         $friendship_point = $lactures[0]['friendship_points'];
	//         $url = base_url().'lacture?l='.$main_url;
	// 		$data_insert =array(
	// 			'user_id'=>$userid,
	// 			'sem_id'=>$get_semid,
	// 			'topic_id'=>$lacture,
	// 			'friendship_point'=>$friendship_point,
	// 			'url'=>$url,
	// 			'timing'=>''
	// 			);
	// 	    $result = $this->cat_frontend->insert_data('tbl_read_lecture',$data_insert);
	// 	}
	//     load_user_dashboard_view('user_dashboard/lacture',$data);
	// }

	// public function chapter_topic()
	// {
	//     $this->checkAuth();
	//     $data['sem_id'] = $this->input->get('sem_id');
	//     if($this->input->get('topic')!=''){

	// 	    $topic = base64_decode($this->input->get('topic'));
	// 	    $sql="SELECT * FROM `tbl_lecture` WHERE `ch_id` = $topic AND `lecture_type`= '1' AND `lect_status`='active' ";
	// 	    $topics =$this->cat_frontend->get_sql_record($sql);
	// 	    $data['topics']=$topics;
	// 	    load_user_dashboard_view('user_dashboard/chapter_topics',$data);
	// 	}else{
	// 		redirect('dashboard');
	// 	}
	// }

	// public function chapter_list()
	// {
	//     $this->checkAuth();
	//     // echo "<pre>";
	//     $sem=base64_decode($this->input->get('sem'));

	//     if(!empty($sem))
	//     {
	//     	$data['sem_id'] = $sem;
	//     }
	//     else{
	//     	$data['sem_id'] = $this->session->userdata('first_sem');
	//     }

	//     if(empty($sem)){
	//     	$sem = $this->session->userdata('first_sem');
	//     }
	//     if($sem==''){
	//     	redirect('pricing');
	//     }
	//     $sql="SELECT * FROM `tbl_chapter` WHERE `sem_id` = $sem AND `ch_status`='active'";
	//     $chapter_lists = $this->cat_frontend->get_sql_record($sql);
	//     foreach ($chapter_lists as $key=> $value) {
	//     	$lacunas =$this->cat_frontend->get_sql_record("SELECT `tbl_lecture`.*, `tbl_lecture_type`.`type_name`, count(`tbl_lecture`.`lect_id`) as `count`  FROM `tbl_lecture` JOIN `tbl_lecture_type` ON `tbl_lecture_type`.`type_id`=`tbl_lecture`.`lecture_type` where `tbl_lecture`.`ch_id`='".$value['ch_id']."' AND `tbl_lecture`.`lect_status`='active' group by `tbl_lecture`.`lecture_type`");
	//     	// print_r($lacunas);
	//     	$practicetest = $this->cat_frontend->get_sql_record("SELECT count(`test_id`)as `test` FROM `tbl_test`  WHERE `test_type` = 3 AND `chapter_id` = '".$value['ch_id']."' AND `test_status`='active'");
	//     	foreach ($practicetest as $pkey => $pvalue) {
	//     		// echo $pvalue['test'];
	//     		$chapter_lists[$key]['practicetest']=$pvalue['test'];
	//     	}

	//     	$onlinetest = $this->cat_frontend->get_sql_record("SELECT count(`test_id`)as `test` FROM `tbl_test`  WHERE `test_type` = 4 AND `chapter_id` = '".$value['ch_id']."' AND `test_status`='active'");
	//     		// echo "<pre>"; print_r($onlinetest); echo "</pre>";
	//     	foreach ($onlinetest as $okey => $ovalue) {
	//     		$chapter_lists[$key]['onlinetest']=$ovalue['test'];
	//     	}
	//     	foreach ($lacunas as $lkey => $lvalue) {
	//     		$chapter_lists[$key][$lvalue['type_name']]=$lvalue['count'];
	//     		$chapter_lists[$key][$lvalue['type_name'].'_lect_id']=$lvalue['lect_id'];
	//     	}
	//     		// echo "<pre>"; print_r($chapter_lists); echo "</pre>";

	//     }
	//     // print_r($this->session->all_userdata());
	//     $userid = $this->session->userdata('id');
	//     $sql21="SELECT `product_order_id` FROM `product_orders` WHERE `order_user_id` LIKE $userid AND `order_products` LIKE $sem AND `order_status` = 'Completed' AND `end_date` < CURRENT_TIMESTAMP() ORDER BY `product_order_id`";
	// 	$student_sems = $this->cat_frontend->get_sql_record($sql21);
	// 	$data['final_sem_test']=1;
	// 	if($student_sems){
	// 		// echo "ok";
	// 		$data['final_sem_test']=0;
	// 	    $sql="SELECT * FROM `tbl_test` WHERE `test_type`='4' AND `sem_id` = $sem";
	// 	    $data['sem_test'] =$this->cat_frontend->get_sql_record($sql);
	// 	}

	//     	// print_r($chapter_lists);
	//     // exit;
	//     	// echo "</pre>";
	//     $data['chapter_lists']= $chapter_lists;
	// 	load_user_dashboard_view('user_dashboard/chapter_list', $data);
	// }

 	public function logout()
    {
		$user_details = array(
		'id' => '' ,
		'username' => '' ,
		'full_name' => '',
		'validated' => true
		);
		$this->session->unset_userdata($user_details);
		$this->session->sess_destroy();
		$url = base_url();
		redirect($url);
		exit();
  	}

 //  	public function view_analysis()
 //  	{
 //  		$this->checkAuth();
 //  		$user_id = $this->session->userdata('id');
 //  		if ($user_id) 
 //  		{
 //            $get_semid = base64_decode($this->input->get('sem'));
 //            if(empty($get_semid))
 //            {
 //            	$get_semid = $this->session->userdata('first_sem');
 //            }

 //            $ddata=$this->test_progress_tab($user_id, $get_semid);
	// 	    $data['total_frienship_points'] = $ddata['total_frienship_points'];
	// 		$data['solved_practiceshits'] = $ddata['solved_practiceshits'];
	// 		$data['practice_shit_points'] = $ddata['practice_shit_points'];
	// 		$data['solved_online_test'] = $ddata['solved_online_test'];
	// 		$data['online_shit_points'] = $ddata['online_shit_points'];
	// 		$data['solved_adaptive_test'] = $ddata['solved_adaptive_test'];
	// 		$data['adaptive_shit_points'] = $ddata['adaptive_shit_points'];
	// 		$data['total_practiceshit'] = $ddata['total_practiceshit'];
	// 		$data['total_onlineshit'] = $ddata['total_onlineshit'];
	// 		$data['total_adaptive_test'] = $ddata['total_adaptive_test'];
	// 		/* For Accuracy Graph By Ankit Chouksey*/
	// 		$data['solved_online_test_data'] = $this->accu_graph();
	// 		/* For Accuracy Graph End*/

 //            $whr = array('sem_id'=>$get_semid);
 //            $first = $this->cat_frontend->getwheres('tbl_chapter', $whr);
 //            // echo $this->db->last_query();
	// 		if(!empty($first))
	// 		{
	// 			// echo "<pre>";print_r($first);
	// 			// echo "</pre>";
	//             foreach ($first as $val)
	//             {
	//                $trr = $val['ch_id'];
	//                $whr1 = array('chapter_id'=>$trr);
	//                $second = $this->cat_frontend->getwheres('tbl_test', $whr1);

	//                // echo $this->db->last_query();
	//                	if(true)
	//                	// if(empty($second))
	//                	{
	// 	               	$trr = $val['ch_id'];
	// 	                $whr = array('tbl_lecture.ch_id'=>$trr);
	// 					$secondss = $this->cat_frontend->get_data_twotable_column_where('tbl_lecture','tbl_test','lect_id','lecture_id','',$whr,'');
	// 					// echo $this->db->last_query();
	//     				$user_id = $this->session->userdata('id');
	//                		foreach ($secondss as $datas) 
	//                		{
	// 	               		$tes = $datas['test_id'];
	// 	               		$wher4 = array('user_id'=>$user_id,'test_id'=>$tes,'status'=>'completed');
	// 	               		$fourth[] = $this->cat_frontend->getwheres('tbl_user_test', $wher4);
	// 	               	}
	//                	}
	// 	           	if(!empty($second))
	// 	            {
	//                		$user_id = $this->session->userdata('id');
	//                		foreach ($second as $datas)
	//                		{
	//                			$tes = $datas['test_id'];
	//                			$wher4 = array('user_id'=>$user_id,'test_id'=>$tes,'status'=>'completed');
	//                			$third[] = $this->cat_frontend->getwheres('tbl_user_test', $wher4);
	//                		}
	//                			// echo $this->db->last_query();
	//                	}
	//             }
	//             $opt_data = array();
 //               	if(!empty($third))
 //               	{
	//                	foreach ($third as $tst_details) 
	//   				{
	//   					$test_id = $tst_details[0]['test_id'];
	// 		  			$get_test_type = "SELECT * FROM `tbl_chapter` INNER JOIN `tbl_test` ON `tbl_chapter`.`ch_id`=`tbl_test`.`chapter_id` where `tbl_test`.`test_id` = '".$tst_details[0]['test_id']."'";
	// 		           	$test_data=$this->cat_frontend->get_sql_record($get_test_type);
	// 			        if(!empty($test_data))
	// 			        {
	// 			        	$attempt = array('test_id'=>$test_id,'user_id'=>$user_id,'attempted'=>'yes');
	// 			  			$question_attemp = $this->cat_frontend->getwheres_count('tbl_user_ques_answer',$attempt);
	// 			  			$attempted = $this->cat_frontend->getwheres('tbl_user_ques_answer',$attempt);
	// 			  			$score = 0;
	// 						$attempt_mark =0;
	// 						$marks_gained =0;
	// 						$marks_lost =0;
	// 						$negative_marks =0;
	// 			  			if(!empty($attempted ))
	// 			  			{
	// 							foreach ($attempted as $mark_attemp) {
	// 								$attemp_question = $mark_attemp['question_id'];
	// 								$attemp_answer = $mark_attemp['answer'];

	// 								$where_test = array('ques_id'=>$attemp_question,'test_id'=> $test_id);
	// 				  				$attempt_marks = $this->cat_frontend->getwheres('tbl_add_question',$where_test);

	// 				  				$attempt_correct = $attempt_marks[0]['ques_right_ans'];       
	// 								$attempt_que_marks = $attempt_marks[0]['que_marks']; 
	// 								$ques_negative_marks = $attempt_marks[0]['ques_negative_marks'];

	// 								$attempt_mark = $attempt_mark + $attempt_que_marks; 
	// 								if ($attemp_answer == $attempt_correct ){
	// 							       	$score++;
	// 							       	$marks_gained = $marks_gained + $attempt_que_marks;
	// 						   		}elseif ($attemp_answer !== $attempt_correct){
	// 						   			$marks_lost = $marks_lost + $attempt_que_marks;
	// 						   			$negative_marks = $negative_marks + $ques_negative_marks;
	// 						   		}
	// 							}
	//   						}

	// 				       	$test_data[0]['attmeped_question']=$question_attemp;
	// 				       	$test_data[0]['correct_question']=$score;
	// 			  			$opt_data[]=$test_data[0];
	// 			  		}
	//            		}
 //               		$data['my_testing'] = $opt_data;
 //        		}
 //        		if(!empty($fourth))
 //               	{		
	//                	foreach ($fourth as $tst_details) 
	//   				{
	//   					$test_id = $tst_details[0]['test_id'];
	// 		  			$get_test_type = "SELECT * FROM `tbl_lecture` INNER JOIN `tbl_test` ON `tbl_lecture`.`lect_id`=`tbl_test`.`lecture_id` where `tbl_test`.`test_id` = '".$tst_details[0]['test_id']."'";
	// 		           	$test_data=$this->cat_frontend->get_sql_record($get_test_type);
	// 		           	// echo "<pre>";
	// 		           	// print_r($test_data);
	// 			        if(!empty($test_data))  
	// 			        {
	// 			        	$attempt = array('test_id'=>$test_id,'user_id'=>$user_id,'attempted'=>'yes');
	// 			  			$question_attemp = $this->cat_frontend->getwheres_count('tbl_user_ques_answer',$attempt);

	// 			  			foreach ($test_data as $for_ch_name) 
	// 			  			{
	// 			  				$chapters_ids = $for_ch_name['ch_id'];
	// 			  				$get_chid = array('ch_id'=>$chapters_ids);
	// 			  				$chapt_data = $this->cat_frontend->getwheres('tbl_chapter',$get_chid);
	// 			  			}
	// 			  			$attempted = $this->cat_frontend->getwheres('tbl_user_ques_answer',$attempt);
	// 			  			$score = 0;
	// 						$attempt_mark =0;
	// 						$marks_gained =0;
	// 						$marks_lost =0;
	// 						$negative_marks =0;
	// 			  			if(!empty($attempted ))
	// 			  			{
	// 							foreach ($attempted as $mark_attemp)
	// 							{
	// 								$attemp_question = $mark_attemp['question_id'];
	// 								$attemp_answer = $mark_attemp['answer'];

	// 								$where_test = array('ques_id'=>$attemp_question,'test_id'=> $test_id);
	// 				  				$attempt_marks = $this->cat_frontend->getwheres('tbl_add_question',$where_test);

	// 				  				$attempt_correct = $attempt_marks[0]['ques_right_ans'];       
	// 								$attempt_que_marks = $attempt_marks[0]['que_marks']; 
	// 								$ques_negative_marks = $attempt_marks[0]['ques_negative_marks'];

	// 								$attempt_mark = $attempt_mark + $attempt_que_marks; 
	// 								if ($attemp_answer == $attempt_correct ) {
	// 							       	$score++;
	// 							       	$marks_gained = $marks_gained + $attempt_que_marks;
	// 						   		}elseif($attemp_answer !== $attempt_correct)
	// 						   		{
	// 						   			$marks_lost = $marks_lost + $attempt_que_marks;
	// 						   			$negative_marks = $negative_marks + $ques_negative_marks;
	// 						   		}
	// 							}
	//   						}
	// 				       	$test_data[0]['attmeped_question']=$question_attemp;
	// 				       	$test_data[0]['correct_question']=$score;
	// 				       	$test_data[0]['ch_name']=$chapt_data[0]['ch_name'];
	// 			  			$opt_data[]=$test_data[0];
	// 	               	}
	//            		}
 //               		$data['my_testing'] = $opt_data;
 //        		}
 //  			}
 //  			// echo "<pre>";
 //  			// print_r($data['my_testing']);
 //  			// die;
	//   		$where_user = array('id'=>$user_id);
	//   		$data['student_record'] = $this->cat_frontend->getwheres('tbl_students', $where_user);
	// 		load_user_dashboard_view('user_dashboard/view_analysis',$data);
 //  		}
	// }

	// /******** Start kamlesh functions ************/
	// public function ask_question()
	// {
	// 	$this->checkAuth();
	//     $std_id = $this->session->userdata('id');

	//     if ($_POST) {   	
	  		
	//     	$title = $this->input->post('title');
	//     	$description = $this->input->post('description');

	//     	$config = array(
 //                'upload_path' => "./uploads/doubt-image/maths",               
 //                'allowed_types' => "*",
 //                'overwrite' => false,
 //                'file_name' => time()
 //            );

 //            $this->load->library('upload', $config);
 //            $this->upload->initialize($config);
        
 //            if($this->upload->do_upload('image')) 
 //            {
 //                $uploadimg = $this->upload->data();
 //                $uimg = $uploadimg['file_name'];
 //            } 
 //            else
 //            {
 //                $uimg = '';
 //                // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
 //                // exit();
 //            }

	//     	$data = array(
	//     		'std_id'=>$std_id,
 //            	'title'=>$title,
 //             	'description'=>$description,
 //             	'image'=>$uimg,
 //             	'create_date'=>date('Y-m-d H:i:s'),
 //             	'status'=>'pending'
	//         );

	// 	    $result = $this->cat_frontend->insert_data('tbl_doubt_send',$data);
	// 	 	$this->session->set_flashdata('message', 'Your Request Has Been Successfully Send');
	// 		redirect("online_dcs");
	//     }

 //  		if ($std_id) {

 //  			$where = array('std_id'=>$std_id, 'status' => 'pending');
 //  			$data['doubt_details'] = $this->cat_frontend->getwheres_orderby('tbl_doubt_send',$where,'id');

	//   		$where = array('std_id'=>$std_id, 'status' => 'responded');
	//   		$data['responded_details'] = $this->cat_frontend->getwheres_orderby('tbl_doubt_send',$where,'id');

	//   		$where_user = array('std_id'=>$std_id);
	//   		$total_question = $this->cat_frontend->getwheres_count('tbl_doubt_send', $where_user);
	//   		$data['total_ask_que'] = $total_question;

	//   		$where_user = array('std_id'=>$std_id,'status' => 'responded');
	//   		$total_question = $this->cat_frontend->getwheres_count('tbl_doubt_send', $where_user);
	//   		$data['total_res_que'] = $total_question;
	  		
 //  		}

 
  		
	//     load_user_dashboard_view('user_dashboard/ask_question',$data);
	// }

	// public function back_doubt() {

	// 	$this->checkAuth();
	//     $std_id = $this->session->userdata('id');

	// 	if ($_POST) {

	// 		$title = $this->input->post('title');
	// 		$description = $this->input->post('description');
	// 		$id = $this->input->post('parent_id');

	//     	$where = array('id'=>$id);
	//   		$question_details = $this->cat_frontend->getwheres('tbl_doubt_send', $where);
	//   		if (!empty($question_details)) {

	// 	  		$data = array(
	// 	    		'std_id'=>$std_id,
	// 	    		'parent_id'=>$id,
	//             	'title'=>$title,
	//              	'description'=>$description,
	//              	'create_date'=>date('Y-m-d H:i:s'),
	//              	'status'=>'pending'
	// 	        );

	// 		    $result = $this->cat_frontend->insert_data('tbl_doubt_send',$data);
	// 		    $this->session->set_flashdata('message', 'Your Request Has Been Successfully Send');
	// 			redirect("online_dcs");
	//   		}
	// 	}
	// }

	// public function edit_ask_question()
	// {
	// 	$this->checkAuth();
	//     $std_id = $this->session->userdata('id');
	//     $ques_id = base64_decode($this->uri->segment(2));

	//     if ($_POST) {

	//     	$title = $this->input->post('title');
	//     	$description = $this->input->post('description');

	//     	if(!empty($_FILES['image']['name']))
 //            {

 //               $config = array(
 //                'upload_path' => "./uploads/doubt-image/maths",
 //                'allowed_types' => "*",
 //                'overwrite' => false,
 //                'file_name' => time()
 //                );

 //                $this->load->library('upload', $config);
 //                $this->upload->initialize($config);
        
 //                if($this->upload->do_upload('image')) 
 //                {
 //                    $uploadimg = $this->upload->data();
 //                    $uimg = $uploadimg['file_name'];
                    
 //                } 
 //                else
 //                {
 //                    $uimg = '';
 //                    // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
 //                    // exit();
 //                }
 //            }
 //            else
 //            {
 //                $uimg = $this->input->post('image_old');
 //            }

	//     	$data = array(
	//     		'std_id'=>$std_id,
 //            	'title'=>$title,
 //             	'description'=>$description,
 //             	'image'=>$uimg,
 //             	'create_date'=>date('Y-m-d H:i:s'),
 //             	'status'=>'pending'
	//         );

	// 	   $wheres = array('id'=>$ques_id);
 //           $this->cat_frontend->update_data('tbl_doubt_send',$data,$wheres);
	// 	 	$this->session->set_flashdata('message', 'Your Request Has Been Successfully Send');
	// 		redirect("online_dcs");
	//     }

 //  		if ($std_id) {

 //  			$where = array('std_id'=>$std_id, 'status' => 'pending');
 //  			$data['doubt_details'] = $this->cat_frontend->getwheres_orderby('tbl_doubt_send',$where,'id');

	//   		$where = array('std_id'=>$std_id, 'status' => 'responded');
	//   		$data['responded_details'] = $this->cat_frontend->getwheres_orderby('tbl_doubt_send',$where,'id');

	//   		$where_user = array('std_id'=>$std_id);
	//   		$total_question = $this->cat_frontend->getwheres_count('tbl_doubt_send', $where_user);
	//   		$data['total_ask_que'] = $total_question;

	//   		$where_user = array('std_id'=>$std_id,'status' => 'responded');
	//   		$total_question = $this->cat_frontend->getwheres_count('tbl_doubt_send', $where_user);
	//   		$data['total_res_que'] = $total_question;
	  		
 //  		}
 //  		$wheres = array('id'=>$ques_id);
 //  		$data['edit_doubt'] = $this->cat_frontend->getwheres('tbl_doubt_send',$wheres);
  		
	//     load_user_dashboard_view('user_dashboard/edit_ask_question',$data);
	// }

	// public function online_dcs()
	// {
	// 	$this->checkAuth();
	//     $std_id = $this->session->userdata('id');

 //  		if ($std_id) {

 //  			$where = array('std_id'=>$std_id,'parent_id'=>'0', 'status' => 'pending');
 //  			$data['doubt_details'] = $this->cat_frontend->getwheres_orderby('tbl_doubt_send',$where,'id');

	//   		$where = array('std_id'=>$std_id,'status' => 'responded');
	//   		$data['responded_details'] = $this->cat_frontend->getwheres_orderby('tbl_doubt_send',$where,'id');

	//   		$where_user = array('std_id'=>$std_id);
	//   		$total_question = $this->cat_frontend->getwheres_count('tbl_doubt_send', $where_user);
	//   		$data['total_ask_que'] = $total_question;

	//   		$where_user = array('std_id'=>$std_id,'status'=>'responded');
	//   		$total_question = $this->cat_frontend->getwheres_count('tbl_doubt_send', $where_user);
	//   		$data['total_res_que'] = $total_question;

	//   		$get_semid=$this->session->userdata('first_sem');
	//   		// $userid=$this->session->userdata('userid');
	//   		$ddata=$this->test_progress_tab($std_id, $get_semid);
	//   		$data['total_frienship_points'] = $ddata['total_frienship_points'];
	//   		$data['practice_shit_points'] = $ddata['practice_shit_points'];
	//   		$data['online_shit_points'] = $ddata['online_shit_points'];
	//   		$data['adaptive_shit_points'] = $ddata['adaptive_shit_points'];
 //  		}
  		
	//     load_user_dashboard_view('user_dashboard/online_dcs',$data);
	// }

	// public function view_question()
	// {
	// 	$this->checkAuth();
	//     $std_id = $this->session->userdata('id');
	//     $doubt_id = base64_decode($this->uri->segment(2));

 //  		if ($std_id) {

 //  			$where = array('id'=>$doubt_id);
	//   		$doubt_details = $this->cat_frontend->getwheres('tbl_doubt_send',$where);
	//   		$parent_id = $doubt_details[0]['parent_id'];
	//   		if ($parent_id != '0') {

	//   			$where = array('id'=>$parent_id);
	//   			$data['doubt_details'] = $this->cat_frontend->getwheres('tbl_doubt_send',$where);

	//   			$wheres = array('doubt_id'=>$parent_id);
	//   			$data['ans_details'] = $this->cat_frontend->getwheres('tbl_doubt_responce',$wheres);
	//   		}else{

	//   			$where = array('id'=>$doubt_id);
	//   			$data['doubt_details'] = $this->cat_frontend->getwheres('tbl_doubt_send',$where);
	//   			$wheres = array('doubt_id'=>$doubt_id);
	//   			$data['ans_details'] = $this->cat_frontend->getwheres('tbl_doubt_responce',$wheres);
	//   			// print_r($data['ans_details']);
	//   		}
	//   		// $sql = "SELECT `tbl_doubt_send`.*,`tbl_doubt_responce`.`answer`,`tbl_doubt_responce`.`image_ans`,`tbl_doubt_responce`.`link`,`tbl_doubt_responce`.`ans_date` FROM `tbl_doubt_send` left join `tbl_doubt_responce` on `tbl_doubt_send`.`id`=`tbl_doubt_responce`.`doubt_id` where `tbl_doubt_send`.`parent_id`!='0' AND `tbl_doubt_send`.`parent_id`=(SELECT `parent_id` as `my_id` FROM `tbl_doubt_send` WHERE `id`=$doubt_id)";
	//   		$sql = "SELECT `tbl_doubt_send`.*,`tbl_doubt_responce`.`answer`,`tbl_doubt_responce`.`image_ans`,`tbl_doubt_responce`.`link`,`tbl_doubt_responce`.`ans_date` FROM `tbl_doubt_send` left join `tbl_doubt_responce` on `tbl_doubt_send`.`id`=`tbl_doubt_responce`.`doubt_id` where `tbl_doubt_send`.`parent_id`!='0' AND `tbl_doubt_send`.`parent_id`= $doubt_id";
 //  			$res_data = $this->db->query($sql);
 //  			$join_data = $res_data->result_array();
 //  			$data['my_doubt'] = $join_data;

		    
	//   			// echo $this->db->last_query();
	//   		$wheres = array('id'=>$doubt_id,'parent_id'=>$parent_id,'status'=>'pending');
	//   		$data['doubt'] = $this->cat_frontend->getwheres('tbl_doubt_send',$wheres);
		  		
	//   	}
	//     load_user_dashboard_view('user_dashboard/view_question',$data);
	// }

	// public function teacher_online_dcs()
	// {
	// 	$this->checkAuth();		
	//     $teacher_id = $this->session->userdata('id');
	//     $role = $this->session->userdata('role');
	//     // echo $role;die;

 //  		if ($role == 'Teacher') {

 //  			$where = array('status' => 'pending');
 //  			$data['doubt_details'] = $this->cat_frontend->getwheres_orderby('tbl_doubt_send',$where,'id');

	//   		$where = array('status' => 'responded');
	//   		$data['responded_details'] = $this->cat_frontend->getwheres_orderby('tbl_doubt_send',$where,'id');

	//   		// $where_user = array('std_id'=>$std_id);
	//   		// $total_question = $this->cat_frontend->getwheres_count('tbl_doubt_send', $where_user);
	//   		// $data['total_ask_que'] = $total_question;

	//   		// $where_user = array('std_id'=>$std_id,'status' => 'responded');
	//   		// $total_question = $this->cat_frontend->getwheres_count('tbl_doubt_send', $where_user);
	//   		// $data['total_res_que'] = $total_question;
	  		
	//     	load_user_dashboard_view('user_dashboard/teacher_online_dcs',$data);
 //  		}else{
  		
	// 	    load_user_dashboard_view('user_dashboard/teacher_online_dcs');
	// 	}
	// }

	// public function send_response()
	// {
	// 	//$this->checkAuth();
	// 	// echo "string";die();
	//     $teacher_id = $this->session->userdata('id');
	//     $doubt_id = base64_decode($this->uri->segment(2));

	//     if ($_POST) {

	//     	$title = $this->input->post('title');
	//     	$answer = $this->input->post('answer');
	//     	$link = $this->input->post('link');

	//     	$config = array(
 //                'upload_path' => "./uploads/ans-image/maths",               
 //                'allowed_types' => "*",
 //                'overwrite' => false,
 //                'file_name' => time()
 //            );

 //            $this->load->library('upload', $config);
 //            $this->upload->initialize($config);
        
 //            if($this->upload->do_upload('image_ans')) 
 //            {
 //                $uploadimg = $this->upload->data();
 //                $uimg = $uploadimg['file_name'];
 //            } 
 //            else
 //            {
 //                $uimg = '';
 //                // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
 //                // exit();
 //            }

	//     	$data = array(
	//     		'teacher_id'=>$teacher_id,
	//     		'doubt_id'=>$doubt_id,
 //            	'title'=>$title,
 //             	'answer'=>$answer,
 //             	'image_ans'=>$uimg,
 //             	'link'=>$link,
 //             	'ans_date'=>date('Y-m-d H:i:s'),
 //             	'status'=>'responded'
	//         );

	//         $doubt_data = array(
	//              	'status' => 'responded'
	// 	        );

	// 	    $result = $this->cat_frontend->insert_data('tbl_doubt_responce',$data);
	// 	    if ($result) {

	// 	    	$doubt_data = array(
	//              	'status'=>'responded'
	// 	        );

	// 	    	$where = array('id'=>$doubt_id);
	// 	    	$this->cat_frontend->update_data('tbl_doubt_send',$doubt_data,$where);
	// 	    }		    
	// 	 	$this->session->set_flashdata('message', 'Your Responce Has Been Successfully Send');
	// 		redirect("teacher_online_dcs");
	//     }

	//     $where = array('id'=>$doubt_id);
 //  		$doubt_details = $this->cat_frontend->getwheres('tbl_doubt_send',$where);
 //  		$parent_id = $doubt_details[0]['parent_id'];
 //  		if ($parent_id != '0') {

 //  			$where = array('id'=>$parent_id);
 //  			$data['doubt_details'] = $this->cat_frontend->getwheres('tbl_doubt_send',$where);

 //  			$wheres = array('doubt_id'=>$parent_id);
 //  			$data['ans_details'] = $this->cat_frontend->getwheres('tbl_doubt_responce',$wheres);

 //  		}else{

 //  			$where = array('id'=>$doubt_id);
 //  			$data['doubt_details'] = $this->cat_frontend->getwheres('tbl_doubt_send',$where);

 //  			// $wheres = array('doubt_id'=>$doubt_id);
 //  			// $data['ans_details'] = $this->cat_frontend->getwheres('tbl_doubt_responce',$wheres);
 //  		}

 //  		$sql = "SELECT `tbl_doubt_send`.*,`tbl_doubt_responce`.`answer`,`tbl_doubt_responce`.`image_ans`,`tbl_doubt_responce`.`link`,`tbl_doubt_responce`.`ans_date` FROM `tbl_doubt_send` left join `tbl_doubt_responce` on `tbl_doubt_send`.`id`=`tbl_doubt_responce`.`doubt_id` where `tbl_doubt_send`.`parent_id`=(SELECT `parent_id` as `my_id` FROM `tbl_doubt_send` WHERE `id`=$doubt_id)";
	// 	$res_data = $this->db->query($sql);
	// 	$join_data = $res_data->result_array();
	// 	$data['my_doubt'] = $join_data;
	    
 //  		$wheres = array('id'=>$doubt_id,'parent_id'=>$parent_id,'status'=>'pending');
 //  		$data['doubt'] = $this->cat_frontend->getwheres('tbl_doubt_send',$wheres);
 //  		// echo "<pre>";
 //  		// print_r($data['doubt']);
 //  		// die;

	//     load_user_dashboard_view('user_dashboard/send_response',$data);
	// }

	// public function view_responce()
	// {
	// 	$this->checkAuth();
	//     // $std_id = $this->session->userdata('id');
	//     $doubt_id = base64_decode($this->uri->segment(2));

	// 	$where = array('id'=>$doubt_id);
 //  		$doubt_details = $this->cat_frontend->getwheres('tbl_doubt_send',$where);
 //  		$parent_id = $doubt_details[0]['parent_id'];
 //  		if ($parent_id != '0') {

 //  			$where = array('id'=>$parent_id);
 //  			$data['doubt_details'] = $this->cat_frontend->getwheres('tbl_doubt_send',$where);

 //  			$wheres = array('doubt_id'=>$parent_id);
 //  			$data['ans_details'] = $this->cat_frontend->getwheres('tbl_doubt_responce',$wheres);

 //  			$sql = "SELECT `tbl_doubt_send`.*,`tbl_doubt_responce`.`answer`,`tbl_doubt_responce`.`image_ans`,`tbl_doubt_responce`.`link`,`tbl_doubt_responce`.`ans_date` FROM `tbl_doubt_send` left join `tbl_doubt_responce` on `tbl_doubt_send`.`id`=`tbl_doubt_responce`.`doubt_id` where `tbl_doubt_send`.`parent_id`=(SELECT `parent_id` as `my_id` FROM `tbl_doubt_send` WHERE `id`=$doubt_id)";
 //  			$res_data = $this->db->query($sql);
 //  			$join_data = $res_data->result_array();
 //  			$data['my_doubt'] = $join_data;

 //  		}else{

 //  			$where = array('id'=>$doubt_id);
 //  			$data['doubt_details'] = $this->cat_frontend->getwheres('tbl_doubt_send',$where);

 //  			$wheres = array('doubt_id'=>$doubt_id);
 //  			$data['ans_details'] = $this->cat_frontend->getwheres('tbl_doubt_responce',$wheres);

 //  		}

  		
	  		
 //  		load_user_dashboard_view('user_dashboard/view_responce',$data);
	// }

	// /******** End of functions by Kamlesh ************/

 	public function summary_analysis($in='',$test_id='')
	{
		$this->checkAuth();
		$data=array();
		if($test_id==''){
	    	$test_id = $this->input->get('test_id');
		}
		$this->checkAnalysisCond($test_id);
		
	    $data['test_id'] = $test_id;
  		$user_id = $this->session->userdata('id');
  		if($user_id) 
  		{
  			$data['testdata'] = $this->cat_frontend->getwheres('tbl_test',array('test_id'=>$test_id));
  			$data['testtime'] = $this->cat_frontend->getwheres('tbl_user_test',array('test_id'=>$test_id,'user_id'=>$user_id));

  			// $where = array('test_id'=>$test_id,'user_id'=>$user_id);
  			// $getdata = $this->cat_frontend->getwheres('tbl_user_ques_answer',$where);
  			// $data['answer_details'] = $getdata;

  			$attempt = array('test_id'=>$test_id,'user_id'=>$user_id,'attempted'=>'yes');
  			$question_attemp = $this->cat_frontend->getwheres_count('tbl_user_ques_answer',$attempt);
  			$data['question_attemp'] = $question_attemp;

  			$attempted = $this->cat_frontend->getwheres('tbl_user_ques_answer',$attempt);
  			$data['attempted'] = $attempted; 
  			$total_time_taken = 0;
  			if(!empty($attempted))/* Time per question */
  			{
  				foreach ($attempted as $value_time) 
  				{
  					$que_marks = $value_time['ques_time'];
  					$hr  =	substr($que_marks ,0,1).'<br>';
  					$min =	substr($que_marks ,2,2).'<br>';
  					$sec =	substr($que_marks ,-2).'<br>';
  					$seconds = 3600 * $hr + 60 * $min + $sec;
  					$total_time_taken += $seconds;
  					// $total_time_taken = $total_time_taken + $que_marks;
  				}
  					// echo gmdate("H:i:s", $total_time_taken);
  					// die;
  				$data['total_time_taken'] = $total_time_taken/60;	
  			}
  			// print_r($data['question_attemp']);
  			// die(); 			
  			  			
  			$wheres = array('test_id' => $test_id,'ques_status' => 'active');
  			$total_question = $this->cat_frontend->getwheres('tbl_add_question',$wheres);
  			$data['total_question'] = count($total_question);
  			$total_ques_marks = 0;
  			$sem1_total_ques = 0;
  			$sem2_total_ques = 0;
  			$sem3_total_ques = 0;
  			$sem4_total_ques = 0;
  			if (!empty($total_question))
  			{
  				foreach ($total_question as $value) 
  				{
  					$que_marks = $value['que_marks'];
  					$total_ques_marks = $total_ques_marks + $que_marks;  
  					if($value['ques_concept_wise']==1){$sem1_total_ques++;}
  					if($value['ques_concept_wise']==2){$sem2_total_ques++;}
  					if($value['ques_concept_wise']==3){$sem3_total_ques++;}
  					if($value['ques_concept_wise']==4){$sem4_total_ques ++;}
  				}
			    $data['total_ques_marks'] = $total_ques_marks;			       	
  			}
			$entdata=array();
			$entdata[1]['total_que']=$sem1_total_ques;
			$entdata[2]['total_que']=$sem2_total_ques;
			$entdata[3]['total_que']=$sem3_total_ques;
			$entdata[4]['total_que']=$sem4_total_ques;

			$score = 0;
			$attempt_mark =0;
			$attempt_que_num =0;
			$marks_gained =0;
			$marks_lost =0;
			$negative_marks =0;
			// $entdata[0]['right_ans']=0;
			// $entdata[0]['wrong_ans']=0;
			$entdata[1]['right_ans']=0;
			$entdata[1]['wrong_ans']=0;
			$entdata[2]['right_ans']=0;
			$entdata[2]['wrong_ans']=0;
			$entdata[3]['right_ans']=0;
			$entdata[3]['wrong_ans']=0;
			$entdata[4]['right_ans']=0;
			$entdata[4]['wrong_ans']=0;
			if(!empty($attempted))
  			{
				foreach ($attempted as $mark_attemp) 
				{
					$attemp_question = $mark_attemp['question_id'];
					$attemp_answer = $mark_attemp['answer'];

					$where = array('ques_id'=>$attemp_question,'test_id'=> $test_id,'ques_status' => 'active');
	  				$attempt_marks = $this->cat_frontend->getwheres('tbl_add_question',$where);

	  				$ques_concept_wise = $attempt_marks[0]['ques_concept_wise'];       
	  				$attempt_correct = $attempt_marks[0]['ques_right_ans'];       
					$attempt_que_marks = $attempt_marks[0]['que_marks']; 
					$ques_negative_marks = $attempt_marks[0]['ques_negative_marks']; 

					/***************Check Multiple Questions******/

					$check_typos = $attempt_marks[0]['question_type'];


					if($check_typos == 2){


					$where2 = array('question_id'=>$attemp_question,'deleted'=>'0');

					$query_for_multi_select = $this->cat_frontend->getwheres('tbl_multiple_choice_questions',$where2);
					
					if(!empty($query_for_multi_select)){

						$final_checks = '';

						foreach($query_for_multi_select as $res_one){


							$final_checks .= $res_one['answer_id'].',';

						}

						$attempt_correct = rtrim($final_checks,',');


					}
				}
				
					$ques_negative_marks=floatval(str_replace("-", "", $ques_negative_marks));

					$attempt_mark = $attempt_mark + $attempt_que_marks; 
					if ($attemp_answer == $attempt_correct ) 
					{
					   	$score++;
				       	$marks_gained = $marks_gained + $attempt_que_marks;
				       	$o=@$entdata[$ques_concept_wise]['right_ans'];
				       	$o++;
	       				$entdata[$ques_concept_wise]['right_ans']=$o;
			   		}elseif ($attemp_answer !== $attempt_correct)
			   		{
			   			$marks_lost = $marks_lost + $attempt_que_marks;
			   			$negative_marks = $negative_marks + $ques_negative_marks;
			   			$o=@$entdata[$ques_concept_wise]['wrong_ans'];
				       	$o++;
	       				$entdata[$ques_concept_wise]['wrong_ans']=$o;
	       				// exit('ossada');
			   		}
			   		$attempt_que_num++;
				}
			}

	       	$data['entdata'] = $entdata;
	       	$data['correct_ans'] = $score;
	       	$data['que_marks'] = $attempt_mark;
	       	$data['marks_lost'] = $marks_lost;	       	
	       	$data['marks_gained'] = $marks_gained - $negative_marks;
	       	
	       	$correct_ans=$data['correct_ans'];
	       	$total_question=count($total_question);
	       	$attempt_que_num;
	       	$incorrect_ans=$attempt_que_num-$correct_ans;
	       	if($correct_ans==0){
	       		$accurracy=0;
	       	}else{
	       		$accurracy = ( $correct_ans / $attempt_que_num ) * 100;
	       	}
	       	$total_ques_marks;
	       	$total_marks_gained = $marks_gained - $negative_marks;
	       	$lecture_id= $data['testdata'][0]['lecture_id'];
	       	$chapter_id= $data['testdata'][0]['chapter_id'];
	       	$sem_id= $data['testdata'][0]['sem_id'];
	       	// echo $this->db->last_query();
	       	if(isset($lecture_id) && $lecture_id!='0'){
	       		$ledata1=$this->db->query("SELECT `ch_id` FROM `tbl_lecture` WHERE `lect_id` = $lecture_id ");
	       		$ledata1_row=$ledata1->row();
	       		$chapter_id=$ledata1_row->ch_id;

	       		$ledata2=$this->db->query("SELECT `sem_id` FROM `tbl_chapter` WHERE `ch_id` = $ledata1_row->ch_id ");
	       		$ledata2_row=$ledata2->row();
	       		$sem_id=$ledata2_row->sem_id;
	       	}
	       	if(isset($chapter_id) && $chapter_id!='0'){
	       		$ledata2=$this->db->query("SELECT `sem_id` FROM `tbl_chapter` WHERE `ch_id` = $chapter_id ");
	       		$ledata2_row=$ledata2->row();
	       		$sem_id=$ledata2_row->sem_id;
	       	}

	       	// print_r($data['testdata']);
	       	$tr_data=array('tr_test_id'=>$test_id,'tr_user_id'=>$user_id,'tr_lecture_id'=>$lecture_id,'tr_chapter_id'=>$chapter_id,'tr_sem_id'=>$sem_id,'tr_test_type'=>$data['testdata'][0]['test_type'],'tr_test_name'=>$data['testdata'][0]['test_name'],'tr_total_que'=>$total_question,'tr_attempt_que'=>$attempt_que_num,'tr_right_ans'=>$correct_ans,'tr_wrong_ans'=>$incorrect_ans,'tr_accuracy'=>$accurracy,'tr_total_score'=>$total_ques_marks,'tr_my_score'=>$total_marks_gained);
	       	$tr_where = array('tr_test_id'=>$test_id,'tr_user_id'=>$user_id);
  			$tr_res_data=$this->cat_frontend->getwheres('tbl_test_result',$tr_where);
  			if($in=='1'){
  				return $tr_data;
	       	}
	       	// var_dump($tr_res_data);
	       	if ($tr_res_data) {
	       		$this->cat_frontend->update_data('tbl_test_result',$tr_data,$tr_where);
	       	}else{
	       		$result = $this->cat_frontend->insert_data('tbl_test_result',$tr_data);
	       	}
	       	// echo "<pre>";
	       	// echo $this->db->last_query();
	       	// // echo 2-floatval('.5');
	       	// $o=$entdata[1]['right_ans'];$o++;
	       	// echo $entdata[1]['right_ans']=$o;
	       	// echo $entdata[1]['right_ans'];
	       	// print_r($data);
	       	// echo "</pre>";
		    load_user_dashboard_view('user_dashboard/summary_analysis2',$data);
  		}
	}
	
	public function checkAnalysisCond($tst_id){

		$tr_where = array('test_id'=>$tst_id);

		$tr_res_data=$this->cat_frontend->getwheres('tbl_test',$tr_where);

		if(!empty($tr_res_data)){

			$check_choices = $tr_res_data[0]['choice'];

			if($check_choices == 2){

			redirect('my-courses');
		}

		}

	}

	public function view_description()
	{
		$this->checkAuth();
	    $std_id = $this->session->userdata('id');
	    $test_id = $this->input->get('test_id');

	    $where = array('test_id'=>$test_id,'ques_status'=>'active');
		$data['question_details'] = $this->cat_frontend->getwheres('tbl_add_question',$where,'');
		
  		 //print_r($data['question_details']);die;
	    load_user_dashboard_view('user_dashboard/view_description',$data);
	}

	// public function test_video_description()
	// {
	// 	$this->checkAuth();
	//     $std_id = $this->session->userdata('id');
	//     $test_id = $this->input->get('test_id');

	//     $where = array('test_id'=>$test_id);
	// 	$data['question_details'] = $this->cat_frontend->getwheres('tbl_test',$where);	
 //  		// echo "<pre>";
 //  		// print_r($data['question_details']);
 //  		// die;
	//     load_user_dashboard_view('user_dashboard/video_description',$data);
	// }

	public function depth_analysis()
	{
		$test_id = $this->input->get('test_id');
	    $data['test_id'] = $test_id;
		load_user_dashboard_view('user_dashboard/depth_analysis.php',$data);

	}

	// public function ajax_accurracy() 
	// {
	//     $test_id = $this->input->get('test_id');
	// 	$user_id = $this->session->userdata('id');
	// 	if ($user_id)
	// 	{
	// 		$where = array('test_id'=>$test_id);
	// 		// $getdata = $this->cat_frontend->getwheres('tbl_user_ques_answer',$where);
	// 		$getdata = $this->cat_frontend->getwheres('tbl_add_question',$where);

	// 		$all_question = count($getdata);
	// 		$attempt = array('test_id'=>$test_id,'user_id'=>$user_id,'attempted'=>'yes');
	// 		$attempt_details = $this->cat_frontend->getwheres('tbl_user_ques_answer',$attempt);
	// 		if(!empty($attempt_details))
	// 		{
	// 			$attempted = count($attempt_details);
	// 			$right = 0;
	// 			$mydata = array();
	// 			foreach ($attempt_details as $mark_attemp) 
	// 			{
	// 				$attemp_question = $mark_attemp['question_id'];
	// 				$attemp_answer = $mark_attemp['answer'];
	// 				$where = array('ques_id'=>$attemp_question,'test_id'=> $test_id);
	// 				$attempt_marks = $this->cat_frontend->getwheres('tbl_add_question',$where);
					
	// 				$attempt_correct = $attempt_marks[0]['ques_right_ans'];       
	// 				if ($attemp_answer == $attempt_correct ) {
	// 					$right++;
	// 				}
	// 				$accurracy1 = $all_question - $attempted;
	// 				$accurracy2 = $right;
	// 				$accurracy3 = $attempted - $right;
	// 				//$mydata[] = $accurracy;
	// 			}
	// 			$datas = array('unattempted'=>$accurracy1,'right'=>$accurracy2,'wrong'=>$accurracy3);
	// 			echo json_encode($datas);
	// 		}else{
	// 			$datas = array('unattempted'=>0,'right'=>0,'wrong'=>0);
	// 			echo json_encode($datas);
	// 		}
	// 	}
	// }

	public function ajax_accurracy() 
	{
	    $test_id = $this->input->get('test_id');
		$user_id = $this->session->userdata('id');
		if ($user_id)
		{
			$where = array('test_id'=>$test_id,'ques_status'=>'active');
			// $getdata = $this->cat_frontend->getwheres('tbl_user_ques_answer',$where);
			$getdata = $this->cat_frontend->getwheres('tbl_add_question',$where);

			$all_question = count($getdata);
			$attempt = array('test_id'=>$test_id,'user_id'=>$user_id,'attempted'=>'yes');
			$attempt_details = $this->cat_frontend->getwheres('tbl_user_ques_answer',$attempt);
			if(!empty($attempt_details))
			{
				$attempted = count($attempt_details);
				$right = 0;
				$mydata = array();
				foreach ($attempt_details as $mark_attemp) 
				{
					$attemp_question = $mark_attemp['question_id'];
					$attemp_answer = $mark_attemp['answer'];
					$where = array('ques_id'=>$attemp_question,'test_id'=> $test_id,'ques_status'=>'active');
					$attempt_marks = $this->cat_frontend->getwheres('tbl_add_question',$where);

					$mult_get_type = $attempt_marks[0]['question_type'];

					if($mult_get_type == 2){


					$where2 = array('question_id'=>$attemp_question,'deleted'=>'0');

					$query_for_multi_select = $this->cat_frontend->getwheres('tbl_multiple_choice_questions',$where2);
					
					if(!empty($query_for_multi_select)){

						$final_checks = '';

						foreach($query_for_multi_select as $res_one){


							$final_checks .= $res_one['answer_id'].',';

						}

						$attempt_correct = rtrim($final_checks,',');


					} 
						} else {
					

								$attempt_correct = $attempt_marks[0]['ques_right_ans'];  

							}


					if ($attemp_answer == $attempt_correct ) {
						$right++;
					}
					$accurracy1 = $all_question - $attempted;
					$accurracy2 = $right;
					$accurracy3 = $attempted - $right;
					//$mydata[] = $accurracy;
				}
				$datas = array('unattempted'=>$accurracy1,'right'=>$accurracy2,'wrong'=>$accurracy3);
				echo json_encode($datas);
			}else{
				$datas = array('unattempted'=>0,'right'=>0,'wrong'=>0);
				echo json_encode($datas);
			}
		}
	}


	// public function difficulty_level()
	// {
	// 	$x = 0;
	//     $score = 0;
	//     $test_id = $this->input->get('test_id');
	// 	$user_id = $this->session->userdata('id');
	// 	$where = array('user_id' =>$user_id,'test_id'=>$test_id);
	// 	$purchase_pid = $this->cat_frontend->getwheres('tbl_user_ques_answer',$where);
	// 	// print_r($purchase_pid);
	// 	// die;
	//    	$get_levels = $this->cat_frontend->get_distinct_wheres('tbl_add_question',array('test_id'=>$test_id),'ques_difficulty_level');   
	//  	// print_r($get_levels);
	//  	//echo $this->db->last_query();
	// 	// die;
	//    	if(!empty($get_levels))
	// 	{
	// 		$test = array();
	// 		foreach($get_levels as $level)
	// 		{
	// 			$my_level = $level['ques_difficulty_level'];
	// 			 $sql = "SELECT * FROM `tbl_user_ques_answer` inner join `tbl_add_question` on `tbl_user_ques_answer`.`question_id`=`tbl_add_question`.`ques_id` where `ques_difficulty_level`= $my_level and `answer` = `ques_right_ans` and `user_id` = $user_id and `tbl_add_question`.`test_id`=$test_id";
	// 			$count_answers = $this->cat_frontend->get_sql_record($sql);
	// 			$level['correct_answers']=COUNT($count_answers);
	// 			// print_r($level['correct_answers']);
	// 			// die;
	// 			// $test[] = $level;
	// 			$sql_wrong = "SELECT * FROM `tbl_user_ques_answer` inner join `tbl_add_question` on `tbl_user_ques_answer`.`question_id`=`tbl_add_question`.`ques_id` where `ques_difficulty_level`= $my_level and `answer` != `ques_right_ans` and `user_id` = $user_id and `tbl_add_question`.`test_id`=$test_id";
	// 			$count_answers_wrong = $this->cat_frontend->get_sql_record($sql_wrong);
	// 			$level['answers_wrong']=COUNT($count_answers_wrong);
	// 			// $test1[] = $level;
	// 			$where_ques = array('test_id'=>$test_id,'ques_difficulty_level'=>$my_level);
	// 			$total_questions = $this->cat_frontend->getwheres('tbl_add_question',$where_ques);
	// 			// echo $this->db->last_query();
	// 			// echo COUNT($total_questions);
	// 			$sum2=COUNT($total_questions);
	// 			$sum1 = $level['answers_wrong'] + $level['correct_answers'] ;
	// 			$level['un_attempt']=$sum2-$sum1;
	// 			$encode_data[] = $level;
	// 		}
	// 		echo json_encode($encode_data);
	// 	}
	// 	// redirect("catalyser_main2/test2");
	// }

	// public function see_difficulty_level()
	// {
	// 	$test_id = $this->input->get('test_id');
	// 	$data['test_id'] = $test_id;

	// 	load_user_dashboard_view('user_dashboard/difficulty_level',$data);
	// }

	// public function topic_wise_analysis()
	// {

	// 	$x = 0;
	// 	$score = 0;

	// 	//$test_id = '7';
	// 	$test_id = $this->input->get('test_id');

	// 	$user_id=$this->session->userdata('id');

	// 	$where = array('user_id' =>$user_id,'test_id'=>$test_id);
	// 	$purchase_pid = $this->cat_frontend->getwheres('tbl_user_ques_answer',$where);

	// 	$get_levels = $this->cat_frontend->get_distinct_wheres('tbl_add_question',array('test_id'=>$test_id),'lecture_id');


	// 	if(!empty($get_levels))
	// 	{
	// 		$test = array();
	// 		$test5 = array();
	// 		foreach($get_levels as $level)
	// 		{

	// 			$my_level = $level['lecture_id'];

	// 			$where_lec = array('lect_id' =>$my_level);
	// 			$lect_name = $this->cat_frontend->getwheres('tbl_lecture',$where_lec);

	// 			if(!empty($lect_name))
	// 			{
	// 				foreach ($lect_name as $lect)
	// 				{
	// 					$level['lecture_name'] =  $lect['lect_name'];
	// 				}
	// 			}
	// 			$test5[] = $level;

	// 			//print_r($lecture_name['lecture_name']);

	// 			$sql = "SELECT * FROM `tbl_user_ques_answer` inner join `tbl_add_question` on `tbl_user_ques_answer`.`question_id`=`tbl_add_question`.`ques_id` where `lecture_id`= $my_level and `answer` = `ques_right_ans` and `user_id` = $user_id and `tbl_add_question`.`test_id`=$test_id";

	// 			$count_answers = $this->cat_frontend->get_sql_record($sql);
	// 			$level['correct_answers']=COUNT($count_answers);
	// 			// $test[] = $level;

	// 			$sql_wrong = "SELECT * FROM `tbl_user_ques_answer` inner join `tbl_add_question` on `tbl_user_ques_answer`.`question_id`=`tbl_add_question`.`ques_id` where `lecture_id`= $my_level and `answer` != `ques_right_ans` and `user_id` = $user_id and `tbl_add_question`.`test_id`=$test_id";

	// 			$count_answers_wrong = $this->cat_frontend->get_sql_record($sql_wrong);
	// 			$level['answers_wrong']=COUNT($count_answers_wrong);
	// 			// $test1[] = $level;


	// 			$where_ques = array('test_id'=>$test_id,'lecture_id'=>$my_level);
	// 			$total_questions = $this->cat_frontend->getwheres('tbl_add_question',$where_ques);
	// 			$sum2=COUNT($total_questions);

	// 			$sum1 = $level['answers_wrong'] + $level['correct_answers'] ;
	// 			$level['un_attempt']=$sum2-$sum1;
	// 			$encode_data[] = $level;
	// 		}
	// 		echo json_encode($encode_data);
	// 	}
	// }
	

	// public function check_topic_analysis()
	// {
	// 	$test_id = $this->input->get('test_id');
	// 	$data['test_id'] = $test_id;
	// 	load_user_dashboard_view('user_dashboard/topic_analysis',$data);
	// }



	public function question_wise_analysis()
	{
		$x = 0;
		$score = 0;
		$test_id = $this->input->get('test_id');
		$data['test_id'] = $test_id;
		if($this->session->userdata('id')) 
		{
			$user_id = $this->session->userdata('id');
			$where = array('user_id' =>$user_id,'test_id'=>$test_id);
			$data['questions_answers'] = $this->cat_frontend->getwheres('tbl_user_ques_answer',$where);
			$sql_check_answers = "SELECT `tbl_user_ques_answer` . * , `tbl_add_question`.`ques_right_ans` FROM `tbl_user_ques_answer` JOIN `tbl_add_question` ON `tbl_add_question`.`ques_id` = `tbl_user_ques_answer`.`question_id` WHERE `tbl_user_ques_answer`.`test_id` =$test_id AND `tbl_user_ques_answer`.`user_id` = $user_id AND  `tbl_add_question`.`ques_status` = 'active'";
			$data['count_answers_check'] = $this->cat_frontend->get_sql_record($sql_check_answers);
			$questions_answers_given = $data['questions_answers']; 
			$data['answered'] = $questions_answers_given[0]['answer'];
			$q_id = $questions_answers_given[0]['question_id'];
			$wheres = array('ques_id'=>$q_id,'test_id'=>$test_id,'ques_status' => 'active');
		
			$ques_data = $this->cat_frontend->getwheres('tbl_add_question',$wheres);

				/*************Multiple**********/

			$get_type =  $ques_data[0]['question_type'];

			if($get_type==2){

				$wheres_multiple = array('question_id'=>$q_id,'deleted'=>'0');

				$ques_data_multiple = $this->cat_frontend->getwheres('tbl_multiple_choice_questions',$wheres_multiple,'');

				if(!empty($ques_data_multiple)){

					$m_correct_ans = '';

					foreach($ques_data_multiple as $m_ansers){

						$m_correct_ans .= $m_ansers['answer_id'].',';
					} 

					$data['correct'] = rtrim($m_correct_ans,','); 

					$data['question_name']=$ques_data[0]['question_desc'];

					$data['right_explaination']=$ques_data[0]['explain_right_answer'];

					$data['ans_1']=$ques_data[0]['ans_1'];
					$data['ans_2']=$ques_data[0]['ans_2'];
					$data['ans_3']=$ques_data[0]['ans_3'];
					$data['ans_4']=$ques_data[0]['ans_4'];

					$data['question_image']=$ques_data[0]['question_image'];
					$data['ans_1_image']=$ques_data[0]['ans_1_image'];
					$data['ans_2_image']=$ques_data[0]['ans_2_image'];
					$data['ans_3_image']=$ques_data[0]['ans_3_image'];
					$data['ans_4_image']=$ques_data[0]['ans_4_image'];
					$data['right_ans_exp_img']=$ques_data[0]['right_ans_exp_img'];

					$data['corect_answer_value'] = $data['correct'];

					$data['my_answered'] = $data['answered'];

					if ($data['answered'] == $data['correct'] ) 
					{
						$data['status']=1;

					}else{ 

						$data['status']=0;
					}

				}

			} else {

			$data['correct'] = $ques_data[0]['ques_right_ans']; 
			
			$data['question_name']=$ques_data[0]['question_desc'];
			
			$data['right_explaination']=$ques_data[0]['explain_right_answer'];
			
			$data['ans_1']=$ques_data[0]['ans_1'];
			$data['ans_2']=$ques_data[0]['ans_2'];
			$data['ans_3']=$ques_data[0]['ans_3'];
			$data['ans_4']=$ques_data[0]['ans_4'];

			$data['question_image']=$ques_data[0]['question_image'];
			$data['ans_1_image']=$ques_data[0]['ans_1_image'];
			$data['ans_2_image']=$ques_data[0]['ans_2_image'];
			$data['ans_3_image']=$ques_data[0]['ans_3_image'];
			$data['ans_4_image']=$ques_data[0]['ans_4_image'];
			$data['right_ans_exp_img']=$ques_data[0]['right_ans_exp_img'];
			
			$data['corect_answer_value'] = $data['correct'];
			
			$data['my_answered'] = $data['answered'];

			if ($data['answered'] == $data['correct'] ) 
			{
				$data['status']=1;
			}else{ 
				$data['status']=0;
			}

		}



			// $data['correct'] = $ques_data[0]['ques_right_ans']; 
			// $data['question_name']=$ques_data[0]['question_desc'];
			// $data['right_explaination']=$ques_data[0]['explain_right_answer'];
			// $data['ans_1']=$ques_data[0]['ans_1'];
			// $data['ans_2']=$ques_data[0]['ans_2'];
			// $data['ans_3']=$ques_data[0]['ans_3'];
			// $data['ans_4']=$ques_data[0]['ans_4'];
			// $data['corect_answer_value'] = $ques_data[0]['ans_'.$data['correct']];
			// $data['my_answered'] = $ques_data[0]['ans_'.$data['answered']];

			// if ($data['answered'] == $data['correct'] ) 
			// {
			// 	$data['status']=1;
			// }else{ 
			// 	$data['status']=0;
			// }


			$sql_notin = "SELECT * FROM  `tbl_add_question` WHERE `test_id`= $test_id AND ques_status='active' AND    `tbl_add_question` .`ques_id` NOT IN (SELECT `question_id` FROM `tbl_user_ques_answer`) order by `ques_id` ASC";
			$data['count_not_attempted'] = $this->cat_frontend->get_sql_record($sql_notin);
		}


		load_user_dashboard_view('user_dashboard/question_wise',$data);
	}

	// public function see_recorded_answers()
	// {
	// 	$x = 0;
	// 	$score = 0;
	// 	if($_POST)
	// 	{
	// 		$question_id = $this->input->post('question_id');
	// 		$question_no = $this->input->post('question_no');
	// 		$data['ques_no'] = $question_no;
	// 		$user_id=$this->session->userdata('id');
	// 		$where = array('user_id' =>$user_id,'question_id'=>$question_id);
	// 		$purchase_pid = $this->cat_frontend->getwheres('tbl_user_ques_answer',$where);
	// 		$answered = $purchase_pid[0]['answer'];
	// 		$q_id = $question_id;
	// 		$wheres = array('ques_id'=>$q_id);
	// 		$ques_data = $this->cat_frontend->getwheres('tbl_add_question',$wheres);
	// 		$data['question_name']=strip_tags($ques_data[0]['question_desc'],'<img><p><sub></sub><sup></sup><br>');
	// 		$data['right_explaination']=$ques_data[0]['explain_right_answer'];
	// 		$data['ans_1']=strip_tags($ques_data[0]['ans_1'],'<img><sub></sub><sup></sup><br>');
	// 		$data['ans_2']=strip_tags($ques_data[0]['ans_2'],'<img><sub></sub><sup></sup><br>');
	// 		$data['ans_3']=strip_tags($ques_data[0]['ans_3'],'<img><sub></sub><sup></sup><br>');
	// 		$data['ans_4']=strip_tags($ques_data[0]['ans_4'],'<img><sub></sub><sup></sup><br>');
	// 		if(!empty($answered))
	// 		{
	// 			$data['my_answered'] = strip_tags($ques_data[0]['ans_'.$answered],'<img><sub></sub><sup></sup><br>');
	// 		}
	// 		else
	// 		{
	// 			$data['my_answered']='';
	// 		}
	// 		$data['give_answered'] = $answered;
	// 		$correct = $ques_data[0]['ques_right_ans']; 
	// 		$data['corect_answer'] = $correct;
	// 		$data['corect_answer_value'] = strip_tags($ques_data[0]['ans_'.$correct],'<img><sub></sub><sup></sup><br>');
	// 		if ($answered == $correct ) {
	// 			$score++;
	// 			$acolor = 'green' ;
	// 		}
	// 		else {
	// 			$acolor = 'red' ;
	// 		}
	// 		$x++;
	// 	}
	// 	echo json_encode($data);
	// } 

	public function see_recorded_answers()
	{
		$x = 0;
		$score = 0;
		if($_POST)
		{
			
			$question_id = $this->input->post('question_id');
			
			$question_no = $this->input->post('question_no');
			
			$data['ques_no'] = $question_no;

			$user_id=$this->session->userdata('id');
			
			$where = array('user_id' =>$user_id,'question_id'=>$question_id);
			
			$purchase_pid = $this->cat_frontend->getwheres('tbl_user_ques_answer',$where);
			
			$answered = $purchase_pid[0]['answer'];
			
			$q_id = $question_id;
			
			$wheres = array('ques_id'=>$q_id);
			
			$ques_data = $this->cat_frontend->getwheres('tbl_add_question',$wheres);

			/***********Added By Shubham Multiple********/

			$typs_quest = $ques_data[0]['question_type'];

			if($typs_quest == 2){

				$data['question_name']=strip_tags($ques_data[0]['question_desc'],'<img><p><sub></sub><sup></sup><br>');
				
				$data['right_explaination']=$ques_data[0]['explain_right_answer'];
				
				$data['ans_1']=strip_tags($ques_data[0]['ans_1'],'<img><sub></sub><sup></sup><br>');
				
				$data['ans_2']=strip_tags($ques_data[0]['ans_2'],'<img><sub></sub><sup></sup><br>');
				
				$data['ans_3']=strip_tags($ques_data[0]['ans_3'],'<img><sub></sub><sup></sup><br>');
				
				$data['ans_4']=strip_tags($ques_data[0]['ans_4'],'<img><sub></sub><sup></sup><br>');
				
				if(!empty($answered))
				{

					$combines_var = '';

					$explode_answers = explode(',',$answered);

					foreach($explode_answers as $resutlss){

						$combines_var .= $ques_data[0]['ans_'.$resutlss].',';

					}

					$fins_used = rtrim($combines_var,',');

					$data['my_answered'] = string_replaces($answered);

				}
				else
				{
					$data['my_answered']='';
				}
				
				$data['give_answered'] = $answered;


				$wheres_multiple = array('question_id'=>$q_id,'deleted'=>'0');

				$ques_data_multiple = $this->cat_frontend->getwheres('tbl_multiple_choice_questions',$wheres_multiple,'');

				if(!empty($ques_data_multiple)){

					$m_correct_ans = '';


					foreach($ques_data_multiple as $m_ansers){

						$m_correct_ans .= $m_ansers['answer_id'].',';


					} 


				}

				$correct = rtrim($m_correct_ans,','); 
				
				$data['corect_answer'] = $correct;
				
				$data['corect_answer_value'] = string_replaces($correct);
				
				if ($answered == $correct ) {
					$score++;
					$acolor = 'green' ;
				}
				
				else {
					
					$acolor = 'red' ;
				}
				
				$x++;

			} else {
			
				$data['question_name']=strip_tags($ques_data[0]['question_desc'],'<img><p><sub></sub><sup></sup><br>');
				
				$data['right_explaination']=$ques_data[0]['explain_right_answer'];
				
				$data['ans_1']=strip_tags($ques_data[0]['ans_1'],'<img><sub></sub><sup></sup><br>');
				
				$data['ans_2']=strip_tags($ques_data[0]['ans_2'],'<img><sub></sub><sup></sup><br>');
				
				$data['ans_3']=strip_tags($ques_data[0]['ans_3'],'<img><sub></sub><sup></sup><br>');
				
				$data['ans_4']=strip_tags($ques_data[0]['ans_4'],'<img><sub></sub><sup></sup><br>');

				if (!empty($ques_data[0]['question_image'])) {
					$data['question_image']= "<a target='_blank' href=".base_url()."uploads/test_question_images/".$ques_data[0]['question_image']."><img width='70px;' src=".base_url()."uploads/test_question_images/".$ques_data[0]['question_image']."></a>";
				}else{
					$data['question_image'] = '';
				}

				if (!empty($ques_data[0]['ans_1_image'])) {
					$data['ans_1_image']= "<a target='_blank' href=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['ans_1_image']."><img width='70px;' src=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['ans_1_image']."></p>";
				}else{
					$data['ans_1_image'] = '';
				}

				if (!empty($ques_data[0]['ans_2_image'])) {
					$data['ans_2_image']= "<a target='_blank' href=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['ans_2_image']."><img width='70px;' src=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['ans_2_image']."></a>";
				}else{
					$data['ans_2_image'] = '';
				}

				if (!empty($ques_data[0]['ans_3_image'])) {
					$data['ans_3_image']= "<a target='_blank' href=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['ans_3_image']."><img width='70px;' src=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['ans_3_image']."></a>";
				}else{
					$data['ans_3_image'] = '';
				}
				if (!empty($ques_data[0]['ans_4_image'])) {
					$data['ans_4_image']= "<a target='_blank' href=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['ans_4_image']."><img width='70px;' src=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['ans_4_image']."></a>";
				}else{
					$data['ans_4_image'] = '';
				}

				if (!empty($ques_data[0]['right_ans_exp_img'])) {
					$data['right_ans_exp_img']= "<a target='_blank' href=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['right_ans_exp_img']."><img width='70px;' src=".base_url()."uploads/test_questions_ans_images/".$ques_data[0]['right_ans_exp_img']."></a>";
				}else{
					$data['right_ans_exp_img'] = '';
				}

				if(!empty($answered))
				{
					//$data['my_answered'] = strip_tags($ques_data[0]['ans_'.$answered],'<img><sub></sub><sup></sup><br>');
					$data['my_answered'] = string_replaces($answered);
				}
				else
				{
					$data['my_answered']='';
				}
				
				$data['give_answered'] = $answered;
				
				$correct = $ques_data[0]['ques_right_ans']; 
				
				$data['corect_answer'] = $correct;
				
				//$data['corect_answer_value'] = strip_tags($ques_data[0]['ans_'.$correct],'<img><sub></sub><sup></sup><br>');
				
				$data['corect_answer_value'] = string_replaces($correct);
				
				if ($answered == $correct ) {
					$score++;
					$acolor = 'green' ;
				}
				
				else {
					$acolor = 'red' ;
				}
				
				$x++;


		}

		}
		
		echo json_encode($data);
	}

	// public function accu_graph(){
	// 	$userid= $this->session->userdata('id');
	// 	$sem= base64_decode($this->input->get('sem'));
	// 	if($sem==''){
	// 		$sem=$this->session->userdata('first_sem');
	// 	}

	// 	$where_test_re = array("tr_user_id"=>$userid,'tr_sem_id'=>$sem,'tr_test_type'=>'4');
	//     $tbl_test_result = $this->cat_frontend->getwheres('tbl_test_result',$where_test_re);
	//     // echo $this->db->last_query();
	    
 //    	// $testdata=$this->summary_analysis(1);
 //    	// echo json_encode($tbl_test_result);
 //    	// echo "<pre>";  print_r($tbl_test_result); echo "</pre>";
 //    	return $tbl_test_result;
	// }
	public function test_dashboard()
  	{
  		$this->checkAuth();
	    $std_id = $this->session->userdata('id');
	    $user_class = $this->session->userdata('user_class');


	    $data['course_name']=$user_class;
	    if ($user_class=='7') {
	    	$class='1';
	    }
	    // echo "<pre>";print_r($this->session->all_userdata());die;
	    // echo "</pre>";
	    $user_class_id_rs=$this->db->query("SELECT `sem_id` FROM `tbl_semester` WHERE `sem_name` LIKE '%$user_class%' ");
        $user_class_id_data=$user_class_id_rs->result_array();
	    // echo $this->db->last_query();
        $user_class_id=$user_class_id_data[0]['sem_id'];
        // echo "<pre>";print_r($user_class_id_data);die;

        $given_test_list=$this->db->query("SELECT * FROM `tbl_test_result` WHERE `tr_user_id` = $std_id AND `deleted` = '0'");
        $data['given_test_list']=$given_test_list->result_array();

	    // echo "<pre>";
	    // echo $this->db->last_query();
	    // print_r($data['given_test_list']);die;
	    $a=0;
	    $notin='';
	    $notin1='';
	    foreach ($data['given_test_list'] as $key => $value) {
	    	if ($a==0) {
	    		$notin =$value['tr_test_id'];
	    	}else{
	    		$notin .=','.$value['tr_test_id'];
	    	}
	    	$a++;
	    }
	    // echo $notin;
	    if ($notin!='') {
	    	$notin1=" AND `test_id` NOT IN($notin)";
	    }
	    $date=date('Y-m-d H:i:s');
	    $test_list=$this->db->query("SELECT * FROM `tbl_test` WHERE `test_status` = 'active' AND `sem_id`= $user_class_id  AND `appear_time` <= '$date' AND `disappear_time` >= '$date' $notin1");
	    
        $data['tests']=$test_list->result_array();

        /**********Expired Tests*********/

        $test_list_expiry=$this->db->query("SELECT * FROM `tbl_test` WHERE `test_status` = 'active' AND `sem_id`= $user_class_id  AND `disappear_time` < '$date' $notin1");


        $data['tests_expires']=$test_list_expiry->result_array();


        //echo $this->db->last_query(); die;
        // echo "<pre>"; print_r($data['tests']); die;
	    
	    $test_count=$this->db->query("SELECT * FROM `tbl_test` WHERE `test_status` = 'active' AND `sem_id`= $user_class_id ");

        $data['test_count']=$test_count->result_array();

  		load_user_dashboard_view('user_dashboard/view_analysis_ashwamegha',$data);
  	}
/* End Of Class*/
}