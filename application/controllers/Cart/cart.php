<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        $this->load->helper('text');
        $this->load->helper('cart_helper');
        
    }

    public function chk_login()
    {
    	$UserId = $this->session->userdata('id');

    	if(empty($UserId))
    	{
    		redirect('home','refresh');
    		die();
    	}
    }


    public function index()
    {
       $userId = $this->session->userdata('id');
       $data['CartInfo'] = $this->home_model->getdata_orderby_where_join2('cart_items','products','product_id','product_id',array('user_id'=>$userId,'status'=>'active'),'','cart_id','');
     
        $this->load_view('cart/cart_view',$data);
    }

    public function my_cart()
    {

    //	$this->chk_login();

       $userId = $this->session->userdata('id');

        if(!empty($userId))
       {
            $finalUserId = $userId;
       }
       else
       {
            $finalUserId = $this->session->userdata('session_id');
       }

             $where = array('user_id'=>$finalUserId,'status'=>'active');
             $data['CartInfo'] = $this->home_model->get_data_orderby_where('cart_items','cart_id',$where,'ASC');
      
       
       if(!empty($data['CartInfo']) && count($data['CartInfo'])>0)
       {
       		foreach($data['CartInfo'] as $crt)
       		{
       			$prod_type[]  = $crt->product_type;
       		}

       		$data['ProductType'] = array_unique($prod_type);
       }

        $this->load_view('cart/my-cart',$data);
    }

    public function add_new_elearning_items_to_cart()
    {
    	$postData = $this->input->post();

        $userId = $this->session->userdata('id');
    	if(!empty($postData))
        {
            $cartItems = array('user_id'   => $userId,
                            'product_id'   => $postData['product_id'],
                            'product_type' => $postData['product_type'],
                            'p_quantity'   => $postData['qty'],
                            'actual_price' => $postData['product_price'],
                            'created_at'   => date('Y-m-d H:i:s'),
                            'course_for'   => $postData['course_for'],

                            );

            $this->home_model->insert_data('cart_items',$cartItems);


            $updatestDetails = array('f_name'   =>  $postData['f_name'],
                                     'l_name'   =>  $postData['l_name'],
                                     'address'  =>  $postData['address'],
                                     'pincode'  =>  $postData['pincode'],
                                     'mobile'   =>  $postData['mobile_no'],
                                     'city'     =>  $postData['city']
                                 );


            $this->home_model->update_data('tbl_students',$updatestDetails,array('id'=>$userId));

            echo "SUCCESS";
        }   
    }

    public function update_cart_item()
    {
        $cart_id      = $this->input->post('cart_id');
        $qty          = $this->input->post('qty');

        $userId = $this->session->userdata('id');


        if(!empty($userId))
        {
            $finalUserId = $userId;
        }
        else
        {
            $finalUserId = $this->session->userdata('session_id');
        }

        if (!empty($finalUserId)) {
            $where = array('cart_id'=>$cart_id);
            $update_qty = array('p_quantity'=>$qty);
            $this->home_model->update_data('cart_items',$update_qty,$where);

            $exist = $this->home_model->get_data_orderby_where('cart_items','',$where,'');
            $actual_price = $exist[0]->actual_price;
            $p_quantity   = $exist[0]->p_quantity;
            $msg = "SUCCESS";
            $total_price = $actual_price*$p_quantity;
        
            $item_count = $this->home_model->cart_item_sum_sql($userId);
            if ($item_count[0]->total_qty != NULL) {
                $item_count = $item_count[0]->total_qty;
            } else {
              $item_count = 0;
            }
            $data = array('msg'=>$msg);
            $arr  = json_encode($data);
            print_r($arr);
            exit;
        }else{
            $data = array('msg'=>'WRONG');
            $arr  = json_encode($data);
            print_r($arr);
            exit;
        }
    }


    public function delete_cart_item($cart_id="")
    {   
            if(!empty($cart_id))
            {
                $where = array('cart_id'=>$cart_id);
                $this->home_model->delete_data('cart_items',$where);
                redirect(base_url('my-cart'));
            }
    }



    public function add_new_product_items_to_cart()
    {
        $postData = $this->input->post();

        $userId = $this->session->userdata('id');

        if(empty($userId))
        {
            $finalUserId = $this->session->userdata('session_id');
        }
        else
        {
            $finalUserId = $userId;
        }

        if(!empty($postData))
        {
                $procartItems = array('user_id'   => $finalUserId,
                            'product_id'   => $postData['product_id'],
                            'product_type' => $postData['product_type'],
                            'p_quantity'   => 1,
                            'actual_price' => $postData['product_price'],
                            'created_at'   => date('Y-m-d H:i:s'),
                            );

            $this->home_model->insert_data('cart_items',$procartItems);

            echo "SUCCESS";
        }
        else
        {
            echo "Not_Login";
        }   
    }


    public function get_cart_count()
    {
        $UserId = $this->session->userdata('id');

        if(empty($UserId))
        {
            $finalUserId = $this->session->userdata('session_id');
        }
        else
        {
            $finalUserId = $UserId;
        }

        if(!empty($finalUserId))
        {
            $cartCount = $this->home_model->cart_item_sum_sql($finalUserId);

            if(!empty($cartCount)>0)
            {
                echo $cartCount[0]->total_qty;
            }
            else
            {
                echo "0";
            }
        }
    }   


}