<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('front_end/cat_frontend');
        $this->load->model('cat_frontend_model','cat_frontend');
        $this->load->helper('main_template_helper');
        $this->load->helper('cart_helper');
         $this->load->model('home_model');
        
    }



     public function chk_login()
    {
        $UserId = $this->session->userdata('id');

        if(empty($UserId))
        {
            redirect('home','refresh');
            die();
        }
    }

    public function checkout_details()
    {
        $this->chk_login();
        $UserId = $this->session->userdata('id');
        $data['DeliveryAddress'] =  $this->home_model->getwhere_data('user_address',array('user_id'=>$UserId,'deleted'=>'0'),'','user_address_id','');

        $data['States'] =  $this->home_model->getwhere_data('cities',array(),'','city_id','city_state');

        $this->load_view('checkout/Select-delivery-mode',$data);
    }

      public function place_checkout_order()
    {
        $this->chk_login();
        $postData = $this->input->post();
        $userId = $this->session->userdata('id');
        if (!empty($postData))
        {
            $CartDetails = $this->home_model->getwhere_data('cart_items',array('user_id'=>$userId,'status'=>'active'),'','cart_id','');

            if (!empty($CartDetails) && count($CartDetails) > 0)
            {
                $sub_total = 0;
                $qty = 0;
                foreach($CartDetails as $cartDetails)
                {
                    $sub_total+= $cartDetails->p_quantity * $cartDetails->actual_price;
                    $qty+= $cartDetails->p_quantity;
                }

                $appliedCouponData = $this->session->userdata('couponData');
                if (!empty($appliedCouponData))
                {
                    $couponId = $this->session->userdata('code_id');
                    if ($appliedCouponData['discount_type'] == "percent")
                    {
                        $percentData = ($sub_total * $appliedCouponData['discount_amount']) / 100;
                        $final = $sub_total - $percentData;
                    }
                    else
                    {
                        $final = $sub_total - $appliedCouponData['discount_amount'];
                    }
                }
                else
                {
                    $final = $sub_total;
                    $couponId = 0;
                }
            }

            $data['deliveryaddress'] = $this->cat_frontend->getwheres('user_address', array(
                'user_id' => $userId,
                'user_address_id' => $postData['delivery_id']
            ));
            $data_order_insert = array(
                'order_code' => 'PRODUCT' . time() ,
                'order_user_id' => $userId,
                'order_products' => $qty,
                'order_amount' => $final,
                'coupon_id' => $couponId,
                'delivery_id' => $postData['delivery_id'],
                'order_status' => 'pending',
                'create_date' => date('Y-m-d H:i:s')
            );
            $order_id = $this->cat_frontend->insert_data('product_orders', $data_order_insert);
            if (!empty($order_id))
            {
                if (!empty($CartDetails) && count($CartDetails) > 0)
                {
                    foreach($CartDetails as $result)
                    {
                        $newData = array(
                            'product_order_id' => $order_id,
                            'product_id' => $result->product_id,
                            'product_quantity' => $result->p_quantity,
                            'product_type' => $result->product_type,
                            'product_price' => $result->actual_price
                        );
                        $this->cat_frontend->insert_data('product_order_details', $newData);
                        $newData = "";
                    }
                }
            }

            $data['amount']       = $final;
           // $data['order_id']     = base64_encode($order_id);
              $data['order_id']     = $order_id;
            $data['product_type'] = "product/e_learning/programe";
            
            $this->load_view('checkout/place_checkout_order',$data);
        }
    }

    public function place_order_programe()
    {
        // $postData = $this->input->post();
        $userId = $this->session->userdata('id');
        if (!empty($userId))
        {
            $where = array('user_id' => $userId,'product_type' => 'programe','cart_items.status' => 'active');
            $data['cart_items'] = $this->cat_frontend->get_data_orderby_where('cart_items','',$where,'');

            $CartDetails = $this->cat_frontend->getdata_orderby_where_join2('cart_items', 'tbl_programs', 'product_id', 'id',$where  , '', 'id', '');
            // echo "<pre>";print_r($CartDetails);die;

            if (!empty($CartDetails) && count($CartDetails) > 0)
            {
                $sub_total = 0;
                $qty = 0;
                foreach($CartDetails as $cartDetails)
                {
                    $sub_total+= $cartDetails->p_quantity * $cartDetails->actual_price;
                    $qty+= $cartDetails->p_quantity;
                }

                $appliedCouponData = $this->session->userdata('couponData');
                if (!empty($appliedCouponData))
                {
                    $couponId = $this->session->userdata('code_id');
                    if ($appliedCouponData['discount_type'] == "percent")
                    {
                        $percentData = ($sub_total * $appliedCouponData['discount_amount']) / 100;
                        $final = $sub_total - $percentData;
                    }
                    else
                    {
                        $final = $sub_total - $appliedCouponData['discount_amount'];
                    }
                }
                else
                {
                    $final = $sub_total;
                    $couponId = 0;
                }
            }

            $where = array('id'=>$userId);
            $data['deliveryaddress'] = $this->cat_frontend->getwheres('tbl_students', $where);
            // echo "<pre>";print_r($data['deliveryaddress']);die;
            // $data['deliveryaddress'] = $this->cat_frontend->getwheres('user_address', array(
            //     'user_id' => $userId,
            //     'user_address_id' => $postData['delivery_id']
            // ));

            $data_order_insert = array(
                'order_code' => 'Program-' . time() ,
                'order_user_id' => $userId,
                'order_products' => $qty,
                'order_amount' => $final,
                'coupon_id' => $couponId,
                'order_status' => 'pending',
                'create_date' => date('Y-m-d H:i:s')
            );
            $order_id = $this->cat_frontend->insert_data('product_orders', $data_order_insert);
            if (!empty($order_id))
            {
                if (!empty($CartDetails) && count($CartDetails) > 0)
                {
                    foreach($CartDetails as $result)
                    {
                        $newData = array(
                            'product_order_id' => $order_id,
                            'product_id' => $result->product_id,
                            'product_type' => $result->product_type,
                            'product_quantity' => $result->p_quantity,
                            'product_price' => $result->actual_price
                        );
                        $this->cat_frontend->insert_data('product_order_details', $newData);
                        $newData = "";
                    }
                }
            }

            $data['amount'] = $final;
            //$data['order_id'] = base64_encode($order_id);
            $data['order_id'] = $order_id;
            $data['product_type'] = "programe";
            
            $this->load_view('checkout/place_checkout_order',$data);
        }
    }

    
    public function ccv_request()
    {

        $this->load_view('checkout/ccavRequestHandler');
    }

    public function ccv_response()
    {
        $this->load_view('checkout/ccavResponseHandler');
    }

    /***********Payment Success********/

    public function success_payment()
    {
        $workingKey='B9EDC56EABC5D6BD99A004FD32CC013A';     //Working Key should be provided here.
        $encResponse=@$_POST["encResp"];//This is the response sent by the CCAvenue Server
        $rcvdString=$this->decrypt($encResponse,$workingKey); //Crypto Decryption used as per the specified working key.

        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);
    
        $keys_response=array();
        $values_response=array();
        if($dataSize>0)
        {
            for($i = 0; $i < $dataSize; $i++) 
            {
                $information=explode('=',$decryptValues[$i]);
                $keys_response[]=$information[0];
                $values_response[]=$information[1];
            }

            $response_array=array_combine($keys_response,$values_response);
        }

        if(!empty($response_array['order_status']))
        {
            $status_payment    = $response_array['order_status'];
            $order_id          = $response_array['order_id'];
            //$decode_orderid    = base64_decode($order_id);
            $decode_orderid    = $order_id;
            $tracking_id       = $response_array['tracking_id'];

         //echo "<pre>";print_r($response_array);die();
           //  echo('out');
            if($status_payment == "Success")
            {
                $update_where = array('product_order_id'=>$decode_orderid);
                $update_data  = array('order_status'=>'Completed','tracking_id'=>$tracking_id);
                $this->cat_frontend->update_data('product_orders',$update_data,$update_where);
                /*code to update eLearningCart*/
                $userId = $this->session->userdata('id');
                $eLearningProducts = $this->cat_frontend->getwheres('product_order_details', array('product_order_id'=>$decode_orderid, 'product_type'=>'e_learning' ));
                if (!empty($eLearningProducts)) {
                    foreach ($eLearningProducts as $eLearningProducts_value) {
                        /* check every product for elearning product*/
                        if ($eLearningProducts_value['product_type']=='e_learning') {
                            $product_id = $eLearningProducts_value['product_id'];
                            $singlrProdunt=$this->cat_frontend->getwheres('products', array('product_id'=>$product_id));
                            $course_id = $singlrProdunt[0]['product_id'];


                             $date  = date('Y-m-d');
                            $endDate = date('Y-m-d', strtotime("+".$singlrProdunt[0]['course_duration']." months", strtotime($date)));

                            $lastLicno = $this->home_model->get_last_license_no('e_learning_licence', 'licence_no', 1, 'licence_no');

                                     if(!empty($lastLicno) && count($lastLicno)>0)
                             {
                               $a = $lastLicno[0]->licence_no;
                            
                               if (strpos($a, LIC) !== false) 
                               {
                                preg_match("/(.*?)(\d+)$/", $a, $matches);
                                $finalLicno = $matches[1] . ($matches[2] + 1);
                               } 
                               else 
                               {
                                  $finalLicno = LIC."1001";
                               }
                            }
                            else
                            {
                                $finalLicno = LIC."1001";
                            }


                            if ($eLearningProducts_value['product_quantity']>1) {
                                $e_learning_licenceData = array('owner_id'   => $userId,
                                                'course_id'     => $course_id,
                                                'alloted_user'  => '',
                                                'order_id'      => $decode_orderid,
                                                'purchase_product_quantity' => $eLearningProducts_value['product_quantity'],
                                                'dashboard_id'  => $userId,
                                                'end_date'      => $endDate,
                                                'licence_no'    => $finalLicno
                                                );
                            }else{
                                $e_learning_licenceData = array('owner_id'   => $userId,
                                                'course_id'     => $course_id,
                                                'alloted_user'  => $userId ,
                                                'order_id'      => $decode_orderid,
                                                 'purchase_product_quantity' => $eLearningProducts_value['product_quantity'],
                                                 'dashboard_id'  => $userId,
                                                  'end_date'     => $endDate,
                                                   'licence_no'    => $finalLicno
                                                );
                            }
                            // print_r($e_learning_licenceData);
                            $this->cat_frontend->insert_data('e_learning_licence',$e_learning_licenceData);
                            $e_learning_licence='';
                        }
                    }
                }
              /*code to update eLearningCart*/

                $this->session->set_flashdata('confirm_order', 'Your program booking has been done');

                $order_code = $this->cat_frontend->getwheres('product_orders',array('product_order_id'=>$decode_orderid));

                //$userId    = $this->session->userdata('id');
                $to        = $this->session->userdata('username');
                $full_name = $this->session->userdata('full_name');

                

                /***Mail For The Server***/ 
                $config['mailtype'] = 'html';
                $config['charset'] = 'iso-8859-1';
                $config['priority'] = 1;
                $this->email->initialize($config);
                $this->email->clear();

                $product_details = $this->cat_frontend->getdata_orderby_where_join2('product_orders','product_order_details','product_order_id','product_order_id',array('product_orders.product_order_id'=>$decode_orderid),'','product_orders.product_order_id','');


                if(!empty($product_details) && count($product_details)>0)
                {
                    foreach($product_details as $proDetails)
                    {
                        $pro_type[] = $proDetails->product_type;
                    }

                    $final_pro_type = array_unique($pro_type);

                    $sub_total = 0;

                    if(in_array('e_learning',$final_pro_type) || in_array('product', $final_pro_type)  && in_array('programe', $final_pro_type))
                    {
                        $message = "<!DOCTYPE html>
                        <html>
                        <head>
                            <title>Event Booking</title>
                        </head>
                        <body>
                        <div style='width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;'>
                            <div style='background-color: #D55220;'>
                                <img src='http://demo.ujjwalpatni.com/bizversity/front_assets/logo.png' style='height: 55px;'>
                            </div>
                            <div style='font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px'>
                                <p>Dear " .$this->session->userdata('full_name'). ",</p>
                                <P>Thanks for registering for the Event <b>" .$details[0]->item_name. "</b>. Your booking id is " .$product_details[0]->order_code."</p>
                                <p>Below are your registration details.</p>";

                                foreach($product_details as $odDetails)
                                {
                                        $details = get_cart_item_details_of_program($odDetails->product_id,$userId,$odDetails->product_type);

                                        if(!empty($details))
                                        {       
                                                  $program_date = date('d-M-y H:i',strtotime($details[0]->program_date));
                                                $program_end_date = date('d-M-y H:i',strtotime($details[0]->program_end_date));


                                                        $message.="<table>

                                                        <tr>
                                                            <th style='width: 190px;height: 30px;'>Event Name</th>
                                                            <td>".$details[0]->program_name."</td>
                                                        </tr>

                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Event Location:</th>
                                                        <td>" .$details[0]->location. "</td>
                                                    </tr>
                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Event DateTime:</th>
                                                        <td>". $program_date. "</td>
                                                    </tr>
                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Event EndTime:</th>
                                                        <td>" .$program_end_date. "</td>
                                                    </tr>
                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Quantity:</th>
                                                        <td>" .$details[0]->p_quantity."</td>
                                                    </tr>
                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Price</th>
                                                        <td>Rs. " .$details[0]->actual_price. "/-</td>
                                                    </tr>
                                                </table>";
                                        }   

                                      
                                }


                                 $message.='<div style="font-size: 16px;font-family: sans-serif;text-align: left;margin-bottom: 40px; margin-right: 25px;">
                                                
                                            <p><b>Below are your Product Details</b></p>
                                            <div style="width: 100%;
                                        margin-bottom: 15px;
                                        overflow-y: hidden;
                                        -ms-overflow-style: -ms-autohiding-scrollbar;">
                                                <table style="border-bottom: 1px solid lightgray; border-spacing: 0;">

                                                <tr>
            <th style="width: 165px;border: 1px solid lightgray;text-align: center; height: 40px;">Program Name</th>
            <th style="width: 165px;border: 1px solid lightgray;text-align: center;height: 40px;">Price</th>
            <th style="width: 165px;border: 1px solid lightgray;text-align: center;height: 40px;">Quantity</th>
        
         </tr>';

                                                foreach($product_details as $repo)
                                                {   
                                                    $details = get_cart_item_details_for_elearning_product($repo->product_id,$userId,$repo->product_type);

                                                    if(!empty($details))
                                                    {
                                                            
                                                            $message.='<tr>
                                                                <td style="width: 165px;border: 1px solid lightgray;text-align: center;height: 90px;"><b style="font-size: 20px;">'.$details[0]->pname.'</b></td>
                                                                <td style="width: 165px;border: 1px solid lightgray;text-align: center; height: 90px;">Rs. '.$details[0]->actual_price.'/-</td>
                                                                <td style="width: 165px;border: 1px solid lightgray;text-align: center;height: 90px;">'.$details[0]->p_quantity.'</td>
                                                            
                                                             </tr>';

                                                    }   
                                                }
                                                
                                                
                                            $message.='</table>
                                            </div>
                                            <table style="margin-top: 25px;">';

                                                

                                                $message.='<tr>
                                                    <th style="width: 130px;height: 30px;">Sub Total:</th>
                                                    <td>Rs. '.$product_details[0]->order_amount.'/-</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 130px;height: 30px;"">Discount:</th>
                                                    <td>Rs. 0/-</td>
                                                </tr>
                                                
                                                
                                            </table>
                                             <p style="text-align: center;"><b>Grand Total: Rs. '.$product_details[0]->order_amount.'/-</b></p>';

                                "<p style='margin:25px 0 5px; '>Thanks!</p>
                                <p style='margin:5px 0;'>Regards,</p>
                                <p style='margin:5px 0;'>Team Bizversity</p>
                            </div>
                        </div>

                        </body>
                        </html>";
                    }

                    else if(in_array('e_learning', $final_pro_type) || in_array('product',$final_pro_type))
                    {   
                            $message = '<!DOCTYPE html>';
                             $message.='<html>
                                        <head>
                                            <title>Product Booking</title>
                                        </head>
                                        <body>

                                        <div style="width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;">
                                            <div style="background-color: #D55220;">
                                                 <img src="http://demo.ujjwalpatni.com/bizversity/front_assets/logo.png" style="height: 55px;">
                                            </div>
                                            <div style="font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px">
                                                 <p>Dear ' .$this->session->userdata('full_name'). ',</p>
                                                <P>Thanks for purchasing ,  Your booking id is '.$product_details[0]->order_code.'</p>
                                            
                                            
                                            <div style="font-size: 16px;font-family: sans-serif;text-align: left;margin-bottom: 40px; margin-right: 25px;">
                                                
                                            <p><b>Below are your Product Details</b></p>
                                            <div style="width: 100%;
                                        margin-bottom: 15px;
                                        overflow-y: hidden;
                                        -ms-overflow-style: -ms-autohiding-scrollbar;">
                                                <table style="border-bottom: 1px solid lightgray; border-spacing: 0;">';

                                                foreach($product_details as $repo)
                                                {
                                                    $details = get_cart_item_details_for_elearning_product($repo->product_id,$userId,$repo->product_type);

                                                    if(!empty($details))
                                                    {
                                                            $message.='<tr>
                                                                <td style="width: 165px;border: 1px solid lightgray;text-align: center;height: 90px;"><b style="font-size: 20px;">'.$details[0]->pname.'</b></td>
                                                                <td style="width: 165px;border: 1px solid lightgray;text-align: center; height: 90px;">Rs. '.$details[0]->actual_price.'/-</td>
                                                                <td style="width: 165px;border: 1px solid lightgray;text-align: center;height: 90px;">'.$details[0]->p_quantity.'</td>
                                                            
                                                             </tr>';
                                                    }
                                                }
                                                
                                                
                                            $message.='</table>
                                            </div>
                                            <table style="margin-top: 25px;">';

                                                    
                                                $message.='<tr>
                                                    <th style="width: 130px;height: 30px;">Sub Total:</th>
                                                    <td>Rs. '.$product_details[0]->order_amount.'/-</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 130px;height: 30px;"">Discount:</th>
                                                    <td>Rs. 0/-</td>
                                                </tr>
                                                
                                                
                                            </table>
                                             <p style="text-align: center;"><b>Grand Total: Rs. '.$product_details[0]->order_amount.'/-</b></p>

                                            <p style="margin:25px 0 5px; ">Thanks!</p>
                                            <p style="margin:5px 0;">Regards,</p>
                                            <p style="margin:5px 0;">Team Ujjwal Patni</p>
                                            </div>
                                        </div>
                                        </body>
                                        </html>';
                    }

                    else
                    {   


                            $message = "<!DOCTYPE html>
                        <html>
                        <head>
                            <title>Event Booking</title>
                        </head>
                        <body>
                        <div style='width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;'>
                            <div style='background-color: #D55220;'>
                                <img src='http://demo.ujjwalpatni.com/bizversity/front_assets/logo.png' style='height: 55px;'>
                            </div>
                            <div style='font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px'>
                                <p>Dear " .$this->session->userdata('full_name'). ",</p>
                                <P>Thanks for registering for the Event <b>" .$details[0]->item_name. "</b>. Your booking id is " .$product_details[0]->order_code."</p>
                                <p>Below are your registration details.</p>";

                                foreach($product_details as $odDetails)
                                {
                                        $details = get_cart_item_details_of_program($odDetails->product_id,$userId,$odDetails->product_type);

                                        if(!empty($details))
                                        {       
                                                  $program_date = date('d-M-y H:i',strtotime($details[0]->program_date));
                                                $program_end_date = date('d-M-y H:i',strtotime($details[0]->program_end_date));


                                                        $message.="<table>

                                                         <tr>
                                                            <th style='width: 190px;height: 30px;'>Event Name</th>
                                                            <td>".$details[0]->program_name."</td>
                                                        </tr>

                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Event Location:</th>
                                                        <td>" .$details[0]->location. "</td>
                                                    </tr>
                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Event DateTime:</th>
                                                        <td>". $program_date. "</td>
                                                    </tr>
                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Event EndTime:</th>
                                                        <td>" .$program_end_date. "</td>
                                                    </tr>
                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Quantity:</th>
                                                        <td>" .$details[0]->p_quantity."</td>
                                                    </tr>
                                                    <tr>
                                                        <th style='width: 190px;height: 30px;'>Price</th>
                                                        <td>Rs. " .$details[0]->actual_price. "/-</td>
                                                    </tr>
                                                    
                                                </table>";
                                        }   

                                      
                                }

                                $message.=  "<table>
                                                <tr>
                                                        <th style='width: 190px;height: 30px;'>Total Price:</th>
                                                        <td>Rs. ".$product_details[0]->order_amount. "/-</td>
                                                    </tr>
                                                </table>
                                <p style='margin:25px 0 5px; '>Thanks!</p>
                                <p style='margin:5px 0;'>Regards,</p>
                                <p style='margin:5px 0;'>Team Bizversity</p>
                            </div>
                        </div>

                        </body>
                        </html>";
                    }
                }
                
                
               
                $from_email = "training@ujjwalpatni.com";
                $this->email->from($from_email,'Ujjwal Patni Training Programs');
                $this->email->to($to); 
                $this->email->subject('Payment Successfull');
                $this->email->set_mailtype('html');
                $this->email->message($message);  
                $send = $this->email->send();
                if($send){
                    // echo('-true');
                       // return true;
                }else{
                    // echo('-false');
                       // return false;
               }
                // echo $order_code[0]['order_code'];

               $this->db->where('user_id',$userId);
                $this->db->delete('cart_items');


               redirect('placed-order-details/'.$order_code[0]['order_code']);
            }else{
                // echo "string";die;
                $order_code = $this->cat_frontend->getwheres('product_orders',array('product_order_id'=>$decode_orderid));
                // echo "<pre>";print_r($order_code);die;
                $this->session->set_flashdata('cancel_order', 'Your program booking has been Failed.');
                /*____ Failure data ________*/ 
                $update_where = array('product_order_id'=>$decode_orderid);
                $update_data  = array('order_status'=>'Aborted','tracking_id'=>$tracking_id);
                $this->cat_frontend->update_data('product_orders',$update_data,$update_where);
                 redirect('cancelled-order-details/'.$order_code[0]['order_code']);
            }
               
        }
        // echo "ok";

    }

   
    /*******************  Payment Succes ****************************/

    /***********Payment Cancel*********/

    public function payment_cancel()
    {
        $workingKey='B9EDC56EABC5D6BD99A004FD32CC013A';     //Working Key should be provided here.
        $encResponse=$_POST["encResp"];         //This is the response sent by the CCAvenue Server
        $rcvdString=$this->decrypt($encResponse,$workingKey);       //Crypto Decryption used as per the specified working key.

        $order_status="";
        $decryptValues=explode('&', $rcvdString);


        $dataSize=sizeof($decryptValues);
    
        $keys_response=array();
        $values_response=array();
        if($dataSize>0)
        {
            for($i = 0; $i < $dataSize; $i++) 
            {
                $information=explode('=',$decryptValues[$i]);

                $keys_response[]=$information[0];
                $values_response[]=$information[1];

            }

            $response_array=array_combine($keys_response,$values_response);
        }


        if(!empty($response_array['order_status']))
        {

            $status_payment = $response_array['order_status'];
            $order_id       = $response_array['order_id'];
           // $decode_orderid = base64_decode($order_id);
             $decode_orderid = $order_id;
            $tracking_id       = $response_array['tracking_id'];


            if($status_payment=='Aborted'){

                /*********Update Staus In Coupon Usr*****/

                $update_where = array('product_order_id'=>$decode_orderid);
                $update_data = array('payment_status'=>'cancelled','tracking_id'=>$tracking_id);

                $this->cat_frontend->update_data('product_orders',$update_data,$update_where);


                $this->session->set_flashdata('cancel_order', 'Your Order booking has been cancelled');

                $order_code = $this->cat_frontend->getwheres('product_orders',array('product_order_id'=>$decode_orderid));

                redirect('cancelled-order-details/'.$order_code[0]['order_code']);
                
                }

            }
        
    }

    function encrypt($plainText,$key)
    {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
        $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $plainPad = $this->pkcs5_pad($plainText, $blockSize);
        
        if(mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
        {
              $encryptedText = mcrypt_generic($openMode, $plainPad);
              mcrypt_generic_deinit($openMode);
                        
        } 
        return bin2hex($encryptedText);
    }

    function decrypt($encryptedText,$key)
    {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText=$this->hextobin($encryptedText);
        $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
        mcrypt_generic_init($openMode, $secretKey, $initVector);
        $decryptedText = mdecrypt_generic($openMode, $encryptedText);
        $decryptedText = rtrim($decryptedText, "\0");
        mcrypt_generic_deinit($openMode);
        return $decryptedText;
        
    }
    //*********** Padding Function *********************

    function pkcs5_pad($plainText, $blockSize)
    {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }

    //********** Hexadecimal to Binary function for php 4.0 version ********

    function hextobin($hexString) 
    { 
            $length = strlen($hexString); 
            $binString="";   
            $count=0; 
            while($count<$length) 
            {       
                $subString =substr($hexString,$count,2);           
                $packedString = pack("H*",$subString); 
                if ($count==0)
            {
                $binString=$packedString;
            } 
                
            else 
            {
                $binString.=$packedString;
            } 
                
            $count+=2; 
            } 
            return $binString; 
    }


     public function placed_order_details($order_code="")
    {
        $userId = $this->session->userdata('id');
        if(!empty($userId) && !empty($order_code))
        {
            $data['OrderDetails'] = $this->cat_frontend->getdata_orderby_where_join2('product_orders','product_order_details','product_order_id','product_order_id',array('order_user_id'=>$userId,'order_status'=>'Completed','order_code'=>$order_code),'','product_orders.product_order_id','');
        }else{
            $data['OrderDetails'] = ''; 
        }

        $this->load_view('checkout/place_order_details',$data);
    }

      public function cancelled_order_details($order_code="")
    {
        $userId = $this->session->userdata('id');
        if(!empty($userId) && !empty($order_code))
        {  
            $data['OrderDetails'] = $this->cat_frontend->getdata_orderby_where_join2('product_orders','product_order_details','product_order_id','product_order_id',array('order_user_id'=>$userId,'order_status'=>'Aborted','order_code'=>$order_code),'','product_orders.product_order_id','');
            
        }else{
            $data['OrderDetails'] = ''; 
        }
            $this->load_view('checkout/place_order_details',$data);
    }



    // *****************************************************************************//


    public function get_cities_data()
    {
        $state_name = $this->input->post('state_name');

        if(!empty($state_name))
        {
            $Cities =  $this->home_model->getwhere_data('cities',array('city_state'=>$state_name),'','city_id','');

           if(!empty($Cities) && count($Cities)>0)
           {
                foreach($Cities as $cityDetails)
                {
                   echo '<option value="'.$cityDetails->city_name.'">'.$cityDetails->city_name.'</option>';
                }
           }
           else
           {
                echo '<option value="">No City Found</option>';
           }
        }
    }


    public function add_new_address()
    {
        $postData = $this->input->post();
        $userId = $this->session->userdata('id');
        if(!empty($postData))
        {
                if(!empty($postData['del_location']))
                {
                    $landmark = $postData['del_location'];
                }
                else
                {
                    $landmark = "";
                }

                $newAddress = array('user_full_name'   => $postData['del_full_name'],
                                    'mobile_no'        => $postData['del_mobile_no'],
                                    'pincode'          => $postData['del_pincode'],
                                    'flat_no'          => $postData['del_flat_no'],
                                    'street'           => $postData['del_street'],
                                    'landmark'         => $landmark,
                                    'city'             => $postData['del_city'],
                                    'state'            => $postData['del_state'],
                                    'user_id'          => $userId,
                                    'created_date'     => date('Y-m-d H:i:s')
                                    );

                $this->cat_frontend->insert_data('user_address',$newAddress);

                redirect('checkout/checkout_details','refresh');
        }
    }


    public function delete_address($encrptId ="")
    {
        $id = base64_decode($encrptId);

        if(!empty($id))
        {
            $updateData = array('deleted' => '1');
            $this->home_model->update_data('user_address',$updateData,array('user_address_id'=>$id));

            redirect('checkout/checkout_details','refresh');
        }
    }

    public function edit_address($encrptId ="")
    {
        $addressId = base64_decode($encrptId);

        if(!empty($addressId))
        {
            $userId = $this->session->userdata('id');
            $data['UserAddress'] =  $this->home_model->getwhere_data('user_address',array('user_id'=>$userId,'user_address_id'=>$addressId),'','user_address_id','');

             $data['States'] =  $this->home_model->getwhere_data('cities',array(),'','city_id','city_state');

              $data['Cities'] =  $this->home_model->getwhere_data('cities',array(),'','city_id','');

            $this->load_view('checkout/edit_address',$data);
        }
    }

    public function update_address()
    {
        $postData = $this->input->post();

        if(!empty($postData))
        {
             if(!empty($postData['del_location']))
                {
                    $landmark = $postData['del_location'];
                }
                else
                {
                    $landmark = "";
                }

                $newAddress = array('user_full_name'   => $postData['del_full_name'],
                                    'mobile_no'        => $postData['del_mobile_no'],
                                    'pincode'          => $postData['del_pincode'],
                                    'flat_no'          => $postData['del_flat_no'],
                                    'street'           => $postData['del_street'],
                                    'landmark'         => $landmark,
                                    'city'             => $postData['del_city'],
                                    'state'            => $postData['del_state']
                                );

                $this->home_model->update_data('user_address',$newAddress,array('user_address_id'=>$postData['user_address_id']));

                redirect('checkout/checkout_details','refresh');
        }
    }


    public function proceed_to_check($encrptId ="")
    {
        $this->chk_login();
        $deliveryId = base64_decode($encrptId);
        $userId = $this->session->userdata('id');

        if(!empty($deliveryId))
        {
            $data['DeliveryAddress'] = $this->cat_frontend->getwheres('user_address',array('user_id'=>$userId,'user_address_id'=>$deliveryId));

           

             $data['CartDetails'] = $this->home_model->get_data_orderby_where('cart_items','cart_id',array('user_id'=>$userId,'status'=>'active'),'ASC');

            $this->load_view('checkout/proceed_to_check_view',$data);
        }
    }



    //*********************************************************************************//
	
}
