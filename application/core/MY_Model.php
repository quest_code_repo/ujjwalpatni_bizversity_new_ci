<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_model extends CI_model {


	public function insert_data($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}
	public function update_data($table, $data, $where)
	{	
		$this->db->where($where);
		$this->db->update($table, $data);
		return true;		
	}

	public function delete_data($table,$where)
	{
		$rs=$this->db->delete($table,$where);
		
		if($rs)
		{ 
			return true; 
		} 
		else 
		{ 
			return false; 
		} 
	}
	
	function get_data_orderby_where($table, $order_clm="", $where="", $order_by="")
	{
		if ($order_by!="") {
			$this->db->order_by($order_clm, $order_by);
		}
		if ($where!="") {
			$this->db->where($where);
		}
		
		$qy=$this->db->get($table);
		return $qy->result();
	}

	function get_data_orderby_where_row($table, $clum="",  $order_clm="", $where="", $order_by="")
	{
		if ($clum!="") {
			$this->db->select($clum);
		}

		if ($order_by!="") {
			$this->db->order_by($order_clm, $order_by);
		}
		if ($where!="") {
			$this->db->where($where);
		}
		$qy=$this->db->get($table);
		return $qy->result();
	}

	public function get_data_twotable_column_where($table1,$table2,$id1,$id2,$column='',$where='',$ordercol='',$orderby='',$groupby='')
		{
		if($column !='')
		{
			$this->db->select($column);	
		}
		else
		{
			$this->db->select('*');
		}
		$this->db->from($table1);		  
		$this->db->join($table2,$table2.'.'.$id2.'='.$table1.'.'.$id1);
		if($where !='')
		{
			$this->db->where($where);	
		}

		if($ordercol !='')
		{
			$this->db->order_by($ordercol,$orderby);	
		}

		if($groupby!='')
		{
		   $this->db->group_by($groupby);
	    }
		

		$que = $this->db->get();
		return $que->result();		
	}

		public function get_sql_record($sql)
			{
				$query =$this->db->query($sql);
				return $query->result();	
			}
		public function get_sql_record_array($sql)
		   {
		       $query =$this->db->query($sql);
		       return $query->result_array();    
		   }
	/******* All Data From Table*********/ 
    public function get_table_data($table) {
        $data = $this->db->get($table);
        $get = $data->result();
        return $get;        
    }

    /******* All Data From Where Condition*********/ 
    public function getwheres_data($table, $where) {
        $this->db->where($where);
        $data = $this->db->get($table);
        $get = $data->result();
        return $get;        
    }	   
 
		
}
