 <?php
class Cat_frontend extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    
    /************Inser Data*************/
    public function insert_data($table, $data)
    {
        $this->db->insert($table, $data);
        $num = $this->db->insert_id();
        return $num;
    }
    
    /*************Update Data*************/
    public function update_data($table, $data, $where)
    {
        $this->db->where($where);
        $rs = $this->db->update($table, $data);
        if ($rs) {
            return true;
        } else {
            return false;
        }
    }
    
    /************* get all Record as where class *************** */
    
    public function getwheres($table, $where, $limit ='')
    {
        if ($limit!='') {
            $this->db->limit($limit);
        }
        $this->db->where($where);
        $data = $this->db->get($table);
        $get  = $data->result_array();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }
    }
    
    public function getwheres_count($table, $where)
    {
        $this->db->where($where);
        $data = $this->db->get($table);
        $get  = $data->result();
        return count($get);
        
    }
    
    /********************Fetch Single Data**************/
    function getSingle($table, $where)
    {
        $this->db->where($where);
        $data = $this->db->get($table);
        $get  = $data->result_array();
        
        $num = $data->num_rows();
        
        if ($num) {
            return $get;
        } else {
            return false;
        }
    }
    
    /***********Fetch Data Single Record**************/
    public function fetch_recordbyid($tbname, $where)
    {
        $this->db->where($where);
        $query = $this->db->get($tbname);
        $num   = $query->num_rows();
        if ($num > 0) {
            $row = $query->row();
            return $row;
        } else {
            return false;
        }
    }
    
    /************Send Mail From Local Host Through SMTP*********/
    
    public function sendmail($useremail, $pass, $username, $to, $subject, $content)
    {
        $this->load->library('email');
        
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = 'smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $useremail;
        $config['smtp_pass']    = $pass;
        $config['charset']      = 'utf-8';
        $config['newline']      = '\r\n';
        $config['crlf']         = '\r\n';
        $config['mailtype']     = 'text'; // text or html
        $config['validation']   = TRUE; // bool whether to validate email or not
        $config['charset']      = 'iso-8859-1';
        
        $this->email->initialize($config);
        
        $this->email->from($useremail, $username);
        $this->email->to($to);
        
        $this->email->subject($subject);
        $this->email->message($content);

        $this->email->send();
        echo $this->email->print_debugger();
        exit;
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

     /************Send Mail From Local Host Through SMTP*********/
    
    public function sendmail_shubham($useremail, $pass, $username, $to, $subject, $content)
    {
        $this->load->library('email');
        
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = 'smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $useremail;
        $config['smtp_pass']    = $pass;
        $config['charset']      = 'utf-8';
        $config['newline']      = '\r\n';
        $config['crlf']         = '\r\n';
        $config['mailtype']     = 'text'; // text or html
        $config['validation']   = TRUE; // bool whether to validate email or not
        $config['charset']      = 'iso-8859-1';
        
        $this->email->initialize($config);
        
        $this->email->from($useremail, $username);
        $this->email->to($to);
        
        $this->email->subject($subject);
        $this->email->message($content);
        //$this->email->attach($attach);

        $this->email->send();
        //echo $this->email->print_debugger();
        //exit;
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function sendmail2($to, $subject, $content)
    {
        
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1;charset=UTF-8" . "\r\n";
        $headers .= "From: Wash Time <info@washtime.com>" . "\r\n" . "Reply-To: info@washtime.com" . "\r\n" . "X-Mailer: PHP/" . phpversion();
        
        if (mail($to, $subject, $content, $headers)) {
            return true;
        } else {
            return false;
        }
    }
    
    /************* get all data as where class *************** */
    
    function getwhere($table, $where="", $orderby = false)
    {  
        if(!empty($where))
        {
            $this->db->where($where);
        }
        if ($orderby) {
            $this->db->order_by($orderby, 'DESC');
        }
        $data = $this->db->get($table);
        $get = $data->result();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }
    }
    
    
    
    /**********Get with Order By************/
    
    function getwheres_orderby($table, $where, $orderby)
    {
        $this->db->where($where);
        if ($orderby != '') {
            $this->db->order_by($orderby, 'DESC');
        }
        $data = $this->db->get($table);
        $get  = $data->result_array();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }
    }
    
    /************** Delete data *************** */
    
    function delete($table, $where)
    {
        
        $this->db->where($where);
        //$this->db->limit('1');
        $del = $this->db->delete($table);
        if ($del) {
            return true;
        } else {
            return false;
        }
    }
    
    
    public function get_data_twotable_column_where($table1, $table2, $id1, $id2, $column = '', $where='', $orderby='', $groupby='' )
    {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table1);
        $this->db->join($table2, $table2 . '.' . $id2 . '=' . $table1 . '.' . $id1);
        if ($where != '') {
            $this->db->where($where);
        }
        if ($orderby != '') {
            $this->db->order_by($orderby, 'DESC');
        }
        if ($groupby != '') {
            $this->db->group_by($groupby);
        }
        $que = $this->db->get();
        
        return $que->result_array();
    }


    public function getdata_orderby_where_join2($table1, $table2, $id1, $id2,$where='' , $column = '', $orderby="" , $group_by="") 
    {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table1);
        $this->db->join($table2, $table2 . '.' . $id2 . '=' . $table1 . '.' . $id1);
        if ($where!="") {
            $this->db->where($where);
        }
        if ($orderby != '') {
            $this->db->order_by($orderby, 'DESC');
        }

        if($group_by !='')
        {
            $this->db->group_by($group_by);
        }
        $que = $this->db->get();

        return $que->result();
    }
    
    
    function get_where_order_group($table, $where, $limit, $orderby, $groupby)
    {
        $this->db->where($where);
        if ($orderby != '') {
            $this->db->order_by($orderby, 'DESC');
        }
        $this->db->group_by($groupby);
        
        if ($limit != '') {
            $this->db->limit($limit);
        }
        
        $data = $this->db->get($table);
        
        $get = $data->result_array();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }
    }
    
    
    /***************Start Get All Data***************************** */
    
    function getdata($table)
    {
        $this->db->select("*");
        $data     = $this->db->get($table);
        $get_data = $data->result_array();
        if ($get_data) {
            return $get_data;
        } else {
            return false;
        }
    }
    
    
    function get_distinct_wheres($table, $where = '', $column)
    {
        // $this->db->select(DISTINCT($column));
        // $this->db->distinct($column);
        
        $this->db->distinct();
        $this->db->select($column);
        
        if (!empty($where)) {
            $this->db->where($where);
        }
        
        $this->db->order_by($column, "asc");
        
        $data = $this->db->get($table);
        $get  = $data->result_array();
        
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }
    }
    
    public function get_sql_record($sql)
    {
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function get_sql_record_obj($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }



    public function curl_mail($to,$subject,$msg,$headers=''){
        if ($headers=='') {
            $headers = "From:Quest Test <questtestmail@gmail.com> \r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=iso-8859-1 \r\n";
        }
        $ch = curl_init();
        $data = array('email'=>$to, 'subject'=>$subject,'msg'=>$msg, 'headers'=>$headers);
        curl_setopt($ch, CURLOPT_URL,"http://extreme.org.in/demo/mail.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);
        // var_dump($server_output);
        return($server_output);
    }

    function get_data_orderby_where($table, $order_clm="", $where="", $order_by="")
    {
        if ($order_by!="") {
            $this->db->order_by($order_clm, $order_by);
        }
        if ($where!="") {
            $this->db->where($where);
        }
        
        $qy=$this->db->get($table);
        return $qy->result();
    }

    function get_data_orderby_where_arras($table, $order_clm="", $where="", $order_by="")
    {
        if ($order_by!="") {
            $this->db->order_by($order_clm, $order_by);
        }
        if ($where!="") {
            $this->db->where($where);
        }
        
        $qy=$this->db->get($table);
        return $qy->result_array();
    }


    //**************************  Search *****************************//


     public function get_autocomplete_ebook_product($table1,$table2,$id1,$id2,$field="",$where="",$term="",$order_by="",$sorting_order="")
    {   
        if(!empty($field))
        {
            $this->db->select($field);
        }
        else
        {
            $this->db->select('*');
        }
        $this->db->from($table1);
        $this->db->join($table2, $table2 . '.' . $id2 . '=' . $table1 . '.' . $id1);
        if(!empty($where))
        {
            $this->db->where($where);
        }
        
        if(!empty($term))
        {
              $this->db->like('pname', $term,'both');
        }
        if(!empty($order_by))
        {
            $this->db->order_by($order_by,$sorting_order);
        }

        $que = $this->db->get();

        if ($que) {
           return $que->result(); 
        } else {
            return FALSE;
        }  
    }

    //**************************  Search *****************************//



    //****************  Seacrvhing Of Products ********************//


public function get_autocomplete_product($table1,$field="",$where="",$term="",$order_by="",$sorting_order="")
    {   
        if(!empty($field))
        {
            $this->db->select($field);
        }
        else
        {
            $this->db->select('*');
        }
        
        if(!empty($where))
        {
            $this->db->where($where);
        }
        
        if(!empty($term))
        {
              $this->db->like('pname', $term,'both');
        }
        if(!empty($order_by))
        {
            $this->db->order_by($order_by,$sorting_order);
        }

        $que = $this->db->get($table1);

        if ($que) {
           return $que->result_array(); 
        } else {
            return FALSE;
        }  
    }

    //****************  Searching Of Products*********************//

    //**************************  Search Course  *****************************//


     public function get_autocomplete_course_product($table1,$table2,$id1,$id2,$field="",$where="",$term="",$order_by="")
    {   
        if(!empty($field))
        {
            $this->db->select($field);
        }
        else
        {
            $this->db->select('*');
        }
        $this->db->from($table1);
        $this->db->join($table2, $table2 . '.' . $id2 . '=' . $table1 . '.' . $id1);
        if(!empty($where))
        {
            $this->db->where($where);
        }
        
        if(!empty($term))
        {
              $this->db->like('pname', $term,'both');
        }
        if(!empty($order_by))
        {
            $this->db->order_by($order_by);
        }

        $que = $this->db->get();

        if ($que) {
           return $que->result(); 
        } else {
            return FALSE;
        }  
    }



}
?> 