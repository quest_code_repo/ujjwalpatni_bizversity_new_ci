<?php
class admin_common_model  extends MY_Model {
	public function __construct()
	{
	$this->load->database();
	}
	// function for all record start
	public function fetch_record($tbname)
	{
		$query = $this->db->get($tbname);
		return $query->result_array();	
	}
	// function for all record end
	public function getwheres($table, $where) {
        $this->db->where($where);
        $data = $this->db->get($table);
        $get = $data->result_array();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }
    }
	// function for single record start
	public function fetch_recordbyid($tbname,$where)
	{
		$this->db->where($where);
		$query = $this->db->get($tbname);
		if($query->num_rows == 1)
		{   
			$row = $query->row();
			return $row;
		}
		else{ 
		return false; 
		}
	}
	// function for single record end

	// function for insert record start
	public function insert_data($table,$data)
	{
		$que = $this->db->insert_string($table,$data);
		$this->db->query($que);
		$id=$this->db->insert_id();
		if($id) { return $id; } else { return false; }
	}
	// function for insert record end

	// function for single record start
	public function fetch_condrecord($tbname,$data,$orderby="")
	{
		$this->db->where($data);

		if(!empty($orderby))
		{
			$this->db->order_by($orderby,'ASC');
		}
		$query = $this->db->get($tbname);
		if($query->num_rows > 0)
		{
			$row = $query->result_array();
			return $row;
		}
		else { return false; }
	}
	// function for single record end

	// function for update record start
	public function update_data($table,$data,$where)
	{
		$this->db->where($where);
		$rs=$this->db->update($table,$data);
		if($rs) { return true; } else { return false; } 
	}
	// function for update record end

	// function for delete record start
	public function delete_data($table,$where)
	{
		$rs=$this->db->delete($table,$where);
		if($rs) { return true; } else { return false; } 
	}
	
	public function fetch_join_condi_records($firsttb,$secondtp,$fname,$sname,$wherefield,$val)
	{
	    $this->db->select($firsttb.'.*',$secondtp.'.*');
		$this->db->from($firsttb);
		$this->db->join($secondtp, "$secondtp.$sname = $firsttb.$fname"); 
		$this->db->where("$secondtp.$wherefield", $val);
		$query = $this->db->get();
		return $query->result();
	}
	public function fetch_join_allrecords($firsttb,$secondtp,$fname,$sname)
	{
	       $this->db->select('*');
		   $this->db->from($firsttb);
		   $this->db->join($secondtp, "$firsttb.$fname= $secondtp.$sname");
		   $query = $this->db->get();
		   return $query->result_array();
	}
	public function fetch_greaterdata($sql)
	{
		$query = $this->db->query($sql);
		if($query->num_rows > 0)
		{
			$row = $query->num_rows();
			return $row;
		}
		else { return false; }
	}
	function emp_list()
	{
		$res = $this->db->select('employees.id,employees.name')
		->join('employee_feedback','employee_feedback.employee_id=employees.id')
		->group_by('employees.id')
		->get('employees');
		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}

	}
	function seller_list()
	{
		$res = $this->db->select('seller.id,seller.firm_name')
		->join('seller_feedback','seller_feedback.seller_id=seller.id')
		->group_by('seller.id')
		->get('seller');
		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}
	function emp_category_list()
	{
		$res = $this->db->select('product_categories.id,product_categories.name')
		->order_by('product_categories.id')
		->get('product_categories');
		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}
	public function fetch_total_record_wher($tbname,$data)
	{
		$this->db->select("*");
		$this->db->where($data);
		$this->db->from($tbname);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function fetch_where($tbname,$data)
	{
		$this->db->select("*");
		$this->db->where($data);
		$this->db->from($tbname);
		$query = $this->db->get();
		return $query->result();
	}
	public function fetch_total_record_seller($tbname,$data)
	{
		$this->db->select('id,firm_name');
		$this->db->where($data);
		$this->db->from($tbname);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function fetch_total_record_employee($tbname,$data)
	{
		$this->db->select('id,name');
		$this->db->where($data);
		$this->db->from($tbname);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function fetch_total_record_like($tbname,$data,$data2)
	{
		$this->db->select("order_id,order_time,customer_id");
		$this->db->like($data,$data2);
		$this->db->from($tbname);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function fetch_total_record_like_count($tbname,$data,$data2)
	{
		$this->db->select("count($tbname.id) as counts");
		$this->db->like($data,$data2);
		$this->db->from($tbname);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function fetch_record_by_query($sql)
	{
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$row = $query->result_array();
			return $row;
		}
		else { return false; }
	}
	/***************************************vinod******************************************/
	public function get_data_column_where($table,$column='',$where='',$orderby='',$rand='')
	{
		if($column !='')
		{
			$this->db->select($column);	
		}
		else
		{
			$this->db->select('*');
		}
		$this->db->from($table);
		if($where !='')
		{
			$this->db->where($where);	
		}
		if($orderby !='')
		{
			$this->db->order_by($orderby,'DESC');	
		}
                if($rand !='')
		{
			$this->db->order_by($orderby,'RANDOM');	
		}
		$que = $this->db->get();
		return $que->result();		
	}

	public function getdata_orderby_where_join_two($table1, $table2, $id1, $id2,$where, $column = '', $orderby="" , $group_by="") 
    {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table1);
        $this->db->join($table2, $table2 . '.' . $id2 . '=' . $table1 . '.' . $id1);
        if ($where!="") {
            $this->db->where($where);
        }
        if ($orderby != '') {
            $this->db->order_by($orderby, 'DESC');
        }

        if($group_by !='')
        {
            $this->db->group_by($group_by);
        }
        $que = $this->db->get();

        return $que->result();
    } 


	public function get_data_three_table_column_where($table1, $table2, $table3, $id1, $id2, $id3, $column = '', $where, $orderby) {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table1);
        $this->db->join($table2, $table2 . '.' . $id2 . '=' . $table1 . '.' . $id1);
        $this->db->join($table3, $table3 . '.' . $id3 . '=' . $table1 . '.' . $id1);
        if ($where != '') {
            $this->db->where($where);
        }
        if ($orderby != '') {
            $this->db->order_by($orderby, 'DESC');
        }
        $que = $this->db->get();

        return $que->result_array();
    }
	
	public function fetch_join_disrecords($firsttb,$secondtp,$fname,$sname)
    {
           $this->db->select('*');
           $this->db->from($firsttb);
           $this->db->join($secondtp, "$firsttb.$fname= $secondtp.$sname");
           $this->db->group_by("$secondtp.name");
           $query = $this->db->get();
           return $query->result_array();

    }
 	
	
	
	function check_salary($smonth,$year){
		$this->db->select('*');
		$this->db->from('employee_salary');
		$this->db->join('employees', 'employee_salary.emp_id= employees.id');
		$where= array(
			'employee_salary.salary_month' => $smonth,
			'employee_salary.salary_year' => $year
		);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_sql_record($sql)
	{
		 $query =$this->db->query($sql);
		 return $query->result_array();
	}


public function fetch_products()  
      {  
         //data is retrive from this query  
         $query = $this->db->get('tbl_productservices');  
         return $query;  
      }  




function sendmail($useremail,$pass,$username,$to,$subject,$content){
           $this->load->library('email');

       $config['protocol']    = 'smtp';
       $config['smtp_host']    = 'ssl://smtp.gmail.com';
       $config['smtp_port']    = '465';
       $config['smtp_timeout'] = '7';
       $config['smtp_user']    = $useremail;
       $config['smtp_pass']    = $pass;
       $config['charset']    = 'utf-8';
       $config['newline']    = "\r\n";
       $config['mailtype'] = 'html'; // text or html
       $config['validation'] = TRUE; // bool whether to validate email or not
       $config['charset'] = 'iso-8859-1';      

       $this->email->initialize($config);

       $this->email->from($useremail, $username);
       $this->email->to($to); 

       $this->email->subject($subject);
       $this->email->message($content);  

       if($this->email->send()){
               return true;
       }else{
               return false;
       }
   }

   function sendmail2($to,$subject,$content){
           
       $headers = "MIME-Version: 1.0" . "\r\n";
               $headers .= "Content-type: text/html; charset=iso-8859-1;charset=UTF-8" . "\r\n";
               $headers .= "From: Wash Time <info@washtime.com>" . "\r\n" .
               "Reply-To: info@washtime.com" . "\r\n" .
               "X-Mailer: PHP/" . phpversion();

       if(mail($to,$subject,$content,$headers)){
               return true;
       }else{
               return false;
       }
   }




   public function insert_file_details($final_file,$file_type,$new_size)
{

    $data = array(
       'file' => $final_file,
       'type' => $file_type,
       'size' => $new_size,
    );

    if (!$this->db->insert('tbl_bluk_quiz_import', $data)) {
        return false;
    }
    else{
        return true;
    }

}

# inserting file in to DB
public function insert_file_content($lineArr)
{

    $data = array(
       'emp_id'     => $lineArr[0],
       'date_data'  => $lineArr[1],
       'abc'        => $lineArr[2],
       'def'        => $lineArr[3],
       'entry'      => $lineArr[4],
       'ghi'        => $lineArr[5],
    );

   $this->db->insert('tbl_bluk_quiz_import', $data);
}



function get_data_orderby_where($table, $order_clm="", $where="", $order_by="")
	{
		if ($order_by!="") {
			$this->db->order_by($order_clm, $order_by);
		}
		if ($where!="") {
			$this->db->where($where);
		}
		
		$qy=$this->db->get($table);
		return $qy->result();
	}

	public function get_sql_record_obj($sql)
	{
		$query =$this->db->query($sql);
		return $query->result();	
	}

	/*___ this function only for program list show from 3 table ___*/ 
	public function get_three_tbl_data_program_details($column="",$where="",$ord_clm="",$order_by="", $limit="",$offset='')
	{
		if ($column!='') {
	    	$this->db->select($column);
		}else{
	    	$this->db->select('*,tbl_programs.id as main_id');
		}
	    $this->db->from('tbl_programs'); 
	    $this->db->join('tbl_programs_category', 'tbl_programs_category.id=tbl_programs.category_id', '');
	    $this->db->join('cities', 'cities.city_id=tbl_programs.city_ids', '');
	    if ($order_by!="") {
          $this->db->order_by($ord_clm, $order_by);
        }	        
        if ($where!="") {
          $this->db->where($where);
        }
        if($limit !='')
       	{
       		$this->db->limit($limit, $offset);
       	}        
	    $query = $this->db->get(); 
	    if($query->num_rows() != 0)
	    {
	        return $query->result();
	    }
	    else
	    {
	        return false;
	    }
	}


	      public function get_all_order_product_search($coloum="", $where="",$search=array(),$order_by="",$groupby="",$value="")
    {
        if ($coloum!="") {
            $this->db->select($coloum);
        } else {
            $this->db->select('*');
        }
        if ($where!="") {
            $this->db->where($where);
        }
        $this->db->from('product_orders'); 
        $this->db->join('product_order_details', 'product_order_details.product_order_id = product_orders.product_order_id');
        $this->db->join('tbl_students', 'product_orders.order_user_id = tbl_students.id');
        $this->db->join('coupons', 'coupons.id = product_orders.coupon_id','LEFT');

        $i=0;
         
       // $search_query_values = explode(' ', $search);

        foreach ($search as $keys) {
                if( $i==0 ) {
                   // $this->db->group_start();
                    $this->db->like($keys, $value); 
                } else {
                $this->db->or_like($keys, $value); 
                }
            
            if( (count($search)-1) ==$i ) {
                //$this->db->group_end();
            }
                $i++;
            
        }

         if ($order_by="" ) {
            $this->db->group_by($order_by,'DESC');
        }

        if ($groupby!="" ) {
            $this->db->group_by($groupby);
        }
        $query = $this->db->get();
        return $query->result();    
    }


}  