<?php
class Authentication_model  extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	
	/***********Fetch Data Single Record**************/
	function getSingle($table, $where) {
        $this->db->where($where);
        $data = $this->db->get($table);
        $get = $data->result_array();
        
        $num = $data->num_rows();

        if ($num) {
            return $get;
        } else {
            return false;
        }
    } 

	
	public function update_data($table,$data,$where)
	{
		$this->db->where($where);
		$rs=$this->db->update($table,$data);
		if($rs) { return true; } else { return false; } 
	}
	
	
	
	
	 //change profile 

	function get_profile($user_id)
	{

	$query = $this->db->get_where('employees', array('id' => $user_id)); 

	//return $query->row();

		$row = $query->row();

		$this->session->set_userdata($row);

		return $row = $query->row();

	}
	
	
	function change_profile($user_id, $img_name)

	{

		

	$data=array(

	'personal_contact_no'=>$this->input->post('personal_contact_no'),

	'city'=>$this->input->post('city'), 'photo'=>$img_name, 'name'=>$this->input->post('name'));



	$this->db->where('id',$user_id);

	$update = $this->db->update('employees',$data);

	//echo $this->db->last_query();

	//die();

	return $update;

	}


}  