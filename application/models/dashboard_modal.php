<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard_modal extends MY_model {	  
	/*___ this function only for cource purches show from 3 table ___*/ 
	public function get_user_cource_details($column="",$where="",$ord_clm="",$order_by="", $limit="",$offset='',$group_by='')
	{
		if ($column!='') {
	    	$this->db->select($column);
		}else{
	    	$this->db->select('*');
		}
	    $this->db->from('product_orders'); 
	    $this->db->join('product_order_details order_details', 'order_details.product_order_id=product_orders.product_order_id', '');
	    $this->db->join('products', 'products.product_id=order_details.product_id', '');
	    if ($order_by!="") {
          $this->db->order_by($ord_clm, $order_by);
        }	        
        if ($where!="") {
          $this->db->where($where);
        }
        if($limit !='')
       	{
       		$this->db->limit($limit, $offset);
       	}
        if($group_by !=''){

     		$this->db->group_by($group_by); 
        }

	    $query = $this->db->get(); 
	    if($query->num_rows() != 0)
	    {
	        return $query->result();
	    }
	    else
	    {
	        return false;
	    }
	}


	public function get_elearning_product_data($coloum="",$where="",$orderby="",$group_by="")
	{

        if ($coloum!="") {
            $this->db->select($coloum);
        } else {
            $this->db->select('*');
        }
         $this->db->where($where);
         $this->db->from('products');
         //$this->db->join('tbl_chapter', 'tbl_chapter.sem_id = products.product_id');
         $this->db->join('product_data_relation', 'product_data_relation.product_id = products.product_id');
        
       
        if ($orderby!="") {
            $this->db->order_by($orderby, 'DESC');
        }
        if($group_by!= "")
        {
             $this->db->group_by($group_by);
        }
        
        $que = $this->db->get();
        return $que->result();       
    
	}
}
