<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home_model extends MY_model {

	
	public function get_sql_record_obj($sql)
	{
		$query =$this->db->query($sql);
		return $query->result();	
	}


	  function getwhere_data($table,$where="",$coloum='',$orderby='',$group_by='') 
    {
        if ($coloum!="") {
            $this->db->select($coloum);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table);
        if($where!="")
        {
             $this->db->where($where);
        }
       
        if ($orderby!="") {
            $this->db->order_by($orderby, 'DESC');
        }
        if($group_by!= "")
        {
             $this->db->group_by($group_by);
        }
        
        $que = $this->db->get();
        return $que->result();       
    }

    function getdata_orderby_where($table,$where="",$coloum='',$ord_clm='',$orderby='',$group_by='') 
    {
        if ($coloum!="") {
            $this->db->select($coloum);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table);
        if($where!="")
        {
            $this->db->where($where);
        }

        if ($ord_clm!="") {
            $this->db->order_by($ord_clm, $orderby);
        }
        if($group_by!= "")
        {
            $this->db->group_by($group_by);
        }        
        $que = $this->db->get();
        return $que->result();       
    }

     public function getdata_orderby_where_join2($table1, $table2, $id1, $id2,$where, $column = '', $orderby="" , $group_by="") 
    {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table1);
        $this->db->join($table2, $table2 . '.' . $id2 . '=' . $table1 . '.' . $id1);
        if ($where!="") {
            $this->db->where($where);
        }
        if ($orderby != '') {
            $this->db->order_by($orderby, 'DESC');
        }

        if($group_by !='')
        {
            $this->db->group_by($group_by);
        }
        $que = $this->db->get();

        return $que->result();
    }  

    public function cart_item_sum_sql($user_id)
    {
        $sql = "SELECT count(cart_id) as total_qty
                FROM cart_items
                WHERE user_id = '$user_id' AND status = 'active'";
        $query =$this->db->query($sql);
        return $query->result();

    }

      function getwhere_data_order_asc($where,$coloum='',$orderby='',$group_by='') 
    {
        if ($coloum!="") {
            $this->db->select($coloum);
        } else {
            $this->db->select('*');
        }
         $this->db->where($where);
         $this->db->from('tbl_programs');
        $this->db->join('tbl_programs_category', 'tbl_programs_category.id = tbl_programs.category_id');
        $this->db->join('cities', 'cities.city_id = tbl_programs.city_ids');
        
       
        if ($orderby!="") {
            $this->db->order_by($orderby, 'ASC');
        }
        if($group_by!= "")
        {
             $this->db->group_by($group_by);
        }

        $this->db->limit('1');
        
        $que = $this->db->get();
        return $que->result();       
    }


     public function getdata_orderby_where_in_and_not_in_join2($table1, $table2, $id1, $id2,$whereIn,$where="",$column = '', $orderby="" , $group_by="") 
    {


        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table1);
        $this->db->join($table2, $table2 . '.' . $id2 . '=' . $table1 . '.' . $id1);
        if ($whereIn!="") {
            //$this->db->where_in($where);
            $whereIn;
        }


         if ($where!="") {
            $this->db->where($where);
            
        }

        if ($orderby != '') {
            $this->db->order_by($orderby, 'DESC');
        }

        if($group_by !='')
        {
            $this->db->group_by($group_by);
        }
        $que = $this->db->get();

        return $que->result();
    }

          function getwhere_limit_data($table,$where="",$coloum='',$orderby='',$group_by='',$limit="",$offset="") 
    {
        if ($coloum!="") {
            $this->db->select($coloum);
        } else {
            $this->db->select('*');
        }
        $this->db->from($table);
        if($where!="")
        {
             $this->db->where($where);
        }
       
        if ($orderby!="") {
            $this->db->order_by($orderby, 'DESC');
        }
        if($group_by!= "")
        {
             $this->db->group_by($group_by);
        }

        
         if($limit!="")
        {
             $this->db->limit($limit,$offset);
        }
        
        $que = $this->db->get();
        return $que->result();       
    }


    public function get_autocomplete_product($field="",$sql,$table,$where="")
    {   
        if(!empty($field))
        {
            $this->db->select($field);
        }
        else
        {
            $this->db->select('*');
        }

        if($where!="")
        {
                $this->db->where($where);
        }
        
        $this->db->like('pname', $sql,'both');
        $res = $this->db->get($table);
        $get = $res->result();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }  
    }



    //**************************  Blog autocomplete *********************//

    public function get_autocomplete_blog($field="",$sql,$table,$where="")
    {   
        if(!empty($field))
        {
            $this->db->select($field);
        }
        else
        {
            $this->db->select('*');
        }

        if($where!="")
        {
                $this->db->where($where);
        }
        
        $this->db->like('blog_title', $sql,'both');
        $res = $this->db->get($table);
        $get = $res->result();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }  
    }


    //******************************************************************//

      public function get_total_no_of_lectures($product_id)
    {
        $sql = "SELECT SUM(ch_no_of_lecture) as total_lectures
                FROM tbl_chapter
                WHERE sem_id = $product_id AND ch_status = 'active'";
        $query =$this->db->query($sql);
        return $query->result();

    }

    public function get_total_no_of_completed_lectures($user_id="",$course_id="")
    {
        $sql = "SELECT count(lect_id) as completed_lectures
                FROM completed_lectures
                WHERE course_id = $course_id AND user_id = $user_id";
        $query =$this->db->query($sql);
        return $query->result();
    }


    public function get_autocomplete_chapter($field="",$sql,$table,$where="")
    {   
        if(!empty($field))
        {
            $this->db->select($field);
        }
        else
        {
            $this->db->select('*');
        }

        if($where!="")
        {
                $this->db->where($where);
        }
        
        $this->db->like('ch_name', $sql,'both');
        $res = $this->db->get($table);
        $get = $res->result();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }  
    }


         public function get_total_no_of_alloted_userfor_course($user_id="",$course_id="",$order_id="")
    {
        $sql = "SELECT count(alloted_user) as total_alloted_user
                FROM e_learning_licence
                WHERE course_id = $course_id AND owner_id = $user_id AND order_id = $order_id AND alloted_user!= 0" ;
        $query =$this->db->query($sql);
        return $query->result();
    }


      function get_last_license_no($table,$order_by,$limit,$column) 
    {
        $this->db->select($column);
        $this->db->order_by($order_by, 'DESC');
        $this->db->limit($limit);
        $data = $this->db->get($table);
        $get = $data->result();
        if ($get) {
            return $get;
        } else {
            return FALSE;
        }
    } 



}
