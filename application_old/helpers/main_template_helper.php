<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('load_user_dashboard_view')){
	function load_user_dashboard_view($view = '',$data = array()){
		$CI =& get_instance();
		$CI->load->model('front_end/cat_frontend');
		$userid = $CI->session->userdata('id');
		$where = array('id'=>$userid);
		$data['student_record'] = $CI->cat_frontend->getwheres('tbl_students', $where);

		$where1 = array('student_id'=>$userid, 'view'=>'no');
		$data['student_dcs_notif'] = $CI->cat_frontend->getwheres('tbl_doubt_responce', $where1);
		// echo $CI->db->last_query();
		 // $sql="select `tbl_semester`.* from `tbl_semester` join `product_orders` ON `product_orders`.`order_products`= `tbl_semester`.`sem_id` WHERE `product_orders`.`order_user_id`=$userid and  `product_orders`.`order_status`='completed' ";
		$sql="select `tbl_semester`.* from `tbl_semester`  ";
		// die;and `product_orders`.`end_date` >= CURDATE() 
		$data['student_sem'] = $CI->cat_frontend->get_sql_record($sql);

		if(!empty($data['student_sem']))
		{
			if($CI->input->get('sem')){
				$mysem=base64_decode($CI->input->get('sem'));
				foreach($data['student_sem'] as $semm){
					$sem_id[]=$semm['sem_id'];
				}
				// print_r($sem_id);
				if(!in_array($mysem, $sem_id)){
					redirect('dashboard', 'refresh');
				}
				$CI->session->set_userdata(array('first_sem' => $mysem ));
			}else{
				// $CI->session->set_userdata(array('first_sem' => $data['student_sem'][0]['sem_id'] ));
			}
			// echo $data['student_sem'][0]['sem_id'];
		}
		// $all_userdata=$CI->session->all_userdata();
		// print_r($all_userdata);
		$CI->load->view('user_dashboard/header_footer/header',$data);
		$CI->load->view($view, $data);
		$CI->load->view('user_dashboard/header_footer/footer', $data);
			
	}  
}

if ( ! function_exists('load_exam_view')){
	function load_exam_view($view = '',$data = array()){
		$CI =& get_instance();
		$CI->load->model('front_end/cat_frontend');
		// $queryf = $CI->db->query('select * from social_links where status = "active"');
		// $news['social'] = $queryf->result_array();
		// $foot['social'] = $queryf->result_array();
		
		// //$menu = $CI->db->query('select * from product_categories where parent_category = 0 and active_status="Active" ');
		// $user = $CI->db->query("select * from customer where id = '".$CI->session->userdata('memberid')."'");
		// $news['user'] = $user->result_array();		
		//$news['menus'] = $menu->result_array();

		$CI->load->view('exam/templates/header', $data);
		$CI->load->view($view, $data);
		$CI->load->view('exam/templates/footer', $data);
	}  
}

if ( ! function_exists('load_front_view')){
	function load_front_view($view = '',$data = array()){
		$CI =& get_instance();
		$CI->load->model('front_end/cat_frontend');
		$userid = $CI->session->userdata('id');
		$where = array('id'=>$userid);
		$data['student_record'] = $CI->cat_frontend->getwheres('tbl_students', $where);
			
		$CI->load->view('template/header',$data);
		$CI->load->view($view, $data);
		$CI->load->view('template/footer', $data);
			
	} 		
}

if ( ! function_exists('new_user_dashboard_view')){
			function new_user_dashboard_view($view = '',$data = array()){
				$CI =& get_instance();
					
					$CI->load->view('user_dashboard/header_footer/header',$data);
					$CI->load->view($view, $data);
					$CI->load->view('user_dashboard/header_footer/footer', $data);
					
			}  
		}

		/***************By Shubham Start********/

		function get_cat_name($cat_id){

				$ci=& get_instance();
		        $ci->load->database(); 
		        $ci->load->model('admin/admin_common_model','front'); 

				$query_data = $ci->front->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active','id'=>$cat_id),'');

		        if(!empty($query_data))
		        {
		          $names = $query_data[0]->name;
		        } else{
		          $names = 'N/A';
		        }          
		          return  $names; 

		}

		 function getd_speakers_name($program){

				$ci=& get_instance();
		        $ci->load->database(); 
		        $ci->load->model('admin/admin_common_model','front'); 


				$query_data = $ci->front->get_sql_record_obj("SELECT * FROM `tbl_speakers` INNER JOIN `tbl_program_speaker_rel` ON `tbl_speakers`.`id` = `tbl_program_speaker_rel`.`speaker_id` WHERE `tbl_program_speaker_rel`.`program_id` = $program AND `tbl_speakers`.`deleted` = '0' AND `tbl_program_speaker_rel`.`deleted` = '0' AND `tbl_speakers`.`status` = 'active' AND `tbl_program_speaker_rel`.`status` = 'active'  GROUP BY `tbl_speakers`.`id` ORDER BY  `tbl_speakers`.`id` DESC");
				$names = '';

		        if(!empty($query_data))
		        {
		        	foreach($query_data as $programs){

		          $names.= $programs->speaker_name.',';

		     	 }
		        } else{

		          $names = 'N/A';

		        }          
		          return  rtrim($names,','); 
		      
		}


		if ( ! function_exists('get_program_cat_list')){
			function get_program_cat_list(){
				$CI =& get_instance();
				$CI->load->database(); 
		        $CI->load->model('admin/admin_common_model','front');
		        
	            $categroy_details =$CI->front->get_data_orderby_where('tbl_programs_category','',array('deleted'=>'0','status'=>'active'),'');
			
				if(!empty($categroy_details))
				{
					return $categroy_details;
				}	
			}  
		}




		/****************END***********/




if ( ! function_exists('get_user_details')){
			function get_user_details(){
				$CI =& get_instance();
				$CI->load->model('front_end/cat_frontend');
				$userid = $CI->session->userdata('id');
	            $where = array('id'=>$userid);
	            $user_details = $CI->cat_frontend->getwheres('tbl_students', $where);
			
				if(!empty($user_details))
				{
					return $user_details;
				}	
			}  
		}



// **************************************** Mentoring ************************************//

if ( ! function_exists('get_mentoring_list')){
			function get_mentoring_list(){
				$CI =& get_instance();
				$CI->load->model('front_end/cat_frontend');
	            $where = array('status'=>'active');
	            $mentoring_details = $CI->cat_frontend->getwheres('mentoring_criteria', $where);
			
				if(!empty($mentoring_details))
				{
					return $mentoring_details;
				}	
			}  
		}


//***************************************** Mentoring **************************************//



// **************************   get total count of product  categories ******************//

		if ( ! function_exists('get_category_total_count')){
			function get_category_total_count($product_category_id=""){
				$CI =& get_instance();

	            
	            $cateCount = $CI->home_model->get_data_twotable_column_where('products','product_categories','product_category_id','id','count("product_id") as total_count',array('product_type'=>'product','product_category_id'=>$product_category_id,'active_inactive'=>'active','stock_status'=>'In Stock','deleted'=>'0'),'product_id','');
			
				if(!empty($cateCount))
				{
					return $cateCount;
				}	
			}  
		}


// ******************************************************************************************//


// *************************************************************************************//

		if ( ! function_exists('check_product_in_cart')){
			function check_product_in_cart($product_id="",$userId=""){
				$CI =& get_instance();

	           
	            $proCheck = $CI->home_model->getwhere_data('cart_items',array('user_id'=>$userId,'product_id'=>$product_id,'product_type'=>'product'),'','','');
			
				if(!empty($proCheck))
				{
					return $proCheck;
				}	
			}  
		}

// ********************************************************************************// 



			/************Multiple Questions By Shubham**********/

	function check_if_multiple_ques($questions_id){

		$CI =& get_instance();
		$where = array('ques_id'=>$questions_id);
		$questions_record = $CI->cat_frontend->getwheres('tbl_add_question', $where);

		if(!empty($questions_record)){

			$check_type = $questions_record[0]['question_type'];

			return $check_type;

		} 

	}

	function get_correct_answers_multiple($questions_id){

		$CI =& get_instance();

		$wheres_multiple = array('question_id'=>$questions_id,'deleted'=>'0');

		$ques_data_multiple = $CI->cat_frontend->getwheres('tbl_multiple_choice_questions',$wheres_multiple,'');

		if(!empty($ques_data_multiple)){

			$m_correct_ans = '';

			foreach($ques_data_multiple as $m_ansers){

				$m_correct_ans .= $m_ansers['answer_id'].',';

			} 

			$crt = rtrim($m_correct_ans,','); 

			return $crt;

		}


	}

	function multiple_view_descriptive($questions_id){

		$CI =& get_instance();

		$wheres_multiple = array('question_id'=>$questions_id,'deleted'=>'0');

		$ques_data_multiple = $CI->cat_frontend->getwheres('tbl_multiple_choice_questions',$wheres_multiple,'');

		$final_result = '';

		if(!empty($ques_data_multiple)){


			foreach($ques_data_multiple as $m_ansers){

				$m_correct_ans = $m_ansers['answer_id'];


				$wheres_multiple_second = array('ques_id'=>$questions_id);

				$ques_data_multiple_second = $CI->cat_frontend->getwheres('tbl_add_question',$wheres_multiple_second,'');

				if(!empty($ques_data_multiple_second)){

					$final_result .= $ques_data_multiple_second[0]['ans_'.$m_correct_ans];

					if (!empty($ques_data_multiple_second[0]['ans_'.$m_correct_ans.'_image'])) {

						$final_result .= "<img width='100px;' src=".base_url()."uploads/test_questions_ans_images/".$ques_data_multiple_second[0]['ans_'.$m_correct_ans.'_image'].">";
					}

				}

			} 

			return $final_result;

		}


	}

	function mult_marks_tots($test_id){

		$CI =& get_instance();

		$wheres_multiple = array('test_id'=>$test_id);

		$ques_data_multiple = $CI->cat_frontend->getwheres('tbl_test',$wheres_multiple,'');

		if(!empty($ques_data_multiple)){

			$totalmarks = $ques_data_multiple[0]['total_marks'];

			return $totalmarks;

		} else {

			return '0';
		}

	}


	function mult_times_tots($test_id){

		$CI =& get_instance();

		$wheres_multiple = array('test_id'=>$test_id);

		$ques_data_multiple = $CI->cat_frontend->getwheres('tbl_test',$wheres_multiple,'');

		if(!empty($ques_data_multiple)){

			$totalmarks_one = $ques_data_multiple[0]['ideal_time_duration'];

			return $totalmarks_one;

		} else {

			return '0';
		}

	}


	function mult_dates_tots($test_id){

		$CI =& get_instance();

		$wheres_multiple = array('test_id'=>$test_id);

		$ques_data_multiple = $CI->cat_frontend->getwheres('tbl_test',$wheres_multiple,'');

		if(!empty($ques_data_multiple)){

			$totalmarks_one = $ques_data_multiple[0]['appear_time'];

			$final_date_send = date('d.m.Y',strtotime($totalmarks_one));

			return $final_date_send;

		} else {

			return '0';
		}

	}


	function string_replaces($string){

		if($string!=''){

		$get_data = explode(',',$string);

		$final_string = '';

		foreach($get_data as $results){

			$final_string_first = $results;

			if($final_string_first == 1){

				$final_string .= 'A'.',';
			
			} if($final_string_first == 2){

				$final_string .= 'B'.',';
			
			} if($final_string_first == 3){

				$final_string .= 'C'.',';
			
			} if($final_string_first == 4){

				$final_string .= 'D'.',';
			}

		}
		return rtrim($final_string,',');

		}

	}

	/**___ Kp Start ___**/ 

	function get_users_ans_multiple_option($user_id,$question_id){

		$CI =& get_instance();

		$where_user_ans = array('user_id'=>$user_id,'question_id'=>$question_id);

		return $user_multi_submit = $CI->cat_frontend->getwheres('tbl_user_ques_answer',$where_user_ans,'');		
	}

	function get_correct_answers_multiple_option($questions_id){

		$CI =& get_instance();

		$wheres_multiple = array('question_id'=>$questions_id,'deleted'=>'0');

		return $ques_data_multiple = $CI->cat_frontend->getwheres('tbl_multiple_choice_questions',$wheres_multiple,'');

	}

	function get_submit_user_ques_ans($question_id)
	{
		$CI =& get_instance();
		$where_ques_ans = array('ques_id'=>$question_id);
		return $ques_ans_multi = $CI->cat_frontend->getwheres('tbl_add_question',$where_ques_ans,'');		
	}

	function get_correct_answers_single_option($user_id,$question_id){

		$CI =& get_instance();

		$where_single = array('user_id'=>$user_id,'question_id'=>$question_id);

		$user_single_option = $CI->cat_frontend->getwheres('tbl_user_ques_answer',$where_single,'');
		if ($user_single_option) {
			return $user_single_option[0]['answer'];
		}else{
			return 0;
		}
		// echo "answer= ".$user_single_option[0]['answer'];die;
	}
	/**___ Kp End ___**/ 