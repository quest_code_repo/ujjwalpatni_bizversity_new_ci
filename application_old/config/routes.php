<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';


/*_______ Start Events Routes _______*/ 
$route['vip'] 					= 'events/vip';
$route['excellence-gurukul'] 	= 'events/excellence_gurukul';
$route['power-parenting'] 		= 'events/power_parenting';
$route['key-note'] 				= 'events/key_note';
$route['elearning/courses/(:any)']      = 'elearning/courses/$1';
$route['contact-us']            =  'home/contact_us';
$route['Ujjwal-Patni-International'] = 'home/ujjwal_patni_international';
$route['about-us']              = 'home/about_us';
$route['privacy-policy']        = 'home/privacy_policy';
$route['my-cart']        		= 'Cart/cart/my_cart';
$route['product/(:any)']        = 'product/index/$1';
$route['checkout-details']        	  = 'checkout/checkout_details';
$route['place-order-checkout']        = 'checkout/place_checkout_order';
$route['place-order-programe']        = 'checkout/place_order_programe';
$route['placed-order-details/(:any)'] = 'checkout/placed_order_details/$1';
$route['cancelled-order-details']     = 'checkout/cancelled_order_details';

/*_______ End Events Routes _______*/ 
/*_______ Start User Dashboard Routes _______*/
$route['my-courses']            = 'dashboard/my_courses';
$route['cource-details'] 		= 'dashboard/cource_details';
$route['completed-course']      = 'dashboard/completed_course';
$route['my-result']             = 'dashboard/my_result';
$route['group-registration/(:any)']    = 'dashboard/group_registration/$1';
$route['group-result']          = 'dashboard/group_result';
$route['purchased-courses']     = 'dashboard/purchased_courses';


$route['test_dashboard'] = 'user_dashboard/user_dashboard/test_dashboard';
$route['logout'] = 'user_dashboard/user_dashboard/logout';
$route['summary_analysis'] = 'user_dashboard/user_dashboard/summary_analysis';
$route['depth_analysis'] = 'user_dashboard/user_dashboard/depth_analysis';
$route['question_wise_analysis'] = 'user_dashboard/user_dashboard/question_wise_analysis';
$route['view_description'] = 'user_dashboard/user_dashboard/view_description';

 
/*_______ End User Dashboard Routes _______*/ 


/* End of file routes.php */
/* Location: ./application/config/routes.php */