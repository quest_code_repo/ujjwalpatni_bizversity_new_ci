<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Events_model extends MY_model {
	  
	/*___ this function only for program list show from 3 table ___*/ 
	public function get_program_details($column="",$where="",$ord_clm="",$order_by="", $limit="",$offset='')
	{
		if ($column!='') {
	    	$this->db->select($column);
		}else{
	    	$this->db->select('*');
		}
	    $this->db->from('tbl_programs'); 
	    $this->db->join('tbl_programs_category prog_cat', 'prog_cat.id=tbl_programs.category_id', '');
	    $this->db->join('cities', 'cities.city_id=tbl_programs.city_ids', '');
	    if ($order_by!="") {
          $this->db->order_by($ord_clm, $order_by);
        }	        
        if ($where!="") {
          $this->db->where($where);
        }
        if($limit !='')
       	{
       		$this->db->limit($limit, $offset);
       	}
      $this->db->group_by('city_ids'); 

	    $query = $this->db->get(); 
	    if($query->num_rows() != 0)
	    {
	        return $query->result();
	    }
	    else
	    {
	        return false;
	    }
	}
}
