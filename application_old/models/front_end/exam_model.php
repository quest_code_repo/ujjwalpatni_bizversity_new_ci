<?php
class Exam_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}

	public function insert_data($table, $data) {
        $this->db->insert($table, $data);
        $num = $this->db->insert_id();
        return $num;
    }

	public function fetch_recordbyid($tbname,$where)
	{
		$this->db->where($where);
		$query = $this->db->get($tbname);
		if($query->num_rows == 1)
		{
			$row = $query->row();
			return $row;
		}
		else{ 
			return $query->result_array();
		}
	}
	public function grt_data($tbname,$where)
	{
		$this->db->where($where);
		$query = $this->db->get($tbname);
		
		return $query->result_array();
		
	}
	function updateRecords($table, $data,$where)
	{ 
		$this->db->where($where);
		$rs=$this->db->update($table,$data);
		if($rs) { 
		    return true; 
	    } else { 
	        return false; 
	    } 
	}
	function deleteRecords($table, $where)
	{ 
	
		$this->db->delete($table, $where); 
		//echo $a=$this->db->last_query();
	}

	public function getEntranceDetails($sem_id='')
	{
		$this->db->order_by('id', 'RANDOM');
		$this->db->where(array('test_type' =>1,'sem_id'=>$sem_id ));
		$this->db->limit(1);
		$query = $this->db->get('tbl_test');
		return $query->row();
    }

	public function getExamQue($examid, $ques_id='')
	{
		
		// echo "test";
		$this->db->select('ques_id, test_id , ans_1 , ans_2 , ans_3 , ans_4 , attachment_img,chapter_id');
		$this->db->where(array('test_id' =>$examid,'ques_status'=>'active' ));
		$this->db->order_by('ques_id', 'ASC');
		$query = $this->db->get('tbl_add_question');
		// echo $this->db->last_query();
		return $query->result_array();
    }

    public function get_sql_record($sql)
    {
      $query =$this->db->query($sql);
      return $query->result_array();        
    }

	public function test()
	{
		echo "test";
    }	

} 
?>