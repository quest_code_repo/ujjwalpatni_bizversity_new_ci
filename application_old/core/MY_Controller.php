<?php ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
      
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert_error">', '</div>');
        $this->load->library('pagination');
        date_default_timezone_set('Asia/Kolkata');
       
           $this->load->model('home_model','front');

      
       
       
    }
    
    /********************login check************************/
     function check_login()
     {
          if($this->session->userdata('login')==FALSE  || ($this->session->userdata('user_id')=='' || $this->session->userdata('user_id')==0) )
          {
             //$this->session->sess_destroy();
             redirect('staff_login/index');
             exit();
          }
          else
          {
            
          }
     }
        /*function for view load*/
    public function load_view($page, $data=array())
    {
        /*__ Program catagories get for header__*/ 
        $where = array('deleted'=>'0', 'status'=>'active');
        $data['events_details'] = $this->front->get_data_orderby_where_row('tbl_programs_category', "",  "id", $where, "ASC");

        $data['Mentoring'] =  $this->front->get_data_orderby_where_row('mentoring_criteria', "",  "id",array('status'=>'active'), "ASC");


        $userId = $this->session->userdata('id');
        if (!empty($userId)) {
          $data['item_count'] = $this->front->cart_item_sum_sql($userId);
        }else{
          $data['item_count'] = '0';
        }

        // echo "<pre>";print_r($data['item_count']);die;
        

        $this->load->view("template/header",$data);
        $this->load->view($page);
        $this->load->view("template/footer");
    }


    // **************************  This Is for Dashoboard  *************************


    public function dashboard_view($page, $data=array())
    {        
        $userId = $this->session->userdata('id');
        if (!empty($userId)) {
          $data['item_count'] = $this->front->cart_item_sum_sql($userId);
        }else{
          $data['item_count'] = '0';
        }

        // echo "<pre>";print_r($data['item_count']);die;
        

        $this->load->view("template/header_dashboard", $data);
        $this->load->view($page, $data);
        $this->load->view("template/footer");
    }


    //************************************************************************************



      function all_page_pagination($page_url,$total_rows,$get_page,$count_data)
      {

         $config['full_tag_open'] = "<ul class='pagination'>";
      $config['full_tag_close'] ="</ul>";
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
      $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
      $config['next_tag_open'] = "<li>";
      $config['next_tagl_close'] = "</li>";
      $config['prev_tag_open'] = "<li>";
      $config['prev_tagl_close'] = "</li>";
      $config['first_tag_open'] = "<li>";
      $config['first_tagl_close'] = "</li>";
      $config['last_tag_open'] = "<li>";
      $config['last_tagl_close'] = "</li>";

      $config["base_url"]=$page_url ;
      $config["total_rows"]=$total_rows;
      $config['page_query_string'] = TRUE;
      $config["per_page"]=$count_data;
      $config['num_links']=10;
      // $config['constant_num_links'] = TRUE;

      
       
       $this->pagination->initialize($config);

       if($get_page)
       {
          $page = $get_page;
       }
       else
       {
          $page=0;
       }
       
       return $this->pagination->create_links();

      }
   
}

?>
