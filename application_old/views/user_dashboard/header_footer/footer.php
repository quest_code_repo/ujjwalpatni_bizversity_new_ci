 <!-- <a href="javascript:;" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a> -->

        <!-- JQuery -->
        <script type="text/javascript" src="<?php echo base_url(); ?>exam_assets/js/jquery-1.11.3.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>exam_assets/js/bootstrap.min.js"></script>
        <!-- dropdownhover effects JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>exam_assets/js/bootstrap-dropdownhover.min.js"></script>
        <!-- Owl JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>exam_assets/js/owl.carousel.min.js"></script>
        <!-- wow JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>exam_assets/js/wow.min.js"></script>
        <!-- video player JavaScript -->
        <script src="<?php echo base_url(); ?>exam_assets/js/jquery.parallax-1.1.3.js"></script>
    <!-- jquery.nice-select.min.js -->
        <script type="text/javascript" src="<?php echo base_url(); ?>exam_assets/js/jquery.nice-select.min.js"></script>
        <!-- Custom JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>exam_assets/js/custom.js"></script>
        
     <!--  <script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/highcharts-more.js"></script> -->
        <script src="<?php echo base_url(); ?>exam_assets/js/exporting.js"></script>

 <!-- Video popup JavaScript -->
        <script src="<?php echo base_url(); ?>exam_assets/js/jquery.popup.js"></script>
        <script src="<?php echo base_url(); ?>exam_assets/js/jquery.popup.dialog.min.js"></script>
        <script>
            (function ($) {
                $(function () {
                    $('[data-dialog]').on('click', function (e) {
                        var $this = $(e.target);
                        $($this.data('dialog')).attr('class', 'popup ' + $this.data('effect'));
                    });
                });
            })(jQuery);
            $(document).ready(function () {
                $('.popup').popup({
                    close: function () {
                        $(this).find('.embed-container').empty();
                    }
                });
                $(document).on('click', '[data-action="watch-video"]', function (e) {
                    e.preventDefault();
                    var plugin = $('#popup-video.popup').data('popup');
                    /* $('#popup-video.popup .embed-container').html(
                     '<iframe src="'
                     + e.currentTarget.href
                     + '?autoplay=1" frameborder="0" allowfullscreen />'
                     );*/
                    $('#popup-video.popup .embed-container').html(
                            '<iframe id="ankitankit" oncontextmenu="return false;" src="'
                            + e.currentTarget.href
                            + '?autoplay=1" allowfullscreen="" oncontextmenu="return false" frameborder="0"></iframe>'
                            );
                    plugin.open();
                });

            });


            
        </script>
        <script src="<?php echo base_url(); ?>exam_assets/js/jQuery-plugin-progressbar.js"></script>
        
        <script>
    // $(".progress-bar2").loading();
    // $('input').on('click', function () {
    //    $(".progress-bar2").loading();
    // });
  </script>
<script src="<?php echo base_url(); ?>exam_assets/js/canvasjs.min.js"></script>
<script type="text/javascript">
window.onload = function () {
  var chart = new CanvasJS.Chart("chartContainer",
  {
    title:{
      text: "",
      fontFamily: "arial black"
    },
                animationEnabled: true,
    legend: {
      verticalAlign: "bottom",
      horizontalAlign: "center"
    },
    theme: "theme1",
    data: [
    {        
      type: "pie",
      indexLabelFontFamily: "Garamond",       
      indexLabelFontSize: 20,
      indexLabelFontWeight: "bold",
      startAngle:0,
      indexLabelFontColor: "MistyRose",       
      indexLabelLineColor: "darkgrey", 
      indexLabelPlacement: "inside", 
      toolTipContent: "{name}: {y}hrs",
      showInLegend: true,
      indexLabel: "#percent%", 
      dataPoints: [
        {  y: 52, name: "Time At Work", legendMarkerType: "triangle"},
        {  y: 44, name: "Time At Home", legendMarkerType: "square"},
        {  y: 12, name: "Time Spent Out", legendMarkerType: "circle"}
      ]
    }
    ]
  });
  chart.render();
}
</script>
        <!-- video popup -->
        <div class="popup effect-fade-scale" id="popup-video">
            <div class="embed-container"></div>
            <a href="#" class="popup-close">
                <i class="glyphicon glyphicon-remove"></i>
            </a>
        </div>
        <div class="popup effect-fade-scale" id="popup-dialog">
            <div class="popup-content">
                <h3>Video Modal</h3>
                <button class="popup-close btn btn-danger">x</button>
            </div>
        </div>

      <script src="<?php echo base_url(); ?>exam_assets/js/jquery.validate.min.js"></script>
<!-- validation for ask question -->
<script type="text/javascript">
  /* validation */
  $("#ask_form").validate({
    rules:{
      
      title: {
        required: true,
      },
      description: {
        required: true,
      }

    },
    
    messages:{
      title: "Please Enter Title Name",
      description: "Please Enter Description"
    },
       
  });
</script>
<!-- validation for responce question -->
<script type="text/javascript">
  /* validation */
  $("#response_form").validate({
    rules:{
      
      title: {
        required: true,
      },
      answer: {
        required: true,
      }
      // link: {
      //   required: true,
      //   url: true
      // }

    },
    
    messages:{
      title: "Please Enter Title Name",
      answer: "Please Enter Answer"
      // link: "Please Enter Valid URL"
    },
       
  });
  // $=jQuery;
  // $.noConflict(true);  
  // jQuery.noConflict(true);  
</script>  

    </body>
</html>