      <!-- loader Section html start -->
        <section class="loader">
            <div class="container">
                <div class="row">
                
                    <div class="col-md-12">
                        <div class="loader-data">
						<p class="userMess animated bounceInDown">Hi <?=@$this->session->userdata('full_name');?>, Here is you overall Performance in the test</p>
                        <?php
                        if($entdata[1]['total_que']==0){$entdata[1]['total_que']=1;}
                        $SAT=($entdata[1]['right_ans']/$entdata[1]['total_que'])*100;
                        if($entdata[2]['total_que']==0){$entdata[2]['total_que']=1;}
                        $MAT=($entdata[2]['right_ans']/$entdata[2]['total_que'])*100;
                        if($entdata[3]['total_que']==0){$entdata[3]['total_que']=1;}
                        $Language=($entdata[3]['right_ans']/$entdata[3]['total_que'])*100;
                        
                        $SATs='';$MATs='';$langu=''; 
                        if($SAT<45){
                          $SATs='SAT';
                        }
                        if($MAT<45){
                          if ($SATs=='SAT') {
                            $MATs=', MAT';
                          }else{
                            $MATs='MAT';
                          }
                        }
                        if($Language<45){
                          if ($SATs=='SAT' || $MATs=='MAT') {
                            $langu=', Language';
                            # code...
                          }else{
                            $langu='Language';
                          }
                        }
                         ?>
                         <h2 class='analysis-cutoff animated slideInDown' >You have achieved <?php $percent = (($correct_ans) / $total_ques_marks) * 100; echo round($percent,2);?>% in your test.</h2>
                            <div class="loader-top text-left">
                                <h3 class="animated fadeInRight">Oops! <?php //@$this->session->userdata('full_name');?> You are unable to clear cutoff of <?php echo$SATs.$MATs.$langu; ?>.</h3>
                                <h4 class="animated fadeInLeft">Your Chances to Crack The NTSE Examination is LOW.</h4>
                            </div>
                            <div class="loader-bottom animated zoomIn ">
                                <div id="container1" class="semi-chart meter-chart" ></div>

                            </div>
                        </div>
                    </div>
                    <center class="center-padding-bottom">
                            <span><span class="cb span-red"></span>Alarming Condition</span>
                            <span><span class="cb span-yellow"></span>Fair Chance</span>
                            <span><span class="cb span-blue"></span>Good Chance</span>
                            <span><span class="cb span-green"></span>Hitting the Deck</span>
                    </center>
                </div>
            </div>        
        </section>
        <!-- loader Section html end -->

        <!-- analysis Section html start -->
        <section class="analysis">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="analysis-data">
                            <div class="analysis-top text-left  wow fadeInUp">YOUR SECTION WISE ANALYSIS</div>
                            <div class="analysis-bottom">
                                <!-- <div class="course aarambh">
                                      <div class="course-no">Course 01</div>
                                      <div class="course-title">आरम्भ</div>
                                      <div class="score-no">
                                        <?php 
                                          if($entdata[1]['total_que']==0){$entdata[1]['total_que']=1;}
                                          $arambh=($entdata[1]['right_ans']/$entdata[1]['total_que'])*100;
                                          echo round($arambh,2); ?>%
                                      </div>
                                </div> -->
                                <div class="course sopan">
                                    <div class="course-no"></div>
                                    <div class="course-title">SAT</div>
                                    <div class="score-no">
                                    <?= round($SAT,2); ?>%
                                          </div>
                                </div>
                                <div class="course utkarsh">
                                      <div class="course-no"></div>
                                      <div class="course-title">MAT</div>
                                      <div class="score-no">
                                      <?= round($MAT,2); ?>%
                                        </div>
                                </div>
                                <div class="course shikhar">
                                      <div class="course-no"></div>
                                      <div class="course-title">Language</div>
                                      <div class="score-no"><?= round($Language,2); ?>%
                                        </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
                    
        </section>
        <div class="clearfix"></div>

        <!-- analysis Section html end -->

        <!-- depth analysis Section html start -->
        <section class="depth  wow zoomIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="depth-data">
                            <div class="depth-txt">YOUR SECTION WISE ANALYSIS.</div>
                            <div class="depth-table">
                                <table class="text-center">
                                    <tr>
                                        <th>Course</th>
                                        <th>Total Questions</th>
                                        <th>Attempted</th>
                                        <th>Correct</th>
                                        <th>Incorrect</th>
                                    </tr>
                                    <?php $p=2;foreach($entdata as $entdta){ ?>
                                    <tr>
                                      <?php if($p==1){echo"<td>आरम्भ</td>";} ?>
                                      <?php if($p==2){echo"<td>SAT</td>";} ?>
                                      <?php if($p==3){echo"<td>MAT</td>";} ?>
                                      <?php if($p==4){echo"<td>Language</td>";} ?>
                                        <td><?=$entdta['total_que'];?></td>
                                        <td><?=$entdta['right_ans']+$entdta['wrong_ans'];?></td>
                                        <td><?=$entdta['right_ans'];?></td>
                                        <td><?=$entdta['wrong_ans'];?></td>
                                    </tr>
                                    <?php $p++; } ?>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                   
                    
                   <div class="col-md-12 " >
                    <div class="box-analysis twoAnalysisBox noShadow">
                  	<div class="col-md-6 wow bounceInLeft" style="visibility: visible; animation-name: bounceInLeft;">
                      	<div class="panel panel-primary dept-analysis text-center">
                        	<h4 class="panel-heading">Detailed Analysis.</h4>
                            
                            <a href="detail_analysis/?test_id=<?=$this->input->get('test_id');?>" class="btn btn-blue">Click to view</a>
                        </div>
                      </div>
                      
                      <div class="col-md-6  wow bounceInRight" style="visibility: visible; animation-name: bounceInRight;"> 
                      	<div class="panel panel-primary dept-analysis text-center">
                        	<h4 class="panel-heading">Description Solution</h4>
                            
                            <a href="view_description/?test_id=<?=$this->input->get('test_id');?>"><button class="btn btn-blue">Click to view</button></a>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                      
             
                    <div class="box-analysis twoAnalysisBox noShadow">
                  	<div class="col-md-6 wow bounceInLeft" style="visibility: visible; animation-name: bounceInLeft;">
                      	<div class="panel panel-primary dept-analysis text-center">
                        	<h4 class="panel-heading">Accuracy Analysis</h4>
                            
                            <a href="depth_analysis/?test_id=<?=$this->input->get('test_id');?>" class="btn btn-blue">Click to view</a>
                        </div>
                      </div>
                      
                      <div class="col-md-6  wow bounceInRight" style="visibility: visible; animation-name: bounceInRight;"> 
                      	<div class="panel panel-primary dept-analysis text-center">
                        	<h4 class="panel-heading">Question Wise Analysis</h4>
                            
                            <a href="question_wise_analysis/?test_id=<?=$this->input->get('test_id');?>"><button class="btn btn-blue">Click to view</button></a>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                      
            </div> 
            
 
            
            
     
        </section>
        <!-- depth analysis Section html end -->
        
       
            
        

        <!-- purchase course Section html start -->
        <section class="purchase">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="purchase-txt text-center">
                            <p></p>
                              <!-- <a href="detail_analysis/?test_id=<?=$this->input->get('test_id');?>" class="btn btn-blue dt-analysis">Detailed Analysis</a> -->
                            <!-- <button class="btn btn-blue">BUY NOW</button> -->
                        </div>
                    </div>
                </div>
            </div>        
        </section>
        <script src="assets/js/highcharts.js"></script>
        <script src="assets/js/highcharts-more.js"></script>
        <script src="assets/js/exporting.js"></script>
       
      <script>
        jQuery(function ($) {
          // alert('ok');
          $('#container1').highcharts({
            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            
            title: {
                text: 'Speedometer'
            },
            
            pane: {
                startAngle: -90,
                endAngle: 90,
                  background: null
            },
              
              plotOptions: {
                  gauge: {
                      dataLabels: {
                          enabled: false
                   },
                      dial: {
                          baseLength: '0%',
                          baseWidth: 10,
                          radius: '100%',
                          rearLength: '0%',
                          topWidth: 1
                      }
                  }
              },
               
            // the value axis
            yAxis: {
                  labels: {
                      enabled: true,
                      x: 35, y: -10
                  },
                  tickPositions: [],
                  minorTickLength: 0,
                min: 0,
                max: 100,
                plotBands: [{
                    from: 0,
                    to: 45,
                    color: 'rgb(255, 0, 0)', // red
                      thickness: '50%'
                }, {
                    from: 45,
                    to: 70,
                    color: 'rgb(255,248,0)', // yellow
                      thickness: '50%'
                }, {
                    from: 70,
                    to: 80,
                    color: 'rgb(0, 128, 255)', // blue
                      thickness: '50%'
                }, {
                    from: 80,
                    to: 100,
                    color: 'rgb(27, 205, 107)', // green
                      thickness: '50%'
                }]        
            },
        
            series: [{
                name: 'Score',
                data: [<?=$percent;?>]
            }]
        
          });
        });
      </script>