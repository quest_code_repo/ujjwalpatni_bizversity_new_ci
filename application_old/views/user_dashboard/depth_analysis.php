<section class="section-dept">
        <div class="container" style="margin-bottom:30px;">
          <div class="row">
              <div class="col-md-10 col-md-offset-1">
                  <div class="analysis-box">
                  <div class="tab tab-dept">
                      <a href="<?php echo base_url(); ?>depth_analysis/?test_id=<?php echo $test_id ?>" class="taba">
                      <img src="exam_assets/images/icons/target.png">
                      <button class="tablinks active">Accuracy</button></a>
                      <!-- <a href="<?php echo base_url(); ?>see_difficulty_level/?test_id=<?php echo $test_id ?>" class="taba">
                      <img src="assets/images/icons/gauge.png">
                      <button class="tablinks  " onclick="">Difficulty Wise</button></a>
                      <a href="<?php echo base_url(); ?>topic_analysis/?test_id=<?php echo $test_id ?>" class="taba">
                      <img src="assets/images/icons/loupe.png">
                      <button class="tablinks" onclick="">Topic Wise</button></a> -->
                      <a href="<?php echo base_url(); ?>question_wise_analysis/?test_id=<?php echo $test_id ?>" class="taba">
                      <img src="exam_assets/images/icons/faq.png">
                      <button class="tablinks" onclick="">Question Wise</button></a>
                    </div>
                    
                    <div id="tab1" class="tabcontent">
                      <h3>Accuracy Analysis</h3>
                      <p></p>
                      <!-- <div id="chartContainer" style="height: 300px; width: 100%;"></div> -->
            					<div class="pie-chart">
						            <div id="chartdiv"></div>
                         </div>
                    
                    
                    <!-- <div id="tab2" class="tabcontent">
                      <h3>Difficulty Wise</h3>
                      <p>Paris is the capital of France.</p> 
                    </div>
                    
                    <div id="tab3" class="tabcontent">
                      <h3>Topic Wise</h3>
                      <p>Tokyo is the capital of Japan.</p>
                    </div>
                     <div id="tab4" class="tabcontent">
                      <h3>Question Wise</h3>
                      <p>Tokyo is the capital of Japan.</p>
                    </div> -->
                    <div class="clearfix"></div>
          </div>
                </div>
            </div>
        </div>
       </section>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="http://ashwameghtest.com/assets/js/highcharts.js"></script>
<script src="http://ashwameghtest.com/assets/js/highcharts-more.js"></script> 
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<!-- Chart code -->
<script>

 var test_id = "<?php echo $this->input->get('test_id'); ?>"

$(document).ready(function(){
        var mybaseUrl="<?=base_url();?>";
        var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
       var main_site_url = mybaseUrl.concat('user_dashboard/user_dashboard/ajax_accurracy?test_id='+test_id);

  $.ajax({
    url: main_site_url,
    method: "GET",
    dataType: 'json',
    success: function(data) {
    //alert(data.unattempted);
            var label = [];
        var correct = [];
        var wrong_answer = [];
        var un_attempt = [];



    //      for(var i in data) {
    //    label.push("Difficulty " + data[i].unattempted);
    //    score.push(data[i].correct_answers);
    //    wrong_answer.push(data[i].answers_wrong); 
        
    // }
       

var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "status": "Un Attempt",
    "litres": data.unattempted,
    "color":"#2F3D4A"
  }, {
    "status": "Correct",
    "litres": data.right,
    "color": "#77D782"
  }, {
    "status": "Wrong",
    "litres": data.wrong,
    "color": "#F75773"
  }],
  "valueField": "litres",
  "titleField": "status",
  "colorField": "color",
  "labelColorField": "color",
   "balloon":{
   "fixedPosition":true
  },
  "export": {
    "enabled": false
  }
});

  },

});
  });
</script>




