<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    body{
      background:#fff;
    }
   .question-space h5{
    margin-top: 20px;
margin-bottom: 20px;
   }

    </style>

<section class="section-dept">
        <div class="container" style="margin-bottom:30px;">
          <div class="row">
              <div class="col-md-10 col-md-offset-1">
                  <div class="analysis-box">
                  <div class="tab tab-dept">
                     <a href="<?php echo base_url(); ?>depth_analysis/?test_id=<?php echo $test_id ?>" class="taba">
                     <img src="exam_assets/images/icons/target.png">
                     <button class="tablinks">Accuracy</button></a>
                      <!-- <a href="<?php echo base_url(); ?>see_difficulty_level/?test_id=<?php echo $test_id ?>" class="taba">
                      <img src="assets/images/icons/gauge.png">
                      <button class="tablinks" onclick="">Difficulty Wise</button></a>
                      <a href="<?php echo base_url(); ?>topic_analysis/?test_id=<?php echo $test_id ?>" class="taba">
                      <img src="assets/images/icons/loupe.png">
                      <button class="tablinks" onclick="">Topic Wise</button></a> -->
                      <a href="<?php echo base_url(); ?>question_wise_analysis/?test_id=<?php echo $test_id ?>" class="taba">
                      <img src="exam_assets/images/icons/faq.png">
                      <button class="tablinks active" onclick="">Question Wise</button></a>
                    </div>
                    
                    <div id="tab1" class="tabcontent">
                      <h3>Question Wise Analysis</h3>
                      <p></p>
                      <!-- <div id="chartContainer" style="height: 300px; width: 100%;"></div> -->
                    
                     <div id="tab4" class="tabcontent">
                   <?php if($this->session->userdata('id')) { ?>
<div class="containers">
      <div class="questionNoOutter" style="margin-bottom: 30px;">
    <ul>  

<?php  

  // $test_id = $this->input->get('test_id');
  // $x = 1;
  // $score = 0;
  // if(!empty($count_answers_check))
  // {
  // foreach ($count_answers_check as $value){

  // $answered = $value['answer'];

  // $q_id = $value['question_id'];

  // $correct = $value['ques_right_ans'];

$test_id = $this->input->get('test_id');
$x = 1;
$score = 0;
if(!empty($count_answers_check))
{
  foreach ($count_answers_check as $value) 
{
$answered = $value['answer'];
$q_id = $value['question_id'];

$send_on_helper =  check_if_multiple_ques($q_id);

if($send_on_helper == 2){

  $correct = get_correct_answers_multiple($q_id);

} else 
{
  $correct = $value['ques_right_ans'];

}



if ($answered == $correct) {
$score++;
?><li class="quesNo" title="Correct" att="0"><div><a href="javascript:void(0);" style="color:inherit" onclick="see_submitted_questions(this);" id="<?php echo $q_id; ?>"><?php echo $x; ?></a></div><i class="questionIcons"></i></li>
<?php }
else if ($answered != $correct) { ?>

<li class="quesNo" title="Wrong Answer" att="0"><div><a href="javascript:void(0);" style="color:inherit" id="<?php echo $q_id; ?>" onclick="see_submitted_questions(this);"><?php echo $x; ?></a></div><i class="questionIcons wrongAnswer"></i></li>
<?php
}


$x++;
}
//end for each

if(!empty($count_not_attempted))
{
foreach($count_not_attempted as $notattmpt)
{ 

  ?>

<li class="quesNo" title="Not Attempted" att="0"><div><a href="javascript:void(0);" style="color:inherit" id="<?php echo $notattmpt['ques_id']; ?>" onclick="see_submitted_questions(this);"><?php echo $x; ?></a></div><i class="questionIcons notattempted"></i></li>

<?php $x++; } } }?>
</ul>
        </div>
<div style="clear:both;"></div>
  <div style="margin-top:20px;text-align: left;display:none;" class="see_question_tabs">
    <h3>Question <span id="question_no">0 </span></h3>
    <p id="question_name"> Find a rational number between  and . </p>
    <span id="append_question_image"></span>
        <div class="row question-space">
            <div class="col-sm-6">
              <div>
                <h5>A. <span id="f_recorded1">First</span> 
                <span id="append_ans_1_image"></span>
                </h5>
              </div>
            </div>
            <div class="col-sm-6">
              <div>
                <h5>B. <span id="f_recorded2">Second</span> 
                <span id="append_ans_2_image"></span>
                </h5>
              </div>
            </div>
        </div>
              <div class="row question-space">
                  <div class="col-sm-6">
                    <div>
                      <h5>C. <span id="f_recorded3">Third</span> 
                        <span id="append_ans_3_image"></span>
                      </h5>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div>
                      <h5>D. <span id="f_recorded4">Fourth</span> 
                      <span id="append_ans_4_image"></span>
                      </h5>
                    </div>
                  </div>
              </div>
  <!--   <h5>A. <span id="f_recorded1">First</span></h5>
    <h5>B. <span id="f_recorded2">Second</span></h5>
    <h5>C. <span id="f_recorded3">Third</span></h5>
    <h5>D. <span id="f_recorded4">Fourth</span></h5> -->

    <p id="submitted_answer"> Your Answer: <span id="your_answer"> N/A </span> </p>

    <div class="QBYQ_questioncorrect"><font class="answerMsg" color="#20d16a">Correct Answer.</font></div>
    <div class="QBYQ_questionIncorrect"><font class="answerMsg" color="#20d16a">Wrong Answer.</font></div>

    <div class="QBYQ_questionnotattempted" style="display: none;"><font class="" color="#20d16a">Not Attempted</font></div>

    <h4 id="correct_suggestions_head">Correct Answer:<span id="correct_suggestions"></span></h4>

    <div class="rightAnswerExpla">
      <i class="ExplanationMark"></i>
      <h2>Right Answer Explanation:</h2><strong><font size="2" face="Arial"><span id="explain_if_correct"></span></font></strong><div>&nbsp;</div>
      <span id="append_right_ans_exp_img"></span>
    </div>


      
  </div>

<div style="margin-top:20px;text-align: left;" class="first_time_tab">
  <h3>Question <span id=""> 1 </span></h3>
    <p id=""> <?php echo strip_tags($question_name,'<img><p><sub></sub><sup></sup><br>');
              if (!empty($question_image)) {
                ?>
                <span><a target="_blank" href="<?php echo base_url();?>uploads/test_question_images/<?php echo $question_image; ?>"><img width="150px;" src="<?php echo base_url();?>uploads/test_question_images/<?php echo $question_image; ?>"></a></span>
              <?php } ?> </p>
              <div class="row question-space">
                  <div class="col-sm-6">
                    <div>
                      <h5>A. <span id=""><?php echo strip_tags($ans_1,'<img><sub></sub><sup></sup><br>'); ?></span> 
                      <?php
                      if (!empty($ans_1_image)) {
                      ?>
                        <span><a target="_blank" href="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $ans_1_image; ?>"><img width="70px;" src="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $ans_1_image; ?>"></a></span>
                      <?php } ?></h5>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div>
                      <h5>B. <span id=""><?php echo strip_tags($ans_2,'<img><sub></sub><sup></sup><br>'); ?></span> <?php
                          if (!empty($ans_2_image)) {
                        ?>
                      <span><a target="_blank" href="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $ans_2_image; ?>"><img width="70px;" src="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $ans_2_image; ?>"></a></span>
                      <?php } ?></h5>
                    </div>
                  </div>
              </div>
              <div class="row question-space">
                <div class="col-sm-6">
                  <div>
                    <h5>C. <span id=""><?php echo strip_tags($ans_3,'<img><sub></sub><sup></sup><br>'); ?></span><?php
                    if (!empty($ans_3_image)) {
                      ?>
                      <span><a target="_blank" href="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $ans_3_image; ?>"><img width="70px;" src="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $ans_3_image; ?>"></a></span>
                    <?php } ?></h5>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div>
                    <h5>D. <span id=""><?php echo strip_tags($ans_4,'<img><sub></sub><sup></sup><br>'); ?></span><?php
                    if (!empty($ans_4_image)) {
                      ?>
                      <span><a target="_blank" href="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $ans_4_image; ?>"><img width="70px;" src="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $ans_4_image; ?>"></a></span>
                    <?php } ?></h5>
                   </div>
                </div>
              </div>



<p id=""> Your Answer: <span id=""> <?php echo string_replaces($my_answered); ?> </span> </p>

<?php if($status==1)
{ ?>
<div class="QBYQ_questioncorrect"><font class="answerMsg" color="#20d16a">Correct Answer.</font></div>
<?php }
else{ ?>
<div class="QBYQ_questionIncorrect"><font class="answerMsg" color="#20d16a">Wrong Answer.</font></div>

<h4 id="correct_suggestions_head">Correct Answer:<span id=""><?php echo string_replaces($corect_answer_value); ?></span></h4>

<?php } ?>
    <div class="rightAnswerExpla">
      <i class="ExplanationMark"></i>
      <h2>Right Answer Explanation:</h2><strong><font size="2" face="Arial"><?php echo $right_explaination ?></font></strong><div>&nbsp;</div><?php
              if (!empty($right_ans_exp_img)) {
                ?>
                <p><img width="150px;" src="<?php echo base_url();?>uploads/test_questions_ans_images/<?php echo $right_ans_exp_img; ?>"></p>
              <?php } ?></div>
        </div>
      
        </div>
        <?php } else{ ?> <div class="container"> <h2>Recored Not Available</h2> </div><?php } ?>
      </div>
      <div class="clearfix"></div>
      </div>
                </div>
            </div>
        </div>

       </section>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/canvasjs.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart.min.js"></script>
       <script>
function see_submitted_questions(item){

  // alert();
   var question_id = $(item).attr("id");
   var question_no = $(item).text();
   $(item).closest('li').addClass('activeQuestion');
  
 
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "<?php echo base_url();?>user_dashboard/user_dashboard/see_recorded_answers",
      data:'question_id='+ question_id + '&&question_no='+ question_no,
      cache: false,
      beforeSend: function(){
        
      },
      complete: function(){
          
      },
        success: function(data) {


         $("#question_no").html(data.ques_no);
         $("#question_name").html(data.question_name);       

         $("#f_recorded1").html(data.ans_1);
         $("#f_recorded2").html(data.ans_2);
         $("#f_recorded3").html(data.ans_3);
         $("#f_recorded4").html(data.ans_4);
         $("#explain_if_correct").html(data.right_explaination);

         $("#append_question_image").html(data.question_image);
         $("#append_ans_1_image").html(data.ans_1_image);
         $("#append_ans_2_image").html(data.ans_2_image);
         $("#append_ans_3_image").html(data.ans_3_image);
         $("#append_ans_3_image").html(data.ans_3_image);
         $("#append_ans_4_image").html(data.ans_4_image);
         $("#append_right_ans_exp_img").html(data.right_ans_exp_img);



          $(".QBYQ_questionIncorrect").css("display","block");
          $(".QBYQ_questioncorrect").css("display","block");
          $("#correct_suggestions_head").css("display","none");
          $(".first_time_tab").css("display","none");
          $(".see_question_tabs").css("display","block");
          $(".QBYQ_questionnotattempted").css("display","none");

         if(data.my_answered)
         {
            $("#your_answer").html(data.my_answered);
         }
         else
         {
                $("#your_answer").html(data.my_answered);
                $(".QBYQ_questionnotattempted").css("display","block");
                $(".QBYQ_questionIncorrect").css("display","none");
                $(".QBYQ_questioncorrect").css("display","none");
         
        }

         if(data['give_answered'] == data['corect_answer'])
         {
         $(".QBYQ_questionIncorrect").css("display","none");
         }
         else
         {
            $(".QBYQ_questioncorrect").css("display","none");
            $("#correct_suggestions_head").css("display","block");
            $("#correct_suggestions").html(data.corect_answer_value);
         }
         
         MathJax.Hub.Queue(["Typeset",MathJax.Hub,"tab4"]);

        
      }
    });


 
    // var sem_id = $(item).attr("id");
    //  $.ajax({
    //   type: "POST",
    //   url: "<?php echo base_url();?>catalyser_main2/remove_sem",
    //   data:'sem_id='+ sem_id,
    //   cache: false,
    //   beforeSend: function(){
        
    //   },
    //   complete: function(){
          
    //   },
    //     success: function(data) {
    //       if(data=="remove")
    //       {
    //           get_cart_details();
    //       }
        
    //   }
    // });
 
}
</script>





