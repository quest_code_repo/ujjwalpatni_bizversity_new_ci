
<?php
// var_dump($this->_ci_cached_vars);die;

 if($testdata[0]['test_type']==1){ ?>
 </pre>
      <!-- loader Section html start -->
        <section class="loader">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="loader-data">
                            <div class="loader-top text-left">
                                Hello <?=$this->session->userdata('full_name');?>, Here is your relationship status with mathematics. You have achieved <?php $percent = (($marks_gained) / $total_ques_marks) * 100; echo round($percent,2);?>% in your test.
                            </div>
                            <div class="loader-bottom">
                                <div id="container1" class="semi-chart meter-chart" >
                                </div>
                                <div class="poor">
                                Poor
                                </div>
                                <div class="average">
                                Average
                                </div>
                                 <div class="good">
                                Good
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </section>
        <!-- loader Section html end -->

        <!-- analysis Section html start -->
        <section class="analysis">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="analysis-data">
                            <div class="analysis-top text-left">YOUR COURSE WISE ANALYSIS</div>
                            <div class="analysis-bottom">
                                <div class="course aarambh">
                                      <div class="course-no">Course 01</div>
                                      <div class="course-title">आरम्भ</div>
                                      <div class="score-no">
                                        <?php 
                                          if($entdata[1]['total_que']==0){$entdata[1]['total_que']=1;}
                                          $arambh=($entdata[1]['right_ans']/$entdata[1]['total_que'])*100;
                                          echo round($arambh,2); ?>%
                                      </div>
                                </div>
                                <div class="course sopan">
                                    <div class="course-no">Course 02</div>
                                    <div class="course-title">सोपान</div>
                                    <div class="score-no">
                                    <?php 
                                          if($entdata[2]['total_que']==0){$entdata[2]['total_que']=1;}
                                          $arambh=($entdata[2]['right_ans']/$entdata[2]['total_que'])*100;
                                          echo round($arambh,2); ?>%
                                          </div>
                                </div>
                                <div class="course utkarsh">
                                      <div class="course-no">Course 03</div>
                                      <div class="course-title">उत्कर्ष</div>
                                      <div class="score-no">
                                      <?php 
                                          if($entdata[3]['total_que']==0){$entdata[3]['total_que']=1;}
                                          $arambh=($entdata[3]['right_ans']/$entdata[3]['total_que'])*100;
                                          echo round($arambh,2); ?>%
                                        </div>
                                </div>
                                <div class="course shikhar">
                                      <div class="course-no">Course 04</div>
                                      <div class="course-title">शिखर</div>
                                      <div class="score-no"><?php 
                                          if($entdata[4]['total_que']==0){$entdata[4]['total_que']=1;}
                                          $arambh=($entdata[4]['right_ans']/$entdata[4]['total_que'])*100;
                                          echo round($arambh,2); ?>%
                                        </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </section>
        <div class="clearfix"></div>

        <!-- analysis Section html end -->

        <!-- depth analysis Section html start -->
        <section class="depth">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="depth-data">
                            <div class="depth-txt">Your In depth Analaysis sesmester wise</div>
                            <div class="depth-table">
                                <table class="text-center">
                                    <tr>
                                        <th>Course</th>
                                        <th>Total Questions</th>
                                        <th>Attempted</th>
                                        <th>Correct</th>
                                        <th>Incorrect</th>
                                    </tr>
                                    <?php $p=1;foreach($entdata as $entdta){ ?>
                                    <tr>
                                      <?php if($p==1){echo"<td>आरम्भ</td>";} ?>
                                      <?php if($p==2){echo"<td>सोपान</td>";} ?>
                                      <?php if($p==3){echo"<td>उत्कर्ष</td>";} ?>
                                      <?php if($p==4){echo"<td>शिखर</td>";} ?>
                                        <td><?=$entdta['total_que'];?></td>
                                        <td><?=$entdta['right_ans']+$entdta['wrong_ans'];?></td>
                                        <td><?=$entdta['right_ans'];?></td>
                                        <td><?=$entdta['wrong_ans'];?></td>
                                    </tr>
                                    <?php $p++; } ?>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </section>
        <!-- depth analysis Section html end -->

        <!-- purchase course Section html start -->
        <section class="purchase">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="purchase-txt text-center">
                            <p><?php //print_r($this->session->all_userdata());
                              $percent = (($marks_gained) / $total_ques_marks) * 100;
                              if($percent<=50){
                                echo "RSM Strongly Recommend That You Should Enroll in Semester First.";
                              }elseif ($percent>50 && $percent<80) {
                                echo "RSM Strongly Recommend That You Should Enroll in Semester First. But You Can Choose Semester 2.";
                              }elseif ($percent>=80) {
                                echo "You Should Enroll in Semester 2. But You Can Skip This Semester.";
                              } ?></p>
                              <a href="pricing" class="btn btn-blue">BUY NOW</a>
                            <!-- <button class="btn btn-blue">BUY NOW</button> -->
                        </div>
                    </div>
                </div>
            </div>        
        </section>
        <script src="assets/js/highcharts.js"></script>
        <script src="assets/js/highcharts-more.js"></script>
        <script src="assets/js/exporting.js"></script>
       
      <script>
        jQuery(function ($) {
          // alert('ok');
          $('#container1').highcharts({
            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            
            title: {
                text: 'Speedometer'
            },
            
            pane: {
                startAngle: -90,
                endAngle: 90,
                  background: null
            },
              
              plotOptions: {
                  gauge: {
                      dataLabels: {
                          enabled: false
                   },
                      dial: {
                          baseLength: '0%',
                          baseWidth: 10,
                          radius: '100%',
                          rearLength: '0%',
                          topWidth: 1
                      }
                  }
              },
               
            // the value axis
            yAxis: {
                  labels: {
                      enabled: true,
                      x: 35, y: -10
                  },
                  tickPositions: [],
                  minorTickLength: 0,
                min: 0,
                max: 100,
                plotBands: [{
                    from: 0,
                    to: 25,
                    color: 'rgb(192, 0, 0)', // red
                      thickness: '50%'
                }, {
                    from: 25,
                    to: 75,
                    color: 'rgb(255, 192, 0)', // yellow
                      thickness: '50%'
                }, {
                    from: 75,
                    to: 100,
                    color: 'rgb(155, 187, 89)', // green
                      thickness: '50%'
                }]        
            },
        
            series: [{
                name: 'Speed',
                data: [<?=$percent;?>]
            }]
        
          });
        });
      </script>

<?php }else{?>
<section class="section-summray">
 		<div class="container">
      	<div class="row">
          	<div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default panel-summary">
                    <div  class="panel-heading">
                      Here is how you have performed in <b><?=ucfirst(@$testdata[0]['test_name']);?></b> test taken on <b><?=@$testtime[0]['date']?></b>
                    </div>
                      <div class="panel-body">
                        <div class="circle-score score1 wow fadeIn" >
                          <img src="<?php echo base_url(); ?>exam_assets/images/icons/trophy2.png">
                          <div class="score-no"><?php echo $marks_gained.'/'; 
                            if(!empty($total_ques_marks))
                            {
                              echo $total_ques_marks;
                            }else{
                              echo "0";
                            }
                          ?></div>
                          <div class="score-title">SCORE</div>
                        </div>
                        <div class="circle-score score2 wow fadeIn">
                          <img src="<?php echo base_url(); ?>exam_assets/images/icons/favorites-button.png">
                          <div class="score-no">
                            <?php 
                            if($question_attemp==0)
                            {
                              echo 0;
                            }else{
                              $accurracy = ($correct_ans / $question_attemp) * 100;
                              echo number_format($accurracy, 2, '.', '').'%'; 
                            }
                            ?>
                          </div>
                          <div class="score-title">ACCURACY</div>
                        </div>
                        <div class="circle-score score3 wow fadeIn" >
                          <img src="<?php echo base_url(); ?>exam_assets/images/icons/like.png">
                          <div class="score-no">
                          <?php 
                          $total_ques = count($question_count); 
                          $attempted_ques = $question_attemp;
                          $attempt_percent =  $attempted_ques/$total_ques;
                          $attmpt_ratio = $attempt_percent*100;
                          echo number_format($attmpt_ratio, 2, '.', '').'%';
                          ?>
                        </div>
                        <div class="score-title">ATTEMPT<br>PERCENTAGE</div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
              </div>
          </div>

          <div class="row mt-20">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-3 question-box box-1">
                    <div class="question-info">
                    <span class="pull-left q-no"><?php echo count($question_count); ?></span>
                      <span class="pull-right q-no">Total Questions</span>
                      <div class="clearfix"></div>
                      </div>
                  </div>
                  <div class="col-md-3 question-box box-2">
                  	<div class="question-info">
                  	<span class="pull-left q-no"><?php echo $question_attemp;?></span>
                      <span class="pull-right q-no">Attempted</span>
                      <div class="clearfix"></div>
                      </div>
                  </div>
                  <div class="col-md-3 question-box box-3">
                  	<div class="question-info">
                      <span class="pull-left q-no"><?php echo $correct_ans; ?></span>
                      <span class="pull-right q-no">Correct</span>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3 question-box box-4">
                  	<div class="question-info">
                  	<span class="pull-left q-no"><?php echo $question_attemp-$correct_ans; ?></span>
                      <span class="pull-right q-no">Incorrect</span>
                      <div class="clearfix"></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row mt-20">
          	<div class="col-md-10 col-md-offset-1">
              	<div class="col-md-6 pl-0">
                  	<div class = "media media-avg">
                         <a class = "pull-left" href = "#">
                            <img class = "media-object"  src="<?php echo base_url(); ?>exam_assets/images/icons/timer.png" alt = "Media Object">
                         </a>
                         
                         <div class = "media-body">
                            <h4 class = "media-heading">Average Time / Questions</h4>
                            <div class="timer">
                              <?php 
                              if($question_attemp==0)
                              {
                                echo '00:00:00';
                              }
                              else{
                                $total_time = $total_time_taken;
                                $get_time =  $total_time/$question_attemp;
                                
                                echo gmdate("H:i:s", $get_time*60);
                              }
                              ?>
                            </div>
                         </div>
                      </div>
                  </div>
                  <div class="col-md-6 pr-0">
                  	<div class = "media media-avg">
                         <a class = "pull-left" href = "#">
                            <img class = "media-object"  src="<?php echo base_url(); ?>exam_assets/images/icons/discount.png" alt = "Media Object">
                         </a>
                         
                         <div class = "media-body">
                            <h4 class = "media-heading">Percentage Score</h4>
                            <div class="timer">
                            <?php 
                            if(!empty($total_ques_marks))
                            {
                            $percent = (($marks_gained) / $total_ques_marks) * 100;
                            echo number_format($percent, 2, '.', '').'%';
                          }
                          else{
                            echo "0";
                          }
                            ?>
                              
                            </div>
                         </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row mt-20">
            <div class="col-md-10 col-md-offset-1">
                <div  class="box-analysis">
                  	<div class="col-md-4">
                      	<div class="dept-analysis text-center">
                        	<h4>In Depth Analysis</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a href="<?php echo base_url(); ?>depth_analysis/?test_id=<?php echo $test_id; ?>"class="btn btn-blue">Click to view</a>
                        </div>
                      </div>
                      <div class="col-md-4">
                      	<div class="dept-analysis text-center">
                        	<h4>Description Solution</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a href="<?php echo base_url();?>view_description/?test_id=<?php echo $test_id; ?>"><button class="btn btn-blue">Click to view</button></a>
                        </div>
                      </div>
                    <!--   <div class="col-md-4">
                      	<div class="dept-analysis text-center last-dept">
                        	<h4>Video Solution</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a href="<?php echo base_url();?>test_video_description/?test_id=<?php echo $test_id; ?>"><button class="btn btn-blue">Click to view</button></a>
                        </div>
                      </div> -->
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
          
      </div>
      <div class="row mt-20">      </div>
      <div class="row mt-20">      </div>
 </section>
 <?php } ?>