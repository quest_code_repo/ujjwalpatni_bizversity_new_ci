<style type="text/css">
  .tes a{
    background-color : #000 !important;
     background-image :none !important;
     color: #fff !important;
     font-weight:bold !important;
     font-size: 12pt;
  }
  .error_validate{color: red;}
</style>

<!-- Footer html -->
<div class="single-line"></div>
<footer class="footer-main">
    <div class="footer-top">
        <div class="container">
           <div class="row">
              <div class="col-sm-10 col-sm-offset-1">
                 <div class="row">
                <div class="col-sm-4">
                   <div class="contact-block">
                       <h4 class="wow fadeInUp">Contact Us</h4>
                       <p class="com-name">Patni pesonality Plus Pvt. ltd.</p>
                       <p>For Products Call +91 92291 86001- 86002</p>
                       <p>For Training Programs +91 92293 86002</p>
                       <p>E-mail : training@ujjwalpatni.com , ppppltd@gmail.com</p>
                   </div>
                </div>
                <div class="col-sm-4">
                   <h4 class="wow fadeInUp">More</h4>
                   <ul>
                       <li><a href="mentoring/coming_soon">Privacy Policy</a></li>
                       <li><a href="mentoring/coming_soon">Terms of Use</a></li>
                       <li><a href="mentoring/coming_soon">Subscription Agreement</a></li>
                       <li><a href="mentoring/coming_soon">Online Sales Disclaimer</a></li>
                   </ul>
                </div>
                <div class="col-sm-4">
                   <h4 class="wow fadeInUp">We Accept</h4> 
                   <div class="cards-img">
                      <ul>
                        <li><img src="front_assets/images/icons/footer-icon/1(3).png" class="img-responsive"></li>
                        <li><img src="front_assets/images/icons/footer-icon/2(2).png" class="img-responsive"></li>
                        <li><img src="front_assets/images/icons/footer-icon/3(1).png" class="img-responsive"></li>
                        <li><img src="front_assets/images/icons/footer-icon/4.png" class="img-responsive"></li>
                      </ul>
                      <ul>
                        <li><img src="front_assets/images/icons/footer-icon/5.png" class="img-responsive"></li>
                        <li><img src="front_assets/images/icons/footer-icon/6.png" class="img-responsive"></li>
                        <li class="pay-money"><img src="front_assets/images/icons/footer-icon/7(1).png" class="img-responsive"></li>
                      </ul>
                   </div>
                </div>
            </div>
              </div>
           </div>
        </div>
    </div>
</footer>
<!-- footer html end -->
<!-- Modal -->
<div id="register-modal" class="modal fade biz-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="section-login">

              <div id="Login-nav-tabs">
                <ul id = "myTab" class = "nav nav-tabs">
                    <li class = "active"><a href = "#home" data-toggle = "tab">Login</a></li>
                    <li><a href = "#ios" data-toggle = "tab">Register</a></li>   
                </ul>
              </div>

              <div id = "myTabContent" class = "tab-content">
                 <div class = "tab-pane fade in active" id = "home">
                     <div class="panel panel-default panel-login">
                            <div class="panel-body">
                <div class="login-form">

                  <div id="Login-box">
                    <form role = "form" id="login_form"  method="post">
                        <div class = "form-group">
                          <label for="name">E-mail</label>
                          <input type="email" name="username" id="login_username"  class="form-control" data-rule-required="true" data-msg-required="Please Enter Email Id" >
                          <span class="error_msg"></span> 
                        </div>
                        <div class = "form-group">
                          <label for="name">Password</label>
                          <input type="password" name="password"  id="login_password"  class="form-control" data-rule-required="true" data-msg-required="Please Enter Password"  >                                  
                        </div>

                        <input type="hidden" name="redirect_url" id="redirect_url" value="">
                        <div class="form-group">
                          <div class="row">
                             <div class="col-sm-6 pull-left">
                                <input type="checkbox" class="icheck pull-left" name="checkbox"  />&nbsp;Remember me
                             </div>
                             <!-- <div class="col-sm-6">
                               <div class="text-right">
                                 <a href="javascript:void(0);" class="forgot-pass-login" data-toggle="modal" data-target="#forgot-pass" data-dismiss="modal" onclick="download_second();">Forgot Password 
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                <input type="submit" id="signIn" class="btn btn-primary btn-register" value="Sign In">
                               </div>
                             </div> -->
                             <div class="col-sm-6">
                               <div class="text-right">
                                 <a href="javascript:void(0);" class="forgot-pass-login" id="Forget-button">Forgot Password 
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                <input type="submit" id="signIn" class="btn btn-primary btn-register" value="Sign In">
                               </div>
                             </div>
                          </div>
                              <span> </span>
                              <span></span>
                        </div>
                        <div class="form-group sign m-0">
                          
                        </div>

                        <div id="loginDiv"></div>
                    </form>
                  </div>


                  <div id="Forget-box" style="display: none;">
                    <form id="forgot_pass" name="forgot_pass" method="post">
                     <div class="panel-body">
                          <p>Please Enter your email address</p>
                          <div class = "form-group">
                              <label for="name">Email Id<span class="star-input-risk">*</span></label>
                              <input type="email" name="forgot_email" class="form-control" data-rule-required="true" data-msg-required="Please Enter Email Address">
                              <!-- <span class="error_msg">rtyy</span> -->
                           </div>

                           <div id="forgetDiv"></div>


                           <div class="">
                             <span><button type="submit" class="btn btn-primary btn-register" id="forget">Submit</button></span>
                             <span class="pull-right">
                              <a href="javascript:void(0);" class="forgot-pass-login" id="Login-button">Login
                              <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </span>

                          </div>
                          <p>You will receive your updated password on email</p>
                    </div>
                    </form>
                  </div>

                </div>
              </div>
                       </div>
                 </div>
                 <div class = "tab-pane fade" id = "ios">
                     <div class="panel panel-default panel-login">
                            <div class="panel-body">
                              <div class="login-form">
                                <form id="registration_form" role="form" action="<?php echo base_url('home/user_registration')?>"  method="post">
                                   <div class = "form-group">
                                      <label for = "name">Name<span class="star-input-risk">*</span></label>
                                      <input type="text" name="full_name" id="full_name" class="form-control" data-rule-required="true" data-msg-required="Please Enter Name">
                                      <!-- <span class="error_msg">try</span> -->
                                   </div>
                                   <div class = "form-group">
                                      <label for="name">Email Id<span class="star-input-risk">*</span></label>
                                      <input type="email" name="username" id="email" class="form-control" data-rule-required="true" data-msg-required="Please Enter Email ">
                                      <!-- <span class="error_msg">rtyy</span> -->
                                   </div>
                                    <div class = "form-group">
                                      <label for = "name">Password<span class="star-input-risk">*</span></label>
                                      <input type="password" id="password" name="password" class="form-control" data-rule-required="true" data-msg-required="Please Enter Password">
                                     <!--  <span class="error_msg">uyuuy</span> -->
                                   </div>
                                   <div class = "form-group">
                                      <label for="name">Confirm Password<span class="star-input-risk">*</span></label>
                                      <input type="password" id="confirm_password" name="confirm_password" class="form-control" data-rule-required="true" data-msg-required="Please Enter Confirm Password">
                                      <!-- <span class="error_msg">tretete</span> -->
                                   </div>
                                    <div class="pull-left">
                                   <input type="checkbox" class="icheck pull-left" id="check" name="term_conditions" checked />&nbsp;I agree with <a href="mentoring/coming_soon">Terms of Use of website</a>
                                    </div>
                                  <div class="text-right">
                                     <input type="submit"  class="btn btn-primary btn-register" id="addUser" value="Register"/>
                                  </div>

                                  <span id="vali_scheme1" class="error_validate1"></span>

                                </form>
                              </div>
                            </div>
                         </div>
                   </div>
                 </div>
               </div>
      </div>
    </div>

  </div>
</div>





<!-- Forgot Password  -->

<!-- Modal -->
<div id="forgot-pass" class="modal fade biz-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Forgot Password</h4>
      </div>
      <div class="modal-body">
         <div class="panel panel-default panel-login">

          <form id="forgot_pass" name="forgot_pass" method="post">
       <div class="panel-body">
            <p>Please Enter your email address</p>
            <div class = "form-group">
                <label for="name">Email Id<span class="star-input-risk">*</span></label>
                <input type="email" name="forgot_email" class="form-control" data-rule-required="true" data-msg-required="Please Enter Email Address">
                <!-- <span class="error_msg">rtyy</span> -->
             </div>

             <div id="forgetDiv"></div>


             <div class="">
               <span><button type="submit" class="btn btn-primary btn-register" id="forget">Submit</button></span>
               <!-- <span class="pull-right">
                <a href="" class="forgot-pass-login" data-toggle="modal" data-target="#register-modal" onclick="download_first();">Login
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </span> -->

            </div>
            <p>You will receive your updated password on email</p>
      </div>

      </form>
      </div>
    </div>
     </div>
  </div>
</div>

<!-- Modal -->
<div id="Corporate-Modal" class="modal fade biz-modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title"><img src="front_assets/images/other/logo.png"></h4>
      </div>
      <div class="modal-body">
         <div>
           <h4>Hi Customer,</h4>
           <p>You may face some technical issues while using the website since we are upgrading it.</p>
           <p>Please corporate with us!</p>
           <p>Thanks.</p>
         </div>
      </div>
    </div>
  </div>
</div>

<div id="lc_chat_layout" class="lc-support-widget lc-offline lc-expanded" >
    <div id="lc_chat_header" class=" lc-header-bg">
        <span id="lc_chat_title">Leave us a message here...</span>
        <span class="pull-right"><img src="front_assets/images/icons/message.png" class="img-responsive" style="height: 24px;"></span>
    </div>
    <div id="lc_chat_container">
        <div id="lc_offline_chat_form" class="lc-offline-form">
           
            <form id="short_message_form" name="short_message_form" method="post">
                <ul class="lc-form-field-wrap">
                    <li class="lc-textfield-wrap">
                        <label for="lc_chat_offline_name" class="lc-chat-required">Name<span> *</span></label>
                        <input type="text"  id="chat_name" name="chat_name" data-rule-required="true" data-msg-required="Please Enter Name"></li>
                    <li class="lc-textfield-wrap">
                       <label for="lc_chat_offline_email" class="lc-chat-required">Email<span> *</span></label>
                       <input type="email"  id="chat_email" name="chat_email" data-rule-required="true" data-msg-required="Please Enter Email"></li>
                    <li class="lc-textfield-wrap">
                        <label for="lc_chat_offline_textarea" class="lc-chat-required">Message<span> *</span></label>
                        <textarea type="text" id="chat_message" name="chat_message" data-rule-required="true" data-msg-required="Please Enter Message"></textarea></li>
                </ul>

                <div id="chatDiv"></div>

                	<div id="lc_button_holder" class="lc-button-holder">
                         <button id="chat_submit" class="lc-button-submit">Submit</button>
                     </div>

            </form>
            
        </div>
    </div>
</div>

<div class="app-sticky">
    <div class="links">
        <div class="txt">Download App</div>
        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.ujjwal.glt020&hl=en"><img src="front_assets/images/icons/android.png"></a>
        <a target="_blank" href="https://itunes.apple.com/in/app/ujjwal-patni/id976941614?mt=8"><img src="front_assets/images/icons/apple.png"></a>
    </div>
</div>



    <!-- Modal -->
                      <div class="modal fade free-e-book-modal" id="myModal" role="dialog">
                        <div class="modal-dialog">
                        
                          <!-- Modal content-->
                          <div class="modal-content">
                           <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
	                        <h4 class="modal-title text-center">Please Fill The Form</h4>
	                      </div>
                        <form id="corporate_form" name="corporate_form" method="post">
                            <div class="modal-body">
                                
			                        <div class="row">
			                             <div class="col-sm-6">
			                                  <div class="form-group">
			                                    <label>Name:</label>
			                                    <input class="form-control" type="text" name="corporate_name" id="corporate_name" data-rule-required="true" data-msg-required="Please Enter Name">
			                                  </div>
			                             </div>
			                             <div class="col-sm-6">
			                                  <div class="form-group">
			                                    <label>Email address:</label>
			                                    <input class="form-control" type="email" name="corporate_email" id="corporate_email" data-rule-required="true" data-msg-required="Please Enter Email">
			                                  </div>
			                             </div>
			                        </div>    
			                       <!--  <div class="row">
			                             <div class="col-sm-6">
			                                  <div class="form-group">
			                                    <label>Organization Name:</label>
			                                    <input class="form-control" type="text" name="corporate_organization" id="corporate_organization" data-rule-required="true" data-msg-required="Please Enter Organization">
			                                  </div>
			                             </div>
			                             <div class="col-sm-6">
			                                  <div class="form-group">
			                                    <label>Position:</label>
			                                    <input class="form-control" type="text" name="corporate_position" id="corporate_position" data-rule-required="true" data-msg-required="Please Enter Position">
			                                  </div>
			                             </div>
			                        </div> -->    
			                      <!--   <div class="row">
			                             <div class="col-sm-6">
			                          
			                                     <div class="form-group">
												  <label>Industry:</label>
												  <select class="form-control" id="corporate_industry" name="corporate_industry" data-rule-required="true" data-msg-required="Please Select Industry">
                            <option value="">Select Industry</option>
												    <option value="Real Estate">Real Estate</option>
												    <option value="Education">Education</option>
												    <option value="Transportation">Transportation</option>
												    <option value="Healthcare">Healthcare</option>
												     <option value="Other">Other</option>

												  </select>
												</div> 
			                          
			                             </div>
			                             <div class="col-sm-6">
			                                  <div class="form-group">
			                                    <label>Staff Count:</label>
			                                    <input class="form-control descPriceNumVal" type="text" name="corporate_staff" id="corporate_staff" data-rule-required="true" data-msg-required="Please Enter Staff Count">
			                                  </div>
			                             </div>
			                        </div> -->     
			                             <div class="text-right">
			                             	<button type="submit" class="btn btn-common" id="corporateAdd">Submit</button> 
			                             </div>
			                              
			                   
                            </div>

                            <div id="corpDiv"></div>

                        </form>


                          </div>
                        </div>
                      </div>







        <a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">^</a>

        
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
       <script src="front_assets/js/animationCounter.js" charset="utf-8"></script>
        <!-- JQuery-ui -->
        <script type="text/javascript" src="front_assets/js/jquery-ui.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script type="text/javascript" src="front_assets/js/bootstrap.min.js"></script>
        <!-- dropdownhover effects JavaScript -->
        <script type="text/javascript" src="front_assets/js/bootstrap-dropdownhover.min.js"></script>
        <!-- Owl JavaScript -->
        <script type="text/javascript" src="front_assets/js/owl.carousel.min.js"></script>
        <!-- wow JavaScript -->
        <script type="text/javascript" src="front_assets/js/wow.min.js"></script>
        <!-- video player JavaScript -->
        
        <!-- Custom JavaScript -->
        <script type="text/javascript" src="front_assets/js/custom.js"></script>

        <script type="text/javascript" src="assets/admincss/js/jquery.validate.min.js"></script>

        <link href="<?php echo base_url(); ?>front_assets/css/jquery.datetimepicker.css" type="text/css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>front_assets/js/jquery.datetimepicker.js"></script>
        <script>
          $('.datetimepicker').datetimepicker({
              formatTime: 'H:i:s',
              formatDate: 'Y-m-d',
          });
          $('.datepicker').datetimepicker({
              timepicker: false,
              format: 'Y-m-d',
              formatDate: 'Y-m-d',
          });
        </script>
        


<script type="text/javascript">
function download_second()
  {
      $('#forgot-pass').show();
      $('#register-modal').hide();
  }
</script>
    <script type="text/javascript">
    $('#counter-block').ready(function(){
         $('.subs1').animationCounter({
          start: 23487,
          step: 6,
          delay:200,
          
        });
         $('.subs2').animationCounter({
          start: 124567,
          step: 4,
          delay:200,
          
        });
         $('.follow').animationCounter({
          start: 4568743,
          step: 8,
          delay:200,
          
        });
       
        $('.date_picker').datepicker({
            date: new Date(),
            timepicker: false,
            dateFormat: 'yy-mm-dd',
        });
    });
    
    </script>
        <script>
            $("#lc_chat_header").click(function(){
                $("#lc_chat_container").toggle();
            }); 
        </script>
        <script>
          $( function() {
             $( "#datepicker1" ).datepicker({ firstDay: 1 });
             $( "#datepicker2" ).datepicker({ firstDay: 1 });
            $( "#datepicker3" ).datepicker({ firstDay: 1 });
            // $( ".datepickerTest" ).datepicker({ firstDay: 1 });
          } );
        </script>

<script type="text/javascript">
$(document).ready(function() {

  // var a = $('#event_dates').val();

       availableDates = [];//mm-dd-yy
       
    $('.dummyTest').datepicker({
    dateFormat: 'mm-dd-yy',
    //startDate: "03-05-2018",
    //endDate: "01-10-2018",
    beforeShowDay: function(d) {
        var dmy = (d.getMonth()+1)
        if(d.getMonth()<9) 
            dmy="0"+dmy; 
        dmy+= "-"; 



        if(d.getDate()<10) dmy+="0"; 
            dmy+=d.getDate() + "-" + d.getFullYear(); 

        //console.log(dmy+' : '+($.inArray(dmy, availableDates)));

        if ($.inArray(dmy, availableDates) != -1) {

          //var Highlight = availableDates;
            
             //return [false,"","unAvailable"]; 
             return [true, "tes"];
        
        } else{
            return [true, "","Available"]; 
        }
    },
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
    });

});

</script>


<script type="text/javascript">
  
    function set_values()
    {

        var a = $('#event_dates').val();
 
      //mm-dd-yy

       
         availableDates1 = [];

         var b  = a.split(',');

       availableDates1 = availableDates1.concat(b);

       console.log(availableDates1);

    $('.datepickerTest').datepicker({

    dateFormat: 'mm-dd-yy',
    startDate: "01-05-2018",
    endDate: "01-10-2018",
    beforeShowDay: function(d) {
        var dmy = (d.getMonth()+1)
        if(d.getMonth()<11) 
            dmy="0"+dmy; 
        dmy+= "-"; 

        if(d.getDate()<12) dmy+="0"; 
            dmy+=d.getDate() + "-" + d.getFullYear(); 

        //console.log(dmy+' : '+($.inArray(dmy, availableDates)));

        if ($.inArray(dmy, availableDates1) != -1) {

          //var Highlight = availableDates;
            
             //return [false,"","unAvailable"]; 
             return [true, "Highlighted"];
        
        } else{
            
            return [true, "","Available"]; 
        }
    },
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
    });
    }

</script>

<script type="text/javascript">
        $(document).ready(function () {
        //called when key is pressed in textbox
        $(".orgPriceNumVal").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $(".orgPriceNumVal").attr("placeholder","Digits Only");
        return false;
        }
        });

        $(".descPriceNumVal").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57)) {
        //display error message
        $(".descPriceNumVal").attr("placeholder","");
        return false;
        }
        });

    });

/******END: Check Price Number  Input**********/

</script>


        <!---  *******************  Registration Code Here -->


<style type="text/css">
.error_validate{color: red;}
</style>

  <script type="text/javascript">
        $(document).ready(function(){
        $('#addUser').click(function(){
        $('#registration_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {

          if($('input[name=term_conditions]:checked').length<=0)
          {
              $('#vali_scheme1').css('color','red');
              $('#vali_scheme1').html('Please Check Terms And Condition');
              return false;

          }

            $.ajax({
                      type :'POST',
                      url  :'<?php echo base_url("home/user_registration")?>',
                      data : $('#registration_form').serialize(),
                      success:function(resp)
                      {
                        resp = resp.trim();
                          if(resp == "SUCCESS")
                          {
                              $('#vali_scheme1').show();
                              $('#vali_scheme1').css('color','green');
                              $('#vali_scheme1').text('You Have Successfully Registered With Us');
                              $("#registration_form")[0].reset();

                               setTimeout(function () {
                         window.location.href= '<?php echo current_url();?>'; // the redirect goes here

                              },2000);
                          }
                          else if(resp == "CHECK_ERROR")
                          {
                              $('#vali_scheme1').show();
                              $('#vali_scheme1').css('color','red');
                              $('#vali_scheme1').text('Your Email Is Already Registered With Us, Please Login To Continue');
                          }
                          else
                          {
                              $('#vali_scheme1').show();
                              $('#vali_scheme1').css('color','red');
                              $('#vali_scheme1').text('Something Went Wrong');
                          }
                      } 


            });
            
            
         }
        });
        });
    });
</script>

        <!-- ************************************************************-->

        <!-- *****************  Login Code Starts ***************-->

          <script type="text/javascript">
        $(document).ready(function(){
        $('#signIn').click(function(){
        $('#login_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {
            $.ajax({
                      type :'POST',
                      url  :'<?php echo base_url("home/user_login")?>',
                      data : $('#login_form').serialize(),
                      success:function(resp)
                      {
                          resp = resp.trim();

                          if(resp == "URL")
                          {
                            var url = $("#redirect_url").val();
                            // alert();
                            window.location.href="<?php echo base_url();?>"+url;
                          }

                          if(resp == "SUCCESS")
                          {
                              window.location.href="<?php echo current_url();?>";
                          }
                          else if(resp == "EMAIL_ERROR")
                          {
                              $('#loginDiv').show();
                              $('#loginDiv').css('color','red');
                              $('#loginDiv').text('Email Id Not Found');
                          }
                          else if(resp == "PASS_ERROR")
                          {
                              $('#loginDiv').show();
                              $('#loginDiv').css('color','red');
                              $('#loginDiv').text('Please Enter Correct Password');
                          }

                          else
                          {
                              $('#loginDiv').show();
                              $('#loginDiv').css('color','red');
                              $('#loginDiv').text('Something Went Wrong');
                          }
                      } 


            });
            
            
         }
        });
        });
    });
</script>


      

<!-- *****************  Contact Us  ***************-->
<script type="text/javascript">
        $(document).ready(function(){
        $('#addContact').click(function(){
        $('#contact_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {
            $.ajax({
                      type :'POST',
                      url  :'<?php echo base_url("home/add_new_contact_imformation")?>',
                      data : $('#contact_form').serialize(),
                      success:function(resp)
                      {
                          resp = resp.trim();

                          if(resp == "SUCCESS")
                          {
                              $('#contactDiv').show();
                              $('#contactDiv').css('color','green');
                              $('#contactDiv').text('Your Details Submitted Successfully');

                               setTimeout(function () {
                         window.location.href= '<?php echo base_url("contact-us")?>'; // the redirect goes here

                              },2000); 
                          }
                          else
                          {
                              $('#contactDiv').show();
                              $('#contactDiv').css('color','red');
                              $('#contactDiv').text('Something Went Wrong');
                          }
                      } 


            });
            
            
         }
        });
        });
    });
</script>


<!-- *****************  Contact Us  ***************-->
<!-- this function for get events details on event change -->


<!-- *********************  Chat box ***********************  -->

	<script type="text/javascript">
        $(document).ready(function(){
        $('#chat_submit').click(function(){
        $('#short_message_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {
            $.ajax({
                      type :'POST',
                      url  :'<?php echo base_url("home/add_new_chat_imformation")?>',
                      data : $('#short_message_form').serialize(),
                      success:function(resp)
                      {
                          resp = resp.trim();

                          if(resp == "SUCCESS")
                          {
                              $('#chatDiv').show();
                              $('#chatDiv').css('color','green');
                              $('#chatDiv').text('Your Details Submitted Successfully');
                              $("#short_message_form")[0].reset();

                                setTimeout(function () {
                              $('#chatDiv').hide();// the redirect goes here

                              },2000);
                          }
                          else
                          {
                              $('#chatDiv').show();
                              $('#chatDiv').css('color','red');
                              $('#chatDiv').text('Something Went Wrong');
                          }
                      } 


            });
            
            
         }
        });
        });
    });
</script>


  <!-- ***********************  forgot passs **************************-->

<script type="text/javascript">
        $(document).ready(function(){
        $('#forget').click(function(){
        $('#forgot_pass').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {
            $.ajax({
                      type :'POST',
                      url  :'<?php echo base_url("home/forgot_pass")?>',
                      data : $('#forgot_pass').serialize(),
                      success:function(resp)
                      {
                          resp = resp.trim();

                          if(resp == "SUCCESS")
                          {
                              $('#forgetDiv').show();
                              $('#forgetDiv').css('color','green');
                              $('#forgetDiv').text('Password Sent To Your Email Id');
                              // $("#short_message_form")[0].reset();

                              //   setTimeout(function () {
                              // $('#chatDiv').hide();// the redirect goes here

                              // },2000);
                          }
                          else if(resp == "EMAIL_ERROR")
                          {
                              $('#forgetDiv').show();
                              $('#forgetDiv').css('color','red');
                              $('#forgetDiv').text('Your Email Id Is not Registered With Us');
                          }

                          else
                          {
                               $('#forgetDiv').show();
                              $('#forgetDiv').css('color','red');
                              $('#forgetDiv').text('Something Went Wrong');
                          }
                      } 


            });
            
            
         }
        });
        });
    });
</script>


  <!-- ********************************************************************-->


	

<!-- *****************************************************-->
    <script type="text/javascript">
    function get_events_date_by_city(city_id)
    {
        // alert(city_id);
        var category_id = $("#event_category_id").val();
        $.ajax({
            type :'POST',
            // dataType: 'json',
            url  :'<?php echo base_url("events/get_events_date_by_city")?>',
            data : { "city_id":city_id,"category_id":category_id },
            success:function(resp)
            {
                $('#program_date').html(resp); 
            } 
        });
    }
    function get_events_details_by_date(program_date)
    {
        // alert(city_id);
        var category_id = $("#event_category_id").val();
        $.ajax({
            type :'POST',
            dataType: 'json',
            url  :'<?php echo base_url("events/get_events_details_by_date")?>',
            data : { "program_date":program_date,"category_id":category_id },
            success:function(resp)
            {
                if(resp.msg == "SUCCESS")
                {  
                    $('#program_name').val(resp.program_name);
                    $('#programes_id').val(resp.program_id);
                    $('#actual_price').text("₹ "+resp.actual_price);
                    $('#programe_price').val(resp.encode_price);
                } 
            } 
        });
    }
    </script>

<script>
$(document).ready(function(){

    $("#btn-share1").click(function(){
        $("#social-share1").toggle();
    });

    $("#btn-share2").click(function(){
        $("#social-share2").toggle();
    });

});
</script>

<script>
$(document).ready(function(){

    
    

    $("#Forget-button").click(function(){
      $("#Login-box").hide();
      $("#Login-nav-tabs").hide();
      $("#Forget-box").show();
    });

    $("#Login-button").click(function(){
      $("#Login-box").show();
      $("#Login-nav-tabs").show();
      $("#Forget-box").hide();
    });

});
</script>

    
        <script type="text/javascript">

    function get_all_courses(){
       jQuery('.CourseName').autocomplete({
              source: '<?php echo base_url()?>elearning/get_course_autocomplete/?course='+$('#CourseName').val(),
            minLength: 1,
            select: function(event, ui) {

              jQuery('.CourseName').val(ui.item.pname); 
              jQuery('#CourseUrl').val(ui.item.url); 
              
            }
        });
    }

    </script>


    </body>
</html>
