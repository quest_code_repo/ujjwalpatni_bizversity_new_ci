<script type="text/javascript" async
 src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML&locale=en">
</script>
<style type="text/css">
.other_option{list-style-type:none !important;padding: 0;}
.other_option li a{ color: #000;font-size: 20px;}
.other_option li{margin-bottom: 10px;
}

.MJXc-display{
        display: inline-block ! important;
    } 

    .other-option a.active, .other-option a:hover {
        color: #777;
        background-color: #D4F0FB;
        display: inline-block;
        width: 100%;
        outline: none;
    }

    .checked-option{
         color: #777;
        background-color: #D4F0FB;
        display: inline-block;
        width: 100%;
        outline: none;
    }


label > input + img {
    border-radius: 20px;
}
.other-option a:hover .option-img{
cursor: default;
}



@media (max-width: 767px) {
    .left-side {
        padding: 0;
    }
    .other-option img{
        height: 32px;
        width: auto;
    }
}


  
</style>
<section class="section-test new">
    <div class="container-fluid p-0">
        <div class="p-0 test-start-mt">
            <?php 
            // echo "<pre>"; print_r($questions); echo "</pre>"; 
              // echo "<pre>"; print_r($this->session->all_userdata()); //echo "</pre>"; 
             // print_r($thisquestion);die;
             // print_r($this->session->userdata('sec_subject'););die;
            ?>
            <div class="question-head">
                <div class="col-sm-6"><h3 style="color:;" class="common-heading"><?=ucfirst(@$this->session->userdata("test_name"));?></h3></div>
                <div class="col-sm-6 pull-right">
                    
                    <?php $sections= $this->session->userdata('sec_subject');


                     $cl=0;
                     foreach ($sections as $key => $section) {
                       
                        $secId='sec'.$key;
                     ?>
                  <!--   <button class="btn btn-reveiw btn-feature" id='<?=$secId;?>' onclick="selsec(<?=$key?>);" style="background: <?=($cl==0)?'#C74724':''?>;" ><?=$section['subject_name']?></button>
                    <?php $cl++; } ?> -->
                    <!-- <button class="btn btn-reveiw btn-feature" id="mat_q_no1" onclick="selsec('mat_q_no');" >MAT</button> -->
                    <!-- <button class="btn btn-reveiw btn-feature" id="lang_q_no1" onclick="selsec('lang_q_no');" >Language</button> -->
                </div>
                <div class="clearfix"></div>
                <div class=" col-sm-12 question-no">
                    <span class="pull-left">Question No <span id='queno'>1</span>   <span id='ques_type'><?php
                    if ($thisquestion['question_type']==1) {
                        //echo "Single Choice Question";

                    }else{
                        //echo "Multiple Choice Question";
                    }
                    ?>
                    </span></span>
                    <span class="pull-right"></span>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="left-side" id="div1">
                <div class="question-type">
                    <div class="question">
                        <input type="hidden" id="single_ques_type" value="<?php echo $thisquestion['question_type'];?>">    
                        <h4><?php $check_ques_type =  $thisquestion['question_type']; ?> </h4>
                        <?php  if($check_ques_type == 1) { ?>
                      
                        <h4><?php echo strip_tags($thisquestion['question_desc'],'<img><p><sub></sub><sup></sup><br>');?></h4>
                        <?php
                            if (!empty($thisquestion['question_image'])) {
                        ?>
                          <img width="100px;" src="uploads/test_question_images/<?php echo $thisquestion['question_image']; ?>">
                        <?php } ?>

                        <?php (!empty($thisquestion['image']))?$thisquestion['image']:'';?>
                        <ul class="other-option">
                            <?php 

                            if(!empty($show_submited)){

                                $ans_check =  $show_submited[0]['answer'];
                            }

                            $alpha='A'; 
                            $i=1; 
                            $right=$questions['answers'];
                            // echo "<pre>";print_r($thisquestion['answers']);die;
                            if(!empty($thisquestion['answers'])){
                                foreach ($thisquestion['answers'] as $key => $value) {

                                    if ($value['id'] == $ans_check) {
                                        $checked = 'checked';
                                    } else {
                                        $checked = '';
                                    }

                            ?>
                            <li>
                                <a href="javascript:;" class="<?php if (!empty($checked)) { echo "checked-option"; }?>" id="ancar_<?php echo $key;?>" index-data="<?php echo $key;?>">
                                <div>
                                    <label><input type="radio" name="answer" id="radio_<?php echo $key;?>" value="<?=$value['id'];?>" <?=($right[$key]['s']==1)?'checked="checked"':'';?> <?php echo $checked; ?> />
                                    <img src="exam_assets/images/icons/letter<?=$alpha;?>.png">  <?php echo strip_tags($value['text'],'<img><sub></sub><sup></sup><br>'); ?></label>
                                    <?php
                                    if (!empty($thisquestion['ans_'.$i.'_image'])) {
                                    ?>
                                    <img width="100px;" src="uploads/test_questions_ans_images/<?php echo $thisquestion['ans_'.$i.'_image']; ?>" class="option-img"/>
                                    <?php } ?>
                                </div>
                                </a>
                            </li>
                            <?php $alpha++; $i++; } } ?>
                        </ul>

                        <?php }else{ ?>


                          <h4><?php echo strip_tags($thisquestion['question_desc'],'<img><p><sub></sub><sup></sup><br>');?></h4>
                          <?php
                          if (!empty($thisquestion['question_image'])) {
                          ?>
                            <img width="50px;" src="uploads/test_question_images/<?php echo $thisquestion['question_image']; ?>">
                          <?php } ?>

                        <?php (!empty($thisquestion['image']))?$thisquestion['image']:'';?>
                        <ul class="other-option">
                            <?php $alpha='A';$i=1; 
                            $right=$questions['answers'];

                            /***********New Work By Shubham********/

                                if(!empty($show_submited)){

                                    $dats_check =  $show_submited[0]['answer'];

                                        $resls = explode(',',$dats_check);
                                    }

                            /***********END****************/
                            if(!empty($thisquestion['answers'])){

                                foreach ($thisquestion['answers'] as $key => $value) {

                                    $checke_val = '';

                                    if(isset($resls)){

                                        if(in_array($value['id'],$resls)){

                                            $checke_val = 'checked';

                                        } else {

                                            $checke_val = '';
                                        }
                                    }

                            ?>
                            <li>
                                <a href="javascript:;" class="<?php if (!empty($checke_val)) { echo "checked-option";}?>" id="ancar_<?php echo $key;?>" index-data="<?php echo $key;?>">
                                <div>
                                    <label><input type="checkbox" name="answer" id="checkbox_<?php echo $key;?>" value="<?=$value['id'];?>" <?php echo $checke_val; ?>/>
                                    <img src="exam_assets/images/icons/letter<?=$alpha;?>.png">  <?php echo strip_tags($value['text'],'<img><sub></sub><sup></sup><br>'); ?></label>

                                    <?php
                                    if (!empty($thisquestion['ans_'.$i.'_image'])) {
                                    ?>
                                      <img width="50px;" src="uploads/test_questions_ans_images/<?php echo $thisquestion['ans_'.$i.'_image']; ?>" class="option-img">
                                    <?php } ?>
                                </div>
                                </a>
                            </li>
                           <?php $alpha++;$i++;} } ?>
                        </ul>

                        <input type="hidden" name="custom_hiiden" value="2" id="custom_hiiden">

                        <?php }  ?>


                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="btn-group3 text-center ">
                <button class="btn btn-save btn-feature" onclick="save_ans();" >Save & Next</button>
                <button class="btn btn-reveiw btn-feature" onClick="skip()">Skip</button>
                <button class="btn btn-reveiw btn-feature" onclick="markReview()">Review Later</button>
                <button class="btn btn-clear btn-feature btn-display-revert" onclick="clearS()">Clear Selection</button>

                <button onclick="confirmAndFinishExam()" class="btn btn-clear btn-feature btn-display" href="finish_exam">Finish</button>
            </div>
        </div>
                
        <div class="test-right">
            <div class="right-side" id="mySidenav">
                <h4 class="heading-black  text-center">Question Overviews:</h4>
                <div class="overflow-box">
                <?php 
                $sec_que_nos=$this->session->userdata('sec_subject');
                
                $dis=0; 
                foreach($sections as $key => $section) {
                    

               // $mat_q_count=count($sec_que_nos[$section['ques_id']]);

                 $mat_q_count=count($sec_que_nos);
                    // echo $section['id'];die();
                ?>
                <div class="btn-group4 new" id="sec_q_<?=$key?>" style="display: <?=($dis==0)?'':'none'?>;">
                    <?php for($ii=0; $ii<$mat_q_count; $ii++){  

                        if (!empty($ans_submited)) {

                            foreach ($ans_submited as $submit_val) {

                                if ($ii+1 == $submit_val['ques_number']) {
                                    $green = 'btn btn-green3';
                                    break;
                                }else{
                                    $green = '';
                                }
                            }
                        }
                        
                    ?>
                        <a href="javascript:;" class="<?php if (!empty($green)) { echo $green;
                        }else{?> btn <?=($ii==0)?'btn-red':'btn-number'; } ?>" id="qbtn<?=$key?>_<?=$ii+1;?>" onClick="jumpQue(this.id,<?=$ii;?>,'<?=$section['chapter_id']?>')" ><?=$ii+1;?></a>
                    <?php } ?>                                              
                </div>
                <?php $dis++; } ?>

                </div>
                <ul class="chk">
                    <li>
                    <div class="checkbox">
                        <label>
                            <span class="cr"></span>
                            <span class="title2">Attempted</span>
                        </label>
                    </div>
                    </li>
                    
                    <li>
                        <div class="checkbox checkbox2">
                            <label>
                                <!-- <input type="checkbox" value=""> -->
                                <span class="cr"></span>
                                <span class="title2">Not Attempted</span>
                            </label>
                        </div>
                     </li>
                    
                    <li>
                    <div class="checkbox checkbox3">
                        <label>
                            <span class="cr"></span>
                            <span class="title2">To be reviewed</span>
                        </label>
                    </div>
                    </li>
                    
                    <li>
                        <div class="checkbox checkbox4">
                        <label>
                            <!-- <input type="checkbox" value=""> -->
                            <span class="cr"></span>
                            <span class="title2">Not view yet</span>
                        </label>
                        </div>
                    </li>
                    <li>
                    <div class="checkbox checkbox5">
                        <label>
                            <!-- <input type="checkbox" value=""> -->
                            <span class="cr"></span>
                            <span class="title2 mark-review">Attempted and  Mark for review</span>
                        </label>
                    </div>
                    </li>
                    <div class="clearfix"></div>
                </ul>
                
                <div class="btn-group5">
                    <button class="btn btn-feature" data-toggle="modal" data-target="#myModal">Instruction</button>
                    
                    <button onclick="confirmAndFinishExam()" class="btn btn-feature btn-display-revert" href="finish_exam">Finish</button>

                     <button onclick="clearS()" class="btn btn-feature btn-display" href="finish_exam">Clear Selection</button>

                    <input type="hidden" name="hsec" id="hsec" value="0">
                    <input type="hidden" id="total_numberings" value="<?php echo $record_contr; ?>">

                </div>
                
            </div>
        </div>
    </div>
</section>
<div><input type="hidden" name="timespent" id="timespent" value=""></div>            
<script type="text/javascript">
    var start = $.now();
    $("#timespent").val(start);
    
    $('.option_radio').hover(function() {
        $(this).find('.option_radio_span').toggleClass('span_hover');
    });

    function selsec(sec) {
        // alert(sec);
        var sect1   = '#'+'sec_q_'+sec;
        // var sec1 = parseInt(sec)+1;
        var sect2   = '#'+'sec'+sec;
        // alert(sect2);
        $('.btn-group4').hide();
        // $('#mat_q_no').hide();
        // $('#lang_q_no').hide();
        $('.col-lg-7 .btn-feature').css('background','#fff');
        $(sect2).css('background','#0080FF');
        var nid=1;
        $(sect1).show();
        var btn="#qbtn"+''+sec+'_'+nid;
        $('#hsec').val(sec);
        // alert(btn);
        $(btn).click();
    }

    function formatTimeOfDay(millisSinceEpoch) {
      var secondsSinceEpoch = (millisSinceEpoch / 1000) | 0;
      var secondsInDay = ((secondsSinceEpoch % 86400) + 86400) % 86400;
      var seconds = secondsInDay % 60;
      var minutes = ((secondsInDay / 60) | 0) % 60;
      var hours = (secondsInDay / 3600) | 0;
      return hours + (minutes < 10 ? ":0" : ":")
          + minutes + (seconds < 10 ? ":0" : ":")
          + seconds;
    }

    function save_ans(){
        
        var end = $.now();
        
        starts=$("#timespent").val();
        
        var send = formatTimeOfDay(end-starts);
        // alert();
        var ques_number= $('#queno').html();
        // alert(ques_number);
        var ans = "";
        var selected = $("input[type='radio'][name='answer']:checked");
        
        var mult_checked = $("input[type='checkbox'][name='answer']:checked");

        var get_types = $('#custom_hiiden').val();

        if(get_types == 2){

            if (mult_checked.length > 0 ) {

                var favorite = [];

                $("input:checkbox[name=answer]:checked").each(function(){

                     favorite.push($(this).val());

                });
                
                ans = favorite;

                qestion_type_checking = 2;
        
            } else {
                
                alert("Please select an answer.");
                return;
            }


        } else {
        
            if (selected.length > 0 ) {
                ans = selected.val();
                qestion_type_checking = '';
            
            }else{
                
                alert("Please select an answer.");
                return;
            }

        }

        var queno= $('#queno').html()-1;
       
        var hsec=$('#hsec').val();
       
        // var section='';
        var qbtn ='#qbtn'+hsec+'_'+$('#queno').html();
        
        var qbtno=$('#queno').html();
        
        var qbtno=parseInt(qbtno)+1;
       
        var qbtn1='#qbtn'+hsec+'_'+qbtno;
        
        /***********New work by shubham******/

        var get_total_questions = $('#total_numberings').val();

        if(qbtno <= get_total_questions){

            $('#queno').html(qbtno);

        } else {

            $('#queno').html(get_total_questions);
        }


        

        data='queno='+ queno+'&ans='+ ans+'&time='+ send+'&section='+ hsec + '&custom_hiddens=' + qestion_type_checking + '&ques_number=' + ques_number ;
        $.ajax({
            url: "<?=base_url()?>front_end/exam_control/save_answer",
            type: "POST",
            data: data,

            cache: false,
            beforeSend: function(){
                $(qbtn).removeClass();
                $(qbtn).addClass('btn btn-green3');

                // alert(qbtn1);
                if($( qbtn1 ).hasClass( "btn-number" )){
                    $(qbtn1).removeClass();
                    $(qbtn1).addClass('btn btn-red');
                }
                // $(".failure_message_login").css('display','none');
            },
            success: function(result){
                // alert(hsec);
                if ($.trim(result) == "changeSec"){
                        var hsecp=parseInt(hsec)+1;
                        // alert("#sec"+hsecp);
                    if ($("#sec"+hsecp).length == 0){
                        $('#sec0').click();
                    }else{
                        $("#sec"+hsecp).click();
                    }
                }else if ($.trim(result) == "sout"){
                    // alert(result);
                    document.location.href="<?=base_url('login')?>";
                }else{
                    $("#div1").html(result);
                    
                     MathJax.Hub.Queue(["Typeset",MathJax.Hub,"div1"]);

                    var start = $.now();
                    $("#timespent").val(start);
                }


                var ques_type = $("#hid_ques_type").val();
                // alert(ques_type);
                $("#ques_type").html(ques_type);
            }
        });
    }

    function jumpQue(id,no,section=''){
        // alert(id);
        var btid='#'+id;
        if($( btid ).hasClass( "btn-number" )){
            $(btid).removeClass();
            $(btid).addClass('btn btn-red');
        }
        $('#queno').html(no+1);
        data='id='+no+'&section='+ section;
        $.ajax({
            url: "<?=base_url()?>front_end/exam_control/next_que",
            type: "GET",
            data: data,
            //cache: false,
            success: function(result)
            {
              $("#div1").html(result);
              //$('.MJXc-display').css('display','inline-block ! important');
              //MathJax.Hub.Queue(["Typeset",MathJax.Hub,"div1"]);
            }
        });
    }

    function skip(){
        // alert();
        var hsec=$('#hsec').val();
        var nid=parseInt($('#queno').html())+1;
        var btn="#qbtn"+hsec+'_'+nid;
        // alert(btn);
        $(btn).click();
    }

    function markReview(){

        var nid=parseInt($('#queno').html())+1;
        var hsec=$('#hsec').val();
        
        var btid='#qbtn'+hsec+'_'+$('#queno').html();
        var nbid='#qbtn'+hsec+'_'+nid;
        $( nbid ).trigger( "click" );
        // alert(nbid);
        if($( btid ).hasClass( "btn-green3" ) || $( btid ).hasClass( "btn-yellow" )){
            $(btid).removeClass();
            $(btid).addClass('btn btn-yellow');
        }else{
            $(btid).removeClass();
            $(btid).addClass('btn btn-violet');
        }
    }

    var elapsedTime = 0;
    var EXAM_TIME_LEFT = <?=$exam_ideal_time?>;
    if(EXAM_TIME_LEFT==0){
        // alert('no time');
        jQuery('#navbar-collapse').html('');
        // jQuery('#navbar-collapse').css('display','none !important');
        // $('.navbar-time').toggle();
    }
    function updateExamTimer() {
        // alert("in");
        var timeLeft = EXAM_TIME_LEFT - elapsedTime;
        elapsedTime += 1;

        var minutes = Math.floor(timeLeft / 60);
        var seconds = timeLeft % 60;
        var hours = Math.floor(minutes / 60);
        var minutes = minutes % 60;



        if (hours < 10) { hours = '0' + hours; }
        if (minutes < 10) { minutes = '0' + minutes; }
        if (seconds < 10) { seconds = '0' + seconds; }


        if (timeLeft <= 0) {
            alert('Your exam has timed out. You will now be redirected to the exam submission screen.');
            // If we're in ajax mode, submit via ajax - otherwise, redirect to the completion page
            if (jQuery('#exam-ui').length) {
              finishExam(false);
            } else {
                data='fin=1';
                $.ajax({
                    url: "<?=base_url()?>front_end/exam_control/finish_user_exam",
                    type: "GET",
                    data: data,
                    // cache: false,
                    success: function(result){
                        // $("#div1").html(result);
                        document.location.href = "summary_analysis/?test_id=<?=@$this->session->userdata("exam")->test_id;?>";
                    }
                });
              // document.location.href = 'finish_exam';
            }

        } else {
            jQuery('#exam-time-left').html(hours + ':' + minutes + ':' + seconds);
            setTimeout('updateExamTimer()', 1000);
        }
    }   

    $( document ).ready(function() {
        if (jQuery('#exam-time-left').length && EXAM_TIME_LEFT) {
        // alert("ok");
            updateExamTimer();
        }
    });

    function confirmAndFinishExam() {

      if (confirm('Are you sure you wish to finish this exam?')) {
        data='fin=1';
        $.ajax({
            url: "<?=base_url()?>front_end/exam_control/finish_user_exam",
            type: "GET",
            data: data,
            // cache: false,
            success: function(result){
                // $("#div1").html(result);
                document.location.href = "summary_analysis/?test_id=<?=@$this->session->userdata("exam")->test_id;?>";
            }
        });
      }

    }

    function clearS(){

        $("input:radio").removeAttr("checked");
        $("input:checkbox").removeAttr("checked");

    }

    //form tags to omit in NS6+:
    var omitformtags = ['input', 'textarea', 'select'];

    omitformtags = omitformtags.join('|');

    function disableselect(e) {
    if (omitformtags.indexOf(e.target.tagName.toLowerCase()) == -1)
    return false;
    }

    function reEnable() {
    return true;
    }

    if (typeof document.onselectstart != 'undefined')
    document.onselectstart = new Function('return false');
    else {
    document.onmousedown = disableselect;
    document.onmouseup = reEnable;
    }
    </script>

<script>
$("a").click(function(){
    // $("a.active").removeClass("active");
    // var ques_type = $("#single_ques_type").val();
    // // alert(ques_type);
    // if (ques_type == 1) {

    //     $("a").removeClass("checked-option");
    // }
    // $(this).addClass("active");


    var ancar_id = $(this).attr('id');
    var index = $(this).attr('index-data');
    var ques_type = $("#single_ques_type").val();
        // alert(ancar_id);
        // alert(index);
    if (ques_type == 1) {
        $("a.active").removeClass("active");
        $("a").removeClass("checked-option");
        $("#radio_"+index).prop("checked", true);
        $(this).addClass("active");
    }else{
        if ($("#checkbox_"+index).is(':checked')) {
            // alert('mul-checked');
            $("#checkbox_"+index).prop("checked", false);
            $("#"+ancar_id).removeClass("active");
            $("#"+ancar_id).removeClass("checked-option");

        }else{
            // alert('mul-uncheked');
            $("#checkbox_"+index).prop("checked", true);
            $(this).addClass("active");
        }

    }


});
</script>

              <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Instruction</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- kamlesh -->
<script type="text/javascript">
    // function select_radio(id)
    // {
    //     $("#radio_"+id).prop("checked", true);
    //     $("#ancar_"+id).addClass("active");
    // }

    // function select_multi_checkbox(id)
    // {
    //     if ($("#checkbox_"+id).is(':checked')) {
    //         alert("checked");
    //         $("#checkbox_"+id).prop("checked", false);
    //         $("#ancar_"+id).removeClass("active");
            
            
    //     }else{

    //         alert("uncheced");
    //         $("#checkbox_"+id).prop("checked", true);
    //    }
    // }
</script>