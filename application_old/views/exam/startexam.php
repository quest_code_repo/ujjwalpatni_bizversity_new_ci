<section class="section-start"> 

    <div class="container">

        <div class="row ">

            <div class="col-md-12">

                <div class="body-txt">

                    <div class="top">

                        <img src="exam_assets/images/clipboards.png">

                        <span class="common-heading">You are about to start the test- <b><?php echo ucfirst(@$exam->test_name);?></b></span>

                    </div>

                    <div class="bottom">

                        <div class="icons">

                            <div class="time">

                            <img  src="assets/images/questions.png">

                            <p>Number of Questions : <b><?=$total_que?></b></p>

                            </div>

                            <div class="time">

                            <img src="assets/images/time left.png">

                            <p>Maximum Time Limit : <b><?=$exam->ideal_time_duration?> mins</b></p>

                            </div>

                            <div class="trophy">

                            <img src="assets/images/speedometer.png">

                            
                            <p>Total Marks : <b><?=$exam->total_marks?></b></p>

                            </div>

                        </div>

                        <ol>

                            <li>Attempt all the questions.</li>

                            <li>Do not use the browser back button while doing this test. This will auto submit your exam.</li>

                            <li>Do not refresh the page while doing this test. This will auto submit your exam.</li>

                            <li>The timer of the exam will not stop once the exam starts</li>

                            <li>IMPORTANT! Remember to click the 'Finish Exam' link at the bottom of the page once you complete the whole exam.</li>

                            <li>Clicking on Back, Refresh or Finish button before you finish the whole exam will end your exam session.</li>

                        </ol>   

                    </div>

                    <div class="button ">
						
                        <a  href="<?php echo base_url();?>my-courses"  class="btn btn-register">Go Back</a>

                        <a  href="<?php echo site_url('front_end/exam_control/doexam/'.@$exam->test_id); ?>/?sem_id=<?php echo @$sem_id; ?>" class="btn btn-common">Start Test</a>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>



