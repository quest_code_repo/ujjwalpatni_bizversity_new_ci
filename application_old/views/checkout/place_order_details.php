

<section class="section-order">
      <div class="container">
          <div class="row">
            <?php if($this->session->flashdata('confirm_order')!='') {?>
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('confirm_order'); ?>
              </div>
            <?php }elseif($this->session->flashdata('failed_order')!=''){ ?>
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('failed_order'); ?>
              </div>
            <?php } ?>
              <div class="col-md-12">
                  <div class="box-white">
                        <div class="order-success text-center">
                            <img src="<?php echo base_url();?>front_assets/images/icons/checked-place.png">
                            <h4>Booking Confirm</h4>
                        </div>
                        <?php
                        if (!empty($OrderDetails)) {
                          foreach ($OrderDetails as $key => $value) {
                            $details = get_cart_item_details($value->product_id, $value->product_type);
                        ?>
                        <div class="row row-order-success m-0">
                            <div class="col-md-3 col-sm-6">
                                <div class="media media-order">
                                   <a class="pull-left" href="javascript:;">
                                    <div class="order-icon">
                                    <img src="<?php echo base_url();?>front_assets/images/icons/r1.png">
                                    </div>
                                   </a>
                                   
                                   <div class="media-body">
                                      <p>Name</p>
                                      <div class="total-rupee"><i class="" aria-hidden="true"></i> <?php echo $details[0]->item_name; ?></div>
                                   </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="media media-order">
                                   <a class="pull-left" href="javascript:;">
                                    <div class="order-icon">
                                    <img src="<?php echo base_url();?>front_assets/images/icons/r2.png">
                                    </div>
                                   </a>                                   
                                   <div class="media-body">
                                      <p>Price</p>
                                      <div class="fa fa-inr total-rupee"> <?php echo $value->product_price?>/-</div>
                                   </div>                                    
                                </div>
                            </div>
                            <!-- <div class="col-md-3 col-sm-6">
                                <div class="media media-order">
                                   <a class="pull-left" href="javascript:;">
                                    <div class="order-icon">
                                    <img src="<?php //echo base_url();?>front_assets/images/icons/r2.png">
                                    </div>
                                   </a>
                                   
                                   <div class="media-body">
                                      <p>Order Status</p>
                                      <div class="fa fa-inr total-rupee"> <?php //echo $value->order_status?>/-</div>
                                   </div>
                                    
                                </div>
                            </div> -->

                            <?php
                                if($value->product_type == "programe")
                                {
                                    ?>
                            <div class="col-md-3 col-sm-6">
                                <div class="media media-order">
                                   <a class="pull-left" href="javascript:;">
                                    <div class="order-icon">
                                    <img src="<?php echo base_url();?>front_assets/images/icons/r3.png">
                                    </div>
                                   </a>
                                   
                                   <div class="media-body">
                                      <p>Programe Date &amp; Time</p>
                                      <div class="total-rupee"><?php echo date('d-M-y H:i',strtotime($details[0]->program_date)); ?></div>
                                   </div>
                                </div>
                            </div>

                                    <?php
                                }
                            ?>

                           


                           <div class="col-md-3 col-sm-6">
                                <div class="media media-order">
                                   <a class="pull-left" href="javascript:;">
                                    <div class="order-icon">
                                    <img src="<?php echo base_url();?>front_assets/images/icons/r4.png">
                                    </div>
                                   </a>
                                   
                                   <div class="media-body">
                                     <?php
                                      if ($value->product_type == 'programe') { 
                                      ?>
                                      <p>Programe Address</p>
                                      <div class="order-content"><?php echo $details[0]->location ?></div>
                                      <?php }else{ ?>
                                      <p>Delivery Address</p>

                                      <?php
                                          $delivery_data = get_delivery_address($value->delivery_id);

                                          if(!empty($delivery_data))
                                          {
                                              ?>  
                                              <div class="order-content"><?php echo $delivery_data[0]['flat_no'];?> <?php echo $delivery_data[0]['street'];?> <?php echo isset($delivery_data[0]['landmark'])?$delivery_data[0]['landmark']:'';?> <?php echo $delivery_data[0]['city'];?> <?php echo $delivery_data[0]['state'];?></div>
                                              <?php
                                          }

                                          else
                                          {
                                              ?>
                                              <div class="order-content">-</div>
                                              <?php
                                          }
                                      ?>

                                      
                                      <?php }  ?>
                                   </div>
                                    
                                </div>
                            </div>
                        </div>
                        <?php }
                         }else{ ?>
                         <div class="row row-order-success m-0 text-center">
                         Data Not Available
                         </div>
                        <?php } ?>
                        


                        <div class="row row-order-success m-0">
                          <div class="col-sm-12 text-center">
                                <a href="vip"><button class="btn btn-default  btn-register">Continue Shopping</button></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

