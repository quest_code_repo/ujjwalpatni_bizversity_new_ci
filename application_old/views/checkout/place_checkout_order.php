<!DOCTYPE html>
<html lang="en">
<head>
  <title>CCAvenue Payment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
  window.onload = function() {
    var d = new Date().getTime();
    document.getElementById("tid").value = d;
  };
</script>
</head>
<body>

<?php 

if (!empty($deliveryaddress)) {

  if($product_type == "programe")
  {
      $f_name = $deliveryaddress[0]['f_name'];
      $l_name  = $deliveryaddress[0]['l_name'];

      $user_full_name = $f_name. ' '.$l_name;

      $billing_address = $deliveryaddress[0]['address'];
  }

  else
  {
       $user_full_name = $deliveryaddress[0]['user_full_name'];

       if(!empty($deliveryaddress[0]['landmark']))
       {
          $landmark = $deliveryaddress[0]['landmark'];
       }
       else
       {
          $landmark = "";
       }
       $billing_address = $deliveryaddress[0]['flat_no'].' '.$deliveryaddress[0]['street'].' '.$landmark;
  }


}

?>
  <div class="container">
    <h2>CCAvenue Payment Gateway</h2>
    <form id="payment_form" method="POST" name="customerData" action="checkout/ccv_request">
      <div class="form-group">
        <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo isset($user_full_name)?$user_full_name:''; ?>">
      </div>
    
      <div class="form-group">
        <input type="text" class="form-control" id="amount" placeholder="Enter amount" name="amount" value="<?php echo $amount; ?>">
      </div>
 
        <input type="hidden" name="tid" id="tid" readonly />
        
        <input type="hidden" name="merchant_id" value="15089"/>
        
        <input type="hidden" name="order_id" value="<?php echo $order_id;?>"/>
        
        <input type="hidden" name="currency" value="INR"/>

        <input type="hidden" name="billing_name" value="<?php echo isset($user_full_name)?$user_full_name:''; ?>"   />

        <input type="hidden" name="billing_address" value="<?php echo $billing_address;?>"   />

         <input type="hidden" name="billing_city" value="<?php echo isset($deliveryaddress[0]['city'])?$deliveryaddress[0]['city']:''; ?>"   />

          <input type="hidden" name="billing_state" value="<?php echo isset($deliveryaddress[0]['state'])?$deliveryaddress[0]['state']:''; ?>"   />

            <input type="hidden" name="billing_zip" value="<?php echo isset($deliveryaddress[0]['pincode'])?$deliveryaddress[0]['pincode']:''; ?>"   />

            <input type="hidden" name="billing_country" value="India"   />
 <?php
                if($product_type == "programe")
                {
                    ?>
                     <input type="hidden" name="billing_tel" value="<?php echo isset($deliveryaddress[0]['mobile'])?$deliveryaddress[0]['mobile']:''; ?>"   />
                    <?php
                }
                else
                {
                    ?>
                    <input type="hidden" name="billing_tel" value="<?php echo isset($deliveryaddress[0]['mobile_no'])?$deliveryaddress[0]['mobile_no']:''; ?>"   />
                    <?php
                }
            ?>
        
        <input type="hidden" name="redirect_url" value="<?php echo base_url();?>checkout/success_payment"/>
        
        <input type="hidden" name="cancel_url" value="<?php echo base_url();?>checkout/payment_cancel"/>
        
        <input type="hidden" name="language" value="EN"/>
        <button type="submit" class="btn btn-default">Pay Now</button>
        </form>
    </div>
  </body>
</html>


<script type="text/javascript">
$(document).ready(function () {
              //alert("test data");
             $('#payment_form').submit();
                
            });

</script>