<style type="text/css">
    /** blog details css  **/
.section-blog-desc{padding: 50px 0;}
.section-blog-desc .blog-details .blog-img img{width: 100%;}
.section-blog-desc  .heading-black{margin-bottom: 30px;}
.section-blog-desc .blog-details  .blog-desc{margin-top: 30px;}
.blog-desc span{color: #000;}
.section-blog-desc .blog-details  .blog-desc span i{padding-right: 5px;color: #D55220;}
.section-blog-desc .blog-details  .blog-desc .blog-user{padding-left: 30px;}
.section-blog-desc .blog-details  .blog-desc .blog-text{margin-top: 30px;color: #777777;}
.socail-icon{margin-top: 20px;}
.socail-icon .fb img,.socail-icon .tw img{width: 160px;}
.socail-icon .tw img{margin: 0 5px;}
.socail-icon  .google img{margin-right: 5px;}
.socail-icon  .google img,.socail-icon  .linkdin img{width: 42px;}
.section-blog-desc .blog-details .heading-black{margin: 20px 0;font-size: 20px;}
.media-comment{margin-top: 40px;}
.media-comment .media-object {
    width: 80px;
    border-radius: 50%;
    border: 1px solid #acbf76;
    padding: 6px;
}
.media-comment .media-body .media-heading{color: #acbf76;}
.media-comment .media-body .media-heading span{color: #333;}
.media-comment .media-body em{color: #777;font-size: 12px;}
.media-comment .pull-left {padding-right: 20px;}
.comment-form .form-group textarea.form-control{height: 180px !important;}
.comment-form .form-group .form-control{height: 45px;}
.comment-form .btn-green{width: 140px;}
.comment-form .form-group .heading-black{font-size: 20px;}
.main-heading .heading-black{padding-left: 15px;}
.search-box h3{margin-top: 0;}
.search-box .input-group .form-control {
    height: 50px;
    border-radius: 0;
    border-right: none !important;
    border: 1px solid #ddd;
    box-shadow: none;
}
.search-box  .input-group-addon {
    background-color: transparent;
    border-radius: 0;
    border-color: #ddd;
}

.search-box{margin-bottom: 40px;}
.category-list{margin-bottom: 40px;}
.category-list ul{list-style: none;padding: 0;}
.category-list ul li{display: block;}
.category-list ul li a{color: #333;padding: 5px 0;display: block;font-size: 16px;position: relative;}
.category-list ul li a:hover{text-decoration: none;color: #D55220;}
.category-list ul li a span{font-size: 25px;position: absolute;right: 0;top: -5px;}
.archive-list li a{padding: 8px 0 !important;display: block;}
.archive-list li a .badge {
    background-color: #d1d1d1;
    padding: 5px 15px 8px;
    border-radius: 4px;
}
.archive-list li a .badge.active{background-color: #ffae36;}
.category-list .archive-list li a span {
    font-size: 11px;
    position: relative;
    right: 0;
    top: 0;
}
.col-md-3.right-panel h3{font-size: 20px;margin-top: 0;font-family: 'HelveticaLTStd-Bold';color: #150539;}
.right-panel{float: right;}
@media (max-width: 768px){
    .right-panel{float: none !important;}
}


/** blog details css end **/
</style>


<!-- Slider Section html -->
            <section class="section-blog-desc">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">


                            <div class="col-md-3 right-panel">
                                <div class="category-list">
                                    <h3>Select Category</h3>
                                    <ul>
                                        <li>
                                            <a href="">All
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">Lifestyle
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">Health
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">Women
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">Other
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="col-md-9 left-panel">
                                <div class="blog-details">
                                    <div class="blog-img">
                                        <img src="front_assets/images/blogs/blog.jpg">
                                    </div>
                                    <div class="blog-desc">
                                        <span class="cal">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>  02 January 2018
                                        </span>
                                        <span class="blog-user">
                                            <i class="fa fa-user" aria-hidden="true"></i>  Rohit Chouhan
                                        </span>
                                        <div class="blog-text">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <!-- Slider Section html end -->