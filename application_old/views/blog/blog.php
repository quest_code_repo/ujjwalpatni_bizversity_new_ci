<style type="text/css">

</style>

<section class="section-blogs section-blog-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12">
                      <div class="row">
                        <div class="col-sm-8">
                          <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                          <li role="presentation"><a href="#lifeStyle" aria-controls="lifeStyle" role="tab" data-toggle="tab">LifeStyle</a></li>
                          <li role="presentation"><a href="#health" aria-controls="health" role="tab" data-toggle="tab">Health</a></li>
                          <li role="presentation"><a href="#other" aria-controls="other" role="tab" data-toggle="tab">Other</a></li>
                      </ul>
                        </div>
                        <div class="col-sm-4">
                          <div class = "input-group blog">
                            <input type = "search" class = "form-control">
                            <span class = "input-group-addon"><i class="fa fa-search"></i></span>
                          </div>
                        </div>
                      </div>
                       
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="all">
                         <div class="row">
                    
                        <div class="col-sm-4">
                             <div class = "panel panel-default panel-blog">
                                <div class = "panel-heading">
                                    <a href="javascript:void(0);"><img src="front_assets/images/blogs/blog.jpg"/></a>
                                    <div class="panel-heading-overlay">
                                        <a href="blog_deatails.php"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class = "panel-body">
                                    <h4 class="heading-blogg"><a href="javascript:void(0);">TIME FOR MINIMALISM</a></h4>
                                     <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 02 January 2018</p>
                                    <!-- <p class="pull-right"><i class="fa fa-user" aria-hidden="true"></i> Rohit Chouhan</p> -->
                                    <div class="clearfix"></div>
                                    <div class="content-blog">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                             <div class = "panel panel-default panel-blog">
                                <div class = "panel-heading">
                                    <a href="javascript:void(0);"><img src="front_assets/images/blogs/blog.jpg"/></a>
                                    <div class="panel-heading-overlay">
                                        <a href="blog_deatails.php"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class = "panel-body">
                                    <h4 class="heading-blogg"><a href="javascript:void(0);">NEW TRENDS IN WEB DESIGN</a></h4>
                                     <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 02 January 2018</p>
                                    <!-- <p class="pull-right"><i class="fa fa-user" aria-hidden="true"></i> Rohit Chouhan</p> -->
                                    <div class="clearfix"></div>
                                    <div class="content-blog">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                             <div class = "panel panel-default panel-blog">
                                <div class = "panel-heading">
                                    <a href="javascript:void(0);"><img src="front_assets/images/blogs/blog.jpg"/></a>
                                    <div class="panel-heading-overlay">
                                        <a href="blog_deatails.php"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class = "panel-body">
                                    <h4 class="heading-blogg"><a href="javascript:void(0);">THE SOUND OF LIFE</a></h4>
                                    <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 02 January 2018</p>
                                    <!-- <p class="pull-right"><i class="fa fa-user" aria-hidden="true"></i> Rohit Chouhan</p> -->
                                    <div class="clearfix"></div>
                                    <div class="content-blog">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                         <div class="col-sm-4">
                             <div class = "panel panel-default panel-blog">
                                <div class = "panel-heading">
                                    <a href="javascript:void(0);"><img src="front_assets/images/blogs/blog.jpg"/></a>
                                    <div class="panel-heading-overlay">
                                        <a href="blog_deatails.php"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class = "panel-body">
                                    <h4 class="heading-blogg"><a href="javascript:void(0);">TIME FOR MINIMALISM</a></h4>
                                     <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 02 January 2018</p>
                                    <!-- <p class="pull-right"><i class="fa fa-user" aria-hidden="true"></i> Rohit Chouhan</p> -->
                                    <div class="clearfix"></div>
                                    <div class="content-blog">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                             <div class = "panel panel-default panel-blog">
                                <div class = "panel-heading">
                                    <a href="javascript:void(0);"><img src="front_assets/images/blogs/blog.jpg"/></a>
                                    <div class="panel-heading-overlay">
                                        <a href="blog_deatails.php"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class = "panel-body">
                                    <h4 class="heading-blogg"><a href="javascript:void(0);">NEW TRENDS IN WEB DESIGN</a></h4>
                                     <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 02 January 2018</p>
                                    <!-- <p class="pull-right"><i class="fa fa-user" aria-hidden="true"></i> Rohit Chouhan</p> -->
                                    <div class="clearfix"></div>
                                    <div class="content-blog">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                             <div class = "panel panel-default panel-blog">
                                <div class = "panel-heading">
                                    <a href="javascript:void(0);"><img src="front_assets/images/blogs/blog.jpg"/></a>
                                    <div class="panel-heading-overlay">
                                        <a href="blog_deatails.php"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class = "panel-body">
                                    <h4 class="heading-blogg"><a href="javascript:void(0);">THE SOUND OF LIFE</a></h4>
                                    <p class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 02 January 2018</p>
                                    <!-- <p class="pull-right"><i class="fa fa-user" aria-hidden="true"></i> Rohit Chouhan</p> -->
                                    <div class="clearfix"></div>
                                    <div class="content-blog">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                             <ul class = "pagination">
                               <li><a href = "#">&laquo;</a></li>
                               <li><a href = "#">1</a></li>
                               <li><a href = "#">2</a></li>
                               <li><a href = "#">3</a></li>
                               <li><a href = "#">4</a></li>
                               <li><a href = "#">5</a></li>
                               <li><a href = "#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lifeStyle">
                        
                    </div>
                    <div role="tabpanel" class="tab-pane" id="health">
                        
                    </div>
                    <div role="tabpanel" class="tab-pane" id="other">
                        
                    </div>
                </div>
            </div>
             </div>
               
                </div>
            </div>
        </section>