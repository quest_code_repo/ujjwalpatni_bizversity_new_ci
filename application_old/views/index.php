<!--Slider Section html -->
<section class="section-banner">
    <img src="front_assets/images/banners/banner.png" class="img-responsive"/>
    <div class="banner-txt">
        <h3 class="wow slideInLeft">TRANSFORM YOUR<br>LIFE AND BUSINESS</h3>
        <ul class="clearfix">
            <li class="wow fadeIn">TRAINING</li>
            <li class="wow fadeIn">MOTIVATION</li>
            <li class="wow fadeIn">ENTERPRISE  MENTORING</li>
        </ul>
    </div>
</section>
<!-- Slider Section html end -->

<!-- Simple Section html -->

        <?php
                if(!empty($upcomingEvent) && count($upcomingEvent)>0)
                {
                    ?>
          <section class="section-register">
                <div class="color-overlay">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                                <div class="row">
                                    <div class="col-md-12 clearfix">
                                        <ul>

                                            <?php
                                                if($upcomingEvent[0]->category_id == 1)
                                                {
                                                    ?>
                                           <li class="event-logo">
                                                <img src="front_assets/images/other/vip-logo.png">
                                            </li>
                                                    <?php
                                                }
                                                elseif($upcomingEvent[0]->category_id == 2)
                                                {
                                                    ?>
                                            <li class="event-logo">
                                                <img src="front_assets/images/other/excellence-logo.png">
                                            </li>
                                                    <?php
                                                }
                                                elseif($upcomingEvent[0]->category_id == 3)
                                                {
                                                    ?>  
                                            <li class="event-logo">
                                                <img src="front_assets/images/other/pioneer.png">
                                            </li>
                                                    <?php
                                                }
                                                elseif($upcomingEvent[0]->category_id == 4)
                                                {
                                                    ?>
                                            <li class="event-logo">
                                                <img src="front_assets/images/other/keynote.png">
                                            </li>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                            <li class="event-logo">
                                                <img src="front_assets/images/other/vip-logo.png">
                                            </li>
                                                    <?php
                                                }
                                            ?>
                                            <li class="event-name">
                                                <?php echo $upcomingEvent[0]->program_name;?>
                                            </li>
                                            <li class="in">
                                                in
                                            </li>
                                            <li class="place">
                                               <?php echo $upcomingEvent[0]->city_name;?>
                                            </li>
                                            <li class="date">
                                               <?php echo date('M d',strtotime($upcomingEvent[0]->program_date));?>
                                            </li>

                                            <?php
                                                if($upcomingEvent[0]->category_id == 1)
                                                {
                                                    ?>
                                           <li class="text-right">
                                                <a href="<?php echo base_url('vip')?>#Register-sec" class="btn btn-register">REGISTER NOW</a>
                                            </li>
                                                    <?php
                                                }

                                                elseif($upcomingEvent[0]->category_id == 2)
                                                {
                                                    ?>
                                            <li class="text-right">
                                                <a href="<?php echo base_url('excellence-gurukul')?>#Register-sec" class="btn btn-register">REGISTER NOW</a>
                                            </li>
                                                    <?php
                                                }
                                                elseif($upcomingEvent[0]->category_id == 3)
                                                {
                                                    ?>
                                            <li class="text-right">
                                                <a href="<?php echo base_url('power-parenting')?>#Register-sec" class="btn btn-register">REGISTER NOW</a>
                                            </li>
                                                    <?php
                                                }
                                                   elseif($upcomingEvent[0]->category_id == 4)
                                                {
                                                    ?>
                                            <li class="text-right">
                                                <a href="<?php echo base_url('key-note')?>#Register-sec" class="btn btn-register">REGISTER NOW</a>
                                            </li>
                                                    <?php
                                                }
                                            ?>

                                        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                    <?php
                }            
        ?>
            
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-improve">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="common-heading wow fadeInUp">WHAT DO YOU WANT TO IMPROVE TODAY?</h2>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="improve-box wow slideInUp">
                            <img src="front_assets/images/icons/1.png">
                            <h4>PERSONAL HABITS</h4>
                            <ul>
                                <li>Setting realistic goals</li>
                                <li>Staying healthy</li>
                                <li>Being more productive</li>
                                <li>Living stressfree</li>
                            </ul>
                            <a href="mentoring/coming_soon" class="btn btn-common">BUILD</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="improve-box wow slideInUp">
                            <img src="front_assets/images/icons/2.png">
                            <h4>BUSINESS HABITS</h4>
                            <ul>
                                <li>Setting realistic goals</li>
                                <li>Staying healthy</li>
                                <li>Being more productive</li>
                                <li>Living stressfree</li>
                            </ul>
                            <a href="mentoring/coming_soon" class="btn btn-common">ACHIEVE</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="improve-box wow slideInUp">
                            <img src="front_assets/images/icons/3.png">
                            <h4>TIME MANAGEMENT</h4>
                            <ul>
                                <li>Setting realistic goals</li>
                                <li>Staying healthy</li>
                                <li>Being more productive</li>
                                <li>Living stressfree</li>
                            </ul>
                            <a href="mentoring/coming_soon" class="btn btn-common">LEARN</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center mt-30">
                            <a href="mentoring/coming_soon" class="btn btn-big">START YOUR E-LEARNING JOURNEY TODAY</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->
        
<!-- Simple Section html -->
<section class="section-download">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7 xs-p0 clearfix">
                <ul>
                    <li>INTERNATIONAL<br>AUTHOR</li>
                    <li>
                        <div><span>7</span> Books in</div>
                        <div><span>14</span> Languages</div>
                    </li>
                    <li>
                        <div><span>1 million</span> Readers</div>
                        <div>across <span>20</span> Countries</div>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <p>Download Free E-Book on Problems Faced by Family Business</p>
            </div>
            <div class="col-md-2">
                <div class="text-center">
                   <!--  <a href="mentoring/coming_soon" class="btn btn-common wow pulse">DOWNLOAD FREE E-BOOk</a> -->
                   <button type="button" class="btn btn-common wow pulse" data-toggle="modal" data-target="#myModal">DOWNLOAD</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-about">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-3 col-sm-4">
                    <div class="about-img wow zoomIn">
                        <img src="front_assets/images/other/ujjwal-patni.png">
                    </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    <div>
                        <div class="about-top">
                            <img src="front_assets/images/other/about-logo.png">
                        </div>
                        <div class="about-body">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/u5DV5lLksUg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="about-foot wow slideInRight">
                            LED 3 GUINNESS WORLD RECORDS
                            <img src="front_assets/images/other/seal.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-ytube">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="text-uppercase txt clearfix">
                    transform your life for <span class="method1">free</span> right now! watch
                    <a href="" class="pull-right">
                        <img src="front_assets/images/other/show-logo.png" class="show-logo">
                        <a href="https://www.youtube.com/watch?v=9ozfTvhZDd8" target="_blank"><img src="front_assets/images/icons/youtube.png" class="youtube"></a>
                    </a>
                </div>
            </div>
            <div class="col-md-1 col-sm-12">
                <div class="subscriber wow flash"><span><?php echo isset($SocialMedia[0]->social_youtube)?$SocialMedia[0]->social_youtube:''?></span> Subcribers</div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-program">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <h2 class="common-heading wow fadeInUp">SIGNATURE PROGRAMMES</h2>
                </div>
                <div class="col-md-12">
                    <div id="owl-program">
                        <div class="item">
                            <div class="pro-box vip">
                                <div class="white-box">
                                    <img src="front_assets/images/other/vip-logo.png">
                                    <div class="txt">
                                    A TWO DAY AND ONE NIGHT RESIDENTIAL TRAINING PROGRAM
                                    </div>
                                    <div class="text-center mt-20"><a href="<?php echo base_url('vip');?>">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    PERSONAL PRODUCTIVITY AND BUSINESS SUCCESS HABITS
                                </div>
                                <a href="<?php echo base_url('vip');?>" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/excellence-logo.png">
                                    <div class="txt">
                                    A ONE YEAR COMPACT PERSONAL AND BUSINESS COURSE OF MULTIPLE SESSIONS
                                    </div>
                                    <div class="text-center mt-20"><a href="<?php echo base_url('excellence-gurukul');?>">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    PERSONAL, BUSINESS & PUBLIC EXCELLENCE
                                </div>
                                <a href="<?php echo base_url('excellence-gurukul');?>" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                       <!--  <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/keynote.png">
                                    <div class="txt">
                                    EQUALLY FLUENT IN BOTH HINDI AND ENGLISH
                                    </div>
                                    <div class="text-center mt-20"><a href="<?php echo base_url('key-note');?>">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    INVITE DR.UJJWAL PATNI TO KICKSTART YOUR CONFERENCE  OR ANNUAL MEETING
                                </div>
                                <a href="<?php echo base_url('key-note');?>" class="btn btn-common">REGISTER</a>
                            </div>
                        </div> -->
                        <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/pioneer.png">
                                    <div class="txt">
                                    A PROGRAMME DESIGNED FOR COUPLES WITH KIDS
                                    </div>
                                    <div class="text-center mt-20"><a href="<?php echo base_url('power-parenting');?>">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    SPECIAL SKILLS TO RAISE AND NURTURE EXTRAORINARY CHILDREN
                                </div>
                                <a href="<?php echo base_url('power-parenting');?>" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<!-- <section class="section-upcoming">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-lg-3 col-lg-offset-1 ">
                <p class="wow slideInLeft">DISCOVER MORE UPCOMING EVENTS</p>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-5">
                <ul id = "myTab" class = "nav nav-tabs">
                    <li class = "active">
                        <a href = "#march" data-toggle = "tab">march</a>
                    </li>
                    <li>
                        <a href = "#april" data-toggle = "tab">april</a>
                    </li>
                    <li>
                        <a href = "#may" data-toggle = "tab">may</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-3">
                <div id = "myTabContent" class = "tab-content">
                    <div class = "tab-pane fade in active" id = "march">
                        <div class="calendar">
                            <div class="calendar-box">
                                <div class="white-box">
                                    <div id="datepicker1"></div>
                                    <hr>
                                    <div class="event-details clearfix">
                                        <span class="event-date"><div>28</div></span>
                                        <span class="event-logo"><img src="front_assets/images/other/vip-logo.png"></span>
                                        <span class="event-place">kolkata</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class = "tab-pane fade" id = "april">
                        <div class="calendar">
                            <div class="calendar-box">
                                <div class="white-box">
                                    <div id="datepicker2"></div>
                                    <hr>
                                    <div class="event-details clearfix">
                                        <span><div class="event-date">28</div></span>
                                        <span class="event-logo"><img src="front_assets/images/other/vip-logo.png"></span>
                                        <span class="event-place">kolkata</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class = "tab-pane fade" id = "may">
                        <div class="calendar">
                            <div class="calendar-box">
                                <div class="white-box">
                                    <div id="datepicker3"></div>
                                    <hr>
                                    <div class="event-details clearfix">
                                        <span><div class="event-date">28</div></span>
                                        <span class="event-logo"><img src="front_assets/images/other/vip-logo.png"></span>
                                        <span class="event-place">kolkata</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- Simple Section html end -->


    
        <?php
                if(!empty($upcomingEvent) && count($upcomingEvent)>0)
                {
                    ?>
          <section class="section-register">
                <div class="color-overlay">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                                <div class="row">
                                    <div class="col-md-12 clearfix">
                                        <ul>

                                            <?php
                                                if($upcomingEvent[0]->category_id == 1)
                                                {
                                                    ?>
                                           <li class="event-logo">
                                                <img src="front_assets/images/other/vip-logo.png">
                                            </li>
                                                    <?php
                                                }
                                                elseif($upcomingEvent[0]->category_id == 2)
                                                {
                                                    ?>
                                            <li class="event-logo">
                                                <img src="front_assets/images/other/excellence-logo.png">
                                            </li>
                                                    <?php
                                                }
                                                elseif($upcomingEvent[0]->category_id == 3)
                                                {
                                                    ?>  
                                            <li class="event-logo">
                                                <img src="front_assets/images/other/pioneer.png">
                                            </li>
                                                    <?php
                                                }
                                                elseif($upcomingEvent[0]->category_id == 4)
                                                {
                                                    ?>
                                            <li class="event-logo">
                                                <img src="front_assets/images/other/keynote.png">
                                            </li>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                            <li class="event-logo">
                                                <img src="front_assets/images/other/vip-logo.png">
                                            </li>
                                                    <?php
                                                }
                                            ?>
                                            <li class="event-name">
                                                <?php echo $upcomingEvent[0]->program_name;?>
                                            </li>
                                            <li class="in">
                                                in
                                            </li>
                                            <li class="place">
                                               <?php echo $upcomingEvent[0]->city_name;?>
                                            </li>
                                            <li class="date">
                                               <?php echo date('M d',strtotime($upcomingEvent[0]->program_date));?>
                                            </li>

                                            <?php
                                                if($upcomingEvent[0]->category_id == 1)
                                                {
                                                    ?>
                                           <li class="text-right">
                                                <a href="<?php echo base_url('vip')?>#Register-sec" class="btn btn-register">REGISTER NOW</a>
                                            </li>
                                                    <?php
                                                }

                                                elseif($upcomingEvent[0]->category_id == 2)
                                                {
                                                    ?>
                                            <li class="text-right">
                                                <a href="<?php echo base_url('excellence-gurukul')?>#Register-sec" class="btn btn-register">REGISTER NOW</a>
                                            </li>
                                                    <?php
                                                }
                                                elseif($upcomingEvent[0]->category_id == 3)
                                                {
                                                    ?>
                                            <li class="text-right">
                                                <a href="<?php echo base_url('power-parenting')?>#Register-sec" class="btn btn-register">REGISTER NOW</a>
                                            </li>
                                                    <?php
                                                }
                                                   elseif($upcomingEvent[0]->category_id == 4)
                                                {
                                                    ?>
                                            <li class="text-right">
                                                <a href="<?php echo base_url('key-note')?>#Register-sec" class="btn btn-register">REGISTER NOW</a>
                                            </li>
                                                    <?php
                                                }
                                            ?>

                                        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                    <?php
                }            
        ?>


<!-- Simple Section html -->
<section class="section-awards">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <h2 class="common-heading wow fadeInUp">AWARDS & CREDENTIALS</h2>
                </div>
                <div class="col-md-12">
                    <div class="top">
                        <h5>LED 3 GUINNESS WORLD RECORDS</h5>
                        <ul>
                            <li>2005 | Guinness World Record</li>
                            <li>2011 | Guinness World Record.</li>
                            <li>2015 | Guinness World Record.</li>
                        </ul>
                        <img src="front_assets/images/other/seal.png">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="bottom">
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Best corporate trainer of India award, MTC Global</div>
                            <img src="front_assets/images/icons/a1.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Pandit Sunderlal Sharma state Literary award,</div>
                            <img src="front_assets/images/icons/a2.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Speech Guru Award, Rotary International</div>
                            <img src="front_assets/images/icons/a3.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Lifetime achievement award, Indian Jaycees</div>
                            <img src="front_assets/images/icons/a4.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Excellence award, Lions International</div>
                            <img src="front_assets/images/icons/a5.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Young Icon of India award</div>
                            <img src="front_assets/images/icons/a6.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Gururatna national award</div>
                            <img src="front_assets/images/icons/a7.png">
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="bottom">
                        <li class="wow lightSpeedIn">
                            Bright Author Award
                            <img src="front_assets/images/icons/a8.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            'Kamal Patra', the biggest award by Indian Jaycee
                            <img src="front_assets/images/icons/a9.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Bachelor in Dental Surgery (B.D.S.)
                            <img src="front_assets/images/icons/a10.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Master of Arts (M.A.) in Political Science.
                            <img src="front_assets/images/icons/a11.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Specialization in Consumer Protection.
                            <img src="front_assets/images/icons/a12.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Specialization in Human Rights.
                            <img src="front_assets/images/icons/a13.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Worked as a faculty for teacher training and upgradation program in various universitie
                            <img src="front_assets/images/icons/a14.png">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-8 xs-p0">
                    <ul>
                        <li>
                            <div class = "media">
                                <div class = "pull-left">
                                    <img class = "media-object" src = "front_assets/images/icons/facebook.png" alt = "facebook icon">
                                </div>
                                <div class = "media-body text-center" id="counter-fb">
                                  <h4 class="media-heading"><?php echo isset($SocialMedia[0]->social_facebook)?$SocialMedia[0]->social_facebook:''?></h4>
                                  Subscribers
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class = "media">
                                <div class = "pull-left">
                                    <img class = "media-object" src = "front_assets/images/icons/twitter.png" alt = "twitter icon">
                                </div>
                                <div class = "media-body text-center" id="counter-tw">
                                  <h4 class="media-heading"><?php echo isset($SocialMedia[0]->social_twitter)?$SocialMedia[0]->social_twitter:''?></h4>
                                  Followers
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class = "media">
                                <div class = "pull-left">
                                    <img class = "media-object" src = "front_assets/images/icons/youtube-circle.png" alt = "youtube icon">
                                </div>
                                <!-- <p class="counter-wrapper"></p> -->

                                <div class = "media-body text-center">
                                    <h4 class="media-heading"><?php echo isset($SocialMedia[0]->social_youtube)?$SocialMedia[0]->social_youtube:''?></h4>
                                  Subscribers
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 text-center">
                    <div class="row">
                        <div class="col-md-6 pl-0">
                            <a href="mentoring/coming_soon" class="btn btn-register">
                                <div>SUBSCRIBE</div>To ONLINE Programme
                            </a>
                        </div>
                        <div class="col-md-6 pl-0">
                            <a href="<?php echo base_url('vip')?>" class="btn btn-register">
                                <div>SUBSCRIBE</div>To OFFLINE Programme
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-students">
    <div class="color-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-10 col-lg-offset-1 col-sm-12">
                <div class = "media">
                    <div class = "pull-left"><div class="media-object wow slideInLeft">4</div></div>
                    <div class = "media-body">
                      <h4 class = "media-heading">Revolutionary Mantras for students</h4>
                      <h5 class = "sub-txt">which will help them have time for enjoyment, and still be successful.</h5>
                      FEBRUARY 2, 2018
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-1 col-sm-12">
                <div class="text-center">
                    <a href="mentoring/coming_soon" class="btn btn-common">
                        <span>More on</span>BLOG
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-clients">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-10 col-md-offset-1">
                    <h2 class="common-heading wow fadeInUp">CLIENTS</h2>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/client1.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/client2.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/client3.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/client4.png" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/client5.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/client6.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/client7.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/client8.png" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/1.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/2.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/3.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/4.png" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/5.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/6.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/7.png" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
                            <div class="client-box">
                                <img src="front_assets/images/other/8.png" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="product-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 product-block">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <div>
                           <h3 class="wow slideInLeft">Products</h3>
                        </div>
                    </div>
                     <?php
                        if(!empty($product_category) && count($product_category)>0)
                        {
                            foreach($product_category as $proDetails)
                            {
                                  ?>
                                   <div class="col-sm-2 col-xs-6">
                                      <div class="wow zoomIn">
                                        <img src="front_assets/images/icons/product1.png" class="img-responsive">
                                        <h4><?php echo $proDetails->name;?></h4>
                                       </div>    
                                    </div>
                                  <?php
                            }
                            ?>

                                 <div class="col-sm-2 col-xs-12">
                                       <a href="product" button class="btn btn-register">Buy Now</button></a>
                                  </div>

                                  <?php
                        }
                        else
                        {
                            ?>
                            <h4>No Product Category Found</h4>
                            <?php
                        }

                    ?>
                   
                </div>
                <div class="single-line"></div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-12">
                    <h2 class="common-heading wow fadeInUp">APPRECIATIONS</h2>
                </div>
                <div class="col-sm-12">
                    <div id="owl-testimonial">
                        <?php
                        if (!empty($testimonials)) {
                            foreach ($testimonials as $value) {
                        ?>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                    “<?php echo $value->descriptions;?>”
                                    </div>
                                <div class="img-box">
                                    <img src="<?php echo base_url();?>uploads/testinomial_images/<?php echo $value->testi_image;?>">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name"><?php echo $value->title;?></div>
                            </div>
                        </div>
                        <?php } }  ?>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end  -->

<script type="text/javascript">
    $(window).on('load',function(){
        $('#Corporate-Modal').modal('show');
    });
</script>


<script type="text/javascript">
        $(document).ready(function(){
        $('#corporateAdd').click(function(){
        $('#corporate_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {
            $.ajax({
                      type :'POST',
                      url  :'<?php echo base_url("home/add_new_corporate_details")?>',
                      data : $('#corporate_form').serialize(),
                      success:function(resp)
                      {
                        resp = resp.trim();
                          if(resp == "SUCCESS")
                          {
                              $('#corpDiv').show();
                              $('#corpDiv').css('color','green');
                              $('#corpDiv').text('Thank You For Submitting Details');
                              $("#corporate_form")[0].reset();

                               setTimeout(function () {
                         window.location.href= '<?php echo base_url('home/thank_you');?>'; // the redirect goes here

                              },2000);
                          }
                          else if(resp == "CHECK_ERROR")
                          {
                              $('#corpDiv').show();
                              $('#corpDiv').css('color','red');
                              $('#corpDiv').text('Your Email Is Already Registered With Us, Please Login To Continue');
                          }
                          else
                          {
                              $('#corpDiv').show();
                              $('#corpDiv').css('color','red');
                              $('#corpDiv').text('Something Went Wrong');
                          }
                      } 


            });
            
            
         }
        });
        });
    });
</script>