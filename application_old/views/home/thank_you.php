<section class="section-inner" style="margin: 100px 0;">
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div>
                          <h4 class="common-heading">Thank You!!</h4>
                       </div>
                    </div>

                    <div class="col-sm-5 thanks-img">
                     <img src="<?php echo base_url()?>front_assets/images/other/55.png" class="img-responsive">
                 </div>
                    <div class="col-sm-7">
                        <div class=" thanku-content">
                        <!-- Heading -->
                           <p class="title wow slideInRight"> <br>Thank You for Downloading. </p>
                           <p class="title wow slideInRight">“The book has been mailed to the email provided by you.<b> Wish you a happy learning!!!</b>”.</p>
                           <p> </p>
                      </div>
                    </div>
                </div>
               
            </div>
        </section>