
<section class="about-us-section">
	<div class="container">
		<div class="row">
           	  <div class="col-sm-12">
           	  	<h2 class="common-heading wow fadeInUp">About Us</h2>
           	  </div>
           </div>
           <div class="row">
              <div class="col-sm-6">
              	 <div class="wow slideInLeft">
              	 	<img src="<?php echo base_url()?>front_assets/images/other/Patni4885.png" class="img-responsive">
              	 </div>
              </div>
			  <div class="col-sm-6">
				<div class="intro-block wow slideInRight">
					<h4 class="sub-heading">Dr Ujjwal Patni</h4>
					<p>Dr Ujjwal Patni is an International Trainer, Bestselling Author and Motivational Speaker.He is only motivational speaker of India who has Led 3 Guinness World Records and 15 other prestigious awards including Top 10 Thinker 2014 by MTC, Best Corporate Trainer of India, Pundit Sunderlal Sharma RajyaAlankaran and Kamal Patra award makes him a true achiever.</p>
					<p>‘The UjjwalPatni Show’ based on life and business is watched by more than one million Indians every week across 30 countries on Youtube & Whatsapp. More than 1 Million people have subscribed him on different digital platforms and ‘UjjwalPatni’ mobile app.</p>
				</div>
			
			</div>
		</div>
	   <div class="his-lagecy-block">	
		<div class="row">
			<div>
				<h2 class="common-heading wow fadeInUp">His Legacy</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-7"> 
                      <ul class="lagecy-list wow slideInLeft">
                      	<li>He has authored 7 books that are published in 12 Indian languages and 2 foreign languages that have sold more than 1 million copies in 28 countries. He has conducted more than 2000 training programs in more than 100 cities nationally and internationally.</li>
                      	<li>More than 2000+ programs have been organised in 100 plus cities in India and abroad attended by more than 1 million audiences. Excellence Gurukul and VIP are his signature programs for entrepreneurs.</li>
                      	<li>His famous books are Power Thinking, JeetYaaHaar – RahoTaiyaar, SafalVaktaSafalVyakti, Judo Jodo Jeeto, Great Words Win Hearts, etc.</li>
                      	<li>His bestselling DVDs are How owners kill their own business, Business kirakshahetusaatkadam, 30 minatshaandarzindagikeliye, Shradhanjali do safalta lo, Public speaking mastery course, 10 aadateinchuniyeaam se khasbaniye, 10 paise se banimerizindagi, etc.</li>
                      	<li>DrUjjwalPatni’s achievements in different fields are featured on various TV channel, premier newspapers and magazines in India and other countries.</li>
                      </ul>
           	  	  </div>
           	  	  <div class="col-sm-5">
           	  	  	<div class="wow slideInRight">
              	 	<img src="front_assets/images/other/index.png" class="img-responsive">
              	 </div>
           	  	  </div>
           	  </div>
		</div>
	</div>
</section>



<section class="section-awards">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <h2 class="common-heading wow fadeInUp">AWARDS & CREDENTIALS</h2>
                </div>
                <div class="col-md-12">
                    <div class="top">
                        <h5>LED 3 GUINNESS WORLD RECORDS</h5>
                        <ul>
                            <li>2005 | Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit.</li>
                            <li>2011 | Amet, consequat nec ipsum. Maecenas consectetur, odio vel aliquam molestie, nunc diam.</li>
                            <li>2015 | Blandit nisl, a placerat felis massa in ante. Donec quis metus magna. Fusce sed ullamcorpe.</li>
                        </ul>
                        <img src="front_assets/images/other/seal.png">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="bottom">
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Best corporate trainer of India award, MTC Global</div>
                            <img src="front_assets/images/icons/a1.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Pandit Sunderlal Sharma state Literary award,</div>
                            <img src="front_assets/images/icons/a2.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Speech Guru Award, Rotary International</div>
                            <img src="front_assets/images/icons/a3.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Lifetime achievement award, Indian Jaycees</div>
                            <img src="front_assets/images/icons/a4.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Excellence award, Lions International</div>
                            <img src="front_assets/images/icons/a5.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Young Icon of India award</div>
                            <img src="front_assets/images/icons/a6.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            <div class="txt">
                            Gururatna national award</div>
                            <img src="front_assets/images/icons/a7.png">
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="bottom">
                        <li class="wow lightSpeedIn">
                            Bright Author Award
                            <img src="front_assets/images/icons/a8.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            'Kamal Patra', the biggest award by Indian Jaycee
                            <img src="front_assets/images/icons/a9.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Bachelor in Dental Surgery (B.D.S.)
                            <img src="front_assets/images/icons/a10.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Master of Arts (M.A.) in Political Science.
                            <img src="front_assets/images/icons/a11.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Specialization in Consumer Protection.
                            <img src="front_assets/images/icons/a12.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Specialization in Human Rights.
                            <img src="front_assets/images/icons/a13.png">
                        </li>
                        <li class="wow lightSpeedIn">
                            Worked as a faculty for teacher training and upgradation program in various universitie
                            <img src="front_assets/images/icons/a14.png">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-testimonial section-wrk-place">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-12">
                    <div id="owl-works">
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                    “It is an ISO Certified 9001:2008 company engaged in corporate training, consulting, executive coaching and publishing in the segment of personal and organizational excellence. The programs are organized for government and semi government organizations, corporate houses, private companies, educational institutions, Ngo's and individuals. ”
                                    </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/ppp.jpg">
                                </div>
                               <!--  <div class="figure1">
                                    <img src="assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="assets/images/other/figure2.png">
                                </div> -->
                                <div class="name">Patni Personality Plus Pvt Ltd</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                “Medident India Books is a fast growing Indian Book Publishing Company that publishes books on Personality Development, Organizational Excellence, Career Building, Network Marketing, Sales, Motivation, Positive Thinking and Leadership. ”
                                </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/medident.jpg">
                                </div>
                               <!--  <div class="figure1">
                                    <img src="assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="assets/images/other/figure2.png">
                                </div> -->
                                <div class="name">Medident India Books</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                “Medident India came in light after achieving one of the most prestigious government training contracts for capacity building and motivational training of six thousand officers' of government in 2010. The company was preferred amongst some of the topmost international companies due to extremely tailored training design suitable for Indian scenario and a comprehensive follow-up program. ”
                                </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/ppp.jpg">
                                </div>
                                <!-- <div class="figure1">
                                    <img src="assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="assets/images/other/figure2.png">
                                </div> -->
                                <div class="name">Medident India Training</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


