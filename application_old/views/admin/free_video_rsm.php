<div class="centercontent tables">
  <form class="stdform" id="video_form" action="<?php echo base_url(); ?>admin/add_semester/free_video_rsm" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Add Free Ride to RSM</h1>
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper">
      	<!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
            echo $this->session->flashdata('error'); 
          }
         ?>

          <p>
            <label>Video Name<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="name" class="smallinput" id="name" ></span>
              <?php echo form_error('lect_name', '<div class="error_validate">', '</div>'); ?>
          </p>

          <p>
            <label>Video URL<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="video_url" class="smallinput" id="video_url"/></span>
              <?php echo form_error('video_url', '<div class="error_validate">', '</div>'); ?>
          </p>

          <p>
            <label>Duration(in min)<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="duration" class="smallinput" id="duration">
              </span>
              <?php echo form_error('duration', '<div class="error_validate">', '</div>'); ?>
          </p>                      
           
        <p>
          <label>Status</label>
          <select name="status" id="status">
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
          </select>
        </p>

        
                 
      </div><!--contentwrapper-->

      <div class="text-center" style="padding-bottom: 20px;">      
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
        
        <a href="<?php echo base_url();?>admin/add_semester/free_video_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
      </div>
      <div class="clearfix"></div>

		
  	  <!-- <p class="stdformbutton">
        <button class="submit radius2" id="addbtn">Save</button>
      </p> -->

  </form>
     
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('test_description');
  CKEDITOR.replace('pfeatures');
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#video_form").validate({
    rules:{
      
      name: {
        required: true,
      },
       video_url: {
        required: true,
        url: true
      },
      duration: {
        required: true,     
      }

    },
    
    messages:{
      name: "Please Enter Video Name",      
      video_url: "Please Enter Valid URL",
      duration: "Please Enter Video Time Duration"

    },
       
  });
</script>


</body>

</html>
