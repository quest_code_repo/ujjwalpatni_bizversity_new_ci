<div class="centercontent tables">
  <form class="stdform" id="features_form" action="<?php echo base_url(); ?>admin/add_semester/add_features" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Add Features</h1>
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper">
      	<!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
            echo $this->session->flashdata('error'); 
          }
         ?>

          <p>
            <label>Name<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="name" class="smallinput" id="name" ></span>
          </p>
          
          <p>
            <label>Icon<span style="color:red;">*</span></label>
              <span class="field"><input type="file" name="icon" class="smallinput" id="icon">
              </span>
          </p> 

          <p>
            <label>Slider Image<span style="color:red;">*</span></label>
              <span class="field"><input type="file" name="image" class="smallinput" id="image">
              </span>
          </p>                      
          <p>
            <label>Short Description<span style="color:red;">*</span></label>
              <textarea type="text" name="content" class="smallinput" id="content"></textarea>
          </p>
          <p>
            <label>Full Description<span style="color:red;">*</span></label>
              <textarea type="text" name="full_desc" class="smallinput" id="full_desc"></textarea>
          </p>
           
        <p>
          <label>Status</label>
          <select name="status" id="status">
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
          </select>
        </p>

        
                 
      </div><!--contentwrapper-->

      <div class="text-center" style="padding-bottom: 20px;">      
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
        
        <a href="<?php echo base_url();?>admin/add_semester/free_video_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
      </div>
      <div class="clearfix"></div>

		
  	  <!-- <p class="stdformbutton">
        <button class="submit radius2" id="addbtn">Save</button>
      </p> -->

  </form>
     
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('full_desc');
  CKEDITOR.replace('pfeatures');
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#features_form").validate({
    rules:{
      
      name: {
        required: true,
      },
       video_url: {
        required: true,
        url: true
      },
      duration: {
        required: true,     
      }

    },
    
    messages:{
      name: "Please Enter Name",      
      video_url: "Please Enter Valid URL",
      duration: "Please Enter Video Time Duration"

    },
       
  });
</script>


</body>

</html>
