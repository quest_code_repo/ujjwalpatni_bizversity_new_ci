<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_semester/add_faqs" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add FAQ's</h1>
           
            
        </div>
        <div id="contentwrapper" class="contentwrapper">
          <div>
          
            <p>
              <label>Question<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="question" class="smallinput" id="question" required="required" /></span>
            </p>

            <p>
              <label>Answer<span style="color:red;">*</span></label>
              <textarea name="answer" class="smallinput" id="answer" required="required"/></textarea>
            </p>

        
            <p>
              <label>Status<span style="color:red;">*</span></label>
               <select name="active_status" id="active_status">
                 <option value="Active">Active</option>
                 <option value="Inactive">Inactive</option>
              </select>
              </p>

            
          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Blog</button>
       </p>
  </form>
     

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('first_para');
  CKEDITOR.replace('second_para');
  CKEDITOR.replace('third_para');

  </script>

</div>

</body>

</html>