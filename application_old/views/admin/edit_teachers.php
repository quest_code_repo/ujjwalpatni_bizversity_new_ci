<div class="centercontent tables">
  <form id="teach_form" class="stdform" action="<?php echo base_url(); ?>admin/teachers/edit_teacher/<?php echo $teachers[0]['id'];?>" method="post" enctype="multipart/form-data">
    <div class="pageheader notab">
        <h1 class="pagetitle">Add Teacher</h1>
       
        
    </div><!--pageheader-->
        
    <div id="contentwrapper" class="contentwrapper">
                    
      <p>
        <label>Username<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="username" class="smallinput" id="username" value="<?php echo $teachers[0]['username'];?>" /></span>
      </p>
      <!-- <p>
        <label>Password<span style="color:red;">*</span></label>
        <span class="field"><input type="text" name="password" class="smallinput" id="password" value="<?php //echo $teachers[0]['password'];?>"/></span>
      </p> -->
      <p>
        <label>FullName<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="full_name" class="smallinput" id="full_name" value="<?php echo $teachers[0]['full_name'];?>"/></span>
      </p>

      <p>
     	  <label>Select Picture<span style="color:red;"></span></label>
          <?php 
          if(!empty($teachers[0]['image_name'])) { ?>
            <img src="<?php echo base_url();?>uploads/user-image/<?php echo $teachers[0]['image_name'];?>" width="100" height="100" />
            <input type="hidden" name="old_image" id="old_image" value="<?php echo $teachers[0]['image_name'];?>" />
          <?php }else{ echo 'No Image';} ?>
        <input type="file" name="image_name" id="image_name" style="margin-left: 158px;padding-top: 10px;">
      </p>

      <p>
        <label>Mobile<span style="color:red;"></span></label>
        <span class="field"><input type="text" name="mobile" class="smallinput" id="mobile" value="<?php echo $teachers[0]['mobile'];?>"/></span>

      </p>
      
      <p>
        <label>Teachers Status</label>
        <select name="status" id="status">
          <option value="active" <?php if($teachers[0]['status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($teachers[0]['status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
        </select>
      </p>

   
    </div><!--contentwrapper-->

    <div class="text-center" style="padding-bottom: 20px;"> 
        <a href="<?php echo base_url();?>admin/teachers/teacher_details"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
     
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
      </div>
      <div class="clearfix"></div>
	
	  <!-- <p class="stdformbutton">
        <button class="submit radius2" id="addbtn">Save</button>
    </p> -->

				
  </form>  
</div>
	



</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('description');
  CKEDITOR.replace('pfeatures');
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#teach_form").validate({
    rules:{
      
      username: {
        required: true,
      },
      full_name: {
        required: true,
      },
      mobile: {
        required: true,
        number:true,
        minlength: 10,
        maxlength: 12
      }

    },
    
    messages:{
      username: "Please Enter Username",
      full_name: "Please Enter Full Name",
      mobile: "Please Enter Valid Mobile No."
      

    },
       
  });
</script>

</body>

</html>
