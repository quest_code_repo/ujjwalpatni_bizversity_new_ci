<style>
#container {  width:100%;
   -webkit-column-width:300px;-moz-column-width:300px;height:1000px; } 
.innerBoxes {display: inline-block; width: 100%; height: auto;  margin: 5px; padding: 3px; color: #000;}
</style>
<div class="centercontent">
  <div class="pageheader notab">
    <h1 class="pagetitle">Menu Permission Control</h1>
  </div>
  <!--pageheader-->
  <script>
  function loademp_byspoke(sid)
  {
	  jQuery.ajax({url: '<?php echo base_url();?>admin/allotmenu_control/loademp_byspoke/'+sid, success: function(result){
			//alert(result);
        	jQuery("#emp_id").html(result);	
    	}});
	  }
	  
	    function load_menu(eid)
  		{
	  jQuery.ajax({url: '<?php echo base_url();?>admin/allotmenu_control/load_menu/'+eid, success: function(result){
			//alert(result);
        	jQuery("#container").html(result);	
    	}});
	  }
  </script>
  <div id="contentwrapper" class="contentwrapper">
    <div class="subcontent">
      <div class="contenttitle2">
        <h3>Allot Menu Permission </h3>
      </div>
      <!--contenttitle-->
      <?php include("session_msg.php"); ?>
      <form id="form1" class="stdform" action="<?php echo base_url(); ?>admin/allotmenu_control/menu_sub" method="post" enctype="multipart/form-data">
        <p> <span class="field"> 
          Employee List :
          <select name="emp_id" id="emp_id" required onchange="load_menu(this.value);">
            <option value="">-Select-</option>
            <?php  foreach ($emp as $empinfo)
			{  
		 ?>
            <option value="<?php echo $empinfo['id']; ?>"><?php echo $empinfo['name']; ?></option>
            <?php 
		 			 } 
			  ?>
          </select>
          </span> </p>
        <div class="contenttitle2">
          <h3> Menu List </h3>
        </div>
        <!--contenttitle--> &nbsp;&nbsp;&nbsp;
        <button class="stdbtn btn_blue">Allot Menu</button>
        
        
        
        <div id="container">
        <?php  foreach ($admin_menu as $menu)
			{  
		 ?>
        <div class="innerBoxes">
         
          <a href="#" class="tooltip"><strong><?php echo $menu['label']; ?></strong> <span> <img class="callout" src="<?php echo admin_assets; ?>images/callout.gif" /> <strong><?php echo $menu['label']; ?></strong><br />
          <?php echo $menu['description']; ?> </span> </a>
          <br />
          <?php $this->method_call =& get_instance();
           $admin_menu = $this->method_call->load_menu_child($menu['id']);  ?>
           <?php 
		   if(!empty($admin_menu)) {
            foreach ($admin_menu as $menu)
			{  
		 ?>
        
         &nbsp; &nbsp;  <input type="checkbox"  name="menu[]" id="menu[]" value="<?php echo $menu['id']; ?>" /> <?php echo $menu['label']; ?>  <br />
         
        <?php  } }  ?>
          
          </div>
        <?php  } ?>
</div>
        
      </form>
      <div class="contenttitle2">
       <br />
      </div>
      <!--contenttitle--> 
    </div>
    <!--subcontent--> 
    
  </div>
  <!-- centercontent -->
  <style>
a.tooltip {outline:none; }
a.tooltip strong {line-height:30px;}
a.tooltip:hover {text-decoration:none;} 
a.tooltip span {
    z-index:10;display:none; padding:14px 20px;
    margin-top:-30px; margin-left:28px;
    width:300px; line-height:16px;
}
a.tooltip:hover span{
    display:inline; position:absolute; color:#111;
    border:1px solid #DCA; background:#fffAF0;}
.callout {z-index:20;position:absolute;top:30px;border:0;left:-12px;}
    
/*CSS3 extras*/
a.tooltip span
{
    border-radius:4px;
    box-shadow: 5px 5px 8px #CCC;
}
</style>
</div>
<!--bodywrapper-->

</body></html>