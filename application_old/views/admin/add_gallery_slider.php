<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
    <form class="stdform" action="<?php echo base_url(); ?>admin/furnish_dashboard/product_gallery_slider" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Gallery Slider Images</h1>
        </div>
        <!--pageheader-->
        <div id="contentwrapper" class="contentwrapper">
            <?php 
                if($this->session->flashdata('success'))
                {
                 ?>
            <div class="alert alert-success"><?php echo $this->session->flashdata('success');?></div>
            <?php
                }
                else if($this->session->flashdata('error'))                {
                 ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
            <?php 
                }
                
                ?>
            <div>
               <p style="color:red;">Note*:Images above size 1024X768 or above 2mb are restricted. </p>
                <p>
                    <label>Product List<span style="color:red;">*</span></label>
                    <select name="product_id" id="product_id" required="required" onchange="refresh_product()">
                        <option value="">--select product--</option>
                        <?php 
                            if(!empty($product_list))
                            {
                              foreach($product_list as $each_product)
                              {
                              ?>
                                <option value="<?php echo $each_product['product_id'];?>" ><?php echo $each_product['pname'];?></option>
                                <?php
                            } }
                            ?>
                    </select>
                </p>
                <p>
                  <label>Attribute List</label>
                    <select name="attr_id" id="attr_id" onchange="get_attr_value(this.value);">
                      <option>-- Select Attribute --</option>
                      <?php 
                        if(!empty($attribute_list))
                        {
                          foreach($attribute_list as $attr_list)
                          {
                          ?>
                            <option value="<?php echo $attr_list['attr_type_id'];?>" ><?php echo $attr_list['attr_type_name'];?></option>
                            <?php
                        } }
                        ?>
                    </select>
                </p>
                <p id="att_val">
                  
                </p>

                 <p>
                  <label>Color</label>
                  <select name="colorhash" id="colorhash"  >
                    <option value="">--select color--</option>
                    <?php 
                      if(!empty($color_list))
                      { 
                         $j=1;
                        foreach($color_list as $eachcolor)
                        {

                          if($eachcolor['color_hash']!='' && $eachcolor['attr_name']!='')
                          {
                      ?>
                      <option value='<?php echo $eachcolor['color_hash']; ?>' data-class="" data-style="background-color:<?php echo $eachcolor['color_hash']; ?>;">                
                       <?php echo $eachcolor['attr_name'];?>  
                      </option>
                      
                    <?php
                         }
                         $j++;
                      }

                     } ?>
                  </select>

                </p>
                <p>
                    <label>Upload  Images</label>
                    <input type="file" name="product_gallery_image[]" id="product_gallery_image" accept="image/*" value="" multiple required="required"/>
                </p>
                <p>
                    <label>Uploaded Images</label>
                <div id="product_gallery" class="col-sm-12">
                </div>
                </p>
            </div>
        </div>
        <!--contentwrapper-->
        <!-- Trigger the modal with a button -->
        <p class="stdformbutton">
            <button class="submit radius2">Submit</button>
        </p>
    </form>
</div>
  




</div><!--bodywrapper-->

</body>

</html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script type="text/javascript">
  function refresh_product()
  {
    $('#attr_id').prop('selectedIndex', 0);
    $('#attr_val').prop('selectedIndex', 0);

    get_gallery_images(0);
    //$('#product_gallery').hide();
  }

 var image_id;
 function get_gallery_images(image_id)
 {

    
  $('#product_gallery').show();

  var pid = $('#product_id').val();
  var color= $('#colorhash').val();


  if(pid == '')
  {
    return false;
  }
  var data = 'product_id='+pid+'&var_id='+image_id+'&color='+color;
  // alert(data);
    $.ajax({

            url: '<?php echo base_url();?>admin/furnish_dashboard/get_gallery_images',
            data: data,
            type: 'POST',
            success: function(result)
            {
               
              
              $('#product_gallery').html(result);
              
            }
     });
 }

var gallery_id;
 function remove_gallery_image(gallery_id,product_id)
 {
   var attrval=$('#attr_val').val();
    $.ajax({

            url: '<?php echo base_url();?>admin/furnish_dashboard/remove_gallery_image',
            data:{'gallery_id':gallery_id},
            datatype:'json',
            type: 'POST',
            success: function(result)
            {
             
              if(result==1)
              {
                get_gallery_images(attrval);
              }
              else
              {
                alert("Image not removed");
              }

            }
     });
 }

 function get_attr_value(att_id)
 {
  $.ajax({
    url : '<?php echo base_url();?>admin/furnish_dashboard/get_attribute_value',
    data:{'attr_id':att_id},
    type: 'POST',
    success: function(result)
    {
        //alert('fdsf');
        var result_data=JSON.parse(result);
        var result_length=result_data['result'].length;
        var cathtml="<label>Attribute Value</label><select name='attr_val' id='attr_val' onchange='get_gallery_images(this.value);'>";
        cathtml+="<option value=''>--Select Attribute --</option>";
        if(result_length>0)
        {
         for(var i=0;i<result_length;i++)
         {
            if(result_data['result'][i]['attr_name']!='')
            {
               cathtml+="<option value='"+result_data['result'][i]['attr_id']+"'>"+result_data['result'][i]['attr_name']+"</option>";
            }
           
          }
        }
        cathtml+="</select>";
        $('#att_val').html(cathtml);
    }
  });
 }
</script>

<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
 -->  
  <script src="<?php echo base_url();?>/assets/admincss/js/jquery-1.12.4.js"></script>
  <script src="<?php echo base_url();?>/assets/admincss/js/jquery-ui-color.js"></script>
  <style>
   
 
    /* select with custom icons */
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item-wrapper 
     {
      width:366px !important;
      padding: 0.5em 0 0.5em 3em;
    }
    .ui-selectmenu-menu .ui-menu.customicons .ui-menu-item .ui-icon,.ui-state-active .ui-icon {
      height: 24px;
      width: 24px;
      top: 0.1em;
      background-image:none !important;
    }
    

    .ui-icon,
.ui-widget-content .ui-icon 
{
  background-image:none !important;
}
 
    /* select with CSS avatar icons */
    
  </style>
  <script>
  $( function() {

    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
      _renderItem: function( ul, item ) {
        var li = $( "<li>" ),
          wrapper = $( "<div>", { text: item.label } );
 
        if ( item.disabled ) {
          li.addClass( "ui-state-disabled" );
        }
 
        $( "<span>", {
          style: item.element.attr("data-style"),
           "class": "ui-icon" + item.element.attr("data-class")
          
        })
          .appendTo( wrapper );
 
        return li.append( wrapper ).appendTo( ul );
      }
    });
 
    
 
    $("#colorhash").iconselectmenu({

       change: function( event, ui ) {
             
             var selected_value = ui.item.value;
             var attr_value=$('#attr_val').val();
             get_gallery_images(attr_value);
         }

    });

    //fire change event on color hash value 
    // $( "#colorhash" ).selectmenu({
        
    //  });
    //fire change event on color hash value
      
  });
  </script>