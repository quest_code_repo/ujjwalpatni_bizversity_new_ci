 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle"><?php echo $title; ?></h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('message'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('message'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	}
        ?>
        <div id="contentwrapper" class="contentwrapper">
         <a href="<?php echo base_url();?>admin/add_semester/free_video_rsm"><button class="btn-primary">Add Video</button></a>

      
               <?php echo $content; ?>
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>