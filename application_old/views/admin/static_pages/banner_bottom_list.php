 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}


.edit-list {
  list-style: outside none none;
  padding: 0;
}
.edit-list.icons-list.icon-width {
  width: 100px;
}
.edit-list > li {
  display: inline-block;
}
.icons-list > li {
  padding: 0 2px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Data List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php 
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	} else if($this->session->flashdata('update'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('update'); ?>
           </div>
           <?php
          }
        ?>
        <div id="contentwrapper" class="contentwrapper">
            <a href="<?php echo base_url();?>admin/static_page/below_banner_bottom"><button class="btn-primary">ADD</button></a>

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

            <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                            if(!empty($all_data))
                                            {
                                                $j=1;
                                                foreach($all_data as $result)
                                            {?>
                                            <tr>

                                                <td><?php echo $j; ?></td>
                                                <td><?php  echo $result->title; ?></td>
                                                <!-- <td><?php  echo get_cat_name($result->category_id); ?></td> -->
                                               <td><?php  echo $result->description; ?></td>
                                                <!-- <td><?php  echo $result->program_from_time; ?></td> -->
                                                <!-- <td><?php  echo $result->program_to_time; ?></td> -->
                                                <td>

                                                <ul class="edit-list icons-list icon-width">

                                                <?php $status_val = $result->status;

                                                if($status_val == 'active'){ ?>
                                                <li>

                                                <a href="<?php echo base_url();?>admin/static_page/edit_below_banner/?item_id=<?php echo base64_encode($result->id);?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 

                                                </li>

                                                <li><a href="javascript:void(0);" onclick="return delete_category_data(<?php echo $result->id; ?>)"  title="Delete"><i class="fa fa-times" aria-hidden="true" style='color:red;'"></i></a>
                                                </li>

                                                <?php } ?>


                                                <li>

                                                <?php $status_val = $result->status;

                                                if($status_val == 'inactive'){ ?>

                                                <a class="activate_id" href="<?php echo base_url();?>admin/static_page/activate_banner_bottom/?program_id=<?php echo base64_encode($result->id);?>"><img src="<?php echo base_url();?>assets/images/icons/inactive_program.png" width="23px;" height="23px;" title="active"></a></td>

                                                <?php } else { ?>

                                                 <a class="deactivate_id" href="<?php echo base_url();?>admin/static_page/deactivate_banner_bottom/?program_id=<?php echo base64_encode($result->id);?>"><img src="<?php echo base_url();?>assets/images/icons/active_program.png" width="23px;" height="23px;" title="inactive"></a></td>

                                                <?php } ?>

                                                </li>



                                                </td>
                                
                                            </tr>
                                            <?php 
                                                $j++;
                                                  }
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>

<script type="text/javascript">

    function delete_category_data(id)
    {

      if(confirm("Are you Sure! You Want to delete it")){   

        $.ajax({
         type:'POST',
         url:'<?php echo site_url();?>admin/static_page/delete_data_coupons',
         data:{"id":id},
         success: function(data)
         {  
            if(data==1){

                 location.reload();
            }
         }
     });

        return true;
      } else{
        
        return false;
      }

    }
  </script>