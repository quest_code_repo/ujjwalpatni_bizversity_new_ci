<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/static_page/edit_improve_main_title" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Edit</h1>

          <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
          <!--   <p>
              <label>Title<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="title" class="mediuminput" id="title" required="required" placeholder="Speaker Name" onblur="check_title();" value="<?php if(isset($all_data)) { echo $all_data[0]->heading;} ?>"/></span>
            </p>
            <p id="title_exist_error" style="color: red;text-align: center;"></p>

            <div class="service_field_error"><?php echo form_error('title'); ?></div> -->


             <p>
              <label>Heading Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="heading" class="mediuminput" id="heading" required="required" placeholder="Heading Name" onblur="check_title();" value="<?php if(isset($all_data)) { echo $all_data[0]->main_title;} ?>"/></span>
            </p>

            <div class="service_field_error"><?php echo form_error('heading'); ?></div>


             <input type="hidden" name="updated_id" value="<?php echo $this->input->get('item_id');?>">
         

          

          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Update</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('sem_description');

  </script>

</div><!--bodywrapper-->

</body>

</html>


<script type="text/javascript">

// function check_title(){

//   var cat_name = $('#speaker_name').val();
//   var type_val = 3;

//   $.ajax({
//          type:'POST',
//          url:'<?php echo site_url();?>admin/Add_programs_category/check_category_title',
//          data:{"name":cat_name,'type_calls':type_val},
//          success: function(data)
//          {  
//             if(data==1){

//               $('#speaker_name').val('');
//               $('#title_exist_error').html('Speaker Name Already Exist');

//             } else{
//               $('#title_exist_error').html('');

//             }
//          }
//      });
// }
</script>