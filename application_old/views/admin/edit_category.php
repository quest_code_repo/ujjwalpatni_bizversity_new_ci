<style>
  .main_cat li {
    list-style-type: none;
    padding: 0 0 5px;
  }
  .main_cat p {
    margin: 10px 0;
  }
</style>
<div class="centercontent tables">
  <form class="stdform" action="" method="post" enctype="multipart/form-data">
    <div class="pageheader notab">
      <h1 class="pagetitle">Edit Category
      </h1>
    </div>
    <!--pageheader-->
    <div id="contentwrapper" class="contentwrapper">
      <div>
        <p>
          <label>Category Name
            <span style="color:red;">*
            </span>
          </label>
          <span class="field">
            <input type="text" name="category_name" class="mediuminput" id="category_name" required="required" value="<?php echo $category_detail[0]['name']; ?>" />
          </span>
        </p>
        <p>
          <label>Category Description
            <span style="color:red;">
            </span>
          </label>
          <textarea name="category_des" class="mediuminput" id="category_des">
            <?php echo $category_detail[0]['category_description']; ?>
          </textarea>
        </p>
        <!-- <p>
<label>Category Alt Text</label>
<span class="field"><input type="text" name="cat_alt_tag" class="mediuminput" id="cat_alt_tag"  value="<?php echo $category_detail[0]['cat_alt_tag']; ?>" /></span>
</p> -->
        <p>
          <label>Parent Category
          </label>
          <select name="above_category" id="above_category">
            <option value="">--select parent category--
            </option>
            <?php 
              if(!empty($parent_category))
              {
              foreach($parent_category as $each_parent_cat)
              {
              if($category_detail[0]['parent_category']==$each_parent_cat['id'])
              { 
              $select_cat="selected='selected'";
              }
              else
              {
              $select_cat="";
              } 
              ?>
            <option value="<?php echo $each_parent_cat['id'];?>" 
                    <?php echo $select_cat;?> >
            <?php echo $each_parent_cat['name'];?>
            </option>
            <?php
            } 
            }
            ?>
        </select>
      </p>
    <p class="form-group">
      <label>Image
      </label>
      <input type="file" name="cat_image" id="cat_image" accept="image/*" value="" 
             <?php if(empty($category_detail[0]['cat_img'])){ ?> 
      <?php } ?> />
      <?php 
if(!empty($category_detail[0]['cat_img'])) 
{
?>
      <img src="<?php echo base_url();?>uploads/category/<?php echo $category_detail[0]['cat_img'];?>" width="100" height="100" />
      <input type="hidden" name="cat_image1" id="cat_image1" value="<?php echo $category_detail[0]['cat_img'];?>" />
      <?php 
}
?>
    </p>
    <!-- <p>
<?php
if(!empty($variant_cat_data))
{
$i=1;
foreach($variant_cat_data as $each_category)
{
?>
<div class="main_cat">
<h4><?php echo $i;?>.&nbsp;&nbsp;<?php echo $each_category['variant_category_name'];?></h4>
<?php
$sub_category=$each_category['sub_category'];
if(!empty($sub_category))
{
$j=1;
foreach($sub_category as $each_subcategory) 
{
$varient_list=$each_subcategory['variant_list'];
?>
<div class="col-sm-3">
<p><?php echo $j;?>.&nbsp;&nbsp;<?php echo $each_subcategory['variant_subcategory_name'];?></p>
<?php 
?>
<?php 
if(!empty($varient_list)){
?>
<ul>
<?php
foreach($varient_list as $each_varient)
{
if(in_array($each_varient['variant_id'],$category_variant_list))
{
$checked_category="checked='checked'";
}
else
{
$checked_category=""; 
}
?>
<li><input type="checkbox" name="varient_ids[]" id="" value="<?php echo $each_varient['variant_id'];?>" <?php echo $checked_category;?> />&nbsp;&nbsp;<?php echo $each_varient['variant_name'];?></li>
<?php
}
?>
</ul>
<?php
} ?>
</div>
<?php
$j++;
} 
}
?>
</div>
<div class="clearfix" ></div>
<hr/>
<?php
$i++;
}
}
?>
</p>-->
    <p>
      <label>Category Status
        <span style="color:red;">
        </span>
      </label>
      <select name="active_status" id="active_status">
        <option value="Active" 
                <?php if($category_detail[0]['active_status']=='Active'){ echo "selected='selected'";} ?> >Active
        </option>
      <option value="Inactive" 
              <?php if($category_detail[0]['active_status']=='Inactive'){ echo "selected='selected'";} ?>>Inactive
    </option>
  </select>
</p>
</div>
</div>
<!--contentwrapper-->
<!-- Trigger the modal with a button -->
<p class="stdformbutton">
  <button class="submit radius2" id="addbtn">Edit Category
  </button>
</p>
<     
</form>
<!------- Including PHP Script here ------>
</div>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
</script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js">
</script>
<script type="text/javascript">
  CKEDITOR.replace('category_des');
</script>
</div>
<!--bodywrapper-->
</body>
</html>
