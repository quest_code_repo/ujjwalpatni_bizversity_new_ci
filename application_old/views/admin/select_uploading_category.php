<div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle"><?php echo $title; ?></h1>
           
 <?php 
            if($this->session->flashdata('success'))
            {
             ?>
             <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
             </div>
             <?php
            }
            else if($this->session->flashdata('error'))
            {
             ?>
             <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error'); ?>
             </div>
             <?php
            }
        ?>
            
        </div><!--pageheader-->
   
        <div id="contentwrapper" class="contentwrapper">
           
           <h2>Upload Images For Questions (If have)</h2>
        <form method="post" action="<?php echo site_url('admin/bulk_import_quiz/import_question_images');?>" enctype="multipart/form-data">
        

                       
           <label>File to Import<span style="color:red;">*</span></label> <input type="file" required="required" name="userFiles[]" multiple  accept="image/*"><br><br>
           
            <input type="submit"  name="fileSubmit" value="UPLOAD" class="btn btn-primary">
        </form>
               
        </div><!--contentwrapper-->
            
        
  </div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>