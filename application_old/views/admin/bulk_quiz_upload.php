<div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle"><?php echo $title; ?></h1>
           
 <?php 
            if($this->session->flashdata('success'))
            {
             ?>
             <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
             </div>
             <?php
            }
            else if($this->session->flashdata('error'))
            {
             ?>
             <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error'); ?>
             </div>
             <?php
            }
        ?>
            
        </div><!--pageheader-->
   
        <div id="contentwrapper" class="contentwrapper">
           
           <h2>Upload CSV For Import Questions</h2>
        <form method="post" action="<?php echo site_url('admin/bulk_import_quiz/quiz_bulk_submit');?>" enctype="multipart/form-data">
         <p>
                          <label>Test Name<span style="color:red;">*</span></label>
                           <select name="test_name" id="test_name" required="required" >
                             <option value="">--select test name--</option>

                            <?php 
                              if(!empty($test_list))
                              {
                                foreach($test_list as $test_data)
                                {
                              ?>
                              <option value="<?php echo $test_data['test_id'];?>"><?php echo $test_data['test_name'];echo "&nbsp;csv_test_id-".$test_data['test_id']?></option>
                              <?php
                                } 
                              }
                            ?>
                           </select>
                           <?php echo form_error('sem_id','<div class="error_validate">','</div>'); ?>
                          </p>

                        <p>
                          <label>Chapter Name<span style="color:red;"></span></label>
                           <select name="chapter_name" id="chapter_name">
                             <option value="">--select chapter name--</option>

                            <?php 
                              if(!empty($chapter_list))
                              {
                                foreach($chapter_list as $chapter_data)
                                {
                              ?>
                              <option value="<?php echo $chapter_data['ch_id'];?>"><?php echo $chapter_data['ch_name'];echo "&nbsp;csv_test_id-".$chapter_data['ch_id']?></option>
                              <?php
                                } 
                              }
                            ?>
                           </select>
                           <?php echo form_error('sem_id','<div class="error_validate">','</div>'); ?>
                          </p>

                            <p>
                          <label>Topic Name<span style="color:red;"></span></label>
                           <select name="topic_name" id="topic_name">
                             <option value="">--select topic name--</option>

                            <?php 
                              if(!empty($topic_list))
                              {
                                foreach($topic_list as $topic_data)
                                {
                              ?>
                              <option value="<?php echo $topic_data['lect_id'];?>"><?php echo $topic_data['lect_name'];echo "&nbsp;csv_test_id-".$topic_data['lect_id']?></option>
                              <?php
                                } 
                              }
                            ?>
                           </select>
                           <?php echo form_error('sem_id','<div class="error_validate">','</div>'); ?>
                          </p>




                        <!--       <p>
                          <label>Uploading Type<span style="color:red;">*</span></label>
                           <select name="question_type" id="question_type" required="required" >
                             <option value="">select Uploading Type</option>
                           
                              <option value="description">Description</option>
                              <option value="image">Image</option>
                             
                           </select>
                           <?php //echo form_error('sem_id','<div class="error_validate">','</div>'); ?>
                          </p> -->
           <label>File to Import<span style="color:red;">*</span></label> <input type="file" name="userfile" required="required"><br><br>
           
            <input type="submit" name="btn-upload" value="UPLOAD" class="btn btn-primary">
        </form>
               
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>