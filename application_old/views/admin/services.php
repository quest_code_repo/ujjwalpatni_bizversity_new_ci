<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<?php if (isset($message)) { ?>
<div class="notibar msgsuccess" style="color:green;"><p><strong>service added successfully</strong></p></div>
<?php } ?>
<form class="stdform" action="<?php echo base_url(); ?>admin/Addservices/myservices" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">

            <h1 class="pagetitle">Add Services</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
      
               
                      <div>
                        <p>
                          <label>Service Name<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="service_name" class="mediuminput" id="service_name"/></span>
                        </p>
                        <div class="service_field_error"><?php echo form_error('service_name'); ?></div>

                        <p>
                          <label>Service Description<span style="color:red;"></span></label>
                            <span class="field"><textarea name="service_des" class="mediuminput" id="service_des"></textarea></span>
                        </p>
                        
 						<p>
                          <label>Service Amount<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="service_ammount" class="mediuminput" id="service_ammount" />
                          </span>
                        </p>
                        <div class="service_field_error"><?php echo form_error('service_ammount'); ?></div>
                          <p>
                          <label>Choose Your Product<span style="color:red;">*</span></label>
                           <select name="product_selection[]" id="product_selection" required="required" multiple>
                             <option value="">--select parent category--</option>

                            <?php 
                              if(!empty($product_details))
                              {
                                foreach($product_details as $each_parent_cat)
                                {
                              ?>
                              <option value="<?php echo $each_parent_cat['product_id'];?>"><?php echo $each_parent_cat['pname'];?></option>
                              <?php
                                } 
                              }
                            ?>
                           </select>
                          </p>

                          <!-- <p>
                            <label>Category Image<span style="color:red;">*</span></label>
                            <span class="field"><input type="file" name="cat_image" class="mediuminput" id="cat_image" accept="image/*" required="required" /></span>
                          </p> -->

                        
                        
                      
            
                        <p>
                          <label>Service Status<span style="color:red;"></span></label>
                           <select name="active_status" id="active_status" >
                             <option value="Active">Active</option>
                             <option value="Inactive">Inactive</option>
                          </select>
                          </p>
            
                        
                        </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
                 <p class="stdformbutton">
                          <button class="submit radius2" id="addbtn">Add Service</button>
                 </p>
                    
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>