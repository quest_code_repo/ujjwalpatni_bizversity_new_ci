<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">
<style type="text/css">
    .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}
</style>
<div class="centercontent tables">
<form class="stdform" id="product_form" name="product_form" action="<?php echo base_url('admin/add_product/add_new_product')?>" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Product</h1>
           
            
        </div><!--pageheader-->


        <input type="hidden" name="product_type" value="product"   />        
        <div id="contentwrapper" class="contentwrapper">
                      <!-- <div class="one_half"> -->
                      <?php 
          
                      if($this->session->flashdata('error'))
                      {
                        ?>
                        <span style="color:red"><?php echo $this->session->flashdata('error'); ?></span>
                        <?php
                       
                      }
                      if($this->session->flashdata('success'))
                      {
                        ?>
                        <span style="color:green"><?php echo $this->session->flashdata('success'); ?></span>
                        <?php
                        
                      }
                  ?>
                        <p>
                          <label>Product Name<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="pname" class="smallinput" id="pname" data-rule-required="true" data-msg-required="Please Enter Product Name" onblur="return check_product_name(this.value);"/></span>
                            <?php echo form_error('pname', '<div class="error_validate">', '</div>'); ?>
                        </p>
                        <p id="pnameDiv"></p>


                <div class="row">
                        <div class="col-md-2">
                          <label>Product Type<span style="color:red;"  ></span></label>
                        </div>


                        <?php
                            if(!empty($product_category) && count($product_category)>0)
                            {
                                foreach($product_category as $subData)
                                {
                                    ?>
                                      <div class="col-md-2">
                                          <label class="radio-inline"><input type="radio" name="product_category_id" value="<?php echo $subData['id'];?>" onclick="get_related_products();"><?php echo $subData['name'];?></label>
                                       </div>
                                    <?php
                                }
                            }
                            else
                            {
                                echo "No Product Category Found , Please Create The New One";
                            }
                        ?>
                      </div>

              

                     <div id="relatedDiv">   
                        <p>
                          <label>Related Product</label>
                          <select class="test form-control" multiple="multiple" >
                            <option value="">Select Related Product</option>
                          </select>
                        </p>
                      </div>  



                        <p>
                            <label>Product Language<span style="color:red;" >*</span></label>
                            <span class="field">
                                <?php
                                    if(!empty($product_language) && count($product_language)>0)
                                    {
                                      ?>
                                         <select class="test1 form-control" name="pro_languages[]" multiple="multiple" data-rule-required="true" data-msg-required="Please Select Product language">
                                          <?php
                                        foreach($product_language as $reData)
                                        {
                                              ?>
                                             
                                                  <option value="<?php echo $reData['product_language_id'];?>"><?php echo $reData['product_language_name'];?></option>
                                                
                                                <?php
                                            }
                                        ?>
                                       </select>
                                       <?php 
                                    }
                                    else
                                    {
                                        echo "No Product Language Found , Please Create The New One";
                                    }
                                ?>
                            </span>

                        </p>
                        
                   
                         <p>
                          <label>Product Image<span style="color:red;">*</span></label>
                          <input type="file" name="image_url" id="image_url" accept="image/*" value="" data-rule-required="true" data-msg-required="Please Select Product Image" onchange="displayPreview(this.files);"/>
                          <?php echo form_error('image_url', '<div class="error_validate">', '</div>'); ?>
                        </p>


                        <p>
                          <label>Product quantity<span style="color:red;">*</span></label>
                          <span class="field"><input type="text" name="quantity" class="smallinput" id="quantity" data-rule-required="true" data-msg-required="Please Enter Product Quantity"/></span>
                           <?php echo form_error('quantity', '<div class="error_validate">', '</div>'); ?>

                        </p>
                        
                        <p>
                          <label>Product price<span style="color:red;">*</span></label>
                          <span class="field"><input type="text" name="product_price" class="smallinput" id="product_price" data-rule-required="true" data-msg-required="Please Enter Product Price"/></span>
                           <?php echo form_error('product_price', '<div class="error_validate">', '</div>'); ?>

                        </p>

                        <p>
                          <label>Discounted Price<span style="color:red;"></span></label>
                          <span class="field"><input type="text" name="disc_price" class="smallinput" id="disc_price" onblur="myFunction()"/></span>

                        </p>
                      
                        <p>
                          <label>If Featured</label>
                          <select name="is_featured" id="is_featured">
                            <option value="no">No</option>
                            <option value="yes">Yes</option>
                          </select>
                        </p>

                        <!--  <p>
                          <label>Featured Image<span style="color:red;"></span></label>
                          <input type="file" name="featured_image" id="featured_image" accept="image/*" data-rule-required="true" data-msg-required="Please Enter Product Name" onchange=""/>
                          <?php //echo form_error('featured_image', '<div class="error_validate">', '</div>'); ?>
                        </p> -->

                        <p>
                          <label>Product Status</label>
                          <select name="active_inactive" id="active_inactive">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                          </select>
                        </p>

                        <p>
                          <label>Stock Status</label>
                          <select name="stock_status" id="stock_status">
                            <option value="In Stock">In Stock</option>
                            <option value="Out Of Stock">Out Of Stock</option>
                          </select>
                        </p>

                        <p>
                          <label>Short Description<span style="color:red;">*</span></label>
                          <textarea name="short_disc" class="smallinput" id="short_disc" data-rule-required="true" data-msg-required="Please Enter Short Description"/></textarea>
                        </p>

                        <p>
                          <label>Description<span style="color:red;">*</span></label>
                          <textarea name="description" class="smallinput" id="description" data-rule-required="true" data-msg-required="Please Enter Description"/></textarea>
                        </p>


                        <input type="hidden" id="check_id" name="check_id" value=""  />
                       <!--  <p>
                          <label>Product Features<span style="color:red;"></span></label>
                          <textarea name="pfeatures"  id="pfeatures"/></textarea>
                        </p> -->

                       <!--  <div class="form-group">
                          <h4>Properties</h4>
                          <div class="cat_pro col-sm-8">

                          </div>
                          <div class="clearfix"></div>
                         
                        </div> -->

                       <!--  </div> -->
<!--  -->
        <div class="row">
          <div class="col-sm-12">
            <button type="submit"  id="addProduct" >Submit</button>
            <a href="<?php echo base_url('admin/add_product/product_list')?>" button type="button" class="btn btn-danger" >Cancel</button></a>
          </div>
        </div>
                   
        </div><!--contentwrapper-->

  </div><!-- centercontent -->
        
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  



</div><!--bodywrapper-->
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('description');
  CKEDITOR.replace('pfeatures');
</script>

<script type="text/javascript">
        $.noConflict();
        jQuery('#addProduct').click(function(){
        jQuery('#product_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
      jQuery(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {
                $('#addProduct').prop("disabled", true);
                form.submit();
         }


        });
      });
      
</script>

<script>
 var catid;
 function get_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
             for(var i=0;i<result_length;i++)
             {
                cathtml+="<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_category').html(cathtml);
        }
     });
 }
 function get_sub_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
              
             for(var i=0;i<result_length;i++)
             {
                
                cathtml += "<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_sub_category').html(cathtml);
        }
     });
 }
</script>

  <script type="text/javascript">
    function get_related_products()
    {
      
        var type =  jQuery("input[name='product_category_id']:checked").val();

        jQuery.ajax({
               type : 'POST',
               url  : '<?php echo base_url('admin/add_product/get_related_products')?>',
               data : {type:type},
               success:function(resp)
               {
                 jQuery('#relatedDiv').html(resp);
                 jQuery('.test').fSelect();
               }
        });
    }
  </script>

<script>
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>


<script>
 (function($) {
        $(function() {
            $('.test1').fSelect();
        });
    })(jQuery);
</script>


<script type="text/javascript">
    function check_product_name(obj)
    {
        var checkId = $('#check_id').val();
        $.ajax({
                type : 'POST',
                url  : '<?php echo site_url('admin/add_product/check_product_name')?>',
                data : {pname:obj,check_id:checkId},
                success:function(resp)
                {
                  resp = resp.trim();

                  if(resp =="SUCCESS")
                  {
                    var pname = $('#pname').val();

                    $('#pnameDiv').css('color','red');
                    $('#pname').val('');
                    $('#pnameDiv').html('Product ' +pname+ ' Already Exists');
                    return false;
                  }
                  else
                  {
                    return true;
                  }
                }

        });
    }


   function myFunction(){

  var product_actual_price     = $('#product_price').val();
  var product_discounted_price = $('#disc_price').val();

  
  if( parseInt(product_discounted_price) > parseInt(product_actual_price)){

    alert('Alert..Discounted Price Can not more than actual price');
     $('#disc_price').val('');

  }

 }

</script>

</body>

</html>
