<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_product/add_new_product" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Product</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
                      <!-- <div class="one_half"> -->
                      <?php 
                        if($this->session->flashdata('error'))
                        {
                          echo $this->session->flashdata('error'); 
                        }
                       ?>
                        <p>
                          <label>Product Name<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="pname" class="smallinput" id="pname" required="required" /></span>
                            <?php echo form_error('pname', '<div class="error_validate">', '</div>'); ?>
                        </p>

                        <p>
                          <label>Product Categories<span style="color:red;">*</span></label>
                          <select name="product_category" id="product_category" onchange="get_sub_list(this.value);" required="required" class="count_cat"> 
                            <option value="">--select category--</option>
                            <?php 
                             if(!empty($product_category))
                             {
                              foreach($product_category as $each_category)
                              {
                               ?>
                               <option value="<?php echo $each_category['id'];?>" onclick=""><?php echo $each_category['name']; ?></option>
                               <?php
                              }

                             }
                            ?>

                          </select>

                           
                            <p>
                          <label>Category Child<span style="color:red;"></span></label>
                            <span class="field" >
                            <select name="category_child" id="sub_category" onchange="get_sub_sub_list(this.value);" class="count_cat"> 
                                <option value="">--select sub category--</option>
                            </select>
                            </span>
                            
                        </p>

                        <label>Category Child Child<span style="color:red;"></span></label>
                            <span class="field"><select name="category_child_child" id="sub_sub_category" class="count_cat"> 
                                <option value="">--select sub category--</option>
                            </select></span>
                            
                        </p>

                           





                           

                            <?php echo form_error('product_category', '<div class="error_validate">', '</div>'); ?>
                        </p>

                         <p>
                          <label>Product Image<span style="color:red;">*</span></label>
                          <input type="file" name="image_url" id="image_url" accept="image/*" value="" required="required" onchange="displayPreview(this.files);"/>
                          <?php echo form_error('image_url', '<div class="error_validate">', '</div>'); ?>
                        </p>

                        <p>
                          <label>Related Product</label>

                          <select name="related_product[]" id="related_product" multiple>
                            <option value="">--Select Related Product--</option>

                            <?php 
                            if(!empty($related_product))
                            {
                              foreach($related_product as $each_parent_cat)
                              {
                            ?>
                            <option value="<?php echo $each_parent_cat['product_id'];?>"><?php echo $each_parent_cat['pname'];?></option>
                            <?php
                              } 
                            }
                          ?>
                           </select>
                          
                        </p>

                        <p>
                          <label>Product quantity<span style="color:red;">*</span></label>
                          <span class="field"><input type="text" name="quantity" class="smallinput" id="quantity" required="required"/></span>
                           <?php echo form_error('quantity', '<div class="error_validate">', '</div>'); ?>

                        </p>
                        
                        <p>
                          <label>Product price<span style="color:red;">*</span></label>
                          <span class="field"><input type="text" name="product_price" class="smallinput" id="product_price" required="required"/></span>
                           <?php echo form_error('product_price', '<div class="error_validate">', '</div>'); ?>

                        </p>

                        <p>
                          <label>Discounted Price<span style="color:red;"></span></label>
                          <span class="field"><input type="text" name="disc_price" class="smallinput" id="disc_price" required="required"/></span>

                        </p>
                      
                        <p>
                          <label>If Featured</label>
                          <select name="is_featured" id="is_featured">
                            <option value="no">No</option>
                            <option value="yes">Yes</option>
                          </select>
                        </p>

                         <p>
                          <label>Featured Image<span style="color:red;"></span></label>
                          <input type="file" name="featured_image" id="featured_image" accept="image/*" value="" onchange=""/>
                          <?php //echo form_error('featured_image', '<div class="error_validate">', '</div>'); ?>
                        </p>

                        <p>
                          <label>Product Status</label>
                          <select name="active_inactive" id="active_inactive">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                          </select>
                        </p>

                        <p>
                          <label>Stock Status</label>
                          <select name="stock_status" id="stock_status">
                            <option value="In Stock">In Stock</option>
                            <option value="Out Of Stock">Out Of Stock</option>
                          </select>
                        </p>

                        <p>
                          <label>Short Description<span style="color:red;">*</span></label>
                          <textarea name="short_disc" class="smallinput" id="short_disc" required="required"/></textarea>
                        </p>

                        <p>
                          <label>Description<span style="color:red;">*</span></label>
                          <textarea name="description" class="smallinput" id="description" required="required"/></textarea>
                        </p>

                        <p>
                          <label>Product Features<span style="color:red;"></span></label>
                          <textarea name="pfeatures"  id="pfeatures"/></textarea>
                        </p>

                        <div class="form-group">
                          <h4>Properties</h4>
                          <div class="cat_pro col-sm-8">

                          </div>
                          <div class="clearfix"></div>
                         
                        </div>

                        
                        
                       <!--  </div> -->


                       
                   
        </div><!--contentwrapper-->
    
     <p class="stdformbutton">
             <button class="submit radius2" id="addbtn">Add Product</button>
     </p>

  </div><!-- centercontent -->
        
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  



</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('description');
  CKEDITOR.replace('pfeatures');
</script>
<script>
 var catid;
 function get_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
             for(var i=0;i<result_length;i++)
             {
                cathtml+="<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_category').html(cathtml);
        }
     });
 }
 function get_sub_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
              
             for(var i=0;i<result_length;i++)
             {
                
                cathtml += "<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_sub_category').html(cathtml);
        }
     });
 }
</script>

</body>

</html>
