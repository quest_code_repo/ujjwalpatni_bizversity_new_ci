<?php define("admin_assets", base_url()."assets/admincss/"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Student Bazaar |  Admin Panel</title>
<link rel="stylesheet" href="<?php echo admin_assets; ?>css/style.default.css" type="text/css" />
<link rel="stylesheet" href="<?php echo admin_assets; ?>css/bootstrap.min.css" type="text/css" />
<script type="text/javascript" src="<?php echo admin_assets; ?>js/plugins/jquery-1.7.min.js"></script>
<script language="javascript" src="<?php echo admin_assets; ?>js/bootstrap.min.js"></script>
</head>

<body class="loginpage">

	<div class="loginbox">
    	<div class="loginboxinner">
        	
            <div class="logo">
            	<img alt="" src="<?php echo admin_assets; ?>images/logo.png" />
                 
                <h1 style="margin-top:10px;"><span>FORGOT</span> PASSWORD</h1>
            </div><!--logo-->
           
            <br clear="all" />
          
          <?php include("session_msg.php");  ?>
            <form id="login" action="<?php echo base_url(); ?>admin/authentication/forgot_sub" method="post">
            	
                <div class="username">
                	<div class="usernameinner">
                    	<input type="text" name="username" id="username" placeholder="Employee Code" />
                    </div>
                </div>
                  <div class="username">
                	<div class="usernameinner">
                    	<input type="text" name="email" id="email" placeholder="Email" />
                    </div>
                </div>
               
                
                <button type="submit">Forgot Password</button>
                
                <div class="keep"><a href="<?php echo base_url(); ?>admin/authentication">Login</a></div>
            
            </form>
            
        </div><!--loginboxinner-->
    </div><!--loginbox-->


</body>

</html>
