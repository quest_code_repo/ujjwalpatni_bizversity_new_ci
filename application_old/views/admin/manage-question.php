<style type="text/css">
    
   .table-question thead th {
  color: #000 !important;
  font-size: 12px;
  font-weight: bold !important;
}
.contentwrapper {
  padding: 20px 0;
}
</style>

<div class="centercontent">
    
    <div class="pageheader notab">
        <h1 class="pagetitle"><?php echo $title; ?></h1>
    </div>
    <?php 
  if($this->session->flashdata('success'))
  {
   ?>
   <div class="alert alert-success">
    <?php echo $this->session->flashdata('success'); ?>
   </div>
   <script type="text/javascript">
      $(document).ready(function() {
        $("#question").trigger('click');
      }
   </script>
   <?php
  }
  else if($this->session->flashdata('error'))
  {
   ?>
   <div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
   </div>
   <?php
  }
    ?>
    <div id="contentwrapper" class="contentwrapper">
        <h4>Test :: <?php echo $test_detail[0]['test_name']; ?></h4>
        
        <section class="section-products" >
            <div class="container" style="width: 1050px;">
                <div class="panel panel-default panel-product-block">
                  <div class="panel-body">
                    <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#question-view">Question View</a></li>
                      <li><a data-toggle="tab" href="#question" id="questionss">Add Question</a></li>
                      <li style="float: right; background-color: orange;"><a href="<?=base_url();?>admin/add_semester/test_list" style="color: #fff; font-weight: bold;" >Finish Adding Question.</a></li>
                      
                    </ul>

                    <div class="tab-content">
                      <div id="question-view" class="tab-pane fade in active">
                        <div id="contentwrapper" class="contentwrapper table-responsive">
                        <table class="table table-bordered table-question ">
                           <!-- <caption>Bordered Table Layout</caption> -->
                           
                           <thead>
                              <tr>
                                <?php
                                 if (!empty($question_detail[0]['chapter_id'])) {
                                    ?> 
                                <!--   <th>Chapter Name</th> -->
                                <?php } ?>
                                <?php
                                 if (!empty($question_detail[0]['lecture_id'])) {
                                    ?>
                                  <!-- <th>Topic Name</th> -->
                                 <?php } ?>
                                 <th>Question</th>
                                 <th>Answer 1</th>
                                 <th>Answer 2</th>
                                 <th>Answer 3</th>
                                 <th>Answer 4</th>
                                 <th>Right Ans</th>
                                 <!-- <th>Explaination</th> -->
                                 <th>Marks</th>
                                 <!-- <th>Negative Marks</th> -->
                                 <!-- <th>Semester Wise</th> -->
                                 <!-- <th>Attachment</th> -->
                                 <!-- <th>Difficulty Level</th> -->
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           
                           <tbody>
                            <?php 
                                if(!empty($question_detail)){
                                foreach ($question_detail as $question) {
                            ?>
                              <tr>
                                <!-- <?php 
                                 if (!empty($question['chapter_id'])) { ?>
                                  <td><?php echo $question['ch_name']; ?></td>
                                <?php } ?>
                                <?php 
                                 if (!empty($question['lecture_id'])) { ?>
                                  <td><?php echo $question['lect_name']; ?></td>
                                <?php } ?> -->
                                 <td><?php echo $question['question_desc']; ?></td>
                                 <td><?php echo $question['ans_1']; ?></td>
                                 <td><?php echo $question['ans_2']; ?></td>
                                 <td><?php echo $question['ans_3']; ?></td>
                                 <td><?php echo $question['ans_4']; ?></td>
                                 <td><?php echo $question['ques_right_ans']; ?></td>
                                 <!-- <td><?php echo $question['explain_right_answer']; ?></td> -->
                                 <td><?php echo $question['que_marks']; ?></td>
                                 <!-- <td><?php echo $question['ques_negative_marks']; ?></td> -->
                                 <!-- <td><?php echo $question['ques_concept_wise']; ?></td> -->
                                 <!-- <td><?php 
                                  if(!empty($question['attachment_img'])) 
                                  {
                                  ?>
                                  <img src="<?php echo base_url();?>uploads/Question/<?php echo $question['attachment_img'];?>" width="100" height="100" />
                                   <?php } ?>
                                  </td> -->
                                 <!-- <td><?php echo $question['ques_difficulty_level']; ?></td> -->
                                 <td><?php echo $question['ques_status']; ?></td>
                                 <td>
                                    <a href="<?php echo base_url(); ?>admin/add_semester/edit_question/<?php echo $question['ques_id'] ?>/?lect_id=<?php 
                                  echo $test_detail[0]['lecture_id']; ?>&ch_id=<?php 
                                  echo $test_detail[0]['chapter_id']; ?>&sem_id=<?php
                                  echo $test_detail[0]['sem_id'];   ?>&test_id=<?php echo $test_detail[0]['test_id']?>" onClick="javascript:return confirm('Are you sure to Update it?')"><i class="fa fa-pencil" aria-hidden="true"></i>
                                  </a>                                    
                                </td>
                                 
                              </tr>
                              
                              <?php }} ?>
                           </tbody>
                            
                        </table>
                      
                        </div>
                      </div>

                      <div id="question" class="tab-pane fade">
                        
                        <div id="contentwrapper" class="contentwrapper">
                          <?php 
                            if($this->session->flashdata('error'))
                            {
                              echo $this->session->flashdata('error'); 
                            }
                           ?>

                        <form class="stdform addquestions" id="question-form" action="<?php echo base_url(); ?>admin/add_semester/add_question" method="post" enctype="multipart/form-data">

                        <input type="hidden" name="test_id" value="<?php echo $test_detail[0]['test_id']; ?>">
                        <input type="hidden" name="lect_id" value="<?php echo $lect_id; ?>">
                        <input type="hidden" name="ch_id" value="<?php echo $ch_id; ?>">
                        <input type="hidden" name="sem_id" value="<?php echo $sem_id; ?>">
                            <p>
                              <label>Question<span style="color:red;">*</span></label>
                                 <textarea name="question_desc" class="smallinput" id="question_desc" rows="5" cols="30" dir="ltr" /></textarea>
                            </p>
                            <p>
                              <label>Answer 1<span style="color:red;">*</span></label>
                                 <textarea name="ans_1" class="smallinput" id="ans_1"/></textarea>
                            </p>
                            <p>
                              <label>Answer 2<span style="color:red;">*</span></label>
                                 <textarea name="ans_2" class="smallinput" id="ans_2"/></textarea>
                            </p>
                            <p>
                              <label>Answer 3<span style="color:red;">*</span></label>
                                 <textarea name="ans_3" class="smallinput" id="ans_3"/></textarea>
                            </p>
                            <p>
                              <label>Answer 4<span style="color:red;">*</span></label>
                                 <textarea name="ans_4" class="smallinput" id="ans_4"/></textarea>
                            </p>
                            
                            <p>
                              <label>Right Answer<span style="color:red;">*</span></label>
                              <select name="ques_right_ans" id="ques_right_ans">
                                <option value="">Select Right Answer</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                              </select>
                            </p>
                            <!-- <p>
                              <label>Right Answer Explanation<span style="color:red;">*</span></label>
                                 <textarea name="explain_right" class="smallinput" id="ans_4"/></textarea>
                            </p> -->
                            <!-- <div style="width: 100%;"> -->
                            <!-- <div style="float: left; width: 33%;"> -->
                            <p>
                              <label>Marks<span style="color:red;">*</span></label>
                                  <input type="text" name="que_marks" class="smallinput" id="que_marks" required="required" style="width: 80%;">
                                <span class="field">
                                </span>
                                <?php echo form_error('que_marks', '<div class="error_validate">', '</div>'); ?>
                            </p>
                            <!-- 
                            </div>
                            <div style="width: 33%; float: left;">  -->
                            <p>
                              <label>Status</label>
                              <select name="ques_status" id="ques_status">
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                              </select>
                              <span class="field">
                              </span>
                            </p>
                            <!-- </div> -->
                            <!-- </div> -->

                            </div>
                            <div style="clear:both"></div>

                        
                           <div class="text-center" style="padding-bottom: 20px;"> 
     
     
                          <button type="submit" class="btn btn-orange" id="addbtn">Save & next</button>
                          <a href="<?php echo base_url();?>admin/add_semester/manage_question/<?php echo $test_detail[0]['test_id']; ?>/?lect_id=<?php echo $lect_id;?>&ch_id=<?php echo $ch_id;?>&sem_id=<?php echo $sem_id;?>"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
                        </div>
                        <div class="clearfix"></div>      
                        <!-- <p class="stdformbutton">
                            <button class="submit radius2" id="addbtn">Save</button>
                        </p> -->

                        </form>
                        </div>
     
                      </div>
                     
                    </div>
                  </div>

                </div>
            </div>
        </section>
    </div><!--contentwrapper-->
            
        
</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
<script src="<?php echo base_url() ?>assets/tinymce/js/tinymce/tinymce.min.js?apiKey=x7mmfrsinal4f5v31e7q5iiwvluk8b2nof1eslzgr05i98bi"></script>
<script>

tinymce.init({
    selector: "textarea",
    themes: "modern",
    height : 50,
    plugins: "code",
  
});

<?php 
  // if(true)
  if($this->session->flashdata('success'))
  {
   ?>
     $(document).ready(function() {
        $("#questionss").trigger('click');
        // $(document).on("click", "#questionss", function() {
        //   alert("Test");
        // });
      });
   
   <?php } ?>
</script>


<!-- <script type="text/javascript">
  CKEDITOR.replace('question_desc');
  CKEDITOR.replace('pfeatures');
</script> -->

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#question-form").validate({
    rules:{
      
       question_desc: {
        required: true,
        minlength: 5,
        maxlength: 30,
        lettersonly: true
      },
      ans_1: {
        required: true,
      },
      ans_2: {
        required: true,
      },
      ans_3: {
        required: true,
      },
      ans_4: {
        required: true,
      },
      ques_right_ans: {
        required: true,
      },
      que_marks: {
        required: true,
        number:true
      },
      ques_negative_marks: {
        required: true,
        number:true
      },
      ques_concept_wise: {
        required: true,
      },
      ques_difficulty_level: {
        required: true,
      }

    },
    
    messages:{
      question_desc: "Please Enter Your Question",
      ans_1: "Please Enter Answer 1",
      ans_2: "Please Enter Answer 2",
      ans_3: "Please Enter Answer 3",
      ans_4: "Please Enter Answer 4",
      ques_right_ans: "Please Enter Right Answer",
      explain_right: "Please Enter Right Answer",
      que_marks: "Please Enter Marks in Number",
      ques_negative_marks: "Please Enter Negative Marks number",
      ques_concept_wise: "Please Enter Concept",
      ques_difficulty_level: "Please Select Difficulty Level"      

    },
       
  });
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/plugins/select2/select2.full.min.js"></script> 

<script type="text/javascript">
  $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
      });

</script>

</body>

</html>