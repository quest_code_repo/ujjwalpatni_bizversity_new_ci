 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}
.edit-list {
  list-style: outside none none;
  padding: 0;
}
.edit-list.icons-list.icon-width {
  width: 100px;
}
.edit-list > li {
  display: inline-block;
}
.icons-list > li {
  padding: 0 2px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Brochure List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	} else if($this->session->flashdata('update'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('update'); ?>
           </div>
           <?php
          }
        ?>
        <div id="contentwrapper" class="contentwrapper">
            <a href="<?php echo base_url();?>admin/cms_control/add_mentoring_proposal"><button class="btn-primary">ADD Mentoring Proposal</button></a>

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

             <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
              <thead>
                  <tr>
                      <th>S.No.</th>
                      <th>Proposal Name</th>
                      <th>Mentoring Category</th>
                      <th>Proposal Url</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                 <?php 
                  if(!empty($all_data))
                  {
                      $j=1;
                      foreach($all_data as $result)
                  {?>
                  <tr>

                      <td><?php echo $j; ?></td>
                      <td><?php  echo $result['mentoring_proposal_name']; ?></td>

                      <?php
                          $str = str_replace('-',' ',$result['mentoring_proposal_for']);
                      ?>

                      <td><?php  echo $str; ?></td>
                      <td><a href="<?php echo base_url('uploads/mentoring_proposal/'.$result['mentoring_proposal_url'].'')?>" target="_blank">View</a></td>
                      <td><a href="<?php echo base_url('admin/cms_control/delete_mentoring_proposal/'.$result['mentoring_proposal_id'].'')?>" onclick="return confirm_delete();">Delete</a></td>
                  </tr>
                  <?php 
                      $j++;
                        }
                      }
                  ?>
                  
              </tbody>
          </table> 
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->


<script type="text/javascript">

  function confirm_delete()
  {
         var r = confirm("Are You Sure Want To Delete!");
          if (r == true) {
              return true;
          } else {
              return false;
          }
  }
 
</script>

</body>

</html>


