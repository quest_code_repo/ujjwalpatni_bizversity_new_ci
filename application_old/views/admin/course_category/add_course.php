<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">
<style type="text/css">
    .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}

  </style>

<div class="centercontent tables">
<form id="course_form" class="stdform" name="course_form" action="javascript:void(0);" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Course</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
                    	<!-- <div class="one_half"> -->
                      <?php 
                        if($this->session->flashdata('error'))
                        {
                          echo $this->session->flashdata('error'); 
                        }
                       ?>
                        <p>
                          <label>Course Name<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" id="course_name" name="course_name" class="smallinput"  data-rule-required="true" data-msg-required="Please Enter Course Name" required /></span>
                           
                        </p>

                      <!--   <p>
                          <label>Course Category<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" class="smallinput" id="course_category" name="course_category"  /></span>
                        </p> -->

                         <p>
                            <label>Course Category<span style="color:red;">*</span></label>
                            <span class="field">
                              <select class="test form-control" multiple="multiple">
                                 <option value="">Select Category</option>
                                 <?php
                                  if(!empty($course_category) && count($course_category)>0)
                                  {
                                    foreach($course_category as $catDetails)
                                    {
                                      ?>
                                      <option value="<?php echo $catDetails->course_category_id;?>"><?php echo $catDetails->course_category_name;?></option>
                                      <?php
                                    }
                                  }

                                 ?>
                            </select>
                            </span>
                          
                          </p>

                         <p>
                          <label>Course Cost<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" class="smallinput" id="course_cost" name="course_cost"  required /></span>
                        </p>
                        <p>
                          <label>Sneak Preview Video<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" class="smallinput" id="video_url" name="video_url"  /></span>
                        </p>

                       
                          <p>
                            <label>Related Courses<span style="color:red;">*</span></label>
                            <span class="field">
                              <select class="test form-control" multiple="multiple">
                                 <option value="">Select</option>
                                  <option value="1">Select 1</option>
                                  <option value="2">Select 2</option>
                                  <option value="3">Select 3</option>
                                  <option value="4">Select 4</option>
                                  <option value="5">Select 5</option>
                                  <option value="6">Select 6</option>
                            </select>
                            </span>
                          
                          </p>
                        <p>
                          <label>Video Hours<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" class="smallinput" id="video_hours" name="video_hours" required /></span>
                        </p>
                        <p>
                          <label>Test<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" class="smallinput" id="test" name="test" required /></span>
                        </p>
                        <p>
                          <label>Resources<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" class="smallinput" id="resources" name="resources" required /></span>
                        </p>
                        
                         <p>
                          <label>Course Duration(in hours)<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="course_duration" class="smallinput" id="course_duration" required >
                            </span>
                           
                        </p>
                          <div class="checkbox clearfix">
                              <label class="access-lable"><input type="checkbox" class="smallinput">Life time access</label>
                          </div>
                         <p>
                          <label>What will I learn<span style="color:red;">*</span></label>
                          <textarea class="smallinput" id="course_learn" name="course_learn" required /></textarea>
                        </p>
                        <p>
                          <label>Description<span style="color:red;">*</span></label>
                          <textarea name="course_description" class="smallinput" id="course_description" required/></textarea>
                        </p>
                         
                       
                   
        </div><!--contentwrapper-->
		
  	  <div class="text-center" style="padding-bottom: 20px;"> 
       <!--  <a href="<?php echo base_url();?>admin/course_category/course_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" >  -->
     
     
        <button type="submit" class="btn btn-orange" id="addCourse">Save</button>
      </div>
      <div class="clearfix"></div>

	</div><!-- centercontent -->
				
  </form>
     
			<!------- Including PHP Script here ------>

		</div>
	



</div><!--bodywrapper-->


 <script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('course_description');
  CKEDITOR.replace('pfeatures');
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>
<script type="text/javascript">
        $.noConflict();
        jQuery('#addCourse').click(function(){
        jQuery('#course_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
      jQuery(element).removeClass(errorClass);
        }
        });
      });
      
</script>

<script>
 var catid;
 function get_sub_list(catid)
 {
 	 $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
  				    var result_data=JSON.parse(result);
  				    var result_length=result_data.length;
  				    var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
				    if(result_length>0)
				    {
					   for(var i=0;i<result_length;i++)
					   {
						    cathtml+="<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
						  }
				    }
				  else
				  {
					cathtml+="<option value=''>--select variant--</option>";
				  }
				  $('#sub_category').html(cathtml);
        }
     });
 }
 function get_sub_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
              
             for(var i=0;i<result_length;i++)
             {
                
                cathtml += "<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_sub_category').html(cathtml);
        }
     });
 }
</script>

<script>
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>




</body>

</html>
