<div class="centercontent tables">
  <form class="stdform" action="<?php echo base_url('admin/add_product/edit_product_ebook')?>" method="post" id="ebook_form" enctype="multipart/form-data">
    <div class="pageheader notab">
      <h1 class="pagetitle">Edit e-Book Product</h1>
    </div><!--pageheader-->
<input type="hidden" name="product_id" value="<?php echo $product_detail[0]['product_id'];?>"  />
    <div id="contentwrapper" class="contentwrapper">
      <!-- <div class="one_half"> -->
      <?php 
          
        if($this->session->flashdata('error'))
        {
          ?>
          <span style="color:red"><?php echo $this->session->flashdata('error'); ?></span>
          <?php
         
        }
        if($this->session->flashdata('success'))
        {
          ?>
          <span style="color:green"><?php echo $this->session->flashdata('success'); ?></span>
          <?php
          
        }
       ?>
        <p>
          <label>E-Book Name<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="pname" class="smallinput" id="pname" data-rule-required="true" data-msg-required="Please Enter Course Name" value="<?php echo $product_detail[0]['pname'];?>" onblur="return check_ebook(this.value);"/></span>
            <?php echo form_error('pname', '<div class="error_validate">', '</div>'); ?>
        </p>

        <p id="pnameDiv"></p>

        <p>
          <label>Categories<span style="color:red;">*</span></label>
          <select name="product_category" id="product_category"  data-rule-required="true" data-msg-required="Please Select Category" class="count_cat"> 
            <option value="">--select category--</option>
            <?php 
             if(!empty($product_category))
             {
              foreach($product_category as $each_category)
              {
               ?>
               <option value="<?php echo $each_category['id'];?>" <?= ($each_category['id']==$product_detail[0]['product_category_id'])? "selected='selected'" : '' ?>><?php echo $each_category['name']; ?></option>
               <?php
              }
             }
            ?>
          </select>
        </p>
        <p><?php echo form_error('product_category', '<div class="error_validate">', '</div>'); ?></p>

        <input type="hidden" name="book_file1" value="<?php echo $product_detail[0]['product_alt_tag'];?>"  />

        <input type="hidden" name="image_url1" value="<?php echo $product_detail[0]['image_url'];?>"  />

        <p>
          <label>Book File<span style="color:red;"></span></label>
          <input type="file" name="book_file" id="book_file"   />
          <?php
          if($this->session->flashdata('book_file_error'))
          {
            echo $this->session->flashdata('book_file_error'); 
          }?>
        </p>

        <p>
          <label>Product Image<span style="color:red;"></span></label>
          <input type="file" name="image_url" id="image_url" accept="image/*" />
          <?php echo form_error('image_url', '<div class="error_validate">', '</div>'); ?>
        </p>

        <p>
          <label>Product price<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="product_price" class="smallinput" id="product_price" data-rule-required="true" data-msg-required="Please Enter Product Price" value="<?php echo $product_detail[0]['product_price']?>"/></span>
           <?php echo form_error('product_price', '<div class="error_validate">', '</div>'); ?>

        </p>

        <p>
          <label>Discounted Price<span style="color:red;"></span></label>
          <span class="field"><input type="text" name="disc_price" class="smallinput" id="disc_price" value="<?php echo isset($product_detail[0]['disc_price'])?$product_detail[0]['disc_price']:''?>" /></span>

        </p>
      
        <p>
          <label>If Featured</label>
          <select name="is_featured" id="is_featured">
             <option value="yes" <?php if($product_detail[0]['is_featured']=='yes'){ echo $selected_featured="selected='selected'";}?>>Yes</option>
                          <option value="no" <?php if($product_detail[0]['is_featured']=='no'){ echo $selected_featured="selected='selected'";}?>>No</option>
          </select>
        </p>

       <!--  <p>
          <label>Featured Image<span style="color:red;"></span></label>
          <input type="file" name="featured_image" id="featured_image" accept="image/*" value="" onchange=""/>
          <?php //echo form_error('featured_image', '<div class="error_validate">', '</div>'); ?>
        </p>
 -->
        <p>
          <label>Product Status</label>
          <select name="active_inactive" id="active_inactive">
            <option value="active" <?php if($product_detail[0]['active_inactive']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
                          <option value="inactive" <?php if($product_detail[0]['active_inactive']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
        </p>


        <p>
          <label>Short Description<span style="color:red;">*</span></label>
          <textarea name="short_disc" class="smallinput" id="short_disc" data-rule-required="true" data-msg-required="Please Enter Short Description" /><?php echo $product_detail[0]['short_disc']?></textarea>
        </p>

        <p>
          <label>Description<span style="color:red;">*</span></label>
          <textarea name="description" class="smallinput" id="description" data-rule-required="true" data-msg-required="Please Enter  Description"  /><?php echo $product_detail[0]['description']?></textarea>
        </p>
        <!--  </div> -->
        <!-- </div>contentwrapper -->

        <input type="hidden" id="check_id" name="check_id" value="<?php echo $product_detail[0]['product_id']?>"  />

        <p class="stdformbutton">
          <button class="submit radius2" id="addeBook">Update</button>
        </p>
    </div><!-- centercontent -->
  </form>
</div>

</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('description');
  CKEDITOR.replace('pfeatures');
</script>

  <script type="text/javascript">
        $.noConflict();
        jQuery('#addeBook').click(function(){
        jQuery('#ebook_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
      jQuery(element).removeClass(errorClass);
        }
        });
      });
      
</script>

<script>
 var catid;
 function get_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
              for(var i=0;i<result_length;i++)
              {
                cathtml+="<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
            cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_category').html(cathtml);
        }
     });
 }

function get_sub_sub_list(catid)
{
  $.ajax({
    url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
    data:{'category_id':catid},
    type: 'POST',
    success: function(result)
    {
      //alert("catid"+result);
      var result_data=JSON.parse(result);
      var result_length=result_data.length;
      var cathtml="";
      cathtml = "<option value=''>--select variant--</option>";
      if(result_length>0)
      {
        for(var i=0;i<result_length;i++)
        {
          cathtml += "<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
        }
      }
      else
      {
        cathtml+="<option value=''>--select variant--</option>";
      }
      $('#sub_sub_category').html(cathtml);
    }
  });
}
</script>


<script type="text/javascript">
    function check_ebook(obj)
    {
      var checkId = $('#check_id').val();

        $.ajax({
                type : 'POST',
                url  : '<?php echo site_url('admin/add_product/check_ebook')?>',
                data : {pname:obj,check_id:checkId},
                success:function(resp)
                {
                  resp = resp.trim();

                  if(resp =="SUCCESS")
                  {
                    var pname = $('#pname').val();

                    $('#pnameDiv').css('color','red');
                    $('#pname').val('');
                    $('#pnameDiv').html('Product ' +pname+ ' Already Exists');
                    return false;
                  }
                  else
                  {
                    return true;
                  }
                }

        });
    }

</script>

</body>
</html>
