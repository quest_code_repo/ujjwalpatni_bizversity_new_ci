<div class="centercontent tables">
<form class="stdform" action="" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Attribute Value</h1>
        </div><!--pageheader-->
        <?php include('session_msg.php'); ?>
        <div id="contentwrapper" class="contentwrapper">
          <div class="">
            <p>
              <label>Attribute Name<span style="color:red;">*</span></label>
                <span class="field">
                <select name="attr" id="attr" required="required" onchange="get_att_val(this.value);">
                  <option value="">--Select--</option>
                    <?php foreach($attr as $att) { ?>
                  <option value="<?php echo $att['attr_type_id']; ?>"><?php echo $att['attr_type_name']; ?></option>
                  <?php } ?>
                </select>
              </span> 
            </p>

            <p id="attr_div">
              <label>Attribute Value<span style="color:red;">*</span></label>
                <span class="field">
                  <input type="text" class="smallinput" name="att_val" >
              </span> 
            </p>

            <p id="has_code" style="display:none;">
              <label>Hash Code<span style="color:red;">*</span></label>
                <span class="field">
                  <input type="text" class="jscolor smallinput" name="hascode"  >
              </span> 
            </p>

            <p id="texture_img" style="display:none;">
              <label>Texture Image<span style="color:red;">*</span></label>
                <span class="field">
                  <input type="file" class="smallinput" name="texture_image"  >
              </span> 
            </p>
            <p>
               <label></label>
              <input type="submit" name="submit" value="submit">
            </p>
          </div>
                   
        </div><!--contentwrapper-->
   </form>
     
      <!------- Including PHP Script here ------>

  </div>
<script type="text/javascript">
var id;
  function get_att_val(id)
  {
    if(id == 1)
      $('#has_code').show();
    else
      $('#has_code').hide();

    if(id==5)
    {
      $('#texture_img').show();
      // $('#attr_div').hide();

    }
    else
    {
       $('#attr_div').show();
      // $('#texture_img').hide();
    }
  }
</script>
</div><!--bodywrapper-->

</body>

</html>