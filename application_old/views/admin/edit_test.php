<link rel="stylesheet" href="<?php echo base_url(); ?>assets/timepicker/jquery.simple-dtpicker.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>


<div class="centercontent tables">
  <form id="edit-test-form" class="stdform" action="" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Edit Test</h1>
         
          
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper"> 
            <?php
        if (!empty($test_detail[0]['sem_id'])) {
        ?>

        <div class="form-group">
          <label>Course Name</label>
          <select name="sem_id" id="sem_id" onchange="get_chapter_course_wise(this.value);">
             <?php 
              if(!empty($course_list))
              {
              foreach($course_list as $each_sem)
              {
              if($test_detail[0]['sem_id']==$each_sem['product_id'])
              { 
              $select_id="selected='selected'";
              }
              else
              {
              $select_id="";
              } 
              ?>
            <option value="<?php echo $each_sem['product_id'];?>" 
                    <?php echo $select_id;?> >
            <?php echo $each_sem['pname'];?>
            </option>
            <?php } } ?>
          </select>

          <!-- <input type="hidden" name="sem_id" id="sem_id" value="<?php echo $test_detail[0]['sem_id'];?>"> -->

        </div>

        <?php } ?>



           <div class="form-group">
          <label>Chapter Name</label>
          <select name="chapter_id" id="chapter_id" >
             <?php 
              if(!empty($chapter_list))
              {
              foreach($chapter_list as $each_sem)
              {
              if($test_detail[0]['chapter_id']==$each_sem['ch_id'])
              { 
              $select_id="selected='selected'";
              }
              else
              {
              $select_id="";
              } 
              ?>
            <option value="<?php echo $each_sem['ch_id'];?>" 
                    <?php echo $select_id;?> >
            <?php echo $each_sem['ch_name'];?>
            </option>
            <?php } } ?>
          </select>

          <!-- <input type="hidden" name="sem_id" id="sem_id" value="<?php echo $test_detail[0]['sem_id'];?>"> -->

        </div>

        <div class="form-group">
          <label>Test Name<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="test_name" class="smallinput" id="test_name" required="required" value="<?php echo $test_detail[0]['test_name'];?>"  /></span>
               <?php echo form_error('test_name', '<div class="error_validate">', '</div>'); ?>
        </div>  

      

        <div class="form-group">
          <label>Ideal Time Duration<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="ideal_time_duration" class="smallinput" id="ideal_time_duration" required="required" value="<?php echo $test_detail[0]['ideal_time_duration'];?>"  /></span>
               <?php echo form_error('ideal_time_duration', '<div class="error_validate">', '</div>'); ?>
        </div>
        
     
        <div class="form-group">
          <label>Total Marks<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="total_marks" class="smallinput" id="total_marks" required="required" value="<?php echo $test_detail[0]['total_marks']; ?>" /></span>
             <?php echo form_error('total_marks', '<div class="error_validate">', '</div>'); ?>
        </div>


         <p>
          <label>Start Time<span style="color:red;">*</span></label>
          <span class="field">
            <input type="text" class="smallinput " name="date10" value="<?php echo $test_detail[0]['appear_time']; ?>"> 
          </span>
        </p>
        <p>
          <label>End Time<span style="color:red;">*</span></label>
          <span class="field">
            <input type="text" class="smallinput " name="date11" value="<?php echo $test_detail[0]['disappear_time']; ?>">
          </span>
        </p>

      
       
<!--          <div class="container">
    <div class="row">
        <div class='col-sm-6'>
            <label>Start Time<span style="color:red;">*</span></label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="date10" value="<?php echo $test_detail[0]['appear_time']; ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
    </div>
</div>


  <div class="container">
    <div class="row">
        <div class='col-sm-6'>
            <label>End Time<span style="color:red;">*</span></label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="date11" value="<?php echo $test_detail[0]['disappear_time']; ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker();
            });
        </script>
    </div>
</div> -->
          

        <div class="form-group">
          <label>Test Status</label>
          <select name="test_status" id="test_status">
            <option value="active" <?php if($test_detail[0]['test_status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($test_detail[0]['test_status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
        </div>

        <div class="text-center" style="padding-bottom: 20px;"> 
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
        <a href="<?php echo base_url();?>admin/add_semester/test_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > 
      </div>
      <div class="clearfix"></div>
      
      </div><!--contentwrapper-->
  <!-- centercontent -->
  </form>    
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('test_description');
  CKEDITOR.replace('description');
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#edit-test-form").validate({
    rules:{
      
      lecture_id: {
        required: true,
      },
       test_type: {
        required: true,
      },
      test_name: {
        required: true,
      },
      description_url: {
        required: true,
        url:true
      },desc_video_pass: {
        required: true,
      },
      ideal_time_duration: {
        required: true,
      },
      test_no_of_question: {
        required: true,
        number:true
      },
      test_passing_marks: {
        required: true,
        number:true
      },
      test_energy_point: {
        required: true,
        number:true
      }

    },
    
    messages:{
      lecture_id: "Please Select Topic Name",
      test_type: "Please Select Test Type",
      test_name: "Please Enter Name",
      ideal_time_duration: "Please Enter Time Duration",
      description_url: "Please Enter Valid Test Description URL",
      desc_video_pass: "Please Enter Test Description Video Password",
      test_no_of_question: "Please Enter Question in Number",
      test_passing_marks: "Please Enter number",
      test_energy_point: "Please Enter number"
      

    },
       
  });
</script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/timepicker/jquery.simple-dtpicker.js"></script>
   <script type="text/javascript">
      (function($) {
        $(function(){
          $('*[name=date10]').appendDtpicker({
            "closeOnSelected": true
          });
        });
        $(function(){
          $('*[name=date11]').appendDtpicker({
            "closeOnSelected": true
          });
        });
//         $('#test_start_time').appendDtpicker({
//   "autodateOnStart": false
// });
        })(jQuery);
      </script>


          <script type="text/javascript">
    function get_chapter_course_wise(courseId)
    {
      $.ajax({
                type : 'POST',
                url  : '<?php echo base_url("admin/add_semester/get_chapter_course_wise")?>',
                data : {'course_id':courseId},
                success:function(resp)
                {
                    resp = resp.trim();
                    $('#chapter_id').html(resp);
                }

      })
    }
</script>


</body>

</html>
