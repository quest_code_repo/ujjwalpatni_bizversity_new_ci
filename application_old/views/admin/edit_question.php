<div class="centercontent tables">
  <form id="edit-test-form" class="stdform editquestion" action="" method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
          <h1 class="pagetitle">Edit Question</h1>
         
          
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper"> 
        <!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {          
            echo $this->session->flashdata('error');           
          }
         ?>

        <input type="hidden" name="test_id" value="<?php echo $question_detail[0]['test_id']; ?>">
        <!-- <input type="hidden" name="segment_id" value="<?php echo $this->uri->segment(5); ?>"> -->
        <div class="form-group">
            <label>Question<span style="color:red;">*</span></label>
            <textarea class="form-control" name="question_desc"  id="question_desc" class="smallinput"><?php echo $question_detail[0]['question_desc']; ?></textarea>
        </div>
        <div class="form-group">
            <label>Answer 1<span style="color:red;">*</span></label>
            <textarea  name="ans_1"  id="ans_1" class="smallinput"><?php echo $question_detail[0]['ans_1']; ?></textarea>
        </div>
        <div class="form-group">
            <label>Answer 2<span style="color:red;">*</span></label>
            <textarea  name="ans_2"  id="ans_2" class="smallinput"><?php echo $question_detail[0]['ans_2']; ?></textarea>
        </div>
        <div class="form-group">
            <label>Answer 3<span style="color:red;">*</span></label>
            <textarea  name="ans_3"  id="ans_3" class="smallinput"><?php echo $question_detail[0]['ans_3']; ?></textarea>
        </div>
        <div class="form-group">
            <label>Answer 4<span style="color:red;">*</span></label>
            <textarea  name="ans_4"  id="ans_4" class="smallinput"><?php echo $question_detail[0]['ans_4']; ?></textarea>
        </div>

        <div class="form-group">
            <label>Right Answer<span style="color:red;">*</span></label>
            <!-- <textarea  name="ques_right_ans"  id="ques_right_ans" class="smallinput"></textarea> -->
            <input type="text" value="<?php echo $question_detail[0]['ques_right_ans']; ?>" name="ques_right_ans" id="ques_right_ans" class="smallinput">
        </div>

        <!-- <div class="form-group">
            <label>Right Answer Explanation<span style="color:red;">*</span></label>
            <textarea name="explain_right" class="smallinput" id="ans_4"/><?php echo $question_detail[0]['explain_right_answer']; ?></textarea>
        </div> -->

        <div class="form-group" style="width: 33%; float: left;">
          <label>Marks<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="que_marks" class="smallinput" id="que_marks" required="required" value="<?php echo $question_detail[0]['que_marks'];?>"  /></span>
               <?php echo form_error('que_marks', '<div class="error_validate">', '</div>'); ?>
        </div>        

       

        <div class="form-group" style="width: 33%; float: left;">
          <label>Test Status</label> <span class="field">
          <select name="ques_status" id="ques_status">
            <option value="active" <?php if($question_detail[0]['ques_status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($question_detail[0]['ques_status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
          </span>
        </div>
        
        <div style="clear:both;"></div>
        <div class="text-center" style="padding-bottom: 20px;"> 
        <a href="<?php echo base_url();?>admin/add_semester/manage_question/<?php echo $question_detail[0]['test_id']; ?>/?lect_id=<?php echo $lect_id;?>&ch_id=<?php echo $ch_id;?>&sem_id=<?php echo $sem_id;?>"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
     
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
      </div>
      <div class="clearfix"></div>   
          
         <!--  <div class="form-group text-center">
              <button class="submit radius2" id="editbtn">Save</button>
          </div> -->
                 
      </div><!--contentwrapper-->
  <!-- centercontent -->
  </form>    
</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/tinymce/js/tinymce/tinymce.min.js?apiKey=x7mmfrsinal4f5v31e7q5iiwvluk8b2nof1eslzgr05i98bi"></script>
<script>

tinymce.init({
    selector: "textarea",
    themes: "modern",
    height : 50,
  
});


</script>
<script>
 var catid;
 function get_variant_list(catid)
 {
  
   $.ajax({

            url: '<?php echo base_url();?>admin/furnish_dashboard/get_category_variant',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
            var result_data=JSON.parse(result);
            var result_length=result_data.length;
            var cathtml="";
            if(result_length>0)
            {
          
             for(var i=0;i<result_length;i++)
             {
            cathtml+="<option value='"+result_data[i]['variant_id']+"'>"+result_data[i]['variant_name']+"</option>";
            
             }

          
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }

        

      
        $('#category_variant').html(cathtml);
              
                  
            }
     });

   $.ajax({
            url: '<?php echo base_url();?>admin/furnish_dashboard/get_cat_property_detail',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert(result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml1 ="";
              if(result_length>0)
              {
                cathtml1 +="<table class='table table-bordered table-responsive'><thead><tr><th>Property</th><th>Value</th></tr></thead>";
                for(var i=0;i<result_length;i++)
                {
                  cathtml1+="<tr><td>"+result_data[i]['property_name']+"</td><td><input name='prop["+result_data[i]['property_id']+"]' type='text'></td></tr>";
                }
                cathtml1 += "</table>";
                //alert(cathtml1);
              }
              else
              {
                cathtml1 = "";
              }
              $('.cat_pro').html(cathtml1);
        }
     });
 }
</script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#edit-test-form").validate({
    rules:{
      
      lecture_id: {
        required: true,
      },
       test_type: {
        required: true,
      },
      test_name: {
        required: true,
      },
      ideal_time_duration: {
        required: true,
      },
      test_no_of_question: {
        required: true,
        number:true
      },
      test_passing_marks: {
        required: true,
        number:true
      },
      test_energy_point: {
        required: true,
        number:true
      }

    },
    
    messages:{
      lecture_id: "Please Select Lecture Name",
      test_type: "Please Select Test Type",
      test_name: "Please Enter Name",
      ideal_time_duration: "Please Enter Time Duration",
      test_no_of_question: "Please Enter Question in Number",
      test_passing_marks: "Please Enter number",
      test_energy_point: "Please Enter number"
      

    },
       
  });
</script>

</body>

</html>
