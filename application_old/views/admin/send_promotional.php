<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<?php if (isset($message)) { ?>
<div class="notibar msgsuccess" style="color:green;"><p><strong>Mail Sent successfully</strong></p></div>
<?php } ?>
<form class="stdform" action="<?php echo base_url(); ?>admin/Addservices/send_promotional_mail" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">

            <h1 class="pagetitle">Send Promotional E-Mails</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
      
               
                      <div>
                      
                          <p>
                          <label>Select All<span style="color:red;"></span></label>
                          <input type="checkbox" id="select_all"/>
                          </p>


                          <p>
                          <label>Choose Reciever Mail<span style="color:red;">*</span></label>
                           <select name="receiver_emails[]" id="receiver_emails" required="required" multiple>

                            <?php 
                              if(!empty($reciver_emails))
                              {
                                foreach($reciver_emails as $reciver_emails)
                                {
                              ?>
                              <option value="<?php echo $reciver_emails['username'];?>"><?php echo $reciver_emails['username'];?></option>
                              <?php
                                } 
                              }
                            ?>
                           </select>
                          </p>

                          <p>
                          <label>Subject<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="email_subject" class="mediuminput" id="email_subject"/></span>
                        </p>
                        <div class="service_field_error"><?php echo form_error('email_subject'); ?></div>


                         <div class="form-group">
                         <h5 style="color: black;font-weight: bold;">Email Content<span style="color:red;">*</span></h5>
                         <textarea name="description" class="smallinput" id="email_content" /></textarea>
                           <?php echo form_error('description', '<div class="error_validate">', '</div>'); ?>
                     </div>


                        
                        
                      
            
                        <p>
                          <label>Service Status<span style="color:red;"></span></label>
                           <select name="active_status" id="active_status" >
                             <option value="Active">Active</option>
                             <option value="Inactive">Inactive</option>
                          </select>
                          </p>
            
                        
                        </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
                 <p class="stdformbutton">
                          <button class="submit radius2" id="addbtn">SEND MAIL</button>
                 </p>
                    
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('email_content');


//  if(jQuery('#select_all').click(function(){
  
    
// });
// }



jQuery("#select_all").change(function(){   
    jQuery(".checkbox").prop('checked', jQuery('#receiver_emails option').prop('selected', true));
});


jQuery('#select_all').change(function(){ 

    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == jQuery(this).prop("checked")){ //if this item is unchecked
        jQuery(".checkbox").prop('checked', jQuery('#receiver_emails option').prop('selected', false));
    }
  });

</script>

</div><!--bodywrapper-->

</body>

</html>