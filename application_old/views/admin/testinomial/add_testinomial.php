<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/testimonial_control/add_testimonial" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Proposal</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Category<span style="color:red;">*</span></label>
              <span class="field">
                <select  id="category" class="test" required="required" multiple="multiple" name="category[]">
                  <option value="">Select Category</option>
                  <?php if(!empty($testimonial_category)) {
                    foreach($testimonial_category as $category){
                  ?>
                  <option value="<?php echo $category->test_cat_id;?>"><?php echo $category->category_name;?></option>
                   <?php } } ?>
                </select>
              </span>
            </p>

            <p>
              <label>Title<span style="color:red;">*</span></label>
              <span class="field">
                <input type="text" name="title" class="mediuminput" id="title" required="required" />
              </span>
            </p>

            <p>
              <label>Descriptions<span style="color:red;">*</span></label>
                <span class="field"><textarea name="descriptions" class="mediuminput" id="descriptions" required="required"></textarea></span>
            </p>

            <p>
              <label>Image<span style="color:red;"></span></label>
              <span class="field"><input type="file" name="testi_image" class="mediuminput" id="testi_image" accept="image/*"  required="required"/></span>
            </p>

          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Testimonial</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>