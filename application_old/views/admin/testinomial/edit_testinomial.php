<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/testimonial_control/edit_testimonial" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
          <h1 class="pagetitle">Edit Testimonial</h1>
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Category<span style="color:red;">*</span></label>
              <span class="field">
                <input type="text" class="mediuminput" id="title" disabled="disabled" value="<?php echo $update_record[0]->category_name;?>"/>
              </span>
            </p>
              <input type="hidden" name="testimonials_id" value="<?php echo $update_record[0]->testimonials_id;?>">
            <p>
              <label>Title<span style="color:red;">*</span></label>
              <span class="field">
                <input type="text" name="title" class="mediuminput" id="title" required="required" value="<?php echo $update_record[0]->title;?>" />
              </span>
            </p>

            <p>
              <label>Descriptions<span style="color:red;">*</span></label>
                <span class="field"><textarea name="descriptions" class="mediuminput" id="descriptions" required="required"><?php echo $update_record[0]->descriptions;?></textarea></span>
            </p>

            <p>
              <label>Old Testimonial Image<span style="color:red;"></span></label>
              <span class="field">
                <img width="300px" src="<?php echo base_url();?>uploads/testinomial_images/<?php echo $update_record[0]->testi_image;?>">

                <input type="hidden" name="old_image" value="<?php echo $update_record[0]->testi_image;?>">
              </span>
            </p>

            <p>
              <label>Image<span style="color:red;"></span></label>
              <span class="field">
                <input type="file" name="testi_image" class="mediuminput" id="testi_image" accept="image/*"/>
              </span>
            </p>

          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Update Testimonial</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>