<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_semester/add_new_ebook_content" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Ebook Content</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Ebook Content Text<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="ebook_content_text" class="mediuminput" id="category_name" required="required" /></span>
            </p>

           
            <p>
              <label>Ebook Content File<span style="color:red;"></span></label>
              <span class="field"><input type="file" name="ebook_content_image" class="mediuminput" id="cat_image" accept="image/*"  required/></span>
            </p>

          
            <p>
              <label>Ebook Content Status<span style="color:red;">*</span></label>
               <select name="active_status" id="active_status">
                 <option value="active">Active</option>
                 <option value="inactive">Inactive</option>
              </select>
              </p>

            
          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Submit</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>