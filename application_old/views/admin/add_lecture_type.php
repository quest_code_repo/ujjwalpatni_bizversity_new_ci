<div class="centercontent tables">
<form id="type-form" class="stdform" action="<?php echo base_url(); ?>admin/add_semester/add_lecture_type" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Topic Type</h1>
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
                    	<!-- <div class="one_half"> -->
                      <?php 
                        if($this->session->flashdata('error'))
                        {
                          echo $this->session->flashdata('error'); 
                        }
                       ?>
                        <p>
                          <label>Topic Type<span style="color:red;">*</span></label>
                            <span class="field"><input type="text" name="type_name" class="smallinput" id="type_name" required="required" /></span>
                            <?php echo form_error('type_name', '<div class="error_validate">', '</div>'); ?>
                        </p>

                        <p>
                          <label>Description<span style="color:red;">*</span></label>
                          <textarea name="type_description" class="smallinput" id="type_description" /></textarea>
                        </p>

                        <p>
                          <label>Topic Status</label>
                          <select name="type_status" id="type_status">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                          </select>
                        </p>

                       <!--  </div> -->


                       
                   
        </div><!--contentwrapper-->

        <div class="text-center" style="padding-bottom: 20px;"> 
     
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
        <a href="<?php echo base_url();?>admin/add_semester/lecture_type_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > 
      </div>
      <div class="clearfix"></div>
		
  	 <!-- <p class="stdformbutton">
             <button class="submit radius2" id="addbtn">Save</button>
     </p -->>

	</div><!-- centercontent -->
				
  </form>
     
			<!------- Including PHP Script here ------>

		</div>
	



</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('type_description');
  CKEDITOR.replace('pfeatures');
</script>
<script>
 var catid;
 function get_sub_list(catid)
 {
 	 $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
  				    var result_data=JSON.parse(result);
  				    var result_length=result_data.length;
  				    var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
				    if(result_length>0)
				    {
					   for(var i=0;i<result_length;i++)
					   {
						    cathtml+="<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
						  }
				    }
				  else
				  {
					cathtml+="<option value=''>--select variant--</option>";
				  }
				  $('#sub_category').html(cathtml);
        }
     });
 }
 function get_sub_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
              
             for(var i=0;i<result_length;i++)
             {
                
                cathtml += "<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_sub_category').html(cathtml);
        }
     });
 }
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#type-form").validate({
    rules:{
      
      type_name: {
        required: true,
      }

    },
    
    messages:{
      type_name: "Please Enter Topic Type"

    },
       
  });
</script>

</body>

</html>
