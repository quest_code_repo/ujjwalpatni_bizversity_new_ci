 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}

 </style>

 <div class="centercontent">
      <div class="pageheader notab">
        <h1 class="pagetitle">Order List</h1>
      </div><!--pageheader-->
        <?php 
        if($this->session->flashdata('success'))
        {
         ?>
         <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
         </div>
         <?php
        }
        else if($this->session->flashdata('error'))
        {
         ?>
          <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php
        }
        ?>
        <div id="contentwrapper" class="contentwrapper">
          <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

          <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
            <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Order Code</th>
                    <th>Tracking ID</th>
                    <!-- <th>Coupon Code</th> -->
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Total Paid Amount</th>
                    <th>Payment Status</th>
                    <th>Delivery Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              <?php 
                if(!empty($all_orders))
                {

                  $get_page = $this->input->get('per_page');

                  if(!empty($get_page)){

                    $j = $get_page+1;

                  } else {

                    $j=1;
                  }
                    foreach($all_orders as $result)
                {?>
                <tr>
                    <td><?php echo $j; ?></td>
                    <td><?php  echo $result->order_code; ?></td>
                    <td><?php  echo $result->tracking_id; ?></td>
                    <!-- <td><?php  //echo $result->code; ?></td> -->
                    <td><?php  echo $result->full_name; ?></td>
                    <td><?php  echo $result->username; ?></td>
                    <td><?php  echo $result->order_amount; ?></td>
                    <td><?php  echo $result->payment_status; ?></td>
                    <td><?php  echo $result->delivery_status; ?></td>
                 
                    <td><a href="<?php echo base_url('admin/Orders/order_view/'.$result->product_order_id.'')?>"   title="View"><i class="fa fa-eye" aria-hidden="true" style='color:red;'"></i></a>
                    <a href="<?php echo base_url('admin/Orders/edit_order/'.$result->product_order_id.'')?>"   title="View"><i class="fa fa-edit" aria-hidden="true" style='color:red;'"></i></a>
                    </td>
    
                </tr>
                <?php 
                    $j++;
                      }
                    }
                ?>
                
            </tbody>
          </table>
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
    
            
        
    </div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>

