<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">
<style type="text/css">
    .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}
</style>
<div class="centercontent tables">
<form class="stdform" id="product_form" name="product_form" action="<?php echo base_url('admin/Orders/update_delivery_status')?>" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Change Delivery Status</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
                      <!-- <div class="one_half"> -->
                      <?php 
          
                      if($this->session->flashdata('error'))
                      {
                        ?>
                        <span style="color:red"><?php echo $this->session->flashdata('error'); ?></span>
                        <?php
                       
                      }
                      if($this->session->flashdata('success'))
                      {
                        ?>
                        <span style="color:green"><?php echo $this->session->flashdata('success'); ?></span>
                        <?php
                        
                      }
                  ?>

              <input type="hidden" name="product_order_id" value="<?php echo $Order[0]['product_order_id'];?>"   />
                      
                        <p>
                          <label>Change Delivery Status</label>
                          <select name="delivery_status" id="delivery_status" required="required">

                          <?php
                            if($Order[0]['delivery_status'] =="delivered")
                            {
                              ?>
                            <option value="delivered" selected="selected" >Delivered</option>
                            <option value="not-delivered">Not-Delivered</option>
                            <?php
                          }
                          else
                          {
                            ?>
                            <option value="delivered"  >Delivered</option>
                            <option value="not-delivered" selected="selected" >Not-Delivered</option>
                            <?php
                          }
                        ?>
                          </select>
                        </p>

                   
        </div><!--contentwrapper-->
    
     <p class="stdformbutton">
             <button type="submit"  id="addProduct">Update Status</button>
     </p>

  </div><!-- centercontent -->
        
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  



</div><!--bodywrapper-->
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>




</body>

</html>
