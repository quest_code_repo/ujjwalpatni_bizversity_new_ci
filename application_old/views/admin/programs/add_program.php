<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">


<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
 .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}
.block-adjust{
  width: 148px !important;
min-width: 12% !important;
display: inline-block !important;
height: 37px !important;
border-radius: 1 !important;
vertical-align: middle;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_programs_category/add_programs" method="post" enctype="multipart/form-data">

 <?php 
          if($this->session->flashdata('success'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
           </div>
           <?php
          }
          else if($this->session->flashdata('error'))
          {
           ?>
           <div class="">
            <?php echo $this->session->flashdata('error'); ?>
           </div>
           <?php
          }
        ?>
        
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Programs</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Program Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_name" class="mediuminput" id="program_name" required="required" placeholder="Program Name"/></span>
            </p>
            <p id="title_exist_error" style="color: red;text-align: center;"></p>
            <div class="service_field_error"><?php echo form_error('program_name'); ?></div>

            <p>
              <label>Category<span style="color:red;">*</span></label>
               <select name="program_category" id="program_category" required="required">
                 <option value="">Select Category</option>

                 <?php if(!empty($cats)){ 

                  foreach($cats as $catgory){?>

                  <option value="<?php echo $catgory->id;?>"><?php echo $catgory->name;?></option>

                  <?php } } ?>
                
              </select>
              </p>

              <p>
                <label>Add Speakers<span style="color:red;">*</span></label>
                <span class="field">
                  <select  id="coupos_programs" class="test form-control" multiple="multiple" name="event_speakers[]">
                     <option value="">Select Speakers</option>
                  <?php if(!empty($speakers)) {
                    foreach($speakers as $speks){
                    ?>
                     <option value="<?php echo $speks->id;?>"><?php echo $speks->speaker_name;?></option>

                     <?php } } ?>
                     
                </select>
                 <span class="" id="no_program_selected" generated="true"></span>
                </span>
              
              </p>

              <p>
                <label>City<span style="color:red;">*</span></label>
                <span class="field">
                  <select  id="program_city" class="test form-control" name="program_city" required="required">
                     <option value="">Select City</option>
                  <?php if(!empty($all_cities)) {
                    foreach($all_cities as $city){
                    ?>
                     <option value="<?php echo $city->city_id;?>"><?php echo $city->city_name;?></option>
                  <?php } } ?>                     
                </select>
                </span>
              </p>


            <p>
              <label>Program Start Date/Time<span style="color:red;">*</span></label>
                <span class="field">
                  <input type="text" name="program_date" class="mediuminput datetimepicker block-adjust" id="program_date" required="required" placeholder="yyyy-mm-dd" />
                  
                    <select class="form-control block-adjust" id="sel1"  required="required" name="start_hours">
                     <option value="">HH</option>
                       <?php 
                    $i;
                    for($i=0;$i<=12;$i++){ 

                      $checks =  strlen($i);

                      if( $checks == 1){ ?>

                      <option value="0<?php echo $i;?>"><?php echo '0'.$i;?></option>

                    <?php  } else { ?>
                     
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>

                      <?php } ?>

                      <?php } ?>
                    </select>
                    <select class="form-control block-adjust" id="sel1"  required="required"  name="start_minutes">
                    <option value="">MM</option>
                      <?php for($i=0;$i<=59;$i++){ 
                     
                      $checks =  strlen($i);

                      if( $checks == 1){ ?>

                      <option value="0<?php echo $i;?>"><?php echo '0'.$i;?></option>

                    <?php  } else { ?>
                     
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>

                      <?php } ?>

                      <?php } ?>
                    </select>
                    <select class="form-control block-adjust" id="sel1"  required="required" name="start_seconds">
                     <option value="">SS</option>
                      <?php for($i=0;$i<=59;$i++){ 

                        $checks =  strlen($i);

                      if( $checks == 1){ ?>

                      <option value="0<?php echo $i;?>"><?php echo '0'.$i;?></option>

                    <?php  } else { ?>
                     
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>

                      <?php } ?>

                      <?php } ?>
                      
                    </select>
                    <select class="form-control block-adjust" id="sel1"  required="required" name="start_ams_pms">
                      <option value="AM">AM</option>
                      <option value="PM">PM</option>
                      
                    </select>
                </span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_date'); ?></div>

             <p>
              <label>Program End Date/Time<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_end_date" class="mediuminput datetimepicker block-adjust" id="program_end_date" required="required" placeholder="yyyy-mm-dd" />
                <select class="form-control block-adjust" id="sel1" name="end_hours" required="required">
                <option value="">HH</option>

                <?php 
                $i;
                for($i=0;$i<=12;$i++){ 

                  $checks =  strlen($i);

                      if( $checks == 1){ ?>

                      <option value="0<?php echo $i;?>"><?php echo '0'.$i;?></option>

                    <?php  } else { ?>
                     
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>

                      <?php } ?>

                      <?php } ?>
                      
                    </select>
                    <select class="form-control block-adjust" id="sel1" required="required" name="end_minutes">
                       <option value="">MM</option>
                      <?php for($i=0;$i<=59;$i++){ 

                        $checks =  strlen($i);

                      if( $checks == 1){ ?>

                      <option value="0<?php echo $i;?>"><?php echo '0'.$i;?></option>

                    <?php  } else { ?>
                     
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>

                      <?php } ?>

                      <?php } ?>
                    </select>
                    <select class="form-control block-adjust" id="sel1" required="required" name="end_seconds">
                       <option value="">SS</option>
                      <?php for($i=0;$i<=59;$i++){ 

                        $checks =  strlen($i);

                      if( $checks == 1){ ?>

                      <option value="0<?php echo $i;?>"><?php echo '0'.$i;?></option>

                    <?php  } else { ?>
                     
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>

                      <?php } ?>

                      <?php } ?>
                    </select>
                    <select class="form-control block-adjust" id="sel1" required="required" name="end_ams_pms">
                     <option value="AM">AM</option>
                      <option value="PM">PM</option>
                    </select></span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_end_date'); ?></div>

            <p>
              <label>Location<span style="color:red;">*</span></label>
                <span class="field"><textarea type="text" name="location" class="mediuminput" id="location" required="required" placeholder="Location" /></textarea></span>
            </p>
            <div class="service_field_error"><?php echo form_error('location'); ?></div>

            <p>
              <label>Program Price In Rs.<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="program_price" class="mediuminput" id="program_price" required="required" placeholder="Enter Price" onkeypress="return isNumberKey(event);"/></span>
            </p>
            <div class="service_field_error"><?php echo form_error('program_price'); ?></div>

           <p>
              <label>Status<span style="color:red;">*</span></label>
               <select name="program_status" id="program_status">
                 <option value="active">Active</option>
                 <option value="inactive">Inactive</option>
              </select>
              </p>

          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn" onclick="return check_programs_have();">Add Program</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('sem_description');

  </script>

</div><!--bodywrapper-->




</body>

</html>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<script type="text/javascript">
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>

<script type="text/javascript">

function check_title(){

  var cat_name = $('#program_name').val();
  var type_val = 2;

  $.ajax({
         type:'POST',
         url:'<?php echo site_url();?>admin/add_programs_category/check_category_title',
         data:{"name":cat_name,'type_calls':type_val},
         success: function(data)
         {  
            if(data==1){

              $('#program_name').val('');
              $('#title_exist_error').html('Program Name Already Exist');

            } else{
              $('#title_exist_error').html('');

            }
         }
     });
}

function isNumberKey(evt)
    {
                 var charCode = (evt.which) ? evt.which : evt.keyCode
                 if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                 return true;
    }
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/admincss/js/jquery.datetimepicker.js"></script> 
 <!-- <script type="text/javascript" src="http://182.70.242.26/bizversity/assets/admincss/js/plugins/jquery.validate.min.js"></script> -->

  <script>
  $( function() {
    $.noConflict();
   
     jQuery('.datetimepicker').datetimepicker({
                timepicker: false,
                datepicker: true,
                format: 'Y-m-d',
                formatDate: 'Y-m-d',
            })


     jQuery('.from_to_time').datetimepicker({
                datepicker: false,
                format: 'H:i:s',
                formatDate: 'H:i:s',
            })



  });

  function check_programs_have(){

    var val_pro = jQuery('#coupos_programs').val();
    
    if(val_pro==''){

    jQuery('#no_program_selected').html('Select Speaker');
    jQuery('#no_program_selected').css('color','red');
    return false;

  } else{

    jQuery('#no_program_selected').html('');

  }
    
  }

  </script>