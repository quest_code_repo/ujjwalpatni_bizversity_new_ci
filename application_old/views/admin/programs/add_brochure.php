<style>
.main_cat li {
  list-style-type: none;
  padding: 0 0 5px;
}
.main_cat p {
    margin: 10px 0;
}
</style>

<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_programs_category/new_brochure" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Brochure</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
          <div>
            <p>
              <label>Brochure Name<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="brochure_name" class="mediuminput" id="brochure_name" required="required" /></span>
            </p>

           
            <p>
             <label>Parent Category<span style="color:red;"></span></label>
              <select name="above_category" id="above_category" required="required">
               <option value="">--select parent category--</option>

              <?php 
                if(!empty($program_category))
                {
                  foreach($program_category as $each_parent_cat)
                  {
                ?>
                <option value="<?php echo $each_parent_cat->id;?>"><?php echo $each_parent_cat->name;?></option>
                <?php
                  } 
                }
              ?>
             </select>
            </p>

            <p>
              <label>Brochure File<span style="color:red;"></span></label>
              <span class="field"><input type="file" name="cat_image" class="mediuminput" id="cat_image" accept="image/*"  required="required"/></span>
            </p>


            

            
          </div>
                       
                        
                   
        </div><!--contentwrapper-->
      <!-- Trigger the modal with a button -->
  
       <p class="stdformbutton">
                <button class="submit radius2" id="addbtn">Add Brochure</button>
       </p>
  </form>
     
      <!------- Including PHP Script here ------>

    </div>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</div><!--bodywrapper-->

</body>

</html>