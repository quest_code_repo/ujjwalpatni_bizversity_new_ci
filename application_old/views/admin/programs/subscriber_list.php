 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}

 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Subscriber List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	}
        ?>
        <div id="contentwrapper" class="contentwrapper">
          

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

            <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Order Code</th>
                                                <th>Trackin ID</th>
                                                <th>Coupon Code</th>
                                                <th>User Name</th>
                                                <th>Email</th>
                                                <th>Phone No</th>
                                                <th>Purchase Program</th>
                                                <th>Total Paid Amount</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                            if(!empty($all_orders))
                                            {

                                              $get_page = $this->input->get('per_page');

                                              if(!empty($get_page)){

                                                $j = $get_page+1;

                                              } else {

                                                $j=1;

                                              }
                                                foreach($all_orders as $result)
                                            {?>
                                            <tr>

                                                <td><?php echo $j; ?></td>
                                                <td><?php  echo $result->order_code; ?></td>
                                                <td><?php  echo $result->tracking_id; ?></td>
                                                <td><?php  echo get_place_order_coupon($result->coupon_id); ?></td>
                                                <td><?php  echo $result->name; ?></td>
                                                <td><?php  echo $result->email; ?></td>
                                                <td><?php  echo $result->phone_no; ?></td>
                                                <td><?php  echo get_place_order_program($result->program_id); ?></td>
                                                <td><?php  echo $result->total_amount; ?></td>
                                                <td><?php  echo $result->status; ?></td>
                                             
                                                <td><a href="javascript:void(0);" onclick="return delete_category_data(<?php echo $result->id ?>)"  title="Delete"><i class="fa fa-times" aria-hidden="true" style='color:red;'"></i></a></td>
                                
                                            </tr>
                                            <?php 
                                                $j++;
                                                  }
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
        <div class="pagination"><p><?php echo $links;?></p></div>
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>

<script type="text/javascript">

    function delete_category_data(id)
    {

      if(confirm("Are you Sure! You Want to delete it")){   

        $.ajax({
         type:'POST',
         url:'<?php echo site_url();?>admin/Add_programs_category/delete_orders_placed',
         data:{"id":id},
         success: function(data)
         {  
            if(data==1){

                 location.reload();
            }
         }
     });

        return true;
      } else{
        
        return false;
      }

    }
  </script>