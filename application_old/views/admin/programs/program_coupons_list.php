 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Program Coupons List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	} else if($this->session->flashdata('update'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('update'); ?>
           </div>
           <?php
          }


        ?>
        <div id="contentwrapper" class="contentwrapper">
            <a href="<?php echo base_url();?>admin/Add_programs_category/add_coupons"><button class="btn-primary">ADD COUPONS</button></a>

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

            <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Coupon Name</th>
                                                <th>Programs</th>
                                                <th>Type</th>
                                                <th>Valid From</th>
                                                <th>Valid Till</th>
                                                <th>Flat Amount</th>
                                                <th>Percent(%) Amount</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                            if(!empty($coupon_list))
                                            {
                                                $j=1;
                                                foreach($coupon_list as $result)
                                            {?>
                                            <tr>

                                                <td><?php echo $j; ?></td>
                                                <td><?php  echo $result->coupon_name; ?></td>
                                                <td><?php  echo getd_programs_name($result->coupon_id); ?></td>
                                                <td><?php  echo $result->coupon_type; ?></td>
                                                <td><?php  echo $result->valid_from; ?></td>
                                                <td><?php  echo $result->valid_till; ?></td>
                                                <td><?php  echo $result->coupon_value; ?></td>
                                                <td><?php  echo $result->coupon_percentage; ?></td>
                                             
                                                <td><a href="<?php echo base_url();?>admin/Add_programs_category/edit_coupons_data/?coupon_id=<?php echo base64_encode($result->coupon_id);?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a><a href="javascript:void(0);" onclick="return delete_category_data(<?php echo $result->coupon_id ?>)"  title="Delete"><i class="fa fa-times" aria-hidden="true" style='color:red;'"></i></a></td>
                                
                                            </tr>
                                            <?php 
                                                $j++;
                                                  }
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>

<script type="text/javascript">

    function delete_category_data(id)
    {

      if(confirm("Are you Sure! You Want to delete it")){   

        $.ajax({
         type:'POST',
         url:'<?php echo site_url();?>admin/Add_programs_category/delete_data_coupons',
         data:{"id":id},
         success: function(data)
         {  
            if(data==1){

                 location.reload();
            }
         }
     });

        return true;
      } else{
        
        return false;
      }

    }
  </script>