 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}
.edit-list {
  list-style: outside none none;
  padding: 0;
}
.edit-list.icons-list.icon-width {
  width: 100px;
}
.edit-list > li {
  display: inline-block;
}
.icons-list > li {
  padding: 0 2px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Program List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	} else if($this->session->flashdata('update'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('update'); ?>
           </div>
           <?php
          }
        ?>
        <div id="contentwrapper" class="contentwrapper">
            <a href="<?php echo base_url();?>admin/add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a>

              <!-- <a href="<?php echo base_url();?>admin/Add_programs_category/add_programs"><button class="btn-primary">ADD PROGRAMS</button></a> -->

            <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
              <thead>
                  <tr>
                      <th>S.No.</th>
                      <th>Program Name</th>
                      <th>Category</th>
                      <th>Speakers Name</th>
                      <th>City</th>
                      <th>Price</th>
                      <th>Start Date/Time</th>
                      <th>End Date/Time</th>
                      <th>Location</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                 <?php 
                  if(!empty($all_data))
                  {
                      $j=1;
                      foreach($all_data as $result)
                  {?>
                  <tr>

                      <td><?php echo $j; ?></td>
                      <td><?php  echo $result->program_name; ?></td>
                      <td><?php  echo $result->name; ?></td>
                      <td><?php  echo getd_speakers_name($result->id); ?></td>
                      <td><?php  echo $result->city_name; ?></td>
                      <td><?php  echo $result->price; ?></td>
                      <!-- <td><?php  //echo get_cat_name($result->category_id); ?></td> -->
                      <td><?php echo date('M d,Y h:i A',strtotime($result->program_date)); ?></td>
                      <td><?php  echo date('M d,Y h:i A',strtotime($result->program_end_date)); ?></td>
                      <td><?php  echo $result->location; ?></td>
                      <td>

                      <ul class="edit-list icons-list icon-width">
                        <li>
                        <a href="<?php echo base_url();?>admin/add_programs_category/edit_programs/?program_id=<?php echo base64_encode($result->main_id);?>" >
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        </li>

                        <li>

                          <a href="javascript:void(0);" onclick="return delete_category_data(<?php echo $result->main_id ?>)"  title="Delete"><i class="fa fa-times" aria-hidden="true" style='color:red;'"></i></a>
                        </li>

                        <li>

                        <?php $status_val = $result->status;

                        if($status_val == 'inactive'){ ?>

                        <a class="activate_id" href="<?php echo base_url();?>admin/add_programs_category/activate_program/?program_id=<?php echo base64_encode($result->main_id);?>"><img src="<?php echo base_url();?>assets/images/icons/inactive_program.png" width="23px;" height="23px;" title="active"></a></td>

                        <?php } else { ?>

                         <a class="deactivate_id" href="<?php echo base_url();?>admin/add_programs_category/deactivate_program/?program_id=<?php echo base64_encode($result->main_id);?>"><img src="<?php echo base_url();?>assets/images/icons/active_program.png" width="23px;" height="23px;" title="inactive"></a></td>

                        <?php } ?>

                        </li>

                      </ul>
      
                  </tr>
                  <?php 
                      $j++;
                        }
                      }
                  ?>
                  
              </tbody>
          </table>
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>


<script type="text/javascript">

    function delete_category_data(id)
    {

      if(confirm("Are you Sure! You Want to delete it")){   

        $.ajax({
         type:'POST',
         url:'<?php echo base_url();?>admin/add_programs_category/delete_data_programs',
         data:{"id":id},
         success: function(data)
         {  
            if(data==1){

                 location.reload();
            }
         }
     });

        return true;
      } else{
        
        return false;
      }

    }

    $(".activate_id").click(function (){

      if(confirm('Really you want to active program')){

        return true;
      } else {
        return false;

      }
    });

     $(".deactivate_id").click(function (){

      if(confirm('Really you want to deactive program')){

        return true;
      } else {
        return false;

      }
    });
  </script>