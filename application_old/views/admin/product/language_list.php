 <style type="text/css">
     button {
  height: 43px;
  margin-bottom: 21px !important;
  width: 133px;
}
thead {
  background-color: hsl(224, 13%, 23%);
}
thead th {
  color: hsl(0, 0%, 100%);
  padding: 5px;
}
.edit-list {
  list-style: outside none none;
  padding: 0;
}
.edit-list.icons-list.icon-width {
  width: 100px;
}
.edit-list > li {
  display: inline-block;
}
.icons-list > li {
  padding: 0 2px;
}
 </style>

 <div class="centercontent">
    
      <div class="pageheader notab">
            <h1 class="pagetitle">Language List</h1>
           
            
        </div><!--pageheader-->
        <?php 
        	if($this->session->flashdata('success'))
        	{
        	 ?>
        	 <div class="alert alert-success">
        	 	<?php echo $this->session->flashdata('success'); ?>
        	 </div>
        	 <?php
        	}
        	else if($this->session->flashdata('error'))
        	{
        	 ?>
        	 <div class="alert alert-danger">
        	 	<?php echo $this->session->flashdata('error'); ?>
        	 </div>
        	 <?php
        	} else if($this->session->flashdata('update'))
          {
           ?>
           <div class="alert alert-success">
            <?php echo $this->session->flashdata('update'); ?>
           </div>
           <?php
          }
        ?>
        <div id="contentwrapper" class="contentwrapper">
            <a href="<?php echo base_url();?>admin/add_product/add_product_language"><button class="btn-primary">ADD Language</button></a>

            

             <table class="table-striped table color-table info-table table-bordered table-view" style="margin-bottom: 2rem !important ">
              <thead>
                  <tr>
                      <th>S.No.</th>
                      <th>Language Name</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                 <?php 
                  if(!empty($LanguageData))
                  {
                      $j=1;
                      foreach($LanguageData as $result)
                  {?>
                  <tr>

                      <td><?php echo $j; ?></td>
                      <td><?php  echo $result->product_language_name; ?></td>
                      <td><?php  echo $result->language_status; ?></td>
                      <td> 
                        <ul class="edit-list icons-list icon-width">
                        <li>
                        <a href="<?php echo base_url();?>admin/add_product/edit_product_language/?product_language_id=<?php echo base64_encode($result->product_language_id);?>" >
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        </li>

                        <li>
                          <a href="javascript:void(0);" onclick="return delete_product_language(<?php echo $result->product_language_id ?>)"  title="Delete"><i class="fa fa-times" aria-hidden="true" style='color:red;'"></i></a>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php 
                      $j++;
                        }
                      }
                  ?>
                  
              </tbody>
          </table> 
      
               <!-- <?php echo $content; ?> -->
        </div><!--contentwrapper-->
            
        
	</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->


  
  <script type="text/javascript">
    function delete_product_language(language_id)
    {
        var r = confirm("Are You Sure You Want To Delete");
        if (r == true) {
            $.ajax({
                      type : 'POST',
                      url  : '<?php echo base_url("admin/add_product/delete_product_language")?>',
                      data : {'product_language_id':language_id},
                      success : function(resp)
                      {
                        resp = resp.trim();
                          if(resp == "SUCCESS")
                          {
                              window.location.href = "<?php echo base_url('admin/add_product/language_list')?>";
                          }
                          else
                          {
                              alert("Something Went Wrong");
                          }
                      }


            })
        } else {
            return false;
        }
    }
  </script>


</body>

</html>



