<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">
<style type="text/css">
    .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}
</style>

<div class="centercontent tables">
  <form id="lecture-form" class="stdform" action="<?php echo base_url(); ?>admin/add_semester/add_new_lecture" method="post" enctype="multipart/form-data">
    <div class="pageheader notab">
      <h1 class="pagetitle">Add Topic</h1>
    </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper">
        <!-- <div class="one_half"> -->
        <?php
        if($this->session->flashdata('error'))
        {
        echo $this->session->flashdata('error');
        }
        ?>

        <p>
          <label>Course Name<span style="color:red;">*</span></label>
          <select name="product_id" id="product_id" class="test" onchange="get_chapter_course_wise(this.value);">
            <option value="">--select course name--</option>
            <?php
            if(!empty($course_list))
            {
            foreach($course_list as $chapter)
            {
            ?>
            <option value="<?php echo $chapter['product_id'];?>"><?php echo $chapter['pname'];?></option>
            <?php
            }
            }
            ?>
          </select>
        </p>



        <p>
          <label>Chapter Name<span style="color:red;">*</span></label>
          <select name="chapter_id" id="chapter_id">
            <option value="">--select chapter name--</option>
            
          </select>
        </p>
        <p>
          <label>Topic Type<span style="color:red;">*</span></label>
          <select name="lecture_type" id="lecture_type" required="required">
            <option value="">--select topic type--</option>
            <?php
            if(!empty($lecture_type))
            {
              foreach($lecture_type as $type)
              {
              ?>
              <option value="<?php echo $type['type_id'];?>"><?php echo $type['type_name'];?></option>
              <?php
              }
            }
            ?>
          </select>
          <?php echo form_error('lecture_type','<div class="error_validate">','</div>'); ?>
        </p>
        <p>
          <label>Topic Name<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="lect_name" class="smallinput" id="lect_name" /></span>
          <?php echo form_error('lect_name', '<div class="error_validate">', '</div>'); ?>
        </p>
        <div id="topic_content">
          <p>
            <label>Topic Content<span style="color:red;">*</span></label>
            <input type="file" name="content" id="content">
          </p>
        </div>
        <div id="vdo_content">
          
          <p>
            <label>Topic Video URL<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="lect_video_url" class="smallinput" id="lect_video_url" /></span>
            <?php echo form_error('lect_video_url', '<div class="error_validate">', '</div>'); ?>
          </p>
          <p>
            <label>Topic Video Password<span style="color:red;">*</span></label>
            <span class="field"><input type="text" name="video_pass" class="smallinput" id="video_pass" required="required" /></span>
          </p>
        </div>
        <p>
          <label>Topic Duration(in min)<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="lect_duration" class="smallinput" id="lect_duration" required="required">
        </span>
        <?php echo form_error('lect_duration', '<div class="error_validate">', '</div>'); ?>
      </p>
     

    <p>
    <label>Topic Status</label>
    <select name="lect_status" id="lect_status">
      <option value="active">Active</option>
      <option value="inactive">Inactive</option>
    </select>
    </p>
    <p>
    <label>Description<span style="color:red;">*</span></label>
    <textarea name="lect_description" class="smallinput" id="lect_description" /></textarea>
    </p>


    </div><!--contentwrapper-->

    <div id="chapDiv" style="color:red;"></div>

    <div class="text-center" style="padding-bottom: 20px;">


    <button type="button" class="btn btn-orange" id="addbtn" onclick="return add_topic();">Save</button>
    <a href="<?php echo base_url();?>admin/add_semester/lecture_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" ></a>
    </div>
    <div class="clearfix"></div>

    <!-- <p class="stdformbutton">
    <button class="submit radius2" id="addbtn">Save</button>
    </p> -->
    </div><!-- centercontent -->

</form>

<!------- Including PHP Script here ------>
</div>



</div><!--bodywrapper-->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/admincss/js/plugins/select2/select2.full.min.js"></script> 
<script type="text/javascript">
  CKEDITOR.replace('lect_description');
  CKEDITOR.replace('pfeatures');
</script>


<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>



<script>
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>



<script type="text/javascript">
  // $=jQuery;
  jQuery(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
  jQuery('#lecture_type').on('change',function(){
    var ok= $('#lecture_type').val();
    // alert(ok); 
    if(ok =='1'){
      $('#topic_content').hide();
      $('#vdo_content').show();
    }
    if(ok =='2'){
      $('#vdo_content').hide();
      $('#topic_content').show();

    }

    if(ok == '3'){
        $('#vdo_content').show();
      $('#topic_content').show();
    }
  });

</script>

<script type="text/javascript">
    function get_chapter_course_wise(courseId)
    {
      $.ajax({
                type : 'POST',
                url  : '<?php echo base_url("admin/add_semester/get_chapter_course_wise")?>',
                data : {'course_id':courseId},
                success:function(resp)
                {
                    resp = resp.trim();
                    $('#chapter_id').html(resp);
                }

      })
    }
</script>


   <script type="text/javascript">

   function add_topic()
   {
      
      var course_id  = $('#product_id').val();
      var chapter_id = $('#chapter_id').val();
      var topic_name    = $('#lect_name').val();
      var lect_type  = $('#lecture_type').val();
      var video_url  = $('#lect_video_url').val();
      var content    = $('#content').val();
      var video_pass = $('#video_pass').val();
      var duration   = $('#lect_duration').val();

    

      var flag =0;
     
      if(course_id == "")
      {
           $('#chapDiv').html('Please Select Course ');
            return false;
      }

       else if(chapter_id == "")
      {
        $('#chapDiv').html('Please Select Chapter');
        return false;
      }

      else if(topic_name == "")
      {
           $('#chapDiv').html('Please Enter Topic Name');
             return false;
      }

      else if(lect_type == "")
      {
           $('#chapDiv').html('Please Select Topic Type');
            return false;
      }

      else if(lect_type == "1")
      {
          if(video_url == "")
          {
                $('#chapDiv').html('Please Enter Video Url');
                return false;
          }
          else if(video_pass == "")
          {
                 $('#chapDiv').html('Please Enter Video Password');
                return false;
          }
          else if(duration == "")
          {
                 $('#chapDiv').html('Please Enter Video duration');
                return false;
          }
          else
          {
             flag = 1;
          }
        }

       else if(lect_type == "2")
      {
          if(content == "")
          {
                $('#chapDiv').html('Please Select Image');
                return false;
          }
          else
          {
              flag = 1;
          }
      }

        else if(lect_type == "3")
      {
          if(video_url == "")
          {
                $('#chapDiv').html('Please Enter Video Url');
                return false;
          }
          else if(video_pass == "")
          {
                 $('#chapDiv').html('Please Enter Video Password');
                return false;
          }
          else if(duration == "")
          {
                 $('#chapDiv').html('Please Enter Video duration');
                return false;
          }

          else if(content == "")
          {
                 $('#chapDiv').html('Please Select Image');
                   return false;
          }

          else
          {
             flag = 1;
          }
      }

     if (flag==1) {

             $('#addbtn').prop('disabled', true);


            var form = $('#lecture-form')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);

            $.ajax({
                  type : 'POST',
                  url  : '<?php echo base_url("admin/add_semester/add_new_lecture")?>',
                  data : formData,
                  contentType: false,       // The content type used when sending data to the server.
                  cache: false,             // To unable request pages to be cached
                  processData:false,
                  success:function(resp)
                  {
                      resp = resp.trim();

                      if(resp == "SUCCESS")
                      {
                            $('#lecture-form')[0].reset();
                            $('#chapDiv').css('color','green');
                            $('#chapDiv').html('Lecture Added Successfully');
                            window.location.href="<?php echo base_url('admin/add_semester/lecture_list')?>";
                      }
                      else
                      {
                          $('#chapDiv').html('Something Went Wrong');
                      }
                  }
        });
      }

        
   }
 </script>


</body>

</html>
