<div class="centercontent tables">
  <form id="teach_form" class="stdform" action="<?php echo base_url(); ?>admin/teachers/add_teacher" method="post" enctype="multipart/form-data">
    <div class="pageheader notab">
        <h1 class="pagetitle">Add Teacher</h1>
       
        
    </div><!--pageheader-->
        
    <div id="contentwrapper" class="contentwrapper">
                    
      <p>
        <label>Username<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="username" class="smallinput" id="username" /></span>
      </p>
      <p>
        <label>Password<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="password" class="smallinput" id="password" required="required" /></span>
          <?php echo form_error('password', '<div class="error_validate">', '</div>'); ?>
      </p>
      <p>
        <label>FullName<span style="color:red;">*</span></label>
          <span class="field"><input type="text" name="fullname" class="smallinput" id="fullname" required="required" /></span>
          <?php echo form_error('fullname', '<div class="error_validate">', '</div>'); ?>
      </p>

      <p>
     	  <label>Select Picture<span style="color:red;"></span></label>
     	  <input type="file" name="image_name" id="image_name" value=""  />
      </p>

      <p>
        <label>Mobile<span style="color:red;"></span></label>
        <span class="field"><input type="text" name="mobile" class="smallinput" id="mobile"/></span>

      </p>
      
      <p>
        <label>Teachers Status</label>
        <select name="status" id="status">
          <option value="active">Active</option>
          <option value="inactive">Inactive</option>
        </select>
      </p>

   
    </div><!--contentwrapper-->

    <div class="text-center" style="padding-bottom: 20px;"> 
        <a href="<?php echo base_url();?>admin/teachers/teacher_details"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
     
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
      </div>
      <div class="clearfix"></div>
	
	  <!-- <p class="stdformbutton">
        <button class="submit radius2" id="addbtn">Save</button>
    </p> -->

				
  </form>  
</div>
	



</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('description');
  CKEDITOR.replace('pfeatures');
</script>
<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  /* validation */
  $("#teach_form").validate({
    rules:{
      
      username: {
        required: true,
      },
       password: {
        required: true,
        minlength: 8,
        maxlength: 15
      },
      fullname: {
        required: true,
      },
      mobile: {
        required: true,
        number:true,
        minlength: 10,
        maxlength: 12
      }

    },
    
    messages:{
      username: "Please Enter Username",
      password: "Please Enter Password min 8 & max 15 digit",
      fullname: "Please Enter Full Name",
      mobile: "Please Enter Valid Mobile No."
      

    },
       
  });
</script>

</body>

</html>
