<div class="centercontent tables">
<form class="stdform" action="<?php echo base_url(); ?>admin/add_semester/add_test_type" method="post" enctype="multipart/form-data">
        <div class="pageheader notab">
            <h1 class="pagetitle">Add Test Type</h1>
           
            
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
        	<!-- <div class="one_half"> -->
          <?php 
            if($this->session->flashdata('error'))
            {
              echo $this->session->flashdata('error'); 
            }
           ?>
            <p>
              <label>Test Type<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="test_type" class="smallinput" id="test_type" required/></span>
                <?php echo form_error('test_type', '<div class="error_validate">', '</div>'); ?>
            </p>

            <p style="padding-bottom: 20px;">
              <label>Test Status</label>
              <select name="status" id="status">
                <option value="active">Active</option>
                <option value="inactive">Inactive</option>
              </select>
            </p>

           <!--  </div> -->

        <div class="text-center" style="padding-bottom: 20px;"> 
     
        <button type="submit" class="btn btn-orange" id="addbtn">Save</button>
        
        <a href="<?php echo base_url();?>admin/add_semester/test_type_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" > </a>
      </div>
      <div class="clearfix"></div>


                       
                   
        </div><!--contentwrapper-->
		
  	 <!-- <p class="stdformbutton">
             <button class="submit radius2" id="addbtn">Save</button>
     </p> -->

	</div><!-- centercontent -->
				
  </form>
     
			<!------- Including PHP Script here ------>

		</div>
	



</div><!--bodywrapper-->

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('type_description');
  CKEDITOR.replace('pfeatures');
</script>
<script>
 var catid;
 function get_sub_list(catid)
 {
 	 $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
  				    var result_data=JSON.parse(result);
  				    var result_length=result_data.length;
  				    var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
				    if(result_length>0)
				    {
					   for(var i=0;i<result_length;i++)
					   {
						    cathtml+="<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
						  }
				    }
				  else
				  {
					cathtml+="<option value=''>--select variant--</option>";
				  }
				  $('#sub_category').html(cathtml);
        }
     });
 }
 function get_sub_sub_list(catid)
 {
   $.ajax({
            url: '<?php echo base_url();?>admin/Addservices/get_sub_categ',
            data:{'category_id':catid},
            type: 'POST',
            success: function(result)
            {
              //alert("catid"+result);
              var result_data=JSON.parse(result);
              var result_length=result_data.length;
              var cathtml="";
              cathtml = "<option value=''>--select variant--</option>";
            if(result_length>0)
            {
              
             for(var i=0;i<result_length;i++)
             {
                
                cathtml += "<option value='"+result_data[i]['id']+"'>"+result_data[i]['name']+"</option>";
              }
            }
          else
          {
          cathtml+="<option value=''>--select variant--</option>";
          }
          $('#sub_sub_category').html(cathtml);
        }
     });
 }
</script>

</body>

</html>
