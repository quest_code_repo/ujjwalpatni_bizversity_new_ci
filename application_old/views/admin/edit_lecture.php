<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admincss/css/fSelect.css">
<style type="text/css">
    .fs-wrap.multiple .fs-option.selected .fs-checkbox i {
      background-color: transparent;
      border-color: #777;
      background-image: url(assets/images/correct.png);
      background-repeat: no-repeat;
      background-position: center;
    }
    .fs-wrap{
        width: 432px !important;
    }
    .fs-dropdown {
  width: 40% !important;
}
</style>
<div class="centercontent tables">
  <form id="edit-lecture-form" class="stdform"  method="post" enctype="multipart/form-data">
      <div class="pageheader notab">
        <h1 class="pagetitle">Edit Topic</h1>
      </div><!--pageheader-->
      
      <div id="contentwrapper" class="contentwrapper"> 
        <!-- <div class="one_half"> -->
        <?php 
          if($this->session->flashdata('error'))
          {
            echo $this->session->flashdata('error'); 
          }
         ?>

         <input type="hidden" name="lect_id" value="<?php echo $lecture_detail[0]['lect_id'];?>"    />


         <div class="form-group">
          <label>Course Name</label>
          <select name="product_id" id="product_id" class="test">
          
             <?php 
              if(!empty($course_list))
              {
              foreach($course_list as $each_chapter)
              {

              if($lecture_detail[0]['course_id'] == $each_chapter['product_id'])
              { 
                  ?>
                   <option value="<?php echo $each_chapter['product_id'];?>" selected >
            <?php echo $each_chapter['pname'];?>
            </option>
                  <?php
                 } 
            }
          }
            ?>
          </select>
        </div>

        <div class="form-group">
          <label>Chapter Name</label>
          <select name="ch_id" id="ch_id" >
            <option value="">--select chapter name--</option>
             <?php 
              if(!empty($chapter_list))
              {
              foreach($chapter_list as $each_chapter)
              {
              if($lecture_detail[0]['ch_id']==$each_chapter['ch_id'])
              { 
              $select_id="selected='selected'";
              }
              else
              {
              $select_id="";
              } 
              ?>
            <option value="<?php echo $each_chapter['ch_id'];?>" 
                    <?php echo $select_id;?> >
            <?php echo $each_chapter['ch_name'];?>
            </option>
            <?php
            } 
            }
            ?>
          </select>
        </div>

        <div class="form-group">
          <label>Topic Type</label>
          <select name="lecture_type" id="lecture_type" onchange="change_leture_type();">
            <option value="">--select topic type--</option>
            <?php 
              if(!empty($lecture_type)){
                foreach($lecture_type as $each_lect_type)
                {
              ?>
                <option value="<?php echo $each_lect_type['type_id'];?>" <?= ($lecture_detail[0]['lecture_type']==$each_lect_type['type_id'])?"selected='selected'" : ''  ?> >
                <?php echo $each_lect_type['type_name'];?>
                </option>
                <?php
                } 
              }
            ?>
          </select>
        </div>

        <div class="form-group">
          <label>Topic Name<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="lect_name" class="smallinput" id="lect_name" " value="<?php echo $lecture_detail[0]['lect_name'];?>"  /></span>
               <?php echo form_error('lect_name', '<div class="error_validate">', '</div>'); ?>
        </div>
        <div id="topic_content">
          <p>
            <label>Topic Content<span style="color:red;">*</span></label>
            <input type="file" name="content" id="content">

            <?php
              if(!empty($lecture_detail[0]['lect_resource']))
              {
                  ?>
                  <a href="<?php echo base_url('uploads/semester/'.$lecture_detail[0]['lect_resource'].'')?>">View</a>
                                    <?php
              }
          ?>
          </p>

        </div>
        <div id="vdo_content">
          <div class="form-group">
            <label>Topic Video URL<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="lect_video_url" class="smallinput" id="lect_video_url" value="<?php echo $lecture_detail[0]['lect_video_url'];?>"  /></span>
          </div>

          <div class="form-group">
            <label>Topic Video Password<span style="color:red;">*</span></label>
                <span class="field"><input type="text" name="video_pass" class="smallinput" id="video_pass" value="<?php echo $lecture_detail[0]['video_pass'];?>"  /></span>
          </div>
        </div>

        

        <div class="form-group">
          <label>Topic Duration(in min)<span style="color:red;">*</span></label>
              <span class="field"><input type="text" name="lect_duration" class="smallinput" id="lect_duration" required="required" value="<?php echo $lecture_detail[0]['lect_duration'];?>"  /></span>
               <?php echo form_error('lect_duration', '<div class="error_validate">', '</div>'); ?>
        </div>

        <div class="form-group" style="padding-bottom: 20px;">
          <label>Topic Status</label>
          <select name="lect_status" id="lect_status">
            <option value="active" <?php if($lecture_detail[0]['lect_status']=='active'){ echo $selected_status="selected='selected'";}?>>Active</option>
            <option value="inactive" <?php if($lecture_detail[0]['lect_status']=='inactive'){ echo $selected_status="selected='selected'";}?>>Inactive</option>
          </select>
        </div>
        
        <div class="form-group">
            <h4>Description<span style="color:red;"></span></h4>
            <textarea class="form-control" name="lect_description"  id="lect_description" ><?php echo $lecture_detail[0]['lect_description']; ?></textarea>
        </div>

        <div id="chapDiv" style="color:red;"></div>

        <div class="text-center" style="padding-bottom: 20px;"> 
        <a href="<?php echo base_url();?>admin/add_semester/lecture_list"><input type="button" class="btn btn-orange" style="background-color: orange;color: white;" value="Cancel" ></a> 

        <button type="button" class="btn btn-orange" id="addbtn"  onclick="return add_topic();">Save</button>
      </div>
      <div class="clearfix"></div>
          
          <!-- <div class="form-group text-center">
              <button class="submit radius2" id="editbtn">Save</button>
          </div> -->
                 
      </div><!--contentwrapper-->
  <!-- centercontent -->
  </form>    
</div><!--bodywrapper-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admincss/js/fSelect.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('lect_description');
  CKEDITOR.replace('description');
</script>

<script src="<?php echo base_url(); ?>assets/admincss/js/jquery.validate.min.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
        change_leture_type();
});
</script>

<script>
 (function($) {
        $(function() {
            $('.test').fSelect();
        });
    })(jQuery);
</script>



<script type="text/javascript">
  // $=jQuery;
  jQuery(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
  // jQuery('#lecture_type').on('change',function(){
  //   var ok= $('#lecture_type').val();
  //   // alert(ok); 
  //   if(ok =='1'){
  //     $('#topic_content').hide();
  //     $('#vdo_content').show();
  //   }
  //   if(ok =='2'){
  //     $('#vdo_content').hide();
  //     $('#topic_content').show();

  //   }

  //   if(ok == '3'){
  //       $('#vdo_content').show();
  //     $('#topic_content').show();
  //   }
  // });

  function change_leture_type()
  {
      var ok= $('#lecture_type').val();
   
    if(ok =='1'){
      $('#topic_content').hide();
      $('#vdo_content').show();
    }
    if(ok =='2'){
      $('#vdo_content').hide();
      $('#topic_content').show();

    }

    if(ok == '3'){
        $('#vdo_content').show();
      $('#topic_content').show();
    }
  }

</script>

<script type="text/javascript">
    function get_chapter_course_wise(courseId)
    {
      $.ajax({
                type : 'POST',
                url  : '<?php echo base_url("admin/add_semester/get_chapter_course_wise")?>',
                data : {'course_id':courseId},
                success:function(resp)
                {
                    resp = resp.trim();
                    $('#chapter_id').html(resp);
                }

      })
    }
</script>


   <script type="text/javascript">

   function add_topic()
   {
      
      var course_id  = $('#product_id').val();
      var chapter_id = $('#chapter_id').val();
      var topic_name    = $('#lect_name').val();
      var lect_type  = $('#lecture_type').val();
      var video_url  = $('#lect_video_url').val();
      var content    = $('#content').val();
      var video_pass = $('#video_pass').val();
      var duration   = $('#lect_duration').val();

    

      var flag =0;
     
      if(course_id == "")
      {
           $('#chapDiv').html('Please Select Course ');
            return false;
      }

       else if(chapter_id == "")
      {
        $('#chapDiv').html('Please Select Chapter');
        return false;
      }

      else if(topic_name == "")
      {
           $('#chapDiv').html('Please Enter Topic Name');
             return false;
      }

      else if(lect_type == "")
      {
           $('#chapDiv').html('Please Select Topic Type');
            return false;
      }

      else if(lect_type == "1")
      {
          if(video_url == "")
          {
                $('#chapDiv').html('Please Enter Video Url');
                return false;
          }
          else if(video_pass == "")
          {
                 $('#chapDiv').html('Please Enter Video Password');
                return false;
          }
          else if(duration == "")
          {
                 $('#chapDiv').html('Please Enter Video duration');
                return false;
          }
          else
          {
             flag = 1;
          }
        }

       else if(lect_type == "2")
      {
          if(content == "")
          {
                $('#chapDiv').html('Please Select Image');
                return false;
          }
          else
          {
              flag = 1;
          }
      }

        else if(lect_type == "3")
      {
          if(video_url == "")
          {
                $('#chapDiv').html('Please Enter Video Url');
                return false;
          }
          else if(video_pass == "")
          {
                 $('#chapDiv').html('Please Enter Video Password');
                return false;
          }
          else if(duration == "")
          {
                 $('#chapDiv').html('Please Enter Video duration');
                return false;
          }

          else if(content == "")
          {
                 $('#chapDiv').html('Please Select Image');
                   return false;
          }

          else
          {
             flag = 1;
          }
      }

     if (flag==1) {

               $('#addbtn').prop('disabled', true);

            var form = $('#edit-lecture-form')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);

            $.ajax({
                  type : 'POST',
                  url  : '<?php echo base_url("admin/add_semester/edit_lecture")?>',
                  data : formData,
                  contentType: false,       // The content type used when sending data to the server.
                  cache: false,             // To unable request pages to be cached
                  processData:false,
                  success:function(resp)
                  {
                      resp = resp.trim();

                      if(resp == "SUCCESS")
                      {
                            $('#edit-lecture-form')[0].reset();
                            $('#chapDiv').css('color','green');
                            $('#chapDiv').html('Lecture Updated Successfully');
                            window.location.href="<?php echo base_url('admin/add_semester/lecture_list')?>";
                      }
                      else
                      {
                          $('#chapDiv').html('Something Went Wrong');
                      }
                  }
        });
      }

        
   }
 </script>


</body>

</html>
