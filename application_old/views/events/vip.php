<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>


<section class="section-banner">
    <img src="front_assets/images/banners/banner-vip.jpg" class="img-responsive"/>
</section>
<div class="single-line-gold"></div>

<section class="topics-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="topics-block">
					<h2 class="common-heading gold-heading wow fadeInUp">Topics to be explored</h2>
					<ul class="topics-list">
                        <?php
                        if (!empty($explored_data)) {
                            $i=1;
                            foreach ($explored_data as $value) {
                                $slide = $i%2;
                        ?>

						<li class="topic-<?php echo $i; ?> wow <?php if ($slide==1) { echo "slideInLeft";
                        }else{ echo "slideInRight"; } ?>" ><?php echo $value->content; ?>

                        <?php
                        if(!empty($value->icon)){
                            ?>
                            <img src="<?php echo base_url();?>uploads/<?php echo $value->icon?>">
                        <?php } ?>
                            
                        </li>
                        <?php $i++; } } ?>
					</ul>
				</div>
				<div class="topic-background">
					<img src="front_assets/images/other/topic-back.png">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-register">
    <div class="color-overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <div class="col-md-12 clearfix">
                            <ul>             
                                <li class="event-name seat-report">
                                    Limited seats available for next event
                                </li>
                                <li class="in">
                                    in
                                </li>
                                <li class="place">
                                    <?php if (!empty($program_details)) {
                                            echo $program_details[0]->city_name;
                                        }else{
                                            echo "No Event Here";
                                        }  
                                    ?>
                                </li>
                                <li class="date">
                                    <?php if (!empty($program_details)) {
                                        echo $usable_time = date("M d", strtotime($program_details[0]->program_date));
                                    }
                                    ?>
                                </li>
                                <li class="text-right">
                                    <a href="#Register-sec" class="btn btn-register Registr-Now">REGISTER NOW</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="why-attend-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="why-attent-inner">
					<div class="row">
						<div class="col-sm-4">
							<div class="why-card wow slideInLeft">
								<div class="why-card-relative">
									<h1>why</h1>
								    <h2>should i attend</h2>
								    <img src="front_assets/images/other/vip-logo.png" class="img-responsive">
								</div>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="why-block-content">
								<p>I desire to be <span>super productive</span> with time in my hand</p>

								<p>I want to know how ordinary people into <span>super achievers</span></p>

								<p>I need a better <span>Solution</span> for managing my calls & social media engagement</p>

								<p>I would like to <span>Balance</span> my work life & family life</p>

								<p>I want to <span>Feel special</span> and be <span>acnowledged </span> everywhere</p>
								<p>I wish to <span>conquer</span> my self doubt & procrastination</p>

								<p>I need immediately implementable <span>Practical ideas</span></p>
								<div>
									<img src="front_assets/images/icons/vip-ques.png" class="img-responsive wow slideInRight">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="wht-to-expect">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="wht-expect-content">
                    <div class="row">
                        <div class="col-md-5 col-sm-12 pr-0">
                            <div class="video-block wow slideInLeft">
                                <div class="wht-expect-video ">
                                <?php if (!empty($program_details[0]->video_url)) { ?>
                                    <iframe width="100%" height="280" src="<?php if (!empty($program_details[0]->video_url)) { echo $program_details[0]->video_url; }?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                 <?php }else{ ?>
                                   <iframe width="100%" height="280" src="https://www.youtube.com/embed/u5DV5lLksUg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                               <?php } ?>  
                                
                            </div>

                            <?php
                                if(!empty($program_details[0]->video_url))
                                {
                                    ?>
                                <div class="text-center share-outer">
                                    <a href="javascript:void(0);" id="btn-share1" class="btn btn-common wow pulse">Share Video With Friends</a>
                                </div>

                                    <?php
                                }
                            ?>
                            

                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12 pl-0">
                            <h2 class="common-heading pink-heading wow fadeInUp">Key Information</h2>
                            <div class="row mr-0">
                                <div class="col-md-6  col-sm-12 pl-0">
                                    <div class="expect-rule-list power-list wow slideInRight">
                                        <ul>
                                            <li>Not more than 50 in any case</li>
                                            <li>Delhi, Kolkata, Mumbai, Raipur, you can choose the date and city of your preference</li>
                                            <li>Mostly five star hotels. Previous programs have been conducted at Taj Vivanta Delhi, The Lalit, Mumbai, Radisson Blu, Delhi and Vedic Village, Kolkata.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6  col-sm-12 pl-0">
                                    <div class="expect-rule-list power-list wow slideInRight">
                                        <ul>
                                            <li>Get inside at 9.30 am on the first day and leave at 6 pm on the next day. </li>
                                            <li>Manual in English and session in Hinglish, highly interactive program with down the earth examples, videos and case studies.</li>
                                            <li>The per participant fee includes One night stay in Five star hotel (double occupancy) with all meals, training kit with manual, DVD, Books and stationeries is 39500 plus GST.</li>
                                        </ul>
                                    </div>
                                </div>

                                <?php
                                        if(!empty($BrochureDetails))
                                    {
                                        ?>
                              <div class="text-center share-outer share-2nd">
                                   <a href="javascript:void(0);" class="btn btn-common wow pulse" id="btn-share2">
                                   Share Brochure With Friends</a>
                                </div>
                                        <?php
                                    }
                                ?>
                                
                            </div>


                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row">
            <?php
            if(!empty($program_details[0]->video_url))
            {
            ?>
            <div class="col-sm-6">
                 <div class="social-share" id="social-share1">
                    <ul>
                        <li><a href="https://twitter.com/intent/tweet?url=<?php echo base_url();?><?php echo $program_details[0]->video_url;?>&text=<?php echo $program_details[0]->program_name;?>&via=Ujjwal_Patni"> <img src="front_assets/images/icons/twitter(1).png" class="img-responsive"></a></li>
                        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?><?php echo $program_details[0]->video_url;?>&title=<?php echo $program_details[0]->program_name;?>
&summary=&source=LinkedIn"> <img src="front_assets/images/icons/linkedin.png" class="img-responsive"></a></li>

                    </ul>
                </div>
            </div>
                        <?php
                    }
            ?>
           


            <?php
                    if(!empty($BrochureDetails))
                    {
                        ?>
            <div class="col-sm-6">
                 <div class="social-share" id="social-share2">
                    <ul>
                        <!-- <li><a href=""> <img src="front_assets/images/icons/fb.png" class="img-responsive"></a></li> -->
                        <li><a href="https://twitter.com/intent/tweet?url=<?php echo base_url();?>uploads/program_brochure<?php echo $BrochureDetails[0]->brochure_image;?>&text=<?php echo $BrochureDetails[0]->brochure_name;?>&via=Ujjwal_Patni"> <img src="front_assets/images/icons/twitter(1).png" class="img-responsive"></a></li>
                        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>uploads/program_brochure/<?php echo $BrochureDetails[0]->brochure_name;?>&title=<?php echo $BrochureDetails[0]->brochure_name;?>
&summary=&source=LinkedIn"> <img src="front_assets/images/icons/linkedin.png" class="img-responsive"></a></li>
                    </ul>
                </div>
            </div>
                        <?php
                    }
            ?>

            
        </div>
    </div>
</section>


<!-- Simple Section html -->
<section class="section-upcoming for-events-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-lg-3 col-lg-offset-3 ">
                <p class="wow slideInLeft">DISCOVER MORE UPCOMING EVENTS</p>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <ul id = "myTab" class = "nav nav-tabs nav-select">
                    <li >
                        <h4>Select Event City</h4>
                    </li>
                    <li class = "active">
                        <div class="form-group">
                            <select class="form-control" id="event_cities" onchange="get_event_dates();">
                                <?php
                                if (!empty($program_details)) {
                                    echo "<option value=''>City</option>";
                                    foreach ($program_details as $key => $vip_cities) {
                                ?>
                                    <option value="<?php echo $vip_cities->city_id;?>"><?php echo $vip_cities->city_name?></option>
                                        
                                <?php  } }else{ ?>
                                    <option>No Event Here</option>

                                <?php } ?>
                            </select>

                            <input type="hidden" id="event_dates" value=""   />

                        </div> 
                      
                    </li>
                  
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-3">
                <div class="calendar">
                            <div class="calendar-box">
                                <div class="white-box">
                                    <div class="datepickerTest" style="display: none;"></div>
                                    <div class="dummyTest"></div>
                                    <hr>
                                   <!--  <div class="event-details clearfix">
                                        <span class="event-date"><div>28</div></span>
                                        <span class="event-logo"><img src="front_assets/images/other/vip-logo.png"></span>
                                        <span class="event-place">kolkata</span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-12">
                    <h2 class="common-heading wow fadeInUp">APPRECIATIONS</h2>
                </div>
                <div class="col-sm-12">
                    <div id="owl-testimonial">
                        <?php
                        if (!empty($testimonials)) {
                            foreach ($testimonials as $value) {
                        ?>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                    “<?php echo $value->descriptions;?>”
                                    </div>
                                <div class="img-box">
                                    <img src="<?php echo base_url();?>uploads/testinomial_images/<?php echo $value->testi_image;?>">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name"><?php echo $value->title;?></div>
                            </div>
                        </div>
                        <?php } }  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-form">
	<div id="Register-sec"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <?php
                // if(empty($this->session->userdata('id')))
                // {
                ?>              
                <!-- <div class="col-sm-12">
                    <h2 class="common-heading gold-heading wow fadeInUp">registration</h2>
                    <h4 class="register-down-text text-center">To register, Please <a href="javascript:;" data-toggle="modal" data-target="#register-modal" class="btn btn-register">Login</a></h4>
                </div> -->
                <?php
                //} elseif (empty($program_details)) { 
                ?>
                <!-- <div class="col-sm-12">
                    <h2 class="common-heading gold-heading wow fadeInUp">registration</h2>
                    <h4 class="register-down-text text-center">No Event Here</h4>
                </div> -->
                <?php //}else{  ?>
                    <div class="col-sm-12">
                        <h2 class="common-heading gold-heading wow fadeInUp">registration</h2>
                    </div>

                <form id="vip_register_form" action="javascript:;" method="POST">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>First Name*</label>
                            <input type="text" name="f_name" data-rule-required="true" data-msg-required="Please Enter First Name" class="form-control" value="<?php echo isset($UserDetail[0]->f_name)?$UserDetail[0]->f_name:$this->session->userdata('full_name');?>">
                        </div>
                    </div>
                    <div class="col-sm-6 padd-left">
                        <div class="form-group">
                            <label>Last Name*</label>
                            <input type="text" name="l_name" data-rule-required="true" data-msg-required="Please Enter Last Name" class="form-control" value="<?php echo isset($UserDetail[0]->l_name)?$UserDetail[0]->l_name:'';?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Address*</label>
                            <input type="text" name="address" data-rule-required="true" data-msg-required="Please Enter Address" class="form-control" value="<?php echo isset($UserDetail[0]->address)?$UserDetail[0]->address:'';?>">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Pincode*</label>
                            <input type="text" name="pincode" data-rule-required="true" data-msg-required="Please Enter Pincode" class="form-control descPriceNumVal" value="<?php echo isset($UserDetail[0]->pincode)?$UserDetail[0]->pincode:'';?>" minlength="6" maxlength="6">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Phone number*</label>
                            <input type="text" name="phone" data-rule-required="true" data-msg-required="Please Enter Phone Number"   class="form-control descPriceNumVal" value="<?php echo isset($UserDetail[0]->mobile)?$UserDetail[0]->mobile:'';?>" minlength="10" maxlength="10">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>E-mail*</label>
                            <input type="email" name="username" data-rule-required="true" data-msg-required="Please Enter Email" class="form-control" value="<?php echo isset($UserDetail[0]->username)?$UserDetail[0]->username:$this->session->userdata('username');?>">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="sel1">City*</label>
                            <select class="form-control" name="city_name" id="sel1" data-rule-required="true" data-msg-required="Please Select City">
                                <option value="">Select your city</option>
                                <?php
                                if (!empty($all_cities)) {
                                    foreach ($all_cities as $value) {
                                        if (!empty($UserDetail[0]->city)) {
                                            if ($UserDetail[0]->city == $value->city_id) {
                                                $selected = "selected";
                                            }else{
                                                $selected = "";
                                            }
                                        }else{
                                            $selected = "";
                                        }
                                ?>
                                <option value="<?php echo $value->city_id; ?>" <?php echo $selected; ?> ><?php echo $value->city_name; ?></option>

                                <?php  }  } ?>
                            </select>
                        </div> 
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="sel1">Select Program City*</label>
                            <select class="form-control" name="event_city" id="event_city" data-rule-required="true" data-msg-required="Please Select Program City" onchange="get_events_date_by_city(this.value);">
                            <option value="">Select</option>
                            <?php
                            if (!empty($program_details)) {
                                foreach ($program_details as $key => $events_cities) {

                                    if (!empty($this->session->userdata('sess_program_city'))) {
                                        if ($this->session->userdata('sess_program_city') == $events_cities->city_ids) {
                                            $selected = "selected";
                                        }else{
                                            $selected = "";
                                        }
                                    }else{
                                        $selected = "";
                                    }
                            ?>
                                <option value="<?php echo $events_cities->city_ids; ?>" <?php echo $selected; ?>><?php echo $events_cities->city_name; ?></option>
                            <?php  } } ?>
                            </select>
                            <input type="hidden" id="event_category_id" value="1">
                        </div> 
                    </div>    

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="sel1">Date Of Program*</label>
                            <select class="form-control" name="program_date" id="program_date" data-rule-required="true" data-msg-required="Please Select Date Of Program" onchange="get_events_details_by_date(this.value);">
                                <?php if (!empty($this->session->userdata('sess_program_date'))) { ?>
                                <option value="<?php echo $this->session->userdata('sess_program_date'); ?>"><?php echo $this->session->userdata('sess_program_date'); ?></option>
                                <?php }else{ ?>
                                <option value="">Select Program Date</option> 
                                <?php } ?>
                            </select>                            
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="sel1">Program Name*</label>
                            <input type="text" name="program_name" id="program_name" class="form-control" readonly="readonly" value="<?php if (!empty($this->session->userdata('sess_program_name'))) { echo $this->session->userdata('sess_program_name'); } ?>" placeholder="Program Name">
                            <input type="hidden" name="programes_id" id="programes_id" value="<?php if (!empty($this->session->userdata('sess_program_id'))) { echo $this->session->userdata('sess_program_id'); } ?>">             
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="event-tickit-amt">
                            <p>Amount</p>
                            <h4 id="actual_price"> 
                            ₹
                            <?php 
                            if (!empty($this->session->userdata('session_price'))) {
                                echo $this->session->userdata('session_price');
                            }elseif (!empty($program_details)) {
                                echo $program_details[0]->price; 
                            }else{
                                echo "0";
                            }
                            ?>
                            </h4>
                            <input type="hidden" id="programe_price" name="programe_price" value="<?php if (!empty($this->session->userdata('session_price'))) {
                                echo base64_encode($this->session->userdata('session_price'));
                            }elseif (!empty($program_details)) { echo $program_details[0]->price; } ?>">
                            <input type="hidden" name="qty" value="<?php echo '1'; ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox form-group">
                            <label class="p-0" >
                                <input value="0" name="terms_condition" id="terms_condition" data-rule-required="true" data-msg-required="Please Select Terms Condition" type="checkbox">
                                <span class="cr"><i class="cr-icon fa fa-check" onclick="terms_condition()" aria-hidden="true"></i></span>
                                I agree to the Terms & Conditions
                                <span class="error" id="error_tc"></span>
                            </label>
                        </div>
                    </div>
                  
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <span class="error" id="error_msg_lgin"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="loader-event" style="display: none;">
                                <img src="front_assets/images/loader.gif">
                            </div>
                        </div>
                        <div class="form-btn-box add-cart add-cart-proceed">
                            <button type="submit" class="btn btn-register" id="add_to_card">Add to Cart and proceed to checkout&nbsp; &nbsp; &nbsp; &nbsp;</button>
                        </div>
                    </div>
                </form>
                <?php //} ?>
            </div>
        </div>
    </div>
</section>   


<!-- Simple Section html -->
<section class="product-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 product-block">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <div>
                           <h3 class="wow slideInLeft">Products</h3>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                      <div class="wow zoomIn">
                        <img src="front_assets/images/icons/product1.png" class="img-responsive">
                        <h4>Training</h4>
                      </div>    
                    </div>
                    <div class="col-sm-2 col-xs-6">
                      <div class="wow zoomIn">
                        <img src="front_assets/images/icons/product2.png" class="img-responsive">
                        <h4>Books</h4>
                      </div>    
                    </div>
                    <div class="col-sm-2 col-xs-6">
                      <div class="wow zoomIn">
                        <img src="front_assets/images/icons/product3.png" class="img-responsive">
                        <h4>CDs</h4>
                      </div>    
                    </div>
                    <div class="col-sm-2 col-xs-6">
                      <div class="wow zoomIn">
                        <img src="front_assets/images/icons/product14.png" class="img-responsive">
                        <h4>Merchandise</h4>
                      </div>    
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <a href="mentoring/coming_soon" button class="btn btn-register">Buy Now</button></a>
                    </div>
                </div>
                <div class="single-line"></div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-program">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <h2 class="common-heading wow fadeInUp">EXPLORE MORE SIGNATURE PROGRAMMES</h2>
                </div>
                <div class="col-md-12">
                    <div id="owl-program">
                       <!--  <div class="item">
                            <div class="pro-box vip">
                                <div class="white-box">
                                    <img src="front_assets/images/other/vip-logo.png">
                                    <div class="txt">
                                    A TWO DAY AND ONE NIGHT RESIDENTIAL TRAINING PROGRAM
                                    </div>
                                    <div class="text-center mt-20"><a href="">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    PERSONAL PRODUCTIVITY AND BUSINESS SUCCESS HABITS
                                </div>
                                <a href="" class="btn btn-common">REGISTER</a>
                            </div>
                        </div> -->
                        <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/excellence-logo.png">
                                    <div class="txt">
                                    A ONE YEAR COMPACT PERSONAL AND BUSINESS COURSE OF MULTIPLE SESSIONS
                                    </div>
                                    <div class="text-center mt-20"><a href="<?php echo base_url('excellence-gurukul')?>">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    PERSONAL, BUSINESS & PUBLIC EXCELLENCE
                                </div>
                                <a href="<?php echo base_url('excellence-gurukul')?>" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                        <!-- <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/keynote.png">
                                    <div class="txt">
                                    EQUALLY FLUENT IN BOTH HINDI AND ENGLISH
                                    </div>
                                    <div class="text-center mt-20"><a href="">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    INVITE DR.UJJWAL PATNI TO KICKSTART YOUR CONFERENCE  OR ANNUAL MEETING
                                </div>
                                <a href="" class="btn btn-common">REGISTER</a>
                            </div>
                        </div> -->
                        <div class="item">
                            <div class="pro-box excellence">
                                <div class="white-box">
                                    <img src="front_assets/images/other/pioneer.png">
                                    <div class="txt">
                                    A PROGRAMME DESIGNED FOR COUPLES WITH KIDS
                                    </div>
                                    <div class="text-center mt-20"><a href="<?php echo base_url('power-parenting');?>">Know more...</a></div>
                                </div>
                                <div class="gradient-box">
                                    SPECIAL SKILLS TO RAISE AND NURTURE EXTRAORINARY CHILDREN
                                </div>
                                <a href="<?php echo base_url('power-parenting');?>" class="btn btn-common">REGISTER</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<script src="<?php ?>front_assets/developer_validate/jquery.validate.min.js"></script>
<script src="<?php ?>front_assets/developer_validate/custom_validate.js"></script> 
<style type="text/css">
    .error{ color: red;}
</style>
<script type="text/javascript">
    // $(document).ready(function (){
    //     $('#vip_register_form').validate({
    //         onfocusout: function(element) {
    //             this.element(element);
    //         },
    //         errorClass: 'error_validate',
    //         errorElement:'span',
    //         highlight: function(element, errorClass) {
    //            $(element).removeClass(errorClass);
    //         }
    //     });
    // });

    function terms_condition(){
       
        var terms_condition = $("#terms_condition").val();
        if (terms_condition=='0') {
            $("#terms_condition").val('1');
        }else{
            $("#terms_condition").val('0');
        }        
    }

    function check_term(){
        var terms_condition = $("#terms_condition").val();
        if (terms_condition=='0') {
            // alert('Please Select Terms And Conditions');
            $("#error_tc").show();
            $("#error_tc").text('Please Select Terms And Conditions');
            return false;
        }else{
            $("#error_tc").hide();
            return true;
        }
    }
</script>

<!-- End -->
<!-- ************************* CART *******************************-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#add_to_card').click(function(){

            $('#vip_register_form').validate({
                onfocusout: function(element) {
                    this.element(element);
                },
                errorClass: 'error_validate',
                errorElement:'span',
                highlight: function(element, errorClass) {
                    $(element).removeClass(errorClass);
                },

                submitHandler:function(form)
                {
                    var terms_condition = $("#terms_condition").val();
                    if (terms_condition == '0') {
                        $("#error_tc").show();
                        $("#error_tc").text('Please Select Terms And Conditions');
                        return false;
                    }else{
                        $("#error_tc").hide();
                        // return true;
                    }

                    $('.loader-event').css('display','block');

                    $.ajax({
                        type :'POST',
                        dataType: 'json',
                        url  :'<?php echo base_url("events/programe_ajax_submit")?>',
                        data : $('#vip_register_form').serialize(),
                        success:function(resp)
                        {
                            if(resp.msg == "first_login"){
                                $("#error_msg_lgin").text('You are already registered with us, Please login');
                                $("#redirect_url").val('vip');
                                setTimeout(function() {
                                    $('#register-modal').modal('show');
                                }, 3000);
                                // alert($("#redirect_url").val());
                                return false;
                            }
                            if(resp.msg == "SUCCESS"){
                                
                                window.location.href = "<?php echo base_url('my-cart') ?>";
                            }
                           
                        } 
                    });
                }
            });
        });
    });
</script>

<script type="text/javascript">
    function get_event_dates()
    {
        var e_cities = $("#event_cities option:selected").val();
        var event_type = 1;

        $.ajax({
                    type : 'POST',
                    url  : '<?php echo base_url("events/get_dates_acc_to_city")?>',
                    data : {'e_city':e_cities,'event_type':event_type},
                    success:function(resp)
                    {
                        $('.datepickerTest').datepicker('destroy');
                        $('#event_dates').val(resp);
                        set_values();
                        $('.dummyTest').css('display','none');
                        $('.datepickerTest').css('display','block');
                        $( ".datepickerTest" ).datepicker();
                    }
        })
        
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function () {
	$(".Registr-Now").on('click', function (e) {
	    e.preventDefault();
	    var target = $(this).attr('href');
	    $('html, body').animate({
	        scrollTop: ($(target).offset().top)
	    }, 1500);
	});
});
</script>
