<!DOCTYPE html>
<html>
<head>
	<title>Event Booking</title>
</head>
<body>
<div style='width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;'>
	<div style='background-color: #D55220;'>
		<img src='<?php echo base_url();?>front_assets/logo.png' style='height: 55px;'>
	</div>
	<div style='font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px'>
		<p>Dear <?php echo $this->session->userdata('full_name'); ?>,</p>
		<P>Thanks for registring for the Event <b><?php echo $details[0]->item_name; ?></b>. Your booking id is <?php echo $product_details[0]->order_code?></p>
	<p>Below are your registration details.</p>
	<table>
		<tr>
			<th style='width: 190px;height: 30px;'>Event Location:</th>
			<td><?php echo $details[0]->location; ?></td>
		</tr>
		<tr>
			<th style='width: 190px;height: 30px;'>Event DateTime:</th>
			<td><?php echo date('d-M-y H:i',strtotime($details[0]->program_date)); ?></td>
		</tr>
		<tr>
			<th style='width: 190px;height: 30px;'>Event EndTime:</th>
			<td><?php echo date('d-M-y H:i',strtotime($details[0]->program_end_date)); ?></td>
		</tr>
		<!-- <tr>
			<th style="width: 190px;height: 30px;">Event Date:</th>
			<td>date</td>
		</tr> -->
		<tr>
			<th style='width: 190px;height: 30px;'>Quantity:</th>
			<td><?php echo $product_details[0]->order_products; ?></td>
		</tr>
		<tr>
			<th style='width: 190px;height: 30px;'>Price</th>
			<td>₹ <?php echo $product_details[0]->order_amount; ?>/-</td>
		</tr>
		<tr>
			<th style='width: 190px;height: 30px;'>Total Price:</th>
			<td>₹ <?php echo $product_details[0]->order_products * $product_details[0]->order_amount; ?></td>
		</tr>
	</table>
		<p style='margin:25px 0 5px; '>Thanks!</p>
		<p style='margin:5px 0;'>Regards,</p>
		<p style='margin:5px 0;'>Team Bizversity</p>
	</div>
</div>

</body>
</html>