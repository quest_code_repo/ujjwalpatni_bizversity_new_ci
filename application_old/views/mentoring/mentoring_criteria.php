
<section class="section-banner">
    <img src="front_assets/images/banners/banner3.png" class="img-responsive"/>
    <div class="single-line"></div>
</section>

<section class="topics-section mentoring">
    <div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="common-heading wow fadeInUp">topics to be explored</h2>
                    </div>
                    <div class="col-sm-6">
                        <div class="topics-block first">

                            <ul class="topics-list">
                        <?php
                        if (!empty($MentoringExp)) {
                            $i=1;
                            foreach ($MentoringExp as $value) {
                                $slide = $i%2;
                        ?>

                        <li class="topic-<?php echo $i; ?> wow <?php if ($slide==1) { echo "slideInLeft";
                        }else{ echo "slideInRight"; } ?>" ><?php echo $value->content; ?>

                        <?php
                        if(!empty($value->icon)){
                            ?>
                            <img src="<?php echo base_url();?>uploads/<?php echo $value->icon?>">
                        <?php } ?>
                            
                        </li>
                        <?php $i++; } } ?>
                    </ul>
                        </div>
                    </div>
                    
                    <?php
                        if(!empty($MentoringProposal))
                        {
                            ?>  
                    <div class="col-sm-12">
                        <div class="text-center mt-30">
                            <a href="<?php echo base_url('uploads/mentoring_proposal/'.$MentoringProposal[0]->mentoring_proposal_url.'')?>" class="btn btn-big" download>DOWNLOAD MENTORING PROPOSAL NOW</a>
                        </div>
                    </div>
                            <?php
                        }
                    ?>
                    
                </div>
			</div>
		</div>
	</div>
</section>

<section class="section-register mentoring">
    <div class="color-overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <div class="col-md-9 col-sm-12 clearfix">
                           <div class="text-uppercase method1">
                               i want dr.ujjwal patni to mentor my business and
                           </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <a href="<?php echo base_url('contact-us')?>" class="btn btn-register">contact me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="why-attend-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="why-attent-inner">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="why-card">
                                <div class="why-card-relative menter-card-relative">
                                    <h1>why</h1>
                                <h2>My Business Needs A</h2>
                                <h3>Mentor</h3>
                                <!--  <img src="assets/images/other/vip-logo.png" class="img-responsive"> -->
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="why-block-content mentor-content">
                                <p>I desire to be <span>super productive</span> with time in my hand</p>

                                <p>I want to know how ordinary people into <span>super achievers</span></p>

                                <p>I need aq better <span>Solution</span> for managing my calls & social media engagement</p>

                                <p>I would like to <span>Balance</span> my work life & family life</p>

                                <p>I want to <span>Feel special</span> and be <span>acnowledged </span> everywhere</p>
                                <p>I wish to <span>conquer</span> my self doubt & procrastination</p>

                                <p>I need immediately implementable <span>Practical ideas</span></p>
                                <div>
                                    <img src="front_assets/images/icons/mentor-que.png" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Simple Section html -->
<section class="section-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-12">
                    <h2 class="common-heading wow fadeInUp">APPRECIATIONS</h2>
                </div>
                <div class="col-sm-12">
                    <div id="owl-testimonial">
                        <?php
                        if (!empty($testimonials)) {
                            foreach ($testimonials as $value) {
                        ?>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                    “<?php echo $value->descriptions;?>”
                                </div>
                                <div class="img-box">
                                    <img src="<?php echo base_url();?>uploads/testinomial_images/<?php echo $value->testi_image;?>">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name"><?php echo $value->title;?></div>
                            </div>
                        </div>
                        <?php } }  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->
<section class="section-form">
    <div class="container">
        <div class="row">

        <form id="mentoring_form" name="mentoring_form" method="post">
            <div class="col-md-8 col-md-offset-2">
                <div class="col-sm-12">
                    <h2 class="common-heading wow fadeInUp">request a session</h2>
                </div>

                <input type="hidden" name="mentoring_class_id" value="<?php echo $CurrentMentoring[0]->id;?>" />

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Name of Organization*</label>
                        <input type="text"  id="name_of_organization" name="name_of_organization" class="form-control" data-rule-required="true" data-msg-required="Please Enter Name Of Organization">
                    </div>
                </div>
                <div class="col-sm-6 padd-left">
                    <div class="form-group">
                        <label>No. of people*</label>
                        <input type="text" id="no_of_people" name="no_of_people" class="form-control" data-rule-required="true" data-msg-required="Please Enter No Of people">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Address*</label>
                        <input type="text" id="mentoring_address" name="mentoring_address" class="form-control" data-rule-required="true" data-msg-required="Please Enter Address">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>City*</label>
                        <select id="mentoring_city" name="mentoring_city" class="form-control" data-rule-required="true" data-msg-required="Please Select City">
                            <option value="">Select City</option>
                            <?php
                                if(!empty($Cities) && count($Cities)>0)
                                {
                                    foreach($Cities as $cityDetails)
                                    {
                                        ?>
                                        <option value="<?php echo $cityDetails->city_name;?>"><?php echo $cityDetails->city_name;?></option>
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                    <option value="">No Record Found</option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 padd-left">
                    <div class="form-group">
                        <label>Pincode*</label>
                        <input type="text" name="mentoring_pincode" name="mentoring_pincode" class="form-control" minlength="6" maxlength="6" data-rule-required="true" data-msg-required="Please Enter Pimcode">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Name of Contact Person*</label>
                        <input type="text" id="mentoring_contact_person" name="mentoring_contact_person" class="form-control" data-rule-required="true" data-msg-required="Please Enter Name of Contact Person">
                    </div>
                </div>
                <div class="col-sm-6 padd-left">
                    <div class="form-group">
                        <label>Surname of Contact Person*</label>
                        <input type="text" id="mentoring_contact_sur_person" name="mentoring_contact_sur_person" class="form-control" data-rule-required="true" data-msg-required="Please Enter Surname of Contact Person">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Phone number*</label>
                        <input type="text" id="mentoring_phone_no" name="mentoring_phone_no" class="form-control" minlength="10" maxlength="10" data-rule-required="true" data-msg-required="Please Enter Phone No">
                    </div>
                </div>
                <div class="col-sm-6 padd-left">
                    <div class="form-group">
                        <label>E-mail*</label>
                        <input type="email" id="mentoring_email" name="mentoring_email" class="form-control" data-rule-required="true" data-msg-required="Please Enter Email">
                    </div>
                </div>
              
                <div class="col-md-12">
                    <div class="checkbox form-group">
                        <label class="p-0">
                            <input  type="checkbox" name="mentoring_check" id="mentoring_check">
                            <span class="cr"><i class="cr-icon fa fa-check" aria-hidden="true"></i></span>
                            I agree to the Terms & Conditions
                        </label>
                    </div>
                </div>

                <div id="mentoringDiv"></div>
             
                <div class="col-md-12">
                    <div class="form-btn-box">
                        <input type="submit" class="btn btn-register" id="mentoring_register" value="submit"  />
                    </div>
                </div>
            </div>
     </form>
        </div>
    </div>
</section>

<!-- Simple Section html -->
<section class="product-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 product-block">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <div>
                           <h3 class="wow slideInLeft">Products</h3>
                        </div>
                    </div>
                     <?php
                        if(!empty($product_category) && count($product_category)>0)
                        {
                            foreach($product_category as $proDetails)
                            {
                                  ?>
                                   <div class="col-sm-2 col-xs-6">
                                      <div class="wow zoomIn">
                                        <img src="front_assets/images/icons/product1.png" class="img-responsive">
                                        <h4><?php echo $proDetails->name;?></h4>
                                       </div>    
                                    </div>
                                  <?php
                            }
                            ?>

                                 <div class="col-sm-2 col-xs-12">
                                       <a href="product" button class="btn btn-register">Buy Now</button></a>
                                  </div>

                                  <?php
                        }
                        else
                        {
                            ?>
                            <h4>No Product Category Found</h4>
                            <?php
                        }

                    ?>
                   
                </div>
                <div class="single-line"></div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<script type="text/javascript">
        $(document).ready(function(){
        $('#mentoring_register').click(function(){
        $('#mentoring_form').validate({
        onfocusout: function(element) {
        this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {

          if($('input[name=mentoring_check]:checked').length<=0)
          {
              $('#mentoringDiv').css('color','red');
              $('#mentoringDiv').html('Please Check Terms And Condition');
              return false;

          }

            $.ajax({
                      type :'POST',
                      url  :'<?php echo base_url("mentoring/new_mentoring_data")?>',
                      data : $('#mentoring_form').serialize(),
                      success:function(resp)
                      {
                        resp = resp.trim();
                          if(resp == "SUCCESS")
                          {
                              $('#mentoringDiv').show();
                              $('#mentoringDiv').css('color','green');
                              $('#mentoringDiv').text('You Have Successfully Requested A Quote, We will contact you soon');
                              $("#mentoring_form")[0].reset();

                               setTimeout(function () {
                         window.location.href= '<?php echo current_url();?>'; // the redirect goes here

                              },2000);
                          }
                         
                          else
                          {
                              $('#mentoringDiv').show();
                              $('#mentoringDiv').css('color','red');
                              $('#mentoringDiv').text('Something Went Wrong');
                          }
                      } 


            });
            
            
         }
        });
        });
    });
</script>