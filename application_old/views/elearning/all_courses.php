<section class="about-us-section all-program-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="common-heading wow fadeInUp">All Courses</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
         <div class="right-side-list inside" id="sidebar" >

                          <ul class=" list-menu">
                                 <li><h4 class="sub-heading">CATEGORIES</h4></li>

                               
                                 <?php
                                      if(!empty($elearning_categories) && count($elearning_categories)>0)
                                      {
                                        $i = 1;
                                          foreach($elearning_categories as $catData)
                                          {
                                            if($i == 1 && ($this->uri->segment('3') == ""))
                                            {
                                              
                                              ?>
                                             <li class="list-group-item active"><a href="elearning/courses/<?php echo $catData->category_url;?>"><?php echo $catData->name;?></a></li>
                                            <?php
                                            }
                                            elseif($this->uri->segment('3') == $catData->category_url)
                                            {
                                                ?>
                                                 <li class="list-group-item active"><a href="elearning/courses/<?php echo $catData->category_url;?>"><?php echo $catData->name;?></a></li>
                                                <?php
                                            }

                                            else
                                            {
                                                ?>
                                                  <li class="list-group-item "><a href="elearning/courses/<?php echo $catData->category_url;?>"><?php echo $catData->name;?></a></li>
                                                <?php
                                            }

                                              $i++;
                                        } 

                                      }
                                 ?>
                                

                                 <li class="list-heading">
                                   <div class="program-checks">
                                      <div class="checkbox">

                                        <?php
                                              if($this->uri->segment('4') == "")
                                              {
                                                  $checked  = "";
                                                  $checked1 = "";
                                              }
                                              elseif($this->uri->segment('4') == "Business")
                                              {
                                                  $checked  = "checked";
                                                  $checked1 = "";
                                              }
                                              elseif($this->uri->segment('4') == "Individual")
                                              {
                                                  $checked  = "";
                                                  $checked1 = "checked";
                                              }
                                        ?>

                                        <label> <input type="radio" name="course_for" value="Business" onclick="get_filter_data(this);" <?php echo $checked;?>> Business</label>
                                      </div>
                                      <div class="checkbox">
                                        <label> <input type="radio" name="course_for" value="Individual" onclick="get_filter_data(this);" <?php echo $checked1;?>> Individuals</label>
                                      </div>
                                   </div>
                                 </li>
                         </ul>
                </div>
      </div>
      <div class="col-sm-9">
         <div class="main_content" id="content">
         <div class="full-select-section">
           <div class="row">
             <div class="col-sm-4 col-sm-offset-8">
              <form id="search_form" action="elearning/search_course" method="post">
               <div class="input-group">
                  <input type="hidden" class="product_id" name="product_id" value=""   />
                  <input type="hidden" class="product_url" name="product_url" value=""   />

                      <input class="form-control course_info" placeholder="Search" type="text" id="pname" value="" onkeyup="course_autocomplete();">
                      <span class="input-group-btn">
                         <button class="btn btn-default" type="submit">
                             <i class="fa fa-search"></i>
                         </button>
                      </span>
                </div>
              </form>  
             </div>
           </div>
           <div class="row">

           <!-- <span class="col-sm-11 select-block" data-reactid="114">   
             <span class="heading-select">Active Filter : </span>
              <div class="select-program-block">
                  <div class="select-text">French</div>
                  <div class="select-cross">
                    <div class="select-close-x">×</div>
                </div>
             </div>
             <div class="select-program-block">
                  <div class="select-text">English</div>
                  <div class="select-cross">
                    <div class="select-close-x">×</div>
                </div>
             </div>
              </span> -->

             <!--  <span class="col-sm-1 language-popup">
                  <button type="button" class="filter-btn" data-toggle="modal" data-target="#myModal"><img class="c-fb-filter-modal-button-img" src="<?php echo base_url();?>front_assets/images/icons/filter-modal-button.jpg"></button>
                    <!- Modal -->
                   <!--  <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog">  -->
                      
                        <!-- Modal content-->
                    <!--     <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title sub-heading">Refine Your Search</h4>
                          </div>
                          <div class="modal-body">
                             <div class="row">
                                 <div class="col-sm-6">
                                   <div class="modal-checks">
                                      <div class="checkbox">
                                    <label><input type="checkbox"> English</label>
                                  </div>
                                  <div class="checkbox">
                                    <label><input type="checkbox"> Russian</label>
                                  </div>
                                  <div class="checkbox">
                                    <label><input type="checkbox"> Chinese (Simplified)</label>
                                  </div>
                                  <div class="checkbox">
                                    <label><input type="checkbox"> Spanish</label>
                                  </div>
                                  <div class="checkbox">
                                    <label><input type="checkbox"> French</label>
                                  </div>
                                  <div class="checkbox">
                                    <label><input type="checkbox"> Russian</label>
                                  </div>
                                   </div>
                                  <div class="row">
                                    <div class="show-more-link">
                                       <a href="">Show More<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                   <div class="modal-checks">
                                     <div class="checkbox">
                                    <label><input type="checkbox"> Hindi</label>
                                  </div>
                                  <div class="checkbox">
                                    <label><input type="checkbox"> Vietnamese</label>
                                  </div>
                                  <div class="checkbox">
                                    <label><input type="checkbox"> Italian</label>
                                  </div> 
                                   </div>
                                </div>
                             </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-register" data-dismiss="modal">Update Results</button>
                          </div>
                        </div>
                        
                      </div>
                    </div> -->
             <!--  </span> -->
         </div>
         </div>

          
            <?php
                if(!empty($elearning_products) && count($elearning_products)>0)
                {
                    foreach($elearning_products as $eproDetails)
                    {
                        ?>
                  <div class="content-block">
                       <div class="row">
                         <div class="col-sm-3">
                           <img src="<?php echo base_url('uploads/products/'.$eproDetails->image_url.'')?>" class="img-responsive">
                         </div>
                         <div class="col-sm-9">
                           <div>
                            <!--  <h3>VIP</h3> -->
                             <h4 class="sub-heading"><?php echo $eproDetails->pname;?></h4>

                             <?php
                              $string =  $eproDetails->short_disc;
                              $final_string = word_limiter($string, 20);
                             ?>
                             <p><?php echo $final_string;?></p>
                             <div class="text-right">
                               <a href="<?php echo base_url('elearning/course_detail/'.$eproDetails->url.'')?>"><button class="btn  btn-register">Read More</button></a>
                             </div>
                           </div>
                         </div>
                         </div>
                  </div>
                        <?php
                    }
                }
                else
                {
                    ?>
                    <h3>No Record Found</h3>
                    <?php
                }
            ?>
            
              
            
              
           
            <div class="text-right">
             <ul class = "pagination">
                <?php echo $links;?>
              </ul>
            </div>
          </div>
        
        </div>
      </div>
    </div>
  </div>
</section>

 <script type="text/javascript" src="assets/js/stickySidebar.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script>
    $(document).ready(function() {
  
      $('#sidebar').stickySidebar({
        sidebarTopMargin: 70,
        footerThreshold: 100
      });
    
    });
</script>


<script type="text/javascript">

    function course_autocomplete(){
       jQuery('.course_info').autocomplete({
            source: '<?php echo base_url()?>elearning/get_course_autocomplete?pname='+$('#pname').val(),
            minLength: 1,
            select: function(event, ui) {

              jQuery('.course_info').val(ui.item.pname); 
              jQuery('.product_id').val(ui.item.product_id);
              jQuery('.product_url').val(ui.item.url); 

            }
        });
    }
</script>

<script type="text/javascript">
    function get_filter_data(obj)
    {
         var href       =  $("#sidebar li.active").find("a").attr('href');
         var course_for =  $("input[name='course_for']:checked").val(); 

          var redirect_url = "<?php echo base_url();?>"+href+"/"+course_for;
            window.location.href = redirect_url;
         
    }
</script>