<style type="">
  .price-cut{
  text-decoration: line-through; font-size: 15px;
}
.register-down-text{
  text-align: center;
      margin-bottom: 30px !important;
}
</style>
<section class="section-banner">
    <img src="front_assets/images/banners/Vip Banner-01.jpg" class="img-responsive"/>
</section>
<div class="single-line-gold"></div>

<section class="topics-section about-us-section  what-lern-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<div class="topics-block">
					<h2 class="common-heading  wow fadeInUp">What Will I Learn?</h2>
					<ul class="topics-list">
						<li class=" wow slideInLeft"><!-- <span><img src="front_assets/images/icons/done-tick.png"></span> --><?php echo $e_learning_product[0]->short_disc;?></li>
					</ul>
				</div>
				
			</div>
             <div class="col-sm-4">
                <div class="right-side-block">
                    <div class="video-block">
                               <div class="content">
                                <a href="" data-toggle="modal" data-target="#myModal-course"><img src="front_assets/images/icons/youtube.png"></a>
                                <h5>Sneak Preview of the Course</h5>
                               </div>
                          
                          <!-- Modal -->
                          <div id="myModal-course" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                
                                <div class="modal-body">
                                    <button type="button" class="close" onclick="pauseVid()" data-dismiss="modal">
                                      <img src="front_assets/images/icons/delete(1).png">
                                    </button>
                                <!--    <video id="Video1" controls  style="width: 100%; height: auto;">
                                     <source src="front_assets/video/ujjwal-patni.mp4" type="video/mp4" />           
                                 </video> -->

                                 <?php
                                      $url = $e_learning_product[0]->video_url;
                                      $vedio_id = str_replace('http://vimeo.com/','',$url);

                                      $embedurl = "http://player.vimeo.com/video/".$vedio_id;
                                 ?>

                                 <iframe src="<?php echo $e_learning_product[0]->video_url;?>" width="500" height="240" frameborder="0" allowfullscreen="allowfullscreen">
                                  </iframe>

                                </div>
                               
                              </div>

                            </div>
                          </div>
                          </div>
                <div class="courses-price">

                    <p>
                      <?php
                          if($e_learning_product[0]->disc_price != 0)
                          {
                              ?>
                                <span  class="price-cut"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $e_learning_product[0]->product_price;?></span>&nbsp;<span ><i class="fa fa-inr" aria-hidden="true"></i><?php echo $e_learning_product[0]->disc_price;?></span>
                              <?php
                          }
                          else
                          {
                              ?>
                              <span><i class="fa fa-inr" aria-hidden="true"></i><?php echo $e_learning_product[0]->product_price;?></span>
                              <?php
                          }
                      ?>
                      
                    </p>


                    
                     
                      <div>
                        <h4 class="sub-heading">Includes:</h4>
                        <ul>
                        <li><img src="assets/images/icons/in1.png"><span><?php echo $product_meta[0]->duration_hours;?> hours on-demand video</span></li>
                        <li><img src="assets/images/icons/in2.png"><span><?php echo $product_meta[0]->articles;?> Articles</span></li>
                        <li><img src="assets/images/icons/in3.png"><span><?php echo $product_meta[0]->supplemental_resources;?> Supplemental Resources</span></li>
                        <?php
                            if($product_meta[0]->full_time_access == "Yes")
                            {
                                ?>
                                <li><img src="assets/images/icons/in4.png"><span>Full lifetime access</span></li>
                                <?php
                            }
                        ?>
                        <?php
                             if($product_meta[0]->access_on_mobile == "Yes")
                            {
                                ?>
                                <li><img src="assets/images/icons/in5.png"><span>Access on mobile and TV</span></li>
                                <?php
                            }
                        ?>  

                          <?php
                             if($product_meta[0]->completion_certificate == "Yes")
                            {
                                ?>
                              <li><img src="assets/images/icons/in6.png"><span>Certificate of Completion</span></li>
                                <?php
                            }
                        ?>  
                        
                       
                      </ul>
                      </div>
                </div>
                
              
                <?php
                    if(!empty($Overallrating) && count($Overallrating)>0)
                    {
                          $rating  = round($Overallrating[0]->overall_avg);
                        ?>
                  <div class="Customer-rating">
                     <h4 class="sub-heading">Customer Rating</h4>
                        <span class="fa fa-star <?php echo ($rating == "1")?'checked':''?>"></span>
                        <span class="fa fa-star <?php echo ($rating == "2")?'checked':''?>"></span>
                        <span class="fa fa-star <?php echo ($rating == "3")?'checked':''?>"></span>
                        <span class="fa fa-star <?php echo ($rating == "4")?'checked':''?>"></span>
                        <span class="fa fa-star <?php echo ($rating == "5")?'checked':''?>"></span>

                          <?php
                              if($rating != 0)
                              {
                                  ?>
                                    <span><?php echo $rating;?> (<?php echo number_format($Overallrating[0]->rating_id);?> ratings)</span>
                                  <?php
                              }
                          ?>
                  </div>
                        <?php
                    }
                        ?>

                </div>
            </div>
		</div>
	</div>
</section>

<section class="about-us-section course-discription">
    <div class="container">
       <div class="his-lagecy-block">   
        <div class="row">
            <div class="col-sm-8">
                <h2 class="common-heading wow fadeInUp">Description</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8"> 
                     <div class="intro-block wow slideInLeft">
                          <p><?php echo isset($e_learning_product[0]->description)?$e_learning_product[0]->description:''?></p>
                     
                     </div>
                  </div>
                  
              </div>
        </div>
    </div>
</section>

<!-- Simple Section html -->
<section class="section-form">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

              <?php
                  if(empty($this->session->userdata('id')))
                  {
                      ?>
                      <div class="col-sm-12">
                          <h2 class="common-heading wow fadeInUp">Registration</h2>
                          <h4 class="register-down-text">To register, Please <a href="javacsript:void(0)" data-toggle="modal" data-target="#register-modal" class="btn btn-register">Login</a></h4>
                      </div>

                      <?php
                  }

                  else
                  {
                      ?>

             <form id="elearning_registration_form" name="elearning_registration_form" method="post">      


                <input type="hidden" name="product_type" value="<?php echo $e_learning_product[0]->product_type;?>"  />
                <input type="hidden" name="product_id" value="<?php echo $e_learning_product[0]->product_id;?>"  />
 
                <div class="row">
                   <div class="col-sm-6">
                    <div class="form-group">
                        <label>Name*</label>
                        <input type="text" id="f_name" name="f_name" class="form-control" value="<?php echo isset($UserDetail[0]->f_name)?$UserDetail[0]->f_name:$this->session->userdata('full_name')?>" data-rule-required="true" data-msg-required="Please Enter First Name">
                    </div>
                </div>
                <div class="col-sm-6 padd-left">
                    <div class="form-group">
                        <label>Last Name*</label>
                        <input type="text" id="l_name" name="l_name" class="form-control" data-rule-required="true" data-msg-required="Please Enter Last Name" value="<?php echo isset($UserDetail[0]->l_name)?$UserDetail[0]->l_name:''?>">
                    </div>
                </div>
                </div>
               <div class="row">
                   <div class="col-md-12">
                    <div class="form-group">
                        <label>Address*</label>
                        <input type="text" id="address" name="address" class="form-control" data-rule-required="true" data-msg-required="Please Enter Address" value="<?php echo isset($UserDetail[0]->address)?$UserDetail[0]->address:''?>">
                    </div>
                </div>
               </div>
                <div class="row">
                   <div class="col-sm-6">
                    <div class="form-group">
                        <label>City*</label>

                         <select class="form-control" id="city" name="city" data-rule-required="true" data-msg-required="Please Select City">
                              <?php
                                  if(!empty($Cities) && count($Cities)>0)
                                  {
                                      foreach($Cities as $cityDetails)
                                      {

                                          if($UserDetail[0]->city == $cityDetails->city_name)
                                          {
                                              ?>
                                               <option value="<?php echo $cityDetails->city_name;?>" selected><?php echo $cityDetails->city_name;?></option>
                                              <?php
                                          }
                                          else
                                          {
                                              ?>
                                                  <option value="<?php echo $cityDetails->city_name;?>" ><?php echo $cityDetails->city_name;?></option>
                                              <?php
                                          }
                                      }
                                  }
                              ?>
                          
                          
                          </select>

                    </div>
                </div>
                <div class="col-sm-6 padd-left">
                    <div class="form-group">
                        <label>Pincode*</label>
                        <input type="text" id="pincode"  name="pincode" class="form-control" data-rule-required="true" data-msg-required="Please Enter Pincode" value="<?php echo isset($UserDetail[0]->pincode)?$UserDetail[0]->pincode:''?>">
                    </div>
                </div>
                </div>
               
               <div class="row">
                   <div class="col-sm-6">
                    <div class="form-group">
                        <label>Phone number*</label>
                        <input type="text" id="mobile_no" name="mobile_no" class="form-control descPriceNumVal" data-rule-required="true" data-msg-required="Please Enter Mobile No" minlength="10" maxlength="13" value="<?php echo isset($UserDetail[0]->mobile)?$UserDetail[0]->mobile:''?>">
                    </div>
                </div>
                <div class="col-sm-6 padd-left">
                    <div class="form-group">
                        <label>E-mail*</label>
                        <input type="text" name="emailId" class="form-control" value="<?php echo $this->session->userdata('username');?>" data-rule-required="true" data-msg-required="Please Enter Email Id">
                    </div>
                </div>
               </div>
                   
                  
               <div class="row">
                    <div class="col-sm-6">
                    <div class="form-group">
                        <label>Course Purchase For* &nbsp;</label>
                    </div>
                    <div class="form-group">
                         <label class="radio-inline"><input name="course_for" type="radio" value="Individual" onclick="reset_qty();">Individual</label>
                        <label class="radio-inline"><input name="course_for" type="radio" value="Group" onclick="reset_qty();">Group</label>
                    </div>
                </div>

                   <div class="col-sm-6 padd-left Individual" style="display: none">
                     <div class="form-group">
                          <label for="sel1">Number Of Members*</label>
                          <select class="form-control" id="no_of_member" name="qty" onchange="reset_product_price();">
                            <option value="1" selected>1</option>
                          </select>
                        </div> 
                   
                    </div>

                 <div class="col-sm-6 padd-left Group">
                     <div class="form-group">
                          <label for="sel1">Number Of Members*</label>
                          <select class="form-control" id="no_of_member" name="qty" onchange="reset_product_price();">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                          </select>
                        </div> 
                   
                </div>

               </div>
                
                <div class="col-sm-12">
                    <div class="event-tickit-amt">
                        <p>Amount</p>
                        <h4>
                          <?php
                              if($e_learning_product[0]->disc_price !=0)
                              {
                                  ?>
                                  <span class="price-cut"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $e_learning_product[0]->product_price;?></span> <span id="final_price"><?php echo $e_learning_product[0]->disc_price;?></span>
                                  <?php
                              }
                              else
                              {
                                  ?>
                                  <span id="final_price"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $e_learning_product[0]->product_price;?></span>
                                  <?php
                              }
                          ?>
                          </h4>
                    </div>
                </div>
                
                <?php
                    if($e_learning_product[0]->disc_price !=0)
                    {
                        ?>
                         <input type="hidden" id="total_price" name="product_price" value="<?php echo $e_learning_product[0]->disc_price;?>"  />
                          
                        <?php
                    }
                    else
                    {
                        ?>
                         <input type="hidden" id="total_price" name="product_price" value="<?php echo $e_learning_product[0]->product_price;?>"  />
                        <?php
                    }
                ?>

               

               <!--  <div class="col-md-12">
                    <div class="checkbox form-group">
                        <label class="p-0">
                            <input value="" type="checkbox">
                            <span class="cr"><i class="cr-icon fa fa-check" aria-hidden="true"></i></span>
                            Remember me
                        </label>
                    </div>
                </div> -->


                <div class="col-md-12">
                    <div class="checkbox form-group">
                        <label class="p-0">
                            <input  type="checkbox" name="terms_condition" value="0">
                            <span class="cr"><i class="cr-icon fa fa-check" aria-hidden="true"></i></span>
                            I agree to the Terms & Conditions
                        </label>
                    </div>
                </div>


               <!--  <div class="col-md-12">
                    <p>Details: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit. Amet, consequat nec ipsum. Maecenas consectetur, odio vel aliquam molestie, nunc diam.Blandit nisl, a placerat felis massa in ante. Donec quis metus magna. Fusce sed ullamcorpe.</p>
                </div> -->


                <div id="elearningDiv"></div>

                <?php
                    $UserId = $this->session->userdata('id');

                    if(!empty($UserId))
                    {
                        $itemExists = check_item_in_cart_user($e_learning_product[0]->product_id,$UserId,'e_learning');

                        if($itemExists == "true")
                        {
                            ?>
                            <div class="col-md-12">
                                <div class="form-btn-box add-cart">
                                    <button type="button" class="btn btn-register">Added to Cart</button>
                                </div>
                              </div>
                            <?php
                        }
                        else
                        {
                            ?>
                             <div class="col-md-12">
                                <div class="form-btn-box add-cart">
                                    <button type="submit" class="btn btn-register" id="cartAdd">Add to Cart</button>
                                </div>
                              </div>

                            <?php
                        }
                    }
                ?>

               


            </form>

                      <?php
                  }
              ?>
                

               
            </div>
        </div>
    </div>
</section>

<!-- Simple Section html -->
<section class="product-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 product-block">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <div>
                           <h3 class="wow slideInLeft">Products</h3>
                        </div>
                    </div>

                    <?php
                        if(!empty($product_category) && count($product_category)>0)
                        {
                            foreach($product_category as $proDetails)
                            {
                                  ?>
                                   <div class="col-sm-2 col-xs-6">
                                      <div class="wow zoomIn">
                                        <img src="<?php echo base_url('uploads/category/'.$proDetails->cat_img.'')?>" class="img-responsive">
                                        <h4><?php echo $proDetails->name;?></h4>
                                       </div>    
                                    </div>
                                  <?php
                            }
                            ?>

                                 <div class="col-sm-2 col-xs-12">
                                       <a href="product" button class="btn btn-register">Buy Now</button></a>
                                  </div>

                                  <?php
                        }
                        else
                        {
                            ?>
                            <h4>No Product Category Found</h4>
                            <?php
                        }

                    ?>

                   
                  
                   
                </div>
                <div class="single-line"></div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->

<!-- Simple Section html -->

       <?php
                  if(!empty($Related_courses) && count($Related_courses)>0)
                  {
                    ?>
<section class="section-program">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <h2 class="common-heading wow fadeInUp">Relevant Courses</h2>
                </div>
                <div class="col-md-12">
                    <div id="owl-program">

                              <?php
                                foreach($Related_courses as $relDetails)
                                {
                                      ?>
                                         <div class="item">
                                              <div class="pro-box vip">
                                                  <div class="white-box">
                                                        <img src="<?php echo base_url('uploads/product/'.$relDetails->image_url.'');?>"     />
                                                      <div class="txt">
                                                            <?php echo $relDetails->short_disc;?>
                                                      </div>
                                                      <!-- <div class="text-center mt-20"><a href="mentoring/coming_soon">Know more...</a></div> -->
                                                  </div>
                                                   <div class="gradient-box">
                                                      <?php echo $relDetails->pname;?>
                                                  </div> 
                                                  <a href="elearning/course_detail/<?php echo $relDetails->url;?>" class="btn btn-common">REGISTER</a>
                                              </div>
                                        </div>
                                      <?php
                                }
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

          <?php
           }
      ?>

                     

<section class="section-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-12">
                    <h2 class="common-heading  wow fadeInUp">What Our Customers Are Saying</h2>
                </div>
                <div class="col-sm-12">
                    <div id="owl-testimonial">
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit amet, consequat nec ipsum. Maecenas consectetur, vel aliquam molestie, nunc diam blandit nisl, a placerat felis ante. ”
                                    </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/person1.jpg">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name">MR. RAHUL AGARWAL,<span>CEO Zen Enterprise</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit amet, consequat nec ipsum. Maecenas consectetur, vel aliquam molestie, nunc diam blandit nisl, a placerat felis ante. ”
                                </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/person2.jpg">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name">MR. RAHUL AGARWAL,<span>CEO Zen Enterprise</span></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="txt">
                                “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu nisl, lacinia a semper sit amet, consequat nec ipsum. Maecenas consectetur, vel aliquam molestie, nunc diam blandit nisl, a placerat felis ante. ”
                                </div>
                                <div class="img-box">
                                    <img src="front_assets/images/other/person3.jpg">
                                </div>
                                <div class="figure1">
                                    <img src="front_assets/images/other/figure1.png">
                                </div>
                                <div class="figure2">
                                    <img src="front_assets/images/other/figure2.png">
                                </div>
                                <div class="name">MR. RAHUL AGARWAL,<span>CEO Zen Enterprise</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Section html end -->


<script>
var vid = document.getElementById("Video1");
    function pauseVid() { 
    vid.pause(); 
} 
</script>


<style type="text/css">
  .error_validate{color: red;}
</style>

  
<script type="text/javascript">
  function reset_product_price()
  {
      var members =  $('#no_of_member').val();
      var price   =  $('#total_price').val();

       total_price = (members) * (price);
      $('#final_price').text(total_price);
     

  }
</script>


<script src="https://player.vimeo.com/api/player.js"></script>


  <!-- ************************* CART *******************************-->


        <script type="text/javascript">
        $(document).ready(function(){
        $('#cartAdd').click(function(){
        $('#elearning_registration_form').validate({
          onfocusout: function(element) {
          this.element(element);
        },
        errorClass: 'error_validate',
        errorElement:'span',
        highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
        },
         submitHandler:function(form)
         {

         if($('input[name=course_for]:checked').length<=0)
          {
              $('#elearningDiv').css('color','red');
              $('#elearningDiv').html('Please Check Course Purchase For');
              return false;

          }

           if($('input[name=terms_condition]:checked').length<=0)
          {
              $('#elearningDiv').css('color','red');
              $('#elearningDiv').html('Please Check Terms and Condition');
              return false;

          }

            $.ajax({
                      type :'POST',
                      url  :'<?php echo base_url("Cart/cart/add_new_elearning_items_to_cart")?>',
                      data : $('#elearning_registration_form').serialize(),
                      success:function(resp)
                      {
                        resp = resp.trim();
                          if(resp == "SUCCESS")
                          {
                              $('#elearningDiv').show();
                              $('#elearningDiv').css('color','green');
                              $('#elearningDiv').text('Item Added To Cart Successfully');
                              setTimeout(function () {
                         window.location.href= '<?php echo base_url("Cart/cart/my_cart")?>'; // the redirect goes here

                              },2000); 
                          }                        
                          else
                          {
                              $('#elearningDiv').show();
                              $('#elearningDiv').css('color','red');
                              $('#elearningDiv').text('Something Went Wrong');
                          }
                      } 
            });
            
            
         }
        });
        });
    });
</script>


<script type="text/javascript">
      function reset_qty()
      {
          var course_for = $('input[name=course_for]:checked').val();

          if(course_for == "Individual")
          {
              $('.Individual').css('display','block');
               $('.Group').css('display','none');
          }
          else
          {
              $('.Group').css('display','block');
              $('.Individual').css('display','none');
          }

      }
</script>