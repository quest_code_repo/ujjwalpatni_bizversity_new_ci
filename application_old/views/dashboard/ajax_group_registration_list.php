<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="user-cours-list wow slideInLeft">
           <nav class="nav-sidebar">
              <ul class="nav tabs">
                <li ><a href="my-courses">My Courses</a></li>
                <li><a href="completed-course">Completed Courses</a></li>
                <li><a href="my-result">My Results</a></li>
                <li class="active"><a href="purchased-courses">Manage Group Users</a></li>
                <li><a href="group-result">My Group Results</a></li>
              </ul>
            </nav>
        </div>
      </div>
      <div class="col-sm-9">
      	 <div class="group-registration-section commn-head-adjust">
      	 	  <div class="row">
		 	 <div class="col-sm-12">
		 	 	<div>
		 	 		<h2 class="common-heading wow fadeInUp">Group Registration</h2>
		 	 		<h4 class="sub-heading">Select Course</h4>
		 	 	
		 	 		 <div class="row">
		 	 		 	<div class="col-sm-3">
		 	 		 		<div class="form-group">
							  <select class="form-control first" >
							   		
							   		<?php
							   				if(!empty($LicensedCourses) && count($LicensedCourses)>0)
							   				{
							   					foreach($LicensedCourses as $courseData)
							   					{
							   					    	?>
							   					    	<option value="<?php echo $courseData->product_id;?>" selected><?php echo $courseData->pname;?></option>
							   					    	
							   					    	<?php
							   					    }
							   				}
							   			
							   		?>
							  </select>
					        </div> 
		 	 		 	</div>
		 	 		 </div>
					 <p>Users Registration Left: <?php echo isset($final_quantity)?$final_quantity:'0'?></p>
					<!--  <h4 class="sub-heading">Enrolled User Details</h4>
					 <div class="row">
					 	<! <div class="col-sm-2">
					 		<div class="form-group">
					 			  <label for="sel1">Show Users:</label>
								  <select class="form-control second" id="sel1">
								    <option>1</option>
								    <option>2</option>
								    <option>3</option>
								    <option>4</option>
								  </select>
							 </div> 
					 	</div> -->
					 <!-- 	<div class="col-sm-3 col-sm-offset-7 ">
					 		<div class="blog-search">
		                          <form method="post" action="">
		                              <div class="input-group">
		                                  <input class="form-control" placeholder="Search" type="text">
		                                  <span class="input-group-btn">
		                                     <button class="btn btn-default" type="button">
		                                         <i class="fa fa-search"></i>
		                                     </button>
		                                  </span>
		                               </div>
		                          </form>
		                         </div>
					 	     </div>
					    </div> -->


					    <?php
					    	if(!empty($GroupUsers) && count($GroupUsers))
					    	{
					    		?>
					   <div class="table-responsive">
					      	<table class="table table-bordered registered ">
						    <thead>
						      <tr>
						        <th>First Name</th>
						        <th>Last Name</th>
						        <th>Email</th>
						      </tr>
						    </thead>
						    <tbody>
						   <?php
						   			foreach($GroupUsers as $grpDetails)
						   			{
						   				?>
									   	 <tr>
									        <td><?php echo $grpDetails->f_name;?></td>
									        <td><?php echo $grpDetails->l_name;?></td>
									        <td><?php echo $grpDetails->username;?></td>
									      </tr>
						   				<?php

						   			}
						   ?>
					    	  
						     
						    </tbody>
						  </table>
					      </div>
					    		<?php
					    	}
					    ?>

					      
						 

					    <form id="add_user_form" name="add_user_form" method="post">

					    	<input type="hidden" id="course_id" name="course_id" value="<?php echo isset($CourseId)?$CourseId:'0'?>"   />

					    	<input type="hidden" id="licence_no" name="licence_no" value="<?php echo isset($licence_no)?$licence_no:'0'?>"   />

						  <div class="table-responsive">
						  	<table class="table table-bordered new-registration">
						    <thead>
						      <tr>
						        <th>Firstname</th>
						        <th>Lastname</th>
						        <th>Email</th>
						       
						      </tr>
						    </thead>
						    <tbody>
						      <tr>
						        <td><input type="text" name="alloted_user_first_name" id="alloted_user_first_name" class="form-control" placeholder="First Name"></td>
						        <td><input type="text" name="alloted_user_last_name" id="alloted_user_last_name" class="form-control" placeholder="Last Name"></td>
						        <td><input type="email" name="alloted_user_email" id="alloted_user_email" class="form-control" placeholder="Email"></td>
						      </tr>
						    
						    </tbody>
						  </table>
						  </div>

						  <div id="newuserDiv" style="color: red;"></div>
						
						  <button type="button" class="btn btn-register" onclick="add_new_user();">Add User</button>

						</form>
		 	 	</div>
		 	 </div>
		 </div>
      	 </div> 
      </div>
   </div>
  </div>
 </section>	


 <script type="text/javascript">
 	function get_course_id()
 	{
 		var course_id = $('#course_id').find(":selected").val();

 		
 		if(course_id!= "" || course_id == undefined)
 		{
 			 var redirect_url = "<?php echo base_url();?>dashboard/group_registration?course_id="+course_id;
		     window.location.href = redirect_url;
 		}
 		else
 		{
 			window.location.href="<?php echo base_url('dashboard/group_registration')?>";
 		}
 	}
 </script>

<script type="text/javascript">

    function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };

	function add_new_user()
	{
		var first_name = $('#alloted_user_first_name').val();
		var last_name  = $('#alloted_user_last_name').val();
		var new_email  = $('#alloted_user_email').val();

		if(first_name == "")
		{
			$('#newuserDiv').html('Please Enter First Name');
		}
		else if(last_name == "")
		{
			$('#newuserDiv').html('Please Enter Last Name');
		}
		else if(new_email == "")
		{
			$('#newuserDiv').html('Please Enter Email');
		}

		else if (!ValidateEmail($('#alloted_user_email').val())) 
		{
            $('#newuserDiv').html('Please Enter valid Email Address');
        }

        else
        {
        	$.ajax({
        				type :'POST',
        				url  :'<?php echo base_url("dashboard/add_new_user_to_group")?>',
        				data : $('#add_user_form').serialize(),
        				success:function(resp)
        				{
        					resp = resp.trim();

        					if(resp == "SUCCESS")
        					{
        						 $('#newuserDiv').css('color','green');
        						 $('#newuserDiv').html('Course Allocated To User Successfully');
        						 $('#add_user_form')[0].reset();

        						  setTimeout(function () {
                         window.location.href= '<?php echo current_url();?>'; // the redirect goes here

                              },2000);
        					}

        					else if(resp == "QUANTITY_OVER")
        					{
        						$('#newuserDiv').html('You Have No More Quantities To Assign');
        					}

        					else if(resp == "MIN_QTY")
        					{
        						$('#newuserDiv').html('You Have Purchased Only 1 Quantity'); 
        					}

        					else
        					{
        						$('#newuserDiv').html('Something Went Wrong'); 
        					}

        				}

        	});
        }
	}
</script>