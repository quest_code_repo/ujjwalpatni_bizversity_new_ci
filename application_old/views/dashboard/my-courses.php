<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="user-cours-list wow slideInLeft">
          <nav class="nav-sidebar">
            <ul class="nav tabs">
              <li class="active"><a href="my-courses">My Courses</a></li>
              <li><a href="completed-course">Completed Courses</a></li>
              <li><a href="my-result">My Results</a></li>
              <li><a href="purchased-courses">Manage Group Users</a></li>
              <li><a href="group-result">My Group Results</a></li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="col-sm-9">
          <div class="my-courses-section commn-head-adjust">
            <div class="row">
                 <div class="col-sm-12">
                     <h2 class="common-heading wow fadeInUp">My Courses</h2>
                 </div>
            </div>
      </div>
      <div class="all-Courses-section">
        <div class="row">
          <?php
          if (!empty($user_cources)) {
             foreach ($user_cources as $cource) {
          ?>
          <div class="col-sm-5 col-sm-offset-1">
            <a href="cource-details?product_id=<?php echo base64_encode($cource->product_id);?>">
              <div class = "panel panel-default">
                 <div class = "panel-heading">
                    <img src="<?php echo base_url();?>uploads/products/<?php echo $cource->image_url;?>" class="img-responsive">
                 </div>
                 <div class = "panel-body">
                  <h4 class="title"><?php echo $cource->pname; ?></h4>
                  <p><?php
                    $string = strip_tags($cource->short_disc);
                    if (strlen($string) > 80) {
                      // truncate string
                      $stringCut = substr($string, 0, 80);
                      $endPoint = strrpos($stringCut, ' ');
                      //if the string doesn't contain any space then it will cut without word basis.
                      $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                    }
                    echo $string;
                    ?></p>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70"
                           aria-valuemin="0" aria-valuemax="100" style="width:70%">
                        </div>
                      </div>
                      <div>
                        <span class="progress-report">65% complete</span>
                      </div>

                      <?php
                          $starRating = get_star_rating_for_my_courses($cource->product_id);

                          if(!empty($starRating))
                          {
                              $rating = round($starRating[0]->overall_avg);
                              ?>  
                      <div>                           
                          <span class="fa fa-star <?php echo ($rating == "1")?'checked':''?>"></span>
                          <span class="fa fa-star <?php echo ($rating == "2")?'checked':''?>"></span>
                          <span class="fa fa-star <?php echo ($rating == "3")?'checked':''?>"></span>
                          <span class="fa fa-star <?php echo ($rating == "4")?'checked':''?>"></span>
                          <span class="fa fa-star <?php echo ($rating == "5")?'checked':''?>"></span>
                      </div>
                              <?php
                          }
                          else
                          {
                              ?>
                        <div>                           
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                      </div>
                              <?php
                          }
                      ?>
                      

                </div>
           </div>
            </a>
          </div>
          <?php }
        }
        else
        {
           ?>
              <h2>No Record Found</h2>
           <?php            
        }
        ?>
        </div>
      </div> 
      </div>
    </div>
  </div>
</section>

