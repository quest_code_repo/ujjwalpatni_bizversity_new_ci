<?php
                                  if(!empty($Chapter) && count($Chapter)>0)
                                  {
                                    $i=1;
                                      foreach($Chapter as $chapResult)
                                      {

                                          $chapterLecture = get_chapter_lecture_acc_to_course($chapResult->ch_id,$chapResult->sem_id);

                                          ?>
                            <div class="tab-pane active" id="tab1"><div class="tab-content-2">
                                 <button class="heading accordion">
                                    <span class="pull-left">
                                    <h5 class="text-left">Section: <?php echo $i;?></h5>  
                                    <h4><?php echo $chapResult->ch_name;?></h4>
                                    </span>
                                    <span class="pull-right">

                                      <?php

                                          $total_lectures = get_total_lecture_of_particular_chapter($chapResult->ch_id,$chapResult->sem_id);
                                      ?>

                                      <h5><b><?php echo isset($total_lectures[0]->total_lectures)?$total_lectures[0]->total_lectures .'/'. $total_lectures[0]->total_lectures:'0/0'?></b></h5>
                                    </span>
                                 
                                 </button>

                                 <div class="content panel" style="display: none;">

                                    <?php
                                        if(!empty($chapterLecture) && count($chapterLecture)>0)
                                        {

                                            $j = 1;
                                            foreach($chapterLecture as $lectDetails)
                                            {
                                                  if($lectDetails->lecture_type == 1)
                                                  {
                                                      ?>
                                                    <li class="active">
                                                        <a href="<?php echo base_url('dashboard/video_course/'.$chapResult->ch_id.'/'.$lectDetails->lect_id.'/'.$chapResult->sem_id.'')?>">
                                                        <span><i class="fa fa-play" aria-hidden="true"></i></span>
                                                        <span><?php echo $j;?>. <?php echo $lectDetails->lect_name;?></span>
                                                        <span class="pull-right">
                                                           <span><?php echo $lectDetails->lect_duration;?></span>
                                                           <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                                        </span> 
                                                         </a>
                                                      </li>
                                                      <?php
                                                     
                                                  }

                                                  else if($lectDetails->lecture_type == 3)
                                                  {
                                                      ?>
                                                <li>
                                                   <a href="">
                                                      <span><i class="fa fa-play" aria-hidden="true"></i></span>
                                                      <span><?php echo $j;?>. <?php echo $lectDetails->lect_name;?></span>
                                                      </a>
                                                      <span class="pull-right">
                                                         <span><?php echo $lectDetails->lect_duration;?></span>
                                                         <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                                      </span>
                                                 </li>
                                                 <li class="sub-list">
                                                   <a href="">
                                                      <p><i class="fa fa-download" aria-hidden="true"></i>&nbsp;After Effects Course Resources.zip</p>
                                                   </a>
                                                 </li>


                                                      <?php
                                                      $j++;
                                                  }
                                            }
                                        }

                                        else
                                        {
                                              ?>
                                                <h3>No Lecture Found</h3>
                                              <?php
                                        }
                                    ?>
                                  

                                </div>


                             </div>
                           </div>
                                          <?php
                                          $i++;
                                      }
                                  }
                              ?>


<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
   acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
           panel.style.display = "none";
       } else {
           panel.style.display = "block";
       }
   });
}
</script>