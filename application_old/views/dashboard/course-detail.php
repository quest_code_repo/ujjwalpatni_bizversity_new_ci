 
<section class="course-main border-course about-us-section course-overlay">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div>
          <img src="<?php echo base_url();?>uploads/products/<?php echo $cource_details[0]->image_url; ?>" class="img-responsive">
        </div>
      </div>
      <div class="col-sm-8">
        <div class="common-heading">
          <h4><?php echo $cource_details[0]->pname?></h4>
            <p>
              <?php
                  if(!empty($StarRating) && count($StarRating)>0)
                  {

                      ?>
                         <div class="starRating1">
                          <div>
                            <div>
                                <div>
                                  <div>
                                      <input id="rating6" type="radio"  value="1" <?php echo ($StarRating[0]->rating_star == "1")?'checked':'';?>>
                                      <label for="rating6"><span></span></label>
                                  </div>
                                  <input id="rating7" type="radio"  value="2" <?php echo ($StarRating[0]->rating_star == "2")?'checked':'';?>>
                                  <label for="rating7"><span></span></label>
                                </div>
                                <input id="rating8" type="radio"  value="3" <?php echo ($StarRating[0]->rating_star == "3")?'checked':'';?>>
                                <label for="rating8"><span></span></label>
                            </div>
                            <input id="rating9" type="radio"  value="4" <?php echo ($StarRating[0]->rating_star == "4")?'checked':'';?>>
                            <label for="rating9"><span></span></label>
                          </div>
                          <input id="rating10" type="radio"  value="5" <?php echo ($StarRating[0]->rating_star == "5")?'checked':'';?>>
                          <label for="rating10"><span></span></label>
                      </div> 
                      <?php
                  }
                  else
                  {
                      ?>
                        <div class="starRating1">
                          <div>
                            <div>
                                <div>
                                  <div>
                                      <input id="rating6" type="radio"  value="1">
                                      <label for="rating6"><span></span></label>
                                  </div>
                                  <input id="rating7" type="radio"  value="2">
                                  <label for="rating7"><span></span></label>
                                </div>
                                <input id="rating8" type="radio" nam value="3">
                                <label for="rating8"><span></span></label>
                            </div>
                            <input id="rating9" type="radio"  value="4">
                            <label for="rating9"><span></span></label>
                          </div>
                          <input id="rating10" type="radio" value="5">
                          <label for="rating10"><span></span></label>
                      </div> 
                     
                      <?php
                  }
              ?>
           


            <span class="edit-rating"><a href=""  data-toggle="modal" data-target="#myModalrating">Edit Your Rating</a></span>
          </p>

            <!-- Modal -->
            <div class="modal fade" id="myModalrating" role="dialog">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title heading-main">Rating</h4>
                  </div>

                <form id="rating_form" name="rating_form" action="<?php echo base_url('dashboard/new_star_rating')?>" method="post">
                  <input type="hidden" name="course_id" value="<?php echo $cource_details[0]->product_id; ?>"  />
                   <div class="modal-body clearfix rating-modal text-center">
                      <div class="starRating1">
                          <div>
                            <div>
                                <div>
                                  <div>
                                      <input id="rating6" type="radio" name="rating" value="1">
                                      <label for="rating6"><span></span></label>
                                  </div>
                                  <input id="rating7" type="radio" name="rating" value="2">
                                  <label for="rating7"><span></span></label>
                                </div>
                                <input id="rating8" type="radio" name="rating" value="3">
                                <label for="rating8"><span></span></label>
                            </div>
                            <input id="rating9" type="radio" name="rating" value="4">
                            <label for="rating9"><span></span></label>
                          </div>
                          <input id="rating10" type="radio" name="rating" value="5">
                          <label for="rating10"><span></span></label>
                      </div>   

                      <div class="text-center">
                        <button type="submit" class="btn btn-register" onclick="return check_rating();">Submit</button>
                      </div>

                  </div>
              </form>
                </div>
              </div>
            </div>
                  
          <div class="row">
            <div class="col-sm-10">
              <p class="progress-count"><?php echo isset($completed_lectures[0]->completed_lectures)?$completed_lectures[0]->completed_lectures:'0'?> of <?php echo isset($Total_lectures[0]->total_lectures)?$Total_lectures[0]->total_lectures:''?> items complete</p>
              <div class="progress">

                <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo isset($completed_lectures[0]->completed_lectures)?$completed_lectures[0]->completed_lectures:'0'?>"
                  aria-valuemin="<?php echo isset($completed_lectures[0]->completed_lectures)?$completed_lectures[0]->completed_lectures:'0'?>" aria-valuemax="<?php echo isset($Total_lectures[0]->total_lectures)?$Total_lectures[0]->total_lectures:''?>" style="width:<?php echo isset($completed_lectures[0]->completed_lectures)?$completed_lectures[0]->completed_lectures:'0'?>%">
                  <span class="sr-only">10% Complete</span>
                </div>

              </div> 
            </div>
            <div class="col-sm-2 pl-0">
              <div class="price-cup">
              </div>
            </div>
          </div>
        </div>
     </div>
    </div> 
  </div>
</section>

<section class="course-main">
  <div class="container">
     <div class="row">
     <div class="col-sm-12">
           <div class="course-detail-heading">
            <ul class = "nav nav-tabs">
               <li ><a href = "dashboard/cource_details?product_id=<?php echo $pro_id;?>">Overview</a></li>
               <li class = "active"><a href = "dashboard/course_content?product_id=<?php echo $pro_id;?>">Course Content</a></li>
            </ul>
          </div>
          <hr>
       </div>

    </div>
    <div class="row">
      <div class="col-sm-12">

        <div class="course-detail-heading pt-10">
               
                      <div class="row">
                         <div class="col-sm-4 col-sm-offset-1">
                            <div class="blog-search">
                              <form id="chapter_form" name="chapter_form" method="post">

                                  <input type="hidden" id="course_id" name="course_id" value="<?php echo $cource_details[0]->product_id;?>"  />

                                  <input type="hidden" id="chapter_id" name="chapter_id" value=""   />

                                  <div class="input-group">
                                      <input class="form-control chapter_info" placeholder="Search" type="text" name="chapter_name" id="chapter_name" onkeyup="chapter_autocomplete();">
                                      <span class="input-group-btn">
                                         <button class="btn btn-default" type="button" onclick="get_chapter();">
                                             <i class="fa fa-search"></i>
                                         </button>
                                      </span>
                                   </div>
                              </form>
                          </div>
                         </div>
                         <div class="col-sm-6">
                           <div class="content-list-course pull-right">
                             <ul id = "myTab" class = "nav nav-tabs mt-0">
                                 <li class = "active">
                                    <a href = "#tab1" data-toggle = "tab">All Courses</a>
                                 </li>
                                 <li><a href = "#tab2" data-toggle = "tab">All Resources</a></li>
                              </ul>
                           </div>
                         </div>
                      </div>


                      <div class="row">

                        <div id = "myTabContent" class = "tab-content">

                           <div class = "tab-pane fade in active" id = "tab1">
                              <div id="chapterResult">

                              <?php
                                  if(!empty($Chapter) && count($Chapter)>0)
                                  {
                                    $i=1;
                                      foreach($Chapter as $chapResult)
                                      {

                                          $chapterLecture = get_chapter_lecture_acc_to_course($chapResult->ch_id,$chapResult->sem_id);

                                          ?>
                            <div class="tab-pane" id="tab1">
                              <div class="tab-content-2">

                                 <button class="heading accordion">
                                    <span class="pull-left">
                                    <h5 class="text-left">Section: <?php echo $i;?></h5>  
                                    <h4><?php echo $chapResult->ch_name;?></h4>
                                    </span>
                                    <span class="pull-right">

                                      <?php

                                          $total_lectures = get_total_lecture_of_particular_chapter($chapResult->ch_id,$chapResult->sem_id);
                                      ?>

                                      <h5><b><?php echo isset($total_lectures[0]->total_lectures)?$total_lectures[0]->total_lectures .'/'. $total_lectures[0]->total_lectures:'0/0'?></b></h5>
                                    </span>
                                 
                                 </button>

                                 <div class="content panel" style="display: none;transition: all ease-in-out 4s;">

                                   <?php
                                        $getExam = get_exam_acc_to_chapter_course($chapResult->ch_id,$chapResult->sem_id);

                                        if(!empty($getExam) && count($getExam)>0)
                                        {
                                          foreach($getExam as $examData)
                                          {
                                            ?>
                                            <li class="sub-list">
                                                   <a href="<?php echo base_url('front_end/exam_control/exam_instruction?eid='.base64_encode($examData->test_id).'')?>" >
                                                      <p><i class="fa fa-download" aria-hidden="true"></i>&nbsp;<?php echo $examData->test_name;?> - : Start Exam</p>
                                                   </a>
                                          </li>
                                            <?php
                                        }
                                      }
                                  ?>


                                        

                                    <?php
                                        if(!empty($chapterLecture) && count($chapterLecture)>0)
                                        {

                                            $j = 1;
                                            foreach($chapterLecture as $lectDetails)
                                            {
                                                  if($lectDetails->lecture_type == 1)
                                                  {
                                                      ?>
                                                    <li class="active">
                                                        <a href="<?php echo base_url('dashboard/video_course/'.$chapResult->ch_id.'/'.$lectDetails->lect_id.'/'.$chapResult->sem_id.'')?>">
                                                        <span><i class="fa fa-play" aria-hidden="true"></i></span>
                                                        <span><?php echo $j;?>. <?php echo $lectDetails->lect_name;?></span>
                                                        <span class="pull-right">
                                                           <span><?php echo $lectDetails->lect_duration;?></span>
                                                           <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                                        </span> 
                                                         </a>
                                                      </li>
                                                      <?php
                                                     
                                                  }

                                                  else if($lectDetails->lecture_type == 3)
                                                  {
                                                      ?>
                                                <li>
                                                   <a href="<?php echo base_url('dashboard/video_course/'.$chapResult->ch_id.'/'.$lectDetails->lect_id.'/'.$chapResult->sem_id.'')?>">
                                                      <span><i class="fa fa-play" aria-hidden="true"></i></span>
                                                      <span><?php echo $j;?>. <?php echo $lectDetails->lect_name;?></span>
                                                      </a>
                                                      <span class="pull-right">
                                                         <span><?php echo $lectDetails->lect_duration;?></span>
                                                         <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                                      </span>
                                                 </li>
                                                 <li class="sub-list">
                                                   <a href="<?php echo base_url('uploads/semester/'.$lectDetails->lect_resource.'')?>">
                                                      <p><i class="fa fa-download" aria-hidden="true"></i>&nbsp;<?php echo $lectDetails->lect_resource;?></p>
                                                   </a>
                                                 </li>

                                                      <?php
                                                      $j++;
                                                  }
                                            }
                                        }

                                        else
                                        {
                                              ?>
                                                <h3>No Lecture Found</h3>
                                              <?php
                                        }
                                    ?>
                                  

                                </div>


                             </div>
                           </div>
                                          <?php
                                          $i++;
                                      }
                                  }
                              ?>

                             </div>
                           </div>
                           
                           <div class = "tab-pane fade" id = "tab2">

                            <?php
                                if(!empty($Chapter) && count($Chapter)>0)
                                {
                                    $k=1;
                                    foreach($Chapter as $charResult)
                                    {

                                          $ResourceDetails = get_chapter_resource_acc_to_course($charResult->ch_id,$charResult->sem_id);

                                        ?>
                                <div class="tab-content-2">
                                     <button class="heading accordion" >
                                        <span class="pull-left">
                                        <h5 class="text-left">Section: <?php echo $k;?></h5>  
                                        <h4><?php echo $charResult->ch_name;?></h4>
                                        </span>
                                        <span class="pull-right">
                                      <?php
                                          $total_lectures = get_total_lecture_of_particular_chapter($chapResult->ch_id,$chapResult->sem_id);
                                      ?>

                                      <h5><b><?php echo isset($total_lectures[0]->total_lectures)?$total_lectures[0]->total_lectures .'/'. $total_lectures[0]->total_lectures:'0/0'?></b></h5>
                                    </span>
                                     
                                     </button>

                                      <?php
                                          if(!empty($ResourceDetails) && count($ResourceDetails)>0)
                                          {
                                              $m = 1;
                                              foreach($ResourceDetails as $relect)
                                              {
                                                  if(!empty($relect->lect_resource))
                                                  {     
                                                     ?>
                                    <div class="content panel" style="display: none;transition: all ease-in-out 4s;">
                                       <li class="active">
                                         <a href="javascript:void(0);">
                                           <!--  <span><i class="fa fa-play" aria-hidden="true"></i></span> -->
                                            <span><?php echo $m;?>. <?php echo $relect->lect_name;?></span>
                                            </a>
                                           
                                       </li>
                                       <li class="sub-list">
                                      <a href="<?php echo base_url('uploads/semester/'.$relect->lect_resource.'')?>">
                                            <p><i class="fa fa-download" aria-hidden="true"></i>&nbsp;<?php echo $relect->lect_resource;?></p>
                                         </a>
                                       </li>
                                    </div>
                                                  <?php
                                                  $m++;
                                              }
                                           }
                                        }

                                        else
                                        {
                                            ?>
                                  <div class="content panel" style="display: none;transition: all ease-in-out 4s;">
                                       <li class="active">
                                         <a href="javascript:void(0);">
                                            <span>No Resource Found</span>
                                            </a>
                                           
                                       </li>
                                    </div>
                                            <?php
                                        }

                                      ?>

                                </div>
                                        <?php
                                        $k++;
                                    }
                                }
                                else
                                {
                                    echo "No Resource Found";
                                }
                            ?>
                              
                           </div>
                           
                                       
                        </div>
                      </div>
            </div>
        </div>
     </div>
  </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">


<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
   acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
           panel.style.display = "none";
       } else {
           panel.style.display = "block";
       }
   });
}
</script>

<script type="text/javascript">

    function chapter_autocomplete(){
       jQuery('.chapter_info').autocomplete({
              source: '<?php echo base_url()?>dashboard/get_chapter_autocomplete/?chapter='+$('#chapter_name').val()+'&course_id='+$('#course_id').val(),
            minLength: 1,
            select: function(event, ui) {

              jQuery('.chapter_info').val(ui.item.pname); 
              jQuery('#chapter_id').val(ui.item.chapter_id); 
              
            }
        });
    }
</script>

<script type="text/javascript">
  function get_chapter()
  {
    $.ajax({
              type :'POST',
              url  :'<?php echo base_url("dashboard/ajax_chapter_list")?>',
              data : $('#chapter_form').serialize(),
              success:function(resp)
              {
                  resp = resp.trim();
                  $('#chapterResult').html(resp);
              } 

    })
  }
</script>


<script type="text/javascript">
  function check_rating()
  {
      var rate = $('input:radio[name=rating]:checked').val();

      if(rate == "" || rate==undefined)
      {
          alert("Please Select Stars To Rate This Course");
          return false;
      }
  }
</script>

