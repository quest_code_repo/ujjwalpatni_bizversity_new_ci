<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="user-cours-list wow slideInLeft">
           <nav class="nav-sidebar">
              <ul class="nav tabs">
                <li ><a href="my-courses">My Courses</a></li>
                <li><a href="completed-course">Completed Courses</a></li>
                <li><a href="my-result">My Results</a></li>
                <li class="active"><a href="purchased-courses">Manage Group Users</a></li>
                <li><a href="group-result">My Group Results</a></li>
              </ul>
            </nav>
        </div>
      </div>
      <div class="col-sm-9">
      	 <div class="group-registration-section commn-head-adjust">
      	 	  <div class="row">
		 	 <div class="col-sm-12">
		 	 	<div>
		 	 		<h2 class="common-heading wow fadeInUp">Group Registration</h2>
		 	 		<h4 class="sub-heading">Select Product</h4>
		 	 		 <div class="row">
		 	 		 	<div class="col-sm-3">
		 	 		 		<div class="form-group">
							  <select class="form-control first" id="course_id" name="course_id" onchange="get_course_id();">
							   		<option value="">Select Course</option>
							   		<?php
							   				if(!empty($LicensedCourses) && count($LicensedCourses)>0)
							   				{
							   					foreach($LicensedCourses as $courseData)
							   					{
							   						?>
							   						<option value="<?php echo $courseData->product_id;?>"><?php echo $courseData->pname;?></option>
							   						<?php
							   					}
							   				}
							   		?>
							  </select>
					        </div> 
		 	 		 	</div>
		 	 		 </div>
						
		 	 	</div>
		 	 </div>
		 </div>
      	 </div> 
      </div>
   </div>
  </div>
 </section>	


 <script type="text/javascript">
 	function get_course_id()
 	{
 		var course_id = $('#course_id').find(":selected").val();

 		
 		if(course_id!= "" || course_id == undefined)
 		{
 			 var redirect_url = "<?php echo base_url();?>dashboard/group_registration?course_id="+course_id;
		     window.location.href = redirect_url;
 		}
 		else
 		{
 			window.location.href="<?php echo base_url('dashboard/group_registration')?>";
 		}
 	}
 </script>

