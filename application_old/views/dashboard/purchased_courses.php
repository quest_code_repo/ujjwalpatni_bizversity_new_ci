
<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="user-cours-list wow slideInLeft">
           <nav class="nav-sidebar">
              <ul class="nav tabs">
                <li ><a href="my-courses">My Courses</a></li>
                <li><a href="completed-course">Completed Courses</a></li>
                <li><a href="my-result">My Results</a></li>
                <li class="active"><a href="purchased-courses">Manage Group Users</a></li>
                <li><a href="group-result">My Group Results</a></li>
              </ul>
            </nav>
        </div>
      </div>
      <div class="col-sm-9">
      	 <div class="group-registration-section commn-head-adjust">
      	 	  <div class="row">
		 	 <div class="col-sm-12">
		 	 	<div>
		 	 		<h2 class="common-heading wow fadeInUp">MY PURCHASED COURSES</h2>
		 	 		 <div class="row">
		 	 		 	<div class="col-sm-12">
		 	 		 		 <table class="table table-bordered registered ">
						    <thead>
						      <tr>
						        <th>COURSE NAME</th>
						        <th>LICENSE NO</th>
						        <th>NO. OF LICENSES</th>
						        <th>STARTING DATE</th>
						        <th>EXPIRY DATE</th>
						        <th>ACTION</th>
						      </tr>
						    </thead>
						    <tbody>
						    	<?php
						    			if(!empty($PurchasedCourses) && count($PurchasedCourses)>0)
						    			{
						    				foreach($PurchasedCourses as $myOrder)
						    				{
						    					?>
						    					 <tr>
											        <td><?php echo $myOrder->pname;?></td>
											        <td><?php echo $myOrder->licence_no;?></td>
											        <td><?php echo $myOrder->purchase_product_quantity;?></td>

											        <?php
											        	$date = $myOrder->alloted_date;
														$createDate = new DateTime($date);
														$strip = $createDate->format('Y-m-d');
											        ?>

											        <td><?php echo ($strip);?></td>
											        <td><?php echo $myOrder->end_date;?></td>
											        <td><a href="group-registration/<?php echo $myOrder->licence_no;?>/<?php echo base64_encode($myOrder->course_id);?>" button class="btn btn-default btn-register add-user-btn">Add USER</button></a></td>
											      </tr>
						    					<?php
						    				}
						    			}

						    			else
						    			{
						    				echo "No Record Found";
						    			}
						    	?>

						    
						    </tbody>
						  </table>
		 	 		 	</div>
		 	 		 </div>
						
		 	 	</div>
		 	 </div>
		 </div>
      	 </div> 
      </div>
   </div>
  </div>
 </section>	




