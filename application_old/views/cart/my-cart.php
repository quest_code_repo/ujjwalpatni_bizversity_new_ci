<section class="add-cart-section about-us-section">
    <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
              <div class="row">
                <div class="col-sm-12">
                  <h2 class="common-heading wow fadeInUp">Shopping Cart</h2>
                </div>
              </div>
             <div class="row">
            <div class="col-md-8">
               
                <?php 
                  $Subtotal=0;
                  
                  if(!empty($CartInfo) && count($CartInfo)>0)
                  {
                    ?>

              <div class="add-cart-heading">
                <div class="row">  
                   <div class="col-md-2 col-md-offset-8">
                      <p>Price</p>
                  </div>  
                   <div class="col-md-2">
                      <p> Quantity</p>
                  </div>  
                </div>
               </div>

                    <?php
                    foreach($CartInfo as $cartDetails)
                    {
                      $details = get_cart_item_details($cartDetails->product_id, $cartDetails->product_type); 
                ?>  
                <div class="add-cart-block">
                  <div class="row">
                    <div class="col-md-2">
                    <?php
                      if ($cartDetails->product_type == 'programe') {
                    ?>
                      <img src="front_assets/images/other/show-logo.png" width="100%">
                    <?php }else { ?>
                      <img src="<?php echo base_url('uploads/products/'.$details[0]->image_url.'')?>" width="100%">
                    <?php } ?>
                    </div>
                    <div class="col-md-6">
                      <div class="item-details"> 
                        <a href="javascript:;"><h4 class="sub-heading"><?php echo $details[0]->item_name; ?></h4></a> 
                        <p>Categories: <?php echo $details[0]->category; ?></p>
                        <a href="Cart/cart/delete_cart_item/<?php echo $cartDetails->cart_id;?>">Delete</a>
                      </div> 
                    </div>
                    <div class="col-md-2 item-price">
                        <p>₹ <?php echo $cartDetails->actual_price;?></p>
                    </div>
                    <div class="col-md-2 text-right item-quantity">
                      <select class="form-control" name="qty" id="" onchange="change_qty(this.value,'<?php echo $cartDetails->cart_id; ?>');" >
                        <?php
                        for ($i=1; $i <= 10 ; $i++) { 
                        ?>
                         <option value="<?php echo $i; ?>" <?php echo ($cartDetails->p_quantity == $i)?'selected':'';?> ><?php echo $i; ?></option>
                         <?php } ?>
                      </select>
                    </div>
                </div>
            </div>
            <?php  $Subtotal = $Subtotal+($cartDetails->actual_price*$cartDetails->p_quantity); 
             }  
           }
           else{
                    echo "<h3>Your Cart Is Empty</h3>";
           } 
        ?>
            
          </div>   
            <div class="col-md-4">
                <div class="proceed-pay-block">
                    <div class="proceed-pay-inner text-center">
                       <p>Your order is eligible for FREE Delivery. <span>Select this option at checkout.</span> 
                       <!-- <a href="">Details</a> -->
                       </p> 
                      <!--  <button class="btn btn-default btn-register" id="add-coupn">Add Coupon</button> -->
                          <!-- <form>
                            <div class="input-group coupn-enter" >
                              <input type="text" class="form-control" placeholder="Coupon Code">
                              <div class="input-group-btn">
                                <button class="btn btn-default btn-register" type="submit">Apply</button>
                              </div>
                            </div>
                          </form> --> 
                      <!--  <div class="form-group coupn-enter" >
                            <input type="text" class="form-control" >
                       </div> -->
                    </div>
                    <div class="Subtotal-block">
                      <p>Subtotal :<span>Rs. <?php echo $Subtotal; ?> </span></p>
                      <!-- <p>Coupon Applied :<span>- Rs. </i> 546.00 </span></p> -->
                      <!-- <p>Coupon Applied for:<span>E-learning </span></p > -->
                      <hr>
                      <h4>Subtotal (<?php echo count($CartInfo); ?> items):<span>Rs. <?php echo $Subtotal; ?> </span></h4>  

                      <?php
                          if(!empty($CartInfo) && count($CartInfo)>0)
                          {
                              if(!empty($ProductType) && count($ProductType)>0)
                              {
                                  if(in_array('e_learning',$ProductType) || in_array('product',$ProductType))
                                  {
                                        ?>
                                        <div class="text-center">
                                           <a href="<?php echo base_url('checkout/checkout_details')?>">
                                              <button class="btn btn-default btn-register">Proceed to checkout</button>
                                           </a>
                                       </div>
                                        <?php
                                  }

                                  else
                                  {
                                      ?>
                                        <div class="text-center">
                                           <a href="place-order-programe">
                                              <button class="btn btn-default btn-register">Proceed to checkout</button>
                                           </a>
                                       </div>
                                      <?php
                                  }
                              }
                            }

                            ?>
                       
                        </div>
                    <div>                        
                     
                    </div>
                </div>
            </div>
        </div>
          </div>
        </div>
</section>

<script>
$(document).ready(function(){
    $("#add-coupn").click(function(){
      $("#field").slideToggle("slow");
    });
});

  function change_qty(qty,cart_id)
  {
    $.ajax({
      type :'POST',
      dataType: 'json',
      url  :'<?php echo base_url("Cart/cart/update_cart_item")?>',
      data : { "qty":qty, "cart_id":cart_id },
      success:function(resp)
      {
          if(resp.msg == "SUCCESS")
          {  
            location.reload();
              // $('#item_value').text(resp.item_count);
              // $('#item_value').text(resp.item_count);
          } 
      } 
    });
  }
</script>