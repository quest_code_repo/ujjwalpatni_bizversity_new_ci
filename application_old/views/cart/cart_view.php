


<section class="add-cart-section about-us-section">
    <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
             <div class="row">
                  <div class="col-sm-12">
                       <h2 class="common-heading wow fadeInUp">Shopping Cart</h2>
                  </div>
                </div>
             <div class="row">
            <div class="col-md-8">
               <div class="add-cart-heading">
                
                <div class="row">  
                   <div class="col-md-2 col-md-offset-8">
                      <p>Price</p>
                  </div>  
                   <div class="col-md-2">
                      <p> Quantity</p>
                  </div>  
                </div>
               </div>  


               <?php 
                    if(!empty($CartInfo) && count($CartInfo)>0)
                    {
                        foreach($CartInfo as $cartDetails)
                        {
                            ?>
          <div class="add-cart-block">
                <div class="row">
                      <div class="col-md-2">
                       <img src="<?php echo base_url('uploads/products/'.$cartDetails->image_url.'')?>" width="100%"> 
                      </div>
                    <div class="col-md-6">
                      <div class="item-details"> 
                          <a href=""><h4 class="sub-heading"><?php echo $cartDetails->pname;?></h4></a> 
                           <p>Categories: Air Pistols</p>
                            <a href="Cart/cart/delete_cart_item/<?php echo $cartDetails->cart_id;?>">Delete</a>
                      </div> 
                    </div>
                    <div class="col-md-2 item-price">
                        <p>₹ <?php echo $cartDetails->actual_price;?></p>
                    </div>
                    <div class="col-md-2 text-right item-quantity">
                      <select class = "form-control" >
                         <option value="1" <?php echo ($cartDetails->p_quantity == '1')?'selected':'';?> >1</option>
                         <option value="2" <?php echo ($cartDetails->p_quantity == '2')?'selected':'';?>>2</option>
                         <option value="3" <?php echo ($cartDetails->p_quantity == '3')?'selected':'';?>>3</option>
                         <option value="4" <?php echo ($cartDetails->p_quantity == '4')?'selected':'';?>>4</option>
                      </select>
                    </div>
                </div>
            </div>
                            <?php
                        }
                    }
               ?>

        


            
          </div>   
            <div class="col-md-4">
                <div class="proceed-pay-block">
                    <div class="proceed-pay-inner text-center">
                       <p>Your order is eligible for FREE Delivery. <span>Select this option at checkout.</span> 
                       <a href="">Details</a></p> 
                       <button class="btn btn-default btn-register" id="add-coupn">Add Coupon</button>
                       <div class="form-group coupn-enter" id="field">
                            <input type="text" class="form-control" >
                       </div>

                    </div>
                    <div class="Subtotal-block">
                      <h4>Subtotal (11 items):<span><i class="fa fa-inr" aria-hidden="true"></i> 14,546.00 </span></h4>  
                       <!--  <div class = "checkbox text-center">
                            <label><input type = "checkbox"> Check me out</label>
                        </div> -->
                        <div class="text-center">
                           <a href="Select-delivery-mode.php">
                              <button class="btn btn-default btn-register">Proceed to checkout</button>
                           </a>
                        </div>
                    </div>
                    <div>                        
                     
                    </div>
                </div>
            </div>
        </div>
          </div>
        </div>
</section>



<script>
$(document).ready(function(){
    $("#add-coupn").click(function(){
      $("#field").slideToggle("slow");
    });
});
</script>