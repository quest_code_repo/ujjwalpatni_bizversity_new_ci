<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elearning extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        $this->load->helper('text');
        $this->load->helper('cart_helper');
        
    }


    public function courses($url="",$course_for="")
    {


    	if(!empty($url) && empty($course_for))
    	{
    	   $course_category = $this->home_model->getwhere_data('product_categories',array('parent_category'=>4,'category_url'=>$url),'id','id','');

         $where = array('product_category_id'=>$course_category[0]->id,'active_inactive'=>'active','product_type'=>'e_learning');

    		$fileteredData = $this->home_model->getwhere_data('products',$where,'product_id,url,pname,image_url,short_disc','product_id','');


                 $data['count_per'] = count($fileteredData);

                /*** CI Pagination Start ***/

                  $count_records = $data['count_per'];

                      if($this->input->get('per_page')) {
                          $get_page = $this->input->get('per_page');
                      } else {
                          $get_page = 0;
                      }
                      $count_data=3;

                if(!empty($this->input->get('per_page')))
                {
                    
                      $data["links"] = $this->all_page_pagination(base_url()."elearning/courses/".$url."?per_page=".$_GET['per_page'],$count_records,$get_page,$count_data);
                }
                else
                {
                     $data["links"] = $this->all_page_pagination(base_url()."elearning/courses/".$url."?",$count_records,$get_page,$count_data);
                }

           $where = array('product_category_id'=>$course_category[0]->id,'active_inactive'=>'active','product_type'=>'e_learning');

            $data['elearning_products'] = $this->home_model->getwhere_limit_data('products',$where,'product_id,url,pname,image_url,short_disc','product_id','',$count_data,$get_page);


    	}

        elseif(!empty($url) && !empty($course_for))
        {
            $course_category = $this->home_model->getwhere_data('product_categories',array('parent_category'=>4,'category_url'=>$url),'id','id','');

            $where = array('product_category_id'=>$course_category[0]->id,'active_inactive'=>'active','product_type'=>'e_learning','course_type'=>$course_for);

            $fileteredData = $this->home_model->getwhere_data('products',$where,'product_id,url,pname,image_url,short_disc','product_id','');


                 $data['count_per'] = count($fileteredData);

                /*** CI Pagination Start ***/

                  $count_records = $data['count_per'];

                      if($this->input->get('per_page')) {
                          $get_page = $this->input->get('per_page');
                      } else {
                          $get_page = 0;
                      }
                      $count_data=3;

                if(!empty($this->input->get('per_page')))
                {
                    
                      $data["links"] = $this->all_page_pagination(base_url()."elearning/courses/".$url.'/'.$course_for."?per_page=".$_GET['per_page'],$count_records,$get_page,$count_data);
                }
                else
                {
                     $data["links"] = $this->all_page_pagination(base_url()."elearning/courses/".$url.'/'.$course_for."?",$count_records,$get_page,$count_data);
                }

           $where = array('product_category_id'=>$course_category[0]->id,'active_inactive'=>'active','product_type'=>'e_learning');

            $data['elearning_products'] = $this->home_model->getwhere_limit_data('products',$where,'product_id,url,pname,image_url,short_disc','product_id','',$count_data,$get_page);

        }

    	else
    	{
    		$course_category = $this->home_model->getwhere_data('product_categories',array('parent_category'=>4),'id','id','');

    	     $fileteredData = $this->home_model->getwhere_data('products',array('product_category_id'=>$course_category[0]->id,'active_inactive'=>'active','product_type'=>'e_learning'),'product_id,url,pname,image_url,short_disc','product_id','');

                    $data['count_per'] = count($fileteredData);

                /*** CI Pagination Start ***/

                  $count_records = $data['count_per'];

                      if($this->input->get('per_page')) {
                          $get_page = $this->input->get('per_page');
                      } else {
                          $get_page = 0;
                      }
                      $count_data=3;

                if(!empty($this->input->get('per_page')))
                {
                    
                      $data["links"] = $this->all_page_pagination(base_url()."elearning/courses/?per_page=".$_GET['per_page'],$count_records,$get_page,$count_data);
                }
                else
                {
                     $data["links"] = $this->all_page_pagination(base_url()."elearning/courses/?",$count_records,$get_page,$count_data);
                }

           $where = array('product_category_id'=>$course_category[0]->id,'active_inactive'=>'active','product_type'=>'e_learning');

            $data['elearning_products'] = $this->home_model->getwhere_limit_data('products',$where,'product_id,url,pname,image_url,short_disc','product_id','',$count_data,$get_page);

    	}
    

      	 $data['elearning_categories'] = $this->home_model->getwhere_data('product_categories',array('parent_category'=>4),'id,name,category_url','id','');

       $this->load_view('elearning/all_courses',$data);
    }


    public function course_detail($product_url)
    {
    	if(!empty($product_url))
    	{
    		$data['e_learning_product'] = $this->home_model->getwhere_data('products',array('url'=>$product_url,'active_inactive'=>'active','product_type'=>'e_learning'),'product_id,url,pname,image_url,short_disc,description,product_price,disc_price,product_type,video_url,product_category_id,course_type','product_id','');

    		$data['product_meta'] =  $this->home_model->getwhere_data('product_data_relation',array('product_id'=>$data['e_learning_product'][0]->product_id),'','product_id','');

            $data['Cities'] = $this->home_model->getwhere_data('cities',array(),'','city_id','');

             $data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);


             if(!empty($data['e_learning_product']))
             {
                $product_category_id = $data['e_learning_product'][0]->product_category_id;
                $course_type         = $data['e_learning_product'][0]->course_type;
                $product_id          = $data['e_learning_product'][0]->product_id;

                $data['Related_courses'] = $this->home_model->getwhere_data('products',array('product_category_id'=>$product_category_id,'active_inactive'=>'active','product_type'=>'e_learning','course_type'=>$course_type,'product_id !='=>$product_id),'product_id,url,pname,image_url,short_disc,url','product_id','');

                  $data['Overallrating'] = $this->home_model->getwhere_data('star_rating',array('course_id'=>$data['e_learning_product'][0]->product_id),'AVG(rating_star) as overall_avg,count(rating_id) as rating_id','rating_id','');
             }


    		if(!empty($this->session->userdata('id')))
    		{
    			$userId = $this->session->userdata('id');
    			$data['UserDetail'] = $this->home_model->getwhere_data('tbl_students',array('id'=>$userId),'','id','');
    		}


			$this->load_view('elearning/e_product_detail',$data);
    	}
    	else
    	{
    		redirect('elearning/courses','refresh');
    	}
    }

    public function get_course_autocomplete()
    {
        $getData = $this->input->get();
        $return_arr = array();

        if(!empty($getData))
        {
             $where = array('product_type'=>'e_learning','stock_status' => 'In Stock','deleted'=>'0','active_inactive'=>'active');
             $res= $this->home_model->get_autocomplete_product('product_id,pname,url',$getData["term"],'products',$where);

             if(!empty($res) && count($res)>0)
        {
            $i=0;

          foreach ($res as $r_val) 
          {
              $i++;
              if($i==10)
              {
                break;
              }

           $row_array['value']                = $r_val->pname;
           $row_array['product_id']           = $r_val->product_id;
           $row_array['url']                  = $r_val->url;
           array_push($return_arr, $row_array);
          }
       }

       }

       echo json_encode($return_arr);
    }


    public function search_course()
    {
            $postData = $this->input->post();

            if(!empty($postData))
            {
                redirect('elearning/course_detail/'.$postData['product_url'].'','refresh');
            }
            else
            {
                redirect('elearning','refresh');
            }
    }

}