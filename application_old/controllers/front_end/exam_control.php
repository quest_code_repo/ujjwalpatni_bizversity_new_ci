<?php error_reporting(0);
if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Exam_control extends MY_Controller 
{
    public function __construct() 
    {
        parent::__construct(); 
        $this->load->helper(array('url','html','form')); 
        // $this->load->model('front/commonmodel');
        $this->load->model('front_end/exam_model');
        $this->checkAuth();
        
    }

    public function checkAuth()
    {
        if($this->session->userdata('id')!='' )
        {
            return;
        }
        else
        {
            redirect('login');
        }
    }

 

    public function exam_instruction() 
    {
        $user_id=$this->session->userdata('id');
        
        if($this->input->get('eid')!=''){
            $eid= base64_decode($this->input->get('eid'));
             //$eid= ($this->input->get('eid'));
            if(is_numeric($eid)){
                $no_que=$this->db->query("SELECT COUNT(`ques_id`) as 'noq', sum(`que_marks`) as 'ttlmarks' FROM `tbl_add_question` WHERE `test_id` =  $eid AND `ques_status` = 'active' ");
                if($no_que->num_rows()>0){
                    //print_r($no_que->result_array());
                    $tque = $no_que->result_array();
                    $total_que = $tque[0]['noq'];
                    $ttlmarks=$tque[0]['ttlmarks'];
                }else{
                	$total_que=0;
                	$ttlmarks=0;
                }

                $data['total_que']=$total_que;
                $data['ttlmarks']=$ttlmarks;

                $data['exam']=$this->exam_model->fetch_recordbyid('tbl_test',array('test_id'=>$eid,'test_status'=>'active'));
                
                $this->session->unset_userdata('questions');

                $this->session->set_userdata(array('exam'=>$data['exam']));

                $data['head_text'] = 1;

                load_exam_view('exam/startexam', $data);
            }else{
                redirect('');
            }
        }else{
            redirect('');
        }
    }

    public function doexam($id='')
    {
        $this->checkAuth();
        if($id!='')
        {            
            $exam = $this->session->userdata('exam');

            $sem_id = $this->input->get('sem_id');
            $user_id= $this->session->userdata('id');
            $testid = array('test_id' => $id,'test_start'=>$id );
            $this->session->set_userdata($testid);
            if($exam->test_type=='2' || $exam->test_type=='3'){
                $exam->ideal_time_duration = 0;
            }
            $data['exam']=$exam;
            $test_name = array('test_name' => $exam->test_name);
            $this->session->set_userdata($test_name);
            $where = array('user_id' => $user_id,'test_id' => $id);
            // $where = array('user_id' => $user_id,'test_id' => $id,'status'=>'completed');
            $examdata = $this->exam_model->grt_data('tbl_user_test',$where);
            // echo $this->db->last_query();
           //  echo"<pre>";print_r($examdata);die;
            if($examdata){
                // echo "string";
                if ($examdata[0]['status']=='completed') {
                    redirect("summary_analysis/?test_id=$id");
                    return;
                }
                $my_time= $examdata[0]['test_start_time'];
                $leps_time= round((time()-$my_time));
                $exam_ideal_time=($exam->ideal_time_duration*60)-$leps_time;
                $refresh='yes';

                $update_data=array('date'=>date('Y-m-d'),'sem_id'=>$sem_id,'score'=>'', 'status' => 'pending');
                $this->exam_model->updateRecords('tbl_user_test',$update_data,$where);
                // echo $this->db->last_query();exit;
                // $where1 = array('user_id' => $user_id,'test_id' => $id);
                // $this->exam_model->deleteRecords('tbl_user_ques_answer',$where1);
            }else{

                $test_start_time = array('test_start_time' => time());
                $test_start_timer= time();
                //$this->session->set_userdata($test_start_time);
                $exam_ideal_time= $exam->ideal_time_duration*60;
                $this->exam_model->insert_data('tbl_user_test',array('user_id'=>$user_id,'test_id'=>$id,'sem_id'=>$sem_id,'date'=>date('Y-m-d'),'test_start_time'=>$test_start_timer, 'status' => 'pending'));
                $refresh='';
            }
            // echo $exam_ideal_time;die;
            $que_data=$this->get_user_exam_data($id);


           // echo "<pre>";print_r($this->session->all_userdata());echo "</pre><br>=============<br>";
            $sec_id_array= $this->session->userdata('sec_subject');


            $sec_id = $sec_id_array[0]['ques_id'];

        
            $questions = array('questions' => $que_data['questions']);

          
            $mmd = $this->session->userdata('questions');

            if ($mmd == '') {
                $this->session->set_userdata($questions);
            }
            $mmd1 = $this->session->userdata('questions');
           
           
            $data['no_questions']=count($que_data['questions']);


            $fdata=$this->get_quest_data($sec_id);

            $data['thisquestion']=$fdata['thisquestion'];

            $data['questions']=@$que_data['questions'][0];
            // echo $exam_ideal_time;
            $data['exam_ideal_time']=$exam_ideal_time;


            // print_r($data['exam_ideal_time']);
            // die();

            /************For Selected Answeres*******/

            $questions_idr  = $sec_id;
            $users_idr      = $this->session->userdata('id');
            $test_id        = $this->session->userdata("test_id");

            $data['show_submited']=$this->exam_model->grt_data('tbl_user_ques_answer',array('user_id'=>$users_idr,'question_id'=>$questions_idr,'test_id'=>$test_id));

            $data['ans_submited']=$this->exam_model->grt_data('tbl_user_ques_answer',array('user_id'=>$users_idr,'test_id'=>$test_id));
            // print_r($data['show_submited']);die;

            $where_qus_count = array('test_id'=>$test_id);
        
            $examdata = $this->exam_model->fetch_recordbyid('tbl_add_question',$where_qus_count);

            $data['record_contr'] = COUNT($examdata);

            /******** Selected End ************/
            load_exam_view('exam/do_exam', $data);

           
        }
    }

    public function get_quest_data($que_id){
        $que_data=$this->exam_model->grt_data('tbl_add_question',array('ques_id'=>$que_id));
        $exam['thisquestion'] = array();
        if (!empty($que_data))
        {

            $exam['thisquestion']['qid']=$que_id;
            $exam['thisquestion']['question_desc']=$que_data[0]['question_desc'];
            $exam['thisquestion']['question_image'] = $que_data[0]['question_image'];
            $exam['thisquestion']['ans_1_image'] = $que_data[0]['ans_1_image'];
            $exam['thisquestion']['ans_2_image'] = $que_data[0]['ans_2_image'];
            $exam['thisquestion']['ans_3_image'] = $que_data[0]['ans_3_image'];
            $exam['thisquestion']['ans_4_image'] = $que_data[0]['ans_4_image'];
            $exam['thisquestion']['right_ans_exp_img']=$que_data[0]['right_ans_exp_img'];

            /***********New By Shubham*********/
            $exam['thisquestion']['question_type'] = $que_data[0]['question_type'];


            $answers = array();
            for ($ai=1;$ai<=4; $ai++) {
                $answer_data = array('id' =>$ai, 'text'=>trim($que_data[0]['ans_'.$ai]),'s'=>0);
                // $answer_data = array('id' =>$ai, 's'=>0);
                array_push($answers, $answer_data);
            }
            $exam['thisquestion']['answers'] = $answers;
        }
        // print_r($exam);
        return $exam;
    }

    public function get_user_exam_data($examid)
    {

        $user = $this->session->userdata('id');
        // $get_subject = $this->exam_model->get_sql_record("SELECT * FROM `tbl_subject` WHERE `id` IN(SELECT DISTINCT(`chapter_id`) FROM `tbl_add_question` WHERE `test_id` = '$examid' AND `ques_status` = 'active' ORDER BY `ques_id` ASC )");
        // echo"<pre>";
        // print_r($get_subject);

          $get_subject = $this->exam_model->get_sql_record("SELECT * FROM  `tbl_add_question` WHERE `test_id` = '$examid' AND `ques_status` = 'active' ORDER BY `ques_id` ASC ");

        foreach ($get_subject as $key => $value) {
            ${'a'.$value['chapter_id']}=0;

        }
       
        
        $examdata = $this->exam_model->getExamQue($examid);

        $exam = array();
        $exam['questions'] = array();
        if (!empty($examdata))
        {
            // $exam['questions'] = $examdata;
            $exam['id']=$this->session->userdata('exam')->test_id;
            $exam['name']=$this->session->userdata('exam')->test_name;

            $this->session->set_userdata(array('sec_subject'=> $get_subject));

             //print_r($this->session->userdata('sec_subject'));die;
             

            foreach ($examdata as $count=>$question)
            {
                $chapter_id = $question['chapter_id'];
                
                $d=${'a'.$chapter_id};
                
                ${'a'.$chapter_id}++;

                $exam['questions'][$chapter_id][$d]['question_id'] = $question['ques_id'];
                $exam['questions'][$chapter_id][$d]['question_image'] = $question['question_image'];
                $exam['questions'][$chapter_id][$d]['ans_1_image'] = $question['ans_1_image'];
                $exam['questions'][$chapter_id][$d]['ans_2_image'] = $question['ans_2_image'];
                $exam['questions'][$chapter_id][$d]['ans_3_image'] = $question['ans_3_image'];
                $exam['questions'][$chapter_id][$d]['ans_4_image'] = $question['ans_4_image'];
                $exam['questions'][$chapter_id][$d]['right_ans_exp_img']=$question['right_ans_exp_img'];
                
                // $exam['questions'][$count]['text'] = $question['question_desc'];
                // $exam['questions'][$count]['image'] = ($question['attachment_img']  != '') ? '<img src="'.base_url().$question['attachment_img'].'" />' : '' ;
                
                $answers = array();
                
                for ($ai=1;$ai<=4; $ai++) {
                  
                    $answer_data = array('id' =>$ai,'s'=>0);
                    
                    array_push($answers, $answer_data);
                }
                $exam['questions'][$chapter_id][$d]['answers'] = $answers;
            }
            //echo "<pre>"; print_r($exam);echo "</pre>";die;
            return $exam;
        }
        return $exam;
    }

    public function save_answer(){


        $question_id    = $_POST["queno"];
        $ans            = $_POST["ans"];
        $time           = $_POST["time"];
        $section_index  = $_POST["section"];
        $ques_number    = $_POST["ques_number"];
        $user_id        = $this->session->userdata('id');
        $questions      = $this->session->userdata("questions");
        $test_id        = $this->session->userdata("test_id");
        $test_type      = $this->session->userdata("test_type");
        $sec_subject    = $this->session->userdata('sec_subject');
        $section        = $sec_subject[$section_index]['chapter_id'];
        $qq_id          = $questions[$section][$question_id]['question_id'];

     // echo '<pre>'; print_r($sec_subject);die("kamlesh");
        $ses=$this->session->userdata('questions');
        if (!is_array($ses)) {
            exit('sout');
        }
        $an=$ans-1;
        
        for ($i=0; $i <4 ; $i++)
        { 
            $ses[$section][$question_id]['answers'][$i]['s']=0;
        }
        $ses[$section][$question_id]['answers'][$an]['s']=1;

        // echo $questions[$question_id]['ques_id'];die;
        
        $this->session->set_userdata(array('questions'=>$ses));
        
        // echo "<pre>";print_r($this->session->userdata('questions'));echo "</pre>";//die;
        /* For Adaptive */
        // if($test_type==2 ){
        //    $this->next_que($question_id, true);
        //    return;
        // }
        /* For Adaptive End */
        
        $where = array('user_id' => $user_id,'question_id' => $qq_id);
        
        $examdata = $this->exam_model->fetch_recordbyid('tbl_user_ques_answer',$where);

        if($examdata){
            $this->exam_model->deleteRecords('tbl_user_ques_answer',$where);
        }
        // die;
       
        $data= array('user_id'=> $user_id, 'test_id'=>$test_id,'date'=>date('Y-m-d'),'question_id'=>$qq_id,'ques_time'=>$time, 'answer'=>$ans,'attempted'=>'yes','ques_number'=>$ques_number,'status'=>'active');
        
        $this->exam_model->insert_data('tbl_user_ques_answer',$data);

        /***********New work*********/

        $check_indexing = $question_id+1;

        $where_qus_count = array('test_id'=>$test_id);
        
        $examdata = $this->exam_model->fetch_recordbyid('tbl_add_question',$where_qus_count);

        $record_contr = COUNT($examdata);

        $minimize_contr = $record_contr-1;

        if($check_indexing <= $minimize_contr){

            $this->next_que($check_indexing,$section);

        } else { 

            $this->next_que($check_indexing-1,$section);

        }




    }



    
    public function next_que($ques_index='', $ques_sec='', $adaptive=false)
    {

        if($ques_index==''){
            
            $ques_index=$this->input->get('id');
        }
        if($ques_sec==''){
            $ques_sec=$this->input->get('section');
        }

        $testid = $this->session->userdata('test_id');

         $get_subject = $this->exam_model->get_sql_record("SELECT * FROM  `tbl_add_question` WHERE `test_id` = '$testid' AND `ques_status` = 'active' ORDER BY `ques_id` ASC ");

         if(!empty($get_subject))
         {
         		$this->session->set_userdata(array('test_questions'=> $get_subject));
         }

         $questions = $this->session->userdata('questions');


        if(is_array($questions)){
            
            if(array_key_exists($ques_sec,$questions)){
                
                if(array_key_exists($ques_index,$questions[$ques_sec]))
                {

                    $fdata=$this->get_quest_data($questions[$ques_sec][$ques_index]['question_id']);

                    /************For Selected Answeres*******/

                    $questions_idr  = $questions[$ques_sec][$ques_index]['question_id'];
                    $users_idr      = $this->session->userdata('id');
                    $test_id        = $this->session->userdata("test_id");

                    $data['show_submited']=$this->exam_model->grt_data('tbl_user_ques_answer',array('user_id'=>$users_idr,'question_id'=>$questions_idr,'test_id'=>$test_id));

                    /********Selected End************/

                    $data['thisquestion']=$fdata['thisquestion'];
                    
                    $data['questions']=$questions[$ques_sec][$ques_index];
                    
                    $data['ques_index']=$ques_index+1;
                    
                    if($adaptive){
                       
                        $this->load->view('exam/single_que_adaptive',$data);
                   
                    }else{
                        
                        $this->load->view('exam/single_que',$data);
                    }
                }else{
                    exit("changeSec");
                    if(array_key_exists($ques_sec+1,$questions)){
                        echo "exist";
                    }else{
                        echo "not exist";
                    }
                    // echo $ques_index;
                    $this->finish_user_exam();
                }
            }else{
                exit("sout");
            }
        }else{
            exit("sout");
        }
    }

    public function finish_user_exam($fin='', $timeout='0'){
    
        $user_id = $this->session->userdata('id');
        $examid = $this->session->userdata('test_id');
        $fin = @$this->input->get['fin'];
        $where = array('user_id' => $user_id,'test_id' => $examid);
        $examdata = $this->exam_model->fetch_recordbyid('tbl_user_test',$where);
        if($examdata){
            $data=array('date'=>date('Y-m-d'), 'status' => 'completed');
            $this->exam_model->updateRecords('tbl_user_test',$data,$where);
        }else{
            // return true;
        }
        if($fin){
            $this->session->unset_userdata('exam');
            $this->session->unset_userdata('questions');
            return true;                
        }
        if ($timeout='1') {
            /* Ankit will do it later. For exam timeout function */
        }
        // $response = 'Click On Finish Button if you wish to Submit the Examination.';
        // redirect("catalyser_main/view_test_analysis/$examid");
        // redirect("summary_analysis/?test_id=examid");
        $this->session->unset_userdata('exam');
        // echo $response;
        $this->load->view('exam/finishExam');
    }

    public function online_test_for_chapter(){
        $ch_id= $this->input->get('id');
        $data['sem_id']= $this->input->get('sem_id');
        $online_test_data=$this->exam_model->grt_data('tbl_test',array('test_type'=>'4','chapter_id'=>$ch_id,'test_status'=>'active'));
        // print_r($online_test_data);
        $data['test_list']=$online_test_data;
        $this->load->view('exam/test_list_popup',$data);

    }   

    public function practiceTest_test_for_chapter(){
        $ch_id= $this->input->get('id');
        $data['sem_id']= $this->input->get('sem_id');
        $online_test_data=$this->exam_model->grt_data('tbl_test',array('test_type'=>'3','chapter_id'=>$ch_id,'test_status'=>'active'));
        // print_r($online_test_data);
        $data['test_list']=$online_test_data;
        $this->load->view('exam/test_list_popup',$data);

    }

    // public function adaptiveTestForLacture(){
    //     $ch_id= $this->input->get('id');
    //     $data['sem_id']= $this->input->get('sem_id');
    //     $online_test_data=$this->exam_model->grt_data('tbl_test',array('test_type'=>'2','lecture_id'=>$ch_id,'test_status'=>'active'));
    //     // print_r($online_test_data);
    //     $data['test_list']=$online_test_data;
    //     $this->load->view('exam/test_list_popup',$data);

    // }

    // public function practiceTestForLacture(){
    //     $ch_id= $this->input->get('id');
    //     $data['sem_id']= $this->input->get('sem_id');
    //     $online_test_data=$this->exam_model->grt_data('tbl_test',array('test_type'=>'3','lecture_id'=>$ch_id,'test_status'=>'active'));
    //     // print_r($online_test_data);
    //     $data['test_list']=$online_test_data;
    //     $this->load->view('exam/test_list_popup',$data);

    // }

} 
?> 