<?php 
 
class Orders extends CI_Controller {

public function __construct()
{
    parent::__construct();
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->model('admin/admin_common_model');
    $this->load->model('front_end/cat_frontend');
    $this->load->helper('xcrud');
    $this->load->library(array('form_validation','session','image_lib')); 
}

public function index()
{
	$data['all_orders'] = $this->cat_frontend->get_sql_record_obj("
    SELECT * FROM `product_orders`
    INNER JOIN `product_order_details` AS `product_order_details` 
    ON `product_orders`.`product_order_id` = `product_order_details`.`product_order_id` 
    INNER JOIN `tbl_students` AS `tbl_students` ON `product_orders`.`order_user_id` = `tbl_students`.`id`
    LEFT JOIN `coupons` AS `coupons` ON `coupons`.`id` = `product_orders`.`coupon_id`"); 

  

    admin_view('admin/Order/order_list', $data);
}


  public function order_view($order_id)
  {
    if(!empty($order_id))
    {
        $data['OrderDetails'] = $this->cat_frontend->getdata_orderby_where_join2('product_orders','product_order_details','product_order_id','product_order_id',array('product_orders.product_order_id'=>$order_id),'','product_orders.product_order_id',''); 

       admin_view('admin/Order/order_view', $data);
    }
    else
    {
        redirect('admin/Orders','refresh');     
    }

	
   }

   public function edit_order($order_id)
   {
      if(!empty($order_id))
      {
          $data['Order']  = $this->cat_frontend->getwheres('product_orders',array('product_order_id'=>$order_id),'');
          admin_view('admin/Order/edit_order',$data);
      }
   }

   public function update_delivery_status()
   {
      $postData = $this->input->post();

      if(!empty($postData))
      {
          $deliverStatus = array('delivery_status' => $postData['delivery_status']);

         $status =  $this->cat_frontend->update_data('product_orders',$deliverStatus,array('product_order_id'=>$postData['product_order_id']));

         
            redirect('admin/Orders' , 'refresh');
         
      }
   }


}


?>