<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class add_semester extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url','xcrud'));
        $this->load->helper('admin_template_helper');
        $this->load->model('admin/admin_common_model');
        $this->load->model('front_end/cat_frontend');
        $this->load->library(array(
            'form_validation',
            'session',
            'image_lib'
        ));
    }

    //function for managing variant list

    //function for adding product
    function add_new_semester()
    {
        if ($_POST) {
            $this->form_validation->set_rules('sem_name', 'Semester Name', 'required');
            // $this->form_validation->set_rules('sem_duration', 'Semester Duration', 'required');
            // $this->form_validation->set_rules('sem_no_of_chapter', 'No of Chapter', 'required');
            // $this->form_validation->set_rules('sem_no_of_lecture', 'No of lecture', 'required');
            // $this->form_validation->set_rules('sem_price', 'Semester Price', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $sem_name          = $this->input->post('sem_name');
                $sem_duration      = $this->input->post('sem_duration');
                $sem_no_of_chapter = $this->input->post('sem_no_of_chapter');
                $sem_no_of_lecture = $this->input->post('sem_no_of_lecture');
                $sem_price         = $this->input->post('sem_price');
                $sem_description   = $this->input->post('sem_description');
                $sem_status        = $this->input->post('sem_status');
                
                $config = array(
                    'upload_path' => "./uploads/semester",               
                    'allowed_types' => "*",
                    'overwrite' => false,
                    'file_name' => time()
                );

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
            
                if($this->upload->do_upload('sem_image')) 
                {
                    $uploadimg = $this->upload->data();
                    $uimg = $uploadimg['file_name'];
                } 
                else
                {
                    $uimg = '';
                    $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                    redirect('admin/add_semester/add_new_semester');
                    exit();
                }
                $uimg     = '';
                $sem_data = array(
                    'sem_name' => $sem_name,
                    'sem_duration' => $sem_duration,
                    'sem_image' => $uimg,
                    'sem_no_of_chapter' => $sem_no_of_chapter,
                    'sem_no_of_lecture' => $sem_no_of_lecture,
                    'sem_price' => $sem_price,
                    'sem_description' => $sem_description,
                    'sem_date' => date("Y-m-d"),
                    'sem_status' => $sem_status
                    
                );
                
                $sem_id = $this->admin_common_model->insert_data('tbl_semester', $sem_data);
                if ($sem_id > 0) {
                    $this->session->set_flashdata("success", "Course added successfully");
                } else {
                    $this->session->set_flashdata("error", "Something went wrong..");
                }
                
                redirect('admin/add_semester/semester_list');
                
            }
        }
        
        //$data['related_product']=$this->admin_common_model->fetch_record('tbl_semester');
        
        admin_view('admin/add_semester');
    }
    //function for adding semester
    //function for editing semester
    function edit_semester($sem_id)
    {
        //code for updating data
        if ($_POST) {
            $this->form_validation->set_rules('sem_name', 'Semester Name', 'required');
            // $this->form_validation->set_rules('sem_duration', 'Semester Duration', 'required');
            // $this->form_validation->set_rules('sem_no_of_chapter', 'No of Chapter', 'required');
            // $this->form_validation->set_rules('sem_no_of_lecture', 'No of lecture', 'required');
            // $this->form_validation->set_rules('sem_price', 'Semester Price', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $sem_name          = $this->input->post('sem_name');
                $sem_duration      = $this->input->post('sem_duration');
                $sem_no_of_chapter = $this->input->post('sem_no_of_chapter');
                $sem_no_of_lecture = $this->input->post('sem_no_of_lecture');
                $sem_price         = $this->input->post('sem_price');
                $sem_description   = $this->input->post('sem_description');
                $sem_status        = $this->input->post('sem_status');

                if(!empty($_FILES['sem_image']['name']))
                {

                   $config = array(
                    'upload_path' => "./uploads/semester",
                    'allowed_types' => "*",
                    'overwrite' => false,
                    'file_name' => time()
                    );

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
            
                    if($this->upload->do_upload('sem_image')) 
                    {
                        $uploadimg = $this->upload->data();
                        $uimg = $uploadimg['file_name'];
                        
                    } 
                    else
                    {
                        $uimg = '';
                        $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                        redirect('admin/add_semester/edit_product/'.$product_id);
                        exit();
                    }
                }
                else
                {
                    $uimg = $this->input->post('sem_image1');
                }
                $sem_data   = array(
                    'sem_name' => $sem_name,
                    'sem_duration' => $sem_duration,
                    'sem_image' => $uimg,
                    'sem_no_of_chapter' => $sem_no_of_chapter,
                    'sem_no_of_lecture' => $sem_no_of_lecture,
                    'sem_price' => $sem_price,
                    'sem_description' => $sem_description,
                    'sem_status' => $sem_status
                );
                $where      = array(
                    'sem_id' => $sem_id
                );
                $sem_update = $this->admin_common_model->update_data('tbl_semester', $sem_data, $where);
                $this->session->set_flashdata("success", "Record Updated successfully");
                redirect('admin/add_semester/semester_list');

            }
        }
        
        $where = array(
            'sem_id' => $sem_id
        );
        $data['sem_detail'] = $this->admin_common_model->getwheres('tbl_semester', $where);
        admin_view('admin/edit_semester', $data);
    }
    //function for editing semester
    
    //function for deleting semester
    function delete_semester($sem_id)
    {
        $product_detail = $this->admin_common_model->getwheres('tbl_semester', array(
            'sem_id' => $sem_id
        ));
        
        if (!empty($product_detail)) {
            //delete product
            $where = array(
                'sem_id' => $sem_id
            );
            $deleted_product = $this->admin_common_model->delete_data('tbl_semester', $where);
            
            if ($deleted_product == TRUE) {
                $this->session->set_flashdata('success', "Course deleted successfully.");
            } else {
                $this->session->set_flashdata('success', "Something went wrong!!Please try again");
            }
        }
        redirect('admin/add_semester/semester_list');
        
    }
    //function for deleting product
    //function for add Chapter
    function add_new_chapter()
    {
       
        if ($_POST) {

                $sem_id           = $this->input->post('product_id');
                $ch_name          = $this->input->post('ch_name');
                //$ch_duration      = $this->input->post('ch_duration');
                $ch_no_of_lecture = $this->input->post('ch_no_of_lecture');
                $chapter_number   = $this->input->post('chapter_number');
                $ch_description   = $this->input->post('ch_description');
                $ch_status        = $this->input->post('ch_status');
                
             
                
                $ch_data = array(
                    'sem_id'            => $sem_id,
                    'ch_name'           => $ch_name,
                   // 'ch_image'          => $uimg,
                    //'ch_duration'       => $ch_duration,
                    'ch_no_of_lecture'  => $ch_no_of_lecture,
                    'chapter_number'    => $chapter_number,
                    'ch_description'    => $ch_description,
                    'ch_date'           => date("Y-m-d"),
                    'ch_status'         => $ch_status
                    
                );
                
                $ch_id = $this->admin_common_model->insert_data('tbl_chapter', $ch_data);
                if ($ch_id > 0) {
                    echo "SUCCESS";
                    die();
                } else {
                    $this->session->set_flashdata("error", "Something went wrong..");
                }
                                
           }
        
        $data['course_list'] =  $this->admin_common_model->getwheres('products', array(
            'product_type' => 'e_learning','deleted'=>'0'));
        
        admin_view('admin/add_chapter', $data);


    }
    //function for editing chpter
    function edit_chapter($ch_id="")
    {

        if ($_POST) {
           
                $sem_id           = $this->input->post('product_id');
                $ch_name          = $this->input->post('ch_name');
                $ch_no_of_lecture = $this->input->post('ch_no_of_lecture');
                $chapter_number   = $this->input->post('chapter_number');
                $ch_description = $this->input->post('ch_description');
                $ch_status      = $this->input->post('ch_status');
                $ch_id1         = $this->input->post('ch_id');
                
             
                $ch_data    = array(
                    'sem_id' => $sem_id,
                    'ch_name' => $ch_name,
                    //'ch_image' => $uimg,
                   // 'ch_duration' => $ch_duration,
                    'ch_no_of_lecture' => $ch_no_of_lecture,
                    'chapter_number' => $chapter_number,
                    'ch_description' => $ch_description,
                    'ch_date' => date("Y-m-d"),
                    'ch_status' => $ch_status
                    
                );
                $where      = array(
                    'ch_id' => $ch_id1
                );
                $sem_update = $this->admin_common_model->update_data('tbl_chapter', $ch_data, $where);

                echo "SUCCESS";
                die();
                
               // $this->session->set_flashdata("success", "Chapter updated successfully");
                //redirect('admin/add_semester/chapter_list');
           
        }
        
        $where = array('ch_id' => $ch_id);
        $data['chapter_detail'] = $this->admin_common_model->getwheres('tbl_chapter', $where);
        
        $data['course_list'] =    $this->admin_common_model->getwheres('products', array(
            'product_type' => 'e_learning','deleted'=>'0'));
        
        admin_view('admin/edit_chapter', $data);
        
    }
    //function for add Lacture Type
    function add_lecture_type()
    {
        if ($_POST) {
            $this->form_validation->set_rules('type_name', 'Topic Type', 'required');
            // $this->form_validation->set_rules('type_description', 'Chapter Name', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $type_name        = $this->input->post('type_name');
                $type_description = $this->input->post('type_description');
                $type_status      = $this->input->post('type_status');

                $type_data = array(
                    'type_name' => $type_name,
                    'type_description' => $type_description,
                    'type_date' => date("Y-m-d"),
                    'type_status' => $type_status
                    
                );
                // echo "<pre>";
                // print_r($ch_data);
                // die();
                
                $type_id = $this->admin_common_model->insert_data('tbl_lecture_type', $type_data);
                if ($type_id > 0) {
                    $this->session->set_flashdata("success", "Topic Type added successfully");
                } else {
                    $this->session->set_flashdata("error", "Something went wrong..");
                }
                
                redirect('admin/add_semester/lecture_type_list');
                
            }
        }
        
        //$data['semester_list']=$this->admin_common_model->fetch_record('tbl_semester');
        
        admin_view('admin/add_lecture_type');
    }
    //function Lecture type edit
    function edit_lecture_type($type_id)
    {
        //code for updating data
        if ($_POST) {
            $this->form_validation->set_rules('type_name', 'Topic Type', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $type_name        = $this->input->post('type_name');
                $type_description = $this->input->post('type_description');
                $type_status      = $this->input->post('type_status');
                
                $type_data = array(
                    'type_name' => $type_name,
                    'type_description' => $type_description,
                    'type_status' => $type_status
                    
                );
                
                $where      = array(
                    'type_id' => $type_id
                );
                $sem_update = $this->admin_common_model->update_data('tbl_lecture_type', $type_data, $where);
                
                $this->session->set_flashdata("success", "Topic Type updated successfully");
                redirect('admin/add_semester/lecture_type_list');
                
            }
        }
        
        $where               = array(
            'type_id' => $type_id
        );
        $data['type_detail'] = $this->admin_common_model->getwheres('tbl_lecture_type', $where);
        
        admin_view('admin/edit_lecture_type', $data);
        
    }
    //function for add lecture
    function add_new_lecture()
    {
        
        if ($_POST) {

                $course_id         = $this->input->post('product_id');  
                $chapter_id        = $this->input->post('chapter_id');
                $lecture_type      = $this->input->post('lecture_type');
                $lect_name         = $this->input->post('lect_name');
                $lect_video_url    = $this->input->post('lect_video_url');
                $video_pass        = $this->input->post('video_pass');
                $lect_duration     = $this->input->post('lect_duration');
                $lect_description  = $this->input->post('lect_description');
                $lect_status       = $this->input->post('lect_status');
               
                if($_FILES['content']['error'] !=4)
                {

                     $config = array(
                            'upload_path' => "./uploads/semester",              
                            'allowed_types' => "*",
                            'overwrite' => false,
                            'file_name' => $_FILES['content']['name']
                        );
                        
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);


                        if($this->upload->do_upload('content')) 
                        {
                            $uploadimg = $this->upload->data();

                            $lect_resource = $uploadimg['file_name'];
                        }
                        else
                        {

                            $lect_resource = "";
                        }
                }
                else
                {
                    $lect_resource = "";
                }

              
                $lecture_data = array(
                    'course_id' => $course_id,
                    'ch_id' => $chapter_id,
                    'lecture_type' => $lecture_type,
                    'lect_name' => $lect_name,
                    'lect_video_url' => $lect_video_url,
                    'video_pass' => $video_pass,
                    'lect_duration' => $lect_duration,
                    'lect_description' => $lect_description,
                    'lect_resource' => $lect_resource,
                    'lect_date' => date("Y-m-d"),
                    'lect_status' => $lect_status
                    
                );
               
                
                $lact_id = $this->admin_common_model->insert_data('tbl_lecture', $lecture_data);
                if ($lact_id > 0) {
                   echo "SUCCESS";
                   die();
                } else {
                    $this->session->set_flashdata("error", "Something went wrong..");
                }
                
                redirect('admin/add_semester/lecture_list');
                
           
        }

         $data['course_list'] = $this->admin_common_model->getwheres('products',array('product_type'=>'e_learning','deleted'=>'0'));
        $data['chapter_list'] = $this->admin_common_model->getwheres('tbl_chapter',array('ch_status'=>'active'));
        $data['lecture_type'] = $this->admin_common_model->getwheres('tbl_lecture_type',array('type_status'=>'active'));
        
        admin_view('admin/add_lecture', $data);
    }
    //function for editing Lecture
    function edit_lecture($lect_id="")
    {
        //code for updating data
        if ($_POST) {
           
            
                $course_id         = $this->input->post('product_id');
                $ch_id             = $this->input->post('ch_id');
                $lecture_type      = $this->input->post('lecture_type');
                $lect_name         = $this->input->post('lect_name');
                $lect_video_url    = $this->input->post('lect_video_url');
                $video_pass        = $this->input->post('video_pass');
                $lect_duration     = $this->input->post('lect_duration');
                $lect_description  = $this->input->post('lect_description');
                $lect_status       = $this->input->post('lect_status');
                $lectId1           = $this->input->post('lect_id');
       

                 if($_FILES['content']['error'] !=4)
                {

                     $config = array(
                            'upload_path' => "./uploads/semester",              
                            'allowed_types' => "*",
                            'overwrite' => false,
                            'file_name' => $_FILES['content']['name']
                        );
                        
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                      
                        if($this->upload->do_upload('content')) 
                        {
                            $uploadimg = $this->upload->data();
                            $lect_resource = $uploadimg['file_name'];
                        }
                        else
                        {
                            $lect_resource = "";
                        }
                }
                else
                {
                    $lect_resource = "";
                }


                
                $lecture_data = array(
                    'course_id' => $course_id,
                    'ch_id' => $ch_id,
                    'lecture_type' => $lecture_type,
                    'lect_name' => $lect_name,
                    'lect_video_url' => $lect_video_url,
                    'video_pass' => $video_pass,
                    'lect_duration' => $lect_duration,
                    'lect_description' => $lect_description,
                    'lect_resource' => $lect_resource,
                    'lect_date' => date("Y-m-d"),
                    'lect_status' => $lect_status
                );
                
                $where = array(
                    'lect_id' => $lectId1
                );
                $this->admin_common_model->update_data('tbl_lecture', $lecture_data, $where);
                
                echo "SUCCESS";
                die();
        }
        
        $where = array(
            'lect_id' => $lect_id
        );
        $data['lecture_detail']= $this->admin_common_model->getwheres('tbl_lecture', $where);
       $data['course_list'] = $this->admin_common_model->getwheres('products',array('product_type'=>'e_learning','deleted'=>'0'));  

        $data['chapter_list'] = $this->admin_common_model->fetch_record('tbl_chapter');
        $data['lecture_type'] = $this->admin_common_model->getwheres('tbl_lecture_type',array('type_status'=>'active'));
        
        
        admin_view('admin/edit_lecture', $data);
    }
    //function for Semester list
    function semester_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_semester');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        
        $xcrud->change_type('sem_image', 'image', false, array(
            'width' => 450,
            'path' => '../uploads/semester',
            'thumbs' => array(
                array(
                    'height' => 55,
                    'width' => 120,
                    'crop' => true,
                    'marker' => '_th'
                )
            )
        ));
        
        
        $xcrud->columns("sem_name,sem_date,sem_status", true);
        
        $xcrud->label('sem_name', 'Name');
        $xcrud->label('sem_duration', 'Duration(In Hours)');
        $xcrud->label('sem_no_of_chapter', 'No of Chapter');
        $xcrud->label('sem_no_of_lecture', 'No of Topic');
        $xcrud->label('sem_price', 'Price');
        // $xcrud->label('sem_image','Image');
        $xcrud->label('sem_description', 'Description');
        $xcrud->label('sem_date', 'Date');
        $xcrud->label('sem_status', 'Course Status');
        
        $xcrud->button('admin/add_semester/edit_semester/{sem_id}', 'Edit Course', 'glyphicon glyphicon-edit');
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Course List';
        admin_view('admin/semester_list', $data);
        
    }

    /* function for subject list*/
    function subject_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_subject');
        /**********view pages***********/
        $xcrud->unset_remove();
        // $xcrud->unset_add();
        // $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $data['content'] = $xcrud->render();
        $data['title']   = 'Subject List';
        admin_view('admin/subject_list', $data);
        
    }
    //function for Chapter list
    function chapter_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_chapter');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->order_by('ch_id', 'desc');
       
        
        $xcrud->relation('sem_id', 'products', 'product_id', 'pname');
        
        $xcrud->columns("sem_id,ch_name,ch_no_of_lecture,chapter_number,ch_date,ch_status", true);
        
        $xcrud->label('sem_id', 'Course Name');
        $xcrud->label('ch_name', 'Chapter Name');
        $xcrud->label('ch_no_of_lecture', 'No of Topic');
        $xcrud->label('chapter_number', 'Chapter No');
        $xcrud->label('ch_date', 'Date');
        $xcrud->label('ch_status', 'Status');
        
        $xcrud->button('admin/add_semester/edit_chapter/{ch_id}', 'Edit Chapter', 'glyphicon glyphicon-edit');
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Chapter List';
        admin_view('admin/chapter_list', $data);
        
    }
    //function for Lacture list
    function lecture_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_lecture');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        
        $xcrud->relation('ch_id', 'tbl_chapter', 'ch_id', 'ch_name');
        $xcrud->relation('lecture_type', 'tbl_lecture_type', 'type_id', 'type_name');
        $xcrud->columns("ch_id,lecture_type,lect_name,lect_video_url,video_pass,lect_duration,lect_description,lect_date,lect_status", true);
        $xcrud->label('ch_id', 'Chapter Name');
        $xcrud->label('lecture_type', 'Topic Type');
        $xcrud->label('lect_name', 'Topic Name');
        $xcrud->label('lect_video_url', 'Video URL');
        $xcrud->label('video_pass', 'Video Pass');
        $xcrud->label('lect_duration', 'Duration');
        $xcrud->label('lect_defi_lable', 'Difficulty Level');
        $xcrud->label('lect_concept_name', 'Concept Name');
        $xcrud->label('lect_no_of_views', 'No of Views');
        $xcrud->label('lect_description', 'Description');
        $xcrud->label('lect_date', 'Date');
        $xcrud->label('lect_status', 'Status');
        $xcrud->label('lect_is_locked', 'Is Locked');
        $xcrud->label('friendship_points', 'Friendship Points');
        
        $xcrud->button('admin/add_semester/edit_lecture/{lect_id}', 'Edit Topic', 'glyphicon glyphicon-edit');
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Topic List';
        admin_view('admin/lecture_list', $data);
        
    }
    
    function lecture_type_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_lecture_type');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        
        
        $xcrud->columns("type_name,type_date,type_status", true);
        
        $xcrud->label('type_name', 'Topic Type Name');
        // $xcrud->label('type_description','Description');
        $xcrud->label('type_date', 'Date');
        $xcrud->label('type_status', 'Status');
        
        // $xcrud->button('edit_lecture_type/{type_id}','Edit Topic Type','glyphicon glyphicon-edit');
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Topic Type List';
        admin_view('admin/lecture_type_list', $data);
        
    }
    //function for Add exam question
    function add_test_type()
    {
        if ($_POST) {
            $this->form_validation->set_rules('test_type', 'Test Type', 'required');
            // $this->form_validation->set_rules('type_description', 'Chapter Name', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $test_type = $this->input->post('test_type');
                $status    = $this->input->post('status');
                
                
                $type_data = array(
                    'test_type' => $test_type,
                    'status' => $status
                );
                // echo "<pre>";
                // print_r($ch_data);
                // die();
                
                $type_id = $this->admin_common_model->insert_data('tbl_test_type', $type_data);
                if ($type_id > 0) {
                    $this->session->set_flashdata("success", "Test Type added successfully");
                } else {
                    $this->session->set_flashdata("error", "Something went wrong..");
                }
                
                redirect('admin/add_semester/test_type_list');
                
            }
        }
        
        admin_view('admin/add_test_type');
    }
    
    function edit_test_type($id)
    {
        //code for updating data
        if ($_POST) {
            $this->form_validation->set_rules('test_type', 'Test Type', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $test_type = $this->input->post('test_type');
                $status    = $this->input->post('status');
                
                
                $type_data = array(
                    'test_type' => $test_type,
                    'status' => $status
                );
                
                $where      = array(
                    'id' => $id
                );
                $sem_update = $this->admin_common_model->update_data('tbl_test_type', $type_data, $where);
                
                $this->session->set_flashdata("success", "Test Type updated successfully");
                redirect('admin/add_semester/test_type_list');
                
            }
        }
        
        $where               = array(
            'id' => $id
        );
        $data['type_detail'] = $this->admin_common_model->getwheres('tbl_test_type', $where);
        
        admin_view('admin/edit_test_type', $data);
        
    }
    
    function test_type_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_test_type');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        
        $xcrud->button('edit_test_type/{id}', 'Edit Test Type', 'glyphicon glyphicon-edit');
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Test Type List';
        admin_view('admin/test_type_list', $data);
        
    }
    
    //function for add test
    function add_test()
    {
         if($_POST)
        {
            // $this->form_validation->set_rules('test_type', 'Test Type', 'required');
            $this->form_validation->set_rules('test_name', 'Test Name', 'required');
            $this->form_validation->set_rules('total_marks', 'Total Marks', 'required');
            // $this->form_validation->set_rules('test_passing_marks', 'Passing Marks', 'required');
            
            if($this->form_validation->run() == FALSE)
            {

            }
            else
            {
                $lecture_id = $this->input->post('lecture_id');
                $chapter_id = $this->input->post('chapter_id');
                $sem_id = $this->input->post('sem_id');
                $test_type = $this->input->post('test_type');
                $test_name = $this->input->post('test_name');
                $ideal_time_duration = $this->input->post('ideal_time_duration');
                $test_no_of_question = $this->input->post('test_no_of_question');
                // $test_passing_marks = $this->input->post('test_passing_marks');
                $test_energy_point = $this->input->post('test_energy_point');
                $description_url = $this->input->post('description_url');
                $desc_video_pass = $this->input->post('desc_video_pass');
                $total_marks = $this->input->post('total_marks');
                $test_status = $this->input->post('test_status');
                $date_start = $this->input->post('date10').':00';
                $date_end = $this->input->post('date11').':00';

                if(strtotime($date_start)>strtotime($date_end)){
                    $this->session->set_flashdata("error","Something went wrong..");   
                    redirect('admin/add_semester/add_test');
                    exit;
                }
                

                // echo $date_start;
                // die;
                $test_data = array(
                    'lecture_id'=>$lecture_id,
                    'chapter_id'=>$chapter_id,
                    'sem_id'=>$sem_id,
                    'test_type'=>$test_type,
                    'test_name'=>$test_name,
                    'ideal_time_duration'=>$ideal_time_duration,
                    'test_no_of_question'=>$test_no_of_question,
                    // 'test_passing_marks'=>$test_passing_marks,
                    'test_timestemp'=>date('Y-m-d g:i a'),
                    'test_energy_point'=>$test_energy_point,
                    'description_url'=>$description_url,
                    'desc_video_pass'=>$desc_video_pass,
                    'total_marks'=>$total_marks,
                    'test_status'=>$test_status,
                    'appear_time'=>$date_start,
                    'disappear_time'=>$date_end,
                    
                );
                // echo "<pre>";
                // print_r($test_data);
                // die();

                $test_id = $this->admin_common_model->insert_data('tbl_test',$test_data);
                // echo $this->db->last_query();die;
                if($test_id >0)
                {
                    $this->session->set_flashdata("success","Test added successfully");
                }
                else
                {
                  $this->session->set_flashdata("error","Something went wrong..");   
                }
                
                redirect('admin/add_semester/test_list'); 

            }           
        }
        
       

         $data['course_list'] = $this->admin_common_model->getwheres('products',array('product_type'=>'e_learning','deleted'=>'0'));

        admin_view('admin/add_test', $data);
    }
    
    function test_list()
    {
        $data['title'] = 'Test List';
        
        $data['test_detail'] = $this->admin_common_model->fetch_record('tbl_test');
    
        admin_view('admin/test_list', $data);
        
    }
    //function for editing Test
  function edit_test($test_id)
{
    //code for updating data
    if($_POST)
    {
       
        $this->form_validation->set_rules('test_name', 'Test Name', 'required');
        $this->form_validation->set_rules('ideal_time_duration','Ideal Time Duration','required');
        
        $this->form_validation->set_rules('total_marks', 'Total Marks', 'required');
     

        if($this->form_validation->run() == FALSE)
        {

        }
        else
        {

            $lecture_id = $this->input->post('lecture_id');
            $chapter_id = $this->input->post('chapter_id');
            $sem_id = $this->input->post('sem_id');
            $test_type = $this->input->post('test_type');
            $test_name = $this->input->post('test_name');
            $ideal_time_duration = $this->input->post('ideal_time_duration');
            $test_no_of_question = $this->input->post('test_no_of_question');
            // $test_passing_marks = $this->input->post('test_passing_marks');
            $test_energy_point = $this->input->post('test_energy_point');
            $description_url = $this->input->post('description_url');
            $desc_video_pass = $this->input->post('desc_video_pass');
            $total_marks = $this->input->post('total_marks');
            $test_status = $this->input->post('test_status');
            $date_start = $this->input->post('date10').':00';
            $date_end = $this->input->post('date11').':00';

            $test_data = array(
                'lecture_id'=>$lecture_id,
                'chapter_id'=>$chapter_id,
                'sem_id'=>$sem_id,
                'test_type'=>$test_type,
                'test_name'=>$test_name,
                'ideal_time_duration'=>$ideal_time_duration,
                'test_no_of_question'=>$test_no_of_question,
                'test_timestemp'=>date('Y-m-d g:i a'),
                'test_energy_point'=>$test_energy_point,
                'description_url'=>$description_url,
                'desc_video_pass'=>$desc_video_pass,
                'total_marks'=>$total_marks,
                'test_status'=>$test_status,
                'appear_time'=>$date_start,
                'disappear_time'=>$date_end,
                
            );
          
                
            $where = array('test_id'=>$test_id);
            $this->admin_common_model->update_data('tbl_test',$test_data,$where);
        
            $this->session->set_flashdata("success","Test updated successfully");
            redirect('admin/add_semester/test_list'); 

        }           
    }

    $where = array('test_id'=>$test_id);
    $data['test_detail']=$this->admin_common_model->getwheres('tbl_test',$where);

    $data['course_list'] = $this->admin_common_model->getwheres('products',array('product_type'=>'e_learning','deleted'=>'0'));

    $data['chapter_list'] = $this->admin_common_model->getwheres('tbl_chapter',array('ch_status'=>'active'));


    admin_view('admin/edit_test',$data);
}
    
    function manage_question()
    {
        $test_id       = $this->uri->segment(4);
        $data['title'] = 'Manage Question';
        
        $lect_id = $this->input->get('lect_id');
        $ch_id   = $this->input->get('ch_id');
        $sem_id  = $this->input->get('sem_id');
        
        
        // if ($ch_id != '0') {
            
        //     $wheres               = array(
        //         'ch_id' => $ch_id
        //     );
        //     $data['lecture_list'] = $this->admin_common_model->getwheres('tbl_lecture', $wheres);
            
        // } elseif ($sem_id != '0') {
            
        //     $wheres = array(
        //         'sem_id' => $sem_id
        //     );
        // }
        // $data['chapter_list'] = $this->admin_common_model->fetch_record('tbl_subject');
        
        
        $where = array( 'test_id' => $test_id );
        $data['test_detail'] = $this->admin_common_model->getwheres('tbl_test', $where);
        // print_r($data['test_detail']);
        // die();
        
        $question_detail = $this->admin_common_model->getwheres('tbl_add_question', $where);
        // echo $this->db->last_query();
        $data['question_detail'] = $question_detail;
        // $lect                    = array();
        // $chap                    = array();
        // if ($question_detail[0]['lecture_id'] != '0') {
            
        //     if (!empty($question_detail)) {
                
        //         foreach ($question_detail as $lecture) {
        //             $lecture_id           = $lecture['lecture_id'];
        //             $wheres               = array(
        //                 'lect_id' => $lecture_id
        //             );
        //             $data['lecture_view'] = $this->admin_common_model->getwheres('tbl_lecture', $wheres);
        //             $lecture['lect_name'] = $data['lecture_view'][0]['lect_name'];
        //             $lect[]               = $lecture;
        //         }
        //     }
        //     $data['question_detail'] = $lect;
        // } elseif ($question_detail[0]['chapter_id'] != '0') {
        //     if (!empty($question_detail)) {
        //         foreach ($question_detail as $chapter) {
        //             $chapter_id           = $chapter['chapter_id'];
        //             $where                = array(
        //                 'ch_id' => $chapter_id
        //             );
        //             $data['chapter_view'] = $this->admin_common_model->getwheres('tbl_chapter', $where);
                    
        //             $chapter['ch_name'] = $data['chapter_view'][0]['ch_name'];
        //             $chap[]             = $chapter;
        //         }
        //     }
        //     $data['question_detail'] = $chap;
        // }
        $data['lect_id'] = $lect_id;
        $data['ch_id']   = $ch_id;
        $data['sem_id']  = $sem_id;
        
        admin_view('admin/manage-question', $data);
    }
    //function for add lecture
    function add_question()
    {
        if ($_POST) {
            // $this->form_validation->set_rules('test_id', 'Test Name', 'required');
            $this->form_validation->set_rules('question_desc', 'Question', 'required');
            $this->form_validation->set_rules('ans_1', 'Answer 1', 'required');
            $this->form_validation->set_rules('ans_2', 'Answer 2', 'required');
            $this->form_validation->set_rules('ans_3', 'Answer 3', 'required');
            $this->form_validation->set_rules('ans_4', 'Answer 4', 'required');
            // $this->form_validation->set_rules('explain_right','Right Answer Exaplaination','required');
            $this->form_validation->set_rules('ques_right_ans', 'Right Question', 'required');
            // $this->form_validation->set_rules('que_marks', 'Marks', 'required');
            // $this->form_validation->set_rules('ques_negative_marks', 'Negative Marks', 'required');
            // $this->form_validation->set_rules('ques_concept_wise', 'Concept Wise', 'required');
            // $this->form_validation->set_rules('ques_difficulty_level','Difficulty Level','required');
            
            if ($this->form_validation->run() == FALSE) {
                return false;
                
            } else {
                // $segment_id = $this->input->post('segment_id');
                $lect_id               = $this->input->post('lect_id');
                $ch_id                 = $this->input->post('ch_id');
                $sem_id                = $this->input->post('sem_id');
                $test_id               = $this->input->post('test_id');
                $question_desc         = trim(str_replace('&nbsp;', '', $this->input->post('question_desc')));
                $ans_1                 = trim(str_replace('&nbsp;', '', $this->input->post('ans_1')));
                $ans_2                 = trim(str_replace('&nbsp;', '', $this->input->post('ans_2')));
                $ans_3                 = trim(str_replace('&nbsp;', '', $this->input->post('ans_3')));
                $ans_4                 = trim(str_replace('&nbsp;', '', $this->input->post('ans_4')));
                $ques_right_ans        = trim(str_replace('&nbsp;', '', $this->input->post('ques_right_ans')));
                $explain_right_ans     = trim(str_replace('&nbsp;', '', $this->input->post('explain_right')));
                $que_marks             = $this->input->post('que_marks');
                $ques_negative_marks   = $this->input->post('ques_negative_marks');
                $ques_concept_wise     = $this->input->post('ques_concept_wise');
                $ques_difficulty_level = $this->input->post('ques_difficulty_level');
                $chapter_id            = $this->input->post('chapter_id');
                $lecture_id            = $this->input->post('lecture_id');
                $ques_status           = $this->input->post('ques_status');
                
                if (empty($chapter_id)) {
                    
                    $chapter_id = '0';
                }
                if (empty($lecture_id)) {
                    
                    $lecture_id = '0';
                }
                
                $config = array(
                    'upload_path' => "./uploads/Question",
                    'allowed_types' => "*",
                    'overwrite' => false,
                    'file_name' => time()
                );
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if ($this->upload->do_upload('attachment_img')) {
                    $uploadimg = $this->upload->data();
                    $uimg      = $uploadimg['file_name'];
                } else {
                    $uimg = '';
                }
                
                $question_data = array(
                    'test_id' => $test_id,
                    'question_desc' => $question_desc,
                    'ans_1' => $ans_1,
                    'ans_2' => $ans_2,
                    'ans_3' => $ans_3,
                    'ans_4' => $ans_4,
                    'ques_right_ans' => $ques_right_ans,
                    'explain_right_answer' => $explain_right_ans,
                    'que_marks' => $que_marks,
                    'ques_negative_marks' => $ques_negative_marks,
                    'ques_concept_wise' => $ques_concept_wise,
                    'attachment_img' => $uimg,
                    'ques_difficulty_level' => $ques_difficulty_level,
                    'chapter_id' => 1,
                    'lecture_id' => $lecture_id,
                    'ques_status' => $ques_status
                    
                );
                
                $ques_id = $this->admin_common_model->insert_data('tbl_add_question', $question_data);
                if ($ques_id > 0) {
                    $this->session->set_flashdata("success", "Question added successfully");
                } else {
                    $this->session->set_flashdata("error", "Something went wrong..");
                }
                
                redirect("admin/add_semester/manage_question/$test_id/?lect_id=$lect_id&ch_id=$ch_id&sem_id=$sem_id");
                
            }
        }
        
        admin_view('admin/add_test');
    }
    
    //function for editing chpter
    function edit_question()
    {
        $ques_id = $this->uri->segment(4);
        $lect_id = $this->input->get('lect_id');
        $ch_id   = $this->input->get('ch_id');
        $sem_id  = $this->input->get('sem_id');
        $test_id =  $this->input->get('test_id');
        
        // if ($ch_id != '0') {
        //     $wheres = array( 'ch_id' => $ch_id );
        //     $data['lecture_list'] = $this->admin_common_model->getwheres('tbl_lecture', $wheres);
        //     $segment_id           = $ch_id;
        // } elseif ($sem_id != '0') {
            
        //     $wheres               = array(
        //         'sem_id' => $sem_id
        //     );
        //     $data['chapter_list'] = $this->admin_common_model->getwheres('tbl_chapter', $wheres);
        //     $segment_id           = $sem_id;
        // }
        // $data['chapter_list'] = $this->admin_common_model->fetch_record('tbl_subject');
        
        if ($_POST) {
            $this->form_validation->set_rules('question_desc', 'Question', 'required');
            $this->form_validation->set_rules('ans_1', 'Answer 1', 'required');
            $this->form_validation->set_rules('ans_2', 'Answer 2', 'required');
            $this->form_validation->set_rules('ans_3', 'Answer 3', 'required');
            $this->form_validation->set_rules('ans_4', 'Answer 4', 'required');
            $this->form_validation->set_rules('ques_right_ans', 'Right Question', 'required');
            // $this->form_validation->set_rules('que_marks', 'Marks', 'required');
            // $this->form_validation->set_rules('ques_negative_marks', 'Negative Marks', 'required');
            // $this->form_validation->set_rules('ques_concept_wise', 'Concept Wise', 'required');
            // $this->form_validation->set_rules('ques_difficulty_level','Difficulty Level','required');
            // $this->form_validation->set_rules('explain_right', 'Right Answer Exaplaination', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $test_id               = $this->input->post('test_id');
                $question_desc         = trim(str_replace('&nbsp;', '', $this->input->post('question_desc')));
                $ans_1                 = trim(str_replace('&nbsp;', '', $this->input->post('ans_1')));
                $ans_2                 = trim(str_replace('&nbsp;', '', $this->input->post('ans_2')));
                $ans_3                 = trim(str_replace('&nbsp;', '', $this->input->post('ans_3')));
                $ans_4                 = trim(str_replace('&nbsp;', '', $this->input->post('ans_4')));
                $ques_right_ans        = trim(str_replace('&nbsp;', '', $this->input->post('ques_right_ans')));
                $explain_right_ans     = trim(str_replace('&nbsp;', '', $this->input->post('explain_right')));
                $que_marks             = $this->input->post('que_marks');
                $ques_negative_marks   = $this->input->post('ques_negative_marks');
                $ques_concept_wise     = $this->input->post('ques_concept_wise');
                $ques_difficulty_level = $this->input->post('ques_difficulty_level');
                $chapter_id            = $this->input->post('chapter_id');
                $lecture_id            = $this->input->post('lecture_id');
                $ques_status           = $this->input->post('ques_status');
                
                if (empty($lecture_id)) {
                    
                    $lecture_id = $lect_id;
                }
                
                
                // if (!empty($_FILES['attachment_img']['name'])) {
                    
                //     $config = array(
                //         'upload_path' => "./uploads/Question",
                //         'allowed_types' => "*",
                //         'overwrite' => false,
                //         'file_name' => time()
                //     );
                    
                //     $this->load->library('upload', $config);
                //     $this->upload->initialize($config);
                    
                //     if ($this->upload->do_upload('attachment_img')) {
                //         $uploadimg = $this->upload->data();
                //         $uimg      = $uploadimg['file_name'];
                //     } else {
                //         $uimg = '';
                //         // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                //         // redirect('admin/add_semester/edit_question/'.$ques_id);
                //         // exit();
                //     }
                // } else {
                //     $uimg = $this->input->post('attachment_img1');
                // }
                
                $question_data = array(
                    'test_id' => $test_id,
                    'question_desc' => $question_desc,
                    'ans_1' => $ans_1,
                    'ans_2' => $ans_2,
                    'ans_3' => $ans_3,
                    'ans_4' => $ans_4,
                    'ques_right_ans' => $ques_right_ans,
                    'explain_right_answer' => $explain_right_ans,
                    'que_marks' => $que_marks,
                    'ques_negative_marks' => $ques_negative_marks,
                    'ques_concept_wise' => $ques_concept_wise,
                    // 'attachment_img' => $uimg,
                    'ques_difficulty_level' => $ques_difficulty_level,
                    'chapter_id' => 1,
                    'lecture_id' => $lecture_id,
                    'ques_status' => $ques_status
                    
                );
                
                $where = array( 'ques_id' => $ques_id );
                $sem_update = $this->admin_common_model->update_data('tbl_add_question', $question_data, $where);
                
                $this->session->set_flashdata("success", "Question updated successfully");
                redirect("admin/add_semester/manage_question/$test_id/?lect_id=$lect_id&ch_id=$ch_id&sem_id=$sem_id");
                
            }
        }
        
        $where = array( 'test_id' => $test_id );
        $data['test_detail']     = $this->admin_common_model->getwheres('tbl_test', $where);

        $where = array( 'ques_id' => $ques_id );
        $data['question_detail'] = $this->admin_common_model->getwheres('tbl_add_question', $where);
        
        $data['lect_id'] = $lect_id;
        $data['ch_id']   = $ch_id;
        $data['sem_id']  = $sem_id;

        admin_view('admin/edit_question', $data);
    }
    
    function free_video_rsm()
    {
        if ($_POST) {
            $name      = $this->input->post('name');
            $video_url = $this->input->post('video_url');
            $duration  = $this->input->post('duration');
            $status    = $this->input->post('status');
            
            $data = array(
                'name' => $name,
                'video_url' => $video_url,
                'duration' => $duration,
                'status' => $status
                
            );
            
            $res = $this->admin_common_model->insert_data('tbl_free_video', $data);
            if ($res > 0) {
                $this->session->set_flashdata("success", "Video added successfully");
            } else {
                $this->session->set_flashdata("error", "Something went wrong..");
            }
            
            redirect("admin/add_semester/free_video_list");
        }
        
        admin_view('admin/free_video_rsm');
    }
    function free_video_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_free_video');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        
        $xcrud->button('edit_free_video/{id}', 'Edit Free Video', 'glyphicon glyphicon-edit');
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        // $xcrud->label('video_url','Video');
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Free Ride RSM List';
        admin_view('admin/free_video_list', $data);
        
    }
    function edit_free_video($id)
    {
        //code for updating data
        if ($_POST) {
            $name      = $this->input->post('name');
            $video_url = $this->input->post('video_url');
            $duration  = $this->input->post('duration');
            $status    = $this->input->post('status');
            
            
            $data = array(
                'name' => $name,
                'video_url' => $video_url,
                'duration' => $duration,
                'status' => $status
                
            );
            
            $where = array(
                'id' => $id
            );
            $this->admin_common_model->update_data('tbl_free_video', $data, $where);
            
            $this->session->set_flashdata("success", "Free Video To RSM Updated Successfully");
            redirect('admin/add_semester/free_video_list');
            
        }
        
        $where                = array(
            'id' => $id
        );
        $data['video_detail'] = $this->admin_common_model->getwheres('tbl_free_video', $where);
        
        admin_view('admin/edit_free_video', $data);
        
    }
    
    function add_features()
    {
        if ($_POST) {
            $name      = $this->input->post('name');
            $content   = $this->input->post('content');
            $full_desc = $this->input->post('full_desc');
            $status    = $this->input->post('status');
            
            $config = array(
                'upload_path' => "./uploads/features-icon",
                'allowed_types' => "*",
                'overwrite' => false,
                'file_name' => time()
            );
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if ($this->upload->do_upload('icon')) {
                $uploadimg = $this->upload->data();
                $icon      = $uploadimg['file_name'];
            } else {
                $icon = '';
                // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                // redirect('admin/add_semester/add_new_chapter');
                // exit();
            }
            
            $config = array(
                'upload_path' => "./uploads/features-image",
                'allowed_types' => "*",
                'overwrite' => false,
                'file_name' => time()
            );
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if ($this->upload->do_upload('image')) {
                $uploadimg = $this->upload->data();
                $uimg      = $uploadimg['file_name'];
            } else {
                $uimg = '';
                // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                // redirect('admin/add_semester/add_new_chapter');
                // exit();
            }
            
            $data = array(
                'name' => $name,
                'icon' => $icon,
                'image' => $uimg,
                'content' => $content,
                'full_desc' => $full_desc,
                'status' => $status
            );
            
            $res = $this->admin_common_model->insert_data('tbl_front_features', $data);
            if ($res > 0) {
                $this->session->set_flashdata("message", "Features added successfully");
            } else {
                $this->session->set_flashdata("error", "Something went wrong..");
            }
            
            redirect("admin/add_semester/features_list");
        }
        
        admin_view('admin/add_features');
    }
    function features_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_front_features');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        
        $xcrud->change_type('icon', 'image', false, array(
            'width' => 450,
            'path' => '../uploads/features-icon',
            'thumbs' => array(
                array(
                    'height' => 55,
                    'width' => 120,
                    'crop' => true,
                    'marker' => '_th'
                )
            )
        ));
        
        $xcrud->change_type('image', 'image', false, array(
            'width' => 450,
            'path' => '../uploads/features-image',
            'thumbs' => array(
                array(
                    'height' => 55,
                    'width' => 120,
                    'crop' => true,
                    'marker' => '_th'
                )
            )
        ));
        
        $xcrud->button('edit_features/{id}', 'Edit Free Video', 'glyphicon glyphicon-edit');
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        $xcrud->label('image', 'Slider Image');
        $xcrud->label('content', 'Short Desc');
        $xcrud->label('full_desc', 'Full Desc');
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Features List';
        admin_view('admin/features_list', $data);
        
    }
    function edit_features($id)
    {
        //code for updating data
        if ($_POST) {
            $name      = $this->input->post('name');
            $content   = $this->input->post('content');
            $full_desc = $this->input->post('full_desc');
            $status    = $this->input->post('status');
            
            if (!empty($_FILES['icon']['name'])) {
                
                $config = array(
                    'upload_path' => "./uploads/features-icon",
                    'allowed_types' => "*",
                    'overwrite' => false,
                    'file_name' => time()
                );
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if ($this->upload->do_upload('icon')) {
                    $uploadimg = $this->upload->data();
                    $icon      = $uploadimg['file_name'];
                    
                } else {
                    $icon = '';
                    // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                    // redirect('admin/add_semester/edit_chapter/'.$product_id);
                    // exit();
                }
            } else {
                $icon = $this->input->post('old_icon');
            }
            
            if (!empty($_FILES['image']['name'])) {
                
                $config = array(
                    'upload_path' => "./uploads/features-image",
                    'allowed_types' => "*",
                    'overwrite' => false,
                    'file_name' => time()
                );
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if ($this->upload->do_upload('image')) {
                    $uploadimg = $this->upload->data();
                    $uimg      = $uploadimg['file_name'];
                    
                } else {
                    $uimg = '';
                    // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                    // redirect('admin/add_semester/edit_chapter/'.$product_id);
                    // exit();
                }
            } else {
                $uimg = $this->input->post('old_image');
            }
            
            
            $data = array(
                'name' => $name,
                'icon' => $icon,
                'image' => $uimg,
                'content' => $content,
                'full_desc' => $full_desc,
                'status' => $status
                
            );
            
            
            
            $where = array(
                'id' => $id
            );
            $this->admin_common_model->update_data('tbl_front_features', $data, $where);
            
            $this->session->set_flashdata("message", "Features Updated Successfully");
            redirect('admin/add_semester/features_list');
            
        }
        
        $where                   = array(
            'id' => $id
        );
        $data['features_detail'] = $this->admin_common_model->getwheres('tbl_front_features', $where);
        // echo "<pre>";
        // print_r($data['features_detail']);
        // die;
        
        admin_view('admin/edit_features', $data);
        
    }
    
    function add_faqs()
    {
        
        $this->form_validation->set_rules('question', 'Question', 'required|trim]');
        $this->form_validation->set_rules('answer', 'Answer', 'required|trim]');
        
        if ($this->form_validation->run() == TRUE) {
            if ($_POST) {
                $question = $this->input->post('question');
                $answer   = $this->input->post('answer');
                $status   = $this->input->post('active_status');
            }
            
            $add_faqs = array(
                'question' => $question,
                'answer' => $answer,
                'status' => $status
            );
            
            $this->admin_common_model->insert_data('tbl_faqs', $add_faqs);
            $this->session->set_flashdata("success", "Question Added Successfully");
            redirect('admin/add_semester/added_faqs');
            
            
        }
        
        admin_view('admin/add_faqs', $about_data);
    }
    function added_faqs()
    {
        
        $xcrud = Xcrud::get_instance()->table('tbl_faqs');
        
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->unset_add();
        //$xcrud->unset_edit();
        
        
        $xcrud->columns("question,answer,status", true);
        
        $xcrud->label('question', 'Question');
        $xcrud->label('answer', 'Answer');
        $xcrud->label('status', 'Status');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Added FAQs';
        
        
        admin_view('admin/added_faqs', $data);
        
    }
    
    function contact_us()
    {
        
        $xcrud = Xcrud::get_instance()->table('contact_us');
        
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->order_by('id', 'desc');
        
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Contact Us';
        
        
        admin_view('admin/contact_us', $data);
        
    }
    
    function order_list()
    {
        
        $xcrud = Xcrud::get_instance()->table('product_orders');
        
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->unset_add();
        //$xcrud->unset_edit();
        
        $xcrud->order_by('product_order_id', 'desc');
        // $xcrud->columns("question,answer,status",true);
        
        // $xcrud->label('question','Question');
        // $xcrud->label('answer','Answer');
        // $xcrud->label('status','Status');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Order Details';
        
        
        admin_view('admin/order_list', $data);
        
    }
    
    function online_dcs()
    {
        
        $xcrud = Xcrud::get_instance()->table('tbl_doubt_send');
        
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_remove();
        $xcrud->unset_view();
        
        $xcrud->where('status', 'pending');
        // $xcrud->order_by('product_order_id','desc');
        $xcrud->columns("title,image,description,create_date,status", true);
        
        // $xcrud->label('title','Question');
        // $xcrud->label('description','Answer');
        // $xcrud->label('question','Question');
        // $xcrud->label('status','Status');
        
        $xcrud->button('send_response/{id}', 'Send Responce', 'icon icon-print');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Online DCS Details';
        
        
        admin_view('admin/online_dcs', $data);
        
    }
    
    function send_response($id)
    {
        $doubt_id = $id;
        
        if ($_POST) {
            
            $answer = $this->input->post('answer');
            $link   = $this->input->post('link');
            
            $config = array(
                'upload_path' => "./uploads/ans-image/maths",
                'allowed_types' => "*",
                'overwrite' => false,
                'file_name' => time()
            );
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if ($this->upload->do_upload('image_ans')) {
                $uploadimg = $this->upload->data();
                $uimg      = $uploadimg['file_name'];
            } else {
                $uimg = '';
                // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                // exit();
            }
            
            $data = array(
                'doubt_id' => $doubt_id,
                'answer' => $answer,
                'image_ans' => $uimg,
                'link' => $link,
                'ans_date' => date('Y-m-d H:i:s'),
                'status' => 'responded'
            );
            
            $result = $this->admin_common_model->insert_data('tbl_doubt_responce', $data);
            if ($result) {
                
                $doubt_data = array(
                    'status' => 'responded'
                );
                
                $where = array(
                    'id' => $doubt_id
                );
                $this->admin_common_model->update_data('tbl_doubt_send', $doubt_data, $where);
            }
            $this->session->set_flashdata('message', 'You have send answer successfully');
            redirect("admin/add_semester/online_dcs");
        }
        
        
        $wheres        = array(
            'id' => $doubt_id
        );
        $data['doubt'] = $this->admin_common_model->getwheres('tbl_doubt_send', $wheres);
        
        admin_view('admin/send_response', $data);
    }
    
    public function set_student_password()
    {
        
        $this->form_validation->set_rules('user_name', 'Username', 'required|trim]');
        $this->form_validation->set_rules('user_password', 'Password', 'required|trim]');
        $this->form_validation->set_rules('test_names', 'Test Name', 'required|trim]');
        
        if ($this->form_validation->run() == TRUE) {
            if ($_POST) {
                $username = $this->input->post('user_name');
                $password = md5($this->input->post('user_password'));
                $testname = $this->input->post('test_names');
            }
            
            $add_details = array(
                'username' => $username,
                'password' => $password,
                'test_name' => $testname,
                'status' => 'active'
            );
            
            $this->admin_common_model->insert_data('student_quiz_credentials', $add_details);
            $this->session->set_flashdata("success_credentials", "Record Added Successfully");
            redirect('admin/add_semester/set_student_password');
            
        }
        
        $data['get_all_tests'] = $this->admin_common_model->fetch_record('tbl_test');
        admin_view('admin/set_password_quiz', $data);
    }
    
    function added_student_credentials()
    {
        
        $xcrud = Xcrud::get_instance()->table('student_quiz_credentials');
        
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->unset_add();
        //$xcrud->unset_edit();
        
        
        $xcrud->columns("username,password,status", true);
        
        $xcrud->label('username', 'Username');
        $xcrud->label('password', 'Password');
        //$xcrud->label('test_name','Test Name');
        $xcrud->label('status', 'Status');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Added Credentials';
        
        
        admin_view('admin/added_quiz_credentilas', $data);
        
    }

    // ************************  Chapter wise course ***************//

    public function get_chapter_course_wise()
    {
        $postData = $this->input->post('course_id');

        if(!empty($postData))
        {
            $chapter = $this->admin_common_model->getwheres('tbl_chapter',array('sem_id'=>$postData));

            if(!empty($chapter) && count($chapter)>0)
            {
                    foreach($chapter as $chapDetails)
                    {
                        echo '<option value="'.$chapDetails['ch_id'].'">'.$chapDetails['ch_name'].'</option>';
                    }
            }
            else
            {
                echo '<option value="">No Record Found</option>';
            }
        }
    }


    //******************************************************************//


      // ******************** E book Contact Details *********************//

    public function e_book_contact()
    {
       $xcrud = Xcrud::get_instance()->table('e_book_contact_form');
        
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->order_by('e_book_id', 'desc');
        
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Ebook Contact';
        
        
        admin_view('admin/contact_us', $data);
    }


    //**********************************************************************//

    public function ebook_content()
    { 
           admin_view('admin/add_ebook_content');
    }

    public function ebook_content_list()
    {
       $data['EbookContent'] =  $this->admin_common_model->getwheres('ebook_content',array());
        admin_view('admin/ebook_content_list',$data);
    }

    public function add_new_ebook_content()
    {
        if ($_POST) {
            
            $ebook_text = $this->input->post('answer');
            $ebook_status   = $this->input->post('active_status');
            
            $config = array(
                'upload_path' => "./uploads/e_books/",
                'allowed_types' => "*",
                'overwrite' => false,
                'file_name' => time()
            );
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if ($this->upload->do_upload('ebook_content_image')) {
                $uploadimg = $this->upload->data();
                $uimg      = $uploadimg['file_name'];
            } else {
                $uimg = '';
                
            }
            
            $data = array(
                'ebook_content_text' => $ebook_text,
                'ebook_content_image' => $uimg,
                'ebook_content_status' => $ebook_status
                
            );
            
            $result = $this->admin_common_model->insert_data('ebook_content', $data);
            
            $this->session->set_flashdata('message', 'Ebook Content Added Successfully');
            redirect("admin/add_semester/ebook_content",'refresh');
        }
    }


    public function delete_ebook_content($ebook_content_id="")
    {
            if(!empty($ebook_content_id))
            {
                $this->db->where('ebook_content_id',$ebook_content_id);
                $this->db->delete('ebook_content');
                redirect('admin/add_semester/ebook_content_list','refresh');
            }
    }

}
?> 