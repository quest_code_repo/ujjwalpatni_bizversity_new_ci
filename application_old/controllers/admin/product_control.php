<?php
class Product_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session','image_lib')); 
	}

	public function categorylist($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('product_categories');
        /**********view pages***********/
        $xcrud->columns('name,parent_category,created_at');
        
        $xcrud->unset_remove(); 
		$xcrud->table_name('');
		$xcrud->relation('parent_category','product_categories','id','name');

		$xcrud->relation('variant_catogories','variants_category','variant_category_id','variant_category_name');
		$xcrud->relation('created_by','employees','id','name');

		
		$data['content'] = $xcrud->render();
		$data['title'] ='Category List';
		admin_view('admin/allotmenu_list', $data);
    }
	
	public function packs($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('product_pack');
        /**********view pages***********/
		//$xcrud->unset_remove();
		$xcrud->relation('unit_id','unit','id','name');
        $data['content'] = $xcrud->render();
		$data['title'] ='Product Pack List';
		admin_view('admin/allotmenu_list', $data);
    }
	
	
	public function unit($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('unit');
        /**********view pages***********/
		//$xcrud->unset_remove();
        $data['content'] = $xcrud->render();
		$data['title'] ='Product Unit List';
		admin_view('admin/allotmenu_list', $data);
    }
	
	public function add_product($msg = NULL){
		$data['title'] ='Add Product';
		$chk = array('parent_category' => 0);
		$data['category']=$this->admin_common_model->fetch_condrecord('product_categories',$chk);
		//$data['brand']=$this->admin_common_model->fetch_record('brand');
		$data['unit']=$this->admin_common_model->fetch_join_allrecords('product_pack','unit','unit_id','id');
		admin_view('admin/add_product', $data);
    }
	
	public function load_subcat($cid){
		$chk = array('parent_category' => $cid);
		$category=$this->admin_common_model->fetch_condrecord('product_categories',$chk);
		if(!empty($category))
		{
			foreach($category as $catname) {
			?>
           <span for="<?php echo $catname['id']; ?>"> 
		   <input type="radio" id="<?php echo $catname['id']; ?>" name="subcat[]" value="<?php echo $catname['id']; ?>" onclick="load_sub_subcat(this.value);"/> <?php echo $catname['name']; ?>
           </span> <br />
           
            <?php
		} }
		
    }
	
	public function load_sub_subcat($cid){
		// echo $cid;
		if($cid > 0 || $cid != "undefined" || $cid != " "){
		$chk = array('parent_category' => $cid);
		$category=$this->admin_common_model->fetch_condrecord('product_categories',$chk);
		//echo $this->db->last_query();
		if(!empty($category))
		{
			foreach($category as $catname) {
			?>
           <span for="<?php echo $catname['id']; ?>"> 
		   <input type="checkbox" id="<?php echo $catname['id']; ?>" name="sub_subcat[]" value="<?php echo $catname['id']; ?>" /> <?php echo $catname['name']; ?>
           </span> <br />
           
            <?php
		} }
		else{
			echo "<span style='font-style:italic;color:#FFF;font-weight:bold;'>No Sub Category...</span>";		
			return false;
		}

		}
		else{	
			echo "<span style='font-style:italic;color:#FFF;font-weight:bold;'>No Sub Category...</span>";		
			return false;
		}
		
    }
	public function createurl($input) {
		$input = str_replace("&nbsp;", " ", $input);
		$input = str_replace(array("'", "-"), "", $input); //remove single quote and dash
		$input = mb_convert_case($input, MB_CASE_LOWER, "UTF-8"); //convert to lowercase
		$input = preg_replace("#[^a-zA-Z]+#", "-", $input); //replace everything non an with dashes
		$input = preg_replace("#(-){2,}#", "$1", $input); //replace multiple dashes with one
		$input = trim($input, "-"); //trim dashes from beginning and end of string if any
		return $input; 
	}
	function image_upload($imgname)
	{
		$config = array(
						'upload_path' => "./uploads/products/",
						'allowed_types' => "gif|jpg|png|jpeg|pdf",
						'overwrite' => false
						
					);
			$this->load->library('upload', $config);
			if($this->upload->do_upload($imgname))
			{
				$uploadimg = $this->upload->data();
				$config = array(
					'source_image'      => $uploadimg['full_path'], //path to the uploaded image
					'new_image'         => "./uploads/thumbs/", //path to
					'maintain_ratio'    => true,
					'width'             => 180,
					'height'            => 180
					);
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$uimg=$uploadimg['file_name'];
			return $uimg;
			}
			else
			{ 
				$uimg='';
				$error = array('error' => $this->upload->display_errors());
				return false;
				
			}
	}
	
	
	

	public function product_sub(){
		$uimg=$this->image_upload('pro_img');
		if($uimg){ $uimg=$uimg; } else { $uimg=''; }
		$url=$this->createurl($this->input->post('pname'));
		$pro = array(
			'pname' => $this->input->post('pname'),
			'purl' => $url,
			'stags' => $this->input->post('ptags'),
			'pdesc' => $this->input->post('desc'),
			'pfeatures' => $this->input->post('features'),
			'is_featured' => $this->input->post('featured_id'),
			'wow_special' => $this->input->post('wow_special_id'),
			'recommended' => $this->input->post('recommended_id'),
			'p_new' => $this->input->post('new_id'),
			'quantity' => $this->input->post('pquantity'),
			//'brand_name' => $this->input->post('brand_id'),
			'sku' => $this->input->post('sku'),
			'video_link' => $this->input->post('video_link'),
			'category_id' => $this->input->post('category_id'),
			'vat' => $this->input->post('vat'),
			'image_url' => $uimg);
			$pid = $this->admin_common_model->insert_data('products',$pro);
			$subcat=$this->input->post('subcat');
			$sub_subcat=$this->input->post('sub_subcat');
			
			if(!empty($subcat)) {
				foreach($subcat as $cat)
				{
					if(!empty($sub_subcat)) {
						foreach($sub_subcat as $sub_cat)
						{
							$chk = array('product_id' => $pid,'category_id' => $cat,'sub_category_id' => $sub_cat);
							$tagschk=$this->admin_common_model->insert_data('product_category_rel',$chk);				
						}
					}
					else
					{						
						$chk1 = array('product_id' => $pid,'category_id' => $cat,'sub_category_id' => '');
						$tagschk1=$this->admin_common_model->insert_data('product_category_rel',$chk1);	
					}
				} 
			}
			
			$unit_id=$this->input->post('unit_id');
			if(!empty($unit_id)) { $s=0;
			foreach($unit_id as $uid)
			{   
				$quachk = array('product_id' => $pid,'pack_id' => $uid,'org_price' => $this->input->post('org_price'.$uid),'disc_price' => $this->input->post('disc_price'.$uid));
			    $tagschk=$this->admin_common_model->insert_data('product_qua',$quachk);
				$s++;
			} }
			//multiple images upload
			if($_FILES['file']['name']){
			$path = './uploads/products/';
			$config = array(
					'upload_path' => $path,
					'max_size' => 1024 * 100,
					'allowed_types' => 'gif|jpeg|jpg|png',
					'overwrite' => true,
					'remove_spaces' => true);
				$images = array();
				$this->load->library('upload');
				$files = $_FILES;
				$count = count($_FILES['file']['name']);
				
				for ($i = 0; $i < $count; $i++) 
				{
					$_FILES['file']['name'] = $files['file']['name'][$i];
					$_FILES['file']['type'] = $files['file']['type'][$i];
					$_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$i];
					$_FILES['file']['error'] = $files['file']['error'][$i];
					$_FILES['file']['size'] = $files['file']['size'][$i];

					$fileName = $_FILES['file']['name'];
					$images[] = $fileName;
					$config['file_name'] = $fileName;

					$this->upload->initialize($config);
				   if ($this->upload->do_upload('file')) 
				   {
						$uploadimg = $this->upload->data();
						//$return['status'] = 'Success';
						$data['upload'] = $this->upload->data();
						//print_r($data['upload']);
						if($data['upload'])
						{
							foreach($data['upload'] as $thumimg)
							{
								//print_r($thumimg);
								$config_t = array(
								'quality' => '100%',
								'source_image' => $uploadimg['full_path'],
								'new_image' => './uploads/thumbs/',
								'maintain_ratio' => true,
								'create_thumb' => TRUE,
								'width' => 180,
								'height' => 180
								);              
								$this->image_lib->initialize($config_t); 
								$this->image_lib->resize();
								
								
							}
							    $uimgName = $data['upload']['file_name'];
								$prodimg = array(
								'product_id' =>$pid,
								'image_url' => $uimgName);
								$pidImgInsert = $this->admin_common_model->insert_data('products_images',$prodimg);
														
						}
						
					} 
					

				}
			}
		redirect('admin/product_control/product_list');
    }
	
	public function product_list($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('products');
        /**********view pages***********/
		$xcrud->unset_remove();
		$xcrud->unset_add();
		$xcrud->unset_edit();
		
		$xcrud->columns('pname,pdesc,category_id,is_featured,recommended,image_url,status',true);		
		$xcrud->relation('category_id','product_categories','id','name');
		//$xcrud->relation('brand_name','brand','brand_id','brand_name');
		$xcrud->button('edit_product/{product_id}','Edit Product','icon-pencil');
		$xcrud->button('inactive_product/{product_id}','Delete Product','icon-remove');
	    $xcrud->change_type('image_url', 'image', false, array(
        'width' => 450,
        'path' => '../uploads/products',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
        $data['content'] = $xcrud->render();
		$data['title']='Product List';
		admin_view('admin/allotmenu_list', $data);
    }
	
	public function edit_product($pid){
		$chk = array('product_id' => $pid);
		$chkImages = array('product_id' => $pid);
		$catchk = array('parent_category' => 0);
		$data['category']=$this->admin_common_model->fetch_condrecord('product_categories',$catchk);
		$data['unit']=$this->admin_common_model->fetch_join_allrecords('product_pack','unit','unit_id','id');
		//$data['brand']=$this->admin_common_model->fetch_record('brand');
		$data['products']=$this->admin_common_model->fetch_recordbyid('products',$chk);
		$data['quantity']=$this->admin_common_model->fetch_condrecord('product_qua',$chk);
		$data['multiimage']=$this->admin_common_model->fetch_condrecord('products_images',$chkImages);
		
		admin_view('admin/edit_product', $data);
	}
	
	public function inactive_product($pid){
		$chked = array('product_id' => $pid);
		$pro = array(
					'active_inactive' => 'inactive',
					'admin_display' => 'no');
		$this->admin_common_model->update_data('products',$pro,$chked);
		redirect('admin/product_control/product_list'); 
	}
	
	public function check_price($pid,$pack_id)
	{
		$chk = array('product_id' => $pid,'pack_id' => $pack_id);
		$onequa=$this->admin_common_model->fetch_recordbyid('product_qua',$chk);
		return $onequa;
	}
	
	public function edit_product_sub(){
		$pid=$this->input->post('pid');
		if($_FILES['pro_img']['name']){
		$uimg=$this->image_upload('pro_img');
		if($uimg){ $uimg=$uimg; }
		$chkImg = array('product_id' => $this->input->post('pid'));
		$proImg = array(
			'image_url' => $uimg
			);
		$this->admin_common_model->update_data('products',$proImg,$chkImg);
		}
		
		$chk = array('product_id' => $this->input->post('pid'));
		$url=$this->createurl($this->input->post('pname'));
		$pro = array(
			'pname' => $this->input->post('pname'),
			'hname' => $this->input->post('hname'),
			'purl' => $url,
			'stags' => $this->input->post('ptags'),
			'pdesc' => $this->input->post('desc'),
			'pfeatures' => $this->input->post('features'),
			//'brand_name' => $this->input->post('brand_id'),
			'quantity' => $this->input->post('pquantity'),
			'is_featured' => $this->input->post('featured_id'),
			'wow_special' => $this->input->post('wow_special_id'),
			'p_new' => $this->input->post('new_id'),
			'active_inactive' => $this->input->post('is_active'),
			'sku' => $this->input->post('sku'),
			'vat' => $this->input->post('vat'),
			'video_link' => $this->input->post('video_link'),
			'category_id' => $this->input->post('category_id')
			); 
		
			$this->admin_common_model->update_data('products',$pro,$chk);
			$this->admin_common_model->delete_data('product_category_rel',$chk);
			$this->admin_common_model->delete_data('product_qua',$chk);
			$subcat=$this->input->post('subcat');
			$sub_subcat=$this->input->post('sub_subcat'); 
			//print_r($subcat);
			//echo "<br>";
			//print_r($sub_subcat);
			//die(); 
			
			if(!empty($subcat)) {
				foreach($subcat as $cat)
				{
					if(!empty($sub_subcat)) {
						foreach($sub_subcat as $sub_cat)
						{
							$chk = array('product_id' => $pid,'category_id' => $cat,'sub_category_id' => $sub_cat);
							$tagschk=$this->admin_common_model->insert_data('product_category_rel',$chk);				
						}
					}
					else
					{						
						$chk1 = array('product_id' => $pid,'category_id' => $cat,'sub_category_id' => '');
						$tagschk1=$this->admin_common_model->insert_data('product_category_rel',$chk1);	
					}
				} 
			}
			//echo $this->db->last_query(); die();
			$unit_id=$this->input->post('unit_id');
			if(!empty($unit_id)) { $s=0;
			foreach($unit_id as $uid)
			{   
				$quachk = array('product_id' => $pid,'pack_id' => $uid,'org_price' => $this->input->post('org_price'.$uid),'disc_price' => $this->input->post('disc_price'.$uid));
			    $tagschk=$this->admin_common_model->insert_data('product_qua',$quachk);
				$s++;
			} }
			//multiple images upload
			if($_FILES['file']['name'])
			{
			$path = './uploads/products/';
			$config = array(
					'upload_path' => $path,
					'max_size' => 1024 * 100,
					'allowed_types' => 'gif|jpeg|jpg|png',
					'overwrite' => true,
					'remove_spaces' => true);
				$images = array();
				$this->load->library('upload');
				$files = $_FILES;
				$count = count($_FILES['file']['name']);
				
				for ($i = 0; $i < $count; $i++) 
				{
					$_FILES['file']['name'] = $files['file']['name'][$i];
					$_FILES['file']['type'] = $files['file']['type'][$i];
					$_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$i];
					$_FILES['file']['error'] = $files['file']['error'][$i];
					$_FILES['file']['size'] = $files['file']['size'][$i];

					$fileName = $_FILES['file']['name'];
					$images[] = $fileName;
					$config['file_name'] = $fileName;

					$this->upload->initialize($config);
				   if ($this->upload->do_upload('file')) 
				   {
						$uploadimg = $this->upload->data();
						//$return['status'] = 'Success';
						$data['upload'] = $this->upload->data();
						//print_r($data['upload']);
						if($data['upload'])
						{
							foreach($data['upload'] as $thumimg)
							{
								//print_r($thumimg);
								$config_t = array(
								'quality' => '100%',
								'source_image' => $uploadimg['full_path'],
								'new_image' => './uploads/thumbs/',
								'maintain_ratio' => true,
								'create_thumb' => false,
								'width' => 180,
								'height' => 180
								);              
								$this->image_lib->initialize($config_t); 
								$this->image_lib->resize();
								$uimgName = $data['upload']['file_name'];
								
							}							    
							$prodimg = array(
							'product_id' =>$pid,
							'image_url' => $uimgName);
							$pidImgInsert = $this->admin_common_model->insert_data('products_images',$prodimg);
														
						}
						
					} 
					

				}
			}
		redirect('admin/product_control/product_list');
    }
	
	public function removeProductImages($pid,$imgid)
	{
		
		$prodimg = array(
						'product_id' =>$pid,
						'id' => $imgid);
		$pidRemoveId = $this->admin_common_model->delete_data('products_images',$prodimg);
		if($pidRemoveId){
			return true;
		}
	}
	public function addWeightVal($weightId,$unitId)
	{
		$weight = array(
			'weight' => $weightId,
			'unit_id' => $unitId
			);
			
			$productPack = $this->admin_common_model->insert_data('product_pack',$weight);
			if($productPack)
			{
				$chkpack = array('pack_id' => $productPack);
				$data = $this->admin_common_model->fetch_condrecord('product_pack',$chkpack);
				if($data){
					foreach($data as $arrdata)
					{   
					    $unitIdV = array('id' => $arrdata['unit_id']);
				        $unitname = $this->admin_common_model->fetch_recordbyid('unit',$unitIdV);
						//echo $unitname['name'];
						
						echo $arrdata['pack_id'].'-------------------'.$arrdata['weight'].'-------------------'.$unitname->name; 
					}
				}
			}
	}
	
	public function chkWeightUnit()
	{
	
		 $weightId = $this->input->post('qweight'); 
		 $unitId =   $this->input->post('qunit'); 
		 $sql = "SELECT * FROM `product_pack` WHERE `weight` = '".$weightId."' AND `unit_id` = '".$unitId."'";
		 $weightchk = $this->admin_common_model->fetch_greaterdata($sql);
		 echo $weightchk;
	}

	public function reviews()
	{
		$xcrud = Xcrud::get_instance()->table('product_reviews');
        /**********view pages***********/
		$xcrud->unset_remove();
		$xcrud->unset_add();
		$xcrud->label('product_id','Product Name');
		$xcrud->label('user_id','User Name');
		$xcrud->relation('product_id','products','product_id','pname');
		$xcrud->relation('user_id','customer','id','first_name');
        $data['content'] = $xcrud->render();
		$data['title']='Product Review List';
		admin_view('admin/allotmenu_list', $data);
	}
	
}