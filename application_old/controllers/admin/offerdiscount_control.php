<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offerdiscount_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('offer_discount');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		//$xcrud->unset_remove(); 
		$xcrud->relation('created_by','employees','id','name');
		$xcrud->change_type('image_url', 'image', false, array(
        'width' => 450,
        'path' => '../uploads/offer',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
        $data['content'] = $xcrud->render();
		$data['title'] ='Discounted Offer List';
		admin_view('admin/offer_list', $data);
    }
	
}