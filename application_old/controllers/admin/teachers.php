<?php 
 
class Teachers extends CI_Controller {

public function __construct()
{
    parent::__construct();
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->model('admin/admin_common_model');
    $this->load->helper('xcrud');
    $this->load->library(array('form_validation','session','image_lib')); 
}


//function for Teacher Add
public function add_teacher()
{
    if($_POST)
    {

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('fullname', 'Fullname', 'required');
        if($this->form_validation->run() == FALSE)
        {

        }
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $fullname = $this->input->post('fullname');
        $mobile = $this->input->post('mobile');

        $config = array(
            'upload_path' => "./uploads/user-image",
            'allowed_types' => "*",
            'overwrite' => false,
            'file_name' => time()
            
            );

            $this->load->library('upload', $config);

            if($this->upload->do_upload('image_name')) 
            {
                $uploadimg = $this->upload->data();
                $image_upload = $uploadimg['file_name'];
            }else{

                $image_upload = '';
          //    $error = array('error'=>$this->upload->display_errors());
                // return $error;
            }            
      
            $add_data = array(
                'role'=>'Teacher',
                'username'=>$username,
                'password'=>$password,
                'full_name'=>$fullname,
                'image_name'=>$image_upload,
                'mobile'=>$mobile,
                'created_at'=>date('Y-m-d'),
                'status'=>'active'
            );  
            // print_r($add_data);
            // die(); 

            $this->admin_common_model->insert_data('tbl_students',$add_data);
            $this->session->set_flashdata('message', 'Teacher Added Successfully');
            redirect("admin/teachers/teacher_details");       
    }

    admin_view('admin/add_teachers',$data);
}

public function teacher_details()
{
    $xcrud = Xcrud::get_instance()->table('tbl_students');
    /**********view pages***********/
    $xcrud->unset_remove();
    $xcrud->unset_add();
    $xcrud->unset_edit();
    //$xcrud->unset_search();
    $xcrud->unset_print();
    $xcrud->unset_csv();

    // $xcrud->table_name('Category List');
    $xcrud->where('role =','Teacher');
    $xcrud->where('status =','active');

    $xcrud->button('edit_teacher/{id}','Edit Teachers Details','glyphicon glyphicon-edit');
    $xcrud->change_type('image_name', 'image', false, array(
        'width' => 450,
        'path' => './uploads/user-image',
        'thumbs' => array(
            array(
            'height' => 55,
            'width' => 120,
            'crop' => true,
            'marker' => '_th'))));

    $xcrud->columns("role,username,full_name,mobile,created_at,status",true);
    $xcrud->label('created_at','Date');
    
    // $xcrud->label('pname','Course Name');
    // $xcrud->label('stock_status','Stock Status');
    
    $data['content'] = $xcrud->render();
    $data['title']='Teacher List';
    admin_view('admin/teacher-details', $data);
}

public function edit_teacher($id)
{
    $where = array('id' => $id);
    if($_POST)
    {
        $username = $this->input->post('username');
        // $password = md5($this->input->post('password'));
        $full_name = $this->input->post('full_name');
        $mobile = $this->input->post('mobile');

        
        if(!empty($_FILES['image_name']['name']))
        {

           $config = array(
            'upload_path' => "./uploads/user-image",
            'allowed_types' => "*",
            'overwrite' => false,
            'file_name' => time()
            );

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
    
            if($this->upload->do_upload('image_name')) 
            {
                $uploadimg = $this->upload->data();
                $uimg = $uploadimg['file_name'];
                
            } 
            else
            {
                $uimg = '';
                // $this->session->set_flashdata("error",$this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
                // redirect('admin/add_semester/edit_product/'.$product_id);
                // exit();
            }
        }
        else
        {
            $uimg = $this->input->post('old_image');
        }            
      
        $update_data = array(
            'role'=>'Teacher',
            'username'=>$username,
            // 'password'=>$password,
            'full_name'=>$full_name,
            'image_name'=>$uimg,
            'mobile'=>$mobile,
            'created_at'=>date('Y-m-d'),
            'status'=>'active'
        );  
            // print_r($update_data);
            // die(); 

            $this->admin_common_model->update_data('tbl_students',$update_data,$where);
            $this->session->set_flashdata('message', 'Teachers Details Update Successfully');
            redirect("admin/teachers/teacher_details");       
    }
    
    $data['teachers'] = $this->admin_common_model->getwheres('tbl_students',$where);
    
    admin_view('admin/edit_teachers',$data);
}

public function user_details()
{
    $xcrud = Xcrud::get_instance()->table('tbl_students');
    /**********view pages***********/
    $xcrud->unset_remove();
    $xcrud->unset_add();
    $xcrud->unset_edit();
    //$xcrud->unset_search();
    $xcrud->unset_print();
    $xcrud->unset_csv();

    // $xcrud->table_name('Category List');
    $xcrud->where('role =','Student');
    $xcrud->where('status =','active');

    $xcrud->order_by('id','desc');

    // $xcrud->button('edit_teacher/{id}','Edit Teachers Details','glyphicon glyphicon-edit');
    $xcrud->change_type('image_name', 'image', false, array(
        'width' => 450,
        'path' => './uploads/user-image',
        'thumbs' => array(
            array(
            'height' => 55,
            'width' => 120,
            'crop' => true,
            'marker' => '_th'))));

    $xcrud->columns("full_name,class_name,mobile,created_at,status",true);
    $xcrud->label('created_at','Date');
    
    // $xcrud->label('pname','Course Name');
    // $xcrud->label('stock_status','Stock Status');
    
    $data['content'] = $xcrud->render();
    $data['title']='Student Details';
    admin_view('admin/teacher-details', $data);
}
 }
?>