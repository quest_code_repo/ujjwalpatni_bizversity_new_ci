<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
     
	public function view($msg = NULL){
		checkAuth();
		$xcrud = Xcrud::get_instance()->table('employees');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
        $xcrud->validation_required(array('emp_code','department','name','gender','official_email','official_contact_no','designation'));
		$xcrud->button('change_password/{id}','Change Password','icon-key','',array('target'=>'_blank'));
		$xcrud->disabled_on_edit ('password');
        $xcrud->label('emp_code','Employee Code');
        $xcrud->relation('department','departments','id','name');
        $xcrud->relation('designation','designations','id','name');
		$xcrud->change_type('password', 'password', 'md5', array('placeholder'=>'enter password'));
		$xcrud->change_type('photo', 'image', false, array(
        'width' => 450,
        'path' => '../uploads/employee',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
        $data['content'] = $xcrud->render();
		$data['title'] ='Employee List';
		admin_view('admin/employee_list', $data);
    }

    public function change_password($id)
    {
    	if (isset($_POST['cng_pass'])) {
            $this->form_validation->set_rules('old_password', 'old password', 'required|xss_clean');
            $this->form_validation->set_rules('new_password', 'new password', 'required|xss_clean');
            $this->form_validation->set_rules('new_confirm_password', 'confirm password', 'required|xss_clean|matches[new_password]');
            if ($this->form_validation->run() == TRUE) {

                $where = array(
                    'id' => $id,
                    'password' => md5($this->input->post('old_password'))
                );
                $data = $this->admin_common_model->fetch_recordbyid('employees', $where);
                if (!empty($data)) {

                    $arr = array
                        (
                        'password' =>  md5($this->input->post('new_password'))
                    );
                    $where = array(
                        'id' => $id,
                        'password' =>  md5($this->input->post('old_password'))
                    );
                    $login = $this->admin_common_model->update_data('employees', $arr, $where);
                    if ($login == 1) {
                        redirect('admin/employee_control/view');
                    } else {
                       echo "failed";
                       die();
                    }
                } else {
                    $this->session->set_flashdata('error', 'Please fill in correct old password');
                }

                
            } else {
               
            }
        }
    	$data['id'] = $id;
    	$data['title'] = 'Change Password';
    	admin_view('admin/change_password', $data);
    }

    public function edit_profile($msg = NULL){
        checkAuth();
        $xcrud = Xcrud::get_instance()->table('employees');
        /**********view pages***********/
        $xcrud->where('id', $this->session->userdata('id'));
        $xcrud->hide_button('save_return');
        $xcrud->hide_button('return');
        $xcrud->hide_button('save_new');
        $xcrud->change_type('photo', 'image', false, array(
        'width' => 450,
        'path' => '../uploads/employee',
        'thumbs' => array(array(
                'height' => 25,
                'width' => 25,
                'crop' => true,
                'marker' => '_th'))));
      $xcrud->relation('department','departments','id','name');
        $xcrud->relation('designation','designations','id','name');      
      $data['content'] = $xcrud->render('edit', $this->session->userdata('id'));
        $data['title'] ='Edit Profile';
        admin_view('admin/allotmenu_list', $data);
    }
    
}