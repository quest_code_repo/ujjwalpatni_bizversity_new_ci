<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offer_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		$xcrud = Xcrud::get_instance()->table('table_scheme');
        /**********view pages***********/
		$xcrud->order_by('id','desc');
		//$xcrud->unset_remove(); 
		$xcrud->relation('associated_with_product_id','products','product_id','pname',array('active_inactive' => 'active'));
		$xcrud->relation('free_product_id','products','product_id','pname',array('active_inactive' => 'active'));
		$xcrud->relation('product_unit','table_size','size_id','name');
		$xcrud->relation('free_product_unit','table_size','size_id','name');
		$xcrud->relation('created_by','employees','id','name');
		$xcrud->change_type('image_url', 'image', false, array(
        'width' => 450,
        'path' => '../uploads/products',
        'thumbs' => array(array(
                'height' => 55,
                'width' => 120,
                'crop' => true,
                'marker' => '_th'))));
        $data['content'] = $xcrud->render();
		$data['title'] ='Bundle Pack Offer List';
		admin_view('admin/offer_list', $data);
    }
	
}