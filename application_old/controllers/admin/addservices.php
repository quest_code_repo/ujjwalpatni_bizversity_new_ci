<?php

class Addservices extends CI_Controller {
    public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session','image_lib')); 
	}

    function myservices()
    {

	
    	$this->form_validation->set_rules('service_ammount', 'Service Amount', 'required|regex_match[/^[1-9]\d{0,7}(?:\.\d{1,4})?$/]');
    	$this->form_validation->set_rules('service_name', 'Service Name', 'required|trim');
    	

    	if ($this->form_validation->run() == TRUE)
    	{
    		if($_POST)
         	{
         		$service_name=$this->input->post('service_name');
         		$service_des=$this->input->post('service_des');
         		$service_ammount=$this->input->post('service_ammount');
         		$product_selection=$this->input->post('product_selection');
                $active_status=$this->input->post('active_status');
                
                $service_data = array ('service_name'=>$service_name,);
               $category_service_id=$this->admin_common_model->insert_data('tbl_service_parent',$service_data); 


        foreach( $product_selection as $value){
        	//print_r($key);
        	
         		$myservice_data=array(
         		'service_name_id'=>$category_service_id,
         		 'service_description'=>$service_des,
         		 'service_ammount'=>$service_ammount,
                 'products_id'=>$value,
                 'status'=>$active_status,
                 'created_date'=>date('Y-m-d')
                 );

         		$category_id=$this->admin_common_model->insert_data('tbl_productservices',$myservice_data);
         		
         	}
         	

         		$myservice_data['message'] = 'Data Inserted Successfully';
         		
            
         	}
    	}
    		 
            $where = array('active_inactive'=>'active');

             $myservice_data['product_details']=$this->admin_common_model->fetch_total_record_wher('products',$where);  
             //return the data in view  
             admin_view('admin/services', $myservice_data); 

    }

    function service_list()
         {
            $xcrud = Xcrud::get_instance()->table('tbl_service_parent');
            /**********view pages***********/
            $xcrud->unset_remove();
            $xcrud->unset_add();
            //$xcrud->unset_edit();
           // $xcrud->unset_search();
            $xcrud->unset_print();
            $xcrud->unset_csv();

            // $xcrud->change_type('image_url', 'image', false, array(
            // 'width' => 450,
            // 'path' => '../uploads/products',
            // 'thumbs' => array(array(
            //         'height' => 55,
            //         'width' => 120,
            //         'crop' => true,
            //         'marker' => '_th'))));
            
            $xcrud->columns("service_name,status",true);
            
            $xcrud->label('service_name','Service Name');
            $xcrud->label('status','Service Status');
            // $xcrud->label('pfeatures','Features');
            // $xcrud->label('pproperties','Properties');
            // $xcrud->label('care_instruction','Care Instruction');

            // $xcrud->label('pwarranty','Warranty');
            // $xcrud->label('preturns','Returns');
            // $xcrud->label('image_url','Product Image');
            // $xcrud->label('pquality_promise','Quality Promise');
            // $xcrud->label('active_inactive','Product Status');

            // $xcrud->button('edit_product/{product_id}','Edit Product','glyphicon glyphicon-edit');
            // $xcrud->button('delete_product/{product_id}','Delete Product','glyphicon glyphicon-remove');


            $myservice_data['content'] = $xcrud->render();
            $myservice_data['title']='Service List';
            admin_view('admin/service_list', $myservice_data);
            
         }



    function Extend_Warranty()
    {

        // $this->form_validation->set_rules('warranty_ammount', 'Warranty Amount', 'required|regex_match[/^[1-9]\d{0,7}(?:\.\d{1,4})?$/]');
        $this->form_validation->set_rules('warranty_ammount', 'Warranty Amount', 'required|regex_match[/^[1-9]\d{0,7}(?:\.\d{1,4})?$/]');
        $this->form_validation->set_rules('warranty_name', 'Warranty Name', 'required|trim');    

            if ($this->form_validation->run() == TRUE)
            {


               if($_POST)
                {
                    $warranty_name=$this->input->post('warranty_name');
                    $warranty_des=$this->input->post('warranty_des');
                    $warranty_ammount=$this->input->post('warranty_ammount');
                    $product_selection=$this->input->post('product_selection');
                    $active_status=$this->input->post('active_status');
                    
                    $warranty_data = array ('warranty_name'=>$warranty_name,);
                   $category_warranty_id=$this->admin_common_model->insert_data('tbl_warranty_parent',$warranty_data); 


            foreach( $product_selection as $value){
            //print_r($key);
            
                $extend_warranty_data=array(
                'warranty_name_id'=>$category_warranty_id,
                 'warranty_description'=>$warranty_des,
                 'warranty_ammount'=>$warranty_ammount,
                 'products_id'=>$value,
                 'status'=>$active_status,
                 'created_date'=>date('Y-m-d')
                 );

                $category_id=$this->admin_common_model->insert_data('tbl_product_warranty',$extend_warranty_data);
            }
                $extend_warranty_data['message'] = 'Data Inserted Successfully';
            }
        }
        $where = array('active_inactive'=>'active');
        $extend_warranty_data['product_details']=$this->admin_common_model->fetch_total_record_wher('products',$where);  

        admin_view('admin/extend_warranty',$extend_warranty_data);
    }



    function warranty_list()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_warranty_parent');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        //$xcrud->unset_edit();
       // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();

        // $xcrud->change_type('image_url', 'image', false, array(
        // 'width' => 450,
        // 'path' => '../uploads/products',
        // 'thumbs' => array(array(
        //         'height' => 55,
        //         'width' => 120,
        //         'crop' => true,
        //         'marker' => '_th'))));
        
        $xcrud->columns("warranty_name,status",true);
        
        $xcrud->label('warranty_name','Service Name');
        $xcrud->label('status','Service Status');
        // $xcrud->label('pfeatures','Features');
        // $xcrud->label('pproperties','Properties');
        // $xcrud->label('care_instruction','Care Instruction');

        // $xcrud->label('pwarranty','Warranty');
        // $xcrud->label('preturns','Returns');
        // $xcrud->label('image_url','Product Image');
        // $xcrud->label('pquality_promise','Quality Promise');
        // $xcrud->label('active_inactive','Product Status');

        // $xcrud->button('edit_product/{product_id}','Edit Product','glyphicon glyphicon-edit');
        // $xcrud->button('delete_product/{product_id}','Delete Product','glyphicon glyphicon-remove');


        $extend_warranty_data['content'] = $xcrud->render();
        $extend_warranty_data['title']='Warranty List';
        admin_view('admin/warranty_list', $extend_warranty_data);
        
    }




    function point_distribution(){

        $this->form_validation->set_rules('point_ammount', 'Amount', 'required|regex_match[/^[1-9]\d{0,7}(?:\.\d{1,4})?$/]');
        $this->form_validation->set_rules('ponit_value', 'Points', 'required|regex_match[/^[1-9]\d{0,7}(?:\.\d{1,4})?$/]');
        $this->form_validation->set_rules('percent_ammount', 'Percentage', 'required');
        

        if ($this->form_validation->run() == TRUE)
        {
           if($_POST)
            {

                $point_ammount=$this->input->post('point_ammount');
                $ponit_value=$this->input->post('ponit_value');
                $percent_ammount=$this->input->post('percent_ammount');
                $active_status=$this->input->post('active_status');

            $point_distribution_data=array(
                'point_amount'=>$point_ammount,
                 'point_number'=>$ponit_value,
                 'amount_percent'=>$percent_ammount,
                 'created_date'=>date('Y-m-d'),
                 'status'=>$active_status
                 );
            }

            $point_distribution=$this->admin_common_model->insert_data('tbl_point_distribution',$point_distribution_data);
            $point_distribution_data['message'] = 'Data Inserted Successfully';
           
           redirect('admin/Addservices/distributed_point_list',$point_distribution_data);

        }

        admin_view('admin/point_distribution',$point_distribution_data);


    }


    function distributed_point_list(){

          $xcrud = Xcrud::get_instance()->table('tbl_point_distribution');
            /**********view pages***********/
           // $xcrud->unset_remove();
            $xcrud->unset_add();
            //$xcrud->unset_edit();
           // $xcrud->unset_search();
            $xcrud->unset_print();
            $xcrud->unset_csv();

            // $xcrud->change_type('image_url', 'image', false, array(
            // 'width' => 450,
            // 'path' => '../uploads/products',
            // 'thumbs' => array(array(
            //         'height' => 55,
            //         'width' => 120,
            //         'crop' => true,
            //         'marker' => '_th'))));
            
            $xcrud->columns("point_amount,point_number,amount_percent,status",true);
            
            $xcrud->label('point_amount','Added Amount');
            $xcrud->label('point_number','Points (On Amount)');
            $xcrud->label('amount_percent','Percent(%)');
            $xcrud->label('status','Service Status');
            // $xcrud->label('pfeatures','Features');
            // $xcrud->label('pproperties','Properties');
            // $xcrud->label('care_instruction','Care Instruction');

            // $xcrud->label('pwarranty','Warranty');
            // $xcrud->label('preturns','Returns');
            // $xcrud->label('image_url','Product Image');
            // $xcrud->label('pquality_promise','Quality Promise');
            // $xcrud->label('active_inactive','Product Status');

            // $xcrud->button('edit_product/{product_id}','Edit Product','glyphicon glyphicon-edit');
            // $xcrud->button('delete_product/{product_id}','Delete Product','glyphicon glyphicon-remove');


            $point_distribution_data['content'] = $xcrud->render();
            $point_distribution_data['title']='Point Distribution List';
            

            admin_view('admin/distributed_point_list',$point_distribution_data);


         }


        function plan_levels(){

        $this->form_validation->set_rules('plan_name', 'level Name', 'required|trim|regex_match[/^[a-zA-Z\s]*$/]');
        
        $this->form_validation->set_rules('plan_points', 'points', 'required|trim|regex_match[/^[1-9]\d{0,7}(?:\.\d{1,4})?$/]');

        if ($this->form_validation->run() == TRUE)
        {


           if($_POST)
            {
                
                $plan_name=$this->input->post('plan_name');
                $plan_points=$this->input->post('plan_points');
                $plan_des=$this->input->post('plan_des');
                $active_status=$this->input->post('active_status');
     
              $plan_levels_data=array(
                'plan_name'=>$plan_name,
                 'required_points'=>$plan_points,
                 'plan_description'=>$plan_des,
                 'created_date'=>date('Y-m-d'),
                 'status'=>$active_status
                 );

            }

            $plan_inserted=$this->admin_common_model->insert_data('tbl_add_plan_levels',$plan_levels_data);
            $plan_levels_data['message'] = 'Data Inserted Successfully';
        }
         admin_view('admin/plan_levels',$plan_levels_data);
    }





    function plan_levels_list(){

        $xcrud = Xcrud::get_instance()->table('tbl_add_plan_levels');
     
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->columns("plan_name,required_points,plan_description,status",true);
        
        $xcrud->label('plan_name','Level Name');
        $xcrud->label('required_points','Required Points');
        $xcrud->label('status','Service Status');

        $add_levels_data['content'] = $xcrud->render();
        $add_levels_data['title']='Plan Levels';
        

        admin_view('admin/plans_level_list',$add_levels_data);
    }


    function send_promotional_mail(){
     
        $this->form_validation->set_rules('email_subject', 'Subject', 'required|trim|regex_match[/^[a-zA-Z\s]*$/]');
        $this->form_validation->set_rules('receiver_emails', 'Reciever Emails', 'required');
        $this->form_validation->set_rules('description', 'Descriptioon', 'required');
        

        if ($this->form_validation->run() == TRUE)
        {


           if($_POST)
            {

                $receiver_emails=$this->input->post('receiver_emails');
                $email_subject=$this->input->post('email_subject');
                $description=$this->input->post('description');
                $active_status=$this->input->post('active_status');

                $email = implode(',', $receiver_emails);
               // print_r($email);die;
                $emails_data=array(
                'receiver_emails'=>$email,
                 'email_subject'=>$email_subject,
                 'description'=>$description,
                 'status'=>$active_status,
                 'send_date'=>date('Y-m-d')
                 );

                $insert_id=$this->admin_common_model->insert_data('send_emails_regitsterd_user',$emails_data);
       

     
            foreach( $receiver_emails as $emails){

                $result = $this->admin_common_model->sendmail('worthfcfc321@gmail.com','worthfcfc123','Builder Admin',$emails,$email_subject,$description);
                //$result = $this->adminmodel->sendmail2($res['email'],$subject,$message);
            }
            

                $emails_data['message'] = 'Data Inserted Successfully';

        

            }
        }

        $where = array('status'=>'active');

        $emails_data['reciver_emails']=$this->admin_common_model->fetch_total_record_wher('customer',$where);  

        admin_view('admin/send_promotional' , $emails_data);
    }




    function sent_emails(){

          $xcrud = Xcrud::get_instance()->table('send_emails_regitsterd_user');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();

            
            $xcrud->columns("receiver_emails,email_subject,description,send_date",true);
            
            $xcrud->label('receiver_emails','Reciever Emails');
            $xcrud->label('email_subject','Email Subject');
            $xcrud->label('description','Email Description');
            $xcrud->label('send_date','Send Date');
           


            $sent_email_data['content'] = $xcrud->render();
            $sent_email_data['title']='Sent Emails';
            

            admin_view('admin/All_sent_emails',$sent_email_data);


         }

    function add_permpit_form(){

      if($_POST)
            {

                $name_file=$this->input->post('name_file');
                $active_status=$this->input->post('active_status');

                $config = array(
                    'upload_path' => "./uploads/userforms/",
                    'allowed_types' => "*",
                    'overwrite' => false,
                    'file_name' => date()
                    );

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('form_file'))
                {
                    $data = array('upload_data' => $this->upload->data()); 
                    $permit_form = $this->upload->file_name;
                   
         
                } 
                else
                {
                    $permit_form = '';
                    $data = array('error' => $this->upload->display_errors()); 
                 
                }

                $add_cat=array(
                 'name_file'=>$name_file,
                 'form_file' => $permit_form,
                 'active_status'=>$active_status,
                 'create_date'=>date('Y-m-d')
                );

                $category_id=$this->admin_common_model->insert_data('import_form',$add_cat);
                $this->session->set_flashdata("success","New File has been added Successfully");
               redirect('admin/Addservices/uploaded_forms');
                if($category_id){
                $data['message'] = 'Data Inserted Successfully';

                }
                else{
               $data['error_data'] = 'Some Problem';

                }



    }


    admin_view('admin/add_permit_form', $data);

    }




    function uploaded_forms(){

          $xcrud = Xcrud::get_instance()->table('import_form');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();

             $xcrud->change_type('form_file', 'file', true, array(
            'width' => 450,
            'path' => '../uploads/userforms',
            'thumbs' => array(array(
                    'height' => 55,
                    'width' => 120,
                    'crop' => true,
                    'marker' => '_th'))));

            $xcrud->columns("name_file,form_file,active_status",true);
            
            $xcrud->label('name_file','File Name');
            $xcrud->label('form_file','File');
            $xcrud->label('active_status','Status');
            
           


            $upload_form_data['content'] = $xcrud->render();
            $upload_form_data['title']='Uploaded Forms';
            

            admin_view('admin/uploaded_permit_forms',$upload_form_data);


         }



    function add_blogs()
    {

        $this->form_validation->set_rules('blog_title', 'Blog Title', 'required|trim]');
        if ($this->form_validation->run() == TRUE)
        {
            if($_POST)
            {
                $blog_title=$this->input->post('blog_title');
               // $blog_image=$this->input->post('blog_image');
                $blog_content=$this->input->post('blog_content');
                $author_name=$this->input->post('author_name');
                $active_status=$this->input->post('active_status');
                $choose_cat=$this->input->post('choose_cat');
                $short_desc=$this->input->post('short_desc');
                $brief_intro=$this->input->post('brief_intro');
                // $category_des=$this->input->post('category_des');

               $config = array(
                    'upload_path' => "./uploads/blog-image/",
                    'allowed_types' => "*",
                    'overwrite' => false,
                    'file_name' => date()
                    );

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('blog_image'))
                {
                    $data = array('upload_data' => $this->upload->data()); 
                    $blog_image_upload = $this->upload->file_name;
                   
         
                } 
                else
                {
                    $blog_image_upload = '';
                    $data = array('error' => $this->upload->display_errors());

                 
                }
                $alpha = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $blog_title));
                $blog_url = str_replace(' ', '-', $alpha);
                $query_res = $this->db->query("SELECT * FROM `blog` WHERE `blog_url` LIKE '%$blog_url%'");
                $nomb = $query_res->num_rows();
                if ($nomb>0) {
                  $blog_url1 = $blog_url.'-'.$nomb;
                }else{
                  $blog_url1 = $blog_url;
                }
                // echo $this->db->last_query();
                // exit($blog_url1);

                $add_blog=array(
                 'blog_title'=>$blog_title,
                 'blog_url'=>$blog_url1,
                 'image_url'=>$blog_image_upload,
                 'blog_desc'=>$blog_content,
                 'author' => $author_name,
                 'status'=>$active_status,
                 'blog_category'=>$choose_cat,
                 'short_decription'=>$short_desc,
                 //'brief_intro'=>$brief_intro,
                 'created_date' => date('Y-m-d')
                );

                $blog_id=$this->admin_common_model->insert_data('blog',$add_blog);

                 $this->session->set_flashdata("success","New blog has been added Successfully");
               redirect('admin/addservices/added_blogs');

            }
        }



        $data['employees'] = $this->admin_common_model->fetch_total_record_wher('employees', array('status' =>'Active'));
        $data['blog_category_name'] = $this->admin_common_model->fetch_total_record_wher('blog_category', array('status' =>'Active'));

        admin_view('admin/add_blog',$data);
    }





    function added_blogs(){

          $xcrud = Xcrud::get_instance()->table('blog');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();
            $xcrud->unset_add();
            //$xcrud->unset_edit();

            $xcrud->change_type('image_url', 'image', false, array(
            'width' => 450,
            'path' => '../uploads/blog-image',
            'thumbs' => array(array(
                    'height' => 55,
                    'width' => 120,
                    'crop' => true,
                    'marker' => '_th'))));
            $xcrud->columns("blog_title,image_url,blog_category,short_decription,blog_desc,author,created_date,status",true);
            
            $xcrud->label('blog_title','Blog Name');
            $xcrud->label('image_url','Image');
            $xcrud->label('choose_cat','Category');
            $xcrud->label('short_decription','Short Description');
            $xcrud->label('blog_desc','Blog Description');
            $xcrud->label('author','Author');
            //$xcrud->label('brief_intro','Brief Intro');
            $xcrud->label('created_date','Published Date');
            $xcrud->label('status','Status');
            
           


            $blog_data['content'] = $xcrud->render();
            $blog_data['title']='Blogs';
            

            admin_view('admin/added_blog_list',$blog_data);


         }


    function add_blog_category(){

     $this->form_validation->set_rules('category_name', 'Category Name', 'required|trim]');
        

        if ($this->form_validation->run() == TRUE)
        {
            if($_POST)
            {
                $category_name=$this->input->post('category_name');      
              //  $parent_name=$this->input->post('parent_name');      
                $active_status=$this->input->post('active_status');
             }   

             $add_blog_parent=array(
                 'blog_cat_name'=>$category_name,
                // 'parent_category'=>$parent_name,
                 'status'=>$active_status,
                 'created_date' => date('Y-m-d')
                );

                $blog_id=$this->admin_common_model->insert_data('blog_category',$add_blog_parent);
                 $this->session->set_flashdata("success","New Category Added");
               redirect('admin/addservices/blog_category_list','refresh');


        }


        $data['blog_category'] = $this->admin_common_model->fetch_total_record_wher('blog_category', array('status' =>'Active'));

         admin_view('admin/add_blog_category',$data);
    }



    function blog_category_list(){

    $xcrud = Xcrud::get_instance()->table('blog_category');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();
           // $xcrud->unset_edit();
            $xcrud->unset_add();
            // $xcrud->change_type('image_url', 'image', false, array(
            // 'width' => 450,
            // 'path' => '../uploads/blog-image',
            // 'thumbs' => array(array(
            //         'height' => 55,
            //         'width' => 120,
            //         'crop' => true,
            //         'marker' => '_th'))));
            $xcrud->columns("blog_cat_name,status,created_date",true);
            
            $xcrud->label('blog_cat_name','Category Name');
           // $xcrud->label('parent_category','Parent Category');
            $xcrud->label('status','Status');
            $xcrud->label('created_date','Date');
           
            
           


            $blog_data['content'] = $xcrud->render();
            $blog_data['title']='Blog Categories';
            

            admin_view('admin/added_blog_list',$blog_data);


    }


    function recent_blog_commnets(){

          $xcrud = Xcrud::get_instance()->table('added_comment');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();
            $xcrud->unset_add();
            //$xcrud->unset_edit();

            // $xcrud->change_type('image_url', 'image', false, array(
            // 'width' => 450,
            // 'path' => '../uploads/blog-image',
            // 'thumbs' => array(array(
            //         'height' => 55,
            //         'width' => 120,
            //         'crop' => true,
            //         'marker' => '_th'))));
            $xcrud->columns("commenter_name,commenter_email,releted_post_title,commenter_comment,status,adding_date",true);
            
            $xcrud->label('commenter_name','Name');
            $xcrud->label('commenter_email','Email');
            $xcrud->label('releted_post_title','Comment On');
            $xcrud->label('commenter_comment','Comment');
            $xcrud->label('status','Status');
            $xcrud->label('adding_date','On Date');
            //$xcrud->label('brief_intro','Brief Intro');
            // $xcrud->label('created_date','Published Date');
            // $xcrud->label('status','Status');
            
           


            $blog_data['content'] = $xcrud->render();
            $blog_data['title']='Comments';
            

            admin_view('admin/user_comment_list',$blog_data);


         }


    /*******************ABOUT US PAGE**********************/

       function add_aboutus_content(){


     $this->form_validation->set_rules('blog_content1', 'Category Name', 'required|trim]');
     $this->form_validation->set_rules('blog_content2', 'Category Name', 'required|trim]');
     $this->form_validation->set_rules('blog_content3', 'Category Name', 'required|trim]');
        

        if ($this->form_validation->run() == TRUE)
        {
            if($_POST)
            {
                $blog_content1=$this->input->post('blog_content1');      
                $blog_content2=$this->input->post('blog_content2');      
                $blog_content3=$this->input->post('blog_content3');
                $status=$this->input->post('active_status');
             }   

             $add_aboutus=array(
                 'first_para'=>$blog_content1,
                 'second_para'=>$blog_content2,
                 'third_para'=>$blog_content3,
                 'status'=>$status
                );

                $about_id=$this->admin_common_model->insert_data('aboutus_content',$add_aboutus);
               $this->session->set_flashdata("success","Content Added Successfully");
               redirect('admin/Addservices/added_aboutus_content');


        }


    admin_view('admin/aboutus_content',$about_data);

    }


    function added_aboutus_content(){

          $xcrud = Xcrud::get_instance()->table('aboutus_content');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();
            $xcrud->unset_add();
            //$xcrud->unset_edit();

            // $xcrud->change_type('image_url', 'image', false, array(
            // 'width' => 450,
            // 'path' => '../uploads/blog-image',
            // 'thumbs' => array(array(
            //         'height' => 55,
            //         'width' => 120,
            //         'crop' => true,
            //         'marker' => '_th'))));
            $xcrud->columns("first_para,second_para,third_para,status",true);
            
            $xcrud->label('first_para','First Paragraph');
            $xcrud->label('second_para','Second Paragraph');
            $xcrud->label('third_para','Third Paragraph');
            // $xcrud->label('commenter_comment','Comment');
            $xcrud->label('status','Status');
           // $xcrud->label('adding_date','On Date');
            //$xcrud->label('brief_intro','Brief Intro');
            // $xcrud->label('created_date','Published Date');
            // $xcrud->label('status','Status');
            
           


            $blog_data['content'] = $xcrud->render();
            $blog_data['title']='Comments';
            

            admin_view('admin/user_comment_list',$blog_data);


         }

    /*************ADD TEAM MEMBER*************/

    function add_team_member(){


     $this->form_validation->set_rules('team_name', 'Name', 'required|trim]');
     $this->form_validation->set_rules('team_Designation', 'Designation', 'required|trim]');
     $this->form_validation->set_rules('member_desc', 'Description', 'required|trim]');

        

        if ($this->form_validation->run() == TRUE)
        {
              if($_POST)
            {
                $team_name=$this->input->post('team_name');        
                $team_Designation=$this->input->post('team_Designation');
                $member_desc=$this->input->post('member_desc');
                $fac_url=$this->input->post('fac_url');
                $google_url=$this->input->post('google_url');
                $twitter_url=$this->input->post('twitter_url');
                $active_status=$this->input->post('active_status');
                
                

               $config = array(
                    'upload_path' => "./uploads/team-image/",
                    'allowed_types' => "*",
                    'overwrite' => false,
                    'file_name' => date()
                    );

                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('member_image'))
                {
                    $data = array('upload_data' => $this->upload->data()); 
                    $team_image_upload = $this->upload->file_name;
                   
         
                } 
                else
                {
                    $team_image_upload = '';
                    $data = array('error' => $this->upload->display_errors());

                 
                }


                $add_team=array(
                 'team_name'=>$team_name,
                 'team_designation'=>$team_Designation,
                 'member_image_url'=>$team_image_upload,
                 'member_desc' => $member_desc,
                 'fac_url' => $fac_url,
                 'google_url' => $google_url,
                 'twitter_url' => $twitter_url,
                 'status'=>$active_status
                 
                );

                $team_member_insert=$this->admin_common_model->insert_data('add_team',$add_team);

                $this->session->set_flashdata("success","New Team Member has been added Successfully");
               redirect('admin/Addservices/team_members');

            }
            

    }
    admin_view('admin/add_team',$about_data);
    }




    function team_members(){

          $xcrud = Xcrud::get_instance()->table('add_team');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();
            $xcrud->unset_add();
            //$xcrud->unset_edit();

            $xcrud->change_type('member_image_url', 'image', false, array(
            'width' => 450,
            'path' => '../uploads/team-image',
            'thumbs' => array(array(
                    'height' => 55,
                    'width' => 120,
                    'crop' => true,
                    'marker' => '_th'))));
            $xcrud->columns("team_name,team_designation,member_image_url,member_desc,fac_url,google_url,twitter_url,status",true);
            
            $xcrud->label('team_name','Member Name');
            $xcrud->label('team_designation','Designation');
            $xcrud->label('member_image_url','Image');
            $xcrud->label('member_desc','Description');

             $xcrud->label('fac_url','Facebook');
             $xcrud->label('google_url','Google');
             $xcrud->label('twitter_url','Twitter');

            // $xcrud->label('commenter_comment','Comment');
            $xcrud->label('status','Status');
           // $xcrud->label('adding_date','On Date');
            //$xcrud->label('brief_intro','Brief Intro');
            // $xcrud->label('created_date','Published Date');
            // $xcrud->label('status','Status');
            

            $blog_data['content'] = $xcrud->render();
            $blog_data['title']='List of Team Members';
            

            admin_view('admin/team_member_list',$blog_data);


         }

     /***************Contact Form Submission******************/    

    public function contact_form_sublission_list(){

    $xcrud = Xcrud::get_instance()->table('tble_contact_form');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();
            $xcrud->unset_add();
            //$xcrud->unset_edit();

            // $xcrud->change_type('image_url', 'image', false, array(
            // 'width' => 450,
            // 'path' => '../uploads/blog-image',
            // 'thumbs' => array(array(
            //         'height' => 55,
            //         'width' => 120,
            //         'crop' => true,
            //         'marker' => '_th'))));
            $xcrud->columns("name,email,subject,message,created_date",true);
            
            $xcrud->label('name','Name');
            $xcrud->label('email','Email');
            $xcrud->label('subject','Subject');
            $xcrud->label('message','Message');
            $xcrud->label('created_date','Date');
            //$xcrud->label('brief_intro','Brief Intro');
            // $xcrud->label('created_date','Published Date');
            // $xcrud->label('status','Status');
            
           


            $blog_data['content'] = $xcrud->render();
            $blog_data['title']='Conact Submissions';
            

            admin_view('admin/contact_form_user_details',$blog_data);


    }

        function get_sub_categ()
        {
            $sub_cat_name=$this->input->post('category_id');  

            if (!empty($sub_cat_name))
            {
                $where = array('active_status'=>'active','parent_category'=>$sub_cat_name);
                $data_cat = $this->admin_common_model->fetch_where('product_categories',$where);

                echo json_encode($data_cat);

            }
        }

        /********Vision and values page***********/

         function add_vision_values(){


     $this->form_validation->set_rules('blog_content1', 'Vision', 'required|trim]');
     $this->form_validation->set_rules('blog_content2', 'Mission', 'required|trim]');
     $this->form_validation->set_rules('blog_content3', 'Values', 'required|trim]');
        

        if ($this->form_validation->run() == TRUE)
        {
            if($_POST)
            {
                $blog_content1=$this->input->post('blog_content1');      
                $blog_content2=$this->input->post('blog_content2');      
                $blog_content3=$this->input->post('blog_content3');
                $status=$this->input->post('active_status');
             }   

             $add_vision=array(
                 'vision'=>$blog_content1,
                 'mission'=>$blog_content2,
                 'values'=>$blog_content3,
                 'status'=>$status
                );

                $about_id=$this->admin_common_model->insert_data('tbl_vision_mission',$add_vision);
               $this->session->set_flashdata("success","Content Added Successfully");
               redirect('admin/Addservices/added_vision_values');


        }

    admin_view('admin/vision_values_content',$about_data);

    }

    function added_vision_values(){

          $xcrud = Xcrud::get_instance()->table('tbl_vision_mission');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();
            $xcrud->unset_add();
            //$xcrud->unset_edit();

            
            $xcrud->columns("id,vision,mission,values,status",true);
            
            $xcrud->label('vision','vision');
            $xcrud->label('mission','mission');
            $xcrud->label('values','values');
            $xcrud->label('status','status');


            $data['content'] = $xcrud->render();
            $data['title']='Added Vision AND Mission';
            

            admin_view('admin/added_vision_value_list',$data);


         }

    /*************Contact us page************/

     function add_contactus_content(){


     $this->form_validation->set_rules('blog_content1', 'Address', 'required|trim]');

        

        if ($this->form_validation->run() == TRUE)
        {
            if($_POST)
            {
                $address=$this->input->post('blog_content1');      
                $status=$this->input->post('active_status');
             }   

             $add_details=array(
                 'address'=>$address,
                 'status'=>$status
                );

               $this->admin_common_model->insert_data('tbl_add_address',$add_details);
               $this->session->set_flashdata("success","Address Added Successfully");
               redirect('admin/Addservices/added_contact_details');


        }

    admin_view('admin/add_contactus_content',$about_data);

    }

    function added_contact_details(){

          $xcrud = Xcrud::get_instance()->table('tbl_add_address');
          
            $xcrud->unset_print();
            $xcrud->unset_csv();
            $xcrud->unset_add();
            //$xcrud->unset_edit();

            
            $xcrud->columns("id,address,status",true);
            
            $xcrud->label('address','Address');
            $xcrud->label('status','status');


            $data['content'] = $xcrud->render();
            $data['title']='Added Contact Details';
            

            admin_view('admin/added_address_value_list',$data);

         }

    /****************Add FAQ's*************/


    function add_faqs()
    {
        $this->form_validation->set_rules('question', 'Question', 'required|trim]');
        $this->form_validation->set_rules('answer', 'Answer', 'required|trim]');
        if ($this->form_validation->run() == TRUE)
        {
            if($_POST)
            {
                $question=$this->input->post('question');   
                $answer=$this->input->post('answer');    
                $status=$this->input->post('active_status');
            }   

            $add_faqs=array(
                 'question'=>$question,
                 'answer'=>$answer,
                 'status'=>$status
                );

            $this->admin_common_model->insert_data('tbl_faqs',$add_faqs);
            $this->session->set_flashdata("success","Question Added Successfully");
            redirect('admin/Addservices/added_faqs');
        }

        admin_view('admin/add_faqs',$about_data);

    }

    function added_faqs()
    {
        $xcrud = Xcrud::get_instance()->table('tbl_faqs');
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->unset_add();
        //$xcrud->unset_edit();
        
        $xcrud->columns("id,question,answer,status",true);
        
        $xcrud->label('question','Question');
        $xcrud->label('answer','Answer');
        $xcrud->label('status','Status');
        $data['content'] = $xcrud->render();
        $data['title']='Added FAQs';
        admin_view('admin/added_faqs',$data);

    }




}

?>