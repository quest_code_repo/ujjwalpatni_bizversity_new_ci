<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Static_page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model','front');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view($msg = NULL){
		Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/xcrud/plugins/ckeditor/ckeditor.js';
		$xcrud = Xcrud::get_instance()->table('pages');
        /**********view pages***********/
        $xcrud->unset_add();

        $xcrud->relation('updated_by','employees','id','name');
        $xcrud->fields('updated_by','false','false','edit');
		$xcrud->order_by('id','desc');
		$xcrud->unset_remove();
		
        $data['content'] = $xcrud->render();
		$data['title'] ='Static Pages';
		admin_view('admin/allotmenu_list', $data);
    }

    /*************Below Banner Content**********/

    public function below_banner_list(){

    	$data['all_data'] = $this->front->get_data_orderby_where('tbl_banner_below','',array('deleted'=>'0'),'');

    	admin_view('admin/static_pages/banner_bottom_list',$data);

    }


    public function below_banner_bottom(){

    	if($_POST){


			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('status', 'Status', 'required');
			//$this->form_validation->set_rules('speaker_image', 'Profile Picture', 'required');

			if ($this->form_validation->run() == TRUE){

				$name_pro = $this->input->post('title');
				$description = $this->input->post('description');
				$status = $this->input->post('status');
				
				$data_insert = array(
					'title'=>$name_pro,
					'description'=>$description,
					'status'=>$status,
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('id')
					);

				$speaker_insert = $this->front->insert_data('tbl_banner_below', $data_insert);
				if ($speaker_insert){
					$this->session->set_flashdata("success", "Record added successfully");

					redirect('admin/static_page/below_banner_list/');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}


			}

    }
    	admin_view('admin/static_pages/add_banner_bottom');


    }


       public function edit_below_banner(){


    	$iteem_id = base64_decode($this->input->get('item_id'));

    	if($_POST){

    			$name_pro = $this->input->post('title');
				$description = $this->input->post('description');
				$status = $this->input->post('status');
				$update_id = base64_decode($this->input->post('updated_id'));


				$update_data =  array(
					'title'=>$name_pro,
					'description'=>$description,
					'status'=>$status,
					'updated_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
					);

				$ups_where = array('id'=>$update_id);

						
				$speaker_updates = $this->front->update_data('tbl_banner_below', $update_data,$ups_where);

				if($speaker_updates){

					$this->session->set_flashdata("update", "Record updated successfully");

					redirect('admin/static_page/below_banner_list/');

				}


    	}

    	$data['all_data'] = $this->front->get_data_orderby_where('tbl_banner_below','',array('deleted'=>'0','status'=>'active','id'=>$iteem_id),'');


    	admin_view('admin/static_pages/edit_banner_bottom',$data);

    }


    public function delete_data_coupons()
   	{

   		$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('id' => $id);
    	$delet_master = $this->front->update_data('tbl_banner_below',$master_delete_arr,$where);
    	
    	if($delet_master){

    		echo '1';
    	}

    }


     public function activate_banner_bottom(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'active');
    	$update_where = array('id'=>$program_id);

    	$run = $this->front->update_data('tbl_banner_below', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Record activated successfully");

			redirect('admin/static_page/below_banner_list/');
		}

    }


    public function deactivate_banner_bottom(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'inactive');
    	$update_where = array('id'=>$program_id);

    	$run = $this->front->update_data('tbl_banner_below', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Record deactivated successfully");

			redirect('admin/static_page/below_banner_list/');
		}

    }

    /***********END BANNER BELOW*************/


     public function second_section_list(){

    	$data['all_data'] = $this->front->get_data_orderby_where('tbl_second_section_data','',array('deleted'=>'0'),'');

    	admin_view('admin/static_pages/second_section_list',$data);

    }


    public function add_second_section(){

    	if($_POST){


			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('sub_title', 'Sub Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('status', 'Status', 'required');
			//$this->form_validation->set_rules('speaker_image', 'Profile Picture', 'required');

			if ($this->form_validation->run() == TRUE){

				$name_pro = $this->input->post('title');
				$description = $this->input->post('description');
				$sub_title = $this->input->post('sub_title');
				$status = $this->input->post('status');
				

				$config = array(
					'upload_path' => "./uploads/front_speaker_video",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('speaker_video'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/static_page/add_second_section');
					exit();
				}

				$check_first = $this->front->get_data_orderby_where('tbl_second_section_data','',array('deleted'=>'0'),'');

				if(empty($check_first)){
				
				$data_insert = array(
					'heading'=>$name_pro,
					'description'=>$description,
					'status'=>$status,
					'video_url'=>$uimg,
					'sub_title'=>$sub_title,
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('id')
					);

				$speaker_insert = $this->front->insert_data('tbl_second_section_data', $data_insert);
				if ($speaker_insert){
					$this->session->set_flashdata("success", "Record added successfully");

					redirect('admin/static_page/second_section_list/');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
					redirect('admin/static_page/second_section_list/');
				}

			} else {


				$this->session->set_flashdata("error", "Only One Entry Allowed");
					redirect('admin/static_page/second_section_list/');

			}


			}

    }
    	admin_view('admin/static_pages/add_second_section');


    }


     public function edit_second_section(){

    	$iteem_id = base64_decode($this->input->get('item_id'));

    	if($_POST){

    			$name_pro = $this->input->post('title');
				$description = $this->input->post('description');
				$sub_title = $this->input->post('sub_title');
				$status = $this->input->post('status');
				$hidden_video = $this->input->post('video_if_not_change');
				$update_id = base64_decode($this->input->post('updated_id'));

				$config = array(
					'upload_path' => "./uploads/front_speaker_video",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('speaker_video'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else if($hidden_video=='')
				{
					$uimg = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/static_page/add_second_section');
					exit();
				}

				if($uimg == '' && $hidden_video!=''){

					$uimg = $hidden_video;
				
				}


				$update_data =  array(
					'heading'=>$name_pro,
					'description'=>$description,
					'status'=>$status,
					'video_url'=>$uimg,
					'sub_title'=>$sub_title,
					'updated_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
					);

				$ups_where = array('id'=>$update_id);

						
				$speaker_updates = $this->front->update_data('tbl_second_section_data', $update_data,$ups_where);

				if($speaker_updates){

					$this->session->set_flashdata("update", "Record updated successfully");

					redirect('admin/static_page/second_section_list/');

				}


    	}

    	$data['all_data'] = $this->front->get_data_orderby_where('tbl_second_section_data','',array('deleted'=>'0','status'=>'active','id'=>$iteem_id),'');


    	admin_view('admin/static_pages/edit_second_section_list',$data);

    }


     public function delete_data_second_section()
   	{

   		$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('id' => $id);
    	$delet_master = $this->front->update_data('tbl_second_section_data',$master_delete_arr,$where);
    	
    	if($delet_master){

    		echo '1';
    	}

    }


     public function activate_banner_sections(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'active');
    	$update_where = array('id'=>$program_id);

    	$run = $this->front->update_data('tbl_second_section_data', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Record activated successfully");

			redirect('admin/static_page/second_section_list/');
		}

    }


    public function deactivate_banner_sections(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'inactive');
    	$update_where = array('id'=>$program_id);

    	$run = $this->front->update_data('tbl_second_section_data', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Record deactivated successfully");

			redirect('admin/static_page/second_section_list/');
		}

    }

    /*****************END SECOND SECTION************/


    /***************IMPROVE SECTION START************/


    public function improve_section_list(){

    	$data['all_data'] = $this->front->get_data_orderby_where('tbl_improve_section_front','',array('deleted'=>'0','type'=>'point_data'),'');


    	$data['title'] = $this->front->get_data_orderby_where('tbl_improve_section_front','',array('deleted'=>'0','type'=>'title_data'),'');

    	admin_view('admin/static_pages/improve_section_list',$data);

    }


    public function add_improve_section(){

    	if($_POST){


			// $this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('sub_title', 'Sub Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('status', 'Status', 'required');
			//$this->form_validation->set_rules('speaker_image', 'Profile Picture', 'required');

			if ($this->form_validation->run() == TRUE){

				$description = $this->input->post('description');
				$sub_title = $this->input->post('sub_title');
				$status = $this->input->post('status');
				

				$config = array(
					'upload_path' => "./uploads/improve_section_images",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else
				{
					$uimg = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/static_page/add_improve_section');
					exit();
				}

				$data_insert = array(
					// 'heading'=>$name_pro,
					'description'=>$description,
					'status'=>$status,
					'image_url'=>$uimg,
					'sub_title'=>$sub_title,
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('id')
					);

				$speaker_insert = $this->front->insert_data('tbl_improve_section_front', $data_insert);
				if ($speaker_insert){
					$this->session->set_flashdata("success", "Record added successfully");

					redirect('admin/static_page/improve_section_list/');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
					redirect('admin/static_page/improve_section_list/');
				}



			}

    }
    	admin_view('admin/static_pages/add_improve_section');


    }


     public function edit_improve_section(){

    	$iteem_id = base64_decode($this->input->get('item_id'));

    	if($_POST){

    			//$name_pro = $this->input->post('title');
				$description = $this->input->post('description');
				$sub_title = $this->input->post('sub_title');
				$status = $this->input->post('status');
				$hidden_video = $this->input->post('video_if_not_change');
				$update_id = base64_decode($this->input->post('updated_id'));

				$config = array(
					'upload_path' => "./uploads/improve_section_images",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else if($hidden_video=='')
				{
					$uimg = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/static_page/add_improve_section');
					exit();
				}

				if($uimg == '' && $hidden_video!=''){

					$uimg = $hidden_video;
				
				}


				$update_data =  array(
					//'heading'=>$name_pro,
					'description'=>$description,
					'status'=>$status,
					'image_url'=>$uimg,
					'sub_title'=>$sub_title,
					'updated_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
					);

				$ups_where = array('id'=>$update_id);

						
				$speaker_updates = $this->front->update_data('tbl_improve_section_front', $update_data,$ups_where);

				if($speaker_updates){

					$this->session->set_flashdata("update", "Record updated successfully");

					redirect('admin/static_page/improve_section_list/');

				}


    	}

    	$data['all_data'] = $this->front->get_data_orderby_where('tbl_improve_section_front','',array('deleted'=>'0','status'=>'active','id'=>$iteem_id),'');


    	admin_view('admin/static_pages/edit_improve_section_list',$data);

    }

      public function delete_data_improve(){

   		$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('id' => $id);
    	$delet_master = $this->front->update_data('tbl_improve_section_front',$master_delete_arr,$where);
    	
    	if($delet_master){

    		echo '1';
    	}

    }


     public function activate_imrove_sections(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'active');
    	$update_where = array('id'=>$program_id);

    	$run = $this->front->update_data('tbl_improve_section_front', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Record activated successfully");

			redirect('admin/static_page/improve_section_list/');
		}

    }


    public function deactivate_improve_sections(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'inactive');
    	$update_where = array('id'=>$program_id);

    	$run = $this->front->update_data('tbl_improve_section_front', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Record deactivated successfully");

			redirect('admin/static_page/improve_section_list/');
		}

    }

    public function edit_improve_main_title(){

    	$iteem_id = base64_decode($this->input->get('item_id'));

    	if($_POST){

    		$name = $this->input->post('heading');
			$update_id = base64_decode($this->input->post('updated_id'));

    		$update_data =  array(
					//'heading'=>$name_pro,
					'main_title'=>$name,
					'updated_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
					);

				$ups_where = array('id'=>$update_id);
						
				$speaker_updates = $this->front->update_data('tbl_improve_section_front', $update_data,$ups_where);

				if($speaker_updates){

					$this->session->set_flashdata("update", "Record updated successfully");

					redirect('admin/static_page/improve_section_list/');

				}



    	}


    	$data['all_data'] = $this->front->get_data_orderby_where('tbl_improve_section_front','',array('deleted'=>'0','status'=>'active','id'=>$iteem_id),'');

    	admin_view('admin/static_pages/edit_single_improve',$data);

    }

    /*************Improve Section End**********/


    public function about_list(){


    	$data['all_data'] = $this->front->get_data_orderby_where('aboutus_content','',array('deleted'=>'0'),'');

    	admin_view('admin/static_pages/about_us_list',$data);


    }


    public function edit_about_us_data(){

    	$iteem_id = base64_decode($this->input->get('item_id'));

    	if($_POST){


    			$name_pro = $this->input->post('title');
				$sub_title = $this->input->post('sub_title');
				$speaker_name = $this->input->post('speaker_name');

				$front_description = $this->input->post('description_front_data');
				$inner_description = $this->input->post('description_back_data');

				$status = $this->input->post('status');

				$front_img_hidden = $this->input->post('front_image_hidden');
				$inner_img_hidden = $this->input->post('inner_image_hidden');

				$update_id = base64_decode($this->input->post('updated_id'));

				$config = array(
					'upload_path' => "./uploads/speaker_image",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);

				$this->load->library('upload', $config);
				
				$this->upload->initialize($config);
				if ($this->upload->do_upload('front_image'))
				{
					$uploadimg = $this->upload->data();
					$uimg = $uploadimg['file_name'];
				}
				else if($front_img_hidden=='')
				{
					$uimg = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/static_page/about_list');
					exit();
				}

				if($uimg == '' && $front_img_hidden!=''){

					$uimg = $front_img_hidden;
				
				}


				$this->upload->initialize($config);
				if ($this->upload->do_upload('back_image'))
				{
					$uploadimg_back = $this->upload->data();
					$uimg_back = $uploadimg_back['file_name'];
				}
				else if($inner_img_hidden=='')
				{
					$uimg_back = '';
					$this->session->set_flashdata("error", $this->upload->display_errors('<p class="alert alert-danger">', '</p>'));
					redirect('admin/static_page/about_list');
					exit();
				}

				if($uimg_back == '' && $inner_img_hidden!=''){

					$uimg_back = $inner_img_hidden;
				
				}



				$update_data =  array(
					'inner_title'=>$name_pro,
					'sub_heading'=>$sub_title,
					'speaker_name'=>$speaker_name,
					'front_description'=>$front_description,
					'inner_description'=>$inner_description,
					'front_image'=>$uimg,
					'inner_image'=>$uimg_back,
					'status'=>$status,
					'updated_at'=>date('Y-m-d H:i:s'),
					'updated_by'=>$this->session->userdata('id')
					);

				$ups_where = array('id'=>$update_id);

						
				$speaker_updates = $this->front->update_data('aboutus_content', $update_data,$ups_where);

				if($speaker_updates){

					$this->session->set_flashdata("update", "Record updated successfully");

					redirect('admin/static_page/about_list/');

				}


    	}


    	$data['all_data'] = $this->front->get_data_orderby_where('aboutus_content','',array('deleted'=>'0','status'=>'active','id'=>$iteem_id),'');

    	admin_view('admin/static_pages/add_about_us_section',$data);

    }

     public function delete_aboutus_content(){

   		$id = $this->input->post('id');
     	
     	$master_delete_arr = array(
	        'deleted_by'=>$this->session->userdata('id'),
	        'deleted'   =>'1',
	        'deleted_at'=>date('Y-m-d H:i:s')	        
	    );

      	$where = array('id' => $id);
    	$delet_master = $this->front->update_data('aboutus_content',$master_delete_arr,$where);
    	
    	if($delet_master){

    		echo '1';
    	}

    }


     public function activate_aboutus_content(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'active');
    	$update_where = array('id'=>$program_id);

    	$run = $this->front->update_data('aboutus_content', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Record activated successfully");

			redirect('admin/static_page/about_list/');
		}

    }


    public function deactivate_aboutus_content(){

    	$program_id = base64_decode($this->input->get('program_id'));

    	$update_data = array('status'=>'inactive');
    	$update_where = array('id'=>$program_id);

    	$run = $this->front->update_data('aboutus_content', $update_data,$update_where);

    	if($run){

    		$this->session->set_flashdata("success", "Record deactivated successfully");

			redirect('admin/static_page/about_list/');
		}

    }













	
}