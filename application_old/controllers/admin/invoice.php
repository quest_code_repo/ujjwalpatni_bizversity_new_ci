<script>
function printData()
{
   var divToPrint=document.getElementById("contentwrapper");
   newWin= window.open('', 'Order Invoice', 'height=auto,width=800');
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
}
</script>
 <div class="centercontent">
     <form method="post">
      <div class="pageheader notab">
           <table width="90%"><tr><td>
    <h1 class="pagetitle">Order Details</h1></td><td align="right">
    <a class="btn btn_orange btn_print" href="javascript:void(0);" onclick="printData();">
<span>Print</span>
</a>
  </td></tr></table>
       </div><!--pageheader-->
   </form>
        <div id="contentwrapper" class="contentwrapper">
      <?php if(!empty($info)) { ?>
              <table style="width:500px;margin:0px;padding:0px;border:1px solid #CCC;border-collapse:collapse;font-size:12px;">
  <tr style="border:1px solid #CCC;border-collapse:collapse;">
    <td width="50%" style="border:1px solid #CCC;border-collapse:collapse;padding:5px;"><img src="<?php echo admin_assets; ?>images/logo.png" height="60" /></td>
    <td align="right" style="border:1px solid #CCC;border-collapse:collapse;padding:5px;" valign="top"><strong>Green Basket </strong><br />
      Mobile : 7770877709<br />
    Phone : 0731-6565654
    </td>
  </tr>
  <tr style="border:1px solid #CCC;border-collapse:collapse;">
    <td colspan="2" style="padding:5px;" align="center">RECIPT</td>
    
  </tr>
  <tr>
    <td style="border:1px solid #CCC;border-collapse:collapse;padding:5px;"><strong>Billing Address :</strong> <br />
    <?php echo $info->billing_address; ?>, <?php echo $info->billing_city; ?> (<?php echo $info->billing_state; ?>) - <?php echo $info->billing_pin; ?>
   <br /> Contact : <?php echo $info->billing_contact; ?>
    </td>
    <td style="border:1px solid #CCC;border-collapse:collapse;padding:5px;"><strong>Shipping Address :</strong> <br />
    <?php echo $info->shipping_address; ?>, <?php echo $info->shipping_city; ?> (<?php echo $info->shipping_state; ?>) - <?php echo $info->shipping_pincode; ?>
   <br /> Contact : <?php echo $info->shipping_contact; ?>
    </td>
  </tr>
   <tr>
    <td style="border:1px solid #CCC;border-collapse:collapse;padding:5px;"><strong>Order Id :</strong> <?php echo $info->order_id; ?> 

    </td>
    <td style="border:1px solid #CCC;border-collapse:collapse;padding:5px;"><strong>Order Date :</strong>  <?php echo $info->order_time; ?>
    
    </td>
  </tr>
  <tr>
    <td colspan="2"><table style="width:500px;margin:0px;padding:0px;border:1px solid #CCC;border-collapse:collapse;font-size:12px;text-align:center;">
      <tr align="center">
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Product Name</strong></td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Unit</strong></td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Quantity</strong></td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><strong>Subtotal</strong></td>
      </tr>
      <?php $total=0; if(!empty($order)) {
		  foreach($order as $item) { ?>
      <tr>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><?php echo $item['pname']; ?> (<?php echo $item['hname']; ?>)</td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><?php echo $item['weight']; ?> <?php echo $item['name']; ?></td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><?php echo $item['qua']; ?>*<?php echo $item['price']; ?></td>
        <td style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><?php echo $ss=number_format($item['qua']*$item['price'],2); ?></td>
      </tr>
      <?php $total=$total+$ss; }} ?>
    </table></td>
    </tr>
  <tr>
    <td align="center" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Total</td>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><?php echo number_format($total,2); ?></td>
  </tr>
  <tr>
    <td align="center" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;">Wallet Outstending : </td>
    <td align="right" style="padding:5px;border:1px solid #CCC;border-collapse:collapse;"><?php echo $wallet; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<?php } ?>
        </div><!--contentwrapper-->
            
        
</div><!-- centercontent -->
    
    
</div><!--bodywrapper-->

</body>

</html>