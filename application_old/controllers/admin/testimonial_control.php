<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model','admin');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session')); 
	}
	
	 
	public function view_category(){
		
		$xcrud = Xcrud::get_instance()->table('testimonials_category');
        /**********view pages***********/
		$xcrud->order_by('test_cat_id', 'desc');
		$xcrud->fields(array('category_name','status'));
		$xcrud->columns('category_name,status');
        $data['contentdata'] = $xcrud->render();
		$data['title'] = 'Testimonial Category List';
		admin_view('admin/testinomial/testmonial_category_list', $data);
    }

	public function add_testimonial(){
		if ($_POST) {
			// echo "<pre>";print_r($_POST);die;
			$category 	  = $this->input->post('category');
			$title 		  = $this->input->post('title');
			$descriptions = $this->input->post('descriptions');

			$config = array(
				'upload_path' => "./uploads/testinomial_images",
				'allowed_types' => "*",
				'overwrite' => false,
				'file_name' => time()
			);
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ($this->upload->do_upload('testi_image'))
			{
				$uploadimg = $this->upload->data();
				$image = $uploadimg['file_name'];
			}
			
			foreach($category as $cate_id) 
	   	 	{
       			$testi_data = array(
					'category_id'	=> $cate_id,
					'title'			=> $title,
					'descriptions'	=> $descriptions,
					'testi_image'	=> $image,
	       		);
	       		$this->admin->insert_data('testimonials',$testi_data);
	  	  	}

	  	  	$this->session->set_flashdata("success", "Testimonials Added Successfully.");
			redirect('admin/testimonial_control/view');
			exit();				
		}
		$data['testimonial_category'] = $this->admin->get_data_orderby_where('testimonials_category','',array('deleted'=>'0','status'=>'active'),'');
		admin_view('admin/testinomial/add_testinomial', $data);
	}

    public function view($msg = NULL){
		
		$xcrud = Xcrud::get_instance()->table('testimonials');
        /**********view pages***********/
		$xcrud->order_by('testimonials_id','ASC');
		$xcrud->unset_add();
		$xcrud->unset_edit();

		$xcrud->change_type('testi_image','image', '',array('width' => 400,'height' => 230));
		
		$xcrud->relation('category_id','testimonials_category','test_cat_id','category_name',array('testimonials_category.status' => 'active'));

		$xcrud->button('admin/testimonial_control/edit_testimonial/?testim_id={testimonials_id}','Update','icon-pencil');

        $data['contentdata'] = $xcrud->render();
		$data['title'] ='Testimonial List';

		admin_view('admin/testinomial/testmonial_list', $data);
    }

    public function edit_testimonial(){
    	$testim_id = $this->input->get('testim_id');
    	
		if ($_POST) {
			$testimo_id	  = $this->input->post('testimonials_id');
			$title 		  = $this->input->post('title');
			$descriptions = $this->input->post('descriptions');
			if (!empty($_FILES['testi_image']['name'])) {
				$config = array(
					'upload_path' => "./uploads/testinomial_images",
					'allowed_types' => "*",
					'overwrite' => false,
					'file_name' => time()
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('testi_image'))
				{
					$uploadimg = $this->upload->data();
					$image = $uploadimg['file_name'];
				}
			}else{
				$image = $this->input->post('old_image');
			}			
			
   			$up_data = array(
				'title'			=> $title,
				'descriptions'	=> $descriptions,
				'testi_image'	=> $image,
       		);
   			$where = array('testimonials_id'=>$testimo_id);
       		$update_program = $this->admin->update_data('testimonials', $up_data, $where);
	  	  	$this->session->set_flashdata("success", "Testimonials Updated Successfully.");
			redirect('admin/testimonial_control/view');
			exit();				
		}

		
		$data['testimonial_category'] = $this->admin->get_data_orderby_where('testimonials_category','',array('deleted'=>'0','status'=>'active'),'');

		$where = array('testimonials_id'=>$testim_id);
		$data['update_record'] = $this->admin->getdata_orderby_where_join_two('testimonials', 'testimonials_category', 'category_id','test_cat_id',$where, $column = '', $orderby="" , $group_by="");
		// echo "<pre>";print_r($data['update_record']);die;
		admin_view('admin/testinomial/edit_testinomial', $data);
	}

    public function front_content()
    {
    	Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/xcrud/plugins/ckeditor/ckeditor.js';
    	$xcrud = Xcrud::get_instance()->table('front_content');
    	$data['contentdata'] = $xcrud->render('edit', 1);
    	// $xcrud->hide_button('save_new');
    	$xcrud->hide_button('Return');
    	// $xcrud->hide_button('save_return');
		$data['title'] ='Content List';
		admin_view('admin/content', $data);
    }
	
}
