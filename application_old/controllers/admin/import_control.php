<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session','image_lib','csvimport')); 
	}
	
	public function index($msg = NULL)
	{
		$data['error']='';
		$data['title'] ='Import Product';
		admin_view('admin/import_product', $data);
    }
	
	public function createurl($input) {
		$input = str_replace("&nbsp;", " ", $input);
		$input = str_replace(array("'", "-"), "", $input); //remove single quote and dash
		$input = mb_convert_case($input, MB_CASE_LOWER, "UTF-8"); //convert to lowercase
		$input = preg_replace("#[^a-zA-Z]+#", "-", $input); //replace everything non an with dashes
		$input = preg_replace("#(-){2,}#", "$1", $input); //replace multiple dashes with one
		$input = trim($input, "-"); //trim dashes from beginning and end of string if any
		return $input; 
	}
	
	function save_product_import()
	{
		//check for csv file
		if(!empty($_FILES['userfile']['name']))
		{
			$x=array(); $m=0; $i=0; $ab=0;
			$pathsaved="./uploads/product_list/";
			$filename=$_FILES['userfile']['name'];
			$type=$_FILES['userfile']['type']; 
			$path1="./uploads/product_list/";
			$path=$path1.date('ymdhsi').$_FILES['userfile']['name'];
			move_uploaded_file($_FILES['userfile']['tmp_name'], $path);
			$handle=fopen("$path","r");
			$i=1;
			while(($data=fgetcsv($handle,1000,","))!== FALSE)
			{

				//checking for first row
				if($i!=1)
				{	

					if(!empty($data[1]))
					{

			 		$pname=trim($data[1]);
					}
					else
					{
			 		$pname='';	
					}

				if(!empty($data[2]))
				{

			 	$pfeatures=trim($data[2]);
				}
				else
				{
			 	$pfeatures='';	
				}

				if(!empty($data[3]))
				{

			 	$care_instruction=trim($data[3]);
				}
				else
				{
			 	$care_instruction='';	
				}

				if(!empty($data[4]))
				{

			 	$pwarranty=trim($data[4]);
				}
				else
				{
			 $pwarranty='';	
				}

				if(!empty($data[5]))
				{

			 	$pquality_promise=trim($data[5]);
				}
				else
				{
			 	$pquality_promise='';	
				}



				if(!empty($data[6]))
				{

			 	$preturns=trim($data[6]);
				}
				else
				{
			 	$preturns='';	
				}
			
				if(!empty($data[7]))
				{

			 	$variant_ids=trim($data[7]);
				}
				else
				{
			 	$variant_ids='';	
				}
			

				if(!empty($data[8]))
				{

				$product_category_id=trim($data[8]);
			
				}
				else
				{
			 	$product_category_id=0;	
				}

				if(!empty($data[9]))
				{

				$quantity=trim($data[9]);
			
				}
				else
				{
			 	$quantity=1;	
				}

			if(!empty($data[10]))
			{

			    $product_price=trim($data[10]);
			
			}
			else
			{
			 	$product_price=0;	
			}


			if(!empty($data[11]))
			{

				$is_featured=trim($data[11]);			
			}
			else
			{
			 	$is_featured='No';	
			}


			if(!empty($data[12]))
			{

			$recommended=trim($data[12]);
			
			}
			else
			{
			 	$recommended='No';	
			}

			if(!empty($data[13]))
			{

				$image_url=trim($data[13]);
			
			}
			else
			{
			 	$image_url='';	
			}

			if(!empty($data[14]))
			{

			  $status=trim($data[14]);
			
			}
			else
			{
			 	$status='Out Of Stock';	
			}
			
			
			if(!empty($data[15]))
			{

			  $active_inactive=trim($data[15]);	
			
			}
			else
			{
			 	$active_inactive='inactive';	
			}
			
			
			
			//check for product existing row
			if(!empty($pname))
			{

			$product_data=array(
			 'pname'=>$pname,
			 'pfeatures'=>$pfeatures,
			 'care_instruction'=>$care_instruction,
			 'pwarranty'=>$pwarranty,
			 'pquality_promise'=>$pquality_promise,
			 'preturns'=>$preturns,
			 'variant_ids'=>$variant_ids,
			 'product_category_id'=>$product_category_id,
			 'quantity'=>$quantity,
			 'product_price'=>$product_price,
			 'is_featured'=>$is_featured,
			 'recommended'=>$recommended,
			 'image_url'=>$image_url,
			 'status'=>$status,
			 'active_inactive'=>$active_inactive
			);
			$this->admin_common_model->insert_data('products',$product_data);
			 $ab++;
			}

		  }
			//check for product existing row

			$i++;
		}

		redirect('admin/furnish_dashboard/product_list');
		
	}
	else
	{
		redirect('admin/import_control');
	}
		//check for csv file
		
		
		
        
	}
	
}