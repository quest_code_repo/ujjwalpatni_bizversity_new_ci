<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bulk_import_quiz extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/Admin_common_model');
		$this->load->helper('xcrud');
		$this->load->library(array('form_validation','session','image_lib','')); 
		
	}
	
	public function bulk_upload_quiz()
	{
    	$data['test_list']=$this->Admin_common_model->fetch_record('tbl_test');
    	$data['chapter_list']=$this->Admin_common_model->fetch_record('tbl_chapter');
    	$data['topic_list']=$this->Admin_common_model->fetch_record('tbl_lecture');
		admin_view('admin/bulk_quiz_upload',$data);
    }

    public function quiz_bulk_submit()
	{     
        //$count=0;
        if(isset($_POST['btn-upload']))
        {
            // $test_id = $this->input->post('test_name');
            // $type = $this->input->post('question_type');
            // $chapter_list = $this->input->post('chapter_name');
            // $topic_list = $this->input->post('topic_name');

            $config['upload_path'] = './uploads/quiz_questions_file/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '1024*20';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload())
            {
                $error = $this->upload->display_errors();

                $this->session->set_flashdata('error',$error."Insert Only CSV Format");
                redirect('admin/bulk_import_quiz/bulk_upload_quiz'); 
            }else
            {
                $file_info = $this->upload->data();
                $file_name = $file_info['file_name'];
                //echo "fdgdfg"; 
                if(!$file_info)
                {
                    echo "File Not Uploaded";
                }
                else
                {
                    $file_uploded = base_url().'uploads/'.$file_name;
                    if (!file_exists(FCPATH."uploads/quiz_questions_file/".$file_name)) 
                    {
                        echo "File Not exist";
                    }
                    else
                    {
                        $handle = fopen(FCPATH."uploads/quiz_questions_file/".$file_name, "r") or die("file cannot open");
                        while($csv_line = fgetcsv($handle,1024))
                        {
                            $count++;
                            if($count == 1)
                            {
                                continue;
                            }
                            for($i = 0, $j = count($csv_line); $i < $j; $i++)
                            {
                                $insert_csv = array();
                                $insert_csv['Roll No'] = $csv_line[0];
                                $insert_csv['Email'] = $csv_line[1];
                                $insert_csv['Password'] = $csv_line[2];
                                $insert_csv['Full Name'] = $csv_line[3];
                                $insert_csv['DOB'] = $csv_line[4];
                                $insert_csv['Mobile'] = $csv_line[5];
                                $insert_csv['Address'] = $csv_line[6];
                                $insert_csv['City'] = $csv_line[7];
                                $insert_csv['State'] = $csv_line[8];
                                $insert_csv['class'] = $csv_line[9];
                            }
                            $i++;
                            $data = array(
                            'roll_number' => $insert_csv['Roll No'],
                            'username' => $insert_csv['Email'],
                            'password' => $insert_csv['Password'],
                            'full_name' => $insert_csv['Full Name'],
                            'date_ob' => $insert_csv['DOB'],
                            'class_name' => $insert_csv['class'],
                            'image_name' => '',
                            'mobile' => $insert_csv['Mobile'],
                            'alt_mobile' => '',
                            'gender' => '',
                            'address' => $insert_csv['Address'],
                            'city' => $insert_csv['City'],
                            'state' => $insert_csv['State'],
                            'pincode' => '',
                            'otp' => '',
                            'created_at' => date("Y-m-d"),
                            'status' => 'active');

                            $insert_questions=$this->Admin_common_model->insert_data('tbl_students', $data);
                        }
                        fclose($handle) or die("can't close file");
                        $data['success']="success";
                        $this->session->set_flashdata('success',"Students Uploaded Successfully");
                        redirect('admin/bulk_import_quiz/bulk_upload_quiz'); 
                    }
                }
            }
        }
        else
        {
            echo "Moveing failed";
        }
    }


	public function import_question_images()
	 {

	 	$data = array();
        if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name']))
        {
            $filesCount = count($_FILES['userFiles']['name']);
            for($i = 0; $i < $filesCount; $i++)
            {
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

                $uploadPath = 'uploads/question_images/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

  //               if (!$this->upload->do_upload('userFile'))
		// {
		// 	$error = $this->upload->display_errors();	 
	 //        $this->session->set_flashdata('error',$error."Insert Only JPG,PNG Format");
 			
		// }

            if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i]['modified'] = date("Y-m-d H:i:s");

            }

        
         
        }
      $this->session->set_flashdata('success',"Images Uploaded Successfully");
      redirect('admin/bulk_import_quiz/bulk_upload_quiz');           
           
        }
	  admin_view('admin/select_uploading_category');
      
	 } 

    
	
}
	
