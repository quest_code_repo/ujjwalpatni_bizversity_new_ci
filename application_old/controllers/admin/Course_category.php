<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course_category extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('admin/admin_common_model','front');
		$this->load->helper('xcrud');
		$this->load->helper('main_template_helper');
		$this->load->library(array('form_validation','session','image_lib')); 
	}


	public function index(){

		//print_r($this->session->userdata('name'));die;

		if($_POST)
		{
			$this->form_validation->set_rules('course_category_name', 'Course Category Name', 'required');
			$this->form_validation->set_rules('active_status', 'Product Price', 'required');




			if ($this->form_validation->run() == TRUE)
			{

				$name   = $this->input->post('course_category_name');
				$status = $this->input->post('active_status');

				$course_category = $this->front->getwheres('course_category',array('course_category_name' => $name,'deleted'=>'0','status'=>'active'));

				if(!empty($course_category))
				{
					$this->session->set_flashdata("error", " $name Course Category already exists, Please Try Another Name ");
					redirect('admin/Course_category','refresh');
					die();
				}


				$data_insert = array('course_category_name'      =>$name,
					                 'status'    =>$status,
					                 'created_at'=>date('Y-m-d H:i:s'),
					                 'created_by'=>$this->session->userdata('id'));

				$cat_insert = $this->front->insert_data('course_category', $data_insert);
				if ($cat_insert)
				{
					$this->session->set_flashdata("success", "Course Category added successfully");

					redirect('admin/Course_category/course_category_list');
				}
				else
				{
					$this->session->set_flashdata("error", "Something went wrong..");
				}

			}
			
			
		}


		admin_view('admin/course_category/add_course_category', $data);
	}


	public function course_category_list(){

		$data['all_data'] = $this->front->get_data_orderby_where('course_category','',array('deleted'=>'0','status'=>'active'),'');

		admin_view('admin/course_category/course_category_list', $data);
	}


	// ****************************** End Of Course Category List*****************************8


	public function course_list()
	{
		$xcrud = Xcrud::get_instance()->table('tbl_course');
        /**********view pages***********/
        $xcrud->unset_remove();
        $xcrud->unset_add();
        $xcrud->unset_edit();
        // $xcrud->unset_search();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        
        // $xcrud->change_type('sem_image', 'image', false, array(
        //     'width' => 450,
        //     'path' => '../uploads/semester',
        //     'thumbs' => array(
        //         array(
        //             'height' => 55,
        //             'width' => 120,
        //             'crop' => true,
        //             'marker' => '_th'
        //         )
        //     )
        // ));
        
        
        // $xcrud->columns("sem_name,sem_date,sem_status", true);
        
        // $xcrud->label('sem_name', 'Name');
        // $xcrud->label('sem_duration', 'Duration(In Hours)');
        // $xcrud->label('sem_no_of_chapter', 'No of Chapter');
        // $xcrud->label('sem_no_of_lecture', 'No of Topic');
        // $xcrud->label('sem_price', 'Price');
        // // $xcrud->label('sem_image','Image');
        // $xcrud->label('sem_description', 'Description');
        // $xcrud->label('sem_date', 'Date');
        // $xcrud->label('sem_status', 'Course Status');
        
        // $xcrud->button('edit_semester/{sem_id}', 'Edit Course', 'glyphicon glyphicon-edit');
        // $xcrud->button('delete_semester/{sem_id}','Delete Semester','glyphicon glyphicon-remove');
        
        
        $data['content'] = $xcrud->render();
        $data['title']   = 'Course List';
        admin_view('admin/course_category/course_list', $data);
        
	}


	public function add_new_course()
	{
		if ($_POST) {
            $this->form_validation->set_rules('course_name', 'Course Name', 'required');
            // $this->form_validation->set_rules('sem_duration', 'Semester Duration', 'required');
            // $this->form_validation->set_rules('sem_no_of_chapter', 'No of Chapter', 'required');
            // $this->form_validation->set_rules('sem_no_of_lecture', 'No of lecture', 'required');
            // $this->form_validation->set_rules('sem_price', 'Semester Price', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $sem_name          = $this->input->post('sem_name');
                $sem_duration      = $this->input->post('sem_duration');
                $sem_no_of_chapter = $this->input->post('sem_no_of_chapter');
                $sem_no_of_lecture = $this->input->post('sem_no_of_lecture');
                $sem_price         = $this->input->post('sem_price');
                $sem_description   = $this->input->post('sem_description');
                $sem_status        = $this->input->post('sem_status');
                
              
                $uimg     = '';
                $sem_data = array(
                    'sem_name' => $sem_name,
                    'sem_duration' => $sem_duration,
                    'sem_image' => $uimg,
                    'sem_no_of_chapter' => $sem_no_of_chapter,
                    'sem_no_of_lecture' => $sem_no_of_lecture,
                    'sem_price' => $sem_price,
                    'sem_description' => $sem_description,
                    'sem_date' => date("Y-m-d"),
                    'sem_status' => $sem_status
                    
                );
                
                $sem_id = $this->admin_common_model->insert_data('tbl_course', $sem_data);
                if ($sem_id > 0) {
                    $this->session->set_flashdata("success", "Course added successfully");
                } else {
                    $this->session->set_flashdata("error", "Something went wrong..");
                }
                
                redirect('admin/course_category/course_list');
                
            }
        }
        
        $data['course_category'] = $this->front->get_data_orderby_where('course_category','',array('deleted'=>'0','status'=>'active'),'');

        
        admin_view('admin/course_category/add_course',$data);
	}
	


} 

?>