<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {
  
  function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        //$this->load->helper('front_end/cat_frontend');
        
    }

    public function index()
    {

        $getData = $this->input->get();
        if(!empty($getData))
        {

           if(!empty($getData['product_category']) && empty($getData['product_languages']))
           {
                 $data['Products'] = $this->home_model->getwheres_data('products',array('product_type'=>'product','active_inactive'=>'active','product_category_id'=>$getData['product_category'],'stock_status'=>'In Stock','deleted'=>'0'));

                

                 $data['Product_category'] =  $this->home_model->getwheres_data('product_categories',array('parent_category'=>14));

                 $data['Product_languages'] = $this->home_model->getwheres_data('product_language',array('language_status'=>'Active','deleted'=>'0'));

               
           }

           elseif(!empty($getData['product_languages']) && empty($getData['product_category']))
           {
                         $pro_languages =  explode(',',$getData['product_languages']);

                         $query =  $this->db->query("SELECT *,GROUP_CONCAT(product_language_id) as all_language_id FROM (`products`) JOIN `product_language_relation` ON `product_language_relation`.`product_id`=`products`.`product_id` WHERE  `product_type` = 'product' AND `active_inactive` = 'active'  AND `stock_status` = 'In Stock' AND `deleted` = '0'  GROUP BY `products`.`product_id` ORDER BY `products`.`product_id` DESC");

                          $rendered_produts = $query->result();

                          if(!empty($rendered_produts))
                          {

                            $final_filtered_products = array();
                                foreach($rendered_produts as $demo_pro)
                                {

                                      $a = explode(',',$demo_pro->all_language_id);

                                      if(count(array_intersect($pro_languages,$a)) == count($pro_languages))
                                      {
                                          $final_filtered_products[] = $demo_pro;
                                      }

                                      $a = "";
                                }
                          }

                            $data['Products'] = $final_filtered_products;


                $data['Product_category'] =  $this->home_model->getwheres_data('product_categories',array('parent_category'=>14));

                 $data['Product_languages'] = $this->home_model->getwheres_data('product_language',array('language_status'=>'Active','deleted'=>'0'));

           }

           elseif(!empty($getData['product_category']) && !empty($getData['product_languages']))
           {

                            $pro_languages =  explode(',',$getData['product_languages']);


                         $query =  $this->db->query("SELECT *,GROUP_CONCAT(product_language_id) as all_language_id FROM (`products`) JOIN `product_language_relation` ON `product_language_relation`.`product_id`=`products`.`product_id` WHERE  `product_type` = 'product' AND `active_inactive` = 'active' AND `product_category_id` = '".$getData["product_category"]."' AND `stock_status` = 'In Stock' AND `deleted` = '0'  GROUP BY `products`.`product_id` ORDER BY `products`.`product_id` DESC");

                          $rendered_produts = $query->result();

                          if(!empty($rendered_produts))
                          {

                            $final_filtered_products = array();
                                foreach($rendered_produts as $demo_pro)
                                {

                                      $a = explode(',',$demo_pro->all_language_id);

                                      if(count(array_intersect($pro_languages,$a)) == count($pro_languages))
                                      {
                                          $final_filtered_products[] = $demo_pro;
                                      }

                                      $a = "";
                                }
                          }

                            $data['Products'] = $final_filtered_products;

                            $data['Product_category'] =  $this->home_model->getwheres_data('product_categories',array('parent_category'=>14));

                             $data['Product_languages'] = $this->home_model->getwheres_data('product_language',array('language_status'=>'Active','deleted'=>'0'));

           }

                   else
                {
                     $data['Products'] = $this->home_model->getwheres_data('products',array('product_type'=>'product','active_inactive'=>'active','stock_status'=>'In Stock','deleted'=>'0'));

                        $data['Product_category'] =  $this->home_model->getwheres_data('product_categories',array('parent_category'=>14));
                         $data['Product_languages'] = $this->home_model->getwheres_data('product_language',array('language_status'=>'Active','deleted'=>'0'));

                         
                }

                  $this->load_view('products/product_view',$data);

        }   
        else
        {
             $data['Products'] = $this->home_model->getwheres_data('products',array('product_type'=>'product','active_inactive'=>'active','stock_status'=>'In Stock','deleted'=>'0'));

                $data['Product_category'] =  $this->home_model->getwheres_data('product_categories',array('parent_category'=>14));
                 $data['Product_languages'] = $this->home_model->getwheres_data('product_language',array('language_status'=>'Active','deleted'=>'0'));

                 $this->load_view('products/product_view',$data);
        }
      
          
    }

}