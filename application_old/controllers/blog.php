<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        //$this->load->model('front_end/cat_frontend');
        
    }

    public function index()
    {
    	$this->load_view('blog/blog');
    }


    public function blog_detail()
    {
    	$this->load_view('blog/blog_detail');
    }

}