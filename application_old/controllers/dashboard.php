<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('home_model');
        $this->load->model('front_end/cat_frontend');
        $this->load->model('dashboard_modal','dashboard');
        $this->load->helper('cart_helper');
    }

    public function chk_login()
    {
    	$UserId = $this->session->userdata('id');
    	if(empty($UserId))
    	{
    		redirect('home','refresh');
    		die();
    	}
    }


	public function index()
	{	
		$this->chk_login();
		$this->dashboard_view('dashboard/my-courses');
	}

	public function my_courses()
	{	
		$this->chk_login();
		$UserId = $this->session->userdata('id');
		$date = date('Y-m-d');
		// $where = array('product_orders.order_user_id'=>$UserId,'products.product_type'=>'e_learning','product_orders.payment_status'=>'paid');
		// $column = array('products.product_id','pname','short_disc','description','image_url');
	 //    $data['user_cources'] = $this->dashboard->get_user_cource_details($column,$where,$ord_clm="",$order_by="", $limit="",$offset='');

	    $data['user_cources'] = $this->home_model->getdata_orderby_where_join2('e_learning_licence','products','course_id','product_id',array('product_type'=>'e_learning','dashboard_id'=>$UserId,'end_date >='=>$date),'product_id,pname,short_disc,description,image_url','id',''); 

	   	
		$this->dashboard_view('dashboard/my-courses', $data);
	}

	public function cource_details()
	{	
		$this->chk_login();
		$product_id = base64_decode($this->input->get('product_id'));
		$UserId = $this->session->userdata('id');

	    $data['cource_details'] = $this->dashboard->get_elearning_product_data('',array('products.product_id'=>$product_id,'product_type'=>'e_learning'),'products.product_id','');

	    $data['Total_lectures'] = $this->home_model->get_total_no_of_lectures($product_id);

	    $data['Chapter'] = $this->home_model->getwhere_data('tbl_chapter',array('sem_id'=>$product_id,'ch_status'=>'active'),'','','');

	    $data['completed_lectures'] = $this->home_model->get_total_no_of_completed_lectures($UserId,$product_id);

	    $data['pro_id'] = $this->input->get('product_id');

	    $data['Instructor'] = $this->home_model->getwhere_data('instructor',array('instructor_course_id'=>$product_id),'','instructor_id','');

	      $data['StarRating'] =  $this->home_model->getwhere_data('star_rating',array('user_id'=>$UserId,'course_id'=>$product_id),'','rating_id','');
	   

		$this->dashboard_view('dashboard/overview_detail', $data);
	}


	public function course_content()
	{
	    $this->chk_login();
		$product_id = base64_decode($this->input->get('product_id'));
		$UserId = $this->session->userdata('id');

	    $data['cource_details'] = $this->dashboard->get_elearning_product_data('',array('products.product_id'=>$product_id,'product_type'=>'e_learning'),'products.product_id','');


	    $data['Total_lectures'] = $this->home_model->get_total_no_of_lectures($product_id);

	    $data['Chapter'] = $this->home_model->getwhere_data('tbl_chapter',array('sem_id'=>$product_id,'ch_status'=>'active'),'','','');

	    $data['completed_lectures'] = $this->home_model->get_total_no_of_completed_lectures($UserId,$product_id);

	     $data['pro_id'] = $this->input->get('product_id');

	       $data['StarRating'] =  $this->home_model->getwhere_data('star_rating',array('user_id'=>$UserId,'course_id'=>$product_id),'','rating_id','');

	   
	    
		$this->dashboard_view('dashboard/course-detail', $data);
	}


	public function video_course($chapter_id="",$lecture_id="",$course_id="")
	{
			$this->chk_login();

			$userId = $this->session->userdata('id');

			if(!empty($chapter_id) && !empty($lecture_id) && !empty($course_id))
			{
					  $data['LectureVideo'] = $this->home_model->getwhere_data('tbl_lecture',array('ch_id'=>$chapter_id,'lect_id'=>$lecture_id,'lect_status'=>'active'),'','','');

			       	 $data['Chapter'] = $this->home_model->getwhere_data('tbl_chapter',array('sem_id'=>$course_id,'ch_status'=>'active'),'','','');

			       	 $data['ActiveLecture'] = $lecture_id;

			       	 $completedlectures = $this->home_model->getwhere_data('completed_lectures',array('course_id'=>$course_id,'ch_id'=>$chapter_id,'lect_id'=>$lecture_id,'user_id'=>$userId),'','completed_lecture_id','');

			       	 if(empty($completedlectures))
			       	 {		
				       	 $updateCompleteLect = array('ch_id'    => $chapter_id,
				       	 							'lect_id'   => $lecture_id,
				       	 							'course_id' => $course_id,
				       	 							'user_id'   => $userId
				       	 						);

				       	 $this->home_model->insert_data('completed_lectures',$updateCompleteLect);
			       	 }



					  $this->dashboard_view('dashboard/video_courses',$data);
			}

			else
			{
				redirect('dashboard/my_courses','refresh');
			}
	}


	// ****************  autocomplete chapter info******************************//\

		public function get_chapter_autocomplete()
		{
			$getData  = $this->input->get();

			 $return_arr = array();

        if(!empty($getData))
        {
             $where = array('ch_status'=>'active','sem_id' => $getData["course_id"]);
             $res= $this->home_model->get_autocomplete_chapter('ch_name,ch_id',$getData["term"],'tbl_chapter',$where);

             if(!empty($res) && count($res)>0)
        {
            $i=0;

          foreach ($res as $r_val) 
          {
              $i++;
              if($i==10)
              {
                break;
              }

           $row_array['value']                = $r_val->ch_name;
           $row_array['chapter_id']           = $r_val->ch_id;
        
           array_push($return_arr, $row_array);
          }
       }

       }

       echo json_encode($return_arr);
		}

	//**********************************************************************//


	public function ajax_chapter_list()
	{
		$postData = $this->input->post();

		if(!empty($postData))
		{
			   $data['Chapter'] = $this->home_model->getwhere_data('tbl_chapter',array('sem_id'=>$postData['course_id'],'ch_status'=>'active','ch_id'=>$postData['chapter_id']),'','','');

			   $this->load->view('dashboard/ajax_chapter_list',$data);
		}
	}


	public function completed_course()
	{	
		$this->chk_login();
		$this->dashboard_view('dashboard/completed-course');
	}
	public function my_result()
	{	
		$this->chk_login();
		$UserId = $this->session->userdata('id');

		$data['MyResult'] = $this->home_model->getwhere_data('tbl_test_result',array('tr_user_id'=>$UserId,'deleted'=>'0'),'','tr_id','');


		$this->dashboard_view('dashboard/my-result',$data);
	}

	public function group_registration($licence_no="",$encryted_course_id="")
	{	

		$this->chk_login();
		$UserId = $this->session->userdata('id');

		$CourseId = base64_decode($encryted_course_id);

		if(!empty($licence_no) && !empty($CourseId))
		{
			$data['LicensedCourses'] = $this->home_model->getdata_orderby_where_join2('e_learning_licence','products','course_id','product_id',array('product_type'=>'e_learning','owner_id'=>$UserId,'e_learning_licence.course_id'=>$CourseId,'licence_no'=>$licence_no,'alloted_user'=>0),'product_id,pname','id',''); 

			$data['GroupUsers'] = $this->home_model->getdata_orderby_where_join2('e_learning_licence','tbl_students','alloted_user','id',array('owner_id'=>$UserId,'e_learning_licence.course_id'=>$CourseId,'licence_no'=>$licence_no),'f_name,l_name,username','tbl_students.id',''); 

			$data['final_quantity'] = $this->get_left_registered_users_no($UserId,$CourseId,$licence_no);

			$data['CourseId']   =  $CourseId;

			$data['licence_no'] =  $licence_no;

			$this->dashboard_view('dashboard/ajax_group_registration_list',$data);
		}
		else
		{
			redirect('purchased-courses','refresh');			
		}

	}


	public function purchased_courses()
	{
		$this->chk_login();
		$UserId = $this->session->userdata('id');

		$data['PurchasedCourses'] = $this->home_model->getdata_orderby_where_join2('e_learning_licence','products','course_id','product_id',array('product_type'=>'e_learning','owner_id'=>$UserId,'alloted_user'=>0),'product_id,pname,course_duration,licence_no,alloted_date,end_date,purchase_product_quantity,e_learning_licence.course_id','id','');


		  $this->dashboard_view('dashboard/purchased_courses',$data);
	}





	// ************************  adding new user to group for elearning license ************************//

	public function add_new_user_to_group()
	{
		$this->chk_login();
		$postData = $this->input->post();

		if(!empty($postData))
		{
			$UserId  = $this->session->userdata('id');

			$get_ordeId = $this->home_model->getwhere_data('e_learning_licence',array('owner_id'=>$UserId,'course_id'=>$postData['course_id'],'alloted_user'=>0,'licence_no'=>$postData['licence_no']),'purchase_product_quantity,order_id','id','');

			if(!empty($get_ordeId))
			{
				$product_quantity  = $get_ordeId[0]->purchase_product_quantity;
				$order_id          = $get_ordeId[0]->order_id;

				if($product_quantity>1)
				{
					// we will minus 1 qunatity since because itself owner will access the course 

					$count_user = $this->home_model->get_total_no_of_alloted_userfor_course($UserId,$postData['course_id'],$order_id);

					if(!empty($count_user))
					{
						$alloted_user_count = $count_user[0]->total_alloted_user;
					}
					else
					{
						$alloted_user_count = 0;
					}

					

					$remaining_quantity = $product_quantity - 1; // here one quantity is minus because owner also acquire 1 quantity

					$final_quantity     = $remaining_quantity - $alloted_user_count;

					if($final_quantity!= 0 && $final_quantity>0)
					{
						$check_user = $this->home_model->getwhere_data('tbl_students',array('username'=>$postData['alloted_user_email']),'','id','');

						$elearningDetails = $this->home_model->getwhere_data('e_learning_licence',array('owner_id'=>$UserId,'course_id'=>$postData['course_id'],'order_id'=>$order_id),'','id','');

						if(count($check_user)>0 && !empty($elearningDetails))
						{
							          $newAllocation = array('owner_id'    => $UserId,
														   'course_id'   => $postData['course_id'],
														   'alloted_user'=> $NewuserId,
														   'alloted_date'=> date('Y-m-d H:i:s'),
														   'end_date'    => $elearningDetails[0]->end_date,
														   'order_id'    => $elearningDetails[0]->order_id,
														   'licence_no'  => $postData['licence_no']
														);

									$this->home_model->insert_data('e_learning_licence',$newAllocation);

									echo "SUCCESS";
									die();

						}

						else
						{
							$pass = mt_rand(1000,9999);
							$newPass = md5($pass);

							$newData = array('username'    => $postData['alloted_user_email'],
											 'f_name'      => $postData['alloted_user_first_name'],
											 'l_name'      => $postData['alloted_user_last_name'],
											 'full_name'   => $postData['alloted_user_first_name'].' '.$postData['alloted_user_last_name'],
											 'password'    => $newPass
											);

							$NewuserId = $this->home_model->insert_data('tbl_students',$newData);
							$newData = "";

							if(!empty($NewuserId))
							{
								

								$courseName = $this->home_model->getwhere_data('products',array('product_id'=>$postData['course_id'],'product_type'=>'e_learning'),'pname','product_id','');

								$OwnerDetails = $this->home_model->getwhere_data('tbl_students',array('id'=>$UserId),'f_name,l_name','id','');

								if(!empty($elearningDetails))
								{
									$newAllocation = array('owner_id'    => $UserId,
														   'course_id'   => $postData['course_id'],
														   'alloted_user'=> $NewuserId,
														   'alloted_date'=> date('Y-m-d H:i:s'),
														   'end_date'    => $elearningDetails[0]->end_date,
														   'order_id'    => $elearningDetails[0]->order_id,
														   'dashboard_id'=> $NewuserId,
														   'licence_no'  => $postData['licence_no']
														);

									$this->home_model->insert_data('e_learning_licence',$newAllocation);


									 		/***Mail For The Server***/ 
							                $config['mailtype'] = 'html';
							                $config['charset'] = 'iso-8859-1';
							                $config['priority'] = 1;
							                $this->email->initialize($config);
							                $this->email->clear();

							                $message = "";


											$message.='<!DOCTYPE html>
											<html>
											<head>
												<title>Course Allocation</title>
											</head>
											<body>

											<div style="width: 1070px; max-width: 100%; height: auto; border:1px solid gray; margin:50px auto;">
												<div style="background-color: #D55220;">
													<img src="http://ujjwalpatni.com/front_assets/logo.png" style="height: 55px;">
												</div>
												<div style="font-size: 16px;font-family: sans-serif;text-align: left; margin: 35px 0 40px  20px">
													<p>Dear '.$postData['alloted_user_first_name'].' '.$postData['alloted_user_last_name'].',</p>
													<P>You have been registered by <b>'.$OwnerDetails[0]->f_name.' '.$OwnerDetails[0]->l_name.'</b> for the course <b>'.$courseName[0]->pname.'</b>.</p>
													<p>Please find below your login details:</p>
													<table>
													<tr>
														<th style="width: 190px;height: 30px;">URL: '.base_url().'</th>
														<td>URL</td>
													</tr>
													<tr>
														<th style="width: 190px;height: 30px;">Email:</th>
														<td>'.$postData['alloted_user_email'].'</td>
													</tr>
													<tr>
														<th style="width: 190px;height: 30px;">Password:</th>
														<td>'.$pass.'</td>
													</tr>
													
												
												</table>
												
											     

												<p style="margin:25px 0 5px; ">Thanks!</p>
												<p style="margin:5px 0;">Regards,</p>
												<p style="margin:5px 0;">Team Ujjwal Patni</p>
												</div>
											</div>

											</body>
											</html>';



						                $to = $postData['alloted_user_email'];
						                $from_email = "training@ujjwalpatni.com";
						                $this->email->from($from_email,'Ujjwal Patni');
						                $this->email->to($to); 
						                $this->email->subject('Course Allocation');
						                $this->email->set_mailtype('html');
						                $this->email->message($message);  
						                $send = $this->email->send();

						                echo "SUCCESS";
								}
							}
						}
					}
					else
					{
						echo "QUANTITY_OVER";
					}
				}
				else
				{
					echo "MIN_QTY";
				}
				
			}
		}
	}

	//*************************************************************************************************//


	public function get_left_registered_users_no($UserId="",$CourseId="",$licence_no="")
	{
		$get_ordeId = $this->home_model->getwhere_data('e_learning_licence',array('owner_id'=>$UserId,'course_id'=>$CourseId,'alloted_user'=>0,'licence_no'=>$licence_no),'purchase_product_quantity,order_id','id','');

			if(!empty($get_ordeId))
			{
				$product_quantity  = $get_ordeId[0]->purchase_product_quantity;
				$order_id          = $get_ordeId[0]->order_id;

				if($product_quantity>1)
				{
					// we will minus 1 qunatity since because itself owner will access the course 

					$count_user = $this->home_model->get_total_no_of_alloted_userfor_course($UserId,$CourseId,$order_id);

					if(!empty($count_user))
					{
						$alloted_user_count = $count_user[0]->total_alloted_user;
					}
					else
					{
						$alloted_user_count = 0;
					}

					

					$remaining_quantity = $product_quantity - 1; // here one quantity is minus because owner also acquire 1 quantity

					$final_quantity    = $remaining_quantity - $alloted_user_count;

					return $final_quantity;
	            }
	            else
	            {
	            	$final_quantity = 0;
	            	return $final_quantity;
	            }
            }
        }



	public function group_result()
	{	
		$this->chk_login();
		$UserId = $this->session->userdata('id');


		$GroupUsers = $this->home_model->getwhere_data('e_learning_licence',array('owner_id'=>$UserId,'alloted_user !='=>0),'alloted_user','id','');

		if(!empty($GroupUsers))
		{
			foreach($GroupUsers as $grpDetails)
			{
				$final_grp_users[] = $grpDetails->alloted_user;
			}

			$WhereIn = $this->db->where_in('tr_user_id', $final_grp_users);
		    $data['GroupResult'] = $this->home_model->getdata_orderby_where_in_and_not_in_join2('tbl_students','tbl_test_result','id','tr_user_id',$WhereIn,array('deleted'=>'0'),'','tr_id','');
		}
		else
		{
			 $data['GroupResult'] = "";
		}

		$this->dashboard_view('dashboard/group-result',$data);
	}




	public function my_profile()
	{
		$this->chk_login();
		$this->dashboard_view('dashboard/my_profile');
	}


	public function change_password()
	{
	    $this->chk_login();
		$user_id = $this->session->userdata('id');
		$postData = $this->input->post();
		if (!empty($postData))
		{
			$current_password = md5($postData['password']);
			$new_password     = $postData['new_password'];
			$cpassword        = $postData['confirm_password'];
			$password = array(
				'password' => $current_password
			);
			$check_pass = $this->cat_frontend->getwheres('tbl_students', $password);

			

			if (empty($check_pass))
			{
				echo "CORRECT_PASSWORD";
				die();
			}

			if ($new_password != $cpassword)
			{
				echo "NO_MATCH";
				die();
			}

			$update_pass = array(
				'password' => md5($new_password)
			);
			$this->cat_frontend->update_data('tbl_students', $update_pass, array(
				'id' => $user_id
			));
			echo "SUCCESS";
		}
	}


	 // ***************************  *****************************//

	public function course_details()
	{
		$postData = $this->input->post();
		if(!empty($postData))
    	{
    		$product_url = $postData['CourseUrl'];

    		$data['e_learning_product'] = $this->home_model->getwhere_data('products',array('url'=>$product_url,'active_inactive'=>'active','product_type'=>'e_learning'),'product_id,url,pname,image_url,short_disc,description,product_price,disc_price,product_type,video_url,product_category_id,course_type','product_id','');

    		$data['product_meta'] =  $this->home_model->getwhere_data('product_data_relation',array('product_id'=>$data['e_learning_product'][0]->product_id),'','product_id','');

            $data['Cities'] = $this->home_model->getwhere_data('cities',array(),'','city_id','');

             $data['product_category'] = $this->home_model->getwhere_limit_data('product_categories',array('parent_category'=>14,'active_status'=>'Active'),'id,name,category_url,cat_img','id','',4);


             if(!empty($data['e_learning_product']))
             {
                $product_category_id = $data['e_learning_product'][0]->product_category_id;
                $course_type         = $data['e_learning_product'][0]->course_type;
                $product_id          = $data['e_learning_product'][0]->product_id;

                $data['Related_courses'] = $this->home_model->getwhere_data('products',array('product_category_id'=>$product_category_id,'active_inactive'=>'active','product_type'=>'e_learning','course_type'=>$course_type,'product_id !='=>$product_id),'product_id,url,pname,image_url,short_disc,url','product_id','');
             }


    		if(!empty($this->session->userdata('id')))
    		{
    			$userId = $this->session->userdata('id');
    			$data['UserDetail'] = $this->home_model->getwhere_data('tbl_students',array('id'=>$userId),'','id','');
    		}

    		

			$this->load_view('elearning/e_product_detail',$data);
    	}
    	else
    	{
    		redirect('elearning/courses','refresh');
    	}
	}


	//***************************************************************************************//


	//********************* New Star Rating *************************//

	public function new_star_rating()
	{
		$postData = $this->input->post();

		if(!empty($postData))
		{
			$UserId = $this->session->userdata('id');

			$newStar = array('user_id'   => $UserId,
							 'course_id' => $postData['course_id'],
							 'rating_star'=> $postData['rating']
							);

			$this->home_model->insert_data('star_rating',$newStar);

			redirect('dashboard/course_content?product_id='.base64_encode($postData['course_id']).'','refresh');
		}
	}


	//********************************************************************//

}

?>